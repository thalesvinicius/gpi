package br.com.gpi.server.core.test;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;

/**
 * Utilize essa classe para utilizar de base para fazer as fixtures
 *
 * @author paulovfm
 */
public class JSONSerializerUtil {

    private ObjectMapper objectMapper;

    public ObjectMapper getObjectMapper() {
        if (objectMapper == null) {
            objectMapper = new ObjectMapper();
        }
        return objectMapper;
    }

    @Test
    private void serializeObject() {

    }

}
