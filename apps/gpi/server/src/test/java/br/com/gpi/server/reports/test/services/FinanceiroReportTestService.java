/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.gpi.server.reports.test.services;

import br.com.gpi.server.domain.entity.Financeiro;
import br.com.gpi.server.domain.service.FinanceiroService;
import br.com.gpi.server.domain.service.reports.BaseReportService;
import br.com.gpi.server.util.reports.ReportUtil;
import static br.com.gpi.server.util.reports.ReportUtil.paramsExpr;
import java.util.Collection;
import java.util.Map;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperPrint;

/**
 *
 * @author rafaelbfs
 */
public class FinanceiroReportTestService extends BaseReportService<Financeiro>{

    @Inject
    private FinanceiroService financeiroService;
    
    @Inject
    public FinanceiroReportTestService(EntityManager em) {
        super(em);
    }
    
    public byte[] relatorioFinanceiroPorProjeto(long idProjeto) throws Exception{
        Map<String, Object> params = paramsExpr.init(ReportUtil.subReportsMapExpr.apply(defaultStyle()));
        params.put("PROJETO_ID", idProjeto);
        params.put("HIDRO_MINERACAO_SIDERURGIA", Boolean.FALSE);
        params.put("SUCRO_ENERGETICO",Boolean.FALSE);
        
        Collection<Financeiro> financ = financeiroService.findAll();
        
        JasperPrint jp;
        jp = generateReport(params, 
                resourceAsStream("/jasper/DadosFinanceirosICE.jasper"),
                jrBeanCollection(financ));
        return JasperExportManager.exportReportToPdf(jp);
    }
    
    
    
}
