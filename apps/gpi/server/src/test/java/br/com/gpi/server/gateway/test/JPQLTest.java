package br.com.gpi.server.gateway.test;

import br.com.gpi.server.domain.entity.Projeto;
import java.util.List;
import java.util.logging.Logger;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import org.apache.deltaspike.core.api.projectstage.ProjectStage;
import org.apache.deltaspike.testcontrol.api.TestControl;
import org.apache.deltaspike.testcontrol.api.junit.CdiTestRunner;
import static org.fest.assertions.api.Assertions.*;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(CdiTestRunner.class)
@TestControl(projectStage = ProjectStage.UnitTest.class)
public class JPQLTest {

    @Inject
    private Logger log;
    @Inject
    private EntityManager em;

    @Test
    public void should_search_successfully() {
        TypedQuery<Projeto> projetoQuery = em.createQuery("SELECT p FROM Projeto p ", Projeto.class);
        List<Projeto> resultList = projetoQuery.getResultList();
        assertThat(resultList).isNotNull();
        resultList.forEach(System.out::println);
    }

}
