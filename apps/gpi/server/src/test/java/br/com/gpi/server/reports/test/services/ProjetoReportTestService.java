package br.com.gpi.server.reports.test.services;

import br.com.gpi.server.domain.entity.UsoFonte;
import br.com.gpi.server.domain.service.ProjetoService;
import br.com.gpi.server.domain.service.reports.BaseReportService;
import static br.com.gpi.server.util.reports.ReportUtil.RELATORIO_PROJETOS;
import static br.com.gpi.server.util.reports.ReportUtil.paramsExpr;
import static br.com.gpi.server.util.reports.ReportUtil.subReportsMapExpr;
import java.io.InputStream;
import java.util.Map;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import net.sf.jasperreports.engine.JasperExportManager;

public class ProjetoReportTestService extends BaseReportService<UsoFonte> {
    
    @Inject
    private ProjetoService projetoService;
    
    @Inject
    public ProjetoReportTestService(EntityManager em) {
        super(em);
    }
    
    public byte[] informacoesProjetos(Long idEmpresa) throws Exception{
        Map<String,Object> map = subReportsMapExpr.apply(defaultStyle());
        
        return JasperExportManager.exportReportToPdf( generateReport( () ->  {
                    Map<String,Object> params = paramsExpr.init(map);
                    params.put("EMPRESA_ID", idEmpresa);
                    return  params;
                } , getClass().getResourceAsStream(RELATORIO_PROJETOS)));
    }
    
}
