package br.com.gpi.server.reports.test.services;

import br.com.gpi.server.domain.entity.Cronograma;
import br.com.gpi.server.domain.service.CronogramaService;
import br.com.gpi.server.domain.service.reports.BaseReportService;
import static br.com.gpi.server.util.reports.ReportUtil.*;
import java.io.InputStream;
import java.util.Collections;
import java.util.Locale;
import java.util.Map;
import java.util.function.Supplier;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import net.sf.jasperreports.engine.JasperExportManager;


public class CronogramaReportTestService extends BaseReportService<Cronograma> {
    
    @Inject
    private CronogramaService cronogramaService;

    @Inject
    public CronogramaReportTestService(EntityManager em) {
        super(em);
    }
    
    public byte[] informacoesCronograma(Long idProjeto) throws Exception{
        Map<String,Object> map = subReportsMapExpr.apply(defaultStyle());
        Cronograma cronograma = cronogramaService.findByProject(idProjeto);
        Supplier<Map<String,Object>> sup = () ->  {
                    Map<String,Object> params = paramsExpr.init(map);
                    params.put("REPORT_LOCALE", new Locale("pt", "BR"));
                    return  params;
        } ;
        return JasperExportManager.exportReportToPdf( generateReport( sup.get(), getClass().getResourceAsStream(RELATORIO_CRONOGRAMA)
                , jrBeanCollection(Collections.singleton(cronograma))
        ));
    }

}
