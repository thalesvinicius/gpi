package br.com.gpi.server.core.test;

import java.util.HashMap;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.spi.PersistenceUnitTransactionType;
import org.jboss.util.Strings;
import org.junit.BeforeClass;

/**
 * This is a abstraction to provide database access for tests.
 *
 */
public abstract class AbstractDBTest {

    private static EntityManager em;
    private static EntityManagerFactory emFactory;
    private static final String TRANSACTION_TYPE = "javax.persistence.transactionType";
    private static final String JDBC_URL = "javax.persistence.jdbc.url";
    private static final String JDBC_DRIVER = "javax.persistence.jdbc.driver";
    private static final String JDBC_USER = "javax.persistence.jdbc.user";
    private static final String JDBC_PASSWORD = "javax.persistence.jdbc.password";
    protected static final String H2_MEM_JDBC_URL = "jdbc:h2:~/db;MODE=MSSQLServer;AUTO_SERVER=TRUE";
//    protected static final String H2_MEM_JDBC_URL = "jdbc:h2:tcp://localhost/mem:gpi;MODE=MSSQLServer";
    protected static final String H2_MEM_JDBC_USER = "SA";
    protected static final String H2_DRIVER = "org.h2.Driver";

    @BeforeClass
    public static void setup() {
        Map<String, String> configOverrides = getDatabaseProperties();
        AbstractDBTest.emFactory = Persistence.createEntityManagerFactory("gpi-test-pu", configOverrides);
        AbstractDBTest.em = AbstractDBTest.emFactory.createEntityManager();
    }

    protected static EntityManager getEm() {
        return AbstractDBTest.em;
    }

    protected static EntityManagerFactory getEmFactory() {
        return AbstractDBTest.emFactory;
    }

    public static Map<String, String> getDatabaseProperties() {
        Map<String, String> configOverrides = new HashMap<>();

        configOverrides.put(TRANSACTION_TYPE,
                PersistenceUnitTransactionType.RESOURCE_LOCAL.name());
        configOverrides.put(JDBC_URL, H2_MEM_JDBC_URL);
        configOverrides.put(JDBC_DRIVER, H2_DRIVER);
        configOverrides.put(JDBC_USER, H2_MEM_JDBC_USER);
        configOverrides.put(JDBC_PASSWORD, Strings.EMPTY);

        return configOverrides;
    }
}
