package br.com.gpi.server.core.test;

import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Alternative;
import javax.enterprise.inject.Default;
import javax.enterprise.inject.Disposes;
import javax.enterprise.inject.Produces;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.transaction.UserTransaction;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import static org.mockito.Mockito.mock;

public class TestProducers {

    @Produces
    public Validator mockValidator() {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        return factory.getValidator();
    }

    @Produces
    public UserTransaction mockTransaction() {
        return mock(UserTransaction.class);
    }

    private final EntityManagerFactory emf = Persistence.createEntityManagerFactory("gpi-test-pu");

    @Produces
    @Alternative
    @RequestScoped
    public EntityManager create() {
        return emf.createEntityManager();
    }

    public void dispose(@Disposes @Default EntityManager entityManager) {
        if (entityManager.isOpen()) {
            entityManager.close();
        }
    }
}
