package br.com.gpi.server.core.test;

import br.com.gpi.server.querybuilder.Condition;
import br.com.gpi.server.querybuilder.ConditionFactory;
import br.com.gpi.server.querybuilder.DynamicQuery;
import br.com.gpi.server.querybuilder.Locator;
import br.com.gpi.server.to.ConditionTO;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.junit.Test;

public class BuscaAvancadaServiceTest {

    @Test
    public void shouldSearchSimpleCompanyField() {
        DynamicQuery dynamicQuery = new DynamicQuery();
        List list = new ArrayList<>(Locator.CAMPOS_MAP.values());
        dynamicQuery.addSelect(list);

        ConditionTO[] toList = {new ConditionTO(new String[]{"anglo"}, "empresaUnidade"),
            new ConditionTO(new String[]{"10", "1100"}, "empregoDirPermanenteTotalPrevisto"),
            new ConditionTO(new String[]{"1", "2"}, "empresaNaturezaJuridica"),
            new ConditionTO(new String[]{"1", "2"}, "projetoEstagiosAlteracao"),
            new ConditionTO(new String[]{"1351"}, "insumoNCM"),
            new ConditionTO(new String[]{"2013-02-01 00:00:00.0", "2014-02-01 00:00:00.0"}, "projetoEstagioAlteracaoDatas"),
            new ConditionTO(new String[]{"2013-02-01 00:00:00.0", "2014-02-01 00:00:00.0"}, "projetoEstagiosPrevisao", new ConditionTO(new String[]{"2", "4"}, ""))};

        ConditionFactory factory = new ConditionFactory();
        Arrays.asList(toList).stream().forEachOrdered(to -> {
            Condition condition = factory.getInstance(to);
            if (condition != null) {
                dynamicQuery.addCondition(condition);
            }
        });
        
        dynamicQuery.addOrderBy(Locator.campoEmpresaNomePrincipal, Locator.campoProjetoNome);

        System.out.println(dynamicQuery.getSQL());
    }

}
