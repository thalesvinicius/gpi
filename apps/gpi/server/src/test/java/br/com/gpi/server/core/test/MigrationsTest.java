package br.com.gpi.server.core.test;

import org.flywaydb.core.Flyway;
import org.junit.Test;

public class MigrationsTest extends AbstractDBTest {

    @Test
    public void should_insert_all_migrations_scripts() {
        Flyway flyway = new Flyway();
        flyway.setDataSource(AbstractDBTest.H2_MEM_JDBC_URL, AbstractDBTest.H2_MEM_JDBC_USER, null);
        flyway.clean();
        flyway.migrate();
    }

}
