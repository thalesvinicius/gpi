package br.com.gpi.server.core.test;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.Entity;
import javax.persistence.Query;
import org.flywaydb.core.Flyway;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import org.reflections.Reflections;

/**
 * Tests all enumerations that belongs a domain table
 *
 * @author <a href="mailto:pviniciusfm@gmail.com">Paulo Vinicius F. Machado</a>
 */
@RunWith(Parameterized.class)
public class ORMTest extends AbstractDBTest {

    private final Class<?> entityClass;
    private static final Logger LOG = Logger.getLogger(ORMTest.class.getName());
    private static final int FIRST_RESULT = 0;

    public ORMTest(Class<?> entity) {
        this.entityClass = entity;
    }

    @BeforeClass
    public static void setupBD() {
        Flyway flyway = new Flyway();
        flyway.setDataSource(H2_MEM_JDBC_URL, H2_MEM_JDBC_USER, null);
        flyway.clean();
        flyway.migrate();
    }

    @Parameters(name = "{0}")
    public static Collection<Object[]> data() {
        Reflections entityClasses = new Reflections("br.com.gpi.server.domain.entity");
        Object[] entities = entityClasses.getTypesAnnotatedWith(Entity.class).toArray();
        Object[][] result = new Object[entities.length][];
        for (int i = 0; i < entities.length; i++) {
            result[i] = new Object[]{entities[i]};
            LOG.warning(entities[i].toString());
        }
        return Arrays.asList(result);
    }

    @Test
    public void testEntity() throws Exception {
        boolean result = true;
        Entity entity = entityClass.getAnnotation(Entity.class);
        if (entity != null) {
            final String className = entityClass.getSimpleName();
            try {
                Query q = getEm().createQuery("SELECT c FROM " + className + " c");
                q.setMaxResults(1);
                List<?> resultList = q.getResultList();
                if (resultList != null && !resultList.isEmpty()) {
                    resultList.get(FIRST_RESULT);
                }
            } catch (Exception ex) {
                LOG.log(Level.SEVERE, "Error in entity test.", ex);
                result = false;
            }
        }
        Assert.assertTrue(result);
    }
}
