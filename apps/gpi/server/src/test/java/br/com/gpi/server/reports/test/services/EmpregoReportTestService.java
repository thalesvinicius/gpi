package br.com.gpi.server.reports.test.services;

import br.com.gpi.server.domain.entity.Emprego;
import br.com.gpi.server.domain.service.reports.BaseReportService;
import java.io.FileNotFoundException;
import java.sql.SQLException;
import java.util.Map;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperPrint;
import static br.com.gpi.server.util.reports.ReportUtil.*;
import java.io.InputStream;


/**
 *
 * @author rafaelbfs
 */
public class EmpregoReportTestService extends BaseReportService<Emprego> {

    @Inject
    public EmpregoReportTestService(EntityManager em) {
        super(em);
    }
    
    private Map<String,Object> subReports(){
        Map<String,Object> mapa = subReportsMapExpr.apply(defaultStyle());
        mapa.put("empregosSucroEnergeticos", resourceAsStream(RELATORIO_EMPREGO_SUCRO_ENERG));
        return mapa;
    }

    public byte[] relatorioEmpregosPorProjeto(long idProjeto) throws JRException, FileNotFoundException, SQLException {
        Map<String, Object> params = paramsExpr.init(subReports());
        params.put("PROJETO_ID", idProjeto);
        params.put("HIDRO_MINERACAO_SIDERURGIA", Boolean.FALSE);
        params.put("SUCRO_ENERGETICO",Boolean.FALSE);
        
        JasperPrint jp;
        jp = generateReport(()->params, resourceAsStream("/jasper/EmpregoICE.jasper"));
        return JasperExportManager.exportReportToPdf(jp);
    }

}
