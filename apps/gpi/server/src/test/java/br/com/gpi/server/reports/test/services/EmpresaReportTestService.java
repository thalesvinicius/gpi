/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.gpi.server.reports.test.services;

import br.com.gpi.server.domain.entity.Empresa;
import br.com.gpi.server.domain.service.reports.BaseReportService;
import static br.com.gpi.server.util.reports.ReportUtil.*;
import java.io.InputStream;
import java.util.Map;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;

/**
 *
 * @author rafaelbfs
 */
public class EmpresaReportTestService extends BaseReportService<Empresa> {

    @Inject
    public EmpresaReportTestService(EntityManager em) {
        super(em);        
    }  
        
    
    public byte[] informacoesEmpresaPorProjeto(Long idProjeto) throws Exception{
        Map<String,Object> map = subReportsMapExpr.apply(defaultStyle());
        
        map.put(CHAVE_CONTATOS_EMPRESA_ICE,resourceAsStream(RELATORIO_CONTAOS_EMPRESA_ICE));
        
        return JasperExportManager.exportReportToPdf( generateReport( () ->  {
                    Map<String,Object> params = paramsExpr.init(map);
                    params.put("EMPRESA_ID", 1L); 
                    params.put("PROJETO_ID", 1L);
                    return  params;
                } , getClass().getResourceAsStream(RELATORIO_DADOS_EMPRESA)
        ));
    }
    
    
    public byte[] relatorioCnaes() throws Exception{
        return JasperExportManager.exportReportToPdf(
                generateReport(() -> paramsExpr.init(
                        subReportsMapExpr.apply(defaultStyle())),
                        getClass().getResourceAsStream(RELATORIO_CNAE_ICE)));
        
    }
    
    public byte[] cabecalho(Map<String,Object> params) throws Exception{
        return JasperExportManager.exportReportToPdf(
                generateReport( () -> { return params; },
                resourceAsStream(CABECALHO_REL_CARTEIRA_PROJETO)));
    }
    
    
    
}
