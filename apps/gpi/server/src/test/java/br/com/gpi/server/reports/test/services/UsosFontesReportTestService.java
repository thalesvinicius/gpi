package br.com.gpi.server.reports.test.services;

import br.com.gpi.server.domain.entity.UsoFonte;
import br.com.gpi.server.domain.service.UsoFonteService;
import br.com.gpi.server.domain.service.reports.BaseReportService;
import static br.com.gpi.server.util.reports.ReportUtil.RELATORIO_USO_FONTES;
import static br.com.gpi.server.util.reports.ReportUtil.paramsExpr;
import static br.com.gpi.server.util.reports.ReportUtil.subReportsMapExpr;
import java.util.List;
import java.util.Map;
import java.util.function.Supplier;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import net.sf.jasperreports.engine.JasperExportManager;

public class UsosFontesReportTestService extends BaseReportService<UsoFonte> {
    
    @Inject
    private UsoFonteService usoFonteService;
    
    @Inject
    public UsosFontesReportTestService(EntityManager em) {
        super(em);
    }

    public byte[] relatorioUsosFontesPorProjeto(Long idProjeto) throws Exception{
        Map<String,Object> map = subReportsMapExpr.apply(defaultStyle());
        List<UsoFonte> usoFontes = usoFonteService.findAllUsoFonteByProjeto(idProjeto);
        Supplier<Map<String,Object>> sup = () ->  {
                    Map<String,Object> params = paramsExpr.init(map);
                    return  params;
        } ;
        return JasperExportManager.exportReportToPdf( generateReport( sup.get(), getClass().getResourceAsStream(RELATORIO_USO_FONTES)
                , jrBeanCollection(usoFontes)
        ));
    }
    
}
