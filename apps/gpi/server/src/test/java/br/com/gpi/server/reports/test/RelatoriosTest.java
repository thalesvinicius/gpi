package br.com.gpi.server.reports.test;

import br.com.gpi.server.domain.service.PleitoService;
import br.com.gpi.server.domain.service.ProjetoService;
import br.com.gpi.server.domain.service.reports.gerencial.RelatorioGerencial04CarteiraProjetos;
import br.com.gpi.server.domain.service.reports.ice.ImpressaoICEReportService;
import br.com.gpi.server.domain.service.reports.nt.NotaTecnicaReportService;
import br.com.gpi.server.reports.test.services.CronogramaReportTestService;
import br.com.gpi.server.reports.test.services.EmpregoReportTestService;
import br.com.gpi.server.reports.test.services.EmpresaReportTestService;
import br.com.gpi.server.reports.test.services.FinanceiroReportTestService;
import br.com.gpi.server.reports.test.services.InformacoesComplementaresTestService;
import br.com.gpi.server.reports.test.services.ProdutoInsumoReportTestService;
import br.com.gpi.server.reports.test.services.ProjetoReportTestService;
import br.com.gpi.server.reports.test.services.UsosFontesReportTestService;
import br.com.gpi.server.util.reports.ReportUtil;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import javax.inject.Inject;
import org.apache.deltaspike.core.api.projectstage.ProjectStage;
import org.apache.deltaspike.testcontrol.api.TestControl;
import org.apache.deltaspike.testcontrol.api.junit.CdiTestRunner;
import org.h2.tools.RunScript;
import org.hamcrest.CoreMatchers;
import org.junit.After;
import static org.junit.Assert.assertThat;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 *
 * @author rafaelbfs
 */
@RunWith(CdiTestRunner.class)
@TestControl(projectStage = ProjectStage.UnitTest.class)
public class RelatoriosTest {

    @Inject
    private ImpressaoICEReportService imp;
    @Inject 
    private NotaTecnicaReportService ntPrint;
    @Inject
    private EmpresaReportTestService empresaService;
    //@Inject
    private EmpregoReportTestService empregoService;
    //@Inject
    private ProdutoInsumoReportTestService proutosInsumosService;
    //@Inject
    private UsosFontesReportTestService usosFontesReportTestService;
    //@Inject
    private ProjetoReportTestService projetoReportTestService;
    //@Inject
    private FinanceiroReportTestService financeiroReportTestService;
    //@Inject
    private CronogramaReportTestService cronogramaReportTestService;
    @Inject
    private InformacoesComplementaresTestService informacoesComplementaresTestService;
    @Inject
    private PleitoService pleitoService;
    @Inject 
    private ProjetoService projetoServ; 
    @Inject
    private RelatorioGerencial04CarteiraProjetos relatorioGerencial04CarteiraProjetos;
    
    
    
    @Before
    public void before() throws SQLException{
        Connection conn = empresaService.getJDBCConnection();
        
        InputStreamReader file = new InputStreamReader(empresaService.resourceAsStream("/db/dump.sql"));
        //dropAll(conn);
        RunScript.execute(conn,file );  
        conn.close();
    }
    
    @After
    public void after() throws SQLException{
        
        Connection conn = empresaService.getJDBCConnection();
        dropAll(conn);
        conn.close();
    }
    
    private void dropAll(Connection conn) throws SQLException{
        
        //PreparedStatement ps = conn.prepareStatement("DROP ALL OBJECTS; \n COMMIT;");
        //ps.executeUpdate();
        //ps.close();
        
    }
    
    @Test
    public void testarRelatorioGerencial04() throws Exception{
        
//        JasperReportBuilder result = relatorioGerencial04CarteiraProjetos.buildCarteira(null, 
//                Arrays.asList(EnumCadeiaProdutiva.ENERGIAS_HIDRICAS.getId().longValue(),EnumCadeiaProdutiva.SUCROENERGETICO.getId().longValue()),
//                Arrays.asList(SituacaoProjetoEnum.PEDENTE_VALIDACAO), Arrays.asList(50,51,52,53,54,55,56,57), null,
//                Collections.emptyList());
//       
        Map<String,Object> params = new HashMap<>();
        params.put("TITULO", "Carteira de Projetos");
        params.put("ESTAGIOS","A S D C H L");
        params.put("STATUS","ST1 ST2 ST3 ST4");
        params.put("ANALISTA","analista 1 Analista 2");
        params.put("REGIAO_PLANEJAMENTO","Todas");
        params.put("LOGO_INDI",relatorioGerencial04CarteiraProjetos.getImage(ReportUtil.LOGO_INDI_NOME));
        byte[] b = empresaService.cabecalho(params);
        FileOutputStream fos = new FileOutputStream(File.createTempFile("relatorioGerencial4",".pdf"));
        fos.write(b);
        fos.flush();
        fos.close();
//        EnumMimeTypes.PDF.exportReport(result).get().writeTo(fos);       
        
    }
    
    
    @Ignore
    public void testarRelatorioUC07() throws Exception {

        ByteArrayOutputStream bos = new ByteArrayOutputStream();
       
        
        byte[] bytes = imp.imprimirICE(Arrays.asList(1L,2L,7L,8L),null);
        bos.write(bytes);
        bos.writeTo(new FileOutputStream(File.createTempFile("imprimirICE", ".pdf")));
        assertThat(bytes.length > 0, CoreMatchers.equalTo(Boolean.TRUE));

    }
    
    @Ignore
    public void validateICE() {
        pleitoService.dryValidation(projetoServ.findById(7L),1L);
    }
    
    @Ignore
    public void testarRelatorioUC09() throws Exception {

        ByteArrayOutputStream bos = new ByteArrayOutputStream();        
        byte[] bytes = ntPrint.gerarNotaTecnica(1L);
        bos.write(bytes);
        bos.writeTo(new FileOutputStream(File.createTempFile("imprimirNT", ".pdf")));
        assertThat(bytes.length > 0, CoreMatchers.equalTo(Boolean.TRUE));

    }
    
    
    @Ignore
    public void testarSubEmpresas() throws Exception{
        
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
       
        byte[] bytes = empresaService.informacoesEmpresaPorProjeto(1L);
        bos.write(bytes);
        bos.writeTo(new FileOutputStream(File.createTempFile("meuPrint.pdf", ".pdf")));
        assertThat(bytes.length > 0, CoreMatchers.equalTo(Boolean.TRUE));
        
    }
    
    @Ignore
    public void testarCnaes() throws Exception{
        
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
       
        byte[] bytes = empresaService.relatorioCnaes();
        bos.write(bytes);
        bos.writeTo(new FileOutputStream(File.createTempFile("meuCnaes.pdf", ".pdf")));
        assertThat(bytes.length > 0, CoreMatchers.equalTo(Boolean.TRUE));
        
    }
    
    @Ignore
    public void testarEmpregos() throws Exception{
        
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
       
        byte[] bytes = empregoService.relatorioEmpregosPorProjeto(1L);
        bos.write(bytes);
        bos.writeTo(new FileOutputStream(File.createTempFile("empregosTEST.pdf", ".pdf")));
        assertThat(bytes.length > 0, CoreMatchers.equalTo(Boolean.TRUE));
        
    }
    
    @Ignore
    public void testarProdutosInsumos() throws Exception {

        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ArrayList<Long> arrayList = new ArrayList<>();
        arrayList.add(1L);
        byte[] bytes = proutosInsumosService.informacoesProdutosInsumos(arrayList);
        bos.write(bytes);
        bos.writeTo(new FileOutputStream(File.createTempFile("produtosInsumosTEST.pdf", ".pdf")));
        assertThat(bytes.length > 0, CoreMatchers.equalTo(Boolean.TRUE));

    }
    
    @Ignore
    public void testarProdutosInsumos_Producao() throws Exception {

        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ArrayList<Long> arrayList = new ArrayList<>();
        arrayList.add(1L);
        byte[] bytes = proutosInsumosService.informacoesProdutosInsumosProducao(arrayList);
        bos.write(bytes);
        bos.writeTo(new FileOutputStream(File.createTempFile("produtosInsumos_ProducaoTEST.pdf", ".pdf")));
        assertThat(bytes.length > 0, CoreMatchers.equalTo(Boolean.TRUE));

    }
    
    @Ignore
    public void testarProdutosInsumos_Servicos() throws Exception {

        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ArrayList<Long> arrayList = new ArrayList<>();
        arrayList.add(1L);
        byte[] bytes = proutosInsumosService.informacoesProdutosInsumosServicos(arrayList);
        bos.write(bytes);
        bos.writeTo(new FileOutputStream(File.createTempFile("produtosInsumos_ServicosTEST.pdf", ".pdf")));
        assertThat(bytes.length > 0, CoreMatchers.equalTo(Boolean.TRUE));

    }
    
    @Ignore
    public void testarUsoFonte() throws Exception {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        Long projeto_id = 1L;
        byte[] bytes = usosFontesReportTestService.relatorioUsosFontesPorProjeto(projeto_id);
        bos.write(bytes);
        bos.writeTo(new FileOutputStream(File.createTempFile("usoFonteTEST.pdf", ".pdf")));
        assertThat(bytes.length > 0, CoreMatchers.equalTo(Boolean.TRUE));
    }
    
    @Ignore
    public void testarProjeto() throws Exception {

        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        byte[] bytes = projetoReportTestService.informacoesProjetos(1L);
        bos.write(bytes);
        bos.writeTo(new FileOutputStream(File.createTempFile("projetosTEST.pdf", ".pdf")));
        assertThat(bytes.length > 0, CoreMatchers.equalTo(Boolean.TRUE));

    }
    
    @Ignore
    public void testarFinanceiro() throws Exception {

        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        byte[] bytes = financeiroReportTestService.relatorioFinanceiroPorProjeto(2L);
        bos.write(bytes);
        bos.writeTo(new FileOutputStream(File.createTempFile("financeiro.pdf", ".pdf")));
        assertThat(bytes.length > 0, CoreMatchers.equalTo(Boolean.TRUE));

    }
    
    @Ignore
    public void testarCronograma() throws Exception {

        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        byte[] bytes = cronogramaReportTestService.informacoesCronograma(1L);
        bos.write(bytes);
        bos.writeTo(new FileOutputStream(File.createTempFile("cronogramaTEST.pdf", ".pdf")));
        assertThat(bytes.length > 0, CoreMatchers.equalTo(Boolean.TRUE));

    }
    
    @Ignore
    public void testarInformacoesComplementares_OutrasInformacoes() throws Exception {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ArrayList<Long> arrayList = new ArrayList<>();
        arrayList.add(1L);
        byte[] bytes = informacoesComplementaresTestService.relatorioInformacoesCoplementaresOutrasInformacoes(arrayList);
        bos.write(bytes);
        bos.writeTo(new FileOutputStream(File.createTempFile("informacoesComplementaresTEST.pdf", ".pdf")));
        assertThat(bytes.length > 0, CoreMatchers.equalTo(Boolean.TRUE));

    }
}
