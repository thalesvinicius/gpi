package br.com.gpi.server.reports.test.services;

import br.com.gpi.server.domain.entity.InsumoProduto;
import br.com.gpi.server.domain.entity.Producao;
import br.com.gpi.server.domain.entity.Produto;
import br.com.gpi.server.domain.entity.Servico;
import br.com.gpi.server.domain.service.InsumoProdutoService;
import br.com.gpi.server.domain.service.ProducaoService;
import br.com.gpi.server.domain.service.ServicoService;
import br.com.gpi.server.domain.service.reports.BaseReportService;
import static br.com.gpi.server.util.reports.ReportUtil.*;
import java.io.InputStream;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.function.Supplier;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import net.sf.jasperreports.engine.JasperExportManager;


public class ProdutoInsumoReportTestService extends BaseReportService<Produto> {
    
    @Inject
    private InsumoProdutoService produtoService;
    
    @Inject
    private ProducaoService producaoService;

    @Inject
    private ServicoService servicoService;

    @Inject
    public ProdutoInsumoReportTestService(EntityManager em) {
        super(em);
    }
    
    public byte[] informacoesProdutosInsumos(List<Long> idsInsumoProduto) throws Exception{
        Map<String,Object> map = subReportsMapExpr.apply(defaultStyle());
        Collection<InsumoProduto> insumoproduto = produtoService.findAll();
        Supplier<Map<String,Object>> sup = () ->  {
                    Map<String,Object> params = paramsExpr.init(map);
                    params.put("REPORT_LOCALE", new Locale("pt", "BR"));
                    return  params;
                };
        return JasperExportManager.exportReportToPdf( generateReport( sup.get() , getClass().getResourceAsStream(RELATORIO_PRODUTOS_INSUMOS)
                , jrBeanCollection(insumoproduto)
        ));
    }
    
    public byte[] informacoesProdutosInsumosProducao(List<Long> idsInsumoProduto) throws Exception{
        Map<String,Object> map = subReportsMapExpr.apply(defaultStyle());
        List<Producao> listProducao = producaoService.findAllProducaoByInsumoProduto(idsInsumoProduto);
         Supplier<Map<String,Object>> sup = () ->  {
                    Map<String,Object> params = paramsExpr.init(map);
                    params.put("REPORT_LOCALE", new Locale("pt", "BR"));
                    return  params;
                } ;
        return JasperExportManager.exportReportToPdf( generateReport(sup.get(), getClass().getResourceAsStream(RELATORIO_PRODUTOS_INSUMOS_PRODUCAO)
                , jrBeanCollection(listProducao)
        ));
    }
    
    public byte[] informacoesProdutosInsumosServicos(List<Long> idsInsumoProduto) throws Exception{
        Map<String,Object> map = subReportsMapExpr.apply(defaultStyle());
        List<Servico> servicos = servicoService.findAllServicoByInsumoProduto(idsInsumoProduto);
        Supplier<Map<String,Object>> sup = () ->  {
                    Map<String,Object> params = paramsExpr.init(map);
                    params.put("REPORT_LOCALE", new Locale("pt", "BR"));
                    return  params;
        }; 
        return JasperExportManager.exportReportToPdf( generateReport(sup.get() , getClass().getResourceAsStream(RELATORIO_PRODUTOS_INSUMOS_SERVICO)
                , jrBeanCollection(servicos)
        ));
    }

}
