package br.com.gpi.server.reports.test.services;

import br.com.gpi.server.domain.entity.Produto;
import br.com.gpi.server.domain.entity.Projeto;
import br.com.gpi.server.domain.service.ProjetoService;
import br.com.gpi.server.domain.service.reports.BaseReportService;
import static br.com.gpi.server.util.reports.ReportUtil.*;
import java.io.InputStream;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.function.Supplier;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import net.sf.jasperreports.engine.JasperExportManager;


public class InformacoesComplementaresTestService extends BaseReportService<Produto> {
    
    @Inject
    private ProjetoService projetoService;

    @Inject
    public InformacoesComplementaresTestService(EntityManager em) {
        super(em);
    }
    
    public byte[] relatorioInformacoesCoplementaresOutrasInformacoes(List<Long> idsProjeto) throws Exception{
        Map<String,Object> map = subReportsMapExpr.apply(defaultStyle());
        Collection<Projeto> projetos = projetoService.findByIdToReport(idsProjeto);
        Supplier<Map<String,Object>> sup = () ->  {
                    Map<String,Object> params = paramsExpr.init(map);
                    params.put("REPORT_LOCALE", new Locale("pt", "BR"));
                    return  params;
                } ;
        return JasperExportManager.exportReportToPdf( generateReport( sup.get(), getClass().getResourceAsStream(RELATORIO_INFORMACOES_CONPLEMENTARES_OUTRAS_INFORMACOES)
                , jrBeanCollection(projetos)
        ));
    }
}
