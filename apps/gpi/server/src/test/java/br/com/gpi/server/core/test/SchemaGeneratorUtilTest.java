package br.com.gpi.server.core.test;

import org.junit.Test;

public class SchemaGeneratorUtilTest {

    @Test
    public void generate_schema() throws Exception {
        final String packageName = "br.com.gpi.server.domain.entity";
        SchemaGenerator gen = new SchemaGenerator(packageName);
        final String directory = "/home/paulovfm/";
        gen.generate(SchemaGenerator.Dialect.SQLSERVER, directory);
    }

}
