package br.com.gpi.server.domain.entity;

import br.com.gpi.server.core.exception.EnumException;

public enum Estagio {

    INVALID(0, "invalid"),  
    FORMALIZADA(1, "Decisão Formalizada"),
    IMPLANTACAO_INICIADO(2, "Implantação Iniciada"),
    INICIO_PROJETO(3, "Início de Projeto"),
    OPERACAO_INICIADA(4, "Operação Iniciada"),
    PROJETO_PROMISSOR(5, "Projeto Promissor"),
    COMPROMISSO_CUMPRIDO(6, "Compromisso Cumprido");

    private Estagio(Integer id, String descricao) {
        this.id = id;
        this.descricao = descricao;
    }

    private final String descricao;
    private final Integer id;

    public String getDescricao() {
        return descricao;
    }

    public Integer getId() {
        return id;
    }

    public String getSigla(Integer id) {
        switch (id) {
            case 1: 
                return "DF";
            case 2:
                return "II";
            case 3:
                return "IP";
            case 4:
                return "OI";
            case 5:
                return "PP";
            case 6:
                return "CC";
            default:
                return "";
        }
        
    }
    
    public static Estagio getEstagioById(final Integer idEstagio) {
        for (Estagio estagio : values()) {
            if (estagio.id.equals(idEstagio)) {
                return estagio;
            }
        }
        throw new EnumException(String.format("Não foi possível recuperar o estágio com o id %s", idEstagio));
    }

}
