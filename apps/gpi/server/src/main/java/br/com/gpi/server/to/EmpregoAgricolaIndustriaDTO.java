package br.com.gpi.server.to;

public class EmpregoAgricolaIndustriaDTO {
    
    private Integer ano;

    private Long empregoAgricolaId;
    
    private Long empregoIndustriaId;
    
    private Integer diretoAgricola;

    private Integer indiretoAgricola;
    
    private Integer diretoIndustria;

    private Integer indiretoIndustria;

    public EmpregoAgricolaIndustriaDTO() {
        
    }

    /**
     * @return the ano
     */
    public Integer getAno() {
        return ano;
    }

    /**
     * @param ano the ano to set
     */
    public void setAno(Integer ano) {
        this.ano = ano;
    }

    /**
     * @return the empregoAgricolaId
     */
    public Long getEmpregoAgricolaId() {
        return empregoAgricolaId;
    }

    /**
     * @param empregoAgricolaId the empregoAgricolaId to set
     */
    public void setEmpregoAgricolaId(Long empregoAgricolaId) {
        this.empregoAgricolaId = empregoAgricolaId;
    }

    /**
     * @return the empregoIndustriaId
     */
    public Long getEmpregoIndustriaId() {
        return empregoIndustriaId;
    }

    /**
     * @param empregoIndustriaId the empregoIndustriaId to set
     */
    public void setEmpregoIndustriaId(Long empregoIndustriaId) {
        this.empregoIndustriaId = empregoIndustriaId;
    }

    /**
     * @return the diretoAgricola
     */
    public Integer getDiretoAgricola() {
        return diretoAgricola;
    }

    /**
     * @param diretoAgricola the diretoAgricola to set
     */
    public void setDiretoAgricola(Integer diretoAgricola) {
        this.diretoAgricola = diretoAgricola;
    }

    /**
     * @return the indiretoAgricola
     */
    public Integer getIndiretoAgricola() {
        return indiretoAgricola;
    }

    /**
     * @param indiretoAgricola the indiretoAgricola to set
     */
    public void setIndiretoAgricola(Integer indiretoAgricola) {
        this.indiretoAgricola = indiretoAgricola;
    }

    /**
     * @return the diretoIndustria
     */
    public Integer getDiretoIndustria() {
        return diretoIndustria;
    }

    /**
     * @param diretoIndustria the diretoIndustria to set
     */
    public void setDiretoIndustria(Integer diretoIndustria) {
        this.diretoIndustria = diretoIndustria;
    }

    /**
     * @return the indiretoIndustria
     */
    public Integer getIndiretoIndustria() {
        return indiretoIndustria;
    }

    /**
     * @param indiretoIndustria the indiretoIndustria to set
     */
    public void setIndiretoIndustria(Integer indiretoIndustria) {
        this.indiretoIndustria = indiretoIndustria;
    }
}