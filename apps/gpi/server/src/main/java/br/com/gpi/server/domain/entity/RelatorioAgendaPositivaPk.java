
package br.com.gpi.server.domain.entity;

import java.io.Serializable;


public class RelatorioAgendaPositivaPk implements Serializable{
    private Long projetoId;    
    private String estagioPrevisto;
    private Boolean isLastVersion;
    
    @Override
    public boolean equals(Object obj) {
        return super.equals(obj); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int hashCode() {
        return super.hashCode(); //To change body of generated methods, choose Tools | Templates.
    }

    public Long getProjetoId() {
        return projetoId;
    }

    public void setProjetoId(Long projetoId) {
        this.projetoId = projetoId;
    }

    public String getEstagioPrevisto() {
        return estagioPrevisto;
    }

    public void setEstagioPrevisto(String estagioPrevisto) {
        this.estagioPrevisto = estagioPrevisto;
    }

    public Boolean getIsLastVersion() {
        return isLastVersion;
    }

    public void setIsLastVersion(Boolean isLastVersion) {
        this.isLastVersion = isLastVersion;
    }
   
}
