package br.com.gpi.server.domain.entity;

import br.com.gpi.server.core.jpa.converter.TipoInstrFormalizacaoEnumConverter;
import br.com.gpi.server.domain.common.AuditedEntity;
import br.com.gpi.server.to.EmpresaUnidadeDTO;
import br.com.gpi.server.to.ProjetoTO;
import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.Date;
import java.util.List;
import java.util.Set;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.PostLoad;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import org.hibernate.envers.Audited;

@Audited
@Entity
@NamedQueries({
    @NamedQuery(name = InstrumentoFormalizacao.Query.recoverSituacaoById, query = "SELECT i.situacaoEnum FROM InstrumentoFormalizacao i WHERE i.id = ?1"),
    @NamedQuery(name = InstrumentoFormalizacao.Query.findByIdForEdit, query = "SELECT i FROM InstrumentoFormalizacao i LEFT JOIN FETCH i.signatarios WHERE i.id = :id"),
    @NamedQuery(name = InstrumentoFormalizacao.Query.findByProcoloVersaoOriginal, query = "SELECT i FROM InstrumentoFormalizacao i WHERE i.protocolo = :protocolo and i.versao = 0"),
    @NamedQuery(name = InstrumentoFormalizacao.Query.countByProtocoloAndInstrumentoNotEquals, query = "SELECT COUNT(i) FROM InstrumentoFormalizacao i WHERE i.protocolo = ?1 and i.id <> ?2 and i.tipo <> 5"),
    @NamedQuery(name = InstrumentoFormalizacao.Query.recoverTipoById, query = "SELECT i.tipo FROM InstrumentoFormalizacao i WHERE i.id = ?1"),
    @NamedQuery(name = InstrumentoFormalizacao.Query.recoverCodigoProtocoloById, query = "SELECT i.protocolo FROM InstrumentoFormalizacao i WHERE i.id = ?1"),
    @NamedQuery(name = InstrumentoFormalizacao.Query.recoverMaiorVersaoByProtocolo, query = "SELECT max(i.versao) FROM InstrumentoFormalizacao i WHERE i.protocolo = ?1"),
    @NamedQuery(name = InstrumentoFormalizacao.Query.updateUsuarioeData, query = "update InstrumentoFormalizacao i set i.usuarioResponsavel = ?2, i.dataUltimaAlteracao = ?3 where i.id = ?1"),
    @NamedQuery(name = InstrumentoFormalizacao.Query.updateDataAssinatura, query = "update InstrumentoFormalizacao i set i.usuarioResponsavel = ?2, i.dataUltimaAlteracao = ?3, i.dataAssinatura = ?4 where i.id = ?1"),
    @NamedQuery(name = InstrumentoFormalizacao.Query.updateSituacaoInstrumento, query = "update InstrumentoFormalizacao i set i.usuarioResponsavel = ?2, i.dataUltimaAlteracao = ?3, i.situacaoEnum = ?4 where i.id = ?1"),
    @NamedQuery(name = InstrumentoFormalizacao.Query.updateLocalDocumento, query = "update InstrumentoFormalizacao i set i.localDocumento = (select l.descricao FROM Local l WHERE l.id = ?2) where i.id = ?1"),
    @NamedQuery(name = InstrumentoFormalizacao.Query.uncheckUltimaVersaoById, query = "update InstrumentoFormalizacao i set i.ultimaVersao = false where i.id = ?1"),
    @NamedQuery(name = InstrumentoFormalizacao.Query.countByProtocoloAndTipoAndSituacaoEnumAndIdNotEquals, query = "SELECT COUNT(i) FROM InstrumentoFormalizacao i WHERE i.protocolo = ?1 AND i.tipo = ?2 AND i.situacaoEnum = ?3 AND i.id <> ?4"),
    @NamedQuery(name = InstrumentoFormalizacao.Query.countByProtocoloAndTipoAndSituacaoEnum, query = "SELECT COUNT(i) FROM InstrumentoFormalizacao i WHERE i.protocolo = ?1 AND i.tipo = ?2 AND i.situacaoEnum = ?3"),
    @NamedQuery(name = InstrumentoFormalizacao.Query.findByResponsavelProjetoProtocoloAssinaturaPrevistaVencida, query = "Select i.titulo, i.id from Projeto p inner join p.instrumentos i where p.usuarioInvestimento.id = ?1 and i.situacaoEnum = 3 and i.dataAssinatura IS NULL and DATEDIFF(day, i.dataAssinaturaPrevista, getDate()) >= 0")
})
public class InstrumentoFormalizacao extends AuditedEntity<Long> {

    private String protocolo;

    @ManyToOne
    @JoinColumn(name = "historicoSituacaoInstrumentoFormalizacao_id")
    private HistoricoSituacaoInstrumentoFormalizacao situacaoAtual;

    @Column(name = "situacaoAtual_id")
    private Integer situacaoEnum;

    @Enumerated
    @Column(name = "tipo", insertable = false, updatable = false)
    private TipoInstrFormalizacao tipo;
    
    @JsonIgnore
    @Column(name = "tipo")
    private Integer tipoFaturamentoInt;
    
    private Integer versao;
    private String titulo;
    @Temporal(TemporalType.DATE)
    private Date dataAssinaturaPrevista;
    @Temporal(TemporalType.DATE)
    private Date dataAssinatura;
    private Boolean possuiContrapartidaFincGoverno;
    private String localDocumento;

    @Basic(optional = false)
    private Boolean ultimaVersao;

    @ManyToMany(fetch = FetchType.LAZY, cascade = {CascadeType.DETACH, CascadeType.REFRESH})
    @JoinTable(name = "PROJETOINSTRUMENTOFORMALIZACAO", joinColumns = {
        @JoinColumn(name = "instrumentoFormalizacao_id")}, inverseJoinColumns = {
        @JoinColumn(name = "projeto_id")})
    private List<Projeto> projetos;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "instrFormalizacao", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Signatario> signatarios;

    // EmpresasSelecionadas e projetosSelacionados são populados pelo método
    // InstrumentoFormalizacaoService.populateSelectedLists
    @Transient
    private Set<EmpresaUnidadeDTO> empresasSelecionadas;
    @Transient
    private Set<ProjetoTO> projetosSelecionados;

    //usado para armazenar a justificativa que vem da tela de projeto
    private transient String justificativa;

    @OneToMany(mappedBy = "instrumentoFormalizacao", fetch = FetchType.LAZY, cascade = {CascadeType.REMOVE})
    private List<HistoricoSituacaoInstrumentoFormalizacao> situacoes;

    public InstrumentoFormalizacao() {
    }

    public InstrumentoFormalizacao(String protocolo, HistoricoSituacaoInstrumentoFormalizacao situacao, TipoInstrFormalizacao tipo, Integer versao, String titulo, Date dataAssinaturaPrevista, Date dataAssinatura, Boolean possuiContrapartidaFincGoverno, List<Projeto> projetos, List<Signatario> signatarios) {
        this.protocolo = protocolo;
        this.situacaoAtual = situacao;
        this.tipo = tipo;
        this.versao = versao;
        this.titulo = titulo;
        this.dataAssinaturaPrevista = dataAssinaturaPrevista;
        this.dataAssinatura = dataAssinatura;
        this.possuiContrapartidaFincGoverno = possuiContrapartidaFincGoverno;
        this.projetos = projetos;
        this.signatarios = signatarios;
    }

    public String getProtocolo() {
        return protocolo;
    }

    public void setProtocolo(String protocolo) {
        this.protocolo = protocolo;
    }

    public HistoricoSituacaoInstrumentoFormalizacao getSituacaoAtual() {
        return situacaoAtual;
    }

    public void setSituacaoAtual(HistoricoSituacaoInstrumentoFormalizacao situacaoAtual) {
        this.situacaoAtual = situacaoAtual;
    }

    public TipoInstrFormalizacao getTipo() {
        return tipo;
    }

    public void setTipo(TipoInstrFormalizacao tipo) {
        this.tipo = tipo;
        this.tipoFaturamentoInt = (new TipoInstrFormalizacaoEnumConverter()).convertToDatabaseColumn(tipo);
    }

    public Integer getTipoFaturamentoInt() {
        return tipoFaturamentoInt;
    }

    public void setTipoFaturamentoInt(Integer tipoFaturamentoInt) {
        this.tipoFaturamentoInt = tipoFaturamentoInt;
        this.tipo = (new TipoInstrFormalizacaoEnumConverter()).convertToEntityAttribute(tipoFaturamentoInt);
    }
    
    

    public Integer getVersao() {
        return versao;
    }

    public void setVersao(Integer versao) {
        this.versao = versao;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public Date getDataAssinaturaPrevista() {
        return dataAssinaturaPrevista;
    }

    public void setDataAssinaturaPrevista(Date dataAssinaturaPrevista) {
        this.dataAssinaturaPrevista = new Date(dataAssinaturaPrevista.getTime());
    }

    public Date getDataAssinatura() {
        if (dataAssinatura != null) {
            return dataAssinatura;
        } else {
            return null;
        }
    }

    public void setDataAssinatura(Date dataAssinatura) {
        if (dataAssinatura != null) {
            this.dataAssinatura = new Date(dataAssinatura.getTime());
        } else {
            this.dataAssinatura = null;
        }
    }

    public Boolean isPossuiContrapartidaFincGoverno() {
        return possuiContrapartidaFincGoverno;
    }

    public void setPossuiContrapartidaFincGoverno(Boolean possuiContrapartidaFincGoverno) {
        this.possuiContrapartidaFincGoverno = possuiContrapartidaFincGoverno;
    }

    public List<Projeto> getProjetos() {
        return projetos;
    }

    public void setProjetos(List<Projeto> projetos) {
        this.projetos = projetos;
    }

    public List<Signatario> getSignatarios() {
        return signatarios;
    }

    public void setSignatarios(List<Signatario> signatarios) {
        this.signatarios = signatarios;
    }

    public Set<EmpresaUnidadeDTO> getEmpresasSelecionadas() {
        return empresasSelecionadas;
    }

    public void setEmpresasSelecionadas(Set<EmpresaUnidadeDTO> empresasSelecionadas) {
        this.empresasSelecionadas = empresasSelecionadas;
    }

    public Set<ProjetoTO> getProjetosSelecionados() {
        return projetosSelecionados;
    }

    public void setProjetosSelecionados(Set<ProjetoTO> projetosSelecionados) {
        this.projetosSelecionados = projetosSelecionados;
    }

    public String getLocalDocumento() {
        return localDocumento;
    }

    public void setLocalDocumento(String localDocumento) {
        this.localDocumento = localDocumento;
    }

    public Boolean isUltimaVersao() {
        return ultimaVersao;
    }

    public void setUltimaVersao(Boolean ultimaVersao) {
        this.ultimaVersao = ultimaVersao;
    }

    public List<HistoricoSituacaoInstrumentoFormalizacao> getSituacoes() {
        return situacoes;
    }

    public void setSituacoes(List<HistoricoSituacaoInstrumentoFormalizacao> situacoes) {
        this.situacoes = situacoes;
    }

    public Integer getSituacaoEnum() {
        return situacaoEnum;
    }

    public void setSituacaoEnum(Integer situacaoEnum) {
        this.situacaoEnum = situacaoEnum;
    }

    public String getJustificativa() {
        return justificativa;
    }

    public void setJustificativa(String justificativa) {
        this.justificativa = justificativa;
    }

    @PostLoad
    private void postLoad() {
        if (situacaoAtual != null) {
            justificativa = situacaoAtual.getJustificativa();
        }
    }

    public static class Query {

        public static final String recoverMaiorVersaoByProtocolo = "InstrumentoFormalizacao.recoverMaiorVersaoByProtocolo";
        public static final String findByIdForEdit = "InstrumentoFormalizacao.findByIdForEdit";
        public static final String recoverSituacaoById = "InstrumentoFormalizacao.recoverSituacaoById";
        public static final String recoverTipoById = "InstrumentoFormalizacao.recoverTipoById";
        public static final String findByProcoloVersaoOriginal = "InstrumentoFormalizacao.findByProcoloVersaoOriginal";
        public static final String recoverCodigoProtocoloById = "InstrumentoFormalizacao.recoverCodigoProtocoloById";
        public static final String updateUsuarioeData = "InstrumentoFormalizacao.updateUsuarioeData";
        public static final String updateDataAssinatura = "InstrumentoFormalizacao.updateDataAssinatura";
        public static final String updateSituacaoInstrumento = "InstrumentoFormalizacao.updateSituacaoInstrumento";
        public static final String updateLocalDocumento = "InstrumentoFormalizacao.updateLocalDocumento";
        public static final String uncheckUltimaVersaoById = "InstrumentoFormalizacao.uncheckUltimaVersaoById";
        public static final String countByProtocoloAndInstrumentoNotEquals = "InstrumentoFormalizacao.countByProtocoloAndInstrumentoNotEquals";
        public static final String countByProtocoloAndTipoAndSituacaoEnumAndIdNotEquals = "InstrumentoFormalizacao.countByProtocoloAndTipoAndSituacaoEnumAndIdNotEquals";
        public static final String countByProtocoloAndTipoAndSituacaoEnum = "InstrumentoFormalizacao.countByProtocoloAndTipoAndSituacaoEnum";
        public static final String findByResponsavelProjetoProtocoloAssinaturaPrevistaVencida = "findByResponsavelProjetoProtocoloAssinaturaPrevistaVencida";
    }
}
