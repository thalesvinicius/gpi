package br.com.gpi.server.endpoint;

import br.com.gpi.server.domain.common.QueryParameter;
import br.com.gpi.server.domain.entity.CadeiaProdutiva;
import br.com.gpi.server.domain.entity.Departamento;
import br.com.gpi.server.domain.entity.Estagio;
import br.com.gpi.server.domain.entity.RegiaoPlanejamento;
import br.com.gpi.server.domain.entity.SituacaoProjetoEnum;
import br.com.gpi.server.domain.entity.User;
import br.com.gpi.server.domain.entity.enumerated.EnumTipoMeioAmbiente;
import br.com.gpi.server.domain.service.DomainTableService;
import br.com.gpi.server.domain.service.RegiaoPlanejamentoService;
import br.com.gpi.server.domain.service.RelatorioAcompanhamentoAmbientalService;
import br.com.gpi.server.domain.service.UserService;
import br.com.gpi.server.domain.view.RelatorioAcompanhamentoAmbiental;
import br.com.gpi.server.util.reports.ReportUtil;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.Supplier;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("relatorio/projeto/ambiental")
@Stateless
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
@TransactionManagement(TransactionManagementType.BEAN)
public class RelatorioAcompanhamentoAmbientalEndpoint {

    @Inject
    private RelatorioAcompanhamentoAmbientalService ambientalService;    
    
    @Inject
    private RegiaoPlanejamentoService regiaoPlanejamentoService;
    
     @Inject 
    private DomainTableService domainTable;
     
     @Inject
    private UserService user;
    
    @GET
    @Path("findByFilters")
    public Response findByFilters(@QueryParam("estagios") List<Estagio> estagios,
        @QueryParam("status") List<SituacaoProjetoEnum> status,
        @QueryParam("regioesPlanejamento") List<Integer> regioesPlanejamento,
        @QueryParam("gerencias") List<Long> gerencias,
        @QueryParam("analistas") List<Integer> analistas,
        @QueryParam("cadeiasProdutivas") List<Integer> cadeiasProdutivas,
        @QueryParam("statusProcesso") List<Integer> statusProcesso,
        @QueryParam("etapa") List<EnumTipoMeioAmbiente> etapas ) {
        
        QueryParameter filtros = QueryParameter.with(RelatorioAcompanhamentoAmbiental.Filtro.ESTAGIO, estagios)
                .and(RelatorioAcompanhamentoAmbiental.Filtro.ANALISTA,analistas)
                .and(RelatorioAcompanhamentoAmbiental.Filtro.CADEIA_PRODUTIVA,cadeiasProdutivas)
                .and(RelatorioAcompanhamentoAmbiental.Filtro.GERENCIA,gerencias)                
                .and(RelatorioAcompanhamentoAmbiental.Filtro.ESTAGIO,estagios)
                .and(RelatorioAcompanhamentoAmbiental.Filtro.STATUS_ATUAL,status)
                .and(RelatorioAcompanhamentoAmbiental.Filtro.REGIAO,regioesPlanejamento)
                .and(RelatorioAcompanhamentoAmbiental.Filtro.STATUS_PROCESSO,statusProcesso)
                .and(RelatorioAcompanhamentoAmbiental.Filtro.ETAPA,etapas);
        
        List<RelatorioAcompanhamentoAmbiental> result = ambientalService.findRelatorioAcompanhamentoAmbientalByFilters(filtros);
        return Response.ok(result).build();
    }
    
    @GET
    @Path("acompanhamento/{format}")
    @Produces({"application/pdf", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"})
    public Response buildAcompanhamentoAmbientalProjeto(@PathParam("format") String format, 
            @QueryParam("estagios") List<Estagio> estagios,
            @QueryParam("status") List<SituacaoProjetoEnum> status,
            @QueryParam("regioesPlanejamento") List<Long> regioesPlanejamento,
            @QueryParam("gerencias") List<Long> gerencias,
            @QueryParam("analistas") List<Long> analistas,
            @QueryParam("cadeiasProdutivas") List<Long> cadeiasProdutivas,
            @QueryParam("statusProcesso") List<Integer> statusProcesso,
            @QueryParam("etapa") List<EnumTipoMeioAmbiente> etapas,
            @QueryParam("todosEstagios") Set<Estagio> todosEstagios,
            @HeaderParam("todasCadeiasProdutivas") String todasCadeiasProdutivas,
            @HeaderParam("todasGerencias") String todasGerencias,
            @HeaderParam("todasRegioesPlanejamento") String todasRegioesPlanejamento,
            @HeaderParam("todosAnalistas") String todosAnalistas) throws FileNotFoundException, IOException {
        
            
        Supplier<Map<String,Object>> parametros = () -> {
            ReportUtil ru = new ReportUtil(){};
            Boolean buscarTodasAsGerencias = ru.buscarTodos(todasGerencias, gerencias);
            Boolean buscarTodasCadeiasProdutivas = ru.buscarTodos(todasCadeiasProdutivas, cadeiasProdutivas);
            Boolean buscarTodasRegioes = ru.buscarTodos(todasRegioesPlanejamento, regioesPlanejamento);
            Boolean buscarTodosAnalistas = ru.buscarTodos(todosAnalistas,analistas);
            Supplier<Boolean> buscarTodosEstagios = ()-> { Set<Estagio> estagiosSet = new HashSet<>(Optional.ofNullable(estagios).orElse(Collections.emptyList()));
                return estagiosSet.containsAll(todosEstagios);    
            };
            final Map<String,Object> params = new HashMap<>();
            params.put("TITULO", "Relatório de Acompanhamento Ambiental dos Projetos");
            params.put("ESTAGIOS",ReportUtil.determinarParametros.apply( buscarTodosEstagios.get() ? null: estagios ,
                    (e) -> ((Estagio) e).getDescricao(),true));
            //params.put("STATUS",ReportUtil.determinarParametros.apply(situacoesProjeto,(sit) -> ((SituacaoProjetoEnum) sit).getDescription(), true));
            params.put("ANALISTA",ru.stringCollection(User::getNome,buscarTodosAnalistas? null:
                    user.findByListIDs(new ArrayList<>(analistas)),true));
            params.put("REGIAO_PLANEJAMENTO",ru.stringCollection(RegiaoPlanejamento::getNome, buscarTodasRegioes ? null : 
                    regiaoPlanejamentoService.findByListIDs(new ArrayList<>(regioesPlanejamento)) , false));
                
            params.put("CADEIA_PRODUTIVA",ru.stringCollection(CadeiaProdutiva::getDescricao,buscarTodasCadeiasProdutivas ? null:
                    domainTable.findByIds(CadeiaProdutiva.class,cadeiasProdutivas ),false));
            params.put("GERENCIA",ru.stringCollection(Departamento::getDescricao,buscarTodasAsGerencias ? null:
                    domainTable.findByIds(Departamento.class, gerencias),false ));
            
            
            return params;
            
        };      
        
        return ambientalService.generateReport(parametros,format, estagios, analistas, cadeiasProdutivas, gerencias, estagios, status, regioesPlanejamento, statusProcesso, etapas);
    }
}