package br.com.gpi.server.domain.service;

import br.com.gpi.server.domain.common.BaseService;
import br.com.gpi.server.domain.entity.AtividadeInstrumento;
import br.com.gpi.server.domain.entity.AtividadeTemplate;
import br.com.gpi.server.domain.entity.Estagio;
import br.com.gpi.server.domain.entity.EstagioProjeto;
import br.com.gpi.server.domain.entity.InstrumentoFormalizacao;
import br.com.gpi.server.domain.entity.SituacaoProjetoEnum;
import br.com.gpi.server.domain.entity.Template;
import br.com.gpi.server.domain.entity.TipoInstrFormalizacao;
import br.com.gpi.server.domain.entity.User;
import br.com.gpi.server.domain.entity.enumerated.EnumTipoTemplate;
import br.com.gpi.server.domain.repository.AtividadeInstrumentoRepository;
import br.com.gpi.server.domain.repository.AtividadeLocalRepository;
import br.com.gpi.server.domain.repository.EstagioProjetoRepository;
import br.com.gpi.server.domain.repository.InstrumentoFormalizacaoRepository;
import br.com.gpi.server.domain.repository.ProjetoRepository;
import br.com.gpi.server.domain.repository.TemplateRepository;
import br.com.gpi.server.to.LocalTO;
import br.com.gpi.server.to.ProjetoTO;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.NotSupportedException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

public class AtividadeInstrumentoService extends BaseService<AtividadeInstrumento> {

    @Inject
    private AtividadeInstrumentoRepository atividadeInstrumentoRepository;

    @Inject
    private AtividadeLocalRepository atividadeLocalRepository;

    @Inject
    private InstrumentoFormalizacaoRepository instrumentoFormalizacaoRepository;

    @Inject
    private InstrumentoFormalizacaoService instrumentoFormalizacaoService;

    @Inject
    private ProjetoService projetoService;

    @Inject
    private ProjetoRepository projetoRepository;

    @Inject
    private TemplateRepository templateRepository;

    @Inject
    private EstagioProjetoRepository estagioProjetoRepository;

    @Inject
    private UserTransaction userTransaction;

    @Inject
    public AtividadeInstrumentoService(EntityManager em) {
        super(AtividadeInstrumento.class, em);
    }

    public List<AtividadeInstrumento> copyTemplateAtividades(Long instrumentoId, Long loggedUserId) throws SystemException {
        try {
            userTransaction.begin();
            this.getEm().joinTransaction();
            InstrumentoFormalizacao instrumento = instrumentoFormalizacaoRepository.findBy(instrumentoId);
//        instrumento.setId(instrumentoId);
            User loggedUser = getEm().find(User.class, loggedUserId);

            List<Template> templateAtivo = templateRepository.findAllByStatusAndTipo(Boolean.TRUE, instrumento.getTipo() == TipoInstrFormalizacao.TERMO_ADITIVO ? EnumTipoTemplate.TERMO_ADITIVO : EnumTipoTemplate.PROTOCOLO_INTENCOES);
            if (!templateAtivo.isEmpty()) {
                Template templateAtividadesInstrumento = templateAtivo.get(0);

                if (templateAtividadesInstrumento.getAtividades() != null && !templateAtividadesInstrumento.getAtividades().isEmpty()) {

                    for (AtividadeTemplate atividadeTemplate : templateAtividadesInstrumento.getAtividades()) {
                        AtividadeInstrumento atividadeInstrumento = new AtividadeInstrumento();

                        /* setando data de inicio da primeira atividade pegando o 
                         primero projeto com status PP no historico*/
                        if (atividadeTemplate.getPosicao() == 1) {
                            List<ProjetoTO> projetos = projetoService.findProjetoTOByInstrumentoFormalizacaoId(instrumentoId);
                            List<ProjetoTO> projetosPP = projetos.stream().filter(u -> u.getEstagios().stream().anyMatch(x -> x.getEstagioId() == Estagio.PROJETO_PROMISSOR.getId())).collect(Collectors.toList());
                            Optional<ProjetoTO> firstProjetoPP = projetosPP.stream().min((ProjetoTO o1, ProjetoTO o2) -> o1.getId().intValue() - o2.getId().intValue());

                            if (firstProjetoPP.isPresent()) {

                                List<EstagioProjeto> estagiosPP = firstProjetoPP.get().getEstagios().stream().filter(u -> u.getEstagioId() == Estagio.PROJETO_PROMISSOR.getId()).collect(Collectors.toList());
                                if (!estagiosPP.isEmpty()) {
                                    atividadeInstrumento.setDataInicio(estagiosPP.get(0).getDataInicioEstagio());
                                    //Atualizando o atributo localDocumento do Instrumento de Formalizacao
                                    this.createUpdateDeleteQuery(InstrumentoFormalizacao.Query.updateLocalDocumento, instrumento.getId(), atividadeTemplate.getAtividadeLocal().getLocal().getId()).executeUpdate();
                                }
                            } else {
                                // throw new RuntimeException("Não existe projeto no estágio PP para atribuir a data de inicio da primeira atividade!");
                            }
                        }

                        atividadeInstrumento.setAtividadeLocal(atividadeTemplate.getAtividadeLocal());
                        atividadeInstrumento.setBloqueiaICE(atividadeTemplate.getBloqueiaICE());
                        atividadeInstrumento.setCc(atividadeTemplate.getCc());
                        atividadeInstrumento.setDf(atividadeTemplate.getDf());
                        atividadeInstrumento.setIi(atividadeTemplate.getIi());
                        atividadeInstrumento.setObrigatorio(atividadeTemplate.getObrigatorio());
                        atividadeInstrumento.setOi(atividadeTemplate.getOi());
                        atividadeInstrumento.setPosicao(atividadeTemplate.getPosicao());
                        atividadeInstrumento.setUsuarioResponsavel(loggedUser);
                        atividadeInstrumento.setInstrumentoFormalizacao(instrumento);

                        atividadeInstrumentoRepository.save(atividadeInstrumento);
                    }
                    instrumentoFormalizacaoRepository.updateUsuarioeData(instrumentoId, loggedUser, new Date());
                }
            }
            List<AtividadeInstrumento> savedAtividades = atividadeInstrumentoRepository.findAllByInstFormId(instrumentoId);
            for (AtividadeInstrumento atividade : savedAtividades) {
                atividade.setLocalAtividade(new LocalTO(atividade.getAtividadeLocal().getLocal().getId(), atividade.getAtividadeLocal().getLocal().getDescricao()));
                atividade.getLocalAtividade().setAtividades(atividadeLocalRepository.findAllByLocalId(atividade.getAtividadeLocal().getLocal().getId()));
            };
            userTransaction.commit();
            return savedAtividades;
        } catch (Exception ex) {
            userTransaction.rollback();
            Logger.getLogger(ProjetoService.class.getName()).log(Level.SEVERE, null, ex);
            throw new RuntimeException(ex);
        }
    }

    public Boolean save(Long idUsuarioLogado, AtividadeInstrumento atividade, Long instFormId) throws SystemException, NotSupportedException {
        try {
            userTransaction.begin();
            this.getEm().joinTransaction();
            InstrumentoFormalizacao instrumento = new InstrumentoFormalizacao();
            instrumento.setId(instFormId);
            User responsavel = this.getEm().find(User.class, idUsuarioLogado);
            atividade.setUsuarioResponsavel(responsavel);
            atividade.setInstrumentoFormalizacao(instrumento);
            Date now = new Date();
            Boolean projetoVersionado = Boolean.FALSE;
            atividade.setDataUltimaAlteracao(now);

            this.merge(atividade);

            this.createUpdateDeleteQuery(InstrumentoFormalizacao.Query.updateUsuarioeData, instFormId, responsavel, now).executeUpdate();

            if (atividade.getDataConclusao() != null) {
                if (Boolean.TRUE.equals(atividade.getDf())) {
                    List<ProjetoTO> projetos = projetoService.findProjetoTOByInstrumentoFormalizacaoId(instFormId);
                    Set<Long> projetosAVersionar = new HashSet<>();

                    projetos.forEach(projeto -> {
                        updateSituacaoAndEstagioDF(projeto, instFormId, idUsuarioLogado, atividade.getDataConclusao());
                        projetosAVersionar.add(projeto.getVersaoPai());
                    });

                    if (!projetosAVersionar.isEmpty()) {
                        projetoService.versionProject(projetosAVersionar);
                        projetoVersionado = Boolean.TRUE;
                    }
                }
            } else if (atividade.getDataInicio() != null && (atividade.getParalela() == null || !atividade.getParalela())) {
                this.createUpdateDeleteQuery(InstrumentoFormalizacao.Query.updateLocalDocumento, instFormId, atividade.getLocalAtividade().getId()).executeUpdate();
            }

            userTransaction.commit();
            return projetoVersionado;
        } catch (Exception ex) {
            userTransaction.rollback();
            Logger.getLogger(ProjetoService.class.getName()).log(Level.SEVERE, null, ex);
            throw new RuntimeException(ex);
        }
    }

    private void updateSituacaoAndEstagioDF(ProjetoTO projeto, Long instFormId, Long idUsuarioLogado, Date dataConclusaoAtividade) {

        projetoService.updateSituacaoProjetoTransactional(SituacaoProjetoEnum.ATIVO, projeto.getId(), idUsuarioLogado, new Date());
        if (projeto.getEstagio().equals(Estagio.PROJETO_PROMISSOR)) {
            projetoService.updateEstagioProjetoTransactional(Estagio.FORMALIZADA, projeto.getId(), idUsuarioLogado, dataConclusaoAtividade);
        }

        User usuario = this.getEm().find(User.class, idUsuarioLogado);
        //atualizando data e a situação da assinatura do instrumento de formalizacao
        instrumentoFormalizacaoService.updateDataAssinaturaTransactional(instFormId, usuario, new Date(), dataConclusaoAtividade);
    }
}
