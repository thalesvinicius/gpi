/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.gpi.server.util.reports;

import br.com.gpi.server.domain.entity.TipoEmprego;
import br.com.gpi.server.domain.service.UserService;
import br.com.gpi.server.domain.user.model.SecurityUser;
import br.com.gpi.server.util.functional.FunctionalInterfaces;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.Renderable;
import net.sf.jasperreports.engine.export.JRXlsExporterParameter;
import net.sf.jasperreports.engine.export.ooxml.JRXlsxExporter;

/**
 *
 * @author rafaelbfs
 */
public interface ReportUtil {
    
    
    String RELATORIO_DADOS_EMPRESA = "/jasper/DadosEmpresa.jasper";
    String RELATORIO_CNAE_ICE = "/jasper/CNAEICE.jasper";
    String RELATORIO_EMPREGO_ICE = "/jasper/EmpregoICE.jasper";
    String RELATORIO_EMPREGO_SUCRO_ENERG = "/jasper/EmpregoSucroEnergICE.jasper";
    String RELATORIO_PRODUTOS_INSUMOS = "/jasper/ProdutosInsumos.jasper";
    String RELATORIO_PRODUTOS_INSUMOS_PRODUCAO = "/jasper/ProdutosInsumos_Producao.jasper";
    String RELATORIO_PRODUTOS_INSUMOS_SERVICO = "/jasper/ProdutosInsumos_Servicos.jasper";
    String RELATORIO_USO_FONTES = "/jasper/FinanceiroUsoFontes.jasper";
    String RELATORIO_PROJETOS = "/jasper/Projeto.jasper";
    String CHAVE_RELATORIO_CRONOGRAMA = "cronograma";
    String RELATORIO_CRONOGRAMA = "/jasper/Cronograma.jasper";
    String DEFAULT_STYLES_LOCATION = "/reports/GPIJasperStyle.jrtx";
    String CHAVE_CONTATOS_EMPRESA_ICE = "contatosEmpresaICE";
    String RELATORIO_CONTAOS_EMPRESA_ICE = "/jasper/ContatosEmpresaICE.jasper";
    String CHAVE_RELATORIO_SUCRO_ENERG = "empregosSucroEnergetico";
    String CHAVE_RELATORIO_PRODUTOS_ICE = "produtosICE";
    String RELATORIO_PRODUTOS_ICE = "/jasper/ProdutosInsumos.jasper";
    String USO_FONTE_DATA_SET_KEY = "USO_FONTE_DS";
    String CHAVE_RELATORIO_FINANCEIRO = "financeiro";
    String RELATORIO_FINANCEIRO = "/jasper/DadosFinanceirosICE.jasper";
    String RELATORIO_ACOMPANHAMENTO_ANUAL = "/jasper/RelatorioAcompanhamentoProtocoloIntencoes.jasper";
    String LOGO_INDI_COLORIDO = "/reports/indi-color.png";
    String LOGO_INDI_NOME = "/reports/logo-indi2.png";
    String CHAVE_RELATORIO_PROJETO_ICE = "projetoItem2";
    String RELATORIO_PROJETO_ICE = "/jasper/Projeto.jasper";
    String RELATORIO_INFORMACOES_CONPLEMENTARES_OUTRAS_INFORMACOES = "/jasper/InformacoesComplementares_OutrasInformacoes.jasper";
    String INFORMACOES_COMPLEMENTARES7_1_DS_KEY = "INFORMACOES_COMPLEMENTARES_7_1";
    String RELATORIO_INFO_COMPLEMENTARES7_1 = "/jasper/InformacoesComplementares7_1.jasper";
    String CHAVE_RELATORIO_INFO_COMPLEMENTARES7_1 = "informacoesComplementares7_1";
    String CHAVE_INFORMACOES_CONPLEMENTARES_OUTRAS_INFORMACOES = "informacoes7.2";
    String RELATORIO_NOTA_TECNICA_PROJETOS = "/jasper/NotaTecnicaProjeto.jasper";
    String CHAVE_RESPONSAVEL_DATA_DS = "RESPONSAVEL_DATA_DS";
    String CHAVE_SUB_RELATORIO_LOCALIZACAO = "projetoLocalizacao";
    String SUBRELATORIO_LOCALIZACAO = "/jasper/Localizacao.jasper";
    String RELATORIO_GERENCIAL_03_AGENDA_POSITIVA = "/jasper/AgendaPositiva.jasper";
    String ESTAGIO_PREVISTO = "ESTAGIO_PREVISTO";
    String PERIODO = "PERIODO";
    String PERIODO_INICIAL = "PERIODO_INICIAL";
    String PERIODO_FINAL = "PERIODO_FINAL";
    String GERENCIA = "GERENCIA";
    String ANALISTA = "ANALISTA";
    String CADEIA_PRODUTIVA = "CADEIA_PRODUTIVA";
    String IDS_CADEIA_PRODUTIVA = "IDS_CADEIA_PRODUTIVA";
    String REGIAO_PLANEJAMENTO = "REGIAO_PLANEJAMENTO";
    String CABECALHO_REL_CARTEIRA_PROJETO = "/jasper/AgendaPositivaCabecalho.jasper";
    String CABECALHO_REL_ACOMPANHAMENTO_IF = "/jasper/AcompanhamentoIFCabecalho.jasper";
    String CABECALHO_REL_GERENCIAIS_XLS = "/jasper/RodapeRelatGerenciais.jasper";
    String CABECALHO_REL_AGENDA_POSITIVA = "/jasper/CabecalhoAgendaPositiva.jasper";
    String TOTAL_INVESTIMENTO = "TOTAL_INVESTIMENTO";
    String TOTAL_FATURAMENTO_INICIAL = "TOTAL_FATURAMENTO_INICIAL";
    String TOTAL_FATURAMENTO_FINAL = "TOTAL_FATURAMENTO_FINAL";
    String TOTAL_QUANTIDADE_PROJETOS = "TOTAL_QUANTIDADE_PROJETOS";
    String TOTAL_EMPREGOS_DIRETOS = "TOTAL_EMPREGOS_DIRETOS";
    String TOTAL_EMPREGOS_INDIRETOS = "TOTAL_EMPREGOS_INDIRETOS";
    String INFO_RESPONSAVEL = "INFO_RESPONSAVEL";
    String TOTAIS_RELATORIO_CART_PROJETOS = "/jasper/rodapeGER4CartProjetos.jasper";
    String PARAM_DT_DECISAO_FORMALIZADA = "dataDecisaoFormalizada";
    String PARAM_NUMERO_PROTOCOLO_INTENCOES = "numeroProtocoloIntencoes";
    
    EnumSet<TipoEmprego> EMPREGOS_PERMANENTES = EnumSet.of(TipoEmprego.PERMANENTE, 
            TipoEmprego.PERMANENTE_MATURACAO_AGRICOLA, TipoEmprego.PERMANENTE_MATURACAO_INDUSTRIAL,
            TipoEmprego.PERMANENTE_OPERACAO);
    SimpleDateFormat DATEFORMAT_MMMM_YY = new SimpleDateFormat("MMMM/yy",new Locale("pt","BR"));
    BigDecimal MIL = new BigDecimal("1000");
    Supplier<NumberFormat> numberPtBr = () -> {
        NumberFormat nf = NumberFormat.getInstance(new Locale("pt","BR")); 
        return nf;
    };
    
    
    public BiFunction<Date,Date,String> periodoString = (ini,fim) -> {
        if(ini == null || fim == null ){
            return "Todos";
        }else{
            return DATEFORMAT_MMMM_YY.format(ini) + " a " + DATEFORMAT_MMMM_YY.format(fim);
        }
    };
    
    public static final Function<InputStream,Map<String,Object>> subReportsMapExpr =  (styles) -> {
                Map<String, Object> map = new HashMap();
                map.put("STYLE_GERAL",styles); 
                return map;        
            }; 
    public static final BiFunction<InputStream,Renderable,Map<String,Object>> subReportsMapLogoExpr =  
            (InputStream styles,Renderable logo) -> {
                Map<String, Object> map = new HashMap();
                map.put("STYLE_GERAL",styles); 
                map.put("INDI-Logo", logo);
                return map;        
            }; 
    public static final ReportParamsInitializer paramsExpr = (subReportsMapExpr1) -> {
        Map<String,Object> params = new HashMap<String,Object>();
        params.put("SUBREPORT_MAP", subReportsMapExpr1); 
        params.put("REPORT_LOCALE",new Locale("pt","BR" ));
        return  params;
    };
    
    public static final Function<String,Set<Long>> parseIntoLongSet = (param) -> {
                if(param == null || param.isEmpty()){
                    return Collections.EMPTY_SET;
                }else{
                    return Arrays.asList(param.split(",")).stream().map(String::trim).map((str)-> Long.valueOf(str)).collect(Collectors.toSet());
                }
        };
    public static final Function<List<Long>,Function<Set<Long>,Boolean>> buscarTodos = (filtros) -> {
                Set<Long> filtroSet = filtros != null ? new HashSet<>(filtros) : Collections.emptySet();
                return (todos) -> filtroSet.containsAll(todos);
     };
    
    public static final FunctionalInterfaces.MultiPrintExporter xlsxExporter = new FunctionalInterfaces.MultiPrintExporter() {
        @Override
        public ByteArrayOutputStream export(List<JasperPrint> prints) throws JRException {
            
            JRXlsxExporter exporter = new JRXlsxExporter();
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, baos);
            exporter.setParameter(JRExporterParameter.IGNORE_PAGE_MARGINS, true);
            exporter.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_COLUMNS,Boolean.FALSE );
            exporter.setParameter(JRXlsExporterParameter.IS_FONT_SIZE_FIX_ENABLED,Boolean.FALSE);
            exporter.setParameter(JRXlsExporterParameter.IS_COLLAPSE_ROW_SPAN,Boolean.TRUE);
            exporter.setParameter(JRXlsExporterParameter.IS_ONE_PAGE_PER_SHEET,Boolean.FALSE);        
            exporter.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE,Boolean.TRUE);
            exporter.setParameter(JRXlsExporterParameter.JASPER_PRINT_LIST,prints );
            //exporter.setParameter(JRXlsExporterParameter., baos);
            exporter.exportReport();
            return baos;
        }
        
    };
    
    public static final FunctionalInterfaces.TriFunction<List<?>,Function<Object,String>,Boolean,String> determinarParametros =  (list,map,isMale)->{
        if(list == null || list.isEmpty()){
            return isMale ? "Todos" : "Todas";
        }else{
            return list.stream().map(map).collect(Collectors.joining(","));
        }
    };
    
    
    
    
    default public <T> String stringCollection(Function<T, String> strProperty, Collection<T> col,Boolean isMale) {
        String todos = isMale ? "Todos":"Todas";
        return Optional.ofNullable(col).map((c) -> {
            if (c.isEmpty()) {
                return todos;
            } else {
                return c.stream().map(strProperty).collect(Collectors.joining(","));
            }
        }).orElse(todos);
    }
    
    default public Boolean buscarTodos(String parametroHeader, List<Long> parametroPesquisa){
        Set<Long> todos = parseIntoLongSet.apply(parametroHeader);
        HashSet<Long> setPesquisa = new HashSet<Long>(parametroPesquisa);
        return setPesquisa.containsAll(todos);
    }
    
    default public Function<UserService,String> responsibleForReportInfo(SecurityUser principal) {
        String nome = principal.getName();
        return (usr) -> "Gerado por " + 
                   ((nome == null || nome.length() == 0  )  ?
                           "Não Autenticado" : nome) + " em " + 
                           (new SimpleDateFormat("dd/MM/yyyy HH:mm")).format(new Date());
    }
    
    
    
    @FunctionalInterface
    public static interface ReportParamsInitializer {
        public Map<String,Object> init(Map<String,Object> subParams);
    }
    
    Function<Double,Double> dividePorMil = (num)-> Optional.ofNullable(num).map(
                (d)-> new BigDecimal(d).divide(MIL).setScale(2, RoundingMode.HALF_UP).doubleValue())
                .orElse(BigDecimal.ZERO.doubleValue());
    
    
    
}
