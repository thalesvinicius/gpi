package br.com.gpi.server.endpoint;

import br.com.gpi.server.domain.entity.Municipio;
import br.com.gpi.server.domain.repository.MunicipioRepository;
import java.util.List;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/municipio")
@Stateless
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
@TransactionManagement(TransactionManagementType.BEAN)
public class MunicipioEndpoint {

    @Inject
    private MunicipioRepository municipioRepository;

    @GET
    @Path("{UFId}")
    public Response findByUF(@PathParam("UFId") Long UFId) {
        
        List<Municipio> result = municipioRepository.findByUF(UFId);
        return Response.ok(result).build();
    }
   
}
