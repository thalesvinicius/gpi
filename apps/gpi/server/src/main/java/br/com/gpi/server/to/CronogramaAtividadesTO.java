package br.com.gpi.server.to;

import br.com.gpi.server.domain.entity.Cronograma;
import br.com.gpi.server.domain.entity.Estagio;
import br.com.gpi.server.domain.entity.EstagioProjeto;
import br.com.gpi.server.domain.entity.SituacaoProjetoEnum;
import br.com.gpi.server.util.DateUtils;
import static br.com.gpi.server.util.DateUtils.convertToLocalDate;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class CronogramaAtividadesTO {

    private LocalDate inicioProjetoRealizado;
    private LocalDateTime projetoPromissorRealizado;
    private LocalDate dfPrevisto;
    private LocalDateTime dfRealizado;
    private LocalDate inicioImplantacaoPrevisto;
    private LocalDateTime inicioImplantacaoRealizado;
    private LocalDate inicioOperacaoPrevisto;
    private LocalDateTime inicioOperacaoRealizado;
    private LocalDateTime compromissoRealizado;

    private List<SituacaoProjetoEnum> situacaoProjeto;

    public CronogramaAtividadesTO() {
    }

    public LocalDate getInicioProjetoRealizado() {
        return inicioProjetoRealizado;
    }

    public void setInicioProjetoRealizado(LocalDate inicioProjetoRealizado) {
        this.inicioProjetoRealizado = inicioProjetoRealizado;
    }

    public LocalDateTime getProjetoPromissorRealizado() {
        return projetoPromissorRealizado;
    }

    public void setProjetoPromissorRealizado(LocalDateTime projetoPromissorRealizado) {
        this.projetoPromissorRealizado = projetoPromissorRealizado;
    }

    public LocalDate getDfPrevisto() {
        return dfPrevisto;
    }

    public void setDfPrevisto(LocalDate dfPrevisto) {
        this.dfPrevisto = dfPrevisto;
    }

    public LocalDateTime getDfRealizado() {
        return dfRealizado;
    }

    public void setDfRealizado(LocalDateTime dfRealizado) {
        this.dfRealizado = dfRealizado;
    }

    public LocalDate getInicioImplantacaoPrevisto() {
        return inicioImplantacaoPrevisto;
    }

    public void setInicioImplantacaoPrevisto(LocalDate inicioImplantacaoPrevisto) {
        this.inicioImplantacaoPrevisto = inicioImplantacaoPrevisto;
    }

    public LocalDateTime getInicioImplantacaoRealizado() {
        return inicioImplantacaoRealizado;
    }

    public void setInicioImplantacaoRealizado(LocalDateTime inicioImplantacaoRealizado) {
        this.inicioImplantacaoRealizado = inicioImplantacaoRealizado;
    }

    public LocalDate getInicioOperacaoPrevisto() {
        return inicioOperacaoPrevisto;
    }

    public void setInicioOperacaoPrevisto(LocalDate inicioOperacaoPrevisto) {
        this.inicioOperacaoPrevisto = inicioOperacaoPrevisto;
    }

    public LocalDateTime getInicioOperacaoRealizado() {
        return inicioOperacaoRealizado;
    }

    public void setInicioOperacaoRealizado(LocalDateTime inicioOperacaoRealizado) {
        this.inicioOperacaoRealizado = inicioOperacaoRealizado;
    }

    public LocalDateTime getCompromissoRealizado() {
        return compromissoRealizado;
    }

    public void setCompromissoRealizado(LocalDateTime compromissoRealizado) {
        this.compromissoRealizado = compromissoRealizado;
    }

    public List<SituacaoProjetoEnum> getSituacaoProjeto() {
        return situacaoProjeto;
    }

    public void setSituacaoProjeto(List<SituacaoProjetoEnum> situacaoProjeto) {
        this.situacaoProjeto = situacaoProjeto;
    }

    public CronogramaAtividadesTO(Cronograma cronograma, List<EstagioProjeto> estagios, SituacaoProjetoEnum situacao) {
        LocalDateTime inicioProjeto = extractDataInicioFromEstagios(estagios, Estagio.INICIO_PROJETO);
        this.inicioProjetoRealizado = inicioProjeto == null ? null : inicioProjeto.toLocalDate();
        this.projetoPromissorRealizado = extractDataInicioFromEstagios(estagios, Estagio.PROJETO_PROMISSOR);
        this.dfRealizado = extractDataInicioFromEstagios(estagios, Estagio.FORMALIZADA);
        this.inicioImplantacaoPrevisto = extractIIFromCronograma(cronograma);
        this.inicioImplantacaoRealizado = extractDataInicioFromEstagios(estagios, Estagio.IMPLANTACAO_INICIADO);
        this.inicioOperacaoPrevisto = extractOIFromCronograma(cronograma);
        this.inicioOperacaoRealizado = extractDataInicioFromEstagios(estagios, Estagio.OPERACAO_INICIADA);
        this.compromissoRealizado = extractDataInicioFromEstagios(estagios, Estagio.COMPROMISSO_CUMPRIDO);
        this.situacaoProjeto = new ArrayList<>();
        this.situacaoProjeto.add(situacao);
    }

    private LocalDateTime extractDataInicioFromEstagios(List<EstagioProjeto> estagios, Estagio estagio) {
        if (estagios == null || estagios.isEmpty()) {
            return null;
        }
        for (EstagioProjeto est : estagios) {
            if (estagio.getId() != null && (estagio.getId().longValue() == est.getEstagioId())) {
                return DateUtils.convertToLocalDateTime(est.getDataInicioEstagio());
            }
        }
        return null;
    }

    private LocalDate extractIIFromCronograma(Cronograma cronograma) {
        if (cronograma != null) {
            Date result = (cronograma.getInicioImplantacao() != null) ? cronograma.getInicioImplantacao() : cronograma.getInicioImplantacaoAgricola();
            return result != null ? convertToLocalDate(result) : null;
        } else {
            return null;
        }
    }

    private LocalDate extractOIFromCronograma(Cronograma cronograma) {
        if (cronograma != null) {
            Date result = cronograma.getInicioOperacao() == null ? cronograma.getInicioOperacaoSucroenergetico() : cronograma.getInicioOperacao();
            return result != null ? convertToLocalDate(result) : null;
        } else {
            return null;
        }
    }

}
