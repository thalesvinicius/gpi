package br.com.gpi.server.domain.user.exception;

import br.com.gpi.server.core.exception.BaseWebApplicationException;

public class AuthorizationException extends BaseWebApplicationException {

    public AuthorizationException(String applicationMessage) {
        super(403, "40301", "user.auth.unauthorized", applicationMessage);
    }

}
