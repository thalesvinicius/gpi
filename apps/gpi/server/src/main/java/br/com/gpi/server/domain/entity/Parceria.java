package br.com.gpi.server.domain.entity;

import br.com.gpi.server.domain.common.BaseEntity;
import com.fasterxml.jackson.annotation.JsonIgnore;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.envers.Audited;

@Audited
@Entity
@Table(name = "Parceria")
public class Parceria extends BaseEntity<Long> {
    
    @JsonIgnore
    @ManyToOne(cascade = {CascadeType.DETACH, CascadeType.REFRESH}, optional = false, fetch = FetchType.LAZY)
    @JoinColumn(name = "insumoProduto_id")    
    private InsumoProduto insumoProduto;
    
    private String descricao;

    public InsumoProduto getInsumoProduto() {
        return insumoProduto;
    }

    public void setInsumoProduto(InsumoProduto insumoProduto) {
        this.insumoProduto = insumoProduto;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }
    
}
