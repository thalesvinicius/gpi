package br.com.gpi.server.domain.entity;

import br.com.gpi.server.domain.common.BaseEntity;
import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.apache.deltaspike.data.api.audit.ModifiedOn;
import org.hibernate.envers.Audited;

@Audited
@Entity
@Table(name = "HistoricoSituacaoInstrumentoFormalizacao")
@NamedQueries({
    @NamedQuery(name = HistoricoSituacaoInstrumentoFormalizacao.Query.findUltimoHistoricoSituacaoByInstrumentoId, query = ""
            + "SELECT h FROM HistoricoSituacaoInstrumentoFormalizacao h "
            + "WHERE h.dataInicioSituacao = ( "
            + "     SELECT MAX(h2.dataInicioSituacao) "
            + "     FROM HistoricoSituacaoInstrumentoFormalizacao h2 "
            + "     WHERE h2.instrumentoFormalizacao.id = ?1"
            + " ) and h.instrumentoFormalizacao.id=?1"),
})
public class HistoricoSituacaoInstrumentoFormalizacao extends BaseEntity<Long> {
 
    public HistoricoSituacaoInstrumentoFormalizacao () {
        
    }
    
    public HistoricoSituacaoInstrumentoFormalizacao (Long instrumentoFormalizacaoId, Long situacaoInstrumentoId) {
        this.instrumentoFormalizacao = new InstrumentoFormalizacao();
        this.instrumentoFormalizacao.setId(instrumentoFormalizacaoId);
        
        this.situacao = new SituacaoInstrumentoFormalizacao(SituacaoInstrFormalizacao.getSituacaoInstrFormalizacao(situacaoInstrumentoId.intValue()));
    }
    
    public HistoricoSituacaoInstrumentoFormalizacao (InstrumentoFormalizacao instrumento) {
        this(instrumento.getId(), instrumento.getSituacaoEnum().longValue());
        this.justificativa = instrumento.getJustificativa();
    }
    
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "instrumentoFormalizacao_id")
    private InstrumentoFormalizacao instrumentoFormalizacao;
    
    @ManyToOne
    @JoinColumn(name = "situacaoInstrumentoFormalizacao_id")
    private SituacaoInstrumentoFormalizacao situacao;
    
    @Temporal(TemporalType.TIMESTAMP)
    @ModifiedOn(onCreate = true)
    private Date dataInicioSituacao;
    
    private String justificativa;

    public InstrumentoFormalizacao getInstrumentoFormalizacao() {
        return instrumentoFormalizacao;
    }

    public void setInstrumentoFormalizacao(InstrumentoFormalizacao instrumentoFormalizacao) {
        this.instrumentoFormalizacao = instrumentoFormalizacao;
    }

    public Date getDataInicioSituacao() {
        return dataInicioSituacao;
    }

    public void setDataInicioSituacao(Date dataInicioSituacao) {
        this.dataInicioSituacao = dataInicioSituacao;
    }

    public String getJustificativa() {
        return justificativa;
    }

    public void setJustificativa(String justificativa) {
        this.justificativa = justificativa;
    }

    public SituacaoInstrumentoFormalizacao getSituacao() {
        return situacao;
    }

    public void setSituacao(SituacaoInstrumentoFormalizacao situacao) {
        this.situacao = situacao;
    }
    
   public static class Query {
        public static final String findUltimoHistoricoSituacaoByInstrumentoId = "HistoricoSituacaoInstrumentoFormalizacao.findUltimoHistoricoSituacaoByInstrumentoId";
   }
}
