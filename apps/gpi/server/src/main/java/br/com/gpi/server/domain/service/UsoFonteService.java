package br.com.gpi.server.domain.service;

import br.com.gpi.server.domain.common.BaseService;
import br.com.gpi.server.domain.entity.UsoFonte;
import java.util.List;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

public class UsoFonteService extends BaseService<UsoFonte> {
 
    @Inject
    public UsoFonteService(EntityManager em) {
        super(UsoFonte.class, em);
    }
    
    public List<UsoFonte> findAllUsoFonteByProjeto(Long id_Projeto) {
        StringBuilder builder = new StringBuilder("Select uf From UsoFonte uf ");
        builder.append(" left join fetch uf.financeiro f ");
        builder.append(" left join fetch f.projeto p ");
        builder.append(" Where p.id = :projeto_id ");
        
        TypedQuery<UsoFonte> query = getEm().createQuery(builder.toString(), UsoFonte.class);
        query.setParameter("projeto_id", id_Projeto);
        return query.getResultList();
    }
}
