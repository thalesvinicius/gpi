package br.com.gpi.server.core.jpa.converter;

import br.com.gpi.server.domain.entity.TipoInstrFormalizacao;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = false)
public class TipoInstrFormalizacaoEnumConverter implements AttributeConverter<TipoInstrFormalizacao, Integer> {

    @Override
    public Integer convertToDatabaseColumn(TipoInstrFormalizacao situacao) {
        return situacao.getId();
    }

    @Override
    public TipoInstrFormalizacao convertToEntityAttribute(Integer id) {
        return TipoInstrFormalizacao.getTipoInstrFormalizacao(id);
    }

}
