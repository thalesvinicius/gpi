package br.com.gpi.server.domain.repository;

import br.com.gpi.server.domain.entity.Localizacao;
import java.util.List;
import org.apache.deltaspike.data.api.EntityRepository;
import org.apache.deltaspike.data.api.Modifying;
import org.apache.deltaspike.data.api.Query;
import org.apache.deltaspike.data.api.Repository;
import org.apache.deltaspike.data.api.criteria.CriteriaSupport;


@Repository(forEntity = Localizacao.class)
public abstract class LocalizacaoRepository implements CriteriaSupport<Localizacao>, EntityRepository<Localizacao, Long>{
    
    @Modifying
    @Query(named = Localizacao.Query.locateByProjetoId)
    public abstract List<Localizacao> findAllByProjetoId(Long id);
    
}
