package br.com.gpi.server.querybuilder;

import java.util.Arrays;
import java.util.List;

public class BetweenCondition extends Condition {

    private List<String> values;
//    private Condition filtroDependencia;

    public BetweenCondition() {

    }

    public BetweenCondition(List<String> values, String conditionKey, Field field, ConditionValueTypeEnum type) {
        super(conditionKey, Arrays.asList(new Field[]{field}), type);
        this.values = values;
//        this.filtroDependencia = filtroDependencia;
    }

    public BetweenCondition(List<String> values, String conditionKey, List<Field> fields, ConditionValueTypeEnum type) {
        super(conditionKey, fields, type);
        this.values = values;
//        this.filtroDependencia = filtroDependencia;
    }

    public List<String> getValues() {
        return values;
    }

    public void setValues(List<String> values) {
        this.values = values;
    }

//    public Condition getFiltroDependencia() {
//        return filtroDependencia;
//    }
//
//    public void setFiltroDependencia(Condition filtroDependencia) {
//        this.filtroDependencia = filtroDependencia;
//    }
    @Override
    public String getSQL() {
        StringBuilder sb = new StringBuilder();
        for (Field field : getFields()) {
            sb.append("(");
            sb.append(field.getTabela().getAlias());
            sb.append(".");
            sb.append(field.getNome());
            sb.append(" BETWEEN ");
            if (getValueType().equals(ConditionValueTypeEnum.STRING)) {
                sb.append("'");
                sb.append(getValues().get(0));
                sb.append("'");
                sb.append(" AND ");
                sb.append("'");
                sb.append(getValues().get(1));
                sb.append("'");
            } else {
                sb.append(getValues().get(0));
                sb.append(" AND ");
                sb.append(getValues().get(1));
            }
            sb.append(") AND ");
        }
        
        sb.delete(sb.length() - 4, sb.length());
        
        return sb.toString();
    }

}
