package br.com.gpi.server.domain.entity;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import org.hibernate.envers.Audited;

@Entity
@IdClass(PainelReuniaoInstrumentosAnoPK.class)
@Table(name = "VIEW_RELATORIO_PAINEL_REUNIOES_PROTOCOLOS_ASSINADOS_ANO")
public class PainelReuniaoInstrumentosAno implements Serializable {
    @Id private Long diretoriaId;
    @Id private Long gerenciaId;    

    private String diretoria;        
    private String gerencia;    
    private Long tipoInstrumentoId;
    private Integer anoAssinaturaInstrumento;
    private Long numProtocolos;

    public PainelReuniaoInstrumentosAno() {
    }
    
    public PainelReuniaoInstrumentosAno(Long gerenciaId, String gerencia,
            Integer anoAssinaturaInstrumento, Long numProtocolos) {
        this.gerenciaId = gerenciaId;
        this.gerencia = gerencia;
        this.anoAssinaturaInstrumento = anoAssinaturaInstrumento;
        this.numProtocolos = numProtocolos;        
    }
    
    /**
     * @return the diretoriaId
     */
    public Long getDiretoriaId() {
        return diretoriaId;
    }

    /**
     * @param diretoriaId the diretoriaId to set
     */
    public void setDiretoriaId(Long diretoriaId) {
        this.diretoriaId = diretoriaId;
    }

    /**
     * @return the diretoria
     */
    public String getDiretoria() {
        return diretoria;
    }

    /**
     * @param diretoria the diretoria to set
     */
    public void setDiretoria(String diretoria) {
        this.diretoria = diretoria;
    }

    /**
     * @return the gerenciaId
     */
    public Long getGerenciaId() {
        return gerenciaId;
    }

    /**
     * @param gerenciaId the gerenciaId to set
     */
    public void setGerenciaId(Long gerenciaId) {
        this.gerenciaId = gerenciaId;
    }

    /**
     * @return the gerencia
     */
    public String getGerencia() {
        return gerencia;
    }

    /**
     * @param gerencia the gerencia to set
     */
    public void setGerencia(String gerencia) {
        this.gerencia = gerencia;
    }

    /**
     * @return the anoAssinaturaInstrumento
     */
    public Integer getAnoAssinaturaInstrumento() {
        return anoAssinaturaInstrumento;
    }

    /**
     * @param anoAssinaturaInstrumento the anoAssinaturaInstrumento to set
     */
    public void setAnoAssinaturaInstrumento(Integer anoAssinaturaInstrumento) {
        this.anoAssinaturaInstrumento = anoAssinaturaInstrumento;
    }

    /**
     * @return the numProtocolos
     */
    public Long getNumProtocolos() {
        return numProtocolos;
    }

    /**
     * @param numProtocolos the numProtocolos to set
     */
    public void setNumProtocolos(Long numProtocolos) {
        this.numProtocolos = numProtocolos;
    }

    /**
     * @return the tipoInstrumentoId
     */
    public Long getTipoInstrumentoId() {
        return tipoInstrumentoId;
    }

    /**
     * @param tipoInstrumentoId the tipoInstrumentoId to set
     */
    public void setTipoInstrumentoId(Long tipoInstrumentoId) {
        this.tipoInstrumentoId = tipoInstrumentoId;
    }

}
