package br.com.gpi.server.core.jpa.converter;

import br.com.gpi.server.domain.entity.enumerated.EnumTipoMeioAmbiente;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = false)
public class TipoMeioAmbienteEnumConverter implements AttributeConverter<EnumTipoMeioAmbiente, Integer> {

    @Override
    public Integer convertToDatabaseColumn(EnumTipoMeioAmbiente tipoMeioAmbiente) {
        return tipoMeioAmbiente.getId();
    }

    @Override
    public EnumTipoMeioAmbiente convertToEntityAttribute(Integer dbData) {
        if (dbData == null) {
            return null;
        }
        return EnumTipoMeioAmbiente.getEnumTipoMeioAmbiente(dbData);
    } 
}
