package br.com.gpi.server.domain.repository;

import br.com.gpi.server.domain.entity.Parametro;
import java.util.List;
import org.apache.deltaspike.data.api.EntityRepository;
import org.apache.deltaspike.data.api.Modifying;
import org.apache.deltaspike.data.api.Query;
import org.apache.deltaspike.data.api.Repository;
import org.apache.deltaspike.data.api.criteria.CriteriaSupport;

@Repository(forEntity = Parametro.class)
public abstract class ParametroRepository implements CriteriaSupport<Parametro>,EntityRepository<Parametro, Long> {

    @Modifying
    @Query(named = Parametro.Query.locateByName)
    public abstract Parametro locateByParametroName(String nome);

}
