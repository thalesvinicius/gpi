package br.com.gpi.server.core.jpa.typesafe;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface EnumerationEntity {

    String table();

    String id();

    String description();
}
