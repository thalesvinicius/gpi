package br.com.gpi.server.endpoint;

import br.com.gpi.server.domain.repository.InstrumentoFormalizacaoRepository;
import br.com.gpi.server.domain.repository.PublicacaoRepository;
import br.com.gpi.server.domain.repository.UserRepository;
import br.com.gpi.server.domain.service.PublicacaoService;
import br.com.gpi.server.domain.user.model.SecurityUser;
import br.com.gpi.server.to.PublicacaoTO;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

@Path("/instrumento/{idFormalizacao}/publicacao")
@Stateless
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
@TransactionManagement(TransactionManagementType.BEAN)
public class PublicacaoEndpoint {

    @Inject
    private PublicacaoRepository publicacaoRepository;
    @Inject
    private PublicacaoService publicacaoService;    
    @Inject
    private InstrumentoFormalizacaoRepository formalizacaoRepository;
    @Inject
    private UserRepository userRepository;

    @PathParam("idFormalizacao")
    private Long idFormalizacao;

    @POST
    public Response saveOrUpdate(PublicacaoTO publicacaoTO, @Context SecurityContext context) {
        try {
            SecurityUser loggedUserWrap = (SecurityUser) context.getUserPrincipal();
            return publicacaoService.saveOrUpdate(publicacaoTO, userRepository.findBy(loggedUserWrap.getId()), idFormalizacao);
        } catch (Exception ex) {
            Logger.getLogger(PublicacaoEndpoint.class.getName()).log(Level.SEVERE, null, ex);
            throw new RuntimeException(ex);
        }
    }

    @GET
    public Response getPublicacaoByIdFormalizacao() {
        return Response.ok(publicacaoService.getPublicacaoByIdFormalizacao(idFormalizacao)).build();
    }
    
}
