package br.com.gpi.server.to;

import br.com.gpi.server.domain.entity.Projeto;
import br.com.gpi.server.domain.entity.TipoEmprego;

public class EmpregoTO {
    
    private Projeto projeto;
    private Integer ano;
    private Integer direto;
    private Integer indireto;
    private TipoEmprego tipoEmprego;

    private Long diretoLong;
    private Long indiretoLong;
    
    
    public EmpregoTO() {
    }

    public EmpregoTO(Integer direto, Integer indireto) {
        this.direto = direto;
        this.indireto = indireto;
    }

    public EmpregoTO(Long diretoLong, Long indiretoLong) {
        this.diretoLong = diretoLong;
        this.indiretoLong = indiretoLong;
    }
    
    public Projeto getProjeto() {
        return projeto;
    }

    public void setProjeto(Projeto projeto) {
        this.projeto = projeto;
    }

    public Integer getAno() {
        return ano;
    }

    public void setAno(Integer ano) {
        this.ano = ano;
    }

    public Integer getDireto() {
        return direto;
    }

    public void setDireto(Integer direto) {
        this.direto = direto;
    }

    public Integer getIndireto() {
        return indireto;
    }

    public void setIndireto(Integer indireto) {
        this.indireto = indireto;
    }

    public TipoEmprego getTipoEmprego() {
        return tipoEmprego;
    }

    public void setTipoEmprego(TipoEmprego tipoEmprego) {
        this.tipoEmprego = tipoEmprego;
    }

    public Long getDiretoLong() {
        return diretoLong;
    }

    public void setDiretoLong(Long diretoLong) {
        this.diretoLong = diretoLong;
    }

    public Long getIndiretoLong() {
        return indiretoLong;
    }

    public void setIndiretoLong(Long indiretoLong) {
        this.indiretoLong = indiretoLong;
    }
    
}
