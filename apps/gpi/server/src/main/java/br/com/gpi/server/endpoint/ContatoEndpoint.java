package br.com.gpi.server.endpoint;

import br.com.gpi.server.domain.entity.Contato;
import br.com.gpi.server.domain.entity.Empresa;
import br.com.gpi.server.domain.entity.User;
import br.com.gpi.server.domain.entity.UsuarioInterno;
import br.com.gpi.server.domain.repository.ContatoRepository;
import br.com.gpi.server.domain.repository.EmpresaRepository;
import br.com.gpi.server.domain.repository.UserRepository;
import br.com.gpi.server.domain.service.ContatoService;
import br.com.gpi.server.domain.user.model.SecurityUser;
import java.util.List;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

@Path("/empresa/{idEmpresa}/contato")
@Stateless
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
@TransactionManagement(TransactionManagementType.BEAN)
public class ContatoEndpoint {

    @Inject
    private ContatoService contatoService;

    @Inject
    private ContatoRepository contatoRepository;

    @Inject
    private EmpresaRepository empresaRepository;

    @Inject
    private UserRepository userRepository;

    @PathParam("idEmpresa")
    private Long idEmpresa;

    @POST
    public Response saveNewContato(Contato contato, @Context SecurityContext context) {
        SecurityUser loggedUser = (SecurityUser) context.getUserPrincipal();
        User user = new UsuarioInterno();
        user.setId(loggedUser.getId());
        contato.setUsuarioResponsavel(userRepository.findBy(loggedUser.getId()));
        Empresa empresa = new Empresa();
        empresa.setId(idEmpresa);
        contato.setEmpresa(empresa);
        contatoRepository.save(contato);
        return Response.ok(contato).build();
    }

    @GET
    public Response findByCompany() {
        List<Contato> result = contatoRepository.findByCompany(idEmpresa);
        return Response.ok(result).build();
    }

    @POST
    @Path("removeContato")
    public Response removeContato(List<Long> ids) {
        contatoRepository.deleteById(ids);
        return Response.ok("Tudo Ok!").build();
    }

}
