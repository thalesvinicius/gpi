package br.com.gpi.server.config;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import org.apache.deltaspike.core.api.config.ConfigProperty;

@ApplicationScoped
public class ApplicationSettings {
    @Inject
    @ConfigProperty(name = "emailVerificationTokenExpiryTimeInMinutes")
    private int emailVerificationTokenExpiryTimeInMinutes;
    @Inject
    @ConfigProperty(name = "emailRegistrationTokenExpiryTimeInMinutes")
    private int emailRegistrationTokenExpiryTimeInMinutes;
    @Inject
    @ConfigProperty(name = "emailLostPasswordTokenExpiryTimeInMinutes")
    private int emailLostPasswordTokenExpiryTimeInMinutes;
    @Inject
    @ConfigProperty(name = "email.receiver.ice.environment")
    private String receiverIceEnvironment;
    @Inject
    @ConfigProperty(name = "baseURL")
    private String baseUrl;
    @Inject
    @ConfigProperty(name = "development.db.url")
    private String developmentDBUrl;
    @Inject
    @ConfigProperty(name = "development.db.username")
    private String developmentDBUsername;
    @Inject
    @ConfigProperty(name = "gpi.version")
    private String version;
    @Inject
    @ConfigProperty(name = "gpi.generatedDateTime")
    private String generatedDateTime;

    public int getEmailVerificationTokenExpiryTimeInMinutes() {
        return emailVerificationTokenExpiryTimeInMinutes;
    }

    public String getBaseUrl() {
        return baseUrl;
    }

    public int getEmailRegistrationTokenExpiryTimeInMinutes() {
        return emailRegistrationTokenExpiryTimeInMinutes;
    }

    public int getEmailLostPasswordTokenExpiryTimeInMinutes() {
        return emailLostPasswordTokenExpiryTimeInMinutes;
    }

    public String getDevelopmentDBUrl() {
        return developmentDBUrl;
    }

    public void setDevelopmentDBUrl(String developmentDBUrl) {
        this.developmentDBUrl = developmentDBUrl;
    }

    public String getDevelopmentDBUsername() {
        return developmentDBUsername;
    }

    public void setDevelopmentDBUsername(String developmentDBUsername) {
        this.developmentDBUsername = developmentDBUsername;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getGeneratedDateTime() {
        return generatedDateTime;
    }

    public void setGeneratedDateTime(String generatedDateTime) {
        this.generatedDateTime = generatedDateTime;
    }

    public String getReceiverIceEnvironment() {
        return receiverIceEnvironment;
    }

    public void setReceiverIceEnvironment(String receiverIceEnvironment) {
        this.receiverIceEnvironment = receiverIceEnvironment;
    }

}
