package br.com.gpi.server.domain.user.exception;

import br.com.gpi.server.core.exception.BaseWebApplicationException;

/**
 *
 * @author <a href="mailto:pviniciusfm@gmail.com">Paulo Vinicius F. Machado</a>
 * @version 1.0
 */
public class UserNotFoundException extends BaseWebApplicationException {

    private static final long serialVersionUID = 1L;

    public UserNotFoundException() {
        super(404, "40402", "user.invalid.notFound", "No User could be found for that Id");
    }
}
