package br.com.gpi.server.domain.entity.enumerated;

import br.com.gpi.server.core.exception.EnumException;
import br.com.gpi.server.core.jpa.typesafe.EnumerationEntityType;

public enum EnumCadeiaProdutiva implements EnumerationEntityType<Integer> {

    AERONAUTICO(1, "Aeronáutico"),
    AGRONEGOCIO(2, "Agronegócio"),
    AGRONEG_ALIM_BEB(3, "Agronegócio, Alimentos, Bebidas"),
    ALIMENTOS(4, "Alimentos"),
    AUTOMOTIVO_AUTOPECAS(5, "Automotivo e Autopeças"),
    BEBIDAS(6, "Bebidas"),
    BEBIDAS_GRAOS(7, "Bebidas e Grãos"),
    BIODIESEL(8, "Biodiesel"),
    BIOTECNOLOGIA(9, "Biotecnologia"),
    CARNES_RACOES(10, "Carnes e Rações"),
    CIMENTO_MINERAIS_NAO_FERROSOS(11, "Cimento e Minerais não Ferrosos"),
    COMERCIO_SERVICOS(12, "Comércio e Serviços"),
    CONSTRUCAO_CIVIL(13, "Construção Civil"),
    COSMETICOS_E_HIGIENE_PESSOAL(14, "Cosméticos e Higiene Pessoal"),
    COURO_CALCADOS(15, "Couro e Calçados"),
    E_COMMERCE(16, "E-commerce"),
    ELETRICO_ELETROELETRONICOS(17, "Elétrico e Eletroeletrônicos"),
    ELETROPORTAVEIS_LINHA_BRANCA(18, "Eletroportáveis (Linha Branca)"),
    ELETROPORTAVEIS(19, "Eletroportáveis"),
    EMBALAGENS_E_PLASTICOS(20, "Embalagens e Plásticos para Construção"),
    EMPREENDIMENTOS_IMOBILIARIOS(21, "Empreendimentos Imobiliários"),
    ENERGIA(22, "Energia"),
    ENERGIAS_HIDRICAS(23, "Energias Hídricas"),
    ENERGIAS_RENOVAVEIS(24, "Energias Renováveis"),
    EQUIP_LAB_E_HOSP(25, "Equip. Labotoriais e Hospitalares"),
    FARMACOS(26, "Fármacos"),
    FERROVIARIA(27, "Ferroviária"),
    INDUSTRIA_QUIMICA(28, "Indústria Química"),
    LACTEOS_CAFE(29, "Lácteos e Café"),
    MASSA_BATATAS(30, "Massa e Batatas"),
    MECANICA_BENS_CAP(31, "Mecânica e Bens de Capital"),
    METALURGICA(32, "Metalúrgica"),
    MICROELETRONICA(33, "Microeletrônica"),
    MINERACAO_FERRO(34, "Mineração Ferro"),
    MOVEIS_ARTEFATOS_DE_MADEIRA(35, "Móveis e Artefatos de Madeira"),
    OUTROS(36, "Outros"),
    PAPEL_CELULOSE(37, "Papel e Celulose"),
    SERVICOS(38, "Serviços"),
    SIDERURGIA(39, "Siderurgia"),
    SOFTWARE_TI(40, "Software e Tecnologia da Informação"),
    SOLAR_EOLICA_OUTRAS_FONTES(41, "Solar, Eólica e Outras Fontes"),
    SUCROENERGETICO(42, "Sucroenergético"),
    TEXTIL_VESTUARIO_CONFECCOES(43, "Têxtil, Vestuário e Confecções");

    private final int id;
    private final String descricao;

    private EnumCadeiaProdutiva(int id, String descricao) {
        this.id = id;
        this.descricao = descricao;
    }

    @Override
    public Integer getId() {
        return this.id;
    }

    @Override
    public String getDescription() {
        return this.descricao;
    }
    
    public static EnumCadeiaProdutiva getCadeiaProdutivaById(final int idCadeiaProdutiva) {
        for (EnumCadeiaProdutiva cadeiaProdutiva : values()) {
            if (cadeiaProdutiva.id == idCadeiaProdutiva) {
                return cadeiaProdutiva;
            }
        }
        throw new EnumException(String.format("Não foi possível recuperar a cadeia produtiva com o id %s", idCadeiaProdutiva));
    }

}
