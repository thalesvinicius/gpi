package br.com.gpi.server.domain.repository;

import br.com.gpi.server.domain.entity.Anexo;
import br.com.gpi.server.domain.entity.Publicacao;
import java.util.List;
import org.apache.deltaspike.data.api.EntityRepository;
import org.apache.deltaspike.data.api.Modifying;
import org.apache.deltaspike.data.api.Query;
import org.apache.deltaspike.data.api.Repository;
import org.apache.deltaspike.data.api.criteria.CriteriaSupport;

@Repository(forEntity = Publicacao.class)
public abstract class PublicacaoRepository implements CriteriaSupport<Publicacao>, EntityRepository<Publicacao, Long> {

    @Modifying
    @Query(named = Publicacao.Query.findAnexosByPublicacao)        
    public abstract List<Anexo> findAnexosByPublicacao(Long idFormalizacao);
    
}
