package br.com.gpi.server.domain.entity;

import java.io.Serializable;
import java.util.Objects;

public class CNAEEmpresaPK implements Serializable {

    private Long empresaId;
    private String cnaeId;

    public Long getEmpresaId() {
        return empresaId;
    }

    public void setEmpresaId(Long empresaId) {
        this.empresaId = empresaId;
    }

    public String getCnaeId() {
        return cnaeId;
    }

    public void setCnaeId(String cnaeId) {
        this.cnaeId = cnaeId;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + Objects.hashCode(this.empresaId);
        hash = 59 * hash + Objects.hashCode(this.cnaeId);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final CNAEEmpresaPK other = (CNAEEmpresaPK) obj;
        if (!Objects.equals(this.empresaId, other.empresaId)) {
            return false;
        }
        if (!Objects.equals(this.cnaeId, other.cnaeId)) {
            return false;
        }
        return true;
    }

}
