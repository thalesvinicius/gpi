package br.com.gpi.server.domain.service;

import br.com.gpi.server.domain.common.BaseService;
import br.com.gpi.server.domain.entity.MicroRegiao;
import javax.inject.Inject;
import javax.persistence.EntityManager;

public class MicroRegiaoService  extends BaseService<MicroRegiao>{
    
    @Inject
    public MicroRegiaoService(EntityManager em) {
        super(MicroRegiao.class, em);
    }
    
}
