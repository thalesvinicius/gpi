package br.com.gpi.server.domain.service;

import br.com.gpi.server.domain.common.BaseService;
import br.com.gpi.server.domain.entity.CadeiaProdutiva;
import br.com.gpi.server.domain.entity.Cronograma;
import br.com.gpi.server.domain.entity.Estagio;
import br.com.gpi.server.domain.entity.Projeto;
import br.com.gpi.server.domain.entity.SituacaoProjetoEnum;
import br.com.gpi.server.domain.entity.User;
import br.com.gpi.server.domain.entity.UsuarioExterno;
import br.com.gpi.server.domain.repository.CadeiaProdutivaRepository;
import br.com.gpi.server.domain.repository.CronogramaRepository;
import br.com.gpi.server.domain.repository.UserRepository;
import br.com.gpi.server.domain.user.model.SecurityUser;
import br.com.gpi.server.util.LogUtil;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;
import javax.validation.Valid;
import javax.ws.rs.core.SecurityContext;

public class CronogramaService extends BaseService<Cronograma> {

    @Inject
    private CronogramaRepository cronogramaRepository;

    @Inject
    private ProjetoService projetoService;

    @Inject
    private UserRepository userRepository;

    @Inject
    private UserTransaction transaction;
    
    @Inject
    private CadeiaProdutivaRepository cadeiaProdutivaRepository;

    @Inject
    public CronogramaService(EntityManager em) {
        super(Cronograma.class, em);
    }

    public Cronograma findByProject(Long id) {
        try {
            return cronogramaRepository.findByProject(id);
        } catch (NoResultException ex) {
            return null;
        }
    }

    public Cronograma saveOrUpdateCronograma(@Valid Cronograma cronograma, SecurityContext context, Long idProjeto) throws SystemException {
        try {
            transaction.begin();
            this.getEm().joinTransaction();

            SecurityUser loggedUser = (SecurityUser) context.getUserPrincipal();
            User user = userRepository.findBy(loggedUser.getId());
            cronograma.setUsuarioResponsavel(user);
            cronograma.setDataUltimaAlteracao(new Date());
            Projeto projeto = new Projeto();
            projeto.setId(idProjeto);

            cronograma.setProjeto(projeto);

            //Altera a situação do projeto se o estágio for INICIO_PROJETO.
            if (projetoService.findEstagioAtualById(idProjeto).getEstagio() == Estagio.INICIO_PROJETO) {
                if(user instanceof UsuarioExterno) {
                    projetoService.updateSituacaoProjetoTransactional(SituacaoProjetoEnum.PENDENTE_ACEITE, idProjeto, user.getId(), new Date());
                } else {
                    projetoService.updateSituacaoProjetoTransactional(SituacaoProjetoEnum.PEDENTE_VALIDACAO, idProjeto, user.getId(), new Date());
                    
                }
            }
            cronograma = (Cronograma) this.getEm().merge(cronograma);

            transaction.commit();
            return cronograma;
        } catch (Exception ex) {
            transaction.rollback();
            Logger.getLogger(ProjetoService.class.getName()).log(Level.SEVERE, null, ex);
            throw new RuntimeException(ex);
        }
    }

    public Date getDataInicioOperacao(Long idProjeto) {
        Cronograma cronograma = findByProject(idProjeto);       
                
        Date dataInicioOperacao = null;
        List<CadeiaProdutiva> cadeiaProdutiva = cadeiaProdutivaRepository.locateByProjetoId(idProjeto);
        
        if (cronograma != null && cadeiaProdutiva != null) {            
            if (cadeiaProdutiva.get(0).getId().equals(42L)) {
                dataInicioOperacao = cronograma.getInicioOperacaoSucroenergetico();
            } else{
                dataInicioOperacao = cronograma.getInicioOperacao();
            }
        }
        return dataInicioOperacao;
    }
    
    public Object verifyChange(Long idProjeto) throws SystemException {
        String atributos = " inicioContratacaoEquipamentos_MOD,inicioImplantacao_MOD,inicioImplantacaoAgricola_MOD, " +
            "inicioImplantacaoIndustrial_MOD,inicioOperacao_MOD,inicioOperacaoSucroenergetico_MOD, " +
            "inicioProjeto_MOD,inicioProjetoExecutivo_MOD,inicioTratamentoTributarioPrevisto_MOD, " +
            "inicioTratamentoTributarioRealizado_MOD,terminoImplantacaoAgricola_MOD, " +
            "terminoImplantacaoIndustrial_MOD,terminoProjeto_MOD,terminoProjetoExecutivo_MOD ";
        Object result = this.getEm().createNativeQuery(
                LogUtil.montaConsultaLog("LogCronograma", atributos, idProjeto).toString()).getResultList();

        return result;
    }          
}
