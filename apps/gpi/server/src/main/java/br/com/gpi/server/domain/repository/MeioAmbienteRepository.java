package br.com.gpi.server.domain.repository;

import br.com.gpi.server.domain.entity.MeioAmbiente;
import java.util.List;
import org.apache.deltaspike.data.api.EntityRepository;
import org.apache.deltaspike.data.api.Modifying;
import org.apache.deltaspike.data.api.Query;
import org.apache.deltaspike.data.api.Repository;
import org.apache.deltaspike.data.api.criteria.CriteriaSupport;

@Repository(forEntity = MeioAmbiente.class)
public abstract class MeioAmbienteRepository implements CriteriaSupport<MeioAmbiente>,EntityRepository<MeioAmbiente, Long>{

    @Modifying
    @Query(named = MeioAmbiente.Query.locateByProjetoIdForEdition)
    public abstract List<MeioAmbiente> locateByProjetoId(Long id);

}
