package br.com.gpi.server.domain.entity;

import br.com.gpi.server.domain.common.BaseEntity;
import br.com.gpi.server.util.MaskUtil;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.envers.Audited;

/**
 *
 * @author lucasgom
 */
@Audited
@Entity
@Table(name = "Endereco")
public class Endereco extends BaseEntity<Long> {

    @Column(name = "logradouro", length = 250, nullable = false)
    private String logradouro;

    @Column(name = "numero", length = 15)
    private String numero;

    @Column(name = "complemento", length = 50, unique = true)
    private String complemento;

    @Column(name = "cep", length = 8)
    private String cep;

    @ManyToOne(cascade = {CascadeType.DETACH, CascadeType.REFRESH})
    @JoinColumn(name = "municipio_id", nullable = true)
    private Municipio municipio;

    @Column(name = "bairro", length = 255)
    private String bairro;

    @Column(name = "cidade", length = 255)
    private String cidade;

    @ManyToOne(cascade = {CascadeType.DETACH, CascadeType.REFRESH})
    @JoinColumn(name = "pais_id", nullable = true)
    private Pais pais;

    @ManyToOne(cascade = {CascadeType.DETACH, CascadeType.REFRESH})
    @JoinColumn(name = "uf_id", nullable = true)
    private UF uf;    

    public String getLogradouro() {
        return logradouro;
    }

    public void setLogradouro(String logradouro) {
        this.logradouro = logradouro;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getComplemento() {
        return complemento;
    }

    public void setComplemento(String complemento) {
        this.complemento = complemento;
    }

    public String getCep() {
        return cep;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }

    public Municipio getMunicipio() {
        return municipio;
    }

    public void setMunicipio(Municipio municipio) {
        this.municipio = municipio;
    }

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public Pais getPais() {
        return pais;
    }

    public void setPais(Pais pais) {
        this.pais = pais;
    }

    public UF getUf() {
        return uf;
    }

    public void setUf(UF uf) {
        this.uf = uf;
    }

    public String getEnderecoFormatado() {
        StringBuilder enderecoFormatadoSb = new StringBuilder();
        if (this.getMunicipio() != null) {
            enderecoFormatadoSb.append(this.getMunicipio().getNome());
        } else if (this.getCidade() != null) {
            enderecoFormatadoSb.append(this.getCidade());
        } else {
            enderecoFormatadoSb.append(" - ");
        }
        enderecoFormatadoSb.append(", ");
        enderecoFormatadoSb.append(this.getLogradouro() == null ? " - " : this.getLogradouro());
        enderecoFormatadoSb.append(", ");
        enderecoFormatadoSb.append(this.getNumero() == null ? " - " : this.getNumero());
        enderecoFormatadoSb.append(", ");
        enderecoFormatadoSb.append(this.getComplemento() == null ? "" : this.getComplemento());
        enderecoFormatadoSb.append(" - CEP: ");
        enderecoFormatadoSb.append(this.getCep() != null ? MaskUtil.format("#####-###", this.getCep()) : " - ");
        enderecoFormatadoSb.append(", ");
        enderecoFormatadoSb.append(this.getBairro() == null ? " - " : this.getBairro());

        return enderecoFormatadoSb.toString();
    }

    @Override
    public String toString() {
        return getEnderecoFormatado();
    }
}
