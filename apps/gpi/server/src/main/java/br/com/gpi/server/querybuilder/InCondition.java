package br.com.gpi.server.querybuilder;

import java.util.Arrays;
import java.util.List;

public class InCondition extends Condition {

    private List<String> values;

    public InCondition() {
    }

    public InCondition(List<String> values, String conditionKey, Field field, ConditionValueTypeEnum type) {
        super(conditionKey, Arrays.asList(new Field[]{field}), type);
        this.values = values;
    }

    public List<String> getValues() {
        return values;
    }

    public void setValues(List<String> values) {
        this.values = values;
    }

    @Override
    public String getSQL() {
        StringBuilder sb = new StringBuilder();
        sb.append(getField().getTabela().getAlias());
        sb.append(".");
        sb.append(getField().getNome());
        sb.append(" IN (");
        getValues().stream().forEachOrdered(u -> {
            if (getValueType().equals(ConditionValueTypeEnum.STRING)) {
                sb.append("'");
                sb.append(u);
                sb.append("'");
            } else {
                sb.append(u);
            }

            sb.append(", ");
        });

        sb.delete(sb.length() - 2, sb.length());
        sb.append(")");

        return sb.toString();
    }

}
