package br.com.gpi.server.domain.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import javax.persistence.Transient;
import org.hibernate.envers.Audited;

@Entity
@IdClass(PainelReuniaoInstrumentosMesAnoPK.class)
@Table(name = "VIEW_RELATORIO_PAINEL_METAS")
public class PainelMetas {
    @Id private Long gerenciaId;
    @Id private Long diretoriaId;
    
    private String gerencia;    
    private String diretoria;    
    private Boolean prospeccaoAtiva;
    private Long estagioId;
    private Long situacaoProjetoId;
    private Integer mesCadastro;
    private Integer anoCadastro;
    private Long empregosDiretos;      
    private Double investimentoPrevistoMilhoes;
    private Double faturamentoPlenoMilhoes;
    @Transient    
    private Long projetos;
            
    public PainelMetas() {
    }
    
    public PainelMetas(Long gerenciaId, String gerencia, Integer mesCadastro, Integer anoCadastro, 
            Long empregosDiretos) {
        this.gerenciaId = gerenciaId;
        this.gerencia = gerencia;
        this.mesCadastro = mesCadastro;
        this.anoCadastro = anoCadastro;
        this.empregosDiretos = empregosDiretos;    
    }
    
    public PainelMetas(Integer mesCadastro,Integer anoCadastro, Long empregosDiretos,
            Double faturamentoPlenoMilhoes, Double investimentoPrevistoMilhoes, Long projetos) {
        this.mesCadastro = mesCadastro;
        this.anoCadastro = anoCadastro;
        this.empregosDiretos = empregosDiretos;        
        this.faturamentoPlenoMilhoes = faturamentoPlenoMilhoes;
        this.investimentoPrevistoMilhoes = investimentoPrevistoMilhoes;
        this.projetos = projetos;        
    }

    /**
     * @return the gerenciaId
     */
    public Long getGerenciaId() {
        return gerenciaId;
    }

    /**
     * @param gerenciaId the gerenciaId to set
     */
    public void setGerenciaId(Long gerenciaId) {
        this.gerenciaId = gerenciaId;
    }

    /**
     * @return the diretoriaId
     */
    public Long getDiretoriaId() {
        return diretoriaId;
    }

    /**
     * @param diretoriaId the diretoriaId to set
     */
    public void setDiretoriaId(Long diretoriaId) {
        this.diretoriaId = diretoriaId;
    }

    /**
     * @return the gerencia
     */
    public String getGerencia() {
        return gerencia;
    }

    /**
     * @param gerencia the gerencia to set
     */
    public void setGerencia(String gerencia) {
        this.gerencia = gerencia;
    }

    /**
     * @return the diretoria
     */
    public String getDiretoria() {
        return diretoria;
    }

    /**
     * @param diretoria the diretoria to set
     */
    public void setDiretoria(String diretoria) {
        this.diretoria = diretoria;
    }

    /**
     * @return the prospeccaoAtiva
     */
    public Boolean getProspeccaoAtiva() {
        return prospeccaoAtiva;
    }

    /**
     * @param prospeccaoAtiva the prospeccaoAtiva to set
     */
    public void setProspeccaoAtiva(Boolean prospeccaoAtiva) {
        this.prospeccaoAtiva = prospeccaoAtiva;
    }

    /**
     * @return the estagioId
     */
    public Long getEstagioId() {
        return estagioId;
    }

    /**
     * @param estagioId the estagioId to set
     */
    public void setEstagioId(Long estagioId) {
        this.estagioId = estagioId;
    }

    /**
     * @return the situacaoProjetoId
     */
    public Long getSituacaoProjetoId() {
        return situacaoProjetoId;
    }

    /**
     * @param situacaoProjetoId the situacaoProjetoId to set
     */
    public void setSituacaoProjetoId(Long situacaoProjetoId) {
        this.situacaoProjetoId = situacaoProjetoId;
    }

    /**
     * @return the mesCadastro
     */
    public Integer getMesCadastro() {
        return mesCadastro;
    }

    /**
     * @param mesCadastro the mesCadastro to set
     */
    public void setMesCadastro(Integer mesCadastro) {
        this.mesCadastro = mesCadastro;
    }

    /**
     * @return the anoCadastro
     */
    public Integer getAnoCadastro() {
        return anoCadastro;
    }

    /**
     * @param anoCadastro the anoCadastro to set
     */
    public void setAnoCadastro(Integer anoCadastro) {
        this.anoCadastro = anoCadastro;
    }

    /**
     * @return the empregosDiretos
     */
    public Long getEmpregosDiretos() {
        return empregosDiretos;
    }

    /**
     * @param empregosDiretos the empregosDiretos to set
     */
    public void setEmpregosDiretos(Long empregosDiretos) {
        this.empregosDiretos = empregosDiretos;
    }

    /**
     * @return the projetos
     */
    public Long getProjetos() {
        return projetos;
    }

    /**
     * @param projetos the projetos to set
     */
    public void setProjetos(Long projetos) {
        this.projetos = projetos;
    }

    /**
     * @return the investimentoPrevistoMilhoes
     */
    public Double getInvestimentoPrevistoMilhoes() {
        return investimentoPrevistoMilhoes;
    }

    /**
     * @param investimentoPrevistoMilhoes the investimentoPrevistoMilhoes to set
     */
    public void setInvestimentoPrevistoMilhoes(Double investimentoPrevistoMilhoes) {
        this.investimentoPrevistoMilhoes = investimentoPrevistoMilhoes;
    }

    /**
     * @return the faturamentoPlenoMilhoes
     */
    public Double getFaturamentoPlenoMilhoes() {
        return faturamentoPlenoMilhoes;
    }

    /**
     * @param faturamentoPlenoMilhoes the faturamentoPlenoMilhoes to set
     */
    public void setFaturamentoPlenoMilhoes(Double faturamentoPlenoMilhoes) {
        this.faturamentoPlenoMilhoes = faturamentoPlenoMilhoes;
    }
    
        
}
