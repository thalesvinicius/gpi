package br.com.gpi.server.domain.repository;

import br.com.gpi.server.domain.entity.InstrumentoFormalizacao;
import br.com.gpi.server.domain.entity.SituacaoInstrFormalizacao;
import br.com.gpi.server.domain.entity.TipoInstrFormalizacao;
import br.com.gpi.server.domain.entity.User;
import java.util.Date;
import org.apache.deltaspike.data.api.EntityRepository;
import org.apache.deltaspike.data.api.Modifying;
import org.apache.deltaspike.data.api.Query;
import org.apache.deltaspike.data.api.Repository;
import org.apache.deltaspike.data.api.criteria.CriteriaSupport;

@Repository(forEntity = InstrumentoFormalizacao.class)
public abstract class InstrumentoFormalizacaoRepository implements CriteriaSupport<InstrumentoFormalizacao>, EntityRepository<InstrumentoFormalizacao, Long> {

    @Query(named = InstrumentoFormalizacao.Query.recoverSituacaoById)
    public abstract Integer recoverSituacaoById(Long idFormalizacao);

    @Query(named = InstrumentoFormalizacao.Query.recoverTipoById)
    public abstract TipoInstrFormalizacao recoverTipoById(Long idFormalizacao);

    @Query(named = InstrumentoFormalizacao.Query.recoverCodigoProtocoloById)
    public abstract String recoverCodigoProtocoloById(Long idFormalizacao);

    @Query(named = InstrumentoFormalizacao.Query.recoverMaiorVersaoByProtocolo)
    public abstract Integer recoverMaiorVersaoByProtocolo(String protocolo);
    
    @Modifying
    @Query(named = InstrumentoFormalizacao.Query.updateUsuarioeData)
    public abstract void updateUsuarioeData(Long id, User usuarioResponsavel, Date dataUltimaAlteracao);
    
    @Modifying
    @Query(named = InstrumentoFormalizacao.Query.updateDataAssinatura)
    public abstract void updateDataAssinatura(Long id, User usuarioResponsavel, Date dataUltimaAlteracao, Date dataAssinatura);
    
    @Modifying
    @Query(named = InstrumentoFormalizacao.Query.updateSituacaoInstrumento)
    public abstract void updateSituacaoInstrumento(Long id, User usuarioResponsavel, Date dataUltimaAlteracao, Integer situacaoInstrFormalizacao);
    
    @Modifying
    @Query(named = InstrumentoFormalizacao.Query.updateLocalDocumento)
    public abstract void updateLocalDocumento(Long id, Long idLocal);
    
    @Modifying
    @Query(named = InstrumentoFormalizacao.Query.uncheckUltimaVersaoById)
    public abstract void uncheckUltimaVersaoById(Long id);
        
    @Query(named = InstrumentoFormalizacao.Query.countByProtocoloAndInstrumentoNotEquals)
    public abstract Long countByProtocoloAndInstrumentoNotEquals(String protocolo, Long id);

    @Query(named = InstrumentoFormalizacao.Query.countByProtocoloAndTipoAndSituacaoEnumAndIdNotEquals)
    public abstract Long countByProtocoloAndTipoAndSituacaoAtualAndIdNotEquals(String protocolo, TipoInstrFormalizacao tipo, Integer situacaoInstrFormalizacaoId, Long id);
        
    @Query(named = InstrumentoFormalizacao.Query.countByProtocoloAndTipoAndSituacaoEnum)
    public abstract Long countByProtocoloAndTipoAndSituacaoAtual(String protocolo, TipoInstrFormalizacao tipo, Integer situacaoInstrFormalizacaoId);
}
