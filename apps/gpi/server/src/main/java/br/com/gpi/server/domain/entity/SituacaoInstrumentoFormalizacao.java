package br.com.gpi.server.domain.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.envers.Audited;

@Audited
@Entity
@Table(name = "SituacaoInstrumentoFormalizacao")
public class SituacaoInstrumentoFormalizacao implements Serializable {
    
    public SituacaoInstrumentoFormalizacao() {
    
    }
    
    public SituacaoInstrumentoFormalizacao(SituacaoInstrFormalizacao situacaoEnum) {
        this.id = Long.valueOf(situacaoEnum.getId());
        this.descricao = situacaoEnum.getDescription();
    }
    
    
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    private Long id;
    
    @Column(name = "descricao", length = 100, insertable = false, updatable = false)
    private String descricao;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }
    
    
}
