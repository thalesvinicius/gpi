package br.com.gpi.server.core;

import org.apache.deltaspike.core.api.projectstage.ProjectStage;
import org.apache.deltaspike.core.api.projectstage.ProjectStageHolder;

public class CustomProjectStageHolder implements ProjectStageHolder {

    public static final class PreProduction extends ProjectStage {

        private static final long serialVersionUID = 1029094387976167179L;
    }

    public static final PreProduction PreProduction = new PreProduction();
}
