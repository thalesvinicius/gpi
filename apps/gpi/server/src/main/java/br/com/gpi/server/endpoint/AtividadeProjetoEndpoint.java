package br.com.gpi.server.endpoint;

import br.com.gpi.server.domain.entity.AtividadeProjeto;
import br.com.gpi.server.domain.entity.User;
import br.com.gpi.server.domain.repository.AtividadeProjetoRepository;
import br.com.gpi.server.domain.repository.UserRepository;
import br.com.gpi.server.domain.service.AtividadeProjetoService;
import br.com.gpi.server.domain.user.model.SecurityUser;
import java.util.List;
import javax.annotation.security.PermitAll;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

@Path("/projeto/{projetoId}/atividade")
@Stateless
@PermitAll
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
@TransactionManagement(TransactionManagementType.BEAN)
public class AtividadeProjetoEndpoint {

    @Inject
    private AtividadeProjetoService atividadeProjetoService;
    
    @Inject
    private AtividadeProjetoRepository atividadeProjetoRepository;
    @Inject
    private UserRepository userRepository;
    @PathParam("projetoId")
    private Long projetoId;

    @GET
    public Response findAtividadeProjetoByProjetoIdForEdition() {
        return Response.ok(atividadeProjetoService.findByProjetoIdForEdit(projetoId)).build();
    }

    @POST
    public Response saveAtividadeProjetoByProjetoId(AtividadeProjeto atividade, @Context SecurityContext context) {
        SecurityUser loggedUserWrap = (SecurityUser) context.getUserPrincipal();
        User loggedUser = userRepository.findBy(loggedUserWrap.getId());
        return Response.ok(atividadeProjetoService.saveAtividadesProjeto(atividade, projetoId, loggedUser)).build();
    }

    @POST
    @Path("copyFromTemplate")
    public Response copyTemplateAtividadesToProjeto(@Context SecurityContext context) {
        SecurityUser loggedUserWrap = (SecurityUser) context.getUserPrincipal();
        User loggedUser = userRepository.findBy(loggedUserWrap.getId());
        List<AtividadeProjeto> atividadesProjeto = atividadeProjetoService.copyTemplateAtividadesToProjeto(projetoId, loggedUser);
        return Response.ok(atividadesProjeto).build();
    }

    @POST
    @Path("removeOne")
    public Response removeOne(AtividadeProjeto atividade) {
        atividadeProjetoRepository.remove(atividadeProjetoRepository.findBy(atividade.getId()));
        return Response.ok().build();
    }
}
