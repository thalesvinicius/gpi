package br.com.gpi.server.endpoint;

import br.com.gpi.server.domain.entity.UsuarioExterno;
import br.com.gpi.server.domain.repository.UserExternalRepository;
import br.com.gpi.server.domain.service.UserService;
import br.com.gpi.server.to.UsuarioExternoTO;
import java.util.List;
import javax.annotation.security.PermitAll;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/userExternal")
@Stateless
@PermitAll
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
@TransactionManagement(TransactionManagementType.BEAN)
public class UsuarioExternoEndpoint {

    @Inject
    private UserService userService;
    
    @PathParam("idEmpresa")
    private Long idEmpresa;
    
    @GET
    @Path("/byEmpresa/{idEmpresa}")
    public Response findByFilters() {
        List<UsuarioExternoTO> result = userService.findUserExternalByFilters(idEmpresa);
        return Response.ok(result).build();
    }    
    
}
