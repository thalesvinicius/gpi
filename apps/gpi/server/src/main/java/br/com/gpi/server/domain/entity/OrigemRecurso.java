package br.com.gpi.server.domain.entity;

import br.com.gpi.server.domain.common.BaseEntity;
import com.fasterxml.jackson.annotation.JsonIgnore;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.envers.Audited;

@Audited
@Entity
@Table(name = "OrigemRecurso")
public class OrigemRecurso extends BaseEntity<Long> {
    
    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "financeiro_id")
    private Financeiro financeiro;
    
    @Column(columnDefinition = "bit")
    private Boolean origemNacional;
    
    @ManyToOne(cascade = {CascadeType.DETACH, CascadeType.REFRESH})
    @JoinColumn(name = "uf_id")
    private UF uf;
    
    @ManyToOne(cascade = {CascadeType.DETACH, CascadeType.REFRESH})
    @JoinColumn(name = "pais_id", nullable = true)
    private Pais pais;
    
    private Double percentualInvestimento;
    
    private Double valorInvestimento;

    public Financeiro getFinanceiro() {
        return financeiro;
    }

    public void setFinanceiro(Financeiro financeiro) {
        this.financeiro = financeiro;
    }

    public Boolean isOrigemNacional() {
        return origemNacional;
    }
    
    public Boolean getOrigemNacional(){return origemNacional;}

    public void setOrigemNacional(Boolean origemNacional) {
        this.origemNacional = origemNacional;
    }

    public UF getUf() {
        return uf;
    }

    public void setUf(UF uf) {
        this.uf = uf;
    }

    public Pais getPais() {
        return pais;
    }

    public void setPais(Pais pais) {
        this.pais = pais;
    }

    public Double getPercentualInvestimento() {
        return percentualInvestimento;
    }

    public void setPercentualInvestimento(Double percentualInvestimento) {
        this.percentualInvestimento = percentualInvestimento;
    }

    public Double getValorInvestimento() {
        return valorInvestimento;
    }

    public void setValorInvestimento(Double valorInvestimento) {
        this.valorInvestimento = valorInvestimento;
    }
    
}
