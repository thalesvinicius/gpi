package br.com.gpi.server.to;

import javax.enterprise.inject.Model;

@Model
public class AnexoResponse {

    private String identifier;
    private String filename;

    public AnexoResponse() {
    }

    public AnexoResponse(String identifier, String filename) {
        this.identifier = identifier;
        this.filename = filename;
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

}
