package br.com.gpi.server.domain.entity.enumerated;

import br.com.gpi.server.core.jpa.typesafe.EnumerationEntityType;


public enum EnumTipoPergunta  implements EnumerationEntityType<Integer> {
 
    SIM_OU_NAO(0, "Sim ou não"),
    AVALIATIVA(1, "Avaliativa");
    
    private final int id;
    private final String descricao;

    private EnumTipoPergunta(int id, String descricao) {
        this.id = id;
        this.descricao = descricao;
    }

    @Override
    public Integer getId() {
        return this.id;
    }

    @Override
    public String getDescription() {
        return this.descricao;
    }
    
}
