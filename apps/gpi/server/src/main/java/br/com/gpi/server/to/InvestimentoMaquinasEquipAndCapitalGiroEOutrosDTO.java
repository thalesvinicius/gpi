package br.com.gpi.server.to;

import br.com.gpi.server.domain.entity.enumerated.EnumTipoInvestimento;

public class InvestimentoMaquinasEquipAndCapitalGiroEOutrosDTO {

    private int ano;    
    private Long idMaquinasEquip;
    private Double valorMaquinasEquip;
    private Long idCapitalGiroEOutros;
    private Double valorCapitalGiroEOutros;

    public InvestimentoMaquinasEquipAndCapitalGiroEOutrosDTO() {
        
    }
    
    public InvestimentoMaquinasEquipAndCapitalGiroEOutrosDTO(int ano, Double valorMaquinasEquip, Double valorCapitalGiroEOutros) {
        this.ano = ano;
        this.valorMaquinasEquip = valorMaquinasEquip;
        this.valorCapitalGiroEOutros = valorCapitalGiroEOutros;        
    }
    
    public int getAno() {
        return ano;
    }

    public void setAno(int ano) {
        this.ano = ano;
    }

    public Double getValorMaquinasEquip() {
        return valorMaquinasEquip;
    }

    public void setValorMaquinasEquip(Double valorMaquinasEquip) {
        this.valorMaquinasEquip = valorMaquinasEquip;
    }

    public Double getValorCapitalGiroEOutros() {
        return valorCapitalGiroEOutros;
    }

    public void setValorCapitalGiroEOutros(Double valorCapitalGiroEOutros) {
        this.valorCapitalGiroEOutros = valorCapitalGiroEOutros;
    }       
    
    public Double getInvestimentoTotal() {
        return valorCapitalGiroEOutros + valorMaquinasEquip;
    }

    public EnumTipoInvestimento getTipoMaquinasEquip() {
        return EnumTipoInvestimento.INVESTIMENTO_MAQ_EQUIP_TERRENO_OBRAS;
    }

    public EnumTipoInvestimento getTipoCapitalGiroEOutros() {
        return EnumTipoInvestimento.INVESTIMENTO_CAPITAL_GIRO_E_OUTROS;
    }

    /**
     * @return the idMaquinasEquip
     */
    public Long getIdMaquinasEquip() {
        return idMaquinasEquip;
    }

    /**
     * @param idMaquinasEquip the idMaquinasEquip to set
     */
    public void setIdMaquinasEquip(Long idMaquinasEquip) {
        this.idMaquinasEquip = idMaquinasEquip;
    }

    /**
     * @return the idCapitalGiroEOutros
     */
    public Long getIdCapitalGiroEOutros() {
        return idCapitalGiroEOutros;
    }

    /**
     * @param idCapitalGiroEOutros the idCapitalGiroEOutros to set
     */
    public void setIdCapitalGiroEOutros(Long idCapitalGiroEOutros) {
        this.idCapitalGiroEOutros = idCapitalGiroEOutros;
    }

}
