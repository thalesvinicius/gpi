package br.com.gpi.server.domain.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import org.hibernate.envers.Audited;

@Audited
@Entity
@Table(name = "RelatorioAcompanhamentoSituacao")
@IdClass(RelatorioAcompanhamentoSituacaoPK.class)
public class RelatorioAcompanhamentoSituacao implements Serializable {
    
    @Id
    @Column(name = "relatorioAcompanhamento_id")
    private long relatorioAcompanhamentoId;
    
    @Id
    @Column(name = "situacaoRelatorio_id")
    private long situacaoRelatorioId;
    
    @Transient
    @ManyToOne
    @JoinColumn(name = "relatorioAcompanhamento_id", insertable = false, updatable = false)
    @JsonIgnore
    private RelatorioAcompanhamento relatorioAcompanhamento;

    @Transient
    @ManyToOne
    @JoinColumn(name = "situacaoRelatorio_id", insertable = false, updatable = false)
    private SituacaoRelatorioEnum situacaoRelatorio;

    public long getRelatorioAcompanhamentoId() {
        return relatorioAcompanhamentoId;
    }

    public void setRelatorioAcompanhamentoId(long relatorioAcompanhamentoId) {
        this.relatorioAcompanhamentoId = relatorioAcompanhamentoId;
    }

    public long getSituacaoRelatorioId() {
        return situacaoRelatorioId;
    }

    public void setSituacaoRelatorioId(long situacaoRelatorioId) {
        this.situacaoRelatorioId = situacaoRelatorioId;
    }

    public RelatorioAcompanhamento getRelatorioAcompanhamento() {
        return relatorioAcompanhamento;
    }

    public void setRelatorioAcompanhamento(RelatorioAcompanhamento relatorioAcompanhamento) {
        this.relatorioAcompanhamento = relatorioAcompanhamento;
    }

    public SituacaoRelatorioEnum getSituacaoRelatorio() {
        return situacaoRelatorio;
    }

    public void setSituacaoRelatorio(SituacaoRelatorioEnum situacaoRelatorio) {
        this.situacaoRelatorio = situacaoRelatorio;
    }
    
}
