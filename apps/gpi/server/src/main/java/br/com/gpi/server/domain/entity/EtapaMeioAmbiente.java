package br.com.gpi.server.domain.entity;

import br.com.gpi.server.core.jpa.converter.TipoMeioAmbienteEnumConverter;
import br.com.gpi.server.domain.common.BaseEntity;
import br.com.gpi.server.domain.entity.enumerated.EnumTipoMeioAmbiente;
import com.fasterxml.jackson.annotation.JsonIgnore;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.envers.Audited;

@Audited
@Entity
@Table(name = "EtapaMeioAmbiente")
public class EtapaMeioAmbiente extends BaseEntity<Long> {
    
    @ManyToOne
    @JsonIgnore
    @JoinColumn(name = "meioAmbiente_id")
    private MeioAmbiente meioAmbiente;

    @Column(name = "tipo", insertable = false, updatable = false)
    @Enumerated
    private EnumTipoMeioAmbiente tipoMeioAmbiente;
    
    @JsonIgnore
    @Column(name = "tipo")
    private Integer tipoMeioAmbienteInt;

    public MeioAmbiente getMeioAmbiente() {
        return meioAmbiente;
    }

    public void setMeioAmbiente(MeioAmbiente meioAmbiente) {
        this.meioAmbiente = meioAmbiente;
    }

    public EnumTipoMeioAmbiente getTipoMeioAmbiente() {
        return tipoMeioAmbiente;
    }

    public void setTipoMeioAmbiente(EnumTipoMeioAmbiente tipoMeioAmbiente) {
        if(tipoMeioAmbiente == null){
            return;
        }
        this.tipoMeioAmbiente = tipoMeioAmbiente;
        this.tipoMeioAmbienteInt = (new TipoMeioAmbienteEnumConverter()).convertToDatabaseColumn(tipoMeioAmbiente);
    }

    public Integer getTipoMeioAmbienteInt() {
        return tipoMeioAmbienteInt;
    }
    public void setTipoMeioAmbienteInt(Integer tipoMeioAmbienteInt) {
        if(tipoMeioAmbienteInt == null) {
            return;
        }
        this.tipoMeioAmbienteInt = tipoMeioAmbienteInt;
        this.tipoMeioAmbiente = (new TipoMeioAmbienteEnumConverter()).convertToEntityAttribute(tipoMeioAmbienteInt);
    }
    
}
