package br.com.gpi.server.domain.repository;

import br.com.gpi.server.domain.entity.UsuarioExterno;
import java.util.List;
import org.apache.deltaspike.data.api.EntityRepository;
import org.apache.deltaspike.data.api.Modifying;
import org.apache.deltaspike.data.api.Query;
import org.apache.deltaspike.data.api.Repository;
import org.apache.deltaspike.data.api.criteria.CriteriaSupport;

@Repository(forEntity = UsuarioExterno.class)
public abstract class UserExternalRepository implements CriteriaSupport<UsuarioExterno>, EntityRepository<UsuarioExterno, Long> {
    @Query(named = UsuarioExterno.Query.updateSituacaoByEmpresa)
    @Modifying
    public abstract void updateSituacaoByEmpresa(Boolean ativo, Long empresaId);
    
    @Query(named = UsuarioExterno.Query.locateByEmpresa)
    public abstract List<UsuarioExterno> locateByEmpresa(Long empresaId);
    
    @Query(named = UsuarioExterno.Query.locateByLogin)
    public abstract List<UsuarioExterno> locateByLogin(String login);
    
    @Query(named = UsuarioExterno.Query.locateByLogin2)
    public abstract List<UsuarioExterno> locateByLogin2(String login);    
    
    @Query(named = UsuarioExterno.Query.countByLoginEqualsAndIdNotEquals)
    public abstract Long countByLoginEqualsAndIdNotEquals(String email, Long id, Long empresaId, Boolean ativo);
    
}
