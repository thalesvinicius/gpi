package br.com.gpi.server.endpoint;

import br.com.gpi.server.core.message.AplicationMessages;
import br.com.gpi.server.domain.entity.AccessToken;
import br.com.gpi.server.domain.entity.Cargo;
import br.com.gpi.server.domain.entity.User;
import br.com.gpi.server.domain.entity.UsuarioExterno;
import br.com.gpi.server.domain.entity.UsuarioInterno;
import br.com.gpi.server.domain.repository.UserExternalRepository;
import br.com.gpi.server.domain.repository.UserInternalRepository;
import br.com.gpi.server.domain.repository.UserRepository;
import br.com.gpi.server.domain.service.DomainTableService;
import br.com.gpi.server.domain.service.SessionService;
import br.com.gpi.server.domain.service.UserService;
import br.com.gpi.server.domain.service.VerificationTokenService;
import br.com.gpi.server.domain.user.model.ChangePasswordRequest;
import br.com.gpi.server.domain.user.model.LostPasswordRequest;
import br.com.gpi.server.domain.user.model.SecurityUser;
import static java.lang.Long.compare;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.security.PermitAll;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.inject.Inject;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.core.UriInfo;

@Path("/user")
@Stateless
@PermitAll
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
@TransactionManagement(TransactionManagementType.BEAN)
public class UserEndpoint {

    @Inject
    private UserService userService;
    @Inject
    private VerificationTokenService verificationTokenService;
    @Inject
    private Logger logger;
    @Context
    private UriInfo uriInfo;
    @Inject
    private AplicationMessages messages;
    @Inject
    private UserRepository userRepository;
    @Inject
    private SessionService sessionService;
    @Inject
    private DomainTableService domainTableService;
    @Inject
    UserInternalRepository userInternalRepository;
    @Inject
    private UserTransaction userTransaction;
    @Inject
    UserExternalRepository userExternalRepository;

    @GET
    @Path("ping")
    @Produces(MediaType.TEXT_PLAIN)
    public String ping() {
        return "alive";
    }

    @POST
    @Path("ativar")
    @Produces(MediaType.TEXT_PLAIN)
    @PermitAll
    public Response activateUser(ChangePasswordRequest changePasswordRequest) {
        try {
            verificationTokenService.verify(changePasswordRequest.getToken(), changePasswordRequest.getEmail(), changePasswordRequest.getPassword());
            return Response.ok().build();
        } catch (Exception ex) {
            Logger.getLogger(UserEndpoint.class.getName()).log(Level.SEVERE, null, ex);
            throw new RuntimeException(ex);
        }
    }

    @POST
    @Path("reiniciarSenha")
    @Produces(MediaType.TEXT_PLAIN)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response resetPassword(ChangePasswordRequest changePasswordRequest) {
        try {
            verificationTokenService.resetPassword(changePasswordRequest);
            return Response.ok(messages.passwordChanged()).build();
        } catch (Exception ex) {
            Logger.getLogger(AuthenticationEndpoint.class.getName()).log(Level.SEVERE, null, ex);
            throw new RuntimeException(ex);
        }
    }
    
    @POST
    @Path("alterarSenha")
    @Produces(MediaType.TEXT_PLAIN)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response changePassword(@Context SecurityContext securityContext,ChangePasswordRequest changePasswordRequest) {
        try {
            User user = userService.loadUser(securityContext);
            user.setPassword(user.hashPassword(changePasswordRequest.getPassword()));
            userService.saveOrUpdate(user, (SecurityUser) securityContext.getUserPrincipal());
            return Response.ok(messages.passwordChanged()).build();
        } catch (Exception ex) {
            Logger.getLogger(AuthenticationEndpoint.class.getName()).log(Level.SEVERE, null, ex);
            throw new RuntimeException(ex);
        }        
    }

    @POST
    @Path("solicitarResetSenha")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    @PermitAll
    public Response lostPasswordRequest(LostPasswordRequest lostPasswordRequest) {
        lostPasswordRequest(Arrays.asList(new LostPasswordRequest[]{lostPasswordRequest}));
        return Response.ok(messages.passwordLostSended()).build();
    }
    
    @POST
    @Path("solicitarResetSenhas")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    @PermitAll
    public Response lostPasswordRequest(List<LostPasswordRequest> lostPasswordRequest) {
        try {
            verificationTokenService.sendLostPasswordToken(lostPasswordRequest);
            return Response.ok(messages.passwordLostSended()).build();
        } catch (SystemException ex) {
            Logger.getLogger(AuthenticationEndpoint.class.getName()).log(Level.SEVERE, null, ex);
            throw new RuntimeException(ex);
        }
    }

    @POST
    public Response saveOrUpdateUser(User user, @Context SecurityContext context) {
        SecurityUser loggedUserWrap = (SecurityUser) context.getUserPrincipal();

        if (user.getId() != null && (compare(user.getId(), loggedUserWrap.getId())) == 0) {
            user.setUsuarioResponsavel(null);
        } else {
            user.setUsuarioResponsavel(userRepository.findBy(loggedUserWrap.getId()));
        }

        user.setDataUltimaAlteracao(new Date());
        if (user.getId() == null) {
            user.setPassword(user.hashPassword("algar123"));
        }
        userRepository.save(user);
        return Response.ok(user).build();
    }

    @POST
    @Path("crudExternalUser")
    public Response createExternalUser(UsuarioExterno user, @Context SecurityContext context) {
        SecurityUser loggedUserWrap = (SecurityUser) context.getUserPrincipal();

        if (user.getId() != null) {
            userService.editExternalUser(user, loggedUserWrap);
        } else {
            userService.createExternalUser(user, loggedUserWrap);
        }
        return Response.ok(user).build();
    }
    
    @POST
    @Path("activateInactivateExternalUser")
    public Response activateInactivateExternalUser(UsuarioExterno user, @Context SecurityContext context) {
        SecurityUser loggedUserWrap = (SecurityUser) context.getUserPrincipal();
        return Response.ok(userService.activateInactivateExternalUser(user, loggedUserWrap)).build();
    }

    @POST
    @Path("removeUser")
    public Response removeUser(List<Long> ids) {
        try {
            userService.remove(ids);
            return Response.ok().build();
        } catch (Exception ex) {
            Logger.getLogger(UserEndpoint.class.getName()).log(Level.SEVERE, null, ex);
            throw new RuntimeException(ex);
        }
    }

    @GET
    @Path("/pesquisaLDAP/{matricula}")
    public Response getInfoLDAP(@PathParam("matricula") String matricula) {
        Hashtable env = new Hashtable(11);
        env.put(javax.naming.Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
        env.put(javax.naming.Context.PROVIDER_URL, "ldap://10.182.63.213:389");
        env.put(javax.naming.Context.SECURITY_AUTHENTICATION, "simple");
        env.put(javax.naming.Context.SECURITY_PRINCIPAL, "CN=GPI USER,OU=Usuarios,DC=INDI,DC=LOCAL");
        env.put(javax.naming.Context.SECURITY_CREDENTIALS, "Gp1.User");
        List<Object> result = new ArrayList<>();
        try {
            DirContext ctx = new InitialDirContext(env);
            String[] attrIDs = {"cn", "mail", "telephoneNumber", "title"};
            SearchControls ctls = new SearchControls();
            ctls.setSearchScope(SearchControls.SUBTREE_SCOPE);
            ctls.setReturningAttributes(attrIDs);

            NamingEnumeration enume = ctx.search("OU=Usuarios,DC=INDI,DC=LOCAL", "sAMAccountName=" + matricula, ctls);
            SearchResult entry = (SearchResult) enume.next();

            if (entry.getAttributes().size() > 0) {
                result.add((entry.getAttributes().get("cn") != null ? entry.getAttributes().get("cn").toString().substring(4, entry.getAttributes().get("cn").toString().length()) : ""));
                result.add((entry.getAttributes().get("mail") != null ? entry.getAttributes().get("mail").toString().substring(6, entry.getAttributes().get("mail").toString().length()) : ""));
                result.add((entry.getAttributes().get("telephoneNumber") != null ? entry.getAttributes().get("telephoneNumber").toString().substring(17, entry.getAttributes().get("telephoneNumber").toString().length()) : ""));
                result.add((entry.getAttributes().get("title") != null ? (Cargo) domainTableService.findByDescricao(Cargo.class, entry.getAttributes().get("title").toString().substring(7, entry.getAttributes().get("title").toString().length())) : ""));
            }
            ctx.close();
        } catch (NamingException e) {
            e.printStackTrace();
        }      
        return Response.ok(result).build();
    }
    
    @GET
    @Path("{id}")
    public User getUser(@PathParam("id") Long id) {
        return userRepository.findBy(id);
    }

    @GET
    @Path("/info")
    public Response getInfo(@Context SecurityContext context) {
        return Response.ok(context).build();
    }

    @GET
    @Path("/roles/{token}")
    public Response getRoles(@PathParam("token") String token) {
        AccessToken recoverSession = sessionService.recoverSession(token);
        return Response.ok(recoverSession.getUser().getRole()).build();
    }

    @GET
    public Response findByFilters(@QueryParam("nomeEmpresa") String nomeEmpresa,
            @QueryParam("matricula") String matricula,
            @QueryParam("nome") String nome,
            @QueryParam("grupoUsuario") Integer grupoUsuarioId,
            @QueryParam("departamento") Integer departamentoId,
            @QueryParam("cargo") Integer cargoId,
            @QueryParam("ativo") Boolean ativo) {
        List<UsuarioInterno> result = userService.findByFilters(matricula, nome, grupoUsuarioId,
                departamentoId, cargoId, ativo);
        return Response.ok(result).build();
    }

    @GET
    @Path("findAnalistasByDepartamentos")
    public Response findAnalistasByDepartamentos(@QueryParam("departamentos") List<Long> departamentosId) {
        List<UsuarioInterno> result = userService.findAnalistasByDepartamentos(departamentosId);
        return Response.ok(result).build();
    }

    @GET
    @Path("sendEmail/{email}")
    public Response sendEmailForUser(@PathParam("email") String email) {
        try {
            userTransaction.begin();
            verificationTokenService.generateEmailVerificationToken(email);
            userTransaction.commit();
            return Response.ok().build();
        } catch (NotSupportedException | SystemException | RollbackException | HeuristicMixedException | HeuristicRollbackException | SecurityException | IllegalStateException ex) {
            throw new RuntimeException(ex);
        }
    }

    @GET
    @Path("checkMatricula")
    public Response locateUserByMatricula(@QueryParam("matriculaId") String matricula) {
        List<UsuarioInterno> usuario = userInternalRepository.LocateByLogin(matricula);
        return Response.ok(usuario).build();
    }

    @GET
    @Path("byDepartamentos")
    public Response locateUserByDepartamentos(@QueryParam("departamentos") List<Long> departamentos) {
        List<UsuarioInterno> usuario = userInternalRepository.LocateByDepartamento(departamentos);
        return Response.ok(usuario).build();
    }

    @GET
    @Path("findByEmpresa/{empresaId}")
    public Response findByEmpresa(@PathParam("empresaId") Long empresaId) {
        List<UsuarioExterno> usuarios = userExternalRepository.locateByEmpresa(empresaId);
        return Response.ok(usuarios).build();
    }
}
