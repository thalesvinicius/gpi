package br.com.gpi.server.domain.entity;

import java.io.Serializable;
import java.util.Objects;


public class PainelReuniaoInstrumentosAnoPK implements Serializable{
    private Long diretoriaId;
    private Long gerenciaId;        

    /**
     * @return the diretoriaId
     */
    public Long getDiretoriaId() {
        return diretoriaId;
    }

    /**
     * @param diretoriaId the diretoriaId to set
     */
    public void setDiretoriaId(Long diretoriaId) {
        this.diretoriaId = diretoriaId;
    }

    /**
     * @return the gerenciaId
     */
    public Long getGerenciaId() {
        return gerenciaId;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 83 * hash + Objects.hashCode(this.diretoriaId);
        hash = 83 * hash + Objects.hashCode(this.gerenciaId);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final PainelReuniaoInstrumentosAnoPK other = (PainelReuniaoInstrumentosAnoPK) obj;
        if (!Objects.equals(this.diretoriaId, other.diretoriaId)) {
            return false;
        }
        if (!Objects.equals(this.gerenciaId, other.gerenciaId)) {
            return false;
        }
        return true;
    }

    /**
     * @param gerenciaId the gerenciaId to set
     */
    public void setGerenciaId(Long gerenciaId) {
        this.gerenciaId = gerenciaId;
    }
    
    
}
