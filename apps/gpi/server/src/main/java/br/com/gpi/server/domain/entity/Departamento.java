package br.com.gpi.server.domain.entity;

import br.com.gpi.server.domain.common.BaseDomainTableEntity;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import org.hibernate.envers.Audited;

@Audited
@Entity
@Table(name = "Departamento")
@Cacheable
@NamedQueries({
    @NamedQuery(name = Departamento.Query.findAllByDepartamentoSuperiorIsNull, query = "SELECT d FROM Departamento d WHERE d.departamentoSuperior IS NULL"),
    @NamedQuery(name = Departamento.Query.findAllByDepartamentoSuperiorId, query = "SELECT d FROM Departamento d WHERE d.departamentoSuperior.id = ?1")    
})
public class Departamento extends BaseDomainTableEntity {

    @Column(name = "descricao", length = 255)
    private String descricao;

    @Column(name = "sigla", length = 50)
    private String sigla;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "departamentoSuperior_id")
    private Departamento departamentoSuperior;

    @Override
    public String getDescricao() {
        return this.descricao;
    }

    @Override
    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Departamento getDepartamentoSuperior() {
        return departamentoSuperior;
    }

    public void setDepartamentoSuperior(Departamento departamentoSuperior) {
        this.departamentoSuperior = departamentoSuperior;
    }

    public String getSigla() {
        return sigla;
    }

    public void setSigla(String sigla) {
        this.sigla = sigla;
    }

    public static class Query {
        public static final String findAllByDepartamentoSuperiorIsNull = "Departamento.findAllByDepartamentoSuperiorIsNull";   
        public static final String findAllByDepartamentoSuperiorId= "Departamento.findAllByDepartamentoSuperiorId";
    }
}
