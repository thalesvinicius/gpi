package br.com.gpi.server.domain.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;
@MappedSuperclass
@IdClass(RelatorioAgendaPositivaPk.class)
public abstract class RelatorioAgendaPositivaAbstract implements Serializable{
    public static enum RelatorioAgendaPositivaEnum {
        DF("DF", "RelatorioAgendaPositivaDF", RelatorioAgendaPositivaDF.class), II("II", "RelatorioAgendaPositivaII", RelatorioAgendaPositivaII.class), OI("OI", "RelatorioAgendaPositivaOI", RelatorioAgendaPositivaOI.class);
        
        private final String siglaEstagio;
        private final String descricaoClasse;
        private final Class clazz;

        private RelatorioAgendaPositivaEnum(String siglaEstagio, String descricaoClasse, Class clazz) {
            this.siglaEstagio = siglaEstagio;
            this.descricaoClasse = descricaoClasse;
            this.clazz = clazz;
        }
        
        public String getSiglaEstagio() {
            return siglaEstagio;
        }

        public String getDescricaoClasse() {
            return descricaoClasse;
        }

        public Class getClazz() {
            return clazz;
        }
    }
    
    @Id
    @Column(name = "id")
    private Long projetoId; 
    
    @Id
    private String estagioPrevisto;
    
    @Id
    private Boolean isLastVersion;

    @Column(name = "projeto")
    private String nomeProjeto;

    @Column(name = "versao")
    private Integer versao;

    @Column(name = "familia_id")
    private Long versaoPai;

    @Column(name = "estagioAtual_id")
    private Estagio estagioAtual_id;

    @Transient
    private String estagioAtual;

    @Column(name = "ultimaVersao")
    private Boolean ultimaVersao;

    @Column(name = "NomeEmpresa")
    private String nomeEmpresa;

    @Column(name = "UsuarioResponsavel")
    private String usuarioResponsavel;

    @Column(name = "UsuarioResponsavel_id")
    private Long usuarioResponsavel_id;
    
    @Column(name = "Departamento")
    private String departamento;

    @Column(name = "departamento_id")
    private Long departamento_id;
    
    @Column(name = "RegiaoPlanejamento")
    private String regiaoPlanejamento;

    @Column(name = "RegiaoPlanejamento_id")
    private Long regiaoPlanejamento_id;

    @Column(name = "CadeiaProdutiva" )
    private String cadeiaProdutivaDesc;

    @Column(name = "cadeiaprodutiva_id")
    private Long cadeiaProdutiva;
    
    @Column(name = "municipio")
    private String municipio;

    @Column(name = "dt_previsao")
    private Date dataPrevisao;

    @Column(name = "dt_real")
    private Date dataReal;

    @Column(name = "totalFaturamentoAnterior")
    private Double totalFaturamentoAnterior;

    @Column(name = "totalFaturamentoPrevisto")
    private Double totalFaturamentoPrevisto;

    @Column(name = "totalInvestimentoPrevisto")
    private Double totalInvestimentoPrevisto;

    @Column(name = "totalEmpregoDireto")
    private Integer totalEmpregoDireto;

    @Column(name = "totalEmpregoIndireto")
    private Integer totalEmpregoIndireto;

    @Column(name = "departamentoSuperior_id")
    private Integer diretoriaId;    
    
    public String getNomeProjeto() {
        return nomeProjeto;
    }

    public void setNomeProjeto(String nomeProjeto) {
        this.nomeProjeto = nomeProjeto;
    }

    public Integer getVersao() {
        return versao;
    }

    public void setVersao(Integer versao) {
        this.versao = versao;
    }

    public Long getVersaoPai() {
        return versaoPai;
    }

    public void setVersaoPai(Long versaoPai) {
        this.versaoPai = versaoPai;
    }

    public Estagio getEstagioAtual_id() {
        return estagioAtual_id;
    }

    public void setEstagioAtual_id(Estagio estagioAtual_id) {
        this.estagioAtual_id = estagioAtual_id;
    }

    public String getEstagioAtual() {
        return (estagioAtual_id != null ? estagioAtual_id.getDescricao() : ""); 
    }

    public void setEstagioAtual(String estagioAtual) {
        this.estagioAtual = estagioAtual;
    }

    public String getCadeiaProdutivaDesc() {
        return cadeiaProdutivaDesc;
    }

    public void setCadeiaProdutivaDesc(String cadeiaProdutivaDesc) {
        this.cadeiaProdutivaDesc = cadeiaProdutivaDesc;
    }

    public Boolean isUltimaVersao() {
        return ultimaVersao;
    }

    public void setUltimaVersao(Boolean ultimaVersao) {
        this.ultimaVersao = ultimaVersao;
    }

    public String getNomeEmpresa() {
        return nomeEmpresa;
    }

    public void setNomeEmpresa(String nomeEmpresa) {
        this.nomeEmpresa = nomeEmpresa;
    }

    public String getUsuarioResponsavel() {
        return usuarioResponsavel;
    }

    public void setUsuarioResponsavel(String usuarioResponsavel) {
        this.usuarioResponsavel = usuarioResponsavel;
    }

    public String getDepartamento() {
        return departamento;
    }

    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    public String getRegiaoPlanejamento() {
        return regiaoPlanejamento;
    }

    public void setRegiaoPlanejamento(String regiaoPlanejamento) {
        this.regiaoPlanejamento = regiaoPlanejamento;
    }

    public Long getRegiaoPlanejamento_id() {
        return regiaoPlanejamento_id;
    }

    public void setRegiaoPlanejamento_id(Long regiaoPlanejamento_id) {
        this.regiaoPlanejamento_id = regiaoPlanejamento_id;
    }

    public Long getCadeiaProdutiva() {
        return cadeiaProdutiva;
    }

    public void setCadeiaProdutiva(Long cadeiaProdutiva) {
        this.cadeiaProdutiva = cadeiaProdutiva;
    }

    public String getMunicipio() {
        return municipio;
    }

    public void setMunicipio(String municipio) {
        this.municipio = municipio;
    }

    public Date getDataPrevisao() {
        return dataPrevisao;
    }

    public void setDataPrevisao(Date dataPrevisao) {
        this.dataPrevisao = dataPrevisao;
    }

    public Date getDataReal() {
        return dataReal;
    }

    public void setDataReal(Date dataReal) {
        this.dataReal = dataReal;
    }

    public Double getTotalFaturamentoAnterior() {
        return totalFaturamentoAnterior == null ? 0D : totalFaturamentoAnterior;
    }

    public void setTotalFaturamentoAnterior(Double totalFaturamentoAnterior) {
        this.totalFaturamentoAnterior = totalFaturamentoAnterior;
    }

    public Double getTotalFaturamentoPrevisto() {
        return totalFaturamentoPrevisto == null ? 0D : totalFaturamentoPrevisto;
    }

    public void setTotalFaturamentoPrevisto(Double totalFaturamentoPrevisto) {
        this.totalFaturamentoPrevisto = totalFaturamentoPrevisto;
    }

    public Double getTotalInvestimentoPrevisto() {
        return totalInvestimentoPrevisto == null ? 0D : totalInvestimentoPrevisto;
    }

    public void setTotalInvestimentoPrevisto(Double totalInvestimentoPrevisto) {
        this.totalInvestimentoPrevisto = totalInvestimentoPrevisto;
    }

    public Integer getTotalEmpregoDireto() {
        return totalEmpregoDireto == null ? 0 : totalEmpregoDireto;
    }

    public void setTotalEmpregoDireto(Integer totalEmpregoDireto) {
        this.totalEmpregoDireto = totalEmpregoDireto;
    }

    public Integer getTotalEmpregoIndireto() {
        return totalEmpregoIndireto == null ? 0 : totalEmpregoIndireto;
    }

    public void setTotalEmpregoIndireto(Integer totalEmpregoIndireto) {
        this.totalEmpregoIndireto = totalEmpregoIndireto;
    }

    public String getEstagioPrevisto() {
        return estagioPrevisto;
    }

    public void setEstagioPrevisto(String estagioPrevisto) {
        this.estagioPrevisto = estagioPrevisto;
    }

    public Long getDepartamento_id() {
        return departamento_id;
    }

    public void setDepartamento_id(Long departamento_id) {
        this.departamento_id = departamento_id;
    }

    public Long getUsuarioResponsavel_id() {
        return usuarioResponsavel_id;
    }

    public void setUsuarioResponsavel_id(Long usuarioResponsavel_id) {
        this.usuarioResponsavel_id = usuarioResponsavel_id;
    }

    public Integer getDiretoriaId() {
        return diretoriaId;
    }

    public void setDiretoriaId(Integer diretoriaId) {
        this.diretoriaId = diretoriaId;
    }

    public Boolean getIsLastVersion() {
        return isLastVersion;
    }

    public void setIsLastVersion(Boolean isLastVersion) {
        this.isLastVersion = isLastVersion;
    }

    public Long getProjetoId() {
        return projetoId;
    }

    public void setProjetoId(Long projetoId) {
        this.projetoId = projetoId;
    }

}
