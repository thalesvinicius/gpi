package br.com.gpi.server.core.jpa.converter;

import br.com.gpi.server.domain.entity.OrigemEmpresaEnum;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = false)
public class OrigemEmpresaEnumConverter implements AttributeConverter<OrigemEmpresaEnum, Integer> {

    @Override
    public Integer convertToDatabaseColumn(OrigemEmpresaEnum situacao) {
        return situacao.getId();
    }

    @Override
    public OrigemEmpresaEnum convertToEntityAttribute(Integer id) {
        return OrigemEmpresaEnum.getOrigemEmpresa(id);
    }

}
