package br.com.gpi.server.endpoint;

import br.com.gpi.server.domain.entity.Infraestrutura;
import br.com.gpi.server.domain.repository.InfraestruturaRepository;
import br.com.gpi.server.domain.service.InfraestruturaService;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.inject.Inject;
import javax.transaction.SystemException;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

@Path("/projeto/{idProjeto}/infraestrutura")
@Stateless
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
@TransactionManagement(TransactionManagementType.BEAN)
public class InfraestruturaEndpoint {

    @Inject
    private InfraestruturaService infraestruturaService;
    
    @Inject
    private InfraestruturaRepository infraestruturaRepository;

    @PathParam("idProjeto")
    private Long idProjeto;

    @POST
    public Response saveOrUpdate(Infraestrutura infraestrutura, @Context SecurityContext context) {
        try {
            return Response.ok(infraestruturaService.saveOrUpdate(infraestrutura, context, idProjeto)).build();
        } catch (Exception ex) {
            Logger.getLogger(InfraestruturaEndpoint.class.getName()).log(Level.SEVERE, null, ex);
            throw new RuntimeException(ex);
        }
    }

    @GET
    @Path("{id}")
    public Infraestrutura getInfraestrutura(@PathParam("id") Long id) {
        return infraestruturaRepository.findBy(id);
    }

    @GET
    public Response findAllInfraestruturaByIdProjeto() {
        List<Infraestrutura> infraestruturas = infraestruturaRepository.locateByProjetoId(idProjeto);
        return Response.ok(infraestruturas).build();
    }

    @GET
    @Path("findChange")
    public Response findChange() {
        Object infraEstrutura = null;
        try {
            infraEstrutura = infraestruturaService.verifyChange(idProjeto);
        } catch (SystemException ex) {
            Logger.getLogger(InfraestruturaEndpoint.class.getName()).log(Level.SEVERE, null, ex);
        }
        return Response.ok(infraEstrutura).build();
    }
}
