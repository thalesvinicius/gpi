package br.com.gpi.server.endpoint;

import br.com.gpi.server.domain.common.DomainTableEntityMapper;
import br.com.gpi.server.domain.service.DomainTableService;
import java.util.List;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/domain")
@Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
public class DomainTablesEndpoint {

    @Inject
    private DomainTableService domainTableService;
    @Inject
    private DomainTableEntityMapper domainTableEntityMapper;

    public DomainTablesEndpoint() {
    }

    @GET
    @Path("{entity}")
    public Response getDomainModelTable(@PathParam("entity") String entity) {
        Class classOfEntity = domainTableEntityMapper.getClassOfEntity(entity);
        if (classOfEntity.isAssignableFrom(Enum.class)){
            return Response.ok( ).build();
        }
        List<?> findAll = domainTableService.findAll(classOfEntity);
        return Response.ok(findAll).build();
    }
    
    @GET
    @Path("{entity}/{orderby}/{direction}")
    public Response getDomainModelTableOrdered(@PathParam("entity") String entity, @PathParam("orderby") String orderBy, @PathParam("direction") String direction) {
        Class classOfEntity = domainTableEntityMapper.getClassOfEntity(entity);
        if (classOfEntity.isAssignableFrom(Enum.class)){
            return Response.ok( ).build();
        }
        List<?> findAll = domainTableService.findAll(classOfEntity, orderBy, direction);
        return Response.ok(findAll).build();
    }

}
