package br.com.gpi.server.domain.service;

import br.com.gpi.server.domain.common.BaseService;
import br.com.gpi.server.domain.entity.AnexoLocalizacao;
import br.com.gpi.server.domain.entity.Estagio;
import br.com.gpi.server.domain.entity.Localizacao;
import br.com.gpi.server.domain.entity.MicroRegiao;
import br.com.gpi.server.domain.entity.MicroRegiao_;
import br.com.gpi.server.domain.entity.Municipio;
import br.com.gpi.server.domain.entity.Municipio_;
import br.com.gpi.server.domain.entity.Projeto;
import br.com.gpi.server.domain.entity.RegiaoPlanejamento;
import br.com.gpi.server.domain.entity.RegiaoPlanejamento_;
import br.com.gpi.server.domain.entity.SituacaoProjetoEnum;
import br.com.gpi.server.domain.entity.User;
import br.com.gpi.server.domain.entity.UsuarioExterno;
import br.com.gpi.server.domain.repository.LocalizacaoRepository;
import br.com.gpi.server.domain.user.model.SecurityUser;
import br.com.gpi.server.to.LocalizacaoEmpreendimentoDTO;
import br.com.gpi.server.util.LogUtil;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;
import javax.ws.rs.core.SecurityContext;

public class LocalizacaoService extends BaseService<Localizacao> {

    @Inject
    private LocalizacaoRepository localizacaoRepository;
    @Inject
    private ProjetoService projetoService;

    @Inject
    private UserTransaction transaction;

    @Inject
    public LocalizacaoService(EntityManager em) {
        super(Localizacao.class, em);
    }

    public List<LocalizacaoEmpreendimentoDTO> findLocalizacoesEmpreendimentos(String strParam) {

        List<LocalizacaoEmpreendimentoDTO> result = new ArrayList<>();

        CriteriaQuery criteriaQueryMunicipio = doQueryMunicipio(strParam);
        result.addAll(getEm().createQuery(criteriaQueryMunicipio).getResultList());

        CriteriaQuery criteriaQueryMicroRegiao = doQueryMicroRegiao(strParam);
        result.addAll(getEm().createQuery(criteriaQueryMicroRegiao).getResultList());

        CriteriaQuery criteriaQueryRegiaoPlanejamento = doQueryRegiaoPlanejamento(strParam);
        result.addAll(getEm().createQuery(criteriaQueryRegiaoPlanejamento).getResultList());

        return result;
    }

    private CriteriaQuery doQueryRegiaoPlanejamento(String strParam) {
        CriteriaBuilder builder = getEm().getCriteriaBuilder();
        CriteriaQuery<LocalizacaoEmpreendimentoDTO> queryRegiaoPlanejamento = builder.createQuery(LocalizacaoEmpreendimentoDTO.class);
        Root<RegiaoPlanejamento> fromRegiaoPlanejamento = queryRegiaoPlanejamento.from(RegiaoPlanejamento.class);
        List<Predicate> conditions = new ArrayList();
        if (strParam != null && !strParam.isEmpty()) {
            conditions.add(builder.like(builder.lower(fromRegiaoPlanejamento.get(RegiaoPlanejamento_.nome)), "%" + strParam.toLowerCase() + "%"));
        }
        CriteriaQuery criteriaQueryRegiaoPlanejamento = queryRegiaoPlanejamento.multiselect(
                fromRegiaoPlanejamento
        ).where(conditions.toArray(new Predicate[]{}))
                .distinct(true);
        return criteriaQueryRegiaoPlanejamento;
    }

    private CriteriaQuery doQueryMicroRegiao(String strParam) {
        CriteriaBuilder builder = getEm().getCriteriaBuilder();
        CriteriaQuery<LocalizacaoEmpreendimentoDTO> queryMicroRegiao = builder.createQuery(LocalizacaoEmpreendimentoDTO.class);
        Root<MicroRegiao> fromMicroRegiao = queryMicroRegiao.from(MicroRegiao.class);
        List<Predicate> conditions = new ArrayList();
        if (strParam != null && !strParam.isEmpty()) {
            conditions.add(builder.like(builder.lower(fromMicroRegiao.get(MicroRegiao_.nome)), "%" + strParam.toLowerCase() + "%"));
        }
        CriteriaQuery criteriaQueryMicroRegiao = queryMicroRegiao.multiselect(
                fromMicroRegiao.get(MicroRegiao_.regiaoPlanejamento),
                fromMicroRegiao
        ).where(conditions.toArray(new Predicate[]{}))
                .distinct(true);
        return criteriaQueryMicroRegiao;
    }

    private CriteriaQuery doQueryMunicipio(String strParam) {
        CriteriaBuilder builder = getEm().getCriteriaBuilder();
        CriteriaQuery<LocalizacaoEmpreendimentoDTO> queryMunicipio = builder.createQuery(LocalizacaoEmpreendimentoDTO.class);
        Root<Municipio> fromMunicipio = queryMunicipio.from(Municipio.class);
        List<Predicate> conditions = new ArrayList();
        if (strParam != null && !strParam.isEmpty()) {
            conditions.add(builder.like(builder.lower(fromMunicipio.get(Municipio_.nome)), "%" + strParam.toLowerCase() + "%"));
        }
        CriteriaQuery criteriaQueryMunicipio = queryMunicipio.multiselect(
                fromMunicipio.get(Municipio_.regiaoPlanejamento),
                fromMunicipio.get(Municipio_.microRegiao),
                fromMunicipio
        ).where(conditions.toArray(new Predicate[]{}))
                .distinct(true);
        return criteriaQueryMunicipio;
    }

    public Localizacao saveOrUpdateLocalizacao(Localizacao localizacao, SecurityContext securityContext, Long idProjeto) throws SystemException {
        try {
            transaction.begin();
            this.getEm().joinTransaction();

            SecurityUser loggedUserWrapper = (SecurityUser) securityContext.getUserPrincipal();
            User loggedUser = this.getEm().find(User.class, loggedUserWrapper.getId());
            localizacao.setUsuarioResponsavel(loggedUser);
            localizacao.setDataUltimaAlteracao(new Date());

            if (!localizacao.getaDefinir()) {
                if (localizacao.getMunicipio() != null) {
                    Municipio municipio = this.getEm().find(Municipio.class, localizacao.getMunicipio().getId());
                    localizacao.setMunicipio(municipio);
                    localizacao.setMicroRegiao(municipio.getMicroRegiao());
                    localizacao.setRegiaoPlanejamento(municipio.getRegiaoPlanejamento());
                } else if (localizacao.getMicroRegiao() != null) {
                    MicroRegiao microRegiao = this.getEm().find(MicroRegiao.class, localizacao.getMicroRegiao().getId());
                    localizacao.setMicroRegiao(microRegiao);
                    localizacao.setRegiaoPlanejamento(microRegiao.getRegiaoPlanejamento());
                } else if (localizacao.getRegiaoPlanejamento() != null) {
                    RegiaoPlanejamento regiaoPlanejamento = this.getEm().find(RegiaoPlanejamento.class, localizacao.getRegiaoPlanejamento().getId());
                    localizacao.setRegiaoPlanejamento(regiaoPlanejamento);
                }
            }

            if (localizacao.getAnexosLocalizacao() != null && !localizacao.getAnexosLocalizacao().isEmpty()) {
                for(AnexoLocalizacao anexo : localizacao.getAnexosLocalizacao()){
                    anexo.setLocalizacao(localizacao);
                }
            }

            Projeto projeto = new Projeto();
            projeto.setId(idProjeto);
            localizacao.setProjeto(projeto);

            //Altera a situação do projeto se o estágio for INICIO_PROJETO.
            if (projetoService.findEstagioAtualById(idProjeto).getEstagio() == Estagio.INICIO_PROJETO) {
                if(loggedUser instanceof UsuarioExterno) {
                    projetoService.updateSituacaoProjetoTransactional(SituacaoProjetoEnum.PENDENTE_ACEITE, idProjeto, loggedUser.getId(), new Date());
                } else {
                    projetoService.updateSituacaoProjetoTransactional(SituacaoProjetoEnum.PEDENTE_VALIDACAO, idProjeto, loggedUser.getId(), new Date());
                    
                }
            }
            
            localizacao = (Localizacao) this.merge(localizacao);
            
            transaction.commit();
            return localizacao;
        } catch (Exception ex) {
            transaction.rollback();
            Logger.getLogger(ProjetoService.class.getName()).log(Level.SEVERE, null, ex);
            throw new RuntimeException(ex);
        }
    }
    
    public Object verifyChange(Long idProjeto) throws SystemException {
        String atributos = "latitude_MOD, longetude_MOD, anexosLocalizacao_MOD, "
                + "aDefinir_MOD, localizacaoSecundaria_MOD, "
                + "microRegiao_MOD, municipio_MOD, projeto_MOD, "
                + "regiaoPlanejamento_MOD ";
        Object result = this.getEm().createNativeQuery(
                LogUtil.montaConsultaLog("LogLocalizacao", atributos, idProjeto).toString()).getResultList();

        return result;
    }

}
