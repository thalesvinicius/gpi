package br.com.gpi.server.domain.entity;

import br.com.gpi.server.core.exception.EnumException;
import br.com.gpi.server.core.jpa.typesafe.EnumerationEntityType;

public enum SituacaoEmpresa implements EnumerationEntityType<Integer> {

   INVALID(0, "invalid"),  ATIVA(1, "Ativa"), CADASTRO_ARQUIVADO(2, "Cadastro Arquivado"), PEDENTE_VALIDACAO(3, "Pendente de Validação") , PEDENTE_APROVACAO(4, "Pendente de Aprovação");

    private SituacaoEmpresa(int id, String descricao) {
        this.id = id;
        this.descricao = descricao;
    }

    private final int id;
    private final String descricao;

    @Override
    public Integer getId() {
        return this.id;
    }

    @Override
    public String getDescription() {
        return this.descricao;
    }

    public static SituacaoEmpresa getSituacaoEmpresa(Integer id) {
        for (SituacaoEmpresa situacaoEmpresa : values()) {
            if (situacaoEmpresa.id == id) {
                return situacaoEmpresa;
            }
        }
        throw new EnumException("SituacaoEmpresa couldn't load.");
    }
}