package br.com.gpi.server.endpoint;

import br.com.gpi.server.domain.common.QueryParameter;
import br.com.gpi.server.domain.entity.Estagio;
import br.com.gpi.server.domain.entity.SituacaoInstrFormalizacao;
import br.com.gpi.server.domain.entity.SituacaoProjetoEnum;
import br.com.gpi.server.domain.entity.TipoInstrFormalizacao;
import br.com.gpi.server.domain.service.reports.gerencial.PainelReuniaoGerencialService;
import java.util.List;
import javax.annotation.security.PermitAll;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("report/painelreuniao")
@Stateless
@PermitAll
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
@TransactionManagement(TransactionManagementType.BEAN)
public class PainelReuniaoGerencialEndpoint {

    @Inject
    PainelReuniaoGerencialService painelReuniaoGerencialService;

    @GET
    @Path("statusreport")
    public Response statusReport(@QueryParam("estagios") List<Estagio> estagios,
            @QueryParam("regioesPlanejamento") List<Long> regioesPlanejamento,
            @QueryParam("departamentos") List<Long> departamentos,
            @QueryParam("mesAno") Long mesAno,
            @QueryParam("dataInicio") Long dataInicio,
            @QueryParam("dataFim") Long dataFim,
            @QueryParam("cadeiasProdutivas") List<Long> cadeiasProdutivas,
            @QueryParam("prospeccaoAtiva") Boolean prospeccaoAtiva,
            @QueryParam("diretoria") Long diretoria) {

        QueryParameter queryParam = QueryParameter.with("estagios", estagios)
                .and("regioesPlanejamento", regioesPlanejamento)
                .and("gerencias", departamentos)
                .and("mesAno", mesAno)
                .and("dataInicio", dataInicio)
                .and("dataFim", dataFim)
                .and("diretoria", diretoria)
                .and("cadeiasProdutivas", cadeiasProdutivas)
                .and("prospeccaoAtiva", prospeccaoAtiva);

        return Response.ok(painelReuniaoGerencialService.findProjetosByFilters(queryParam)).build();
    }

    @GET
    @Path("projetosemdf")
    public Response projetosEmDF(@QueryParam("diretoria") Long diretoria,
            @QueryParam("regioesPlanejamento") List<Long> regioesPlanejamento,
            @QueryParam("departamentos") List<Long> departamentos,
            @QueryParam("mesAno") Long mesAno,
            @QueryParam("cadeiasProdutivas") List<Long> cadeiasProdutivas,
            @QueryParam("prospeccaoAtiva") Boolean prospeccaoAtiva,
            @QueryParam("dataInicio") Long dataInicio,
            @QueryParam("dataFim") Long dataFim) {

        QueryParameter queryParam = QueryParameter.with("estagioId", Estagio.FORMALIZADA.getId())
                .and("regioesPlanejamento", regioesPlanejamento)
                .and("gerencias", departamentos)
                .and("dataInicio", dataInicio)
                .and("dataFim", dataFim)
                .and("cadeiasProdutivas", cadeiasProdutivas)
                .and("diretoria", diretoria)
                .and("prospeccaoAtiva", prospeccaoAtiva);

        return Response.ok(painelReuniaoGerencialService.findProjetosByFilters(queryParam)).build();
    }

    @GET
    @Path("protocoloscancelados")
    public Response protocolosCancelados(@QueryParam("diretoria") Long diretoria,
            @QueryParam("mesAno") Long mesAno,
            @QueryParam("regioesPlanejamento") List<Long> regioesPlanejamento,
            @QueryParam("departamentos") List<Long> departamentos,
            @QueryParam("cadeiasProdutivas") List<Long> cadeiasProdutivas,
            @QueryParam("anual") Boolean anual) {

        QueryParameter queryParam = QueryParameter.with("mesAno", mesAno)
                .and("regioesPlanejamento", regioesPlanejamento)
                .and("gerencias", departamentos)
                .and("mesAno", mesAno)
                .and("cadeiasProdutivas", cadeiasProdutivas)
                .and("anual", anual)
                .and("diretoria", diretoria)
                .and("situacaoInstrumentoId", SituacaoInstrFormalizacao.CANCELADO.getId())
                .and("tipoInstrumentoId", TipoInstrFormalizacao.PROTOCOLO_INTENCAO.getId());

        return Response.ok(painelReuniaoGerencialService.findProtocolosByFilters(queryParam)).build();
    }

    @GET
    @Path("projetoscancelados")
    public Response projetosCancelados(@QueryParam("diretoria") Long diretoria,
            @QueryParam("mesAno") Long mesAno,
            @QueryParam("regioesPlanejamento") List<Long> regioesPlanejamento,
            @QueryParam("departamentos") List<Long> departamentos,
            @QueryParam("cadeiasProdutivas") List<Long> cadeiasProdutivas,
            @QueryParam("anual") Boolean anual) {

        QueryParameter queryParam = QueryParameter.with("mesAno", mesAno)
                .and("regioesPlanejamento", regioesPlanejamento)
                .and("gerencias", departamentos)
                .and("mesAno", mesAno)
                .and("cadeiasProdutivas", cadeiasProdutivas)
                .and("anual", anual)
                .and("diretoria", diretoria)
                .and("situacaoProjetoId", SituacaoProjetoEnum.CANCELADO.getId());

        return Response.ok(painelReuniaoGerencialService.findProjetosByFilters(queryParam)).build();
    }

    @GET
    @Path("previsaoultrapassada")
    public Response protocolosPrevisaoUltrapassada(@QueryParam("diretoria") Long diretoria,
            @QueryParam("mesAno") Long mesAno) {
        QueryParameter queryParam = QueryParameter.with("diretoria", diretoria)
                .and("mesAno", mesAno)
                .and("situacaoInstrumentoId", SituacaoInstrFormalizacao.EM_NEGOCIACAO.getId())
                .and("tipoInstrumentoId", TipoInstrFormalizacao.PROTOCOLO_INTENCAO.getId());
        return Response.ok(painelReuniaoGerencialService.findProtocolosByFilters(queryParam)).build();
    }

    @GET
    @Path("contatoinicial")
    public Response projetosContatoInicial(@QueryParam("diretoria") Long diretoria,
            @QueryParam("mesAno") Long mesAno) {
        QueryParameter queryParam = QueryParameter.with("diretoria", diretoria)
                .and("mesAno", mesAno)
                .and("situacaoProjetoId", SituacaoProjetoEnum.ATIVO.getId())                                
                .and("estagioProjetoId", Estagio.INICIO_PROJETO.getId());
        return Response.ok(painelReuniaoGerencialService.findProjetosByFilters(queryParam)).build();
    }

    @GET
    @Path("pesquisasrecebidas")
    public Response pesquisasRecebidas(@QueryParam("diretoria") Long diretoria,
            @QueryParam("mesAno") Long mesAno,
            @QueryParam("anual") Boolean anual,
            @QueryParam("departamentos") List<Long> departamentos) {
        QueryParameter queryParam = QueryParameter.with("diretoria", diretoria)
                .and("mesAno", mesAno)
                .and("diretoria", diretoria)
                .and("gerencias", departamentos)
                .and("anual", anual);
        return Response.ok(painelReuniaoGerencialService.findPesquisasRecebidasByFilters(queryParam)).build();
    }

    @GET
    @Path("indicesatisfacao")
    public Response indiceSatisfacao(@QueryParam("diretoria") Long diretoria,
            @QueryParam("mesAno") Long mesAno) {
        QueryParameter queryParam = QueryParameter.with("diretoria", diretoria)
                .and("mesAno", mesAno);
        return Response.ok(painelReuniaoGerencialService.findIndiceSatisfacaoByFilters(queryParam)).build();
    }

    @GET
    @Path("projetosnoano")
    public Response projetosNoAno(@QueryParam("diretoria") Long diretoria,
            @QueryParam("mesAno") Long mesAno,
            @QueryParam("departamentos") List<Long> departamentos,
            @QueryParam("termo") String termo,
            @QueryParam("cadeiasProdutivas") List<Long> cadeiasProdutivas,
            @QueryParam("tiposInstrumentoFormalizacao") List<TipoInstrFormalizacao> tiposInstrumentoFormalizacao) {
        QueryParameter queryParam = QueryParameter.with("diretoria", diretoria)
                .and("mesAno", mesAno)
                .and("gerencias", departamentos)
                .and("mesAno", mesAno)
                .and("cadeiasProdutivas", cadeiasProdutivas)
                .and("termo", termo)
                .and("tiposInstrumentoFormalizacao", tiposInstrumentoFormalizacao);
        return Response.ok(painelReuniaoGerencialService.countProjectsByFiltersGroupByCadeiaProdutiva(queryParam)).build();
    }

    @GET
    @Path("protocolosnoano")
    public Response protocolosNoAno(@QueryParam("diretoria") Long diretoria,
            @QueryParam("mesAno") Long mesAno,
            @QueryParam("departamentos") List<Long> departamentos,
            @QueryParam("tiposInstrumentoFormalizacao") List<TipoInstrFormalizacao> tiposInstrumentoFormalizacao) {
        QueryParameter queryParam = QueryParameter.with("diretoria", diretoria)
                .and("mesAno", mesAno)
                .and("gerencias", departamentos)
                .and("tiposInstrumentoFormalizacao", tiposInstrumentoFormalizacao);
        return Response.ok(painelReuniaoGerencialService.countProtocolosAssinadosByFiltersGroupByDiretoriaAndMesAndAnoAssinatura(queryParam)).build();
    }
    
    @GET
    @Path("paineldemetas")
    public Response painelMetas(@QueryParam("estagio") Estagio estagio,
        @QueryParam("departamentos") List<Long> departamentos,
        @QueryParam("dataInicio") Long dataInicio, 
        @QueryParam("dataFim") Long dataFim, 
        @QueryParam("prospeccaoAtiva") Boolean prospeccaoAtiva,
        @QueryParam("diretoria") Long diretoria) {
        
        QueryParameter queryParam = QueryParameter.with("estagio", estagio)
        .and("gerencias", departamentos)                
        .and("dataInicio", dataInicio)                               
        .and("dataFim", dataFim)    
        .and("diretoria", diretoria)                
        .and("prospeccaoAtiva", prospeccaoAtiva);                      
            return Response.ok(painelReuniaoGerencialService.countPainelMetasByFilters(queryParam)).build();
    }

}
