package br.com.gpi.server.domain.entity;

import br.com.gpi.server.domain.common.BaseEntity;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import org.hibernate.envers.Audited;

@Audited
@Entity
@Table(name = "AtividadeLocal")
@NamedQueries({
    @NamedQuery(name = AtividadeLocal.Query.findAllByLocalId, query = "SELECT a FROM AtividadeLocal a WHERE a.local.id = ?"),
    @NamedQuery(name = AtividadeLocal.Query.findAllByLocalAtivo, query = "SELECT a FROM AtividadeLocal a where a.ativo = true"),
    @NamedQuery(name = AtividadeLocal.Query.findAllActiveByLocalId, query = "SELECT a FROM AtividadeLocal a WHERE a.local.id = ? and a.ativo = true")
    
})
@Cacheable
public class AtividadeLocal extends BaseEntity<Long> {
    
    @ManyToOne
    @JoinColumn(name = "local_id", nullable = false)
    private Local local;
    
    @Column(nullable = false)
    private String descricao;
    
    private Boolean ativo;
    
    public Local getLocal() {

        return local;
    }

    public void setLocal(Local local) {
        this.local = local;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Boolean getAtivo() {
        return ativo;
    }

    public void setAtivo(Boolean ativo) {
        this.ativo = ativo;
    }
    
    public static class Query {        
        public static final String findAllByLocalId = "AtividadeLocal.findAllByLocalId";
        public static final String findAllActiveByLocalId = "AtividadeLocal.findAllActiveByLocalId";
        public static final String findAllByLocalAtivo = "AtividadeLocal.findAllByLocalAtivo";
    }    
}
