package br.com.gpi.server.domain.entity;

import br.com.gpi.server.domain.common.BaseEntity;
import java.util.Date;
import java.util.List;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.hibernate.envers.AuditJoinTable;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
@Audited
@Entity
@Table(name = "Pesquisa")
@NamedQueries({
    @NamedQuery(name = Pesquisa.Query.locateByInstrumentoFormalizacaoId, query = "SELECT p FROM Pesquisa p WHERE p.instrumentoFormalizacao.id = ?"),
    @NamedQuery(name = Pesquisa.Query.locateLastByInstrumentoFormalizacaoId, query = "SELECT max(p) FROM Pesquisa p WHERE p.instrumentoFormalizacao.id = ?"),
    @NamedQuery(name = Pesquisa.Query.locateByEmpresaId, query = "SELECT pes FROM Pesquisa pes JOIN pes.instrumentoFormalizacao.projetos pro WHERE pro.empresa.id = ?"),
    @NamedQuery(name = Pesquisa.Query.locateById, query = "SELECT pes FROM Pesquisa pes WHERE pes.id = ?")
})
public class Pesquisa extends BaseEntity<Long>{
    
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "instrumentoFormalizacao_id")
    private InstrumentoFormalizacao instrumentoFormalizacao;
    
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataSolicitacao;
    
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataResposta;
    
    private String comentario;

    @Temporal(TemporalType.TIMESTAMP)
    private Date dataUltimaAlteracao;
    
    private Float indiceSatisfacao;

    @OrderBy("id")    
    @NotAudited
    @OneToMany(mappedBy = "pesquisaId", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private List<PesquisaPerguntaPesquisa> pesquisaPerguntaPesquisaList;

    @Column(name = "contatoExternoResponsavel_id", nullable = true)
    private Long contatoExternoResponsavel;

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(name = "AnexoPesquisa", joinColumns = {
        @JoinColumn(name = "pesquisa_id")}, inverseJoinColumns = {
        @JoinColumn(name = "anexo_id")})
    private Set<Anexo> anexos;   

    public InstrumentoFormalizacao getInstrumentoFormalizacao() {
        return instrumentoFormalizacao;
    }

    public void setInstrumentoFormalizacao(InstrumentoFormalizacao instrumentoFormalizacao) {
        this.instrumentoFormalizacao = instrumentoFormalizacao;
    }

    public Date getDataSolicitacao() {
        return dataSolicitacao;
    }

    public void setDataSolicitacao(Date dataSolicitacao) {
        this.dataSolicitacao = dataSolicitacao;
    }

    public Date getDataResposta() {
        return dataResposta;
    }

    public void setDataResposta(Date dataResposta) {
        this.dataResposta = dataResposta;
    }

    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    public Date getDataUltimaAlteracao() {
        return dataUltimaAlteracao;
    }

    public void setDataUltimaAlteracao(Date dataUltimaAlteracao) {
        this.dataUltimaAlteracao = dataUltimaAlteracao;
    }

    public Set<Anexo> getAnexos() {
        return anexos;
    }

    public void setAnexos(Set<Anexo> anexos) {
        this.anexos = anexos;
    }

    public List<PesquisaPerguntaPesquisa> getPesquisaPerguntaPesquisaList() {
        return pesquisaPerguntaPesquisaList;
    }

    public void setPesquisaPerguntaPesquisaList(List<PesquisaPerguntaPesquisa> pesquisaPerguntaPesquisaList) {
        this.pesquisaPerguntaPesquisaList = pesquisaPerguntaPesquisaList;
    }

    public Long getContatoExternoResponsavel() {
        return contatoExternoResponsavel;
    }

    public void setContatoExternoResponsavel(Long contatoExternoResponsavel) {
        this.contatoExternoResponsavel = contatoExternoResponsavel;
    }

    public Float getIndiceSatisfacao() {
        return indiceSatisfacao;
    }

    public void setIndiceSatisfacao(Float indiceSatisfacao) {
        this.indiceSatisfacao = indiceSatisfacao;
    }

    public static class Query {
        public static final String locateByInstrumentoFormalizacaoId = "Pesquisa.locateByInstrumentoFormalizacaoId";
        public static final String locateLastByInstrumentoFormalizacaoId = "Pesquisa.locateLastByInstrumentoFormalizacaoId";
        public static final String locateByEmpresaId = "Pesquisa.locateByEmpresaId";
        public static final String locateById = "Pesquisa.locateById";
    }
}
