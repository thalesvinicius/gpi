package br.com.gpi.server.endpoint;

import br.com.gpi.server.domain.common.QueryParameter;
import br.com.gpi.server.domain.entity.Estagio;
import br.com.gpi.server.domain.entity.Projeto;
import br.com.gpi.server.domain.entity.SituacaoEmpresa;
import br.com.gpi.server.domain.entity.SituacaoProjetoEnum;
import br.com.gpi.server.domain.repository.ProjetoRepository;
import br.com.gpi.server.domain.service.ProjetoService;
import br.com.gpi.server.domain.service.reports.ice.ImpressaoICEReportService;
import br.com.gpi.server.domain.user.model.SecurityUser;
import br.com.gpi.server.to.ProjetoTO;
import java.io.ByteArrayInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Collection;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.inject.Inject;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;
import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import net.sf.jasperreports.engine.JRException;
import org.apache.commons.lang3.StringUtils;

@Path("/projeto")
@Stateless
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
@TransactionManagement(TransactionManagementType.BEAN)
public class ProjetoEndpoint {

    @Inject
    private ProjetoService projetoService;

    @Inject
    private ImpressaoICEReportService impressaoICEReportService;

    @Inject
    private ProjetoRepository projetoRepository;
    @Inject
    private UserTransaction transaction;

    @GET
    public Response findByFilters(@QueryParam("nomeEmpresa") String nomeEmpresa,
            @QueryParam("nomeProjeto") String nomeProjeto,
            @QueryParam("cnpj") String cnpj,
            @QueryParam("estagio") Estagio estagio,
            @QueryParam("cadeiaProdutiva") Integer cadeiaProdutivaId,
            @QueryParam("situacaoProjeto") SituacaoProjetoEnum situacaoProjeto,
            @QueryParam("departamento") List<Long> departamentos,
            @QueryParam("usuarioResponsavel") List<Long> usuariosResponsaveis,
            @QueryParam("situacaoEmpresa") SituacaoEmpresa situacaoEmpresa,
            @QueryParam("empresaId") Long empresaId,
            @QueryParam("unidadeId") Long unidadeId,
            @QueryParam("pageSize") Integer pageSize,
            @QueryParam("page") Integer page,
            @QueryParam("countResults") Boolean countResults) {

        QueryParameter filters = QueryParameter.with(Projeto.Filters.NOME_EMPRESA, nomeEmpresa)
                .and(Projeto.Filters.NOME_PROJETO, nomeProjeto)
                .and(Projeto.Filters.CNPJ, cnpj)
                .and(Projeto.Filters.ESTAGIO, estagio)
                .and(Projeto.Filters.CADEIA_PRODUTIVA, cadeiaProdutivaId)
                .and(Projeto.Filters.SITUACAO_PROJETO, situacaoProjeto)
                .and(Projeto.Filters.DEPARTAMENTOS, departamentos)
                .and(Projeto.Filters.USUARIO_RESPONSAVEL_PROJETO, usuariosResponsaveis)
                .and(Projeto.Filters.SITUACAO_EMPRESA, situacaoEmpresa)
                .and(Projeto.Filters.EMPRESA_ID, empresaId)
                .and(Projeto.Filters.UNIDADE_ID, unidadeId);

        if (countResults != null && countResults) {
            return Response.ok(projetoService.countFindByFilters(filters)).build();
        } else {
            if (page != null) {
                filters.paginate(pageSize, page);
            }
            return Response.ok(projetoService.findByFilters(filters)).build();
        }
    }

    @POST
    public Response saveNewProjeto(@Valid Projeto projeto, @Context SecurityContext securityContext) {
        try {
            SecurityUser loggedUser = (SecurityUser) securityContext.getUserPrincipal();
            return Response.ok(projetoService.saveNewProjeto(projeto, loggedUser.getId())).build();
        } catch (SystemException ex) {
            Logger.getLogger(EmpresaEndpoint.class.getName()).log(Level.SEVERE, null, ex);
            return Response.serverError().build();
        }
    }

    @GET
    @Path("recentes")
    public Response getProjetosRecentes(@Context SecurityContext securityContext) {
        try {
            SecurityUser loggedUser = (SecurityUser) securityContext.getUserPrincipal();
            return Response.ok(projetoService.findProjetosRecentesUsuario(loggedUser.getId())).build();
        } catch (SystemException ex) {
            Logger.getLogger(ProjetoEndpoint.class.getName()).log(Level.SEVERE, null, ex);
            throw new RuntimeException(ex);
        }
    }

    @POST
    @Path("recentes")
    public Response insertProjetosRecentes(@Context SecurityContext securityContext, ProjetoTO projeto) {
        try {
            transaction.begin();
            projetoService.insertProjetoRecente(projeto.getId(), ((SecurityUser) securityContext.getUserPrincipal()).getId());
            transaction.commit();
        } catch (NotSupportedException | SystemException | RollbackException | HeuristicMixedException | HeuristicRollbackException | SecurityException | IllegalStateException ex) {
            return Response.serverError().build();
        }
        return Response.ok().build();
    }

    @POST
    @Path("{id}/updateEstagio")
    public Response updateEstagioProjeto(ProjetoTO projeto, @PathParam("id") Long id, @Context SecurityContext securityContext) {
        SecurityUser loggedUser = (SecurityUser) securityContext.getUserPrincipal();
        projetoService.updateEstagioProjeto(projeto.getEstagio(), id, loggedUser.getId());
        return Response.ok().build();
    }

    @POST
    @Path("{id}/updateSituacao")
    public Response updateSituacaoProjeto(ProjetoTO projeto, @PathParam("id") Long id, @Context SecurityContext securityContext) {
        SecurityUser loggedUser = (SecurityUser) securityContext.getUserPrincipal();
        projetoService.updateSituacaoProjeto(projeto.getSituacao(), id, loggedUser.getId());
        return Response.ok().build();
    }

    @POST
    @Path("{id}/updateSituacaoAndEstagio")
    public Response updateSituacaoAndEstagioProjeto(ProjetoTO projeto, @PathParam("id") Long id, @Context SecurityContext securityContext) {
        SecurityUser loggedUser = (SecurityUser) securityContext.getUserPrincipal();
        projetoService.updateSituacaoAndEstagioProjeto(projeto.getSituacao(), projeto.getEstagio(), id, loggedUser.getId());
        return Response.ok().build();
    }

    @GET
    @Path("{id}")
    public Response findById(@PathParam("id") Long id) {
        Projeto result = projetoService.findById(id);
        return Response.ok(result).build();
    }

    @GET
    @Path("{id}/projetoTO")
    public Response findProjetoTOById(@PathParam("id") Long id) {
        ProjetoTO result = projetoService.findProjetoTOById(id);
        return Response.ok(result).build();
    }

    @PUT
    @Path("{id}")
    public Response editProjeto(@Valid Projeto projeto, @Context SecurityContext securityContext) {
        try {
            SecurityUser loggedUser = (SecurityUser) securityContext.getUserPrincipal();
            return Response.ok(projetoService.save(projeto, loggedUser.getId())).build();
        } catch (SystemException ex) {
            Logger.getLogger(EmpresaEndpoint.class.getName()).log(Level.SEVERE, null, ex);
            return Response.serverError().build();
        }
    }

    @DELETE
    public Response removeProjeto(@Valid Projeto projeto) {
        projetoRepository.remove(projeto);
        return Response.ok(projeto).build();
    }

    @GET
    @Path("empresa")
    public Collection<ProjetoTO> findByEmpresa(@QueryParam("id") List<Long> idEmpresas) {
        return projetoService.findByEmpresas(idEmpresas);
    }

    @GET
    @Path("report")
    @Produces("application/pdf")
    public Response findByIdToReport(@QueryParam("id") List<Long> idProjetos, @Context SecurityContext sc) throws JRException, FileNotFoundException, JRException, JRException, SQLException, IOException {
        ByteArrayInputStream reportInputStream = new ByteArrayInputStream(impressaoICEReportService.imprimirICE(idProjetos, sc));
        String projetos = StringUtils.join(idProjetos, "-");
        String filename = "ICE-" + projetos + ".pdf";
        return Response.ok()
                .entity(reportInputStream)
                .header("Content-Disposition", "attachment; filename='" + filename + "'")
                .build();
    }

    @GET
    @Path("unidade")
    public Collection<ProjetoTO> findByUnidade(@QueryParam("id") List<Long> idUnidades) {
        return projetoService.findByUnidadesEmpresa(idUnidades);
    }

    @GET
    @Path("responsavelInvestimento")
    public Response countByResponsavelInvestimentoInList(@QueryParam("usersId") List<Long> usersId) {
        List<Projeto> projetos = projetoRepository.countByResponsavelInvestimentoInList(usersId);
        return Response.ok(projetos).build();
    }
}
