package br.com.gpi.server.domain.repository;

import br.com.gpi.server.domain.entity.SituacaoProjeto;
import java.util.List;
import org.apache.deltaspike.data.api.EntityRepository;
import org.apache.deltaspike.data.api.Query;
import org.apache.deltaspike.data.api.Repository;
import org.apache.deltaspike.data.api.criteria.CriteriaSupport;

@Repository(forEntity = SituacaoProjeto.class)
public abstract class SituacaoProjetoRepository implements CriteriaSupport<SituacaoProjeto>, EntityRepository<SituacaoProjeto, String> {
    @Query(named = SituacaoProjeto.Query.findAllByProjeto)
    public abstract List<SituacaoProjeto> findAllByProjeto(Long idProjeto);
}
