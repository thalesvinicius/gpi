package br.com.gpi.server.domain.entity;

import br.com.gpi.server.domain.common.AuditedEntity;
import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.Date;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import org.hibernate.envers.Audited;

@Table(name = "InformacaoAdicional")
@Audited
@Entity
@NamedQueries({
    @NamedQuery(name = Contato.Query.locateByCompany, query = "SELECT c FROM Contato c WHERE c.empresa.id = ?1"),
    @NamedQuery(name = Contato.Query.deleteByid, query = "DELETE FROM Contato c WHERE c.id in ?1")
})
public class Contato extends AuditedEntity<Long> {

    @Enumerated
    private TipoContato tipoContato;
    
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "empresa_id", nullable = false)
    private Empresa empresa;

    @NotNull
    @Column(name = "assunto", length = 150, nullable = false)
    private String assunto;

    @NotNull
    @Column(name = "data", nullable = false)
    @Temporal(TemporalType.DATE)
    private Date dataEfetivado;
    
    @NotNull
    @Column(name = "descricao", nullable = false, length=1500)
    private String descricao;

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(name="AnexoInformacaoAdicional", joinColumns={@JoinColumn(name="informacaoAdicional_id")}, inverseJoinColumns={@JoinColumn(name="anexo_id")})
    private Set<Anexo> anexos;
    
    public static class Query {
        public static final String locateByCompany = "Contato.locateByCompany";
        public static final String deleteByid = "Contato.deleteByid";
    }
    

    public TipoContato getTipoContato() {
        return tipoContato;
    }

    public void setTipoContato(TipoContato tipoContato) {
        this.tipoContato = tipoContato;
    }

    public String getAssunto() {
        return assunto;
    }

    public void setAssunto(String assunto) {
        this.assunto = assunto;
    }

    public Date getDataEfetivado() {
        return dataEfetivado;
    }

    public void setDataEfetivado(Date data) {
        this.dataEfetivado = data;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Set<Anexo> getAnexos() {
        return anexos;
    }

    public void setAnexos(Set<Anexo> anexos) {
        this.anexos = anexos;
    }

    public Empresa getEmpresa() {
        return empresa;
    }

    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }

    public String getTipoContatoDescricao() {
        return tipoContato.getDescription();
    }
    
    

}
