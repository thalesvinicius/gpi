package br.com.gpi.server.domain.entity.enumerated;

import br.com.gpi.server.core.exception.EnumException;
import br.com.gpi.server.core.jpa.typesafe.EnumerationEntityType;

public enum EnumTipoProduto implements EnumerationEntityType<Integer> {
    
    PRODUTOS_FABRICADOS_E_COM_POR_MINAS(1, "Produtos a Serem Fabricados e Comercializados por Minas"),
    PRODUTOS_ADQUIRIDOS_E_COM_POR_MINAS(2, "Produtos a Serem Adquiridos e Comercializados por Minas"),
    PRODUTOS_ADQ_DE_OUTROS_ESTADOS_PARA_COMERC(3, "Produtos Adquiridos de Outros Estados para Comercialização"),
    PRODUTOS_IMPORTADOS_PARA_COMERC(4, "Produtos Importados para Comercialização");
    
    private final int id;
    private final String descricao;

    private EnumTipoProduto(int id, String descricao) {
        this.id = id;
        this.descricao = descricao;
    }

    @Override
    public Integer getId() {
        return this.id;
    }

    @Override
    public String getDescription() {
        return this.descricao;
    }
    
    public static EnumTipoProduto getEnumTipoProduto(Integer id) {
        for (EnumTipoProduto tipoProduto : values()) {
            if (tipoProduto.id == id) {
                return tipoProduto;
            }
        }
        throw new EnumException("EnumTipoProduto couldn't load.");
    }  
}
