package br.com.gpi.server.core.exception;

/**
 *
 * @author <a href="mailto:pviniciusfm@gmail.com">Paulo Vinicius F. Machado</a>
 */
public class ApplicationException extends Exception {

    public ApplicationException(String message) {
        super(message);
    }

}
