package br.com.gpi.server.domain.service;

import br.com.gpi.server.domain.common.BaseService;
import br.com.gpi.server.domain.entity.Emprego;
import br.com.gpi.server.domain.entity.Estagio;
import br.com.gpi.server.domain.entity.Projeto;
import br.com.gpi.server.domain.entity.SituacaoProjetoEnum;
import br.com.gpi.server.domain.entity.TipoEmprego;
import br.com.gpi.server.domain.entity.User;
import br.com.gpi.server.domain.entity.UsuarioExterno;
import br.com.gpi.server.domain.repository.EmpregoRepository;
import br.com.gpi.server.domain.user.model.SecurityUser;
import br.com.gpi.server.to.EmpregoAgricolaIndustriaDTO;
import br.com.gpi.server.util.LogUtil;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;
import javax.ws.rs.core.SecurityContext;

public class EmpregoService extends BaseService<Emprego> {

    @Inject
    private EmpregoRepository empregoRepository;

    @Inject
    private ProjetoService projetoService;
    
    @Inject
    private UserTransaction transaction;

    @Inject
    public EmpregoService(EntityManager em) {
        super(Emprego.class, em);
    }

    public Collection<EmpregoAgricolaIndustriaDTO> findAllEmpregoAgricolaIndustriaDTOPermanente(Long idProjeto) {
        List<Emprego> empregoAgricola = findAllEmpregosByTipoEmprego(idProjeto, TipoEmprego.PERMANENTE_MATURACAO_AGRICOLA.getId());
        List<Emprego> empregoIndustria = findAllEmpregosByTipoEmprego(idProjeto, TipoEmprego.PERMANENTE_MATURACAO_INDUSTRIAL.getId());
        return convertToEmpregoAgricolaIndustriaDTO(empregoAgricola, empregoIndustria);
    }

    public Collection<EmpregoAgricolaIndustriaDTO> findAllEmpregoAgricolaIndustriaDTOTemporario(Long idProjeto) {
        List<Emprego> empregoAgricola = findAllEmpregosByTipoEmprego(idProjeto, TipoEmprego.TEMPORARIO_IMPLANTACAO_AGRICOLA.getId());
        List<Emprego> empregoIndustria = findAllEmpregosByTipoEmprego(idProjeto, TipoEmprego.TEMPORARIO_IMPLANTACAO_INDUSTRIAL.getId());
        return convertToEmpregoAgricolaIndustriaDTO(empregoAgricola, empregoIndustria);
    }

    public List<Emprego> findAllEmpregosByTipoEmprego(Long idProjeto, Integer idTipoEmprego) {
        StringBuilder builder = new StringBuilder("SELECT e FROM Emprego e ");
        builder.append("\n where e.tipoEmprego = ").append(idTipoEmprego);
        builder.append("\n and e.projeto.id = ").append(idProjeto);
        builder.append("\n order by e.ano");
        TypedQuery<Emprego> query = getEm().createQuery(builder.toString(), Emprego.class);
        return query.getResultList();
    }

    private Collection<EmpregoAgricolaIndustriaDTO> convertToEmpregoAgricolaIndustriaDTO(List<Emprego> empregosAgricola, List<Emprego> empregosIndustria) {
        Map<Integer, EmpregoAgricolaIndustriaDTO> mapDTO = new HashMap<>();
        for (Emprego empregoAgricola : empregosAgricola) {
            EmpregoAgricolaIndustriaDTO empregoAgricolaIndustriaDTO = new EmpregoAgricolaIndustriaDTO();
            empregoAgricolaIndustriaDTO.setAno(empregoAgricola.getAno());
            empregoAgricolaIndustriaDTO.setEmpregoAgricolaId(empregoAgricola.getId());
            empregoAgricolaIndustriaDTO.setDiretoAgricola(empregoAgricola.getDireto());
            empregoAgricolaIndustriaDTO.setIndiretoAgricola(empregoAgricola.getIndireto());
            mapDTO.put(empregoAgricola.getAno(), empregoAgricolaIndustriaDTO);
        }

        for (Emprego empregoIndustria : empregosIndustria) {
            EmpregoAgricolaIndustriaDTO empregoAgricolaIndustriaDTO = null;
            if (mapDTO.containsKey(empregoIndustria.getAno())) {
                empregoAgricolaIndustriaDTO = mapDTO.get(empregoIndustria.getAno());
            } else {
                empregoAgricolaIndustriaDTO = new EmpregoAgricolaIndustriaDTO();
                mapDTO.put(empregoIndustria.getAno(), empregoAgricolaIndustriaDTO);
            }
            empregoAgricolaIndustriaDTO.setAno(empregoIndustria.getAno());
            empregoAgricolaIndustriaDTO.setEmpregoIndustriaId(empregoIndustria.getId());
            empregoAgricolaIndustriaDTO.setDiretoIndustria(empregoIndustria.getDireto());
            empregoAgricolaIndustriaDTO.setIndiretoIndustria(empregoIndustria.getIndireto());
        }

        return mapDTO.values();
    }

    public void saveOrUpdateEmprego(List<Emprego> empregosList, SecurityContext context, Long idProjeto) throws SystemException {
        try {
            transaction.begin();
            this.getEm().joinTransaction();

            SecurityUser loggedUserWrap = (SecurityUser) context.getUserPrincipal();
            User user = this.getEm().find(User.class, loggedUserWrap.getId());
            Projeto projeto = new Projeto();
            projeto.setId(idProjeto);
            Date dataUltimaAlteracao = new Date();

            //Altera a situação do projeto se o estágio for INICIO_PROJETO.
            if (projetoService.findEstagioAtualById(idProjeto).getEstagio() == Estagio.INICIO_PROJETO) {
                if(user instanceof UsuarioExterno) {
                    projetoService.updateSituacaoProjetoTransactional(SituacaoProjetoEnum.PENDENTE_ACEITE, idProjeto, loggedUserWrap.getId(), new Date());
                } else {
                    projetoService.updateSituacaoProjetoTransactional(SituacaoProjetoEnum.PEDENTE_VALIDACAO, idProjeto, loggedUserWrap.getId(), new Date());
                    
                }
            }

            for (Emprego emprego : empregosList) {
                emprego.setUsuarioResponsavel(user);
                emprego.setDataUltimaAlteracao(dataUltimaAlteracao);
                emprego.setProjeto(projeto);
                this.merge(emprego);
            }
            
            this.createUpdateDeleteQuery(Projeto.Query.updateUsuarioeData, projeto.getId(), user, dataUltimaAlteracao);
            
            transaction.commit();
        } catch (Exception ex) {
            transaction.rollback();
            Logger.getLogger(ProjetoService.class.getName()).log(Level.SEVERE, null, ex);
            throw new RuntimeException(ex);
        }
    }
    
    public Object verifyChange(Long idProjeto) throws SystemException {
        String atributos = " tipo, ano_MOD, direto_MOD, diretoRealizado_MOD, indireto_MOD ";
        Object result = this.getEm().createNativeQuery(
                LogUtil.montaConsultaLog("LogEmprego", atributos, idProjeto).toString()).getResultList();

        return result;
    }      
}
