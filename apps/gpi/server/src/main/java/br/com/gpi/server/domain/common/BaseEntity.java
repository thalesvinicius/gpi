package br.com.gpi.server.domain.common;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import static java.lang.Math.abs;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;
import org.hibernate.envers.Audited;

/**
 * Default Domain Entity.
 *
 * @author <a href="mailto:pviniciusfm@gmail.com">Paulo Vinicius F. Machado</a>
 * @param <T> The type of primary key
 *
 */
@Audited 
@MappedSuperclass
@JsonIgnoreProperties(ignoreUnknown = true)
public abstract class BaseEntity<T> implements Serializable, Persistable{

    private static final long serialVersionUID = 0xDDD0000D0001L;
    @Id
    @Basic(optional = false)
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private T id;

    /**
     * Recover identification of entity
     *
     * @return identification
     */
    public T getId() {
        return id;
    }

    public void setId(T id) {
        this.id = id;
    }

    /**
     * Prime aux numbers for hashCode generation. HashCodes com menor
     * divisibilidade sao melhores para distribuicao de buckets, menor chance de
     * ocorrerem colisoes.
     */
    private static final int[] PRIMES = {
        101, 103, 107, 109, 113, 127, 131, 137, 139, 149,
        151, 157, 163, 167, 173, 179, 181, 191, 193, 197,
        199, 211, 223, 227, 229, 233, 239, 241, 251, 257,
        263, 269, 271, 277, 281, 283, 293, 307, 311, 313,
        317, 331, 337, 347, 349, 353, 359, 367, 373, 379,
        383, 389, 397
    };
    /**
     * Primo de valor significativo para uma determinada classe. Usado no
     * cálculo do hashCode.
     */
    @Transient
    private int meaningfulNumber = 0;

    @Override
    public int hashCode() {

        if (meaningfulNumber == 0) {
            int i = abs(removeJavaAssistAppend(getClass().getSimpleName()).hashCode());
            meaningfulNumber = PRIMES[ i % PRIMES.length];
        }

        T thisId = getId();

        int identity = thisId != null && thisId.hashCode() > 0 ? thisId.hashCode() : 31;

        return identity * meaningfulNumber;
    }

    @Override
    @SuppressWarnings("unchecked")
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }

        if (obj.hashCode() != hashCode() || !hasSameClass(obj.getClass())) {
            return false;
        }

        BaseEntity<T> entity = (BaseEntity) obj;

        T otherId = entity.getId();

        //Checking same reference pointer
        if (otherId == null && getId() == null) {
            return super.equals(obj);
        } else if (otherId == null || getId() == null) {

            return false;

        } else {
            return getId().equals(otherId);
        }
    }

    /**
     * Verifica se duas classes distintas sao as mesmas.
     *
     * @param clazz
     * @return
     */
    protected boolean hasSameClass(Class<?> clazz) {
        String className = removeJavaAssistAppend(getClass().getName());
        String otherClassName = removeJavaAssistAppend(clazz.getName());
        return className.equals(otherClassName);
    }

    /**
     * Remove sufixo adicionado de classes de instrumentacao de javassist. Em
     * objetos OneToOne e ManyToOne Lazy, proxies sao criados e classes de java
     * assist arguidas quanto a equalidade.
     *
     * @param className
     * @return
     */
    protected String removeJavaAssistAppend(final String className) {
        String newClassName = className;
        int i = className.indexOf('_');
        if (i != -1) {
            newClassName = className.substring(0, i);
        }
        return newClassName;
    }

    /**
     * Obtêm um r�tulo da entidade persistente. Usado para UI ou identifica�ões
     * gerais.
     *
     * @return
     */
    @JsonIgnore
    public String getLabel() {
        StringBuilder sb = new StringBuilder(removeJavaAssistAppend(getClass().getName()));
        return sb.append(" - ").append(getId()).toString();
    }

    @Override
    public String toString() {
        return getLabel();
    }
}