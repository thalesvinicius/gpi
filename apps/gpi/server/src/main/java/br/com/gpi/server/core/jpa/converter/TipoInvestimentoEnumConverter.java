package br.com.gpi.server.core.jpa.converter;

import br.com.gpi.server.domain.entity.enumerated.EnumTipoInvestimento;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = false)
public class TipoInvestimentoEnumConverter implements AttributeConverter<EnumTipoInvestimento, Integer> {
    
    @Override
    public Integer convertToDatabaseColumn(EnumTipoInvestimento tipoInvestimento) {
        return tipoInvestimento.getId();
    }

    @Override
    public EnumTipoInvestimento convertToEntityAttribute(Integer dbData) {
        return EnumTipoInvestimento.getEnumTipoInvestimento(dbData);
    }    
}