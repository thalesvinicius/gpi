package br.com.gpi.server.domain.entity;

import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import org.hibernate.envers.Audited;

@Entity
@Table(name = "view_relatorio_pesquisas_recebidas")
public class RelatorioPesquisasRecebidas {
    @Id
    private Long pesquisaId;
    private String empresa;
    private String gerencia;
    private Long gerenciaId;
    private String diretoria;
    private Long diretoriaId;
    private String responsavel;
    private Float porcentagemPesquisa;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date dataRecebimento;
    private String comentario;
    private Float meta;


    public Long getPesquisaId() {
        return pesquisaId;
    }

    public void setPesquisaId(Long pesquisaId) {
        this.pesquisaId = pesquisaId;
    }

    public String getEmpresa() {
        return empresa;
    }

    public void setEmpresa(String empresa) {
        this.empresa = empresa;
    }

    public String getGerencia() {
        return gerencia;
    }

    public void setGerencia(String gerencia) {
        this.gerencia = gerencia;
    }

    public String getDiretoria() {
        return diretoria;
    }

    public void setDiretoria(String diretoria) {
        this.diretoria = diretoria;
    }

    public String getResponsavel() {
        return responsavel;
    }

    public void setResponsavel(String responsavel) {
        this.responsavel = responsavel;
    }

    public Float getPorcentagemPesquisa() {
        return porcentagemPesquisa;
    }

    public void setPorcentagemPesquisa(Float porcentagemPesquisa) {
        this.porcentagemPesquisa = porcentagemPesquisa;
    }

    public Date getDataRecebimento() {
        return dataRecebimento;
    }

    public void setDataRecebimento(Date dataRecebimento) {
        this.dataRecebimento = dataRecebimento;
    }

    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    public Long getGerenciaId() {
        return gerenciaId;
    }

    public void setGerenciaId(Long gerenciaId) {
        this.gerenciaId = gerenciaId;
    }

    public Long getDiretoriaId() {
        return diretoriaId;
    }

    public void setDiretoriaId(Long diretoriaId) {
        this.diretoriaId = diretoriaId;
    }

    public Float getMeta() {
        return meta;
    }

    public void setMeta(Float meta) {
        this.meta = meta;
    }
        
}
