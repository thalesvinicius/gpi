package br.com.gpi.server.domain.user.exception;

import br.com.gpi.server.core.exception.BaseWebApplicationException;

/**
 * @version 1.0
 * @author <a href="mailto:pviniciusfm@gmail.com">Paulo Vinicius F. Machado</a>
 */
public class TokenHasExpiredException extends BaseWebApplicationException {

    private static final long serialVersionUID = 1L;

    public TokenHasExpiredException() {
        super(403, "40304", "user.token.expired", "An attempt was made to load a token that has expired");
    }
}
