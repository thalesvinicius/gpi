package br.com.gpi.server.domain.entity;

import br.com.gpi.server.core.exception.EnumException;
import br.com.gpi.server.core.jpa.typesafe.EnumerationEntityType;
import static br.com.gpi.server.domain.entity.SituacaoProjetoEnum.values;

//@EnumerationEntity(table = "SituacaoProjetoEnum",)
public enum SituacaoProjetoEnum implements EnumerationEntityType<Integer> {

    INVALID(0, "invalid"),  ATIVO(1, "Ativo"), CANCELADO(2, "Cancelado"), PENDENTE_ACEITE(3, "Pendente de Aceite"), PEDENTE_VALIDACAO(4, "Pendente de Validação"), SUSPENSO(5, "Suspenso");

    private SituacaoProjetoEnum(int id, String descricao) {
        this.id = id;
        this.descricao = descricao;
    }

    private final int id;
    private final String descricao;

    @Override
    public Integer getId() {
        return this.id;
    }

    @Override
    public String getDescription() {
        return this.descricao;
    }
    
    public static SituacaoProjetoEnum getSituacaoProjeto(Integer id) {
        for (SituacaoProjetoEnum situacaoProjeto : values()) {
            if (situacaoProjeto.id == id) {
                return situacaoProjeto;
            }
        }
        throw new EnumException("SituacaoProjeto couldn't load.");
    }
}
