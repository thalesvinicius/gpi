package br.com.gpi.server.to;

import br.com.gpi.server.domain.entity.CadeiaProdutiva;
import br.com.gpi.server.domain.entity.SituacaoProjetoEnum;
import java.util.List;
import javax.enterprise.inject.Model;

@Model
public class ProjetoFilter {

    private String nomeEmpresa;
    private String nomeProjeto;
    private String cnpj;
    private String estagio;
    private CadeiaProdutiva cadeiaProdutiva;
    private SituacaoProjetoEnum situacaoProjeto;
    private String nomeDepartamento;
    private List<Integer> departamentos;
    private List<Integer> usuariosResponsaveis;

    public ProjetoFilter() {
    }

    public String getNomeEmpresa() {
        return nomeEmpresa;
    }

    public void setNomeEmpresa(String nomeEmpresa) {
        this.nomeEmpresa = nomeEmpresa;
    }

    public String getNomeProjeto() {
        return nomeProjeto;
    }

    public void setNomeProjeto(String nomeProjeto) {
        this.nomeProjeto = nomeProjeto;
    }

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public String getEstagio() {
        return estagio;
    }

    public void setEstagio(String estagio) {
        this.estagio = estagio;
    }

    public CadeiaProdutiva getCadeiaProdutiva() {
        return cadeiaProdutiva;
    }

    public void setCadeiaProdutiva(CadeiaProdutiva cadeiaProdutiva) {
        this.cadeiaProdutiva = cadeiaProdutiva;
    }

    public SituacaoProjetoEnum getSituacaoProjeto() {
        return situacaoProjeto;
    }

    public void setSituacaoProjeto(SituacaoProjetoEnum situacaoProjeto) {
        this.situacaoProjeto = situacaoProjeto;
    }

    public String getNomeDepartamento() {
        return nomeDepartamento;
    }

    public void setNomeDepartamento(String nomeDepartamento) {
        this.nomeDepartamento = nomeDepartamento;
    }

    public List<Integer> getDepartamentos() {
        return departamentos;
    }

    public void setDepartamentos(List<Integer> departamentos) {
        this.departamentos = departamentos;
    }

    public List<Integer> getUsuariosResponsaveis() {
        return usuariosResponsaveis;
    }

    public void setUsuariosResponsaveis(List<Integer> usuariosResponsaveis) {
        this.usuariosResponsaveis = usuariosResponsaveis;
    }

}
