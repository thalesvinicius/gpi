package br.com.gpi.server.to;

import br.com.gpi.server.domain.entity.AtividadeLocal;
import java.util.List;

public class LocalTO {

    private Long id;
    private String descricao;
    private List<AtividadeLocal> atividades;

    public LocalTO() {
    }
    
    public LocalTO(Long id, String descricao) {
        this.id = id;
        this.descricao = descricao;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public List<AtividadeLocal> getAtividades() {
        return atividades;
    }

    public void setAtividades(List<AtividadeLocal> atividades) {
        this.atividades = atividades;
    }
    
}
