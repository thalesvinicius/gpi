package br.com.gpi.server.domain.entity;

import br.com.gpi.server.domain.common.BaseEntity;
import com.fasterxml.jackson.annotation.JsonIgnore;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.envers.Audited;

@Audited
@Entity
@Table(name = "Produto")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "tipo",
        discriminatorType = DiscriminatorType.INTEGER
)
public abstract class Produto extends BaseEntity<Long> {

    public static final String PRODUTO_FAB_E_COM_POR_MINAS = "1";
    public static final String PRODUTO_ADQ_E_COM_POR_MINAS = "2";
    public static final String PRODUTO_ADQ_OUTROS_ESTADOS_PARA_COM = "3";
    public static final String PRODUTO_IMP_PARA_COM = "4";

    @JsonIgnore
    @ManyToOne(cascade = {CascadeType.DETACH, CascadeType.REFRESH}, optional = false, fetch = FetchType.LAZY)
    @JoinColumn(name = "insumoProduto_id")
    private InsumoProduto insumoProduto;

    private String nome;

    @ManyToOne(cascade = {CascadeType.DETACH, CascadeType.REFRESH})
    @JoinColumn(name = "ncm_id")
    private NCM ncm;

    @Column(columnDefinition = "bit")
    private Boolean sujeitoST;

    private Integer quantidade1;

    private Integer quantidade2;

    private Integer quantidade3;

    private String unidadeMedida;
    
    @Column(insertable = false, updatable = false)
    private Integer tipo;

//    @Column(name = "tipo")
//    private EnumTipoProduto tipoProduto;
    public InsumoProduto getInsumoProduto() {
        return insumoProduto;
    }

    public void setInsumoProduto(InsumoProduto insumoProduto) {
        this.insumoProduto = insumoProduto;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public NCM getNcm() {
        return ncm;
    }

    public void setNcm(NCM ncm) {
        this.ncm = ncm;
    }

    public Boolean isSujeitoST() {
        return sujeitoST;
    }

    public Boolean getSujeitoST() {
        return sujeitoST;
    }

    public void setSujeitoST(Boolean sujeitoST) {
        this.sujeitoST = sujeitoST;
    }

    public Integer getQuantidade1() {
        return quantidade1;
    }

    public void setQuantidade1(Integer quantidade1) {
        this.quantidade1 = quantidade1;
    }

    public Integer getQuantidade2() {
        return quantidade2;
    }

    public void setQuantidade2(Integer quantidade2) {
        this.quantidade2 = quantidade2;
    }

    public Integer getQuantidade3() {
        return quantidade3;
    }

    public void setQuantidade3(Integer quantidade3) {
        this.quantidade3 = quantidade3;
    }

    public String getUnidadeMedida() {
        return unidadeMedida;
    }

    public void setUnidadeMedida(String unidadeMedida) {
        this.unidadeMedida = unidadeMedida;
    }

//    public EnumTipoProduto getTipoProduto() {
//        return tipoProduto;
//    }
//
//    public void setTipoProduto(EnumTipoProduto tipoProduto) {
//        this.tipoProduto = tipoProduto;
//    }
    public Integer getTipo() {
        return tipo;
    }

    public void setTipo(Integer tipo) {
        this.tipo = tipo;
    }
}
