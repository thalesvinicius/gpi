package br.com.gpi.server.to;

import br.com.gpi.server.domain.entity.DescricaoFaseEnum;
import java.util.Date;

public class CalendarioAcompanhamentoAnualTO extends AcompanhamentoTO{
    
    private Long id;
    private DescricaoFaseEnum descricaoFaseEnum;
    private Date dataRealizado;
    private Date dataPrevisto;

    public DescricaoFaseEnum getDescricaoFaseEnum() {
        return descricaoFaseEnum;
    }

    public void setDescricaoFaseEnum(DescricaoFaseEnum descricaoFaseEnum) {
        this.descricaoFaseEnum = descricaoFaseEnum;
    }

    public Date getDataRealizado() {
        return dataRealizado;
    }

    public void setDataRealizado(Date dataRealizado) {
        this.dataRealizado = dataRealizado;
    }

    public Date getDataPrevisto() {
        return dataPrevisto;
    }

    public void setDataPrevisto(Date dataPrevisto) {
        this.dataPrevisto = dataPrevisto;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
        
}
