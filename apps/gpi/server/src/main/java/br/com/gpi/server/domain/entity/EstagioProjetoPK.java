package br.com.gpi.server.domain.entity;

import java.io.Serializable;
import java.util.Date;

public class EstagioProjetoPK implements Serializable {

    private long projetoId;
    private long estagioId;
    private Date dataInicioEstagio;

    public long getProjetoId() {
        return projetoId;
    }

    public void setProjetoId(long projetoId) {
        this.projetoId = projetoId;
    }

    public long getEstagioId() {
        return estagioId;
    }

    public void setEstagioId(long estagioId) {
        this.estagioId = estagioId;
    }

    public Date getDataInicioEstagio() {
        return dataInicioEstagio;
    }

    public void setDataInicioEstagio(Date dataInicioEstagio) {
        this.dataInicioEstagio = dataInicioEstagio;
    }
    
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 29 * hash + (int) (this.projetoId ^ (this.projetoId >>> 32));
        hash = 29 * hash + (int) (this.estagioId ^ (this.estagioId >>> 32));
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final EstagioProjetoPK other = (EstagioProjetoPK) obj;
        
        if (this.projetoId != other.projetoId) {
            return false;
        }
        if (this.estagioId != other.estagioId) {
            return false;
        }
        if(this.dataInicioEstagio != other.dataInicioEstagio) {
            return false;
        }
        return true;
    }
    
}
