package br.com.gpi.server.domain.repository;

import br.com.gpi.server.domain.entity.Financeiro;
import java.util.List;
import org.apache.deltaspike.data.api.EntityRepository;
import org.apache.deltaspike.data.api.Modifying;
import org.apache.deltaspike.data.api.Query;
import org.apache.deltaspike.data.api.Repository;
import org.apache.deltaspike.data.api.criteria.CriteriaSupport;


@Repository(forEntity = Financeiro.class)
public abstract class FinanceiroRepository implements CriteriaSupport<Financeiro>, EntityRepository<Financeiro, Long> {
        
    @Modifying
    @Query(named = Financeiro.Query.locateByProjetoId)
    public abstract List<Financeiro> locateByProjetoId(Long id);
    
}
