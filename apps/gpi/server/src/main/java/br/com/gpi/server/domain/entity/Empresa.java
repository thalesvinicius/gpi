package br.com.gpi.server.domain.entity;

import br.com.gpi.server.core.jpa.converter.OrigemEmpresaEnumConverter;
import br.com.gpi.server.core.jpa.converter.SituacaoEmpresaEnumConverter;
import br.com.gpi.server.domain.common.AuditedEntity;
import br.com.gpi.server.util.MaskUtil;
import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PostLoad;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import org.hibernate.annotations.Where;
import org.hibernate.envers.Audited;

/**
 *
 * @author lucasgom
 */
@Audited
@Entity
@Table(name = "Empresa")
@NamedQueries({
    @NamedQuery(name = Empresa.Query.deleteByid, query = "DELETE FROM Empresa e WHERE e.id in ?1"),
    @NamedQuery(name = Empresa.Query.updateSituacaoEmpresa, query = "UPDATE Empresa e SET e.situacao = ?1 WHERE e.id = ?2"),
    @NamedQuery(name = Empresa.Query.updateUsuarioExternoResponsavel, query = "UPDATE Empresa e SET e.responsavel = ?1 WHERE e.id = ?2"),
    @NamedQuery(name = Empresa.Query.FIND_EMPRESA_BY_USUARIO_EXTERNO_RESPONSAVEL, query = "Select e from Empresa e where e.responsavel.id = ?1"),
    @NamedQuery(name = Empresa.Query.findAllByIds, query = "SELECT u FROM Empresa u WHERE u.id in ?1"),
    @NamedQuery(name = Empresa.Query.findNomeByCadeiaProdutivaGerencia, query = "SELECT u.nomePrincipal, u.id FROM Empresa u WHERE u.cadeiaProdutiva in ?1 and u.situacao = ?2") 
})
public class Empresa extends AuditedEntity<Long> {

    @Column(name = "cnpj", length = 14, unique = true)
    private String cnpj;

    @NotNull
    @Column(name = "nomePrincipal", length = 100, nullable = false)
    private String nomePrincipal;
    
    @Enumerated
    @Column(name = "origemEmpresa", insertable = false, updatable = false)
    private OrigemEmpresaEnum origemEmpresa;
    
    @Column(name = "origemEmpresa")
    private Integer origemEmpresaInt;

    @Column(name = "empresaNovaMG", columnDefinition = "bit")
    private Boolean empresaNovaMG;
    
    @Column(name = "inscricaoEstadual", length = 8)
    private String inscricaoEstadual;

    @Column(name = "razaoSocial", length = 200)
    private String razaoSocial;

    @Column(name = "composicaoSocietaria", length = 1000)
    private String composicaoSocietaria;

    @Column(name = "historico", length = 1000)
    private String historico;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "endereco_id", unique = true, nullable = true)
    private Endereco endereco;

    @OneToOne(cascade = CascadeType.ALL,fetch = FetchType.LAZY)
    @JoinColumn(name = "anexo_id", unique = true, nullable = true)
    private Anexo logo;

    @ManyToOne(cascade = {CascadeType.DETACH, CascadeType.REFRESH})
    @JoinColumn(name = "naturezaJuridica_id")
    private NaturezaJuridica naturezaJuridica;

    @OneToMany(cascade = {CascadeType.REMOVE, CascadeType.DETACH, CascadeType.REFRESH}, fetch = FetchType.LAZY, mappedBy = "empresa")
    private Set<UnidadeEmpresa> unidades;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "empresa")
    private Set<Contato> contatos;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "empresa", fetch = FetchType.LAZY, orphanRemoval = true)
    @Where(clause = "nivel = " + CNAE.DIVISAO)
    private Set<CNAEEmpresa> divisoesCnae;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "empresa", fetch = FetchType.LAZY, orphanRemoval = true)
    @Where(clause = "nivel = " + CNAE.GRUPO)
    private Set<CNAEEmpresa> gruposCnae;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "empresa", fetch = FetchType.LAZY, orphanRemoval = true)
    @Where(clause = "nivel = " + CNAE.CLASSE)
    private Set<CNAEEmpresa> classesCnae;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "empresa", fetch = FetchType.LAZY, orphanRemoval = true)
    @Where(clause = "nivel = " + CNAE.SUBCLASSE)
    private Set<CNAEEmpresa> subclassesCnae;
     
    private transient List<CNAEEmpresa> divisoesCnaePersistido;
    private transient List<CNAEEmpresa> gruposCnaePersistido;
    private transient List<CNAEEmpresa> classesCnaePersistido;
    private transient List<CNAEEmpresa> subclassesCnaePersistido;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY, mappedBy = "empresa")
    private Set<NomeEmpresa> nomesEmpresa;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY, mappedBy = "empresa")
    private Set<ComposicaoSocietaria> composicaoSocietarias;
    
    @ManyToOne(cascade = {CascadeType.DETACH, CascadeType.REFRESH})
    @JoinColumn(name = "cadeiaProdutiva_id")
    private CadeiaProdutiva cadeiaProdutiva;

    @Enumerated
    @Column(name = "situacao", insertable = false, updatable = false)
    private SituacaoEmpresa situacao;
    
    @JsonIgnore
    @Column(name = "situacao")
    private Integer situacaoInt;

    private String justificativaArquivamento;

    private transient String cnpjFormatado;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "empresa")
    private Set<Projeto> projetos;

    @OneToOne
    @JoinColumn(name = "usuarioExternoResponsavel_id")
    private UsuarioExterno responsavel;

    private transient Long responsavelPersistidoId;
    
    private transient String assuntoFaleConosco;
    private transient String descricaoFaleConosco;
    
    @ManyToOne(cascade = {CascadeType.DETACH, CascadeType.REFRESH})
    @JoinColumn(name = "paisOrigem", nullable = true)
    private Pais paisOrigem;

    @ManyToOne(cascade = {CascadeType.DETACH, CascadeType.REFRESH})
    @JoinColumn(name = "estadoOrigem", nullable = true)
    private UF estadoOrigem;    
    
    private transient SituacaoEmpresa situacaoOld;

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public String getNomePrincipal() {
        return nomePrincipal;
    }

    public void setNomePrincipal(String nomePrincipal) {
        this.nomePrincipal = nomePrincipal;
    }

    public String getInscricaoEstadual() {
        return inscricaoEstadual;
    }

    public void setInscricaoEstadual(String inscricaoEstadual) {
        this.inscricaoEstadual = inscricaoEstadual;
    }

    public String getRazaoSocial() {
        return razaoSocial;
    }

    public void setRazaoSocial(String razaoSocial) {
        this.razaoSocial = razaoSocial;
    }

    public Serializable getComposicaoSocietaria() {
        return composicaoSocietaria;
    }

    public void setComposicaoSocietaria(String composicaoSocietaria) {
        this.composicaoSocietaria = composicaoSocietaria;
    }

    public String getHistorico() {
        return historico;
    }

    public void setHistorico(String historico) {
        this.historico = historico;
    }

    public Endereco getEndereco() {
        return endereco;
    }

    public void setEndereco(Endereco endereco) {
        this.endereco = endereco;
    }

    public Anexo getLogo() {
        return logo;
    }

    public void setLogo(Anexo logo) {
        this.logo = logo;
    }

    public NaturezaJuridica getNaturezaJuridica() {
        return naturezaJuridica;
    }

    public void setNaturezaJuridica(NaturezaJuridica naturezaJuridica) {
        this.naturezaJuridica = naturezaJuridica;
    }

    public Set<UnidadeEmpresa> getUnidades() {
        return unidades;
    }

    public void setUnidades(Set<UnidadeEmpresa> unidades) {
        this.unidades = unidades;
    }

    public Set<Contato> getContatos() {
        return contatos;
    }

    public void setContatos(Set<Contato> contatos) {
        this.contatos = contatos;
    }

    public Set<CNAEEmpresa> getDivisoesCnae() {
        return divisoesCnae;
    }

    public void setDivisoesCnae(Set<CNAEEmpresa> divisoesCnae) {
        this.divisoesCnae = divisoesCnae;
    }

    public Set<CNAEEmpresa> getGruposCnae() {
        return gruposCnae;
    }

    public void setGruposCnae(Set<CNAEEmpresa> gruposCnae) {
        this.gruposCnae = gruposCnae;
    }

    public Set<CNAEEmpresa> getClassesCnae() {
        return classesCnae;
    }

    public void setClassesCnae(Set<CNAEEmpresa> classesCnae) {
        this.classesCnae = classesCnae;
    }

    public Set<CNAEEmpresa> getSubclassesCnae() {
        return subclassesCnae;
    }

    public void setSubclassesCnae(Set<CNAEEmpresa> subclassesCnae) {
        this.subclassesCnae = subclassesCnae;
    }
    
    public List<CNAEEmpresa> getDivisoesCnaePersistido() {
        return divisoesCnaePersistido;
    }

    public void setDivisoesCnaePersistido(List<CNAEEmpresa> divisoesCnaePersistido) {
        this.divisoesCnaePersistido = divisoesCnaePersistido;
    }

    public List<CNAEEmpresa> getGruposCnaePersistido() {
        return gruposCnaePersistido;
    }

    public void setGruposCnaePersistido(List<CNAEEmpresa> gruposCnaePersistido) {
        this.gruposCnaePersistido = gruposCnaePersistido;
    }

    public List<CNAEEmpresa> getClassesCnaePersistido() {
        return classesCnaePersistido;
    }

    public void setClassesCnaePersistido(List<CNAEEmpresa> classesCnaePersistido) {
        this.classesCnaePersistido = classesCnaePersistido;
    }

    public List<CNAEEmpresa> getSubclassesCnaePersistido() {
        return subclassesCnaePersistido;
    }

    public void setSubclassesCnaePersistido(List<CNAEEmpresa> subclassesCnaePersistido) {
        this.subclassesCnaePersistido = subclassesCnaePersistido;
    }
    
    public Set<NomeEmpresa> getNomesEmpresa() {
        return nomesEmpresa;
    }

    public void setNomesEmpresa(Set<NomeEmpresa> nomesEmpresa) {
        this.nomesEmpresa = nomesEmpresa;
    }

    public CadeiaProdutiva getCadeiaProdutiva() {
        return cadeiaProdutiva;
    }

    public void setCadeiaProdutiva(CadeiaProdutiva cadeiaProdutiva) {
        this.cadeiaProdutiva = cadeiaProdutiva;
    }

    public SituacaoEmpresa getSituacao() {
        return situacao;
    }

    public void setSituacao(SituacaoEmpresa situacao) {
        if(situacao == null) {
            return;
        }
        this.situacao = situacao;
        this.situacaoInt = (new SituacaoEmpresaEnumConverter()).convertToDatabaseColumn(situacao);
    }

    public String getJustificativaArquivamento() {
        return justificativaArquivamento;
    }

    public void setJustificativaArquivamento(String justificativaArquivamento) {
        this.justificativaArquivamento = justificativaArquivamento;
    }

    public String getCnpjFormatado() {
        return cnpjFormatado;
    }

    public Set<Projeto> getProjetos() {
        return projetos;
    }

    public void setProjetos(Set<Projeto> projetos) {
        this.projetos = projetos;
    }

    public void addProjeto(Projeto projeto) {
        getProjetos().add(projeto);
        projeto.setEmpresa(this);
    }

    public Long getResponsavelPersistidoId() {
        return responsavelPersistidoId;
    }

    public void setResponsavelPersistidoId(Long responsavelPersistidoId) {
        this.responsavelPersistidoId = responsavelPersistidoId;
    }

    public String getAssuntoFaleConosco() {
        return assuntoFaleConosco;
    }

    public void setAssuntoFaleConosco(String assuntoFaleConosco) {
        this.assuntoFaleConosco = assuntoFaleConosco;
    }

    public String getDescricaoFaleConosco() {
        return descricaoFaleConosco;
    }

    public void setDescricaoFaleConosco(String descricaoFaleConosco) {
        this.descricaoFaleConosco = descricaoFaleConosco;
    }

    public OrigemEmpresaEnum getOrigemEmpresa() {
        return origemEmpresa;
    }

    public void setOrigemEmpresa(OrigemEmpresaEnum origemEmpresa) {
        this.origemEmpresa = origemEmpresa;
        this.origemEmpresaInt = (new OrigemEmpresaEnumConverter()).convertToDatabaseColumn(origemEmpresa);
    }
    
    public Boolean isEmpresaNovaMG() {
        return empresaNovaMG;
    }

    public void setEmpresaNovaMG(Boolean empresaNovaMG) {
        this.empresaNovaMG = empresaNovaMG;
    }
    
    @PostLoad
    private void postLoad(){
        cnpjFormatado = MaskUtil.format("##.###.###/####-##", cnpj);
    }
        
    public void loadForEdition() {
        
        if (responsavel != null) {
            responsavelPersistidoId = responsavel.getId();
        }
        if(divisoesCnae != null) {
            divisoesCnaePersistido = new ArrayList<>(divisoesCnae);
        }
        if(gruposCnae != null) {
            gruposCnaePersistido = new ArrayList<>(gruposCnae);
        }
        if(classesCnae != null) {
            classesCnaePersistido = new ArrayList<>(classesCnae);
        }
        if(subclassesCnae != null) {
            subclassesCnaePersistido = new ArrayList<>(subclassesCnae);
        }
        if(situacao != null) {
            setSituacaoOld(situacao);
        }
    }

    public UsuarioExterno getResponsavel() {
        return responsavel;
    }

    public void setResponsavel(UsuarioExterno responsavel) {
        this.responsavel = responsavel;
    }

    public SituacaoEmpresa getSituacaoOld() {
        return situacaoOld;
    }

    public void setSituacaoOld(SituacaoEmpresa situacaoOld) {
        this.situacaoOld = situacaoOld;
    }

    public static class Query {

        public static final String updateSituacaoEmpresa = "Empresa.updateSituacaoEmpresa";
        public static final String deleteByid = "Empresa.deleteById";
        public static final String updateUsuarioExternoResponsavel = "Empresa.updateUsuarioExternoResponsavel";
        public static final String FIND_EMPRESA_BY_USUARIO_EXTERNO_RESPONSAVEL = "Empresa.findEmpresaByUsuarioExternoResponsavel";
        public static final String findAllByIds = "Empresa.findAllByIds";
        public static final String findNomeByCadeiaProdutivaGerencia = "Empresa.findNomeByCadeiaProdutivaGerencia";
        
    }

    public Set<ComposicaoSocietaria> getComposicaoSocietarias() {
        return composicaoSocietarias;
    }

    public void setComposicaoSocietarias(Set<ComposicaoSocietaria> composicaoSocietarias) {
        this.composicaoSocietarias = composicaoSocietarias;
    }

    public Pais getPaisOrigem() {
        return paisOrigem;
    }

    public void setPaisOrigem(Pais paisOrigem) {
        this.paisOrigem = paisOrigem;
    }

    public UF getEstadoOrigem() {
        return estadoOrigem;
    }

    public void setEstadoOrigem(UF estadoOrigem) {
        this.estadoOrigem = estadoOrigem;
    }
    

    public Integer getOrigemEmpresaInt() {
        return origemEmpresaInt;
    }

    public void setOrigemEmpresaInt(Integer origemEmpresaInt) {
        this.origemEmpresaInt = origemEmpresaInt;
        this.origemEmpresa = (new OrigemEmpresaEnumConverter()).convertToEntityAttribute(origemEmpresaInt);
    }

    public Integer getSituacaoInt() {
        return situacaoInt;
    }

    public void setSituacaoInt(Integer situacaoInt) {
        if(situacaoInt == null) {
            return;
        }
        this.situacaoInt = situacaoInt;
        this.situacao = (new SituacaoEmpresaEnumConverter()).convertToEntityAttribute(situacaoInt);
    }
}