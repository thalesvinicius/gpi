package br.com.gpi.server.to.report;

import br.com.gpi.server.domain.entity.UsoFonte;
import br.com.gpi.server.domain.entity.enumerated.EnumTipoInvestimento;
import static br.com.gpi.server.domain.entity.enumerated.EnumTipoInvestimento.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

/**
 *
 * @author rafaelbfs
 */
public class UsoFonteReportTO {

    public static final List<Pair<EnumTipoInvestimento, String>> listaUC07 = Arrays.asList(new ImmutablePair(INVESTIMENTO_TERRENO, "Terreno"),
            new ImmutablePair(INVESTIMENTO_ESTUDO_PROJETO_DESP_PRE_OPERACIONAIS, "Estudos/Projetos e Despesas Pré-Operacionais"),
            new ImmutablePair(INVESTIMENTO_OBRAS_CIVIS_INSTALACOES, "Obras Civis/Instalações"),
            new ImmutablePair(INVESTIMENTO_MAQ_EQUIP_NACIONAIS, "Maquinas/Equip. Nacionais"),
            new ImmutablePair(INVESTIMENTO_MAQ_EQUIP_IMPORTADOS, "Maquinas/Equip. Importados"),
            new ImmutablePair(INVESTIMENTO_MAQ_EQUIP_USADOS, "Maquinas/Equip. Usados"),
            new ImmutablePair(INVESTIMENTO_CAPITAL_GIRO, "Capital de Giro"),
            new ImmutablePair(OUTROS_INVESTIMENTOS, OUTROS_INVESTIMENTOS.getDescription()),
            new ImmutablePair(RECURSOS_PROPRIOS, RECURSOS_PROPRIOS.getDescription()),
            new ImmutablePair(BDMG, BDMG.getDescription()),
            new ImmutablePair(OUTRAS_FONTES, OUTRAS_FONTES.getDescription())
    );

    public static List<UsoFonteReportTO> toOrderedList(Collection<UsoFonte> col) {
        Map<EnumTipoInvestimento, UsoFonte> map = mapaDeLista(col);
        List<UsoFonteReportTO> reportObjects = new ArrayList<>(listaUC07.size());
        for (Pair<EnumTipoInvestimento, String> pair : listaUC07) {
            UsoFonte aux = map.get(pair.getLeft());
            reportObjects.add(new UsoFonteReportTO(pair.getRight(),
                    (aux == null) ? Double.valueOf(0D) : aux.getValorRealizado(),
                    (aux == null) ? Double.valueOf(0D) : aux.getValorARealizar()));

        }
        return reportObjects;
    }

    public static Map<EnumTipoInvestimento, UsoFonte> mapaDeLista(Collection<UsoFonte> col) {
        Map<EnumTipoInvestimento, UsoFonte> map = new HashMap<EnumTipoInvestimento, UsoFonte>();

        for (UsoFonte uso : col) {
            map.put(uso.getTipo(), uso);
        }
        return map;
    }

    private String descricao;
    private Double valorRealizado, valorARealizar;

    public UsoFonteReportTO(String descricao, Double valorRealizado, Double valorARealizar) {
        this.descricao = descricao;
        this.valorRealizado = valorRealizado;
        this.valorARealizar = valorARealizar;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Double getValorRealizado() {
        return valorRealizado;
    }

    public void setValorRealizado(Double valorRealizado) {
        this.valorRealizado = valorRealizado;
    }

    public Double getValorARealizar() {
        return valorARealizar;
    }

    public void setValorARealizar(Double valorARealizar) {
        this.valorARealizar = valorARealizar;
    }

    public Double getValorTotal() {
        Double valorARealizarAux = (valorARealizar != null) ? valorARealizar : new Double("0");
        Double valorRealizadoAux = (valorRealizado != null) ? valorRealizado : new Double("0");
        return valorARealizarAux + valorRealizadoAux;
    }

}
