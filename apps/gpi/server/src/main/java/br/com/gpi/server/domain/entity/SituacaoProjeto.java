package br.com.gpi.server.domain.entity;

import br.com.gpi.server.core.jpa.converter.SituacaoProjetoEnumConverter;
import br.com.gpi.server.domain.common.BaseEntity;
import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.apache.deltaspike.data.api.audit.ModifiedOn;
import org.hibernate.envers.Audited;

@Audited
@Entity
@Table(name = "HistoricoSituacaoProjeto")
@NamedQueries({
    @NamedQuery(name = SituacaoProjeto.Query.findAllByProjeto, query = "SELECT e FROM SituacaoProjeto e WHERE e.projetoId = ?1")
})
public class SituacaoProjeto extends BaseEntity<Long> {
    
    @JsonIgnore
    @ManyToOne(cascade = {CascadeType.DETACH, CascadeType.REFRESH}, fetch = FetchType.LAZY)
    @JoinColumn(name = "projeto_id")
    private Projeto projeto;
    
    @Column(name = "projeto_id", insertable = false, updatable = false)
    private Long projetoId;
    
    @Enumerated
    @Column(name = "situacaoProjeto_id", insertable = false, updatable = false)
    private SituacaoProjetoEnum situacao;
    
    @JsonIgnore
    @Column(name = "situacaoProjeto_id")
    private Integer situacaoInt;
    
    @Column(name = "data")
    @Temporal(TemporalType.TIMESTAMP)
    @ModifiedOn(onCreate = true)
    private Date dataSituacao;
    
    private String justificativa;
    
    public SituacaoProjeto() {
    }

    public SituacaoProjeto(SituacaoProjetoEnum situacao, Long projetoId, String justificativa) {
        setSituacao(situacao);
        this.projetoId = projetoId;
        //setando o id no projeto para o cascade
        this.projeto = new Projeto();
        projeto.setId(projetoId);
        this.justificativa = justificativa;
    }
    
    public SituacaoProjeto(Projeto projeto) {
        setSituacao(projeto.getSituacao());
        this.projetoId = projeto.getId();
        this.projeto = projeto;
        this.justificativa = projeto.getJustificativa();
    }

    public Projeto getProjeto() {
        return projeto;
    }

    public void setProjeto(Projeto projeto) {
        this.projeto = projeto;
    }

    public Long getProjetoId() {
        return projetoId;
    }

    public void setProjetoId(Long projetoId) {
        this.projetoId = projetoId;
    }

    public SituacaoProjetoEnum getSituacao() {
        return situacao;
    }

    public void setSituacao(SituacaoProjetoEnum situacao) {
        this.situacao = situacao;
        this.situacaoInt = (new SituacaoProjetoEnumConverter()).convertToDatabaseColumn(situacao);
    }

    public Integer getSituacaoInt() {
        return situacaoInt;
    }

    public void setSituacaoInt(Integer situacaoInt) {
        this.situacaoInt = situacaoInt;
        this.situacao = (new SituacaoProjetoEnumConverter()).convertToEntityAttribute(situacaoInt);
    }
    
    public Date getDataSituacao() {
        return dataSituacao;
    }

    public void setDataSituacao(Date dataSituacao) {
        this.dataSituacao = dataSituacao;
    }

    public String getJustificativa() {
        return justificativa;
    }

    public void setJustificativa(String justificativa) {
        this.justificativa = justificativa;
    }
    
    public static class Query {

        public static final String findAllByProjeto = "SituacaoProjeto.findAllByProjeto";

        public Query() {
        }
    }

    public String getSituacaoDescricao() {
        return situacao.getDescription();
    }
}
