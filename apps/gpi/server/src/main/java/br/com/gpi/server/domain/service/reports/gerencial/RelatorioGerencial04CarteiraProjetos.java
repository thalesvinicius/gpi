/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.gpi.server.domain.service.reports.gerencial;

import br.com.gpi.server.domain.common.QueryParameter;
import br.com.gpi.server.domain.entity.Estagio;
import br.com.gpi.server.domain.entity.SituacaoProjetoEnum;
import br.com.gpi.server.domain.service.reports.BaseReportService;
import br.com.gpi.server.domain.view.RelatorioCarteiraProjeto;
import br.com.gpi.server.domain.view.RelatorioCarteiraProjeto_;
import br.com.gpi.server.util.EnumMimeTypes;
import br.com.gpi.server.util.functional.FunctionalInterfaces;
import br.com.gpi.server.util.reports.ReportUtil;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.ws.rs.core.Response;
import net.sf.dynamicreports.jasper.builder.JasperReportBuilder;
import static net.sf.dynamicreports.report.builder.DynamicReports.col;
import static net.sf.dynamicreports.report.builder.DynamicReports.type;
import static net.sf.dynamicreports.report.builder.DynamicReports.stl;
import net.sf.dynamicreports.report.builder.column.ColumnBuilder;
import net.sf.dynamicreports.report.constant.HorizontalAlignment;
import net.sf.dynamicreports.report.constant.VerticalAlignment;
import net.sf.dynamicreports.report.datasource.DRDataSource;
import net.sf.jasperreports.engine.JRDataSource;

/**
 *
 * @author rafaelbfs
 */
public class RelatorioGerencial04CarteiraProjetos extends BaseReportService<Object>{

       
    @Inject
    public RelatorioGerencial04CarteiraProjetos(EntityManager em) {
        super(em);
    }
    
    public JasperReportBuilder buildCarteira(final String format, List<RelatorioCarteiraProjeto> result,
            Supplier<Map<String,Object>> parametros) throws FileNotFoundException, IOException {
        
        
        
        String titulo = "Relatório Carteira de Projetos";
        
        // Colunas do estagio
        ColumnBuilder[] cb = {col.column("Empresa", "nomeEmpresa", type.stringType()).setWidth(13),
            col.column("Projeto", "nomeProjeto", type.stringType()).setWidth(20),
            col.column("Cidade", "enderecoEmpresa.municipio.nome", type.stringType()).setWidth(10),
            col.column("Região Planejamento", "localizacao.regiaoPlanejamento.nome", type.stringType()).setWidth(9),
            col.column("Investimento R$(mil)", "valorInvestimento", type.doubleType()).setPattern("#,##0.00").setWidth(8),
            col.column("Fat. Inicial R$(mil)", "valorFaturamentoInicial", type.doubleType()).setPattern("#,##0.00").setWidth(8),
            col.column("Fat. Final R$(mil)", "valorFaturamentoFinal", type.doubleType()).setPattern("#,##0.00").setWidth(8),
            col.column("Empregos Diretos", "empregosDiretos", type.integerType()).setWidth(6),
            col.column("Empregos Indiretos", "empregosIndiretos", type.integerType()).setWidth(6),
            col.column("Cadeia Produtiva", "cadeiaProdutiva.descricao", type.stringType()).setHorizontalAlignment(HorizontalAlignment.CENTER).setWidth(10).setStyle(stl.style().setVerticalAlignment(VerticalAlignment.MIDDLE)),
            col.column("Status Atual", "situacao", type.stringType()).setWidth(5),
            col.column("Estágio    Atual", "estagio", type.stringType()).setWidth(7),
            col.column("Analista", "analista", type.stringType()).setWidth(8),
            col.column("Gerência", "gerente", type.stringType()).setWidth(6)};      
        Supplier<JasperReportBuilder> initializer = () -> defaultInit.init(cb, getCarteiraDataSource(result));
        
        Supplier<Map<String, Object>> parametrosFinais = () -> {
            Map<String, Object> params = parametros.get();
            //params.putAll(gerarParametrosTotais(result));
            params.put("LOGO_INDI",getImage(ReportUtil.LOGO_INDI_NOME));
            return params;
        };
        
        return generatorChain(initializer,
                resolveHeader(format, parametrosFinais.get()),
                createDefaultSummary(format, parametrosFinais.get()));
    }
    
    public Response generateReport(String format,  List<Estagio> estagios, List<Long> cadeiasProdutivas,
             List<SituacaoProjetoEnum> situacoesProjeto, List<Long> departamentos, List<Long> regioesPlanejamento,
             List<Long> usuariosResponsaveis, Supplier<Map<String,Object>> parametros) throws FileNotFoundException, IOException {
        
        QueryParameter filtros = QueryParameter.with(
             RelatorioCarteiraProjeto.Filtro.ESTAGIO, estagios)
            .and(RelatorioCarteiraProjeto.Filtro.ANALISTA,usuariosResponsaveis)
            .and(RelatorioCarteiraProjeto.Filtro.CADEIA_PRODUTIVA,cadeiasProdutivas)
            .and(RelatorioCarteiraProjeto.Filtro.GERENCIA,departamentos)
            .and(RelatorioCarteiraProjeto.Filtro.STATUS_ATUAL,situacoesProjeto.stream().map(SituacaoProjetoEnum::getId).collect(Collectors.toList()))
            .and(RelatorioCarteiraProjeto.Filtro.REGIAO,regioesPlanejamento);
                    
        List<RelatorioCarteiraProjeto> result = generate(filtros);
        Map<String, Object> params = parametros.get();
        params.putAll(gerarParametrosTotais(result));
        final JasperReportBuilder finalReport = buildCarteira(format, result, ()-> params);
        final EnumMimeTypes mimetype = EnumMimeTypes.valueOf(format.toUpperCase());
        
        
        FunctionalInterfaces.BinarySupplier sup = dynamicReportsBinarySupplier(finalReport, ReportUtil.CABECALHO_REL_CARTEIRA_PROJETO, 
                ReportUtil.TOTAIS_RELATORIO_CART_PROJETOS, mimetype, params);
        return buildResponse(sup,mimetype.mimeType);        
    }
    
    private Map<String,Object> gerarParametrosTotais(List<RelatorioCarteiraProjeto> result){
        final double ZERO = BigDecimal.ZERO.doubleValue();
        Map<String,Object> parametros = new HashMap<>();
        parametros.put(ReportUtil.TOTAL_INVESTIMENTO,result.stream().collect(Collectors.summingDouble((r)-> r.getInvestimentoPrevisto() != null ? r.getInvestimentoPrevisto() : ZERO )));
        parametros.put(ReportUtil.TOTAL_FATURAMENTO_INICIAL,result.stream().collect(Collectors.summingDouble((r)-> r.getFaturamentoInicial() != null ? r.getFaturamentoInicial() : ZERO)));
        parametros.put(ReportUtil.TOTAL_EMPREGOS_DIRETOS,result.stream().collect(Collectors.summingInt((r)-> Optional.ofNullable(r.getEmpregosDiretos()).orElse(0))).longValue());
        parametros.put(ReportUtil.TOTAL_EMPREGOS_INDIRETOS,result.stream().collect(Collectors.summingInt((r)-> Optional.ofNullable(r.getEmpregosIndiretos()).orElse(0))).longValue());
        parametros.put(ReportUtil.TOTAL_QUANTIDADE_PROJETOS,result.stream().collect(Collectors.counting()));
        parametros.put(ReportUtil.TOTAL_FATURAMENTO_FINAL,result.stream().collect(Collectors.summingDouble((r)-> r.getFaturamentoFinal() != null ? r.getFaturamentoFinal() : ZERO)));
        
        return parametros;
    }
    
    
    
    public List<RelatorioCarteiraProjeto> generate(QueryParameter filtros) {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<RelatorioCarteiraProjeto> query = builder.createQuery(RelatorioCarteiraProjeto.class).distinct(true);
        Root<RelatorioCarteiraProjeto> fromRelatorioCateira = query.from(RelatorioCarteiraProjeto.class);

        List<Predicate> conditions = new ArrayList<>();
        
        builFiltersConditions(fromRelatorioCateira, conditions, builder, filtros);
        
        TypedQuery<RelatorioCarteiraProjeto> typedQuery = entityManager.createQuery(query
                .where(conditions.toArray(new Predicate[]{}))
                .orderBy(builder.asc(fromRelatorioCateira.get(RelatorioCarteiraProjeto_.empresa)))
        );        

        return typedQuery.getResultList();
    }
    

    private void builFiltersConditions(Root<RelatorioCarteiraProjeto> fromRelatorioCateira, List<Predicate> conditions, CriteriaBuilder builder, QueryParameter filtros) {
        if(filtros.exists(RelatorioCarteiraProjeto.Filtro.ESTAGIO)) {
            conditions.add(fromRelatorioCateira.get(RelatorioCarteiraProjeto_.estagioAtual).in(filtros.get(RelatorioCarteiraProjeto.Filtro.ESTAGIO, List.class)));
        }
        
        if(filtros.exists(RelatorioCarteiraProjeto.Filtro.CADEIA_PRODUTIVA)) {
            conditions.add(fromRelatorioCateira.get(RelatorioCarteiraProjeto_.cadeiaProdutivaId).in(filtros.get(RelatorioCarteiraProjeto.Filtro.CADEIA_PRODUTIVA, List.class)));            
        }

        if(filtros.exists(RelatorioCarteiraProjeto.Filtro.STATUS_ATUAL)) {
            conditions.add(fromRelatorioCateira.get(RelatorioCarteiraProjeto_.situacaoProjeto).in(filtros.get(RelatorioCarteiraProjeto.Filtro.STATUS_ATUAL, List.class)));
        }
        
        if(filtros.exists(RelatorioCarteiraProjeto.Filtro.REGIAO)) {
            conditions.add(fromRelatorioCateira.get(RelatorioCarteiraProjeto_.regiaoPlanejamentoId).in(filtros.get(RelatorioCarteiraProjeto.Filtro.REGIAO, List.class)));            
        }

        if(filtros.exists(RelatorioCarteiraProjeto.Filtro.GERENCIA)) {
            conditions.add(fromRelatorioCateira.get(RelatorioCarteiraProjeto_.gerenciaId).in(filtros.get(RelatorioCarteiraProjeto.Filtro.GERENCIA, List.class)));            
            if(filtros.exists(RelatorioCarteiraProjeto.Filtro.ANALISTA)) {
                conditions.add(fromRelatorioCateira.get(RelatorioCarteiraProjeto_.analistaId).in(filtros.get(RelatorioCarteiraProjeto.Filtro.ANALISTA, List.class)));            
            }
        } else {
            if(filtros.exists(RelatorioCarteiraProjeto.Filtro.ANALISTA)) {
                conditions.add(fromRelatorioCateira.get(RelatorioCarteiraProjeto_.analistaId).in(filtros.get(RelatorioCarteiraProjeto.Filtro.ANALISTA, List.class)));
            }
        }
    }   
    
    private JRDataSource getCarteiraDataSource(List<RelatorioCarteiraProjeto> result) {
        
        DRDataSource dataSource = new DRDataSource("nomeEmpresa", "nomeProjeto", "enderecoEmpresa.municipio.nome", "localizacao.regiaoPlanejamento.nome", "valorInvestimento", "valorFaturamentoInicial", "valorFaturamentoFinal", "empregosDiretos", "empregosIndiretos", "cadeiaProdutiva.descricao", "situacao", "estagio", "analista", "gerente");
        result.stream().forEach((to) -> {
            dataSource.add(getLinhaCarteira(to));
        });
        return dataSource;
    }
    
    private Object[] getLinhaCarteira(RelatorioCarteiraProjeto to) {
        Object[] linha = {to.getEmpresa(), to.getProjeto(), to.getCidade(),
            to.getRegiaoPlanejamento(), to.getInvestimentoPrevisto(), to.getFaturamentoInicial(), to.getFaturamentoFinal(), 
            to.getEmpregosDiretos(), to.getEmpregosIndiretos(), to.getCadeiaProdutiva(), to.getSituacaoProjeto(),
            to.getEstagioAtual(), to.getAnalista(), to.getGerencia()};
        return linha;
    }      
}
