package br.com.gpi.server.querybuilder;

import java.util.Arrays;
import java.util.List;

public class StandardConditionInfo extends ConditionInfo {

    private List<Field> fields;

    public StandardConditionInfo(Class instance, ConditionValueTypeEnum type, ConditionComparisonTypeEnum comparisonType, Field... fields) {
        super(instance, type, comparisonType);
        this.fields = Arrays.asList(fields);
    }
    
    public StandardConditionInfo(Class instance, ConditionValueTypeEnum type, Field... fields) {
        super(instance, type);
        this.fields = Arrays.asList(fields);
    }

    @Override
    public List<Field> getFields() {
        return fields;
    }

    public void setFields(List<Field> fields) {
        this.fields = fields;
    }
}
