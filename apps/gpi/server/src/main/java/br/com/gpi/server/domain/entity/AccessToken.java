package br.com.gpi.server.domain.entity;

import br.com.gpi.server.domain.common.BaseEntity;
import br.com.gpi.server.domain.user.model.GPISecurityContext;
import br.com.gpi.server.domain.user.model.SecurityUser;
import com.fasterxml.jackson.annotation.JsonIgnore;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.ws.rs.core.SecurityContext;

/**
 * @author <a href="mailto:pviniciusfm@gmail.com">Paulo Vinicius F. Machado</a>
 */
@Entity
@Table(name = "AccessToken")
@NamedQueries({
    @NamedQuery(name = AccessToken.Query.locateByToken, query = "SELECT a FROM AccessToken a WHERE a.token = :token")
})
public class AccessToken extends BaseEntity<Long> implements Comparable<AccessToken> {

    private static final long serialVersionUID = 1L;

    @Column(length = 36)
    private String token;

    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date timeCreated;
    
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date lastUpdated;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id")
    @JsonIgnore
    private User user;

    public AccessToken() {
    }

    public AccessToken(User user, String token) {
        this.user = user;
        this.token = token;
        this.timeCreated = new Date();
        this.lastUpdated = new Date();
    }

    public String getToken() {
        return token;
    }

    public User getUser() {
        return user;
    }
    
    public void setUser(User user) {
        this.user = user;
    }

    public LocalDateTime getTimeCreated() {
         if(timeCreated == null){
            return null;
        }
        Instant instant = Instant.ofEpochMilli(timeCreated.getTime());
        return LocalDateTime.ofInstant(instant, ZoneOffset.UTC);
    }

    @Override
    public int compareTo(AccessToken userSession) {
        Instant instant = userSession.getLastUpdated().toInstant(ZoneOffset.UTC);
        return this.lastUpdated.compareTo(Date.from(instant));
    }

    public LocalDateTime getLastUpdated() {
        if(lastUpdated == null){
            return null;
        }
        Instant instant = Instant.ofEpochMilli(lastUpdated.getTime());
        return LocalDateTime.ofInstant(instant, ZoneOffset.UTC);
    }

    public void setLastUpdated(LocalDateTime lastUpdated) {
        if (lastUpdated == null){
            this.lastUpdated = null;
        }else{
        Instant instant = lastUpdated.toInstant(ZoneOffset.UTC);
            this.lastUpdated = Date.from(instant);
        }
    }

    public void setTimeCreated(LocalDateTime timeCreated) {
        if (timeCreated == null){
            this.timeCreated = null;
        }else{
            Instant instant = timeCreated.toInstant(ZoneOffset.UTC);
            this.timeCreated = Date.from(instant);
        }
    }

   

    public SecurityContext getSecurityContext() {
        return new GPISecurityContext(new SecurityUser(getUser()));
    }

    public static class Query {

        public static final String locateByToken = "AccessToken.locateByToken";
    }
}
