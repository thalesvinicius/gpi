package br.com.gpi.server.domain.entity;

import java.io.Serializable;

public class PesquisaPerguntaPesquisaPK implements Serializable {

    private long pesquisaId;
    private long perguntaPesquisaId;

    public long getPesquisaId() {
        return pesquisaId;
    }

    public void setPesquisaId(long pesquisaId) {
        this.pesquisaId = pesquisaId;
    }

    public long getPerguntaPesquisaId() {
        return perguntaPesquisaId;
    }

    public void setPerguntaPesquisaId(long perguntaPesquisaId) {
        this.perguntaPesquisaId = perguntaPesquisaId;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 89 * hash + (int) (this.pesquisaId ^ (this.pesquisaId >>> 32));
        hash = 89 * hash + (int) (this.perguntaPesquisaId ^ (this.perguntaPesquisaId >>> 32));
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final PesquisaPerguntaPesquisaPK other = (PesquisaPerguntaPesquisaPK) obj;
        if (this.pesquisaId != other.pesquisaId) {
            return false;
        }
        if (this.perguntaPesquisaId != other.perguntaPesquisaId) {
            return false;
        }
        return true;
    }

}
