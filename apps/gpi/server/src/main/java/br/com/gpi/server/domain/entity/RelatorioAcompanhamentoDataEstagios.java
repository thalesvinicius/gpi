package br.com.gpi.server.domain.entity;

import br.com.gpi.server.core.jpa.converter.SituacaoProjetoEnumConverter;
import br.com.gpi.server.domain.common.BaseEntity;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.hibernate.envers.Audited;

@Entity
@Table(name = "VIEW_RELATORIO_ACOMPANHAMENTO_DATAS_ESTAGIOS")
public class RelatorioAcompanhamentoDataEstagios extends BaseEntity<Long> {

    @Column(name = "empresa_id")
    private Long empresaId;

    @Column(name = "projeto_id")
    private Long projetoId;

    @Column(name = "cadeiaProdutiva_id")
    private Long cadeiaProdutivaId;

    @Column(name = "usuarioResponsavel_id")
    private Long usuarioResponsavelId;

    @Column(name = "regiaoPlanejamento_id")
    private Long regiaoPlanejamentoId;

    @Column(name = "departamento_id")
    private Long departamentoId;

    @Basic(optional = false)
    private Boolean ultimaVersao;
    
    private Integer versao;

    private String nomeEmpresa;

    private String nomeProjeto;

    private String cidade;

    private String regiaoPlanejamento;

    private Double valorInvestimento;

    private Double faturamentoInicial;

    private Double faturamentoFinal;

    private Integer empregosDiretos;

    private Integer empregosIndiretos;

    private String cadeiaProdutiva;

    @Enumerated
    @Convert(converter = SituacaoProjetoEnumConverter.class)
    private SituacaoProjetoEnum situacaoAtual;

    @Enumerated
    @Convert(converter = br.com.gpi.server.core.jpa.converter.EstagioEnumConverter.class)
    private Estagio estagioAtual;

    private String analista;

    private String departamento;
    
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataPrevistaDF;

    @Temporal(TemporalType.TIMESTAMP)
    private Date dataPrevistaII;

    @Temporal(TemporalType.TIMESTAMP)
    private Date dataPrevistaOI;

    @Temporal(TemporalType.TIMESTAMP)
    private Date dataRealIP;

    @Temporal(TemporalType.TIMESTAMP)
    private Date dataRealPP;

    @Temporal(TemporalType.TIMESTAMP)
    private Date dataRealDF;

    @Temporal(TemporalType.TIMESTAMP)
    private Date dataRealII;

    @Temporal(TemporalType.TIMESTAMP)
    private Date dataRealOI;

    @Temporal(TemporalType.TIMESTAMP)
    private Date dataRealCC;

    public Long getEmpresaId() {
        return empresaId;
    }

    public void setEmpresaId(Long empresaId) {
        this.empresaId = empresaId;
    }

    public Long getProjetoId() {
        return projetoId;
    }

    public void setProjetoId(Long projetoId) {
        this.projetoId = projetoId;
    }

    public Long getCadeiaProdutivaId() {
        return cadeiaProdutivaId;
    }

    public void setCadeiaProdutivaId(Long cadeiaProdutivaId) {
        this.cadeiaProdutivaId = cadeiaProdutivaId;
    }

    public Long getUsuarioResponsavelId() {
        return usuarioResponsavelId;
    }

    public void setUsuarioResponsavelId(Long usuarioResponsavelId) {
        this.usuarioResponsavelId = usuarioResponsavelId;
    }

    public Long getRegiaoPlanejamentoId() {
        return regiaoPlanejamentoId;
    }

    public void setRegiaoPlanejamentoId(Long regiaoPlanejamentoId) {
        this.regiaoPlanejamentoId = regiaoPlanejamentoId;
    }

    public Long getDepartamentoId() {
        return departamentoId;
    }

    public void setDepartamentoId(Long departamentoId) {
        this.departamentoId = departamentoId;
    }

    public Boolean getUltimaVersao() {
        return ultimaVersao;
    }

    public void setUltimaVersao(Boolean ultimaVersao) {
        this.ultimaVersao = ultimaVersao;
    }

    public Integer getVersao() {
        return versao;
    }

    public void setVersao(Integer versao) {
        this.versao = versao;
    }

    public String getNomeEmpresa() {
        return nomeEmpresa;
    }

    public void setNomeEmpresa(String nomeEmpresa) {
        this.nomeEmpresa = nomeEmpresa;
    }

    public String getNomeProjeto() {
        return nomeProjeto;
    }

    public void setNomeProjeto(String nomeProjeto) {
        this.nomeProjeto = nomeProjeto;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public String getRegiaoPlanejamento() {
        return regiaoPlanejamento;
    }

    public void setRegiaoPlanejamento(String regiaoPlanejamento) {
        this.regiaoPlanejamento = regiaoPlanejamento;
    }

    public Double getValorInvestimento() {
        return valorInvestimento;
    }

    public void setValorInvestimento(Double valorInvestimento) {
        this.valorInvestimento = valorInvestimento;
    }

    public Double getFaturamentoInicial() {
        return faturamentoInicial;
    }

    public void setFaturamentoInicial(Double faturamentoInicial) {
        this.faturamentoInicial = faturamentoInicial;
    }

    public Double getFaturamentoFinal() {
        return faturamentoFinal;
    }

    public void setFaturamentoFinal(Double faturamentoFinal) {
        this.faturamentoFinal = faturamentoFinal;
    }

    public Integer getEmpregosDiretos() {
        return empregosDiretos;
    }

    public void setEmpregosDiretos(Integer empregosDiretos) {
        this.empregosDiretos = empregosDiretos;
    }

    public Integer getEmpregosIndiretos() {
        return empregosIndiretos;
    }

    public void setEmpregosIndiretos(Integer empregosIndiretos) {
        this.empregosIndiretos = empregosIndiretos;
    }

    public String getCadeiaProdutiva() {
        return cadeiaProdutiva;
    }

    public void setCadeiaProdutiva(String cadeiaProdutiva) {
        this.cadeiaProdutiva = cadeiaProdutiva;
    }

    public SituacaoProjetoEnum getSituacaoAtual() {
        return situacaoAtual;
    }

    public void setSituacaoAtual(SituacaoProjetoEnum situacaoAtual) {
        this.situacaoAtual = situacaoAtual;
    }

    public Estagio getEstagioAtual() {
        return estagioAtual;
    }

    public void setEstagioAtual(Estagio estagioAtual) {
        this.estagioAtual = estagioAtual;
    }

    public String getAnalista() {
        return analista;
    }

    public void setAnalista(String analista) {
        this.analista = analista;
    }

    public String getDepartamento() {
        return departamento;
    }

    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    public Date getDataPrevistaDF() {
        return dataPrevistaDF;
    }

    public void setDataPrevistaDF(Date dataPrevistaDF) {
        this.dataPrevistaDF = dataPrevistaDF;
    }

    public Date getDataPrevistaII() {
        return dataPrevistaII;
    }

    public void setDataPrevistaII(Date dataPrevistaII) {
        this.dataPrevistaII = dataPrevistaII;
    }

    public Date getDataPrevistaOI() {
        return dataPrevistaOI;
    }

    public void setDataPrevistaOI(Date dataPrevistaOI) {
        this.dataPrevistaOI = dataPrevistaOI;
    }

    public Date getDataRealIP() {
        return dataRealIP;
    }

    public void setDataRealIP(Date dataRealIP) {
        this.dataRealIP = dataRealIP;
    }

    public Date getDataRealPP() {
        return dataRealPP;
    }

    public void setDataRealPP(Date dataRealPP) {
        this.dataRealPP = dataRealPP;
    }

    public Date getDataRealDF() {
        return dataRealDF;
    }

    public void setDataRealDF(Date dataRealDF) {
        this.dataRealDF = dataRealDF;
    }

    public Date getDataRealII() {
        return dataRealII;
    }

    public void setDataRealII(Date dataRealII) {
        this.dataRealII = dataRealII;
    }

    public Date getDataRealOI() {
        return dataRealOI;
    }

    public void setDataRealOI(Date dataRealOI) {
        this.dataRealOI = dataRealOI;
    }

    public Date getDataRealCC() {
        return dataRealCC;
    }

    public void setDataRealCC(Date dataRealCC) {
        this.dataRealCC = dataRealCC;
    }
    
    public String getSituacaoAtualDescricao() {
        return this.situacaoAtual.getDescription();
    }
    
    public String getEstagioAtualDescricao() {
        return this.estagioAtual.getDescricao();
    }

}
