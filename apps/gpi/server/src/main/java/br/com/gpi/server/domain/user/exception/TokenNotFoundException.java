package br.com.gpi.server.domain.user.exception;

import br.com.gpi.server.core.exception.BaseWebApplicationException;

/**
 * @author <a href="mailto:pviniciusfm@gmail.com">Paulo Vinicius F. Machado</a>
 * @version 1.0
 */
public class TokenNotFoundException extends BaseWebApplicationException {

    private static final long serialVersionUID = 1L;

    public TokenNotFoundException() {
        super(404, "40407", "user.token.invalid", "No token could be found for that Id");
    }
}
