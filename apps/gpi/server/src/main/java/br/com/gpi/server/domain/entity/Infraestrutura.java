package br.com.gpi.server.domain.entity;

import br.com.gpi.server.domain.common.AuditedEntity;
import com.fasterxml.jackson.annotation.JsonIgnore;
import java.math.BigDecimal;
import java.util.List;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import org.hibernate.envers.Audited;

@Audited
@Entity
@Table(name = "Infraestrutura")
@NamedQueries({
    @NamedQuery(name = Infraestrutura.Query.locateByProjetoId, query = "SELECT i FROM Infraestrutura i WHERE i.projeto.id = ?")
})
public class Infraestrutura extends AuditedEntity<Long> {

    @JsonIgnore
    @OneToOne
    @JoinColumn(name = "projeto_id")
    private Projeto projeto;
    private BigDecimal areaNecessaria;
    private BigDecimal areaConstruida;
    private BigDecimal potenciaEnergeticaEstimada;
    private BigDecimal consumoAguaEstimado;
    private BigDecimal consumoCombustivelEstimado;
    private BigDecimal consumoGasEstimado;
    private BigDecimal geracaoEnergiaPropria;
    private BigDecimal geracaoEnergiaVenda;
    private BigDecimal demandaEnergiaEstimada;
    private BigDecimal ofertaEnergiaVendaEstimada;
    private Boolean possuiTerreno;
    private Boolean contatoCemig;
    private Boolean areaAlagada;
    private Boolean energiaJaContratada;
    
    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, mappedBy = "infraestrutura",orphanRemoval = true)
    private Set<EnergiaContratada> energiaContratadas;

    public Projeto getProjeto() {
        return projeto;
    }

    public void setProjeto(Projeto projeto) {
        this.projeto = projeto;
    }

    public BigDecimal getAreaNecessaria() {
        return areaNecessaria;
    }

    public void setAreaNecessaria(BigDecimal areaNecessaria) {
        this.areaNecessaria = areaNecessaria;
    }

    public BigDecimal getAreaConstruida() {
        return areaConstruida;
    }

    public void setAreaConstruida(BigDecimal areaConstruida) {
        this.areaConstruida = areaConstruida;
    }

    public BigDecimal getPotenciaEnergeticaEstimada() {
        return potenciaEnergeticaEstimada;
    }

    public void setPotenciaEnergeticaEstimada(BigDecimal potenciaEnergeticaEstimada) {
        this.potenciaEnergeticaEstimada = potenciaEnergeticaEstimada;
    }

    public BigDecimal getConsumoAguaEstimado() {
        return consumoAguaEstimado;
    }

    public void setConsumoAguaEstimado(BigDecimal consumoAguaEstimado) {
        this.consumoAguaEstimado = consumoAguaEstimado;
    }

    public BigDecimal getConsumoCombustivelEstimado() {
        return consumoCombustivelEstimado;
    }

    public void setConsumoCombustivelEstimado(BigDecimal consumoCombustivelEstimado) {
        this.consumoCombustivelEstimado = consumoCombustivelEstimado;
    }

    public BigDecimal getConsumoGasEstimado() {
        return consumoGasEstimado;
    }

    public void setConsumoGasEstimado(BigDecimal consumoGasEstimado) {
        this.consumoGasEstimado = consumoGasEstimado;
    }

    public BigDecimal getGeracaoEnergiaPropria() {
        return geracaoEnergiaPropria;
    }

    public void setGeracaoEnergiaPropria(BigDecimal geracaoEnergiaPropria) {
        this.geracaoEnergiaPropria = geracaoEnergiaPropria;
    }

    public BigDecimal getGeracaoEnergiaVenda() {
        return geracaoEnergiaVenda;
    }

    public void setGeracaoEnergiaVenda(BigDecimal geracaoEnergiaVenda) {
        this.geracaoEnergiaVenda = geracaoEnergiaVenda;
    }

    public BigDecimal getDemandaEnergiaEstimada() {
        return demandaEnergiaEstimada;
    }

    public void setDemandaEnergiaEstimada(BigDecimal demandaEnergiaEstimada) {
        this.demandaEnergiaEstimada = demandaEnergiaEstimada;
    }

    public BigDecimal getOfertaEnergiaVendaEstimada() {
        return ofertaEnergiaVendaEstimada;
    }

    public void setOfertaEnergiaVendaEstimada(BigDecimal ofertaEnergiaVendaEstimada) {
        this.ofertaEnergiaVendaEstimada = ofertaEnergiaVendaEstimada;
    }

    public Boolean isPossuiTerreno() {
        return possuiTerreno;
    }
    
    public Boolean getPossuiTerreno(){
        return possuiTerreno;
    }

    public void setPossuiTerreno(Boolean possuiTerreno) {
        this.possuiTerreno = possuiTerreno;
    }

    public Boolean isContatoCemig() {
        return contatoCemig;
    }
    
    public Boolean getContatoCemig() {
        return contatoCemig;
    }

    public void setContatoCemig(Boolean contatoCemig) {
        this.contatoCemig = contatoCemig;
    }

    public Boolean isAreaAlagada() {
        return areaAlagada;
    }
    
    public Boolean getAreaAlagada() {
        return areaAlagada;
    }

    public void setAreaAlagada(Boolean areaAlagada) {
        this.areaAlagada = areaAlagada;
    }

    public Boolean isEnergiaJaContratada() {
        return energiaJaContratada;
    }
    
    public Boolean getEnergiaJaContratada() {
        return energiaJaContratada;
    }

    public void setEnergiaJaContratada(Boolean energiaJaContratada) {
        this.energiaJaContratada = energiaJaContratada;
    }

    public Set<EnergiaContratada> getEnergiaContratadas() {
        return energiaContratadas;
    }

    public void setEnergiaContratadas(Set<EnergiaContratada> energiaContratadas) {
        this.energiaContratadas = energiaContratadas;
    }

    public static class Query {
        public static final String locateByProjetoId = "Infraestrutura.locateByProjetoId";
    }

}
