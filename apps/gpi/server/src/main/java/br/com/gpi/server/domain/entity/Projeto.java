package br.com.gpi.server.domain.entity;

import br.com.gpi.server.core.jpa.converter.EstagioEnumConverter;
import br.com.gpi.server.core.jpa.converter.SituacaoProjetoEnumConverter;
import br.com.gpi.server.domain.common.AuditedEntity;
import br.com.gpi.server.domain.entity.Projeto.Query;
import br.com.gpi.server.to.EmpresaUnidadeDTO;
import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.OrderBy;
import javax.persistence.PostLoad;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

@Entity
@NamedQueries({
    @NamedQuery(name = Query.countByResponsavelInvestimentoInList, query = "SELECT count(p) FROM Projeto p WHERE p.usuarioInvestimento.id IN ?1 "),
    @NamedQuery(name = Query.findVersoesAtuaisByVersaoPai, query = "SELECT p FROM Projeto p WHERE p.versaoPai in ?1 and p.ultimaVersao = true"),
    @NamedQuery(name = Query.updateEstagioProjeto, query = "update Projeto p set p.estagioAtual = ?1, p.usuarioResponsavel = ?3, p.dataUltimaAlteracao = ?4 where id = ?2"),
    @NamedQuery(name = Query.updateSituacaoProjeto, query = "update Projeto p set p.situacaoAtual = ?1, p.situacao = ?5, p.usuarioResponsavel = ?3, p.dataUltimaAlteracao = ?4 where id = ?2"),
    @NamedQuery(name = Query.updateUltimaAtividade, query = "update Projeto p set p.ultimaAtividade = ?1 where p.id = ?2"),
    @NamedQuery(name = Query.updateUsuarioeData, query = "update Projeto p set p.usuarioResponsavel = ?2, p.dataUltimaAlteracao = ?3 where p.id = ?1"),
    @NamedQuery(name = Query.findSituacaoAtualEnumByProjetoId, query = "SELECT p.situacaoAtual.situacao FROM Projeto p where p.id = ?1"),
    @NamedQuery(name = Query.findByVersaoAndSituacaoNotEqualsAndUltimaVersaoAndVersaoPai, query = "SELECT p FROM Projeto p where  p.versao = ?1 AND p.situacao <> ?2 AND p.ultimaVersao = ?3 AND p.versaoPai = ?4"),
    @NamedQuery(name = Query.findBySituacaoEUsuarioInvestimento, query = "SELECT p.nome, p.id FROM Projeto p where p.situacaoAtual.situacao =  ?1 and p.usuarioInvestimento.id = ?2 and p.ultimaVersao = true"),
    @NamedQuery(name = Query.findByUsuarioInvestimentoProjEmEstagioInicioProjetoMais120Dias, query = "SELECT p.nome, p.id FROM Projeto p where p.estagioAtual = 3 and DATEDIFF(day, p.dataCadastro, getDate()) > 120 and p.usuarioInvestimento.id = ?1 and p.ultimaVersao = true and p.situacao = 1"),
    @NamedQuery(name = Query.findByUsuarioInvestimentoProjInicioImplantacaoVencido, query = "SELECT p.nome, p.id FROM Projeto p inner join p.cronogramas c where p.usuarioInvestimento.id = ?1 and p.cadeiaProdutiva.id != 42 and c.inicioImplantacao != null and DATEDIFF(day, c.inicioImplantacao, getDate()) >= 0 and p.situacao = 1 and p.ultimaVersao = true and p.id not in (SELECT e.projetoId FROM EstagioProjeto e WHERE e.projetoId = p.id and e.estagioId = 2)"),
    @NamedQuery(name = Query.findByUsuarioInvestimentoProjInicioImplantacaoVencidoSucroEnergetico, query = "SELECT p.nome, p.id FROM Projeto p inner join p.cronogramas c where p.usuarioInvestimento.id = ?1 and p.cadeiaProdutiva.id = 42 and c.inicioImplantacaoAgricola != null and DATEDIFF(day, c.inicioImplantacaoAgricola, getDate()) >= 0 and p.situacao = 1 and p.ultimaVersao = true and p.id not in (SELECT e.projetoId FROM EstagioProjeto e WHERE e.projetoId = p.id and e.estagioId = 2)"),
    @NamedQuery(name = Query.findByUsuarioInvestimentoProjInicioOperacaoVencido, query = "SELECT p.nome, p.id FROM Projeto p inner join p.cronogramas c where p.usuarioInvestimento.id = ?1 and p.cadeiaProdutiva.id != 42 and c.inicioOperacao != null and DATEDIFF(day, c.inicioOperacao, getDate()) >= 0 and p.situacao = 1 and p.ultimaVersao = true and p.id not in (SELECT e.projetoId FROM EstagioProjeto e WHERE e.projetoId = p.id and e.estagioId = 4)"),
    @NamedQuery(name = Query.findByUsuarioInvestimentoProjInicioOperacaoVencidoSucroEnergetico, query = "SELECT p.nome, p.id FROM Projeto p inner join p.cronogramas c where p.usuarioInvestimento.id = ?1 and p.cadeiaProdutiva.id = 42 and c.inicioOperacaoSucroenergetico != null and DATEDIFF(day, c.inicioOperacaoSucroenergetico, getDate()) >= 0 and p.situacao = 1 and p.ultimaVersao = true and p.id not in (SELECT e.projetoId FROM EstagioProjeto e WHERE e.projetoId = p.id and e.estagioId = 4)"),
    @NamedQuery(name = Query.findByVersaoPaiAndUltimaVersao, query = "SELECT p FROM Projeto p where  p.versaoPai = ?1 AND p.ultimaVersao = ?2")
})
@Audited
public class Projeto extends AuditedEntity<Long> {

    @ManyToOne
    @JoinColumn(name = "usuarioInvestimento_id")
    private UsuarioInterno usuarioInvestimento;

    @ManyToOne
    @JoinColumn(name = "empresa_id")
    private Empresa empresa;

    @ManyToOne
    @JoinColumn(name = "unidade_id")
    private UnidadeEmpresa unidade;

    private String nome;
    private String descricao;

    //usado para armazenar a justificativa que vem da tela de projeto
    private transient String justificativa;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "historicosituacaoprojeto_id")
    private SituacaoProjeto situacaoAtual;

    @Enumerated
    @Column(name = "situacaoAtual_id", insertable = false, updatable = false)
    private SituacaoProjetoEnum situacao;
    
    @JsonIgnore
    @Column(name = "situacaoAtual_id")
    private Integer situacaoInt;

    @NotAudited
    @OneToMany(mappedBy = "projeto", cascade = {CascadeType.DETACH, CascadeType.REFRESH}, fetch = FetchType.EAGER)
    private List<SituacaoProjeto> situacoesProjeto;

    private Boolean prospeccaoAtiva;
    private int tipo;

    @ManyToOne
    @JoinColumn(name = "cadeiaProdutiva_id")
    private CadeiaProdutiva cadeiaProdutiva;

    private String informacaoPrimeiroContato;

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(name = "AnexoProjeto", joinColumns = {
        @JoinColumn(name = "projeto_id")}, inverseJoinColumns = {
        @JoinColumn(name = "anexo_id")})
    private Set<Anexo> anexos;

    @NotAudited
    @OneToMany(mappedBy = "projetoId", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<EstagioProjeto> estagios;

    @OneToOne(cascade = CascadeType.ALL, mappedBy = "projeto", fetch = FetchType.LAZY)
    private Localizacao localizacao;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "projeto", fetch = FetchType.EAGER, orphanRemoval = true)
    private Set<Emprego> empregos;

    @Temporal(TemporalType.TIMESTAMP)
    private Date dataCadastro;

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "projetos")
    private Set<InstrumentoFormalizacao> instrumentos;

    private transient EmpresaUnidadeDTO empresaUnidadeDTO;

    private Integer versao;

    @Column(name = "familia_id")
    private Long versaoPai;

    @Basic(optional = false)
    private Boolean ultimaVersao;

    @Enumerated
    @Column(name = "estagioAtual_id", insertable = false, updatable = false)
    private Estagio estagioAtual;
    
    @Column(name = "estagioAtual_id")
    private Integer estagioAtualInt;
    
    
    @JsonIgnore
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "projeto", fetch = FetchType.LAZY)
    private InsumoProduto insumoProduto;

    @JsonIgnore
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "projeto", fetch = FetchType.LAZY)
    private Financeiro financeiro;

    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "projeto", fetch = FetchType.LAZY)
    private Collection<Cronograma> cronogramas;

    @JsonIgnore
    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "projeto")
    private Pleito pleito;

    @OneToOne(cascade = CascadeType.ALL, mappedBy = "projeto", fetch = FetchType.LAZY,
            targetEntity = Infraestrutura.class)
    private Infraestrutura infraestrutura;

    @OneToOne(cascade = CascadeType.ALL, mappedBy = "projeto", fetch = FetchType.LAZY,
            targetEntity = MeioAmbiente.class)
    private MeioAmbiente meioAmbiente;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "projeto", fetch = FetchType.LAZY)
    private List<AtividadeProjeto> atividades;

    @OneToOne
    @JoinColumn(name = "ultimaAtividade_id")
    private AtividadeProjeto ultimaAtividade;
    
    @OrderBy("id asc")
    @OneToMany(mappedBy = "projeto", fetch = FetchType.LAZY)
    private List<RelatorioAcompanhamento> relatoriosAcompanhamento;

    public Projeto() {
    }

    public UsuarioInterno getUsuarioInvestimento() {
        return usuarioInvestimento;
    }

    public void setUsuarioInvestimento(UsuarioInterno usuarioInvestimento) {
        this.usuarioInvestimento = usuarioInvestimento;
    }

    public Empresa getEmpresa() {
        return empresa;
    }

    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }

    public UnidadeEmpresa getUnidade() {
        return unidade;
    }

    public void setUnidade(UnidadeEmpresa unidade) {
        this.unidade = unidade;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public SituacaoProjetoEnum getSituacao() {
        return situacao;
    }

    public void setSituacao(SituacaoProjetoEnum situacao) {
        if(situacao == null) {
            return;
        }
        this.situacao = situacao;
        this.situacaoInt = (new SituacaoProjetoEnumConverter()).convertToDatabaseColumn(situacao);
    }

    public Integer getSituacaoInt() {
        return situacaoInt;
    }

    public void setSituacaoInt(Integer situacaoInt) {
        if(situacaoInt == null) {
            return;
        }
        this.situacaoInt = situacaoInt;
        this.situacao = (new SituacaoProjetoEnumConverter()).convertToEntityAttribute(situacaoInt);
        
    }

    public SituacaoProjeto getSituacaoAtual() {
        return situacaoAtual;
    }

    public void setSituacaoAtual(SituacaoProjeto situacaoAtual) {
        this.situacaoAtual = situacaoAtual;
    }

    public String getJustificativa() {
        return justificativa;
    }

    public void setJustificativa(String justificativa) {
        this.justificativa = justificativa;
    }

    public List<SituacaoProjeto> getSituacoesProjeto() {
        return situacoesProjeto;
    }

    public void setSituacoesProjeto(List<SituacaoProjeto> situacoesProjeto) {
        this.situacoesProjeto = situacoesProjeto;
    }

    public Boolean getProspeccaoAtiva() {
        return prospeccaoAtiva;
    }

    public void setProspeccaoAtiva(Boolean prospeccaoAtiva) {
        this.prospeccaoAtiva = prospeccaoAtiva;
    }

    public int getTipo() {
        return tipo;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }

    public CadeiaProdutiva getCadeiaProdutiva() {
        return cadeiaProdutiva;
    }

    public void setCadeiaProdutiva(CadeiaProdutiva cadeiaProdutiva) {
        this.cadeiaProdutiva = cadeiaProdutiva;
    }

    public String getInformacaoPrimeiroContato() {
        return informacaoPrimeiroContato;
    }

    public void setInformacaoPrimeiroContato(String informacaoPrimeiroContato) {
        this.informacaoPrimeiroContato = informacaoPrimeiroContato;
    }

    public Set<Anexo> getAnexos() {
        return anexos;
    }

    public void setAnexos(Set<Anexo> anexos) {
        this.anexos = anexos;
    }

    public List<EstagioProjeto> getEstagios() {
        return estagios;
    }

    public void setEstagios(List<EstagioProjeto> estagios) {
        this.estagios = estagios;
    }

    public Collection<Cronograma> getCronogramas() {
        return cronogramas;
    }

    public void setCronogramas(Collection<Cronograma> cronogramas) {
        this.cronogramas = cronogramas;
    }

    public Date getDataCadastro() {
        return dataCadastro;
    }

    public void setDataCadastro(Date dataCadastro) {
        this.dataCadastro = dataCadastro;
    }

    public Set<Emprego> getEmpregos() {
        return empregos;
    }

    public void setEmpregos(Set<Emprego> empregos) {
        this.empregos = empregos;
    }

    public void addEmprego(Emprego emprego) {
        getEmpregos().add(emprego);
        emprego.setProjeto(this);
    }

    public InsumoProduto getInsumoProduto() {
        return insumoProduto;
    }

    public void setInsumoProduto(InsumoProduto insumoProduto) {
        this.insumoProduto = insumoProduto;
    }

    public EmpresaUnidadeDTO getEmpresaUnidadeDTO() {
        return empresaUnidadeDTO;
    }

    public void setEmpresaUnidadeDTO(EmpresaUnidadeDTO empresaUnidadeDTO) {
        this.empresaUnidadeDTO = empresaUnidadeDTO;
    }

    @PostLoad
    private void postLoad() {
        if (this.empresa != null) {
            empresaUnidadeDTO = new EmpresaUnidadeDTO();
            empresaUnidadeDTO.setEmpresaId(empresa.getId());

            if (this.unidade != null) {
                empresaUnidadeDTO.setUnidadeId(this.unidade.getId());
                empresaUnidadeDTO.setNomePrincipal(unidade.getNome());
                if (unidade.getEndereco() != null) {
                    empresaUnidadeDTO.setEnderecoFormatado(unidade.getEndereco().getEnderecoFormatado());
                }
            } else {
                empresaUnidadeDTO.setNomePrincipal(empresa.getNomePrincipal());
                empresaUnidadeDTO.setNomesEmpresa(empresa.getNomesEmpresa().stream().map(u -> u.getNome()).collect(Collectors.toList()));
                empresaUnidadeDTO.setRazaoSocial(empresa.getRazaoSocial());
                if (empresa.getEndereco() != null) {
                    empresaUnidadeDTO.setEnderecoFormatado(empresa.getEndereco().getEnderecoFormatado());
                }
            }
        }

        if (situacaoAtual != null) {
            justificativa = situacaoAtual.getJustificativa();
        }
    }

    public Integer getVersao() {
        return versao;
    }

    public void setVersao(Integer versao) {
        this.versao = versao;
    }

    public Long getVersaoPai() {
        return versaoPai;
    }

    public void setVersaoPai(Long versaoPai) {
        this.versaoPai = versaoPai;
    }

    public Boolean isUltimaVersao() {
        return ultimaVersao;
    }

    public void setUltimaVersao(Boolean ultimaVersao) {
        this.ultimaVersao = ultimaVersao;
    }

    public Estagio getEstagioAtual() {
        return estagioAtual;
    }

    public void setEstagioAtual(Estagio estagioAtual) {
        this.estagioAtual = estagioAtual;
        this.estagioAtualInt = (new EstagioEnumConverter()).convertToDatabaseColumn(estagioAtual);
    }

    public Financeiro getFinanceiro() {
        return financeiro;
    }

    public void setFinanceiro(Financeiro financeiro) {
        this.financeiro = financeiro;
    }

    public Pleito getPleito() {
        return pleito;
    }

    public void setPleito(Pleito pleito) {
        this.pleito = pleito;
    }

    public Infraestrutura getInfraestrutura() {
        return infraestrutura;
    }

    public void setInfraestrutura(Infraestrutura infraestrutura) {
        this.infraestrutura = infraestrutura;
    }

    public MeioAmbiente getMeioAmbiente() {
        return meioAmbiente;
    }

    public void setMeioAmbiente(MeioAmbiente meioAmbiente) {
        this.meioAmbiente = meioAmbiente;
    }

    public List<AtividadeProjeto> getAtividades() {
        return atividades;
    }

    public void setAtividades(List<AtividadeProjeto> atividades) {
        this.atividades = atividades;
    }

    public void addAtividade(AtividadeProjeto atividade) {
        if (this.atividades == null) {
            this.atividades = new ArrayList<>();
        }
        this.atividades.add(atividade);
        atividade.setProjeto(this);
    }

    public Set<InstrumentoFormalizacao> getInstrumentos() {
        return instrumentos;
    }

    public void setInstrumentos(Set<InstrumentoFormalizacao> instrumentos) {
        this.instrumentos = instrumentos;
    }

    public Localizacao getLocalizacao() {
        return localizacao;
    }

    public void setLocalizacao(Localizacao localizacao) {
        this.localizacao = localizacao;
    }

    public AtividadeProjeto getUltimaAtividade() {
        return ultimaAtividade;
    }

    public void setUltimaAtividade(AtividadeProjeto ultimaAtividade) {
        this.ultimaAtividade = ultimaAtividade;
    }

    public List<RelatorioAcompanhamento> getRelatoriosAcompanhamento() {
        return relatoriosAcompanhamento;
    }

    public void setRelatoriosAcompanhamento(List<RelatorioAcompanhamento> relatoriosAcompanhamento) {
        this.relatoriosAcompanhamento = relatoriosAcompanhamento;
    }

    public static class Query {

        public static final String findVersoesAnteriores = "Projeto.findVersoesAnteriores";
        public static final String findVersoesAtuaisByVersaoPai = "Projeto.findVersoesAtuaisByVersaoPai";
        public static final String countByResponsavelInvestimentoInList = "Projeto.findAllByResponsavelInvestimentoInList";
        public static final String updateEstagioProjeto = "Projeto.updateEstagioProjeto";
        public static final String updateSituacaoProjeto = "Projeto.updateSituacaoProjeto";
        public static final String updateUltimaAtividade = "Projeto.updateUltimaAtividade";
        public static final String updateUsuarioeData = "Projeto.updateUsuarioeData";
        public static final String findByInstrumentoFormalizacaoId = "Projeto.findByInstrumentoFormalizacaoId";
        public static final String findSituacaoAtualEnumByProjetoId = "Projeto.findSituacaoAtualEnumByProjetoId";        
        public static final String findByVersaoAndSituacaoNotEqualsAndUltimaVersaoAndVersaoPai = "Projeto.findByVersaoAndSituacaoNotEqualsAndUltimaVersaoAndVersaoPai";
        public static final String findBySituacaoEUsuarioInvestimento = "Projeto.findBySituacaoEUsuarioInvestimento";
        public static final String findByUsuarioInvestimentoProjEmEstagioInicioProjetoMais120Dias = "Projeto.findByUsuarioInvestimentoProjEmEstagioInicioProjetoMais120Dias";
        public static final String findByUsuarioInvestimentoProjInicioImplantacaoVencido = "Projeto.findByUsuarioInvestimentoProjInicioImplantacaoVencido";
        public static final String findByUsuarioInvestimentoProjInicioOperacaoVencido = "Projeto.findByUsuarioInvestimentoProjInicioOperacaoVencido";
        public static final String findByUsuarioInvestimentoProjInicioImplantacaoVencidoSucroEnergetico = "Projeto.findByUsuarioInvestimentoProjInicioImplantacaoVencidoSucroEnergetico";
        public static final String findByUsuarioInvestimentoProjInicioOperacaoVencidoSucroEnergetico = "Projeto.findByUsuarioInvestimentoProjInicioOperacaoVencidoSucroEnergetico";
        public static final String findByVersaoPaiAndUltimaVersao = "Projeto.findByVersaoPaiAndUltimaVersao";                
        
        public Query() {
        }
    }

    public static class Filters {

        public Filters() {
        }

        public static final String NOME_EMPRESA = "nomeEmpresa";
        public static final String SITUACAO_EMPRESA = "situacaoEmpresa";
        public static final String CNPJ = "cnpj";
        public static final String NOME_PROJETO = "nomeProjeto";
        public static final String ESTAGIO = "estagio";
        public static final String CADEIA_PRODUTIVA = "cadeiaProdutivaId";
        public static final String SITUACAO_PROJETO = "situacaoProjeto";
        public static final String USUARIO_RESPONSAVEL_PROJETO = "usuarioResponsavel";
        public static final String DEPARTAMENTOS = "departamentos";
        public static final String EMPRESA_ID = "empresaId";
        public static final String UNIDADE_ID = "unidadeId";        
    }

    public Integer getEstagioAtualInt() {
        return estagioAtualInt;
    }

    public void setEstagioAtualInt(Integer estagioAtualInt) {
        this.estagioAtualInt = estagioAtualInt;
        this.estagioAtual = (new EstagioEnumConverter()).convertToEntityAttribute(estagioAtualInt);
    }
    
    

}
