package br.com.gpi.server.domain.service;

import br.com.gpi.server.domain.common.BaseService;
import br.com.gpi.server.domain.entity.Producao;
import java.util.List;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

public class ProducaoService extends BaseService<Producao> {

    @Inject
    public ProducaoService(EntityManager em) {
        super(Producao.class, em);
    }

    public List<Producao> findAllProducaoByInsumoProduto(List<Long> ids_insumoProduto) {
        StringBuilder builder = new StringBuilder("SELECT p FROM Producao p ");
        builder.append(" WHERE p.insumoProduto.id in (:ids_insumoProduto) ");
        
        TypedQuery<Producao> query = getEm().createQuery(builder.toString(), Producao.class);
        query.setParameter("ids_insumoProduto", ids_insumoProduto);
        return query.getResultList();
    }    
    
}
