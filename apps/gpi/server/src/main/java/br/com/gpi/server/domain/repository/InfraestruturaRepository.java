package br.com.gpi.server.domain.repository;

import br.com.gpi.server.domain.entity.Infraestrutura;
import java.util.List;
import org.apache.deltaspike.data.api.EntityRepository;
import org.apache.deltaspike.data.api.Modifying;
import org.apache.deltaspike.data.api.Query;
import org.apache.deltaspike.data.api.Repository;
import org.apache.deltaspike.data.api.criteria.CriteriaSupport;

@Repository(forEntity = Infraestrutura.class)
public abstract class InfraestruturaRepository implements CriteriaSupport<Infraestrutura>,EntityRepository<Infraestrutura, Long>{

    @Modifying
    @Query(named = Infraestrutura.Query.locateByProjetoId)
    public abstract List<Infraestrutura> locateByProjetoId(Long id);

}
