package br.com.gpi.server.domain.repository;

import br.com.gpi.server.domain.entity.EstagioProjeto;
import java.util.List;
import org.apache.deltaspike.data.api.EntityRepository;
import org.apache.deltaspike.data.api.Query;
import org.apache.deltaspike.data.api.Repository;
import org.apache.deltaspike.data.api.criteria.CriteriaSupport;

@Repository(forEntity = EstagioProjeto.class)
public abstract class EstagioProjetoRepository implements CriteriaSupport<EstagioProjeto>, EntityRepository<EstagioProjeto, String> {
    @Query(named = EstagioProjeto.Query.findAllByProjeto)
    public abstract List<EstagioProjeto> findAllByProjeto(Long idProjeto);
}
