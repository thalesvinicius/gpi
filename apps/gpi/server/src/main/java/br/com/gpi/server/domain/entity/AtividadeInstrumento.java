package br.com.gpi.server.domain.entity;

import br.com.gpi.server.domain.common.AuditedEntity;
import br.com.gpi.server.to.LocalTO;
import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.Date;
import javax.annotation.PostConstruct;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.hibernate.envers.Audited;

@Audited
@Entity
@Table(name = "AtividadeInstrumento")
@NamedQueries({
    @NamedQuery(name = AtividadeInstrumento.Query.findAllByInstFormId, query = "SELECT a FROM AtividadeInstrumento a WHERE a.instrumentoFormalizacao.id = ? order by a.posicao")
})

public class AtividadeInstrumento extends AuditedEntity<Long> {
    
    private Integer posicao;

    @JsonIgnore    
    @ManyToOne(cascade = {CascadeType.DETACH, CascadeType.REFRESH})
    @JoinColumn(name = "instrumentoFormalizacao_id")
    private InstrumentoFormalizacao instrumentoFormalizacao;

    @ManyToOne(cascade = {CascadeType.DETACH, CascadeType.REFRESH})
    @JoinColumn(name = "atividadeLocal_id")
    private AtividadeLocal atividadeLocal;
    
    private transient LocalTO localAtividade;
            
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "anexo_id", unique = true, nullable = true)
    private Anexo anexo;
    
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataInicio;
    
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataConclusao;
    
    private String observacao;
    
    @Column(name = "obrigatorio", columnDefinition = "bit")    
    private Boolean obrigatorio;

    @Column(name = "paralela", columnDefinition = "bit")    
    private Boolean paralela;
    
    @Column(name = "bloqueiaICE", columnDefinition = "bit")    
    private Boolean bloqueiaICE;  
    
    @Column(name = "df", columnDefinition = "bit")    
    private Boolean df;
    
    @Column(name = "ii", columnDefinition = "bit")    
    private Boolean ii;

    @Column(name = "oi", columnDefinition = "bit")    
    private Boolean oi;

    @Column(name = "cc", columnDefinition = "bit")    
    private Boolean cc;


    public Integer getPosicao() {
        return posicao;
    }

    public void setPosicao(Integer posicao) {
        this.posicao = posicao;
    }

    public InstrumentoFormalizacao getInstrumentoFormalizacao() {
        return instrumentoFormalizacao;
    }

    public void setInstrumentoFormalizacao(InstrumentoFormalizacao instrumentoFormalizacao) {
        this.instrumentoFormalizacao = instrumentoFormalizacao;
    }

    public AtividadeLocal getAtividadeLocal() {
        return atividadeLocal;
    }

    public void setAtividadeLocal(AtividadeLocal atividadeLocal) {
        this.atividadeLocal = atividadeLocal;
    }

    public LocalTO getLocalAtividade() {
        return localAtividade;
    }

    public void setLocalAtividade(LocalTO localAtividade) {
        this.localAtividade = localAtividade;
    }

    public Anexo getAnexo() {
        return anexo;
    }

    public void setAnexo(Anexo anexo) {
        this.anexo = anexo;
    }

    public Date getDataInicio() {
        return dataInicio;
    }

    public void setDataInicio(Date dataInicio) {
        this.dataInicio = dataInicio;
    }

    public Date getDataConclusao() {
        return dataConclusao;
    }

    public void setDataConclusao(Date dataConclusao) {
        this.dataConclusao = dataConclusao;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public Boolean isObrigatorio() {
        return obrigatorio;
    }

    public void setObrigatorio(Boolean obrigatorio) {
        this.obrigatorio = obrigatorio;
    }
    
    public Boolean getParalela() {
        return paralela;
    }

    public void setParalela(Boolean paralela) {
        this.paralela = paralela;
    }

    public Boolean getBloqueiaICE() {
        return bloqueiaICE;
    }

    public void setBloqueiaICE(Boolean bloqueiaICE) {
        this.bloqueiaICE = bloqueiaICE;
    }

    public Boolean getDf() {
        return df;
    }

    public void setDf(Boolean df) {
        this.df = df;
    }

    public Boolean getIi() {
        return ii;
    }

    public void setIi(Boolean ii) {
        this.ii = ii;
    }

    public Boolean getOi() {
        return oi;
    }

    public void setOi(Boolean oi) {
        this.oi = oi;
    }

    public Boolean getCc() {
        return cc;
    }

    public void setCc(Boolean cc) {
        this.cc = cc;
    }        
    
    public static class Query {        
        public static final String findAllByInstFormId = "AtividadeInstrumento.findAllByInstFormId";

    }
    @PostConstruct
    private void postLoad() {
        this.localAtividade = new LocalTO(atividadeLocal.getLocal().getId(), atividadeLocal.getLocal().getDescricao());
    }
}
