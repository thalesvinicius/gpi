package br.com.gpi.server.domain.entity.enumerated;

public enum EnumClasseEmpreendimento {

    C1(1, 30), C2(2, 30), C3(3, 360), C4(4, 360), C5(5, 540), C6(6, 540);

    private final int id;
    private final int dias;

    private EnumClasseEmpreendimento(int id, int dias) {
        this.id = id;
        this.dias = dias;
    }

    public Integer getId() {
        return this.id;
    }

    public Integer getDias() {
        return this.dias;
    }
    
    public static Integer getQuantidadeDiasComId(int id) {
        for(EnumClasseEmpreendimento eae : values()){
            if(eae.id == id){
                return eae.getDias();
            }
        }
        return null;
    }    
}