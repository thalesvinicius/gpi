package br.com.gpi.server.querybuilder;

import java.util.Objects;

public class Field {

    private String nome;
    private String descricao;
    private Table tabela;
    private Category category;
    private boolean visible;
    
    public Field(Field campoBuscaAvancada) {
        this.nome = campoBuscaAvancada.getNome();
        this.descricao = campoBuscaAvancada.getDescricao();
        this.tabela = campoBuscaAvancada.getTabela();
        this.visible = campoBuscaAvancada.getVisible();
              
    }

    public Field(Category category, String nome, String descricao, Table classe) {
        this.nome = nome;
        this.descricao = descricao;
        this.tabela = classe;
        this.category = category;
    }
    
    public Field(Category category, String nome, String descricao, Table classe, boolean visible) {
        this(category, nome, descricao, classe);
        this.visible = visible;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Table getTabela() {
        return tabela;
    }

    public void setTabela(Table tabela) {
        this.tabela = tabela;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public String getAlias() {
        return this.getTabela().getAlias().concat("_").concat(this.getNome());
    }

    public String getSQL() {
        return this.getTabela().getAlias().concat(".").concat(this.getNome().concat(" as ").concat(this.getAlias()));
    }

    public boolean getVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 79 * hash + Objects.hashCode(this.nome);
        hash = 79 * hash + Objects.hashCode(this.descricao);
        hash = 79 * hash + Objects.hashCode(this.tabela);
        hash = 79 * hash + Objects.hashCode(this.category);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Field other = (Field) obj;
        if (!Objects.equals(this.nome, other.nome)) {
            return false;
        }
        if (!Objects.equals(this.descricao, other.descricao)) {
            return false;
        }
        if (!Objects.equals(this.tabela, other.tabela)) {
            return false;
        }
        if (this.category != other.category) {
            return false;
        }
        return true;
    }
    
    
}
