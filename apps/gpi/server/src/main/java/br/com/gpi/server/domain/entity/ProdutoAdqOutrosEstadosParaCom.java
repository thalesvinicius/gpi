package br.com.gpi.server.domain.entity;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import org.hibernate.envers.Audited;

@Audited
@Entity
@DiscriminatorValue(Produto.PRODUTO_ADQ_OUTROS_ESTADOS_PARA_COM)
public class ProdutoAdqOutrosEstadosParaCom extends Produto {
    
}
