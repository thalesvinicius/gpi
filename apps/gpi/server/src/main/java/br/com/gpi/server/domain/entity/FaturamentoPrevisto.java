package br.com.gpi.server.domain.entity;

import br.com.gpi.server.core.jpa.converter.TipoFaturamentoEnumConverter;
import br.com.gpi.server.domain.common.BaseEntity;
import br.com.gpi.server.domain.entity.enumerated.EnumTipoFaturamento;
import com.fasterxml.jackson.annotation.JsonIgnore;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import org.hibernate.envers.Audited;


@Audited
@Entity
@Table(name = "FaturamentoPrevisto")
@NamedQueries({
    @NamedQuery(name = FaturamentoPrevisto.Query.locateByTipoFaturamentoAndFinanceiroId, query = "SELECT f FROM FaturamentoPrevisto f WHERE f.tipoFaturamento = ?1 and f.financeiro.id = ?2")
})
public class FaturamentoPrevisto extends BaseEntity<Long> {

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "financeiro_id")
    private Financeiro financeiro;

    @Enumerated
    @Column(name = "tipo", insertable = false, updatable = false)
    private EnumTipoFaturamento tipoFaturamento;
    
    @JsonIgnore 
    @Column(name = "tipo")
    private Integer tipoFaturamentoInt;

    private int ano;

    private Double valor;

    private Double valorRealizado;

    public FaturamentoPrevisto() {
    }

    public FaturamentoPrevisto(int ano, Double valor) {
        this.ano = ano;
        this.valor = valor;
    }

    public Financeiro getFinanceiro() {
        return financeiro;
    }

    public void setFinanceiro(Financeiro financeiro) {
        this.financeiro = financeiro;
    }

     public void setTipoFaturamento(EnumTipoFaturamento tipoFaturamento) {
        if(tipoFaturamento == null) {
            return;
        }
         this.tipoFaturamento = tipoFaturamento;
        this.tipoFaturamentoInt = (new TipoFaturamentoEnumConverter()).convertToDatabaseColumn(tipoFaturamento);
    }

    public Integer getTipoFaturamentoInt() {
        return tipoFaturamentoInt;
    }

    public void setTipoFaturamentoInt(Integer tipoFaturamentoInt) {
        if(tipoFaturamentoInt == null) {
            return;
        }
        this.tipoFaturamentoInt = tipoFaturamentoInt;
        this.tipoFaturamento = (new TipoFaturamentoEnumConverter()).convertToEntityAttribute(tipoFaturamentoInt);
    }

    public EnumTipoFaturamento getTipoFaturamento() {
        return tipoFaturamento;
    }

    public int getAno() {
        return ano;
    }

    public void setAno(int ano) {
        this.ano = ano;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public static class Query {

        public static final String locateByTipoFaturamentoAndFinanceiroId = "FaturamentoPrevisto.locateByTipoFaturamentoAndFinanceiroId";
    }

    public Double getValorRealizado() {
        return valorRealizado;
    }

    public void setValorRealizado(Double valorRealizado) {
        this.valorRealizado = valorRealizado;
    }

}
