package br.com.gpi.server.domain.producer;

import br.com.gpi.server.domain.common.BaseEntity;
import br.com.gpi.server.domain.common.BaseService;
import java.lang.reflect.ParameterizedType;
import javax.enterprise.inject.Produces;
import javax.enterprise.inject.spi.BeanManager;
import javax.enterprise.inject.spi.InjectionPoint;
import javax.inject.Inject;
import javax.persistence.EntityManager;

/**
 *
 * @author <a href="mailto:pviniciusfm@gmail.com">Paulo Vinicius F. Machado</a>
 */
public class DomainProducer {

    @Inject
    private EntityManager entityManager;

    /**
     * Producer method of Generic Dao Injections
     *
     * @param <T>
     * @param injectionPoint
     * @param beanManager
     * @return produced GenericDAO of parameterized type
     */
    @Produces
    @SuppressWarnings("unchecked")
    public <T extends BaseEntity> BaseService<T> produceGenericDao(InjectionPoint injectionPoint, BeanManager beanManager) {
        ParameterizedType type = (ParameterizedType) injectionPoint.getType();
        Class<?> classe = (Class<?>) type.getActualTypeArguments()[0];
        return new BaseService(classe, entityManager);
    }
}
