package br.com.gpi.server.core.jpa.typesafe;

public interface EnumerationEntityType<PK> {

    PK getId();

    String getDescription();
}
