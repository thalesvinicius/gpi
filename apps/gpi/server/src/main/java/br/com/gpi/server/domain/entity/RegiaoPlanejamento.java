package br.com.gpi.server.domain.entity;

import br.com.gpi.server.domain.common.BaseEntity;
import javax.persistence.Cacheable;
import javax.persistence.Entity;
import org.hibernate.envers.Audited;

@Audited
@Entity
@Cacheable
public class RegiaoPlanejamento extends BaseEntity<Long> {

    private String nome;

    public RegiaoPlanejamento() {
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

}
