package br.com.gpi.server.endpoint;

import br.com.gpi.server.config.ApplicationSettings;
import br.com.gpi.server.to.EnvironmentInfo;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.apache.deltaspike.core.api.projectstage.ProjectStage;

@Path("/monitor")
@Stateless
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
@TransactionManagement(TransactionManagementType.BEAN)
public class MonitoringEndpoint {

    @Inject
    private ApplicationSettings settings;
    @Inject
    private ProjectStage projectStage;

    @GET
    @Path("environment")    
    public Response environment() {
        EnvironmentInfo environmentInfo = new EnvironmentInfo(settings.getVersion(), projectStage.toString(), settings.getGeneratedDateTime());
        return Response.ok(environmentInfo).build();
    }

    @GET
    @Path("version")
    @Produces(MediaType.TEXT_PLAIN)
    public String version() {
        return String.format("%s - %s ", settings.getVersion(), settings.getGeneratedDateTime());
    }

}
