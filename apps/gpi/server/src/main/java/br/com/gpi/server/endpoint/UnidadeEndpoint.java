package br.com.gpi.server.endpoint;

import br.com.gpi.server.domain.entity.UnidadeEmpresa;
import java.util.logging.Logger;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.apache.deltaspike.data.api.EntityRepository;

//@Path("/unidade")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
public class UnidadeEndpoint {

    @Inject
    private EntityRepository<UnidadeEmpresa, Long> service;

    @Inject
    private Logger logger;

    @PUT
    public Response save(UnidadeEmpresa unidade) {
        return Response.ok().build();
    }

    @POST
    public Response update(UnidadeEmpresa unidade) throws WebApplicationException {
        return Response.ok().build();
    }

    @GET
    public Response list() {
        return Response.ok(service.findAll()).build();
    }

    @DELETE
    public Response delete(UnidadeEmpresa unidade) {
        return Response.ok().build();
    }

}
