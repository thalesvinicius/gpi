package br.com.gpi.server.to;

public class FaturamentoIndustrializadosEAdquiridosParaComercializacaoDTO {
    private int ano;
    private Double valorIndustrializados;
    private Long idIndustrializados;
    private Double valorAdquiridosComercializacao;
    private Long idAdquiridosComercializacao;

    public FaturamentoIndustrializadosEAdquiridosParaComercializacaoDTO() {
        
    }

    public FaturamentoIndustrializadosEAdquiridosParaComercializacaoDTO(int ano,
            Double valorIndustrializados, Double valorAdquiridosComercializacao) {
        this.ano = ano;
        this.valorAdquiridosComercializacao = valorAdquiridosComercializacao;
        this.valorIndustrializados = valorIndustrializados;        
    }

    public int getAno() {
        return ano;
    }

    public void setAno(int ano) {
        this.ano = ano;
    }

    public Double getValorIndustrializados() {
        return valorIndustrializados;
    }

    public void setValorIndustrializados(Double valorIndustrializados) {
        this.valorIndustrializados = valorIndustrializados;
    }

    public Double getValorAdquiridosComercializacao() {
        return valorAdquiridosComercializacao;
    }

    public void setValorAdquiridosComercializacao(Double valorAdquiridosComercializacao) {
        this.valorAdquiridosComercializacao = valorAdquiridosComercializacao;
    }

    public Long getIdIndustrializados() {
        return idIndustrializados;
    }

    public void setIdIndustrializados(Long idIndustrializados) {
        this.idIndustrializados = idIndustrializados;
    }

    public Long getIdAdquiridosComercializacao() {
        return idAdquiridosComercializacao;
    }

    public void setIdAdquiridosComercializacao(Long idAdquiridosComercializacao) {
        this.idAdquiridosComercializacao = idAdquiridosComercializacao;
    }
        
}
