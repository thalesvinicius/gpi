package br.com.gpi.server.domain.repository;

import br.com.gpi.server.domain.entity.AtividadeProjeto;
import br.com.gpi.server.domain.entity.Estagio;
import br.com.gpi.server.domain.entity.Projeto;
import br.com.gpi.server.domain.entity.SituacaoProjeto;
import br.com.gpi.server.domain.entity.SituacaoProjetoEnum;
import br.com.gpi.server.domain.entity.User;
import java.util.Date;
import java.util.List;
import java.util.Set;
import org.apache.deltaspike.data.api.EntityRepository;
import org.apache.deltaspike.data.api.Modifying;
import org.apache.deltaspike.data.api.Query;
import org.apache.deltaspike.data.api.Repository;
import org.apache.deltaspike.data.api.criteria.CriteriaSupport;

@Repository(forEntity = Projeto.class)
public abstract class ProjetoRepository implements CriteriaSupport<Projeto>, EntityRepository<Projeto, Long> {

    @Query(named = Projeto.Query.countByResponsavelInvestimentoInList)
    public abstract List<Projeto> countByResponsavelInvestimentoInList(List<Long> usersId);
    
    @Query(named = Projeto.Query.findByInstrumentoFormalizacaoId)
    public abstract Set<Projeto> findByInstrumentoFormalizacaoId(Long instrumentoFormalizacaoId);
    
    @Modifying
    @Query(named = Projeto.Query.updateEstagioProjeto)
    public abstract void updateEstagioProjeto(Estagio estagio, Long id, User usuario, Date dataAlteracao);
    
    @Modifying
    @Query(named = Projeto.Query.updateSituacaoProjeto)
    public abstract void updateSituacaoProjeto(SituacaoProjeto situacao, Long id, User usuario, Date dataAlteracao, SituacaoProjetoEnum situacaoAtual);
    
    @Modifying
    @Query(named = Projeto.Query.updateUltimaAtividade)
    public abstract void updateUltimaAtividade(AtividadeProjeto atividadeProjeto, Long id);
    
    @Modifying
    @Query(named = Projeto.Query.updateUsuarioeData)
    public abstract void updateUsuarioeData(Long id, User usuarioResponsavel, Date dataUltimaAlteracao);
            
    @Query(named = Projeto.Query.findSituacaoAtualEnumByProjetoId)
    public abstract SituacaoProjetoEnum findSituacaoAtualEnumByProjetoId(Long projetoId);            
}
