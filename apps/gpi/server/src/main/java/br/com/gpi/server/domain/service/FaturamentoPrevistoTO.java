package br.com.gpi.server.domain.service;

import br.com.gpi.server.domain.entity.Financeiro;
import br.com.gpi.server.domain.entity.enumerated.EnumTipoFaturamento;

public class FaturamentoPrevistoTO {
    
    private Financeiro financeiro;
    private EnumTipoFaturamento tipoFaturamento;
    private int ano;
    private Double valor;

    public FaturamentoPrevistoTO() {
    }

    public FaturamentoPrevistoTO(Double valor) {
        this.valor = valor;
    }

    public Financeiro getFinanceiro() {
        return financeiro;
    }

    public void setFinanceiro(Financeiro financeiro) {
        this.financeiro = financeiro;
    }

    public EnumTipoFaturamento getTipoFaturamento() {
        return tipoFaturamento;
    }

    public void setTipoFaturamento(EnumTipoFaturamento tipoFaturamento) {
        this.tipoFaturamento = tipoFaturamento;
    }

    public int getAno() {
        return ano;
    }

    public void setAno(int ano) {
        this.ano = ano;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }
    
}
