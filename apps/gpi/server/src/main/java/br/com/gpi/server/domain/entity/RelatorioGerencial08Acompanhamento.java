/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.gpi.server.domain.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 *
 * @author rafaelbfs
 */
@Entity
@Table(name = "VIEW_RELATORIO_ACOPANHAMENTO_GERAL")
public class RelatorioGerencial08Acompanhamento implements Serializable{
    
    public RelatorioGerencial08Acompanhamento(){
        
    }

    public RelatorioGerencial08Acompanhamento(BigDecimal investimentoPrevisto, BigDecimal investimentoRealizado, BigDecimal faturamentoPrevisto, BigDecimal faturamentoRealizado, Integer empregosPermanentesDiretos, Integer empregoPermanentesDiretosRealizados) {
        this.investimentoPrevisto = investimentoPrevisto;
        this.investimentoRealizado = investimentoRealizado;
        this.faturamentoPrevisto = faturamentoPrevisto;
        this.faturamentoRealizado = faturamentoRealizado;
        this.empregosPermanentesDiretos = empregosPermanentesDiretos;
        this.empregoPermanentesDiretosRealizados = empregoPermanentesDiretosRealizados;
    }
    
    
    
    @Id
    @Column(name = "ID_projeto")
    private Long idProjeto;
    
    @Column(name = "NOME_PROJETO")
    private String nomeProjeto;
    @Column(name = "NOME_EMPRESA")
    private String empresa;
    
    private String cidade;
    @Column( name = "CADEIA_PRODUTIVA")
    private String cadeiaProdutiva;
    @Id
    private Integer ano;
    
    @Column(name = "regiao_planejamento")
    private String regiaoPlanejamento;
    
    private String analista;
    
    private String gerencia;
    
    @Transient
    private String descStatus;
    @Transient
    private String descEstagio;
    
    private Long analistaId;
   
    private Long gerenciaId;
    
    private Long regiaoPlanejamentoId;
    
    @Column(name = "estagioId", insertable = false, updatable = false)
    private Estagio estagioAtual;
    
    
    @Column(name = "ID_CADEIA_PRODUTIVA" , insertable = false, updatable = false)
    private Long cadeiaProdutivaId;
    
    
    @Column(name = "statusId", insertable = false, updatable = false )
    private SituacaoProjetoEnum statusAtual;   
    
    private Long statusId;          
    
    
            
    
    @Column(name= "INVESTIMENTO_PREVISTO")
    private BigDecimal investimentoPrevisto;
    
    @Column(name = "INVESTIMENTO_REALIZADO")
    private BigDecimal investimentoRealizado;
    
    @Column(name = "FATURAMENTO_PREVISTO")
    private BigDecimal faturamentoPrevisto;
    
    @Column(name = "FATURAMENTO_REALIZADO")
    private  BigDecimal faturamentoRealizado;
    
    @Column(name = "PERMANENTES_DIRETOS")
    private Integer empregosPermanentesDiretos;
    
    @Column(name = "PERMANENTES_DIRETOS_REALIZADOS")
    private Integer empregoPermanentesDiretosRealizados;
    
    
     
    public Long getIdProjeto() {
        return idProjeto;
    }

    public void setIdProjeto(Long idProjeto) {
        this.idProjeto = idProjeto;
    }

    public String getNomeProjeto() {
        return nomeProjeto;
    }

    public void setNomeProjeto(String nomeProjeto) {
        this.nomeProjeto = nomeProjeto;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public String getCadeiaProdutiva() {
        return cadeiaProdutiva;
    }

    public void setCadeiaProdutiva(String cadeiaProdutiva) {
        this.cadeiaProdutiva = cadeiaProdutiva;
    }

    public Integer getAno() {
        return ano;
    }

    public void setAno(Integer ano) {
        this.ano = ano;
    }

    public String getRegiaoPlanejamento() {
        return regiaoPlanejamento;
    }

    public void setRegiaoPlanejamento(String regiaoPlanejamento) {
        this.regiaoPlanejamento = regiaoPlanejamento;
    }

    public String getAnalista() {
        return analista;
    }

    public void setAnalista(String analista) {
        this.analista = analista;
    }

    public String getGerencia() {
        return gerencia;
    }

    public String getDescStatus() {
        String valor = statusAtual == null ? "" :statusAtual.getDescription();
        descStatus = valor;
        return valor;
    }

    public void setDescStatus(String descStatus) {
        this.descStatus = descStatus;
    }

    public String getDescEstagio() {
        String valor = estagioAtual == null ? "" : estagioAtual.getDescricao();
        descEstagio = valor;
        return valor;
    }

    public void setDescEstagio(String descEstagio) {
        this.descEstagio = descEstagio;
    }
    
    

    public void setGerencia(String gerencia) {
        this.gerencia = gerencia;
    }

    public BigDecimal getInvestimentoPrevisto() {
        return investimentoPrevisto;
    }

    public void setInvestimentoPrevisto(BigDecimal investimentoPrevisto) {
        this.investimentoPrevisto = investimentoPrevisto;
    }

    public BigDecimal getInvestimentoRealizado() {
        return investimentoRealizado;
    }

    public void setInvestimentoRealizado(BigDecimal investimentoRealizado) {
        this.investimentoRealizado = investimentoRealizado;
    }

    public BigDecimal getFaturamentoPrevisto() {
        return faturamentoPrevisto;
    }

    public void setFaturamentoPrevisto(BigDecimal faturamentoPrevisto) {
        this.faturamentoPrevisto = faturamentoPrevisto;
    }

    public BigDecimal getFaturamentoRealizado() {
        return faturamentoRealizado;
    }

    public void setFaturamentoRealizado(BigDecimal faturamentoRealizado) {
        this.faturamentoRealizado = faturamentoRealizado;
    }

    public Integer getEmpregosPermanentesDiretos() {
        return empregosPermanentesDiretos;
    }

    public void setEmpregosPermanentesDiretos(Integer empregosPermanentesDiretos) {
        this.empregosPermanentesDiretos = empregosPermanentesDiretos;
    }

    public Integer getEmpregoPermanentesDiretosRealizados() {
        return empregoPermanentesDiretosRealizados;
    }

    public void setEmpregoPermanentesDiretosRealizados(Integer empregoPermanentesDiretosRealizados) {
        this.empregoPermanentesDiretosRealizados = empregoPermanentesDiretosRealizados;
    }

    public Long getAnalistaId() {
        return analistaId;
    }

    public void setAnalistaId(Long analistaId) {
        this.analistaId = analistaId;
    }

    public Long getGerenciaId() {
        return gerenciaId;
    }

    public void setGerenciaId(Long gerenciaId) {
        this.gerenciaId = gerenciaId;
    }
    
    public Long getRegiaoPlanejamentoId() {
        return regiaoPlanejamentoId;
    }

    public void setRegiaoPlanejamentoId(Long regiaoPlanejamentoId) {
        this.regiaoPlanejamentoId = regiaoPlanejamentoId;
    }

    

    public Long getCadeiaProdutivaId() {
        return cadeiaProdutivaId;
    }

    public void setCadeiaProdutivaId(Long cadeiaProdutivaId) {
        this.cadeiaProdutivaId = cadeiaProdutivaId;
    }

    public Long getStatusId() {
        return statusId;
    }

    public void setStatusId(Long statusId) {
        this.statusId = statusId;
    }

    public String getEmpresa() {
        return empresa;
    }

    public void setEmpresa(String empresa) {
        this.empresa = empresa;
    }

    
    public SituacaoProjetoEnum getStatusAtual() {
        return statusAtual;
    }

    public void setStatusAtual(SituacaoProjetoEnum statusAtual) {
        this.statusAtual = statusAtual;
    }

    public Estagio getEstagioAtual() {
        return estagioAtual;
    }

    public void setEstagioAtual(Estagio estagioAtual) {
        this.estagioAtual = estagioAtual;
    }
    
    
    
    
    
    public static class Filtro {
        
        public static final String EMPRESA = "empresa";
        public static final String GERENCIA = "gerencia";
        public static final String CADEIA_PRODUTIVA = "cadeiaProdutiva";
        public static final String ANALISTA = "analista";
        public static final String ESTAGIO = "estagio";
        public static final String STATUS_ATUAL = "statusId";
        public static final String REGIAO = "regiao";
    }
    
    
    
    
    
    
    
    
    
            
    
    
    
    
    
    
    
}
