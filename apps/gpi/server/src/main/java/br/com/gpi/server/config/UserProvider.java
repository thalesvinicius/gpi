package br.com.gpi.server.config;

import br.com.gpi.server.domain.entity.User;
import java.io.Serializable;
import javax.enterprise.context.SessionScoped;
import javax.ws.rs.Produces;
import org.apache.deltaspike.data.api.audit.CurrentUser;

@SessionScoped
public class UserProvider implements Serializable {

    private User user;

    @Produces
    @CurrentUser
    public User currentUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

}
