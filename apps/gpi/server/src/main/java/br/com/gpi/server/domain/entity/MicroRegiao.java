package br.com.gpi.server.domain.entity;

import br.com.gpi.server.domain.common.BaseEntity;
import java.io.Serializable;
import javax.persistence.Cacheable;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import org.hibernate.envers.Audited;

@Audited
@Entity
@Cacheable
public class MicroRegiao extends BaseEntity<Long> implements Serializable {

    private String nome;
    
    @ManyToOne
    @JoinColumn(name = "regiaoPlanejamento_id")    
    private RegiaoPlanejamento regiaoPlanejamento;

    public MicroRegiao() {
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public RegiaoPlanejamento getRegiaoPlanejamento() {
        return regiaoPlanejamento;
    }

    public void setRegiaoPlanejamento(RegiaoPlanejamento regiaoPlanejamento) {
        this.regiaoPlanejamento = regiaoPlanejamento;
    }

}
