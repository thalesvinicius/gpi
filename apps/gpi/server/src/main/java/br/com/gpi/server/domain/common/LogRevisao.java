package br.com.gpi.server.domain.common;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import org.hibernate.envers.RevisionEntity;
import org.hibernate.envers.RevisionNumber;
import org.hibernate.envers.RevisionTimestamp;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

/**
 * Entididade que representa a tabela de revisões
 * dos históricos das entidades.
 * 
 * @author lucasmfe
 */

@Entity(name = "LogRevisao")
@RevisionEntity(LogRevisaoListener.class)
@Table(name="LogRevisao")
public class LogRevisao implements Serializable{
    
    @Id
    @Basic(optional = false)
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @RevisionNumber
    private Long id;
        
    @RevisionTimestamp	
    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "logDataUltimaAlteracao", nullable = false)
    private Date data;

    @NotNull
    @NotBlank @Length(min=1,max=100)
    @Column(name = "logUsuarioResponsavel", length = 100, nullable = false)
    private String responsavel;

    
    /**
     *  Getter e Setter
     */
    public Date getData() {
        return data;
    }
    public void setData(Date data) {
        this.data = data;
    }
    
    public void setResponsavel(String responsavel) {
        this.responsavel = responsavel;
    }
    public String getResponsavel() {
        return responsavel;
    }

    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
}
