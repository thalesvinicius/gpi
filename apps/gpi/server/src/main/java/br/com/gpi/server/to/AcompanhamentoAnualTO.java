package br.com.gpi.server.to;

import br.com.gpi.server.domain.entity.RelatorioAcompanhamento;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.util.ArrayList;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class AcompanhamentoAnualTO {
    private Long versaoCorrenteProjetoId;
    private Long versaoCongeladaProjetoId;
    private Long usuarioExternoResponsavelId;
    
    private List<CalendarioAcompanhamentoAnualTO> calendarios;
    private List<FaturamentoAcompanhamentoAnualTO> faturamentos;
    private List<InvestimentoAcompanhamentoAnualTO> investimentos;
    private List<EmpregoAcompanhamentoAnualTO> empregos;

    private RelatorioAcompanhamento relatorioAcompanhamento;
    private ProjetoTO projeto;

    public ProjetoTO getProjeto() {
        return projeto;
    }

    public void setProjeto(ProjetoTO projeto) {
        this.projeto = projeto;
    }
    
    public AcompanhamentoAnualTO(){
        
    }
    
    public Long getVersaoCorrenteProjetoId() {
        return versaoCorrenteProjetoId;
    }

    public void setVersaoCorrenteProjetoId(Long versaoCorrenteProjetoId) {
        this.versaoCorrenteProjetoId = versaoCorrenteProjetoId;
    }

    public Long getVersaoCongeladaProjetoId() {
        return versaoCongeladaProjetoId;
    }

    public void setVersaoCongeladaProjetoId(Long versaoCongeladaProjetoId) {
        this.versaoCongeladaProjetoId = versaoCongeladaProjetoId;
    }

    public List<CalendarioAcompanhamentoAnualTO> getCalendarios() {
        return calendarios;
    }

    public void setCalendarios(List<CalendarioAcompanhamentoAnualTO> calendarios) {
        this.calendarios = calendarios;
    }

    public List<FaturamentoAcompanhamentoAnualTO> getFaturamentos() {
        return faturamentos;
    }

    public void setFaturamentos(List<FaturamentoAcompanhamentoAnualTO> faturamentos) {
        this.faturamentos = faturamentos;
    }

    public List<InvestimentoAcompanhamentoAnualTO> getInvestimentos() {
        return investimentos;
    }

    public void setInvestimentos(List<InvestimentoAcompanhamentoAnualTO> investimentos) {
        this.investimentos = investimentos;
    }

    public List<EmpregoAcompanhamentoAnualTO> getEmpregos() {
        return empregos;
    }

    public void setEmpregos(List<EmpregoAcompanhamentoAnualTO> empregos) {
        this.empregos = empregos;
    }

    public RelatorioAcompanhamento getRelatorioAcompanhamento() {
        return relatorioAcompanhamento;
    }

    public void setRelatorioAcompanhamento(RelatorioAcompanhamento relatorioAcompanhamento) {
        this.relatorioAcompanhamento = relatorioAcompanhamento;
    }

    public void addToCalendarios(CalendarioAcompanhamentoAnualTO c) {
        if (calendarios == null) {
           calendarios = new  ArrayList<>();
        }
       calendarios.add(c);    
    }    
    
    public void addToFaturamentos(FaturamentoAcompanhamentoAnualTO f) {
        if (faturamentos == null) {
           faturamentos = new  ArrayList<>();
        }
        faturamentos.add(f);
    }

    public void addToInvestimentos(InvestimentoAcompanhamentoAnualTO i) {
        if (investimentos == null) {
           investimentos = new  ArrayList<>();
        }
        investimentos.add(i);
    }

    public void addToEmpregos(EmpregoAcompanhamentoAnualTO e) {
        if (empregos == null) {
           empregos = new  ArrayList<>();
        }
        empregos.add(e);        
    }

    public Long getUsuarioExternoResponsavelId() {
        return usuarioExternoResponsavelId;
    }

    public void setUsuarioExternoResponsavelId(Long usuarioExternoResponsavelId) {
        this.usuarioExternoResponsavelId = usuarioExternoResponsavelId;
    }
    
}
