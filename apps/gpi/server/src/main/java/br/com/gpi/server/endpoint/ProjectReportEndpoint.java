package br.com.gpi.server.endpoint;

import br.com.gpi.server.domain.common.QueryParameter;
import br.com.gpi.server.domain.entity.CadeiaProdutiva;
import br.com.gpi.server.domain.entity.Departamento;
import br.com.gpi.server.domain.entity.Estagio;
import br.com.gpi.server.domain.entity.RegiaoPlanejamento;
import br.com.gpi.server.domain.entity.RelatorioAcompanhamentoDataEstagios;
import br.com.gpi.server.domain.entity.RelatorioGerencial08Acompanhamento;
import br.com.gpi.server.domain.entity.SituacaoProjetoEnum;
import br.com.gpi.server.domain.entity.User;
import br.com.gpi.server.domain.service.DomainTableService;
import br.com.gpi.server.domain.service.RegiaoPlanejamentoService;
import br.com.gpi.server.domain.service.UserService;
import br.com.gpi.server.domain.service.reports.RelatorioGerencial08AcompanhamentoService;
import br.com.gpi.server.domain.service.reports.gerencial.RelatorioAcompanhamentoDataEstagiosService;
import br.com.gpi.server.domain.service.reports.gerencial.RelatorioGerencial04CarteiraProjetos;
import br.com.gpi.server.domain.user.model.SecurityUser;
import br.com.gpi.server.domain.view.RelatorioCarteiraProjeto;
import br.com.gpi.server.util.reports.ReportUtil;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.function.Supplier;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

@Path("relatorio/projeto")
@Stateless
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
@TransactionManagement(TransactionManagementType.BEAN)
public class ProjectReportEndpoint {

    @Inject
    private RelatorioGerencial04CarteiraProjetos gerencial04CarteiraProjetos;

    @Inject
    private RelatorioAcompanhamentoDataEstagiosService relatorioAcompanhamentoDatasEstagiosService;
    
    @Inject
    private RegiaoPlanejamentoService regiaoPlanejamentoService;
    
     @Inject 
    private DomainTableService domainTable;
     
     @Inject
    private UserService user;
     
     @Inject
     private RelatorioGerencial08AcompanhamentoService gerencial08AcompanhamentoService;

    @GET
    @Path("estagio/{format}")
    @Produces({"application/pdf", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"})
    public Response buildEstagio(@PathParam("format") String format,
            @QueryParam("estagios") List<Estagio> estagios,
            @QueryParam("todosEstagios") Set<Estagio> todosEstagios,
            @QueryParam("cadeiasProdutivas") List<Long> cadeiasProdutivas,
            @QueryParam("status") List<SituacaoProjetoEnum> situacoesProjeto,
            @QueryParam("departamentos") List<Long> departamentos,
            @QueryParam("regioesPlanejamento") List<Long> regioesPlanejamento,
            @QueryParam("usuariosResponsavel") List<Long> usuariosResponsaveis,
            @QueryParam("versao") Boolean ultimaVersao,
            @HeaderParam("todasCadeiasProdutivas") String todasCadeiasProdutivas,
            @HeaderParam("todasGerencias") String todasGerencias,
            @HeaderParam("todasRegioesPlanejamento") String todasRegioesPlanejamento,
            @HeaderParam("todosAnalistas") String todosAnalistas,
            @Context SecurityContext sc) throws FileNotFoundException, IOException {
        
        ReportUtil ru = new ReportUtil(){};
        Function<UserService, String> auth = ru.responsibleForReportInfo((SecurityUser) sc.getUserPrincipal());
        String informacoes = auth.apply(user);
            
            Boolean buscarTodasAsGerencias = ru.buscarTodos(todasGerencias, departamentos);
            Boolean buscarTodasCadeiasProdutivas = ru.buscarTodos(todasCadeiasProdutivas, cadeiasProdutivas);
            Boolean buscarTodasRegioes = ru.buscarTodos(todasRegioesPlanejamento, regioesPlanejamento);
            Boolean buscarTodosAnalistas = ru.buscarTodos(todosAnalistas,usuariosResponsaveis );
            Set<Estagio> estagiosSet = new HashSet<>(Optional.ofNullable(estagios).orElse(Collections.emptyList()));
            Boolean buscarTodosEstagios = estagiosSet.containsAll(todosEstagios);    
            final Map<String,Object> params = new HashMap<>();
            params.put("TITULO", "Relatório de Acompanhamento");
            params.put("ESTAGIOS",ReportUtil.determinarParametros.apply( buscarTodosEstagios ? null: estagios ,
                    (e) -> ((Estagio) e).getDescricao(),true));
            params.put("STATUS",ReportUtil.determinarParametros.apply(situacoesProjeto,(sit) -> ((SituacaoProjetoEnum) sit).getDescription(), true));
            params.put("ANALISTA",ru.stringCollection(User::getNome,buscarTodosAnalistas? null:
                    user.findByListIDs(new ArrayList<>(usuariosResponsaveis)),true));
            params.put("REGIAO_PLANEJAMENTO",ru.stringCollection(RegiaoPlanejamento::getNome, buscarTodasRegioes ? null : 
                    regiaoPlanejamentoService.findByListIDs(new ArrayList<>(regioesPlanejamento)) , false));
                
            params.put("CADEIA_PRODUTIVA",ru.stringCollection(CadeiaProdutiva::getDescricao,buscarTodasCadeiasProdutivas ? null:
                    domainTable.findByIds(CadeiaProdutiva.class,cadeiasProdutivas ),false));
            params.put("GERENCIA",ru.stringCollection(Departamento::getDescricao,buscarTodasAsGerencias ? null:
                    domainTable.findByIds(Departamento.class, departamentos),false ));
            params.put(ReportUtil.INFO_RESPONSAVEL,informacoes);
            params.put("VERSAO",Boolean.TRUE.equals(ultimaVersao) ? "Corrente" : "Congelada");
                   
        
        return relatorioAcompanhamentoDatasEstagiosService.generateReport(format, estagios, cadeiasProdutivas, situacoesProjeto, departamentos, 
                regioesPlanejamento, usuariosResponsaveis, ultimaVersao,params);
    }

    @GET
    @Path("carteira/{format}")
    @Produces({"application/pdf", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"})
    public Response buildCarteira(@PathParam("format") String format,
            @QueryParam("estagios") List<Estagio> estagios,
            @QueryParam("cadeiasProdutivas") List<Long> cadeiasProdutivas,
            @QueryParam("status") List<SituacaoProjetoEnum> situacoesProjeto,
            @QueryParam("todosEstagios") Set<Estagio> todosEstagios,
            @QueryParam("departamentos") List<Long> departamentos,
            @QueryParam("regioesPlanejamento") List<Long> regioesPlanejamento,
            @QueryParam("usuariosResponsavel") List<Long> usuariosResponsaveis,
            @HeaderParam("todasCadeiasProdutivas") String todasCadeiasProdutivas,
            @HeaderParam("todasGerencias") String todasGerencias,
            @HeaderParam("todasRegioesPlanejamento") String todasRegioesPlanejamento,
            @HeaderParam("todosAnalistas") String todosAnalistas, 
            @Context SecurityContext sc) throws FileNotFoundException, IOException {
        
         Supplier<Boolean> buscarTodosEstagios = ()-> { Set<Estagio> estagiosSet = new HashSet<>(Optional.ofNullable(estagios).orElse(Collections.emptyList()));
                return estagiosSet.containsAll(todosEstagios);    
            };
         ReportUtil ru = new ReportUtil(){};
         SecurityUser principal = (SecurityUser) sc.getUserPrincipal();
         String responsavel = ru.responsibleForReportInfo(principal).apply(user);
         Supplier<Map<String,Object>> parametros = () -> {
            
            Boolean buscarTodasAsGerencias = ru.buscarTodos(todasGerencias, departamentos);
            Boolean buscarTodasCadeiasProdutivas = ru.buscarTodos(todasCadeiasProdutivas, cadeiasProdutivas);
            Boolean buscarTodasRegioes = ru.buscarTodos(todasRegioesPlanejamento, regioesPlanejamento);
            Boolean buscarTodosAnalistas = ru.buscarTodos(todosAnalistas,usuariosResponsaveis );
            final Map<String,Object> params = new HashMap<>();
            params.put("TITULO", "Relatório Carteira de Projetos");
            params.put("ESTAGIOS",ReportUtil.determinarParametros.apply( buscarTodosEstagios.get() ? null : estagios,(e) -> ((Estagio) e).getDescricao(),true));
            params.put("STATUS",ReportUtil.determinarParametros.apply(situacoesProjeto,(sit) -> ((SituacaoProjetoEnum) sit).getDescription(), true));
            params.put("ANALISTA",ru.stringCollection(User::getNome,buscarTodosAnalistas? null:
                    user.findByListIDs(new ArrayList<>(usuariosResponsaveis)),true));
            params.put("REGIAO_PLANEJAMENTO",ru.stringCollection(RegiaoPlanejamento::getNome, buscarTodasRegioes ? null : 
                    regiaoPlanejamentoService.findByListIDs(new ArrayList<>(regioesPlanejamento)) , false));
                
            params.put("CADEIA_PRODUTIVA",ru.stringCollection(CadeiaProdutiva::getDescricao,buscarTodasCadeiasProdutivas ? null:
                    domainTable.findByIds(CadeiaProdutiva.class,cadeiasProdutivas ),false));
            params.put("GERENCIA",ru.stringCollection(Departamento::getDescricao,buscarTodasAsGerencias ? null:
                    domainTable.findByIds(Departamento.class, departamentos),false ));
            //params.put("LOGO_INDI",getImage(LOGO_INDI_NOME));
            params.put(ReportUtil.INFO_RESPONSAVEL,responsavel);
            return params;
            
        };    
        
        return gerencial04CarteiraProjetos.generateReport(format, estagios, cadeiasProdutivas, situacoesProjeto, departamentos, regioesPlanejamento, usuariosResponsaveis,parametros);
        
    }
    
    @GET
    @Path("getCarteira")
    public Response getCarteira(
            @QueryParam("estagios") List<Estagio> estagios,
            @QueryParam("cadeiasProdutivas") List<Long> cadeiasProdutivas,
            @QueryParam("status") List<SituacaoProjetoEnum> situacoesProjeto,
            @QueryParam("departamentos") List<Long> departamentos,
            @QueryParam("regioesPlanejamento") List<Long> regioesPlanejamento,
            @QueryParam("usuariosResponsavel") List<Long> usuariosResponsaveis) {

            QueryParameter filtros = QueryParameter.with(
                     RelatorioCarteiraProjeto.Filtro.ESTAGIO, estagios)
                .and(RelatorioCarteiraProjeto.Filtro.ANALISTA,usuariosResponsaveis)
                .and(RelatorioCarteiraProjeto.Filtro.CADEIA_PRODUTIVA,cadeiasProdutivas)
                .and(RelatorioCarteiraProjeto.Filtro.GERENCIA,departamentos)
                .and(RelatorioCarteiraProjeto.Filtro.STATUS_ATUAL,situacoesProjeto)
                .and(RelatorioCarteiraProjeto.Filtro.REGIAO,regioesPlanejamento);
                
        return Response.ok(gerencial04CarteiraProjetos.generate(filtros)).build();
    }
    
    @GET
    @Path("getByFilters")
    public Response getRelatorioProjetoTOByFilters(
            @QueryParam("estagios") List<Estagio> estagios,
            @QueryParam("cadeiasProdutivas") List<Long> cadeiasProdutivas,
            @QueryParam("status") List<SituacaoProjetoEnum> situacoesProjeto,
            @QueryParam("departamentos") List<Long> departamentos,
            @QueryParam("regioesPlanejamento") List<Long> regioesPlanejamento,
            @QueryParam("usuariosResponsavel") List<Long> usuariosResponsaveis,
            @QueryParam("versao") Boolean ultimaVersao) {

        return Response.ok(getRelatorioProjetoTOByFilters(ultimaVersao, situacoesProjeto, estagios, cadeiasProdutivas, departamentos, regioesPlanejamento, usuariosResponsaveis)).build();
    }

    private List<RelatorioAcompanhamentoDataEstagios> getRelatorioProjetoTOByFilters(Boolean ultimaVersao, List<SituacaoProjetoEnum> situacoesProjeto, List<Estagio> estagios, List<Long> cadeiasProdutivas, List<Long> departamentos, List<Long> regioesPlanejamento, List<Long> usuariosResponsaveis) {
        if (situacoesProjeto == null || situacoesProjeto.isEmpty()) {
            situacoesProjeto = new ArrayList<>(Arrays.asList(SituacaoProjetoEnum.values()));
            situacoesProjeto.remove(SituacaoProjetoEnum.PEDENTE_VALIDACAO);
            situacoesProjeto.remove(SituacaoProjetoEnum.PENDENTE_ACEITE);
        }
        return relatorioAcompanhamentoDatasEstagiosService.findProjectsByFilters(estagios, cadeiasProdutivas, situacoesProjeto, departamentos, regioesPlanejamento, usuariosResponsaveis, ultimaVersao);
    } 
    
    
    @GET
    @Path("acompanhamento/export/{format}")
    @Produces({"application/pdf", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"})
    public Response relatorioAcompanhamento(@PathParam("format") String format,
            @QueryParam("estagios") List<Estagio> estagios,
            @QueryParam("cadeiasProdutivas") List<Long> cadeiasProdutivas,
            @QueryParam("status") List<SituacaoProjetoEnum> situacoesProjeto,
            @QueryParam("departamentos") List<Long> departamentos,
            @QueryParam("todosEstagios") Set<Estagio> todosEstagios,
            @QueryParam("regioesPlanejamento") List<Long> regioesPlanejamento,
            @QueryParam("analistas") List<Long> usuariosResponsaveis,
            @QueryParam("empresa") String empresa,
            @HeaderParam("todasCadeiasProdutivas") String todasCadeiasProdutivas,
            @HeaderParam("todasGerencias") String todasGerencias,
            @HeaderParam("todasRegioesPlanejamento") String todasRegioesPlanejamento,
            @HeaderParam("todosAnalistas") String todosAnalistas, 
            @Context SecurityContext sc) throws FileNotFoundException, IOException {
        List<RelatorioGerencial08Acompanhamento> diesen = gerencial08AcompanhamentoService.pesquisar(estagios, cadeiasProdutivas, situacoesProjeto, departamentos, regioesPlanejamento, usuariosResponsaveis, empresa);
        ReportUtil ru = new ReportUtil(){};
         SecurityUser principal = (SecurityUser) sc.getUserPrincipal();
         String responsavel = ru.responsibleForReportInfo(principal).apply(user);
         Supplier<Map<String,Object>> parametros = () -> {
            
             Supplier<Boolean> buscarTodosEstagios = ()-> { Set<Estagio> estagiosSet = new HashSet<>(Optional.ofNullable(estagios).orElse(Collections.emptyList()));
                return estagiosSet.containsAll(todosEstagios);    
            };
            Boolean buscarTodasAsGerencias = ru.buscarTodos(todasGerencias, departamentos);
            Boolean buscarTodasCadeiasProdutivas = ru.buscarTodos(todasCadeiasProdutivas, cadeiasProdutivas);
            Boolean buscarTodasRegioes = ru.buscarTodos(todasRegioesPlanejamento, regioesPlanejamento);
            Boolean buscarTodosAnalistas = ru.buscarTodos(todosAnalistas,usuariosResponsaveis );
            final Map<String,Object> params = new HashMap<>();
            
            params.put("ESTAGIOS",ReportUtil.determinarParametros.apply( buscarTodosEstagios.get() ? null : estagios,(e) -> ((Estagio) e).getDescricao(),true));
            params.put("STATUS",ReportUtil.determinarParametros.apply(situacoesProjeto,(sit) -> ((SituacaoProjetoEnum) sit).getDescription(), true));
            params.put("ANALISTA",ru.stringCollection(User::getNome,buscarTodosAnalistas? null:
                    user.findByListIDs(new ArrayList<>(usuariosResponsaveis)),true));
            params.put("REGIAO_PLANEJAMENTO",ru.stringCollection(RegiaoPlanejamento::getNome, buscarTodasRegioes ? null : 
                    regiaoPlanejamentoService.findByListIDs(new ArrayList<>(regioesPlanejamento)) , false));
                
            params.put("CADEIA_PRODUTIVA",ru.stringCollection(CadeiaProdutiva::getDescricao,buscarTodasCadeiasProdutivas ? null:
                    domainTable.findByIds(CadeiaProdutiva.class,cadeiasProdutivas ),false));
            params.put("GERENCIA",ru.stringCollection(Departamento::getDescricao,buscarTodasAsGerencias ? null:
                    domainTable.findByIds(Departamento.class, departamentos),false ));
            //params.put("LOGO_INDI",getImage(LOGO_INDI_NOME));
            params.put(ReportUtil.INFO_RESPONSAVEL,responsavel);
            return params;
            
        };   
        return gerencial08AcompanhamentoService.relatorio(diesen, format, parametros);
    }
    
    @GET
    @Path("acompanhamento/pesquisar")
    public Response getRelatorioProjetoTOByFilters(
            @QueryParam("estagios") List<Estagio> estagios,
            @QueryParam("cadeiasProdutivas") List<Long> cadeiasProdutivas,
            @QueryParam("status") List<SituacaoProjetoEnum> situacoesProjeto,
            @QueryParam("departamentos") List<Long> departamentos,
            @QueryParam("regioesPlanejamento") List<Long> regioesPlanejamento,
            @QueryParam("analistas") List<Long> usuariosResponsaveis,
            @QueryParam("empresa") String empresa) {

        return Response.ok(gerencial08AcompanhamentoService.totalizarPorEmpresa( gerencial08AcompanhamentoService.pesquisar(estagios, cadeiasProdutivas, 
                situacoesProjeto, departamentos, regioesPlanejamento, usuariosResponsaveis, empresa))).build();
    }
    
    
    
    
}
