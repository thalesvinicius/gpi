/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.gpi.server.querybuilder;

import br.com.gpi.server.to.ConditionTO;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author lucasgom
 */
public class ConditionFactory {

    public Condition getInstance(ConditionTO conditionTO) {
        try {
            ConditionInfo info = Locator.FILTROS.get(conditionTO.getConditionKey());
            Condition condition = null;
            boolean hasValidValues = conditionTO.getValues() != null && conditionTO.getValues().length > 0
                    && conditionTO.getValues()[0] != null && !conditionTO.getValues()[0].trim().equals("");
            if (info != null && hasValidValues) {
                
                Class clazz = info.getInstance();
                condition = (Condition) clazz.newInstance();
                // verifing if is a ComposedConditionInfo because the way to build the fields is different
                boolean isComposedInfo = info instanceof ComposedConditionInfo;
                condition.setFields(isComposedInfo ? ((ComposedConditionInfo)info).getFields(Arrays.asList(conditionTO.getDependency().getValues())) : info.getFields());
                
                condition.setValueType(info.getType());

                if (condition instanceof SimpleCondition) {
                    SimpleCondition simple = (SimpleCondition) condition;
                    simple.setValue(conditionTO.getValues()[0]);
                    simple.setComparisonType(info.getComparisonType());
                } else if (condition instanceof OrCondition) {
                    OrCondition or = (OrCondition) condition;
                    or.setComparisonType(info.getComparisonType());
                    or.setValue(conditionTO.getValues()[0]);
                } else if (condition instanceof InCondition) {
                    InCondition in = (InCondition) condition;
                    in.setValues(Arrays.asList(conditionTO.getValues()));
                } else if (condition instanceof BetweenCondition) {
                    BetweenCondition between = (BetweenCondition) condition;
                    between.setValues(Arrays.asList(conditionTO.getValues()));
                }
            } else {
                System.out.println("Não achou filtro ".concat(conditionTO.getConditionKey()));
            }

            return condition;
        } catch (InstantiationException | IllegalAccessException ex) {
            Logger.getLogger(ConditionFactory.class.getName()).log(Level.SEVERE, null, ex);
            throw new RuntimeException(ex);
        }
    }

}
