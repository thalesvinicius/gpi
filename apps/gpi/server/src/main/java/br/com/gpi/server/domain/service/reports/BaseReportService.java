package br.com.gpi.server.domain.service.reports;

import br.com.gpi.server.domain.entity.User;
import br.com.gpi.server.domain.service.UserService;
import br.com.gpi.server.domain.service.reports.gerencial.RelatorioGerencial04CarteiraProjetos;
import br.com.gpi.server.domain.user.model.SecurityUser;
import br.com.gpi.server.util.EnumMimeTypes;
import br.com.gpi.server.util.functional.FunctionalInterfaces;
import static br.com.gpi.server.util.functional.FunctionalInterfaces.*;
import br.com.gpi.server.util.reports.ReportUtil;
import java.awt.Color;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import net.sf.dynamicreports.jasper.builder.JasperReportBuilder;
import static net.sf.dynamicreports.report.builder.DynamicReports.cmp;
import static net.sf.dynamicreports.report.builder.DynamicReports.margin;
import static net.sf.dynamicreports.report.builder.DynamicReports.report;
import static net.sf.dynamicreports.report.builder.DynamicReports.stl;
import net.sf.dynamicreports.report.builder.style.StyleBuilder;
import net.sf.dynamicreports.report.constant.HorizontalAlignment;
import net.sf.dynamicreports.report.constant.LineStyle;
import net.sf.dynamicreports.report.constant.PageOrientation;
import net.sf.dynamicreports.report.constant.PageType;
import net.sf.dynamicreports.report.constant.SplitType;
import net.sf.dynamicreports.report.constant.VerticalAlignment;
import net.sf.jasperreports.engine.DefaultJasperReportsContext;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRResultSetDataSource;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.Renderable;
import net.sf.jasperreports.engine.RenderableUtil;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.type.OnErrorTypeEnum;
import net.sf.jasperreports.engine.util.JRLoader;
import org.apache.commons.lang3.tuple.Pair;
import org.hibernate.Session;
import org.hibernate.engine.jdbc.connections.spi.ConnectionProvider;
import org.hibernate.engine.spi.SessionFactoryImplementor;


/**
 *
 * @author rafaelbfs
 */

public class BaseReportService<D> {

    protected final EntityManager entityManager;
    protected final Locale ptBR = new Locale("pt", "BR");
    
    @Inject
    protected UserService userService;
    @Inject
    protected Logger logger;

    @Inject
    public BaseReportService(EntityManager em) {       
        entityManager = em; 
        
    }
    
    protected Renderable getINDILogo(){
        return getImage(ReportUtil.LOGO_INDI_NOME);
    }
    
    public Renderable getImage(String resourceLocation){
        try {
            return RenderableUtil.getInstance(
                    DefaultJasperReportsContext.getInstance())
                    .getRenderable(resourceAsStream(resourceLocation), OnErrorTypeEnum.ERROR);
        } catch (JRException ex) {
            throw new IllegalStateException("Falha no carregamento da imagem",ex);
        }
    }

    protected InputStream defaultStyle() {
        return resourceAsStream("/reports/GPIJasperStyle.jrtx");
    }

    protected Supplier<JRDataSource> jrBeanCollection(Collection<?> col) {
        return () -> new JRBeanCollectionDataSource(col);
    }

    protected Function<List<D>, Supplier<JRDataSource>> jrBCol
            = (col) -> () -> (new JRBeanCollectionDataSource(col));

    public InputStream resourceAsStream(String name) {
        InputStream inputStream = this.getClass().getResourceAsStream(name);
        if (inputStream != null) {
            return inputStream;
        } else {
            throw new IllegalArgumentException("Caminho não encontrado: " + name);
        }
    }
    
    protected JasperReport loadObject(InputStream is){
        try {
            return (JasperReport) JRLoader.loadObject(is);
        } catch (JRException ex) {
            throw new IllegalArgumentException("Arquivo inválido.",ex);
        }
    }

    /**
     * Obtém conexão {@link java.sql.Connection} do <b>EntityManager</b> para
     * ser passada ao Jasper.
     *
     * @return A referida conexão.
     */
    public final Connection getJDBCConnection() {
        try {
            Session session = entityManager.unwrap(Session.class);
            SessionFactoryImplementor sfi = (SessionFactoryImplementor) session.getSessionFactory();
            ConnectionProvider cp = sfi.getConnectionProvider();
            return cp.getConnection();
        } catch (Exception e) {
            throw new IllegalStateException("Exceção ao tentar criar conexão ao banco de dados:\n"
                    + e.getMessage(), e);
        }

    }
    
    public final Connection getJDBCConnection(Session session) {
        try {
            SessionFactoryImplementor sfi = (SessionFactoryImplementor) session.getSessionFactory();
            ConnectionProvider cp = sfi.getConnectionProvider();
            return cp.getConnection();
        } catch (Exception e) {
            throw new IllegalStateException("Exceção ao tentar criar conexão ao banco de dados:\n"
                    + e.getMessage(), e);
        }

    }
    
     public final Session getSession() {
        try {
            return entityManager.unwrap(Session.class);
        } catch (Exception e) {
            throw new IllegalStateException("Exceção ao tentar criar conexão ao banco de dados:\n"
                    + e.getMessage(), e);
        }

    }
    
    public JasperPrint generateReport(Map<String, Object> parametros, InputStream file,
            ParameterizedSelectBuilder querybuilder) throws SQLException{
       Connection conn = null;
       PreparedStatement ps = null ;
       ResultSet rs = null;
       Session session = null;
        
        try {
            session = getSession();
            conn = getJDBCConnection(session);  
           Pair<String, List<Object>> tuple = querybuilder.build();
           ps = conn.prepareStatement(tuple.getLeft());           
           ListIterator<Object> iterator = tuple.getRight().listIterator();
           while(iterator.hasNext()){
                Object param = iterator.next();
                set(ps,iterator.nextIndex(), param);
           } 
           rs = ps.executeQuery(); 
           final  ResultSet rs2 = rs;
           JasperPrint jp = generateReport(parametros, file, () -> new JRResultSetDataSource(rs2));            
           return jp;         
        }catch(JRException ex){
            logger.log(Level.SEVERE,"Falha durente geração do relatório" , ex);            
            throw new RuntimeException(ex.getMessage(),ex);
        } catch (FileNotFoundException ex) {
            logger.log(Level.SEVERE, "Erro no carregamento do relatório", ex);
            throw new RuntimeException(ex.getMessage(),ex);
        } catch (SQLException ex) {
            logger.log(Level.SEVERE, "Erro ao executar consulta SQL com mensagem:\n"+ex.getMessage()  ,ex);
            throw new RuntimeException(ex.getMessage(),ex);
        } catch (Exception ex) {
            logger.log(Level.SEVERE, ex.getMessage() , ex);
            throw new RuntimeException(ex.getMessage(),ex);
        }finally{
            if(rs != null && !rs.isClosed()) rs.close();
            if(ps != null && !ps.isClosed()) ps.close();
            if(conn != null && !conn.isClosed()) conn.close();
            if(session != null && session.isOpen()) session.close();
        }
        
    }
    
    public JasperPrint generateReport(Map<String, Object> parametros,
            InputStream file,
            Supplier<JRDataSource> dataSourceExpr)
            throws JRException, FileNotFoundException, SQLException {
        
        Session session = getSession();
        Connection conn = getJDBCConnection(session);
        
        parametros.put("REPORT_CONNECTION",conn);
        
                
        try{
            return JasperFillManager.fillReport(file, parametros, dataSourceExpr.get());
        }finally{            
            if(conn != null && !conn.isClosed()) conn.close();
            if(session != null && session.isOpen()) session.close();
        }    
    }

    public JasperPrint generateReport(Supplier<Map<String, Object>> paramsSup,
            InputStream file)
            throws JRException, FileNotFoundException, SQLException {
        
        
        return JasperFillManager.fillReport(file, paramsSup.get(), new JREmptyDataSource(1));

    }
    
    public JasperPrint generateReport(Supplier<Map<String, Object>> paramsSup,
            Collection<D> reportItems,
            InputStream file)
            throws JRException, FileNotFoundException, SQLException {    
        
        return JasperFillManager.fillReport(file, paramsSup.get(), new JRBeanCollectionDataSource(reportItems));

    }
    
    
    
    
    
    protected ReportInitializer defaultInit = (columns,dataSource) -> {
        StyleBuilder boldStyle = stl.style().bold();
        StyleBuilder boldCenteredStyle = stl.style(boldStyle).setFont(stl.fontArialBold().setFontSize(7)).setHorizontalAlignment(HorizontalAlignment.CENTER).setVerticalAlignment(VerticalAlignment.MIDDLE);
        StyleBuilder cond = stl.style(stl.fontArial().setFontSize(7)).setLeftIndent(3).setRightIndent(3).setRightPadding(2).setTopPadding(4).setBottomPadding(2).setBorder(stl.pen(new Float("0.25"), LineStyle.SOLID));
        
        for(int i = 0 ; i < columns.length ; i++){            
            columns[i].setStyle(cond);
        }
        StyleBuilder columnTitleStyle = stl.style(boldCenteredStyle)
                .setBorder(stl.pen(new Float(0.5), LineStyle.SOLID))
                .setBackgroundColor(Color.LIGHT_GRAY);
        JasperReportBuilder report = report()//create new report design
                    .setPageFormat(PageType.A4, PageOrientation.LANDSCAPE)
                    .setPageMargin(margin(8)).setDetailSplitType(SplitType.PREVENT)
                    .setColumnTitleStyle(columnTitleStyle)
                    .setDetailStyle(stl.style().setBackgroundColor(Color.WHITE).setFont(stl.fontArial()).setFontSize(7).setBorder(stl.pen(new Float(0.25), LineStyle.SOLID))).columns(columns)               
                    .setDataSource(dataSource)
                    .setLocale(ptBR);
        return report;
    };
    
    public JasperReportBuilder generatorChain(Supplier<JasperReportBuilder>  initializer, 
            JaspeReportBuilderFunction... finishers){
        JasperReportBuilder report = initializer.get();
        for (JaspeReportBuilderFunction finisher : finishers) {
            report = finisher.build(report);
        }
        return report;                       
    }
    
    protected JaspeReportBuilderFunction resolveHeader(String format, Map<String,Object> params){
        return (report) -> {
            StyleBuilder boldCenteredStyle = stl.style().setFont(stl.fontArialBold().bold().setFontSize(7)).setHorizontalAlignment(HorizontalAlignment.CENTER).setVerticalAlignment(VerticalAlignment.MIDDLE);
            if("PDF".equalsIgnoreCase(format)){
                report.title((cmp.subreport(loadObject(resourceAsStream( ReportUtil.CABECALHO_REL_CARTEIRA_PROJETO))).setDataSource(new JREmptyDataSource(1)).setParameters(params).setMinHeight(100)));
                report.pageFooter(cmp.text("Página").setStyle(boldCenteredStyle), cmp.pageXslashY().setStyle(boldCenteredStyle));
           }
           return report;
        };
    }
    protected JaspeReportBuilderFunction createDefaultSummary(String format,Map<String,Object> params){
        return (report) -> {
            //StyleBuilder boldCenteredStyle = stl.style().setFont(stl.fontArialBold().bold().setFontSize(7)).setHorizontalAlignment(HorizontalAlignment.CENTER).setVerticalAlignment(VerticalAlignment.MIDDLE);
            if("PDF".equalsIgnoreCase(format)){
                report.summary(cmp.subreport(loadObject(resourceAsStream(ReportUtil.TOTAIS_RELATORIO_CART_PROJETOS))).setDataSource(new JREmptyDataSource(1)).setParameters(params));
           }
           return report;
        };
    }
    
    protected JasperPrint gerarInfoFiltrosExcel(Map<String,Object> parametros) {
        return gerarPrintAvulso(parametros,ReportUtil.CABECALHO_REL_CARTEIRA_PROJETO);
    }
    
    protected JasperPrint gerarTotaisExcel(Map<String,Object> parametros){
        return gerarPrintAvulso(parametros,ReportUtil.TOTAIS_RELATORIO_CART_PROJETOS);
    }
    
    protected FunctionalInterfaces.BinarySupplier dynamicReportsBinarySupplier(JasperReportBuilder finalReport,
            String arqCabecalho, String arqRodape ,EnumMimeTypes mimetype, Map<String,Object> params){
        return  () -> {
            if(EnumMimeTypes.XLSX.equals(mimetype)){
                return ReportUtil.xlsxExporter.export(Arrays.asList(finalReport.ignorePagination().toJasperPrint(),
                        gerarPrintAvulso(params, arqCabecalho),
                        gerarPrintAvulso(params, arqRodape)));
            }else {
                return mimetype.exportReport(finalReport).get();
            }
        };   
    }
    
    
    
    protected JasperPrint gerarPrintAvulso(Map<String,Object> parametros, String relatorio) {
        InputStream rep = resourceAsStream( relatorio);
        Supplier<JRDataSource> ds = () -> new JREmptyDataSource(1);
        try {
            return generateReport(parametros, rep, ds);
        } catch (JRException ex) {
            logger.log(Level.SEVERE, null, ex);
            throw new RuntimeException(ex);
        } catch (FileNotFoundException ex) {
            logger.getLogger(RelatorioGerencial04CarteiraProjetos.class.getName()).log(Level.SEVERE, null, ex);
            throw new RuntimeException(ex);
        } catch (SQLException ex) {
            logger.getLogger(RelatorioGerencial04CarteiraProjetos.class.getName()).log(Level.SEVERE, null, ex);
            throw new RuntimeException(ex);
        }
    }
    
    
    
    public Function<SecurityContext,HashMap<String, Object>> generateResponsibleForReportDS(){
        
        return (sc) -> {
                   
                   HashMap<String, Object> map = new HashMap<>();
                   map.put("DATA_GERACAO", new Date());
                   map.put("RESPONSAVEL_GERACAO",getPrincipalUserName(sc));
                   return map;
         };
    }
    
    protected String getPrincipalUserName(SecurityContext sc){
        if(sc == null || !(sc.getUserPrincipal() instanceof SecurityUser) || sc.getUserPrincipal().getName() == null ){
            return "Usuário sem autenticação";
        }else {
            User usr = userService.findById(((SecurityUser) sc.getUserPrincipal()).getId());
            return usr.getNome();
        }            
    }    
    
    protected <P> void set(final PreparedStatement ps, final Integer index, final P parameter) {
        try {
            if (parameter instanceof Integer) {
                ps.setInt(index, (Integer) parameter);
            } else if (parameter instanceof String) {
                ps.setString(index, (String) parameter);
            } else if (parameter instanceof Long) {
                ps.setLong(index, (Long) parameter);
            } else if (parameter instanceof Timestamp){
                ps.setTimestamp(index, (Timestamp) parameter);
            } else if (parameter instanceof Date ){
                ps.setDate(index, (java.sql.Date) parameter);
            } else if (parameter instanceof Boolean) {
                ps.setBoolean(index, (Boolean) parameter );
            } else {
                ps.setObject(index, parameter);
            }
        } catch (Exception e) {
            logger.severe("Erro ao passar parametro [" + index + "] do tipo " + parameter.getClass().getName());
            logger.log(Level.SEVERE,"Causado por:" + e.getMessage() ,e);
            throw new RuntimeException(e.getMessage(), e);
        }
    }    

    protected String fmtMonetary(Double valor) {
        NumberFormat moedaFormat = NumberFormat.getCurrencyInstance(ptBR);
        return valor == null ? null : moedaFormat.format(valor);
    }

    protected String fmtDate(Date date) {
        String dateString = "";
        if (date != null) {
            SimpleDateFormat sdf = new SimpleDateFormat("MMM/yyyy", ptBR);
            dateString = sdf.format(date);
        }
        return dateString;
    }
    
    protected Response buildResponse(JasperReportBuilder builder, String formatoResponse,String extension){
        try{
            BinarySupplier reportgen = binarySupplier(extension,builder);
            return buildResponse(reportgen, formatoResponse);
        }catch(Exception e){
            logger.log(Level.SEVERE, e.getMessage(), e);
            return Response.serverError().entity(e).build();
        }
        
    }
    
    protected Response buildResponse(BinarySupplier binarySupplier, String formato){
        try{
            return Response.ok().type(formato).entity(binarySupplier.get().toByteArray()).build();
        }catch(Exception e){
            logger.log(Level.SEVERE, e.getMessage(), e);
            return Response.serverError().entity(e).build();
        }
    }
    
    private BinarySupplier binarySupplier(String format,JasperReportBuilder jrb){
        EnumMimeTypes enumMT = EnumMimeTypes.valueOf(format.toUpperCase());
        return enumMT.exportReport(jrb);           
                
    }
    
     protected <T> String stringCollection(Function<T, String> strProperty, Collection<T> col) {
        return Optional.ofNullable(col).map((c) -> {
            if (c.isEmpty()) {
                return "Todos";
            } else {
                return c.stream().map(strProperty).collect(Collectors.joining(","));
            }
        }).orElse("Todos");
    }
     
     
    
    

}
