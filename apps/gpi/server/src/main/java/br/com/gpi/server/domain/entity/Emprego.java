package br.com.gpi.server.domain.entity;

import br.com.gpi.server.core.jpa.converter.TipoEmpregoEnumConverter;
import br.com.gpi.server.domain.common.AuditedEntity;
import com.fasterxml.jackson.annotation.JsonIgnore;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import org.hibernate.envers.Audited;

@Audited
@Entity
@Table(name = "Emprego", uniqueConstraints = @UniqueConstraint(columnNames = {"tipo", "ano"}))
@NamedQueries({
    @NamedQuery(name = Emprego.Query.locateByProjetoId, query = "SELECT p FROM Emprego p WHERE p.projeto.id = ?")
})
public class Emprego extends AuditedEntity<Long> {

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.REFRESH, CascadeType.DETACH})
    @JoinColumn(name = "projeto_id")
    private Projeto projeto;

    @NotNull
    private Integer ano;

    private Integer direto;

    private Integer indireto;
  
    @Enumerated
    @Column(name = "tipo", insertable = false, updatable = false)
    private TipoEmprego tipoEmprego;
    
    @JsonIgnore
    @Column(name = "tipo")
    private Integer tipoInt;
    
    @Column(name = "diretoRealizado")
    private Integer diretoRealizado;

    public Emprego() {
    }

    public Projeto getProjeto() {
        return projeto;
    }

    public void setProjeto(Projeto projeto) {
        this.projeto = projeto;
    }

    public Integer getAno() {
        return ano;
    }

    public void setAno(Integer ano) {
        this.ano = ano;
    }

    public Integer getDireto() {
        return direto;
    }

    public void setDireto(Integer direto) {
        this.direto = direto;
    }

    public Integer getIndireto() {
        return indireto;
    }

    public void setIndireto(Integer indireto) {
        this.indireto = indireto;
    }

    public TipoEmprego getTipoEmprego() {
        return tipoEmprego;
    }

    public void setTipoEmprego(TipoEmprego tipoEmprego) {
        if(tipoEmprego == null) {
            return;
        }
        this.tipoEmprego = tipoEmprego;
        this.tipoInt = (new TipoEmpregoEnumConverter()).convertToDatabaseColumn(tipoEmprego);
    }
    
     public Integer getTipoInt() {
        return tipoInt;
    }

    public final void setTipoInt(Integer tipoInt) {
        if(tipoInt == null) {
            return;
        }
        this.tipoInt = tipoInt;
        this.tipoEmprego = (new TipoEmpregoEnumConverter()).convertToEntityAttribute(tipoInt);
    }
    
    public static class Query {

        public static final String locateByProjetoId = "Emprego.locateByProjetoId";
    }

    public Integer getDiretoRealizado() {
        return diretoRealizado;
    }

    public void setDiretoRealizado(Integer diretoRealizado) {
        this.diretoRealizado = diretoRealizado;
    }
}
