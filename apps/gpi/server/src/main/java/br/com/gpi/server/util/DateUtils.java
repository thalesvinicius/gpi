package br.com.gpi.server.util;

import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.Date;
import java.util.Locale;

public class DateUtils {
    
    private static final Locale ptBR = new Locale("pt", "BR");
    public static final String MMMyyyy = "MMM/yyyy";
    public static final String ddMMyyyy = "dd/MM/yyyy";    

    public static LocalDateTime convertToLocalDateTime(Date date) {
        Instant instant = Instant.ofEpochMilli(date.getTime());
        return LocalDateTime.ofInstant(instant, ZoneOffset.UTC);
    }
    
    public static LocalDate convertToLocalDate(Date date) {
        LocalDate localDate = null;
        if (date != null) {
            Instant instant = Instant.ofEpochMilli(date.getTime());
            localDate = LocalDateTime.ofInstant(instant, ZoneId.systemDefault()).toLocalDate();
        }
        return localDate;
    }
    
    /*
    *   Converte a data para o formato passado
    */
    public static String fmtString(Date date, String format) {
        String dateString = "";
        if (date != null) {
            SimpleDateFormat sdf = new SimpleDateFormat(format, ptBR);
            dateString = sdf.format(date);
        }
        return dateString;
    }    
    
    /*
    *   Converte milisseconds para data, caso dê erro retorna null
    */
    public static Date fromMillisecondsToDate(Long dateMill){
        if(dateMill != null){
            return new Date(dateMill);
        }
        return null;
    }    
}
