package br.com.gpi.server.domain.repository;

import br.com.gpi.server.domain.entity.AtividadeLocal;
import java.util.List;
import org.apache.deltaspike.data.api.EntityRepository;
import org.apache.deltaspike.data.api.Modifying;
import org.apache.deltaspike.data.api.Query;
import org.apache.deltaspike.data.api.Repository;
import org.apache.deltaspike.data.api.criteria.CriteriaSupport;

@Repository(forEntity = AtividadeLocal.class)
public abstract class AtividadeLocalRepository  implements CriteriaSupport<AtividadeLocal>,EntityRepository<AtividadeLocal, Long>{

    @Modifying
    @Query(named = AtividadeLocal.Query.findAllByLocalId)
    public abstract List<AtividadeLocal> findAllByLocalId(Long id);
    
    @Modifying
    @Query(named = AtividadeLocal.Query.findAllActiveByLocalId)
    public abstract List<AtividadeLocal> findAllActiveByLocalId(Long id);

    @Modifying
    @Query(named = AtividadeLocal.Query.findAllByLocalAtivo)
    public abstract List<AtividadeLocal> findAllByLocalAtivo();    
}
