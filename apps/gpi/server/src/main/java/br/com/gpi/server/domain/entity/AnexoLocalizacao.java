package br.com.gpi.server.domain.entity;

import br.com.gpi.server.domain.common.BaseEntity;
import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import org.hibernate.envers.Audited;

/**
 * @since 01/07/2014
 * @author <a href="mailto:thalesvd@algartech.com">Thales Dias</a>
 */
@Audited
@Entity
@Table(name = "AnexoLocalizacao")
public class AnexoLocalizacao extends BaseEntity<Long> implements Serializable {

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "localizacao_id")
    private Localizacao localizacao;
    
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "anexo_id")
    private Anexo anexo;

    @NotNull
    @Column(name = "anexoAreaLocal", nullable = false, columnDefinition = "bit")
    private Boolean anexoAreaLocal;

    @Column(name = "tipoLocalizacao", nullable = true)
    private int tipoLocalizacao;

    public Boolean isAnexoAreaLocal() {
        return anexoAreaLocal;
    }

    public void setAnexoAreaLocal(Boolean anexoAreaLocal) {
        this.anexoAreaLocal = anexoAreaLocal;
    }

    public Localizacao getLocalizacao() {
        return localizacao;
    }

    public void setLocalizacao(Localizacao localizacao) {
        this.localizacao = localizacao;
    }

    public int getTipoLocalizacao() {
        return tipoLocalizacao;
    }

    public void setTipoLocalizacao(int tipoLocalizacao) {
        this.tipoLocalizacao = tipoLocalizacao;
    }

    public Anexo getAnexo() {
        return anexo;
    }

    public void setAnexo(Anexo anexo) {
        this.anexo = anexo;
    }

}
