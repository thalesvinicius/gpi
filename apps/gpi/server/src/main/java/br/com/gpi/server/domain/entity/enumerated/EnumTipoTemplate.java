package br.com.gpi.server.domain.entity.enumerated;

import br.com.gpi.server.core.exception.EnumException;
import br.com.gpi.server.core.jpa.typesafe.EnumerationEntityType;

public enum EnumTipoTemplate implements EnumerationEntityType<Integer> {
 
    PROJETO(0, "Projeto"),
    PROTOCOLO_INTENCOES(1, "Protocolo de Intenções"),
    TERMO_ADITIVO(2, "Termo Aditivo");

    private final int id;
    private final String descricao;

    private EnumTipoTemplate(int id, String descricao) {
        this.id = id;
        this.descricao = descricao;
    }    
    
    @Override
    public Integer getId() {
        return this.id;
    }

    @Override
    public String getDescription() {
        return this.descricao;
    }
    
    public static EnumTipoTemplate getEnumTipoTemplate(Integer id) {
        for (EnumTipoTemplate tipoTemplate : values()) {
            if (tipoTemplate.id == id) {
                return tipoTemplate;
            }
        }
        throw new EnumException("EnumTipoTemplate couldn't load.");
    }          
    
    
}
