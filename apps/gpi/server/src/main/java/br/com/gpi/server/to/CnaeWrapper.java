package br.com.gpi.server.to;

import br.com.gpi.server.domain.entity.CNAE;
import java.util.List;
import javax.enterprise.inject.Model;

@Model
public class CnaeWrapper {

    private List<CNAE> divisoesSelecionadas;
    
    private List<CNAE> gruposCarregados;
    private List<CNAE> gruposSelecionados;
    
    private List<CNAE> classesCarregadas;
    private List<CNAE> classesSelecionadas;
    
    private List<CNAE> subclassesCarregadas;

    public CnaeWrapper() {
    }

    public List<CNAE> getDivisoesSelecionadas() {
        return divisoesSelecionadas;
    }

    public void setDivisoesSelecionadas(List<CNAE> divisoesSelecionadas) {
        this.divisoesSelecionadas = divisoesSelecionadas;
    }

    public List<CNAE> getGruposCarregados() {
        return gruposCarregados;
    }

    public void setGruposCarregados(List<CNAE> gruposCarregados) {
        this.gruposCarregados = gruposCarregados;
    }

    public List<CNAE> getGruposSelecionados() {
        return gruposSelecionados;
    }

    public void setGruposSelecionados(List<CNAE> gruposSelecionados) {
        this.gruposSelecionados = gruposSelecionados;
    }

    public List<CNAE> getClassesCarregadas() {
        return classesCarregadas;
    }

    public void setClassesCarregadas(List<CNAE> classesCarregadas) {
        this.classesCarregadas = classesCarregadas;
    }

    public List<CNAE> getClassesSelecionadas() {
        return classesSelecionadas;
    }

    public void setClassesSelecionadas(List<CNAE> classesSelecionadas) {
        this.classesSelecionadas = classesSelecionadas;
    }

    public List<CNAE> getSubclassesCarregadas() {
        return subclassesCarregadas;
    }

    public void setSubclassesCarregadas(List<CNAE> subclassesCarregadas) {
        this.subclassesCarregadas = subclassesCarregadas;
    }

}
