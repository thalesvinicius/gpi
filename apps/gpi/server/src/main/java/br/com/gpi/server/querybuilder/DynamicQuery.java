package br.com.gpi.server.querybuilder;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Set;
import java.util.TreeSet;
import static java.util.stream.Collectors.toSet;
import java.util.stream.Stream;

public class DynamicQuery {

    private final Set<Field> selectFields;
    private final Set<Condition> conditionFields;
    private final Set<Field> orderByFields;

    public DynamicQuery() {
        super();
        selectFields = new LinkedHashSet<>();
        conditionFields = new LinkedHashSet<>();
        orderByFields = new LinkedHashSet<>();
    }

    public DynamicQuery(Set<Field> selectFields, Set<Condition> conditionFields, Set<Field> orderByFields) {
        this.selectFields = selectFields;
        this.conditionFields = conditionFields;
        this.orderByFields = orderByFields;
    }

    public void addSelect(Field... fields) {
        selectFields.addAll(Arrays.asList(fields));
    }

    public void addSelect(Collection<Field> fields) {
        selectFields.addAll(fields);
    }

    public void addCondition(Condition... conditions) {
        conditionFields.addAll(Arrays.asList(conditions));
    }

    public void addOrderBy(List<Field> fields) {
        orderByFields.addAll(fields);
    }

    public void addOrderBy(Field... fields) {
        orderByFields.addAll(Arrays.asList(fields));
    }

    /**
     *
     * @return sql contendo as condições e as clausulas de seleção
     */
    @JsonIgnore
    public String getSQL() {
        StringBuilder query = new StringBuilder();
        query.append("SELECT ");
//        query.append("DISTINCT ");
        String transformedSelectFields = transformFields(selectFields);
        query.append(transformedSelectFields);
        query.append(" FROM ");
        String transformedRelationships = buildRelationships();
        query.append(transformedRelationships);
        if (!conditionFields.isEmpty()) {
            query.append(" WHERE ");
            transformCondiditions(conditionFields, query);
        }
        if (!orderByFields.isEmpty()) {
            boolean orderBy = false;
            for (Field field : orderByFields) {
//                if (selectFields.contains(field)) {
                if (query.indexOf(field.getTabela().getName()) != -1) {
                    orderBy = true;
                    break;
                }
            }
            if (orderBy) {
                query.append(" ORDER BY ");
                for (Field field : orderByFields) {
//                    if (selectFields.contains(field)) {
                    if (query.indexOf(field.getTabela().getName()) != -1) {
                        query.append(field.getTabela().getAlias()).append(".").append(field.getNome());
                        query.append(", ");
                    }
                }
                query.delete(query.length() - 2, query.length());
            }
        }

        return query.toString();
    }

    /**
     * Transforma os campos selecionados na enumeração utilizada na clausula
     * select por exemplo: Se estiverem selecionados os campos nomeEmpresa e
     * unidadeEmpresa esse metodo transformar o objeto na string
     * "emp.nomeEmpresa, und.nomeUnidade" para ser utilizado na clausula select.
     *
     * @param fields
     * @return (alias.fieldName, alias2.fieldName2)
     */
    private String transformFields(Set<Field> fields) {
        StringBuilder result = new StringBuilder();
        Iterator<Field> iterator = fields.iterator();
        while (iterator.hasNext()) {
            Field field = iterator.next();
            result.append(field.getSQL());
            if (iterator.hasNext()) {
                result.append(",");
            }
            result.append(" ");
        }
        return result.toString();
    }

    private String buildRelationships() {
        StringBuilder builder = new StringBuilder();
        Set<List<Field>> fieldsMatrix = this.conditionFields.stream().map((c) -> c.getFields()).collect(toSet());
        Set<Table> conditionTablesSet = fieldsMatrix.stream().flatMap((d) -> d.stream().map((c) -> c.getTabela())).collect(toSet());

        //Concatena todas as tabelas selecionadas em um unico conjunto de dados
        Set<Table> allTables = new TreeSet<>(Stream.concat(selectFields.stream().map((c) -> c.getTabela()), conditionTablesSet.stream()).collect(toSet()));
        //Filtra todos os campos selecionados procurando rootTables
        Set<Table> rootTables = new TreeSet<>(allTables.stream().filter((table) -> table.isRootTable()).collect(toSet()));

        Set<Table> allNotRootDependencyTables = new TreeSet();

        for (Table table : allTables) {
            if (table.getDependency() != null) {
                if (table.isRootTable() && !rootTables.contains(table.getDependency().getToTable())) {
                    rootTables.add(table.getDependency().getToTable());
                } else if (!allNotRootDependencyTables.contains(table.getDependency().getToTable())) {
                    allNotRootDependencyTables.add(table.getDependency().getToTable());
                }
            }
        }

        allTables.addAll(allNotRootDependencyTables);

        //Filtra todos os campos selecionados procurando tabelas que não são rootTables        
        Set<Table> tables = new TreeSet<>(allTables.stream().filter((table) -> !table.isRootTable()).collect(toSet()));
        //Estabelece relacionamentos entre as rootTables
        buildRootRelationships(builder, rootTables);
        //Estabelece relacionamento entre as tabelas normais
        Set<Table> tablesInQuery = new TreeSet<>(rootTables);
        buildNormalRelationships(builder, tables, tablesInQuery);

        return builder.toString();
    }

    private String buildNormalRelationships(StringBuilder builder, Set<Table> tablesToJoin, Set<Table> tablesInQuery) {
        Queue<Table> queue = new PriorityQueue<>(tablesToJoin);
        while (!queue.isEmpty()) {
            Table table = queue.poll();
            if (table.getDependency() != null) {
                if (tablesInQuery.contains(table)) {
                    continue;
                }
                tablesInQuery.add(table);
                tablesInQuery.add(table.getDependency().getToTable());
                builder.append(getSQLJoinStatement(table.getDependency().getToTable(), table, table.getDependency().getFkName()));
            }

            for (Table additionalTable : table.getAditionalTables()) {
                if (tablesInQuery.contains(additionalTable)) {
                    continue;
                }
                tablesInQuery.add(additionalTable);
                builder.append(getSQLJoinStatement(additionalTable.getDependency().getToTable(), additionalTable, additionalTable.getDependency().getFkName()));
            }
            builder.append(" ");
        }
        return builder.toString();
    }

    private void transformCondiditions(Set<Condition> conditionFields, StringBuilder builder) {
        for (Condition condition : conditionFields) {
            builder.append(condition.getSQL());
            builder.append(" AND ");
        }

        builder.delete(builder.length() - 4, builder.length());
    }

    /**
     * Realiza relacionamentos principais de acordo com as prioridades definidas
     * RootTables são tabelas que podem ser selecionadas sem existir um
     * relacionamento (não tem dependências) porém se existirem duas ou mais
     * root tables esse método saberá como estabelecer a relação entre elas
     *
     * @param builder SQL statement
     * @param rootTables tables without dependencies
     */
    private void buildRootRelationships(StringBuilder builder, Set<Table> rootTables) {
        Queue<Table> queue = new PriorityQueue<>(rootTables);
        for (Table table : rootTables) {
            if (table.getDependency() != null && queue.contains(table.getDependency().getToTable())) {
                rootTables.add(table.getDependency().getToTable());
            }
        }
        if (!queue.isEmpty()) {
            Table root = queue.poll();
            builder.append(root.getSQLFromDeclaration());

            Set<Table> rootTablesInQuery = new HashSet<>();
            rootTablesInQuery.add(root);
            while (!queue.isEmpty()) {
                Table otherRootTable = queue.poll();
                // Percorre as tabelas que ainda não estão na query, construindo as relações com a root table que já está na query
                for (Table tableInQuery : rootTablesInQuery) {
                    if (tableInQuery.getPossibleLinks().containsKey(otherRootTable.getAlias())) {
                        TableLink rootDependency = tableInQuery.getLink(otherRootTable.getAlias());
                        if (rootDependency instanceof ChainedTableLink) {
                            ChainedTableLink linkDependency = (ChainedTableLink) rootDependency;
                            builder.append(linkDependency.createLinkStatement(tableInQuery));
                        } else {
                            builder.append(getSQLJoinStatement(tableInQuery, rootDependency.getToTable(), rootDependency.getFkName()));
                        }
                        rootTablesInQuery.add(otherRootTable);
                        break;
                    }
                }

                if (otherRootTable.getDependency() != null) {
                    if (rootTablesInQuery.contains(otherRootTable.getDependency().getToTable())) {
                        continue;
                    }
                    if (!rootTablesInQuery.contains(otherRootTable)) {
                        rootTablesInQuery.add(otherRootTable);
                    }
                    rootTablesInQuery.add(otherRootTable.getDependency().getToTable());
                    builder.append(getSQLJoinStatement(otherRootTable.getDependency().getToTable(), otherRootTable, otherRootTable.getDependency().getFkName()));
                }
            }
        }
    }

    @JsonIgnore
    public String getSQLJoinStatement(Table fromTable, Table targetTable, String fk
    ) {
        StringBuilder result = new StringBuilder();
        result.append(" LEFT JOIN ");
        result.append(targetTable.getName());
        result.append(" ");
        result.append(targetTable.getAlias());
        result.append(" ON ");
        result.append(targetTable.getAlias()).append(".").append(fk);
        result.append(" = ");
        result.append(fromTable.getAlias()).append(".").append(fromTable.getPkName());
        return result.toString();
    }

}
