package br.com.gpi.server.domain.service;

import br.com.gpi.server.domain.common.BaseService;
import br.com.gpi.server.domain.common.QueryParameter;
import br.com.gpi.server.domain.entity.AccessToken;
import br.com.gpi.server.domain.entity.Cargo;
import br.com.gpi.server.domain.entity.Cargo_;
import br.com.gpi.server.domain.entity.Departamento;
import br.com.gpi.server.domain.entity.Departamento_;
import br.com.gpi.server.domain.entity.Empresa;
import br.com.gpi.server.domain.entity.Empresa_;
import br.com.gpi.server.domain.entity.GrupoUsuario;
import br.com.gpi.server.domain.entity.GrupoUsuario_;
import br.com.gpi.server.domain.entity.TipoUsuario;
import br.com.gpi.server.domain.entity.UnidadeEmpresa;
import br.com.gpi.server.domain.entity.UnidadeEmpresa_;
import br.com.gpi.server.domain.entity.User;
import br.com.gpi.server.domain.entity.UserGroup;
import br.com.gpi.server.domain.entity.UsuarioExterno;
import br.com.gpi.server.domain.entity.UsuarioExterno_;
import br.com.gpi.server.domain.entity.UsuarioInterno;
import br.com.gpi.server.domain.entity.UsuarioInterno_;
import br.com.gpi.server.domain.repository.UserExternalRepository;
import br.com.gpi.server.domain.repository.UserRepository;
import br.com.gpi.server.domain.user.exception.AuthenticationException;
import br.com.gpi.server.domain.user.exception.CannotInactivateUserExceptionException;
import br.com.gpi.server.domain.user.exception.DuplicateUserException;
import br.com.gpi.server.domain.user.model.AuthenticatedUserToken;
import br.com.gpi.server.domain.user.model.CreateUserRequest;
import br.com.gpi.server.domain.user.model.SecurityUser;
import br.com.gpi.server.to.UsuarioExternoTO;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.transaction.NotSupportedException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;
import javax.validation.Validator;
import javax.ws.rs.core.SecurityContext;
import org.apache.commons.collections.CollectionUtils;

public class UserService extends BaseService<User> {

    private final int FIRST_ROW = 0;
    @Inject
    private Validator validator;
    @Inject
    private UserRepository repository;
    @Inject
    private UserExternalRepository usuarioExternoRepository;

    @Inject
    private UserTransaction transaction;

    @Inject
    public UserService(EntityManager em) {
        super(User.class, em);
    }

    public List<UsuarioExternoTO> findUserExternalByFilters(Long idEmpresa) {
        CriteriaBuilder builder = getEm().getCriteriaBuilder();
        CriteriaQuery<UsuarioExternoTO> query = builder.createQuery(UsuarioExternoTO.class);
        Root<UsuarioExterno> fromUsuarioExterno = query.from(UsuarioExterno.class);
        Join<UsuarioExterno, Empresa> fromEmpresa = fromUsuarioExterno.join(UsuarioExterno_.empresa);
        Join<UsuarioExterno, UnidadeEmpresa> fromUnidade = fromUsuarioExterno.join(UsuarioExterno_.unidade, JoinType.LEFT);
        Join<UsuarioExterno, User> fromUsuario = fromUsuarioExterno.join(UsuarioExterno_.usuarioResponsavel, JoinType.LEFT);
        List<Predicate> conditions = new ArrayList();
        if (idEmpresa != null) {
            conditions.add(builder.equal(fromUsuarioExterno.get(UsuarioExterno_.empresaId), idEmpresa));
        }

        TypedQuery<UsuarioExternoTO> typedQuery = getEm().createQuery(query
                .multiselect(fromUsuarioExterno.get(UsuarioExterno_.id),
                        fromEmpresa.get(Empresa_.nomePrincipal),
                        fromUnidade.get(UnidadeEmpresa_.nome),
                        fromUsuarioExterno.get(UsuarioExterno_.nome),
                        fromUsuarioExterno.get(UsuarioExterno_.email),
                        fromUsuarioExterno.get(UsuarioExterno_.ativo),
                        fromUsuarioExterno.get(UsuarioExterno_.cargoExterno),
                        fromUsuarioExterno.get(UsuarioExterno_.telefone),
                        fromUsuarioExterno.get(UsuarioExterno_.telefone2),
                        fromUsuarioExterno.get(UsuarioExterno_.mailing),
                        fromUsuarioExterno.get(UsuarioExterno_.dataUltimaAlteracao),
                        fromUsuario,
                        fromUsuarioExterno.get(UsuarioExterno_.ativo),
                        fromUsuarioExterno.get(UsuarioExterno_.password),
                        fromUsuarioExterno.get(UsuarioExterno_.empresaId),
                        fromUsuarioExterno.get(UsuarioExterno_.unidadeId))
                .where(conditions.toArray(new Predicate[]{}))
                .distinct(true)
        );

        return typedQuery.getResultList();
    }

    public List<UsuarioInterno> findByFilters(String matricula, String nome, Integer grupoUsuarioId,
            Integer departamentoId, Integer cargoId, Boolean ativo) {
        CriteriaBuilder builder = getEm().getCriteriaBuilder();
        CriteriaQuery<UsuarioInterno> query = builder.createQuery(UsuarioInterno.class);
        Root<UsuarioInterno> fromUsuarioInterno = query.from(UsuarioInterno.class);
        List<Predicate> conditions = new ArrayList();

        if (matricula != null) {
            conditions.add(builder.equal(fromUsuarioInterno.get(UsuarioInterno_.matricula), matricula));
        }

        if (nome != null) {
            conditions.add(builder.like(fromUsuarioInterno.get(UsuarioInterno_.nome), "%" + nome + "%"));
        }

        if (grupoUsuarioId != null) {
            Join<UsuarioInterno, GrupoUsuario> fromGrupoUsuario = fromUsuarioInterno.join(UsuarioInterno_.role);
            conditions.add(builder.equal(fromGrupoUsuario.get(GrupoUsuario_.id), grupoUsuarioId));
        }
        if (cargoId != null) {
            Join<UsuarioInterno, Cargo> fromCargo = fromUsuarioInterno.join(UsuarioInterno_.cargo);
            conditions.add(builder.equal(fromCargo.get(Cargo_.id), cargoId));
        }
        if (departamentoId != null) {
            Join<UsuarioInterno, Departamento> fromDeparamento = fromUsuarioInterno.join(UsuarioInterno_.departamento);
            conditions.add(builder.equal(fromDeparamento.get(Departamento_.id), departamentoId));
        }

        if (ativo != null) {
            conditions.add(builder.equal(fromUsuarioInterno.get(UsuarioInterno_.ativo), ativo));
        }

        TypedQuery<UsuarioInterno> typedQuery = getEm().createQuery(query.select(fromUsuarioInterno).where(conditions.toArray(new Predicate[]{})).orderBy(builder.asc(fromUsuarioInterno.get(UsuarioInterno_.nome))).distinct(true));

        return typedQuery.getResultList();
    }

    public List<UsuarioInterno> findAnalistasByDepartamentos(List<Long> departamentosId) {
        CriteriaBuilder builder = getEm().getCriteriaBuilder();
        CriteriaQuery<UsuarioInterno> query = builder.createQuery(UsuarioInterno.class);
        Root<UsuarioInterno> fromUsuarioInterno = query.from(UsuarioInterno.class);
        List<Predicate> conditions = new ArrayList();

        Join<UsuarioInterno, Departamento> fromDeparamento = fromUsuarioInterno.join(UsuarioInterno_.departamento);
        conditions.add(fromDeparamento.get(Departamento_.id).in(departamentosId));

        TypedQuery<UsuarioInterno> typedQuery
                = getEm().createQuery(query.select(fromUsuarioInterno)
                        .where(conditions.toArray(new Predicate[]{}))
                        .orderBy(builder.asc(fromUsuarioInterno.get(UsuarioInterno_.nome)))
                        .distinct(true));

        return typedQuery.getResultList();
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public AuthenticatedUserToken createUser(CreateUserRequest request, TipoUsuario tipoUsuario, UserGroup role) {
        validate(validator, request);
        User searchedForUser = findByLogin(request.getUser().getEmail(), tipoUsuario);;

        if (searchedForUser != null) {
            throw new DuplicateUserException();
        }

        User newUser = createNewUser(request, role);
        AuthenticatedUserToken token = new AuthenticatedUserToken(newUser.getEmail());
        merge(newUser);
        return token;
    }

    private User createNewUser(CreateUserRequest request, UserGroup role) {
        UsuarioInterno userToSave = new UsuarioInterno(request.getUser());
        try {
            userToSave.setPassword(userToSave.hashPassword(request.getPassword()));
        } catch (Exception e) {
            throw new AuthenticationException();
        }
        userToSave.setRoleByEnum(role);
        return userToSave;
    }

    public boolean checkUserCredentials(User user, String password) {
        return user.hashPassword(password).equals(user.getPassword());
    }

    public User findInternalByLogin(String login) {
        return locateByNamedQuery(UsuarioInterno.Query.locateByLogin, QueryParameter.with("login", login).parameters());
    }

    public User findExternalByLogin(String login) {
        return locateByNamedQuery(UsuarioExterno.Query.locateByLogin, QueryParameter.with("login", login).parameters());
    }

    public void createNewSession(User user, String token) {
        AccessToken accessToken = new AccessToken(user, token);
        user.getAccessTokens().add(accessToken);
        repository.save(user);
    }

    public User loadUser(SecurityContext context) {
        SecurityUser externalUser = (SecurityUser) context.getUserPrincipal();
        return findById(externalUser.getId());
    }

    public void remove(User user) throws SystemException {
        boolean hasTransaction = false;
        try {
            try {
                transaction.begin();
            } catch (NotSupportedException e) {
                hasTransaction = true;
            }

            this.getEm().joinTransaction();
            
            if (!user.isAtivo()) {
                super.remove(user);
            }
            
            if (!hasTransaction) {
                transaction.commit();
            }
        } catch (Exception ex) {
            if (!hasTransaction) {
                transaction.rollback();
            }
            throw new RuntimeException(ex);
        }
    }

    public void removeAll(List<User> users) throws SystemException {
        try {
            transaction.begin();
            this.getEm().joinTransaction();

            for (User user : users) {
                this.remove(user);
            }

            transaction.commit();
        } catch (Exception ex) {
            transaction.rollback();
            throw new RuntimeException(ex);
        }
    }

    public void remove(List<Long> ids) throws SystemException {
        List<User> users = this.createQuery(User.Query.findByIds, ids).getResultList();
        this.removeAll(users);
    }

    public User findByLogin(String login, TipoUsuario tipoUsuario) {
        if (tipoUsuario.equals(TipoUsuario.EXTERNO)) {
            return findExternalByLogin(login);
        } else {
            return findInternalByLogin(login);
        }
    }

    public User saveOrUpdate(User user, SecurityUser loggedUserWrap) {
        if (user.getId() != null && (Long.compare(user.getId(), loggedUserWrap.getId())) == 0) {
            user.setUsuarioResponsavel(null);
        } else {
            user.setUsuarioResponsavel(repository.findBy(loggedUserWrap.getId()));
        }
        user.setDataUltimaAlteracao(new Date());
        if (user.getId() == null) {
            user.setPassword(user.hashPassword("algar123"));
        }

        return repository.save(user);
    }

    @Deprecated
    public User createExternalUser(UsuarioExterno user, SecurityUser loggedUserWrap) {
        user.setPassword("-------");
        if (user.isAtivo() == null) {
            user.setAtivo(Boolean.FALSE);
        }

        return saveExternalUser(user, loggedUserWrap);
    }

    @Deprecated
    public UsuarioExterno editExternalUser(UsuarioExterno user, SecurityUser loggedUserWrap) {
        if (!user.isAtivo()) {
            if ((user.isMailing() != null && user.isMailing()) || CollectionUtils.isNotEmpty(this.createTypedQuery(Empresa.class, Empresa.Query.FIND_EMPRESA_BY_USUARIO_EXTERNO_RESPONSAVEL, user.getId()).getResultList())) {
                throw new CannotInactivateUserExceptionException();
            }
        }
        return saveExternalUser(user, loggedUserWrap);
    }

    public User createExternalUserTransactional(UsuarioExterno user, SecurityUser loggedUserWrap) throws SystemException {
        boolean hasTransaction = false;
        try {
            try {
                transaction.begin();
            } catch (NotSupportedException e) {
                hasTransaction = true;
            }

            this.getEm().joinTransaction();

            user.setPassword("-------");
            if (user.isAtivo() == null) {
                user.setAtivo(Boolean.FALSE);
            }
            if (!hasTransaction) {
                transaction.commit();
            }
            return saveExternalUserTransactional(user, loggedUserWrap);
        } catch (Exception ex) {
            if (!hasTransaction) {
                transaction.rollback();
            }
            throw new RuntimeException(ex);
        }
    }

    public UsuarioExterno editExternalUserTransactional(UsuarioExterno user, SecurityUser loggedUserWrap) throws SystemException {
        try {
            try {
                transaction.begin();
            } catch (NotSupportedException e) {

            }
            this.getEm().joinTransaction();

            if (!user.isAtivo()) {
                if ((user.isMailing() != null && user.isMailing()) || CollectionUtils.isNotEmpty(this.createTypedQuery(Empresa.class, Empresa.Query.FIND_EMPRESA_BY_USUARIO_EXTERNO_RESPONSAVEL, user.getId()).getResultList())) {
                    throw new CannotInactivateUserExceptionException();
                }
            }
            transaction.commit();
            return saveExternalUserTransactional(user, loggedUserWrap);
        } catch (Exception ex) {
            transaction.rollback();
            throw new RuntimeException(ex);
        }
    }

    @Deprecated
    private UsuarioExterno saveExternalUser(UsuarioExterno user, SecurityUser loggedUserWrap) {
        checkDuplicatedEmail(user, user.getId(), user.getEmpresaId());

        if (user.getId() != null && (Long.compare(user.getId(), loggedUserWrap.getId())) == 0) {
            user.setUsuarioResponsavel(null);
        } else {
            if (loggedUserWrap.getId() != null) {
                user.setUsuarioResponsavel(repository.findBy(loggedUserWrap.getId()));
            } else {
                user.setUsuarioResponsavel(null);
            }
        }

        user.setDataUltimaAlteracao(new Date());
        //FIXME: Retirar após ser feita a mudança de cargo para string        
        user.setCargo(new Cargo(1L));
        user.setRole(new GrupoUsuario(GrupoUsuario.USUARIO_EXTERNO_RESPONSAVEL, null));
        return usuarioExternoRepository.save(user);
    }

    private UsuarioExterno saveExternalUserTransactional(UsuarioExterno user, SecurityUser loggedUserWrap) throws SystemException {

        checkDuplicatedEmail(user, user.getId(), user.getEmpresaId());

        if (user.getId() != null && (Long.compare(user.getId(), loggedUserWrap.getId())) == 0) {
            user.setUsuarioResponsavel(null);
        } else {
            if (loggedUserWrap.getId() != null) {
                user.setUsuarioResponsavel(this.getEm().find(UsuarioExterno.class, loggedUserWrap.getId()));
            } else {
                user.setUsuarioResponsavel(null);
            }
        }

        user.setDataUltimaAlteracao(new Date());
        //FIXME: Retirar após ser feita a mudança de cargo para string        
        user.setCargo(new Cargo(1L));
        user.setRole(new GrupoUsuario(GrupoUsuario.USUARIO_EXTERNO_RESPONSAVEL, null));

        return (UsuarioExterno) this.merge(user);
    }

    public User activateInactivateExternalUser(UsuarioExterno user, SecurityUser loggedUserWrap) {
        if (user.isAtivo()) {
            user.setAtivo(Boolean.FALSE);
        } else {
            user.setAtivo(Boolean.TRUE);
        }

        return this.editExternalUser(user, loggedUserWrap);
    }

    public boolean isEmailValid(String email, Long id, Long empresaId) {

        if (id != null) {
            Long numEmails = usuarioExternoRepository.countByLoginEqualsAndIdNotEquals(email, id, empresaId, Boolean.TRUE);
            if (numEmails > 0) {
                return false;
            } else {
                return true;
            }
        }

        List<User> userExternal = findByNamedQueryWithLimitedResult(UsuarioExterno.Query.locateByLogin2, 1, email, empresaId);
        if (userExternal != null && !userExternal.isEmpty() && userExternal.get(0) != null) {
            return false;
        } else {
            return true;
        }

    }

    private void checkDuplicatedEmail(User user, Long id, Long empresaId) {
        if (!isEmailValid(user.getEmail(), id, empresaId)) {
            throw new ExternalUserDuplicatedException();
        }
    }
}
