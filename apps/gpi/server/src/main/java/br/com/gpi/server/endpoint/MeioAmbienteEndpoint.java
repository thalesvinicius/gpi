package br.com.gpi.server.endpoint;

import br.com.gpi.server.domain.entity.MeioAmbiente;
import br.com.gpi.server.domain.repository.MeioAmbienteRepository;
import br.com.gpi.server.domain.repository.ProjetoRepository;
import br.com.gpi.server.domain.repository.UserRepository;
import br.com.gpi.server.domain.service.MeioAmbienteService;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.inject.Inject;
import javax.transaction.SystemException;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

@Path("/projeto/{idProjeto}/meioAmbiente")
@Stateless
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
@TransactionManagement(TransactionManagementType.BEAN)
public class MeioAmbienteEndpoint {

    @Inject
    private MeioAmbienteService meioAmbienteService;
    @Inject
    private MeioAmbienteRepository meioAmbienteRepository;
    @Inject
    private ProjetoRepository projetoRepository;
    @Inject
    private UserRepository userRepository;

    @PathParam("idProjeto")
    private Long idProjeto;

    @POST
    public Response saveOrUpdate(MeioAmbiente meioAmbiente, @Context SecurityContext context) {
        try {
            return Response.ok(meioAmbienteService.saveOrUpdate(meioAmbiente, context, idProjeto)).build();
        } catch (Exception ex) {
            Logger.getLogger(MeioAmbienteEndpoint.class.getName()).log(Level.SEVERE, null, ex);
            throw new RuntimeException(ex);
        }
    }

    @GET
    @Path("{id}")
    public MeioAmbiente getMeioAmbiente(@PathParam("id") Long id) {
        return meioAmbienteRepository.findBy(id);
    }

    @GET
    public Response findAllMeioAmbienteByIdProjeto() {
        List<MeioAmbiente> listaMeioAmbiente = meioAmbienteRepository.locateByProjetoId(idProjeto);
        return Response.ok(listaMeioAmbiente).build();
    }

    @GET
    @Path("findChange")
    public Response findChange() {
        Object meioAmbiente = null;
        try {
            meioAmbiente = meioAmbienteService.verifyChange(idProjeto);
        } catch (SystemException ex) {
            Logger.getLogger(MeioAmbienteEndpoint.class.getName()).log(Level.SEVERE, null, ex);
        }
        return Response.ok(meioAmbiente).build();
    }
}
