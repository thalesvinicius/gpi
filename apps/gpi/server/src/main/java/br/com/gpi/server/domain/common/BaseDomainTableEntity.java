package br.com.gpi.server.domain.common;

import javax.persistence.MappedSuperclass;

@MappedSuperclass
public abstract class BaseDomainTableEntity extends BaseEntity<Long> {

    /**
     * Method used to get the descricao from a DomainTableEntity
     *
     * @return
     */
    public abstract String getDescricao();

    public abstract void setDescricao(String descricao);

}
