package br.com.gpi.server.querybuilder;

public enum ConditionValueTypeEnum {

    STRING(1), OTHER(2);
    private final Integer id;

    ConditionValueTypeEnum(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }
}
