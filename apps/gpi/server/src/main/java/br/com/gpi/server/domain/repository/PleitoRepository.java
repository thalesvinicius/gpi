package br.com.gpi.server.domain.repository;

import br.com.gpi.server.domain.entity.Pleito;
import java.util.List;
import org.apache.deltaspike.data.api.EntityRepository;
import org.apache.deltaspike.data.api.Modifying;
import org.apache.deltaspike.data.api.Query;
import org.apache.deltaspike.data.api.Repository;
import org.apache.deltaspike.data.api.criteria.CriteriaSupport;

@Repository(forEntity = Pleito.class)
public abstract class PleitoRepository implements CriteriaSupport<Pleito>,EntityRepository<Pleito, Long> {

    @Modifying
    @Query(named = Pleito.Query.locateByProjetoId)
    public abstract List<Pleito> locateByProjetoId(Long id);
  
}
