package br.com.gpi.server.endpoint;

import br.com.gpi.server.domain.common.QueryParameter;
import br.com.gpi.server.domain.entity.Estagio;
import br.com.gpi.server.domain.entity.Parametro;
import br.com.gpi.server.domain.entity.Projeto;
import br.com.gpi.server.domain.entity.RelatorioAcompanhamento;
import br.com.gpi.server.domain.entity.SituacaoRelatorioEnum;
import br.com.gpi.server.domain.entity.ViewRelatorioAcompanhamentoAnual;
import br.com.gpi.server.domain.repository.ParametroRepository;
import br.com.gpi.server.domain.service.ProjetoService;
import br.com.gpi.server.domain.service.RelatorioAcompanhamentoAnualService;
import br.com.gpi.server.domain.service.reports.RelatorioAcompanhamentoAnualReportService;
import br.com.gpi.server.domain.user.model.SecurityUser;
import br.com.gpi.server.to.AcompanhamentoAnualTO;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.inject.Inject;
import javax.transaction.NotSupportedException;
import javax.transaction.SystemException;
import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

@Path("projeto/acompanhamentoAnual")
@Stateless
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
@TransactionManagement(TransactionManagementType.BEAN)
public class RelatorioAcompanhamentoAnualEndpoint {
    
    @Inject
    private ProjetoService projetoService;

    @Inject
    private ParametroRepository parametroRepository;

    @Inject
    private RelatorioAcompanhamentoAnualService relatorioAcompanhamentoService;
    
    @Inject
     private RelatorioAcompanhamentoAnualReportService relatorioAcompanhamentoAnualService;
    
    @GET
    @Path("gerar")
    public Response buildRelatorioAcompanhamentoAnual(
            @QueryParam("ano") String ano,
            @QueryParam("situacao") SituacaoRelatorioEnum situacao,
            @QueryParam("nomeEmpresa") String nomeEmpresa,
            @QueryParam("nomeProjeto") String nomeProjeto,
            @QueryParam("estagios") List<Estagio> estagios,
            @QueryParam("departamentos") List<Long> departamentos,
            @QueryParam("usuarioResponsavel") List<Long> usuariosResponsaveis,
            @QueryParam("cadeiasProdutivas") List<Long> cadeiasProdutivas) {
        
        QueryParameter filters = QueryParameter.with(RelatorioAcompanhamento.Filters.ANO, ano)
                .and(RelatorioAcompanhamento.Filters.SITUACAO_ACOMPANHAMENTO, situacao)
                .and(Projeto.Filters.NOME_EMPRESA, nomeEmpresa)
                .and(Projeto.Filters.NOME_PROJETO, nomeProjeto)
                .and(Projeto.Filters.ESTAGIO, estagios)
                .and(Projeto.Filters.CADEIA_PRODUTIVA, cadeiasProdutivas)
                .and(Projeto.Filters.DEPARTAMENTOS, departamentos)
                .and(Projeto.Filters.USUARIO_RESPONSAVEL_PROJETO, usuariosResponsaveis);
        
        List<ViewRelatorioAcompanhamentoAnual> relatorioAcompanhamentoAnualList = 
                projetoService.buildRelatorioAcompanhamentoAnual(filters);
        return Response.ok(relatorioAcompanhamentoAnualList).build();
    }
    
    @POST
    public Response saveOrUpdate(AcompanhamentoAnualTO acompanhamento, @Context SecurityContext context) {
        try {
            relatorioAcompanhamentoService.updateRelatorioAcompanhamentoAnual(acompanhamento, context);
        } catch (Exception ex) {
            Logger.getLogger(RelatorioAcompanhamentoAnualEndpoint.class.getName()).log(Level.SEVERE, null, ex);
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(ex).build();
        }
        return Response.ok().build();
    }
    
    @GET
    @Path("anosanteriores")
    public Response findAllAnosAnterioresByProjetoId(@QueryParam("familiaId")Long familiaId, @QueryParam("ano") Integer ano) {
        return Response.ok(relatorioAcompanhamentoService.findAllRelatorioAcompanhamentoByFamiliaIdAndAnoLessThan(familiaId, ano)).build();
    }
    
    @GET    
    public Response get(@QueryParam("familiaId") Long familiaId, @QueryParam("projetoId") Long projetoId) {
        AcompanhamentoAnualTO acompanhamentoAnual= new AcompanhamentoAnualTO();                
        try {
            acompanhamentoAnual = relatorioAcompanhamentoService.mountRelatorioAcompanhamentoTO(familiaId, projetoId);            
        } catch (Exception ex) {
            Logger.getLogger(RelatorioAcompanhamentoAnualEndpoint.class.getName()).log(Level.SEVERE, null, ex);
            return Response.status(Response.Status.NOT_FOUND).entity(ex).build();
        }                
        return Response.ok(acompanhamentoAnual).build();    
    }
    
    @GET
    @Path("validar")
    public Response validate(@QueryParam("relatorioId") Long relatorioId) {
        RelatorioAcompanhamento relatorio = null;
        try {
            relatorio = relatorioAcompanhamentoService.validaRelatorio(relatorioId);
        } catch (Exception ex) {
            Logger.getLogger(RelatorioAcompanhamentoAnualEndpoint.class.getName()).log(Level.SEVERE, null, ex);
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(ex).build();
        }        
        return Response.ok(relatorio).build();
    }

    @POST
    @Path("enviarPrimeiraCampanha")
    public Response sendFirstCampaign(Long[] dadosCampanha, @Context SecurityContext securityContext) {
        Parametro parametro = parametroRepository.locateByParametroName("DuracaoPrimeiraCampanhaEmDias");
        SecurityUser loggedUser = (SecurityUser) securityContext.getUserPrincipal();
        int duracaoPrimeiraCampanha = !parametro.getValor().equals("") ? Integer.parseInt(parametro.getValor()) : 0;
        String ano = "";
        for (Long campanha : dadosCampanha) {
            if (ano.equals("")) {
                ano = campanha.toString();
                continue;
            }
            try {
                relatorioAcompanhamentoService.sendFirstCampaign(ano, campanha, loggedUser.getId(), duracaoPrimeiraCampanha);
            } catch (SystemException | NotSupportedException ex) {
                Logger.getLogger(RelatorioAcompanhamentoAnualEndpoint.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return null;
    }    
    
    @POST
    @Path("enviarSegundaCampanha")
    public Response sendSecondCampaign(Long[] dadosCampanha, @Context SecurityContext securityContext) {
        Parametro parametro = parametroRepository.locateByParametroName("DuracaoSegundaCampanhaEmDias");
        SecurityUser loggedUser = (SecurityUser) securityContext.getUserPrincipal();
        int duracaoSegundaCampanha = !parametro.getValor().equals("") ? Integer.parseInt(parametro.getValor()) : 0;
        String ano = "";
        for (Long campanha : dadosCampanha) {
            if (ano.equals("")) {
                ano = campanha.toString();
                continue;
            }
            try {
                return Response.ok(relatorioAcompanhamentoService.sendSecondCampaign(ano, campanha, loggedUser.getId(), duracaoSegundaCampanha)).build();
            } catch (SystemException | NotSupportedException ex) {
                Logger.getLogger(RelatorioAcompanhamentoAnualEndpoint.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return null;
    }
    
    @POST
    @Path("print/pdf")
    @Consumes("application/json")
    @Produces("application/pdf")
    public Response acompanhamentoAnualPDF(@Valid AcompanhamentoAnualTO report){
        
        try {
            return Response.ok(
                relatorioAcompanhamentoAnualService.generateReport(report), 
                "application/pdf").build();
        }catch(Exception e){
            return Response.serverError().build();
        }   
    
    }    
}
