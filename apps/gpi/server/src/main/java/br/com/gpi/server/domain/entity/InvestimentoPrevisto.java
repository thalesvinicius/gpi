package br.com.gpi.server.domain.entity;

import br.com.gpi.server.core.jpa.converter.TipoInvestimentoEnumConverter;
import br.com.gpi.server.domain.common.BaseEntity;
import br.com.gpi.server.domain.entity.enumerated.EnumTipoInvestimento;
import com.fasterxml.jackson.annotation.JsonIgnore;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.envers.Audited;

@Audited
@Entity
@Table(name = "InvestimentoPrevisto")
public class InvestimentoPrevisto extends BaseEntity<Long> {

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "financeiro_id")
    private Financeiro financeiro;

    @Enumerated
    @Column(name = "tipo", insertable = false, updatable = false)
    private EnumTipoInvestimento tipoInvestimento;
    
    @JsonIgnore
    @Column(name = "tipo")
    private Integer tipoInvestimentoInt;
    
    private int ano;
    
    private Double valor;

    private Double valorRealizado;
    
    public Financeiro getFinanceiro() {
        return financeiro;
    }

    public void setFinanceiro(Financeiro financeiro) {
        this.financeiro = financeiro;
    }

    public EnumTipoInvestimento getTipoInvestimento() {
        return tipoInvestimento;
    }

    public void setTipoInvestimento(EnumTipoInvestimento tipoInvestimento) {
        if(tipoInvestimento == null) {
            return;
        }
        this.tipoInvestimento = tipoInvestimento;
        this.tipoInvestimentoInt = (new TipoInvestimentoEnumConverter()).convertToDatabaseColumn(tipoInvestimento);
    }

    public Integer getTipoInvestimentoInt() {
        return tipoInvestimentoInt;
    }

    public void setTipoInvestimentoInt(Integer tipoInvestimentoInt) {
        if(tipoInvestimentoInt == null){
            return;
        }
        this.tipoInvestimentoInt = tipoInvestimentoInt;
        this.tipoInvestimento = (new TipoInvestimentoEnumConverter()).convertToEntityAttribute(tipoInvestimentoInt);
    }
    
    
    

    public int getAno() {
        return ano;
    }

    public void setAno(int ano) {
        this.ano = ano;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }
    
    public Double getValorRealizado() {
        return valorRealizado;
}

    public void setValorRealizado(Double valorRealizado) {
        this.valorRealizado = valorRealizado;
    }
    
}
