package br.com.gpi.server.domain.user.model;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author <a href="mailto:pviniciusfm@gmail.com">Paulo Vinicius F. Machado</a>
 * @version 1.0
 */
@XmlRootElement
public class LostPasswordRequest {

    @NotNull
    private String emailAddress;

    public LostPasswordRequest() {
    }

    public LostPasswordRequest(final String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }
}
