/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.gpi.server.domain.service.reports;

import br.com.gpi.server.domain.entity.DescricaoFaseEnum;
import br.com.gpi.server.domain.entity.InstrumentoFormalizacao;
import br.com.gpi.server.domain.entity.Localizacao;
import br.com.gpi.server.domain.entity.Municipio;
import br.com.gpi.server.domain.entity.Projeto;
import br.com.gpi.server.domain.entity.RelatorioAcompanhamento;
import br.com.gpi.server.domain.entity.TipoInstrFormalizacao;
import br.com.gpi.server.domain.entity.UnidadeEmpresa;
import br.com.gpi.server.domain.service.InstrumentoFormalizacaoService;
import br.com.gpi.server.domain.service.ProjetoService;
import br.com.gpi.server.to.AcompanhamentoAnualTO;
import br.com.gpi.server.to.CalendarioAcompanhamentoAnualTO;
import br.com.gpi.server.util.reports.ReportUtil;
import java.time.Instant;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperPrint;

/**
 *
 * @author rafaelbfs
 */
public class RelatorioAcompanhamentoAnualReportService extends BaseReportService<AcompanhamentoAnualTO>{

    @Inject
    public RelatorioAcompanhamentoAnualReportService(EntityManager em) {
        super(em);
    }
    @Inject
    private ProjetoService projetoService;
    @Inject
    private InstrumentoFormalizacaoService instrumentoService;
    
    private List<DataEstagioRepTO> criarCalendarioExibicao(AcompanhamentoAnualTO rel){
        Map<DescricaoFaseEnum, List<CalendarioAcompanhamentoAnualTO>> c = rel.getCalendarios().stream().collect(Collectors.groupingBy(CalendarioAcompanhamentoAnualTO::getDescricaoFaseEnum));
        final List<DescricaoFaseEnum> estagios = Arrays.asList(DescricaoFaseEnum.INICIO_IMPLANTACAO,DescricaoFaseEnum.INICIO_OPERACAO,DescricaoFaseEnum.INICIO_TRATAMENTO_TRIBUTARIO);
        return estagios.stream().map((f) -> {
            return c.containsKey(f) && !c.get(f).isEmpty() ? new DataEstagioRepTO(f.getId().shortValue(), 
                    f.getDescricao(),c.get(f).get(0).getDataPrevisto() , c.get(f).get(0).getDataRealizado()):
                    new DataEstagioRepTO(f.getId().shortValue(),f.getDescricao() );
        }).collect(Collectors.toList());
    }
    
    private String protocoloMaisRecente(Set<InstrumentoFormalizacao> instrumentos){
        List<InstrumentoFormalizacao> protocolosInencoes = instrumentos.stream().collect(Collectors.groupingBy(InstrumentoFormalizacao::getTipo)).getOrDefault(TipoInstrFormalizacao.PROTOCOLO_INTENCAO,Collections.emptyList());
        return protocolosInencoes.stream().sorted((p1,p2)-> Optional.ofNullable(p1.getDataAssinaturaPrevista()).orElse(Date.from(Instant.MIN))
                .compareTo(Optional.ofNullable(p2.getDataAssinaturaPrevista()).orElse(Date.from(Instant.MIN))))
                .findFirst().map(InstrumentoFormalizacao::getProtocolo)
                .map((np) -> np == null ? null : (np.length() <= 4 ? np : 
                        np.substring(0,np.length()-4) + "/" + np.substring(np.length()-4)))
                .orElse(" NÃO ENCONTRADO");
    }
    
    public byte[] generateReport(AcompanhamentoAnualTO acompanhamento){
        try {
            Projeto projeto = projetoService.findById(acompanhamento.getProjeto().getId());
            if(projeto.getVersaoPai() != projeto.getId()){
                projeto = projetoService.findProjetoVersaoCongeladaByFamiliaId(projeto.getVersaoPai());
            }
            Map<String,Object> parametros = new HashMap<>();
            parametros.put(ReportUtil.PARAM_DT_DECISAO_FORMALIZADA,acompanhamento.getCalendarios()
                    .stream().collect(Collectors.groupingBy(CalendarioAcompanhamentoAnualTO::getDescricaoFaseEnum)).getOrDefault(DescricaoFaseEnum.DECISAO_FORMALIZADA,
                    Collections.singletonList(new CalendarioAcompanhamentoAnualTO())).get(0).getDataRealizado());
            parametros.put("calendarioAcompanhamento", criarCalendarioExibicao(acompanhamento));
            
            parametros.put("numeroProtocoloIntencoes",protocoloMaisRecente(projeto.getInstrumentos()));
            parametros.put("descricaoProjeto", projeto.getDescricao() == null ? 
                    projeto.getNome():projeto.getDescricao());
            parametros.put("localizacaoProjeto",Optional.ofNullable(projeto.getLocalizacao())
                    .map(Localizacao::getMunicipio).map(Municipio::getNome).orElse("Não definida"));
            
            //utilizar endereços mais completos do banco de dados
           
            acompanhamento.getProjeto().setEnderecoEmpresa(projeto.getEmpresa().getEndereco());
            acompanhamento.getProjeto().setEnderecoUnidade(Optional.ofNullable(projeto.getUnidade())
                    .map(UnidadeEmpresa::getEndereco).orElse(projeto.getEmpresa().getEndereco()));
            Optional<RelatorioAcompanhamento> relAcomp = projeto.getRelatoriosAcompanhamento().stream()
                    .collect(Collectors.groupingBy(RelatorioAcompanhamento::getId))
                    .getOrDefault(acompanhamento.getRelatorioAcompanhamento().getId(),Collections.emptyList())
                    .stream().findFirst();
            relAcomp.ifPresent((v) -> acompanhamento.setRelatorioAcompanhamento(v));
            acompanhamento.getRelatorioAcompanhamento().setProjeto(projeto);
            
            
            parametros.put("periodo", "Período de 1º de Janeiro a 31 de Dezembro de " + 
                    Optional.ofNullable(acompanhamento.getRelatorioAcompanhamento())
                            .map(RelatorioAcompanhamento::getAno).map((a)-> a < 1000 ? 9999:a).orElse(9999).toString());
            
            
            JasperPrint jp = generateReport(()-> parametros,
                    Collections.singleton(acompanhamento) ,
                    resourceAsStream(ReportUtil.RELATORIO_ACOMPANHAMENTO_ANUAL));
            return JasperExportManager.exportReportToPdf(jp); 
            
        } catch (Exception ex) {
            Logger.getLogger(RelatorioAcompanhamentoAnualReportService.class.getName()).log(Level.SEVERE, null, ex);
            throw new RuntimeException("Falha ao gerar relatório", ex);
        }
        
    }
    
    public static class DataEstagioRepTO {
        private Date dataPrevista,dataRealizada;
        private short indice;
        private String descricao;

        public DataEstagioRepTO(short indice, String descricao, Date dataPrevista, Date dataRealizada) {
            this.dataPrevista = dataPrevista;
            this.dataRealizada = dataRealizada;
            this.indice = indice;
            this.descricao = descricao;
        }
        
        public DataEstagioRepTO(short indice, String descricao) {
            this.indice = indice;
            this.descricao = descricao;
        }

        public Date getDataPrevista() {
            return dataPrevista;
        }

        public void setDataPrevista(Date dataPrevista) {
            this.dataPrevista = dataPrevista;
        }

        public Date getDataRealizada() {
            return dataRealizada;
        }

        public void setDataRealizada(Date dataRealizada) {
            this.dataRealizada = dataRealizada;
        }

        public short getIndice() {
            return indice;
        }

        public void setIndice(short indice) {
            this.indice = indice;
        }

        public String getDescricao() {
            return descricao;
        }

        public void setDescricao(String descricao) {
            this.descricao = descricao;
        }
        
        
        
        
        
    }
    
}
