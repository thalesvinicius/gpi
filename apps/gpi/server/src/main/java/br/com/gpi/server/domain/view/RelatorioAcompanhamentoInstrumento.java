package br.com.gpi.server.domain.view;

import br.com.gpi.server.domain.common.BaseEntity;
import br.com.gpi.server.domain.entity.TipoInstrFormalizacao;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;

@Entity
@Table(name = "VIEW_RELATORIO_ACOMPANHAMENTO_INSTRUMENTO")
public class RelatorioAcompanhamentoInstrumento extends BaseEntity<Long> implements Serializable {
    
    @Id 
    private String empresa;
    @Id
    private String titulo;
    
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date previsaoAssinatura;
    @Id
    private TipoInstrFormalizacao tipo;
    private String localAtividade;
    private Integer localAtividadeId;
    @Id
    private String atividade;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date dataInicioAtividade;
    private String analista;
    private String gerencia;
    @Id
    private Long analistaId;
    private Long gerenciaId;    

    public RelatorioAcompanhamentoInstrumento() {
    }

    /**
     * @return the empresa
     */
    public String getEmpresa() {
        return empresa;
    }

    /**
     * @param empresa the empresa to set
     */
    public void setEmpresa(String empresa) {
        this.empresa = empresa;
    }

    /**
     * @return the titulo
     */
    public String getTitulo() {
        return titulo;
    }

    /**
     * @param titulo the titulo to set
     */
    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    /**
     * @return the previsaoAssinatura
     */
    public Date getPrevisaoAssinatura() {
        return previsaoAssinatura;
    }

    /**
     * @param previsaoAssinatura the previsaoAssinatura to set
     */
    public void setPrevisaoAssinatura(Date previsaoAssinatura) {
        this.previsaoAssinatura = previsaoAssinatura;
    }

    /**
     * @return the tipo
     */
    public String getTipo() {
        return tipo == null ? null : tipo.getDescricao();
    }

    /**
     * @param tipo the tipo to set
     */
    public void setTipo(TipoInstrFormalizacao tipo) {
        this.tipo = tipo;
    }

    /**
     * @return the localAtividade
     */
    public String getLocalAtividade() {
        return localAtividade;
    }

    /**
     * @param localAtividade the localAtividade to set
     */
    public void setLocalAtividade(String localAtividade) {
        this.localAtividade = localAtividade;
    }

    /**
     * @return the atividade
     */
    public String getAtividade() {
        return atividade;
    }

    /**
     * @param atividade the atividade to set
     */
    public void setAtividade(String atividade) {
        this.atividade = atividade;
    }

    /**
     * @return the dataInicioAtividade
     */
    public Date getDataInicioAtividade() {
        return dataInicioAtividade;
    }

    /**
     * @param dataInicioAtividade the dataInicioAtividade to set
     */
    public void setDataInicioAtividade(Date dataInicioAtividade) {
        this.dataInicioAtividade = dataInicioAtividade;
    }

    /**
     * @return the analista
     */
    public String getAnalista() {
        return analista;
    }

    /**
     * @param analista the analista to set
     */
    public void setAnalista(String analista) {
        this.analista = analista;
    }

    /**
     * @return the gerencia
     */
    public String getGerencia() {
        return gerencia;
    }

    /**
     * @param gerencia the gerencia to set
     */
    public void setGerencia(String gerencia) {
        this.gerencia = gerencia;
    }

    /**
     * @return the analistaId
     */
    public Long getAnalistaId() {
        return analistaId;
    }

    /**
     * @param analistaId the analistaId to set
     */
    public void setAnalistaId(Long analistaId) {
        this.analistaId = analistaId;
    }

    /**
     * @return the gerenciaId
     */
    public Long getGerenciaId() {
        return gerenciaId;
    }

    /**
     * @param gerenciaId the gerenciaId to set
     */
    public void setGerenciaId(Long gerenciaId) {
        this.gerenciaId = gerenciaId;
    }

    /**
     * @return the localAtividadeId
     */
    public Integer getLocalAtividadeId() {
        return localAtividadeId;
    }

    /**
     * @param localAtividadeId the localAtividadeId to set
     */
    public void setLocalAtividadeId(Integer localAtividadeId) {
        this.localAtividadeId = localAtividadeId;
    }

    public static class Filtro {
        public static final String GERENCIA = "gerencia";
        public static final String ANALISTA = "analista";
        public static final String TIPOS = "tipos";
        public static final String LOCAIS = "locais";
        public static final String PREVISAO_ASSINATURA_DE = "previsaoAssinaturaDe";
        public static final String PREVISAO_ASSINATURA_ATE = "previsaoAssinaturaAte";
    }
}