package br.com.gpi.server.endpoint;

import br.com.gpi.server.domain.common.BaseService;
import br.com.gpi.server.domain.entity.Anexo;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.security.PermitAll;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import org.apache.commons.io.FileUtils;
import org.jboss.resteasy.plugins.providers.multipart.InputPart;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;

@Path("/anexo")
@Stateless
@PermitAll
@Consumes(MediaType.APPLICATION_JSON  + ";charset=UTF-8")
@Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
@TransactionManagement(TransactionManagementType.BEAN)
public class AnexoEndpoint {

    private static final String SERVER_UPLOAD_LOCATION_FOLDER = "/opt/gpi_anexos/";
    private static final AtomicInteger atomicInteger = new AtomicInteger(1);

    @Inject
    private Logger logger;
    
    @Inject
    private BaseService<Anexo> anexoRepository;

    @GET
    @Produces(MediaType.TEXT_PLAIN + ";charset=UTF-8")
    public Response downloadFile(@QueryParam("flowFilename") String flowFilename,
            @QueryParam("flowChunkNumber") String flowChunkNumber,
            @QueryParam("flowRelativePath") String flowRelativePath,
            @QueryParam("flowTotalChunks") String flowTotalChunks) {

        String response = String.format("%s / %s / %s / %s", flowFilename, flowChunkNumber, flowRelativePath, flowTotalChunks);
        logger.info(response);
        return Response.status(Status.NOT_FOUND).build();

    }

    @GET
    @Path("{fileName}")
    @Produces(MediaType.APPLICATION_OCTET_STREAM + ";charset=UTF-8")
    public Response downloadFile(@QueryParam("id") String id, @PathParam("fileName") String fileName) {

        FileInputStream is;
        try {
            is = new FileInputStream(new File(SERVER_UPLOAD_LOCATION_FOLDER, id));
        } catch (FileNotFoundException ex) {
            Logger.getLogger(AnexoEndpoint.class.getName()).log(Level.SEVERE, null, ex);
            return Response.status(Response.Status.NOT_FOUND).build();
        }
        return Response.ok().entity(is).build();

    }

    @POST    
    @Consumes(MediaType.MULTIPART_FORM_DATA+ ";charset=UTF-8")
    public Response uploadFile(MultipartFormDataInput input) throws IOException {

        String fileName = "";
        String fileIdentifier = "";

        Map<String, List<InputPart>> formParts = input.getFormDataMap();

        List<InputPart> inPart = formParts.get("file");
        List<InputPart> auxFilename = formParts.get("flowFilename");
        
        auxFilename.get(0).setMediaType(new MediaType("text","plain", "utf-8"));
        fileName = auxFilename.get(0).getBody(String.class, null);
        
        fileName = new String(fileName.getBytes(Charset.forName("us-ascii")),Charset.forName("UTF-8"));

        for (InputPart inputPart : inPart) {

            try {

                // Retrieve headers, read the Content-Disposition header to obtain the original name of the file
                MultivaluedMap<String, String> headers = inputPart.getHeaders();
//                fileName = parseFileName(headers);

                // Handle the body of that part with an InputStream
                InputStream istream = inputPart.getBody(InputStream.class, null);

                fileIdentifier = saveFile(istream);

            } catch (IOException e) {
                throw new WebApplicationException(e);
            }

        }
        Anexo anexo = new Anexo(fileName, fileIdentifier, parseExtension(fileName));
        anexoRepository.getEm().persist(anexo);

        logger.log(Level.INFO, "File saved to server location : {0}", fileName);

        return Response.ok(anexo).build();
    }

    // Parse Content-Disposition header to get the original file name
    private String parseFileName(MultivaluedMap<String, String> headers) {

        String[] contentDispositionHeader = headers.getFirst("Content-Disposition").split(";");

        for (String name : contentDispositionHeader) {

            if ((name.trim().startsWith("filename"))) {

                String[] tmp = name.split("=");

                String fileName = tmp[1].trim().replaceAll("\"", "");

                return fileName;
            }
        }
        return "anexo";
    }

    /**
     * Save file in temporary directory to persist generated data
     *
     * @param uploadedInputStream
     * @return upload identifier
     */
    private String saveFile(InputStream uploadedInputStream) {

        try {
            File file = File.createTempFile("anexo", null, new File(SERVER_UPLOAD_LOCATION_FOLDER));
            FileUtils.copyInputStreamToFile(uploadedInputStream, file);
            String generatedIdentifier = file.getName();
            return generatedIdentifier;
        } catch (IOException e) {
            throw new WebApplicationException(e);
        }
    }

    private String parseExtension(String fullFileName) {
        if (fullFileName == null || fullFileName.lastIndexOf('.') == -1) {
            return "";
        } else {
            return fullFileName.substring(fullFileName.lastIndexOf('.') + 1);
        }
    }
}
