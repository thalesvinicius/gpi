/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.gpi.server.util;

import java.awt.Color;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import javax.ws.rs.core.Response;
import net.sf.dynamicreports.jasper.builder.JasperReportBuilder;
import static net.sf.dynamicreports.report.builder.DynamicReports.cmp;
import static net.sf.dynamicreports.report.builder.DynamicReports.report;
import static net.sf.dynamicreports.report.builder.DynamicReports.stl;
import net.sf.dynamicreports.report.builder.column.ColumnBuilder;
import net.sf.dynamicreports.report.builder.style.StyleBuilder;
import net.sf.dynamicreports.report.constant.HorizontalAlignment;
import net.sf.dynamicreports.report.constant.PageOrientation;
import net.sf.dynamicreports.report.constant.PageType;
import net.sf.dynamicreports.report.exception.DRException;
import net.sf.jasperreports.engine.DefaultJasperReportsContext;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.Renderable;
import net.sf.jasperreports.engine.RenderableUtil;
import net.sf.jasperreports.engine.type.OnErrorTypeEnum;

/**
 *
 * @author marcoabc
 */
public class GenerateReportUtil {
    
    /*
     *   Cria o relatório
     */
    public static Response build(String titulo, Formato formato, ColumnBuilder[] columns, JRDataSource dataSource, String nomeArquivo) throws FileNotFoundException, IOException {
        StyleBuilder boldStyle = stl.style().bold();
        StyleBuilder boldCenteredStyle = stl.style(boldStyle).setHorizontalAlignment(HorizontalAlignment.CENTER);

        StyleBuilder columnTitleStyle = stl.style(boldCenteredStyle)
                .setBorder(stl.pen1Point())
                .setBackgroundColor(Color.LIGHT_GRAY);
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        try {
            JasperReportBuilder report = report()//create new report design
                    .setPageFormat(PageType.A4, PageOrientation.LANDSCAPE)
                    .setColumnTitleStyle(columnTitleStyle)
                    .highlightDetailEvenRows()
                    .columns(columns)
                    .title(cmp.text(titulo).setStyle(boldCenteredStyle))//shows report title
                    .pageFooter(cmp.pageXofY().setStyle(boldCenteredStyle))//shows number of page at page footer
                    .setDataSource(dataSource); //set datasource      
            Response.ResponseBuilder resultBuilder = Response.ok()
                    .header("Content-Disposition", "attachment; filename='" + nomeArquivo + formato.getExtensao() + "'");

            if (formato.equals(Formato.PDF)) {
                resultBuilder.type("application/pdf");
                report.toPdf(byteArrayOutputStream);
            } else if (formato.equals(Formato.XLS)) {
                resultBuilder.type("application/vnd.ms-excel");
                report.toXls(byteArrayOutputStream);
            }

            resultBuilder.entity(byteArrayOutputStream.toByteArray());
            return resultBuilder.build();
        } catch (DRException e) {
            return Response.serverError().entity(e).build();
        }
    }
    
    public static  Renderable getImage(String resourceLocation){
        try {
            return RenderableUtil.getInstance(
                    DefaultJasperReportsContext.getInstance())
                    .getRenderable(resourceAsStream(resourceLocation), OnErrorTypeEnum.ERROR);
        } catch (JRException ex) {
            throw new IllegalStateException("Falha no carregamento da imagem",ex);
        }
    }
    
     public static InputStream resourceAsStream(String name) {
        InputStream inputStream = GenerateReportUtil.class.getResourceAsStream(name);
        if (inputStream != null) {
            return inputStream;
        } else {
            throw new IllegalArgumentException("Caminho não encontrado: " + name);
        }
    }
}
