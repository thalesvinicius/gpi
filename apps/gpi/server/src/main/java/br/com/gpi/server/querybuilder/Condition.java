package br.com.gpi.server.querybuilder;

import java.util.List;

public abstract class Condition {

    private String conditionKey;
    private List<Field> fields;
    private ConditionValueTypeEnum valueType;

    public Condition() {
    }

    public Condition(String conditionKey, List<Field> fields, ConditionValueTypeEnum type) {
        this.conditionKey = conditionKey;
        this.valueType = type;
    }

    public String getConditionKey() {
        return conditionKey;
    }

    public void setConditionKey(String conditionKey) {
        this.conditionKey = conditionKey;
    }
    
    public Field getField() {
        return fields.get(0);
    }

    public List<Field> getFields() {
        return fields;
    }

    public void setFields(List<Field> fields) {
        this.fields = fields;
    }
    
    /**
     * Used to determine if will be neccessary to use '' or not around value
 Returns the valueType of the value that will be used on query
     *
     * @return
     */
    public ConditionValueTypeEnum getValueType() {
        return valueType;
    }

    public void setValueType(ConditionValueTypeEnum valueType) {
        this.valueType = valueType;
    }

    public abstract String getSQL();

}
