package br.com.gpi.server.to;

import br.com.gpi.server.domain.entity.enumerated.EnumTipoInvestimento;


public class InvestimentoIndustrialAgricolaCapacitacaoEPropriosDTO {

    private int ano;
    private Long idIndustrial;
    private Double valorIndustrial;
    private Long idAgricola;
    private Double valorAgricola;
    private Long idCapacitacao;
    private Double valorCapacitacao;
    private Long idProprios;
    private Double valorProprios;

    
    public InvestimentoIndustrialAgricolaCapacitacaoEPropriosDTO() {
        
    }
    
    public InvestimentoIndustrialAgricolaCapacitacaoEPropriosDTO(int ano, Double valorIndustrial, 
            Double valorAgricola, Double valorCapacitacao, Double valorProprios) {
        this.ano = ano;
        this.valorIndustrial = valorIndustrial;
        this.valorAgricola = valorAgricola;
        this.valorCapacitacao = valorCapacitacao;        
        this.valorProprios = valorProprios;
    }

    public int getAno() {
        return ano;
    }

    public void setAno(int ano) {
        this.ano = ano;
    }

    public Double getValorIndustrial() {
        return valorIndustrial;
    }

    public void setValorIndustrial(Double valorIndustrial) {
        this.valorIndustrial = valorIndustrial;
    }

    public Double getValorAgricola() {
        return valorAgricola;
    }

    public void setValorAgricola(Double valorAgricola) {
        this.valorAgricola = valorAgricola;
    }

    public Double getValorCapacitacao() {
        return valorCapacitacao;
    }

    public void setValorCapacitacao(Double valorCapacitacao) {
        this.valorCapacitacao = valorCapacitacao;
    }        

    public Double getValorProprios() {
        return valorProprios;
    }

    public void setValorProprios(Double valorProprios) {
        this.valorProprios = valorProprios;
    }
    
    public EnumTipoInvestimento getTipoIndustrial() {
        return EnumTipoInvestimento.INVESTIMENTO_INDUSTRIAL;
    }

    public EnumTipoInvestimento getTipoAgricola() {
        return EnumTipoInvestimento.INVESTIMENTO_AGRICOLA;
    }
    
    public EnumTipoInvestimento getTipoCapacitacao() {
        return EnumTipoInvestimento.INVESTIMENTO_CAPACITACAO_PROFISSIONAL;
    }

    public EnumTipoInvestimento getTipoProprios() {
        return EnumTipoInvestimento.INVESTIMENTO_PROPRIOS;
    }

    public Long getIdAgricola() {
        return idAgricola;
    }

    public void setIdAgricola(Long idAgricola) {
        this.idAgricola = idAgricola;
    }

    public Long getIdCapacitacao() {
        return idCapacitacao;
    }

    public void setIdCapacitacao(Long idCapacitacao) {
        this.idCapacitacao = idCapacitacao;
    }

    public Long getIdProprios() {
        return idProprios;
    }

    public void setIdProprios(Long idProprios) {
        this.idProprios = idProprios;
    }

    public Long getIdIndustrial() {
        return idIndustrial;
    }

    public void setIdIndustrial(Long idIndustrial) {
        this.idIndustrial = idIndustrial;
    }
}
