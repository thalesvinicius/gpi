package br.com.gpi.server.endpoint;

import br.com.gpi.server.domain.entity.FaturamentoPrevisto;
import br.com.gpi.server.domain.entity.Financeiro;
import br.com.gpi.server.domain.entity.enumerated.EnumTipoFaturamento;
import br.com.gpi.server.domain.repository.FinanceiroRepository;
import br.com.gpi.server.domain.repository.ProjetoRepository;
import br.com.gpi.server.domain.repository.UserRepository;
import br.com.gpi.server.domain.service.FinanceiroService;
import br.com.gpi.server.to.FaturamentoDoGrupoERealizadoEmMinasGeraisDTO;
import br.com.gpi.server.to.FaturamentoIndustrializadosEAdquiridosParaComercializacaoDTO;
import br.com.gpi.server.to.InvestimentoIndustrialAgricolaCapacitacaoEPropriosDTO;
import br.com.gpi.server.to.InvestimentoMaquinasEquipAndCapitalGiroEOutrosDTO;
import java.util.Collection;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.inject.Inject;
import javax.transaction.SystemException;
import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

@Path("/projeto/{idProjeto}/financeiro")
@Stateless
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
@TransactionManagement(TransactionManagementType.BEAN)
public class FinanceiroEndpoint {

    @Inject
    private FinanceiroService financeiroService;
    @Inject
    private FinanceiroRepository financeiroRepository;
    @Inject
    private ProjetoRepository projetoRepository;
    @Inject
    private UserRepository userRepository;

    @PathParam("idProjeto")
    private Long idProjeto;

    @POST
    public Response saveOrUpdate(@Valid Financeiro financeiro, @Context SecurityContext context) {
        try {
            return Response.ok(financeiroService.saveOrUpdate(financeiro, context, idProjeto)).build();
        } catch (Exception ex) {
            Logger.getLogger(FinanceiroEndpoint.class.getName()).log(Level.SEVERE, null, ex);
            throw new RuntimeException(ex);
        }
    }

    @GET
    @Path("{id}")
    public Financeiro getFinanceiro(@PathParam("id") Long id) {
        return financeiroRepository.findBy(id);
    }

    @GET
    public Response findAllFinanceirosByIdProjeto() {
        List<Financeiro> financeiros = financeiroRepository.locateByProjetoId(idProjeto);
        return Response.ok(financeiros).build();
    }

    @GET
    @Path("{id}/investimentoPrevisto1")
    public Response findAllInvestimentoMaquinasEquipAndCapitalGiroEOutrosByFinanceiroId(@PathParam("id") Long id) {
        Collection<InvestimentoMaquinasEquipAndCapitalGiroEOutrosDTO> investimentoList = financeiroService.findInvestimentoMaquinasEquipAndCapitalGiroEOutrosByFinanceiroId(id);
        return Response.ok(investimentoList).build();
    }

    @GET
    @Path("{id}/investimentoPrevisto2")
    public Response findAllInvestimentoIndustrialAgricolaCapacitacaoEPropriosByFinanceiroId(@PathParam("id") Long id) {
        Collection<InvestimentoIndustrialAgricolaCapacitacaoEPropriosDTO> investimentoList = financeiroService.findIndustrialAgricolaCapacitacaoEPropriosByFinanceiroId(id);
        return Response.ok(investimentoList).build();
    }

    @GET
    @Path("{id}/faturamentoAnterior")
    public Response findFaturamentoDoGrupoERealizadoEmMinasGeraisByFinanceiroId(@PathParam("id") Long id) {
        Collection<FaturamentoDoGrupoERealizadoEmMinasGeraisDTO> faturamentoList = financeiroService.findFaturamentoDoGrupoERealizadoEmMinasGeraisByFinanceiroId(id);
        return Response.ok(faturamentoList).build();
    }

    @GET
    @Path("{id}/faturamentoPrevisto1")
    public Response findFaturamentoIndustrializadosEAdquiridosParaComercializacaoByFinanceiroId(@PathParam("id") Long id) {
        Collection<FaturamentoIndustrializadosEAdquiridosParaComercializacaoDTO> faturamentoList = financeiroService.findFaturamentoIndustrializadosEAdquiridosParaComercializacaoByFinanceiroId(id);
        return Response.ok(faturamentoList).build();
    }

    @GET
    @Path("{id}/faturamentoPrevisto2")
    public Response findFaturamentoByFinanceiroId(@PathParam("id") Long id) {
        Collection<FaturamentoPrevisto> faturamentoList = financeiroService.findFaturamentoPrevistoByTipoFaturamentoAndFinanceiroId(EnumTipoFaturamento.FATURAMENTO_PREVISTO, id);
        return Response.ok(faturamentoList).build();
    }

    @GET
    @Path("findChange")
    public Response findChange() {
        Object financeiro = null;
        try {
            financeiro = financeiroService.verifyChange(idProjeto);
        } catch (SystemException ex) {
            Logger.getLogger(FinanceiroEndpoint.class.getName()).log(Level.SEVERE, null, ex);
        }
        return Response.ok(financeiro).build();
    }
}