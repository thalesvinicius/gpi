package br.com.gpi.server.domain.service;

import br.com.gpi.server.domain.common.BaseService;
import br.com.gpi.server.domain.common.QueryParameter;
import br.com.gpi.server.domain.entity.AccessToken;
import br.com.gpi.server.domain.entity.TipoUsuario;
import br.com.gpi.server.domain.entity.UsuarioExterno;
import br.com.gpi.server.domain.user.exception.TokenHasExpiredException;
import br.com.gpi.server.domain.user.exception.TokenNotFoundException;
import java.time.LocalDateTime;
import javax.inject.Inject;
import javax.persistence.EntityManager;

/**
 * @author <a href="mailto:pviniciusfm@gmail.com">Paulo Vinicius F. Machado</a>
 */
public class SessionService extends BaseService<AccessToken> {

    @Inject
    public SessionService(EntityManager em) {
        super(AccessToken.class, em);
    }

    public AccessToken recoverSession(String token) throws TokenHasExpiredException, TokenNotFoundException {
        AccessToken accessToken = locateByNamedQuery(AccessToken.Query.locateByToken, QueryParameter.with("token", token).parameters());
        if (accessToken == null) {
            throw new TokenNotFoundException();
        } else if(accessToken.getUser().getTipo().equals(TipoUsuario.EXTERNO.getId())){
            accessToken.setUser(this.getEm().find(UsuarioExterno.class, accessToken.getUser().getId()));
        }
        
        validateTokenExpirationDate(accessToken);
        return accessToken;
    }

    private void validateTokenExpirationDate(AccessToken accessToken) throws TokenHasExpiredException {
        LocalDateTime expirationDateTime = accessToken.getLastUpdated().plusMinutes(3600);
        if (LocalDateTime.now().isAfter(expirationDateTime)) {
            throw new TokenHasExpiredException();
        }
    }
}
