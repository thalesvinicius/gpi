package br.com.gpi.server.domain.service;

import br.com.gpi.server.domain.common.BaseService;
import br.com.gpi.server.domain.common.QueryParameter;
import br.com.gpi.server.domain.entity.Empresa;
import br.com.gpi.server.domain.entity.Empresa_;
import br.com.gpi.server.domain.entity.Estagio;
import br.com.gpi.server.domain.entity.HistoricoSituacaoInstrumentoFormalizacao;
import br.com.gpi.server.domain.entity.HistoricoSituacaoInstrumentoFormalizacao_;
import br.com.gpi.server.domain.entity.InstrumentoFormalizacao;
import br.com.gpi.server.domain.entity.InstrumentoFormalizacao_;
import br.com.gpi.server.domain.entity.NomeEmpresa;
import br.com.gpi.server.domain.entity.NomeEmpresa_;
import br.com.gpi.server.domain.entity.Projeto;
import br.com.gpi.server.domain.entity.Projeto_;
import br.com.gpi.server.domain.entity.Signatario;
import br.com.gpi.server.domain.entity.SituacaoInstrFormalizacao;
import br.com.gpi.server.domain.entity.SituacaoInstrumentoFormalizacao;
import br.com.gpi.server.domain.entity.SituacaoInstrumentoFormalizacao_;
import br.com.gpi.server.domain.entity.SituacaoProjetoEnum;
import br.com.gpi.server.domain.entity.TipoInstrFormalizacao;
import br.com.gpi.server.domain.entity.UnidadeEmpresa;
import br.com.gpi.server.domain.entity.UnidadeEmpresa_;
import br.com.gpi.server.domain.entity.User;
import br.com.gpi.server.domain.repository.HistoricoSituacaoInstrumentoFormalizacaoRepository;
import br.com.gpi.server.domain.repository.InstrumentoFormalizacaoRepository;
import br.com.gpi.server.domain.repository.ProjetoRepository;
import br.com.gpi.server.domain.repository.UserRepository;
import br.com.gpi.server.to.EmpresaUnidadeDTO;
import br.com.gpi.server.to.InstrumentoFormalizacaoTO;
import br.com.gpi.server.to.ProjetoTO;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Tuple;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;

public class InstrumentoFormalizacaoService extends BaseService<InstrumentoFormalizacao> {

    @Inject
    private InstrumentoFormalizacaoRepository formalizacaoRepository;
    @Inject
    private ProjetoRepository projetoRepository;
    @Inject
    private EmpresaService empresaService;    
    @Inject
    private UserRepository userRepository;
    @Inject
    private PesquisaService pesquisaService;
    @Inject
    HistoricoSituacaoInstrumentoFormalizacaoRepository historicoInstrumentoFormalizacaoRepository;

    @Inject
    public InstrumentoFormalizacaoService(EntityManager em) {
        super(InstrumentoFormalizacao.class, em);
    }

    public List<InstrumentoFormalizacaoTO> findVersoesAnteriores(InstrumentoFormalizacaoTO instrFormalizacao) {
        CriteriaBuilder builder = getEm().getCriteriaBuilder();
        CriteriaQuery<InstrumentoFormalizacaoTO> query = builder.createQuery(InstrumentoFormalizacaoTO.class);
        Root<InstrumentoFormalizacao> fromInstr = query.from(InstrumentoFormalizacao.class);
        List<Predicate> conditions = new ArrayList();

        conditions.add(builder.equal(fromInstr.get(InstrumentoFormalizacao_.protocolo), instrFormalizacao.getProtocolo()));
        conditions.add(builder.notEqual(fromInstr.get(InstrumentoFormalizacao_.id), instrFormalizacao.getId()));

        TypedQuery<InstrumentoFormalizacaoTO> typedQuery = getEm().createQuery(query
                .multiselect(fromInstr.get(InstrumentoFormalizacao_.id),
                        fromInstr.get(InstrumentoFormalizacao_.protocolo),
                        fromInstr.get(InstrumentoFormalizacao_.tipo),
                        fromInstr.get(InstrumentoFormalizacao_.situacaoAtual),
                        fromInstr.get(InstrumentoFormalizacao_.dataAssinatura),
                        fromInstr.get(InstrumentoFormalizacao_.versao),
                        fromInstr.get(InstrumentoFormalizacao_.localDocumento))
                .where(conditions.toArray(new Predicate[]{}))
                .orderBy(builder.desc(fromInstr.get(InstrumentoFormalizacao_.versao))));

        List<InstrumentoFormalizacaoTO> resultList = typedQuery.getResultList();

        return resultList;
    }

    public Collection<InstrumentoFormalizacaoTO> findByFilters(TipoInstrFormalizacao tipoInstrFormalizacao,
            Integer situacaoInstrumentoFormalizacaoId,
            String nomeEmpresa,
            String protocolo,
            String tituloInstrumento,
            String nomeProjeto) {

        CriteriaBuilder builder = getEm().getCriteriaBuilder();
        CriteriaQuery<InstrumentoFormalizacaoTO> query = builder.createQuery(InstrumentoFormalizacaoTO.class);
        Root<InstrumentoFormalizacao> fromInstr = query.from(InstrumentoFormalizacao.class);
        List<Predicate> conditions = new ArrayList();
        Join<InstrumentoFormalizacao, Projeto> fromProjeto = fromInstr.join(InstrumentoFormalizacao_.projetos);
        Join<Projeto, Empresa> fromEmpresa = fromProjeto.join(Projeto_.empresa);
        Join<Projeto, UnidadeEmpresa> fromUnidasdeEmpresa = fromProjeto.join(Projeto_.unidade, JoinType.LEFT);
        Join<InstrumentoFormalizacao, HistoricoSituacaoInstrumentoFormalizacao> fromHistoricoSituacaoInstrumentoformalizacao = fromInstr.join(InstrumentoFormalizacao_.situacaoAtual);
        Join<HistoricoSituacaoInstrumentoFormalizacao, SituacaoInstrumentoFormalizacao> fromSituacaoInstrumentoformalizacao = fromHistoricoSituacaoInstrumentoformalizacao.join(HistoricoSituacaoInstrumentoFormalizacao_.situacao);

        conditions.add(builder.equal(fromInstr.get(InstrumentoFormalizacao_.ultimaVersao), Boolean.TRUE));

        if (nomeEmpresa != null && !nomeEmpresa.trim().equals("")) {
            Join<Empresa, NomeEmpresa> fromNomesEmpresa = fromEmpresa.join(Empresa_.nomesEmpresa, JoinType.LEFT);
            conditions.add(builder.or(
                    builder.like(builder.lower(fromEmpresa.get(Empresa_.nomePrincipal)), "%" + nomeEmpresa.toLowerCase() + "%"),
                    builder.like(builder.lower(fromNomesEmpresa.get(NomeEmpresa_.nome)), "%" + nomeEmpresa.toLowerCase() + "%"),
                    builder.like(builder.lower(fromEmpresa.get(Empresa_.razaoSocial)), "%" + nomeEmpresa.toLowerCase() + "%")
            ));

        }

        if (protocolo != null && !protocolo.trim().equals("")) {
            conditions.add(builder.equal(fromInstr.get(InstrumentoFormalizacao_.protocolo), protocolo));
        }

        if (situacaoInstrumentoFormalizacaoId != null) {
            conditions.add(builder.equal(fromSituacaoInstrumentoformalizacao.get(SituacaoInstrumentoFormalizacao_.id), situacaoInstrumentoFormalizacaoId));
        }

        if (tipoInstrFormalizacao != null) {
            conditions.add(builder.equal(fromInstr.get(InstrumentoFormalizacao_.tipo), tipoInstrFormalizacao));
        }

        if (tituloInstrumento != null && !tituloInstrumento.trim().equals("")) {
            conditions.add(builder.like(fromInstr.get(InstrumentoFormalizacao_.titulo), "%" + tituloInstrumento + "%"));
        }

        if (nomeProjeto != null && !nomeProjeto.trim().equals("")) {
            conditions.add(builder.like(fromProjeto.get(Projeto_.nome), "%" + nomeProjeto + "%"));
        }

        TypedQuery<InstrumentoFormalizacaoTO> typedQuery = getEm().createQuery(query
                .multiselect(fromInstr.get(InstrumentoFormalizacao_.id),
                        fromInstr.get(InstrumentoFormalizacao_.protocolo),
                        fromInstr.get(InstrumentoFormalizacao_.tipo),
                        fromInstr.get(InstrumentoFormalizacao_.situacaoAtual),
                        fromInstr.get(InstrumentoFormalizacao_.dataAssinatura),
                        fromInstr.get(InstrumentoFormalizacao_.versao),
                        fromProjeto.get(Projeto_.empresa),
                        fromUnidasdeEmpresa,
                        fromInstr.get(InstrumentoFormalizacao_.localDocumento),
                        fromInstr.get(InstrumentoFormalizacao_.titulo))
                .where(conditions.toArray(new Predicate[]{}))
                .orderBy(builder.desc(fromInstr.get(InstrumentoFormalizacao_.dataUltimaAlteracao)))
        );
        Collection<InstrumentoFormalizacaoTO> resultList = reduceResultListInstrFormalizacao(typedQuery.getResultList());

        for (InstrumentoFormalizacaoTO instrumento : resultList) {
            List<InstrumentoFormalizacaoTO> versoesAnteriores = findVersoesAnteriores(instrumento);
            if (versoesAnteriores != null && !versoesAnteriores.isEmpty()) {
                instrumento.setVinculados(versoesAnteriores);
            }
        }
        return resultList;
    }

    public void updateDataAssinaturaTransactional(Long instFormId, User usuarioResponsavel, Date dataUltimaAlteracao, Date dataAssinatura) {
        this.getEm().joinTransaction();
        InstrumentoFormalizacao instrumentoFormalizacao = this.locateByIdForEdition(instFormId);
        this.merge(new HistoricoSituacaoInstrumentoFormalizacao(instFormId, SituacaoInstrFormalizacao.ASSINADO.getId().longValue()));
        instrumentoFormalizacao.setDataAssinatura(dataAssinatura);
        instrumentoFormalizacao.setSituacaoAtual(historicoInstrumentoFormalizacaoRepository.findUltimoHistoricoSituacaoByInstrumentoId(instrumentoFormalizacao.getId()));
        instrumentoFormalizacao.setSituacaoEnum(instrumentoFormalizacao.getSituacaoAtual().getSituacao().getId().intValue());
        this.merge(instrumentoFormalizacao);

        pesquisaService.createPesquisaPostInstrumentoSignatureTransactional(instrumentoFormalizacao);
    }

    public void updateLocalDocumento(Long instFormId, Long idLocal) {
        formalizacaoRepository.updateLocalDocumento(instFormId, idLocal);
    }

    public Long saveInstrumentoFormalizacao(InstrumentoFormalizacao instrumentoFormalizacao, Long idUsuarioResponsavel) {
        List<Projeto> projetoList = new ArrayList<>();
        if (instrumentoFormalizacao.getProjetos() != null && !instrumentoFormalizacao.getProjetos().isEmpty()) {
            for (Projeto proj : instrumentoFormalizacao.getProjetos()) {
                Projeto projetoDB = projetoRepository.findBy(proj.getId());
                projetoList.add(projetoDB);
            }
        }
        if (instrumentoFormalizacao.getId() == null) {
            if (instrumentoFormalizacao.getProtocolo() != null && instrumentoFormalizacao.getTipo() == TipoInstrFormalizacao.TERMO_ADITIVO) {
                Tuple ultimaVersao = this.findUltimaVersaoByProtocolo(instrumentoFormalizacao.getProtocolo());
                instrumentoFormalizacao.setVersao(ultimaVersao.get(1, Integer.class) + 1);
                formalizacaoRepository.uncheckUltimaVersaoById(ultimaVersao.get(0, Long.class));
            } else {
                instrumentoFormalizacao.setVersao(0);
            }
            instrumentoFormalizacao.setUltimaVersao(Boolean.TRUE);
        }

        if (!projetoList.isEmpty()) {
            instrumentoFormalizacao.setProjetos(projetoList);
        }

        instrumentoFormalizacao.setUsuarioResponsavel(userRepository.findBy(idUsuarioResponsavel));

        if (instrumentoFormalizacao.getSignatarios() != null && !instrumentoFormalizacao.getSignatarios().isEmpty()) {
            for(Signatario s : instrumentoFormalizacao.getSignatarios()) {
                s.setInstrFormalizacao(instrumentoFormalizacao);
            }
        }

        HistoricoSituacaoInstrumentoFormalizacao situacao = null;
        if (instrumentoFormalizacao.getSituacaoAtual() != null && instrumentoFormalizacao.getSituacaoAtual().getId() != null) {
            if (instrumentoFormalizacao.getSituacaoAtual().getSituacao().getId().intValue() != instrumentoFormalizacao.getSituacaoEnum()) {
                situacao = new HistoricoSituacaoInstrumentoFormalizacao(instrumentoFormalizacao);
                instrumentoFormalizacao.setSituacaoAtual(null);
            }
        } else {
            situacao = new HistoricoSituacaoInstrumentoFormalizacao(instrumentoFormalizacao.getId(), SituacaoInstrFormalizacao.EM_NEGOCIACAO.getId().longValue());
            instrumentoFormalizacao.setSituacaoAtual(null);
        }

        instrumentoFormalizacao = formalizacaoRepository.save(instrumentoFormalizacao);

        if (situacao != null) {
            situacao.setInstrumentoFormalizacao(instrumentoFormalizacao);
            historicoInstrumentoFormalizacaoRepository.save(situacao);
            instrumentoFormalizacao.setSituacaoAtual(situacao);
            formalizacaoRepository.save(instrumentoFormalizacao);
        }
        return instrumentoFormalizacao.getId();
    }

    private Collection<InstrumentoFormalizacaoTO> reduceResultListInstrFormalizacao(List<InstrumentoFormalizacaoTO> resultList) {
        Map<Long, InstrumentoFormalizacaoTO> mapIntrFormalizacao = new HashMap<>();
        if (resultList != null) {
            for (InstrumentoFormalizacaoTO instr : resultList) {
                if (mapIntrFormalizacao.containsKey(instr.getId())) {
                    InstrumentoFormalizacaoTO instrTO = mapIntrFormalizacao.get(instr.getId());
                    instrTO.getEmpresas().add(instr.getEmpresaOrUnidade());
                } else {
                    instr.setEmpresas(new HashSet<>());
                    instr.getEmpresas().add(instr.getEmpresaOrUnidade());
                    mapIntrFormalizacao.put(instr.getId(), instr);
                }
            }
        }
        return mapIntrFormalizacao.values();
    }

    private void populateSelectedLists(InstrumentoFormalizacao instrumentoFormalizacao) {

        List<Tuple> resultList = findProjectsAndCompanies(instrumentoFormalizacao);

        Set<ProjetoTO> projetoTOSet = new HashSet<>();
        Set<EmpresaUnidadeDTO> empresaUnidadeTOSet = new HashSet<>();

        for (Tuple tuple : resultList) {
            Long projetoId = (Long) tuple.get(1);
            String projetoNome = (String) tuple.get(2);
            Empresa empresa = (Empresa) tuple.get(3);
            UnidadeEmpresa unidadeEmpresa = (UnidadeEmpresa) tuple.get(4);
            Long versaoPai = (Long) tuple.get(5);
            User usuarioInvestimento = (User)tuple.get(6);

            ProjetoTO projetoTO = new ProjetoTO(projetoId, projetoNome);
            projetoTO.setVersaoPai(versaoPai);
            projetoTO.setUsuarioInvestimento(usuarioInvestimento);

            projetoTOSet.add(projetoTO);
            EmpresaUnidadeDTO empresaUnidadeDTO = buildEmpresaUnidadeDTO(unidadeEmpresa, empresa);
            empresaUnidadeTOSet.add(empresaUnidadeDTO);
        }

        instrumentoFormalizacao.setProjetosSelecionados(projetoTOSet);
        instrumentoFormalizacao.setEmpresasSelecionadas(empresaUnidadeTOSet);
    }

    private void populateAtualSelectedLists(InstrumentoFormalizacao instrumentoFormalizacao) {

        List<Tuple> resultList = findProjectsAndCompanies(instrumentoFormalizacao);

        Set<ProjetoTO> projetoTOSet = new HashSet<>();
        Set<EmpresaUnidadeDTO> empresaUnidadeTOSet = new HashSet<>();

        for (Tuple tuple : resultList) {
            Long projetoId = (Long) tuple.get(1);
            String projetoNome = (String) tuple.get(2);
            ProjetoTO projetoTO = new ProjetoTO(projetoId, projetoNome);
            Long versaoPai = (Long) tuple.get(5);
            projetoTO.setVersaoPai(versaoPai);
            projetoTOSet.add(projetoTO);
        }
        
        resultList = findProjectsCurrentVersionAndCompanies(projetoTOSet.stream().map(u -> u.getVersaoPai()).collect(Collectors.toList()));
        
        projetoTOSet.clear();
        
        for (Tuple tuple : resultList) {
            Long projetoId = (Long) tuple.get(0);
            String projetoNome = (String) tuple.get(1);
            Empresa empresa = (Empresa) tuple.get(2);
            UnidadeEmpresa unidadeEmpresa = (UnidadeEmpresa) tuple.get(3);
            ProjetoTO projetoTO = new ProjetoTO(projetoId, projetoNome);
            Long versaoPai = (Long) tuple.get(4);
            projetoTO.setVersaoPai(versaoPai);


            EmpresaUnidadeDTO empresaUnidadeDTO = buildEmpresaUnidadeDTO(unidadeEmpresa, empresa);
            empresaUnidadeTOSet.add(empresaUnidadeDTO);
            
            projetoTOSet.add(projetoTO);
        }
        instrumentoFormalizacao.setProjetosSelecionados(projetoTOSet);
        instrumentoFormalizacao.setEmpresasSelecionadas(empresaUnidadeTOSet);
    }

    private EmpresaUnidadeDTO buildEmpresaUnidadeDTO(UnidadeEmpresa unidadeEmpresa, Empresa empresa) {
        String nomePrincipal = (unidadeEmpresa == null) ? empresa.getNomePrincipal() : unidadeEmpresa.getNome();
        String razaoSocial = (unidadeEmpresa == null) ? empresa.getRazaoSocial() : unidadeEmpresa.getNome();
        String enderecoFormatado = getEnderecoEmpresaUnidadeFormatado(unidadeEmpresa, empresa);
        EmpresaUnidadeDTO empresaUnidadeDTO = new EmpresaUnidadeDTO(empresa.getId(), (unidadeEmpresa != null) ? unidadeEmpresa.getId() : null, nomePrincipal, razaoSocial, enderecoFormatado, empresa.getResponsavel());
        return empresaUnidadeDTO;
    }

    private String getEnderecoEmpresaUnidadeFormatado(UnidadeEmpresa unidadeEmpresa, Empresa empresa) {
        boolean isUnidade = unidadeEmpresa != null;
        String enderecoUnidade = isUnidade && unidadeEmpresa.getEndereco() != null ? unidadeEmpresa.getEndereco().getEnderecoFormatado() : "";
        String enderecoEmpresa = empresa.getEndereco() != null ? empresa.getEndereco().getEnderecoFormatado() : "";
        
        return isUnidade ? enderecoUnidade : enderecoEmpresa;
    }

    public List<ProjetoTO> locateAvaibleProjectsByEmpresa(List<Long> idsEmpresa) {
        return locateAvaibleProjects(idsEmpresa, null);
    }

    public List<ProjetoTO> locateAvaibleProjectsByUnidade(List<Long> idsUnidade) {
        return locateAvaibleProjects(null, idsUnidade);
    }

    private List<ProjetoTO> locateAvaibleProjects(List<Long> idsEmpresa, List<Long> idsUnidade) {
        CriteriaBuilder builder = getEm().getCriteriaBuilder();
        CriteriaQuery<ProjetoTO> query = builder.createQuery(ProjetoTO.class);
        Root<Projeto> fromProjeto = query.from(Projeto.class);
        List<Predicate> conditions = new ArrayList();
        Join<Projeto, InstrumentoFormalizacao> fromInstForm = fromProjeto.join(Projeto_.instrumentos, JoinType.LEFT);

        if (idsUnidade != null && !idsUnidade.isEmpty()) {
            Join<Projeto, UnidadeEmpresa> fromUnidadeEmpresa = fromProjeto.join(Projeto_.unidade, JoinType.INNER);
            conditions.add(fromUnidadeEmpresa.get(UnidadeEmpresa_.id).in(idsUnidade));
        } else if (idsEmpresa != null && !idsEmpresa.isEmpty()) {
            Join<Projeto, Empresa> fromEmpresa = fromProjeto.join(Projeto_.empresa, JoinType.INNER);
            conditions.add(fromEmpresa.get(Empresa_.id).in(idsEmpresa));
            conditions.add(fromProjeto.get(Projeto_.unidade).isNull());
        }
        //Deverá exibir somente projetos que estejam nos seguintes estágios: PP, DF, II ou OI, e sua Situação\Status esteja Ativo.
        conditions.add(fromProjeto.get(Projeto_.estagioAtual).in(Estagio.FORMALIZADA, Estagio.PROJETO_PROMISSOR, Estagio.OPERACAO_INICIADA, Estagio.IMPLANTACAO_INICIADO));
        conditions.add(fromProjeto.get(Projeto_.situacao).in(SituacaoProjetoEnum.ATIVO));
        conditions.add(builder.equal(fromProjeto.get(Projeto_.ultimaVersao), true));

        Subquery<Projeto> sqProjeto = query.subquery(Projeto.class);
        Root fromSqProjeto = sqProjeto.from(Projeto.class);
        Join<Projeto, InstrumentoFormalizacao> fromSqInstr = fromSqProjeto.join(Projeto_.instrumentos);
        sqProjeto.select(fromSqProjeto.get(Projeto_.id));        
        sqProjeto.correlate(fromProjeto);
        sqProjeto.where(builder.equal(fromSqInstr.get(InstrumentoFormalizacao_.situacaoEnum), SituacaoInstrFormalizacao.EM_NEGOCIACAO.getId()));
        conditions.add(builder.not(fromProjeto.in(sqProjeto)));
                
//        Subquery<InstrumentoFormalizacao> sq = query.subquery(InstrumentoFormalizacao.class);
//        Root fromInstrumento = sq.from(InstrumentoFormalizacao.class);
//        sq.select(fromInstrumento.get(InstrumentoFormalizacao_.id));
//        sq.correlate(fromProjeto);
//        sq.where(builder.equal(fromInstrumento.get(InstrumentoFormalizacao_.situacaoEnum), SituacaoInstrFormalizacao.EM_NEGOCIACAO.getId()));
//        conditions.add(builder.or(builder.isNull(fromInstForm), builder.not(fromInstForm.in(sq))));

        TypedQuery<ProjetoTO> typedQuery = getEm().createQuery(query
                .multiselect(fromProjeto.get(Projeto_.id),
                        fromProjeto.get(Projeto_.nome),
                        fromProjeto.get(Projeto_.versaoPai))
                .where(conditions.toArray(new Predicate[]{}))
                .orderBy(builder.asc(fromProjeto.get(Projeto_.id)))
                .distinct(true)
        );
        return typedQuery.getResultList();

    }

    private List<Tuple> findProjectsAndCompanies(InstrumentoFormalizacao instrumentoFormalizacao) {
        CriteriaBuilder builder = getEm().getCriteriaBuilder();
        CriteriaQuery<Tuple> query = builder.createQuery(Tuple.class);
        Root<InstrumentoFormalizacao> fromInstr = query.from(InstrumentoFormalizacao.class);
        List<Predicate> conditions = new ArrayList();
        conditions.add(builder.equal(fromInstr.get(InstrumentoFormalizacao_.id), instrumentoFormalizacao.getId()));
        Join<InstrumentoFormalizacao, Projeto> fromProjeto = fromInstr.join(InstrumentoFormalizacao_.projetos);
        Join<Projeto, UnidadeEmpresa> fromUnidasdeEmpresa = fromProjeto.join(Projeto_.unidade, JoinType.LEFT);
        TypedQuery<Tuple> typedQuery = getEm().createQuery(query
                .multiselect(fromInstr.get(InstrumentoFormalizacao_.id),
                        fromProjeto.get(Projeto_.id),
                        fromProjeto.get(Projeto_.nome),
                        fromProjeto.get(Projeto_.empresa),
                        fromUnidasdeEmpresa,
                        fromProjeto.get(Projeto_.versaoPai),
                        fromProjeto.get(Projeto_.usuarioInvestimento))
                .where(conditions.toArray(new Predicate[]{}))
        );
        List<Tuple> resultList = typedQuery.getResultList();
        return resultList;
    }

    private List<Tuple> findProjectsCurrentVersionAndCompanies(List<Long> familiaIds) {
        CriteriaBuilder builder = getEm().getCriteriaBuilder();
        CriteriaQuery<Tuple> query = builder.createQuery(Tuple.class);
        Root<Projeto> fromProjeto = query.from(Projeto.class);
        List<Predicate> conditions = new ArrayList();
        conditions.add(fromProjeto.get(Projeto_.versaoPai).in(familiaIds));
        conditions.add(builder.equal(fromProjeto.get(Projeto_.ultimaVersao), true));
        Join<Projeto, UnidadeEmpresa> fromUnidadesEmpresa = fromProjeto.join(Projeto_.unidade, JoinType.LEFT);
        
        TypedQuery<Tuple> typedQuery = getEm().createQuery(query
                .multiselect(fromProjeto.get(Projeto_.id),
                        fromProjeto.get(Projeto_.nome),
                        fromProjeto.get(Projeto_.empresa),
                        fromUnidadesEmpresa,
                        fromProjeto.get(Projeto_.versaoPai))
                .where(conditions.toArray(new Predicate[]{}))
        );
        List<Tuple> resultList = typedQuery.getResultList();
        return resultList;
    }

    public InstrumentoFormalizacao locateByIdForEdition(Long id) {
        InstrumentoFormalizacao instrumentoFormalizacaoForEdit = locateByNamedQuery(InstrumentoFormalizacao.Query.findByIdForEdit, QueryParameter.with("id", id).parameters());
        if (instrumentoFormalizacaoForEdit != null) {
            populateSelectedLists(instrumentoFormalizacaoForEdit);
        }
        return instrumentoFormalizacaoForEdit;
    }

    public InstrumentoFormalizacao locateProcoloVersaoOriginal(String protocolo) {
        InstrumentoFormalizacao instrOriginal = locateByNamedQuery(InstrumentoFormalizacao.Query.findByProcoloVersaoOriginal, QueryParameter.with("protocolo", protocolo).parameters());
        if (instrOriginal != null) {
            populateSelectedLists(instrOriginal);
        }
        return instrOriginal;
    }

    public InstrumentoFormalizacao locateByProcoloVersaoOriginalProjetosAtuais(String protocolo) {
        InstrumentoFormalizacao instrOriginal = locateByNamedQuery(InstrumentoFormalizacao.Query.findByProcoloVersaoOriginal, QueryParameter.with("protocolo", protocolo).parameters());
        if (instrOriginal != null) {
            populateAtualSelectedLists(instrOriginal);
        }
        return instrOriginal;
    }

    public InstrumentoFormalizacaoTO findTOById(Long id) {
        InstrumentoFormalizacao instrumentoFormalizacao = this.locateByIdForEdition(id);
        InstrumentoFormalizacaoTO instrumentoFormalizacaoTO = null;

        if (instrumentoFormalizacao != null) {
            instrumentoFormalizacaoTO = new InstrumentoFormalizacaoTO();
            instrumentoFormalizacaoTO.setId(id);
            instrumentoFormalizacaoTO.setSituacao(instrumentoFormalizacao.getSituacaoAtual());
            instrumentoFormalizacaoTO.setDataAssinatura(instrumentoFormalizacao.getDataAssinatura());
            instrumentoFormalizacaoTO.setVersao(instrumentoFormalizacao.getVersao());
            instrumentoFormalizacaoTO.setEmpresas(instrumentoFormalizacao.getEmpresasSelecionadas().stream().map(u -> u.getNomePrincipal()).collect(Collectors.toSet()));
            instrumentoFormalizacaoTO.setProjetos(instrumentoFormalizacao.getProjetosSelecionados().stream().map(u -> u.getNomeProjeto()).collect(Collectors.toSet()));
            instrumentoFormalizacaoTO.setTipoInstrFormalizacao(instrumentoFormalizacao.getTipo());
            instrumentoFormalizacaoTO.setUsuarioResponsavelPelaInformacao(instrumentoFormalizacao.getUsuarioResponsavel().getNome());
            instrumentoFormalizacaoTO.setDataUltimaAlteracao(instrumentoFormalizacao.getDataUltimaAlteracao());
            instrumentoFormalizacaoTO.setSituacoes(instrumentoFormalizacao.getSituacoes());
        }

        return instrumentoFormalizacaoTO;
    }

    public List<InstrumentoFormalizacaoTO> findByProjetoIdWithFirstVersion(Long projetoId) {
        return findByProjetoId(projetoId, true);
    }

    public List<InstrumentoFormalizacaoTO> findByProjetoId(Long projetoId, boolean isFirstVersion) {
        CriteriaBuilder builder = getEm().getCriteriaBuilder();
        CriteriaQuery<InstrumentoFormalizacaoTO> query = builder.createQuery(InstrumentoFormalizacaoTO.class);
        Root<InstrumentoFormalizacao> fromInstr = query.from(InstrumentoFormalizacao.class);
        List<Predicate> conditions = new ArrayList();

        Join<InstrumentoFormalizacao, Projeto> fromProjeto = fromInstr.join(InstrumentoFormalizacao_.projetos);

        conditions.add(builder.equal(fromProjeto.get(Projeto_.id), projetoId));
        if (isFirstVersion) {
            conditions.add(builder.equal(fromInstr.get(InstrumentoFormalizacao_.versao), 0L));
        }

        TypedQuery<InstrumentoFormalizacaoTO> typedQuery = getEm().createQuery(query
                .multiselect(fromInstr.get(InstrumentoFormalizacao_.id),
                        fromInstr.get(InstrumentoFormalizacao_.protocolo),
                        fromInstr.get(InstrumentoFormalizacao_.tipo),
                        fromInstr.get(InstrumentoFormalizacao_.situacaoAtual),
                        fromInstr.get(InstrumentoFormalizacao_.dataAssinatura),
                        fromInstr.get(InstrumentoFormalizacao_.versao))
                .where(conditions.toArray(new Predicate[]{}))
                .distinct(true)
                .orderBy(builder.asc(fromInstr.get(InstrumentoFormalizacao_.id))
                )
        );

        List<InstrumentoFormalizacaoTO> resultList = typedQuery.getResultList();

        return resultList;
    }

    private Tuple findUltimaVersaoByProtocolo(String protocolo) {
        CriteriaBuilder builder = getEm().getCriteriaBuilder();
        CriteriaQuery<Tuple> query = builder.createQuery(Tuple.class);
        Root<InstrumentoFormalizacao> fromInstr = query.from(InstrumentoFormalizacao.class);
        List<Predicate> conditions = new ArrayList();
        conditions.add(builder.equal(fromInstr.get(InstrumentoFormalizacao_.protocolo), protocolo));
        conditions.add(builder.equal(fromInstr.get(InstrumentoFormalizacao_.ultimaVersao), Boolean.TRUE));

        TypedQuery<Tuple> typedQuery = getEm().createQuery(query
                .multiselect(fromInstr.get(InstrumentoFormalizacao_.id),
                        fromInstr.get(InstrumentoFormalizacao_.versao))
                .where(conditions.toArray(new Predicate[]{}))
        );
        Tuple resultList = typedQuery.getSingleResult();
        return resultList;
    }
}
