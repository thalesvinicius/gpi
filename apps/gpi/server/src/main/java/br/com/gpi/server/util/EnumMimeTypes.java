/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.gpi.server.util;

import static br.com.gpi.server.util.functional.FunctionalInterfaces.*;
import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import net.sf.dynamicreports.jasper.builder.JasperReportBuilder;
import static net.sf.dynamicreports.report.builder.DynamicReports.export;

/**
 *
 * @author rafaelbfs
 *  
 */
public enum EnumMimeTypes {
    DOCX("application/vnd.openxmlformats-officedocument.wordprocessingml.document", JasperReportBuilder::toDocx ),
    XLSX("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", (jrb,os)-> jrb.ignorePagination().toXlsx(export.xlsxExporter(os).setDetectCellType(Boolean.TRUE))),
    XLS("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",(jrb,os)-> jrb.ignorePagination().toXlsx(os)),         
    DOC("application/msword",JasperReportBuilder::toRtf),
    PDF("application/pdf",JasperReportBuilder::toPdf);
    
    //"application/vnd.ms-excel"
   
    
    
    private EnumMimeTypes(String mimeType, BiFunctionChecked<JasperReportBuilder,OutputStream,JasperReportBuilder> bifunc){
        this.mimeType = mimeType;
        this.exporter = bifunc;
    }
    
    public BinarySupplier exportReport(JasperReportBuilder jrb){
        
        return () -> {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            exporter.apply(jrb, baos);
            return baos;
        };
        
    }
    
    public final String mimeType;
    private final BiFunctionChecked<JasperReportBuilder,OutputStream,JasperReportBuilder> exporter;
    
   
    
}
