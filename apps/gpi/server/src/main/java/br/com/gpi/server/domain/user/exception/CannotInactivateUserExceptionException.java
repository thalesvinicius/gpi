package br.com.gpi.server.domain.user.exception;

import br.com.gpi.server.core.exception.BaseWebApplicationException;

public class CannotInactivateUserExceptionException extends BaseWebApplicationException {

    public CannotInactivateUserExceptionException() {
        super(409, "40901", "MSG89_CONTATO_RESPONSAVEL", "An atempt was made to inactivate a user that is mailing or is investiment responsible!");
    }
}
