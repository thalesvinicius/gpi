package br.com.gpi.server.domain.entity;

import br.com.gpi.server.core.jpa.converter.TipoInvestimentoEnumConverter;
import br.com.gpi.server.domain.common.BaseEntity;
import br.com.gpi.server.domain.entity.enumerated.EnumTipoInvestimento;
import com.fasterxml.jackson.annotation.JsonIgnore;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.envers.Audited;

@Audited
@Entity
@Table(name = "UsoFonte")
public class UsoFonte extends BaseEntity<Long> {
    
    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "financeiro_id")
    private Financeiro financeiro;
    
    @Enumerated
    @Column(name = "tipo", updatable = false, insertable = false)
    private EnumTipoInvestimento tipo;
    
    @JsonIgnore
    @Column(name = "tipo")
    private Integer tipoInt;
    
    private Double valorRealizado;
    
    private Double valorARealizar;

    public Financeiro getFinanceiro() {
        return financeiro;
    }

    public void setFinanceiro(Financeiro financeiro) {
        this.financeiro = financeiro;
    }

    public EnumTipoInvestimento getTipo() {
        return tipo;
    }

    public void setTipo(EnumTipoInvestimento tipoInvestimento) {
        if(tipoInvestimento == null){
            return;
        }
        this.tipo = tipoInvestimento;
        this.tipoInt = (new TipoInvestimentoEnumConverter()).convertToDatabaseColumn(tipoInvestimento);
    }

    public Integer getTipoInt() {
        return tipoInt;
    }

    public void setTipoInt(Integer tipoInt) {
        if(tipoInt == null) {
            return;
        }
        this.tipoInt = tipoInt;
        this.tipo = (new TipoInvestimentoEnumConverter()).convertToEntityAttribute(tipoInt);
    }
    
    public Double getValorRealizado() {
        return valorRealizado;
    }

    public void setValorRealizado(Double valorRealizado) {
        this.valorRealizado = valorRealizado;
    }

    public Double getValorARealizar() {
        return valorARealizar;
    }

    public void setValorARealizar(Double valorARealizar) {
        this.valorARealizar = valorARealizar;
    }
    
    
}
