package br.com.gpi.server.to;

import br.com.gpi.server.domain.entity.Endereco;
import java.util.Objects;

public class EmpresaDTO {

    private final Long empresaId;
    private final String nomeEmpresa;
    private final Endereco enderecoEmpresa;

    public EmpresaDTO(Long empresaId, String nomeEmpresa, Endereco enderecoEmpresa) {
        this.empresaId = empresaId;
        this.nomeEmpresa = nomeEmpresa;
        this.enderecoEmpresa = enderecoEmpresa;
    }

    public Long getEmpresaId() {
        return empresaId;
    }

    public String getNomeEmpresa() {
        return nomeEmpresa;
    }

    public Endereco getEnderecoEmpresa() {
        return enderecoEmpresa;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 37 * hash + Objects.hashCode(this.empresaId);
        hash = 37 * hash + Objects.hashCode(this.nomeEmpresa);
        hash = 37 * hash + Objects.hashCode(this.enderecoEmpresa);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final EmpresaDTO other = (EmpresaDTO) obj;
        if (!Objects.equals(this.empresaId,  other.empresaId)) {
            return false;
        }        
        return true;
    }

}
