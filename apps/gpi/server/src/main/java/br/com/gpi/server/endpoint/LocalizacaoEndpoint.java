package br.com.gpi.server.endpoint;

import br.com.gpi.server.domain.common.BaseService;
import br.com.gpi.server.domain.entity.Anexo;
import br.com.gpi.server.domain.entity.Localizacao;
import br.com.gpi.server.domain.repository.LocalizacaoRepository;
import br.com.gpi.server.domain.service.LocalizacaoService;
import br.com.gpi.server.domain.service.MicroRegiaoService;
import br.com.gpi.server.domain.service.MunicipioService;
import br.com.gpi.server.domain.service.ProjetoService;
import br.com.gpi.server.domain.service.RegiaoPlanejamentoService;
import br.com.gpi.server.domain.service.UserService;
import br.com.gpi.server.to.LocalizacaoEmpreendimentoDTO;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.inject.Inject;
import javax.transaction.SystemException;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

@Path("/projeto/{idProjeto}/localizacao")
@Stateless
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
@TransactionManagement(TransactionManagementType.BEAN)
public class LocalizacaoEndpoint {

    @Inject
    private LocalizacaoService localizacaoService;
    @Inject
    private LocalizacaoRepository localizacaoRepository;
    @Inject
    private ProjetoService projetoService;
    @Inject
    private UserService userService;
    @Inject
    private MicroRegiaoService microRegiaoService;
    @Inject
    private RegiaoPlanejamentoService regiaoPlanejamentoService;
    @Inject
    private MunicipioService municipioService;
    @Inject
    private BaseService<Anexo> anexoService;

    @PathParam("idProjeto")
    Long idProjeto;

    @POST
    public Response saveOrUpdateLocalizacao(Localizacao localizacao, @Context SecurityContext securityContext) {
        try {
            return Response.ok(localizacaoService.saveOrUpdateLocalizacao(localizacao, securityContext, idProjeto)).build();
        } catch (Exception ex) {
            Logger.getLogger(LocalizacaoEndpoint.class.getName()).log(Level.SEVERE, null, ex);
            throw new RuntimeException(ex);
        }
    }

    @GET
    @Path("findChange")
    public Response findChange() {
        Object localizacao = null;
        try {
            localizacao = localizacaoService.verifyChange(idProjeto);
        } catch (SystemException ex) {
            Logger.getLogger(LocalizacaoEndpoint.class.getName()).log(Level.SEVERE, null, ex);
        }
        return Response.ok(localizacao).build();
    }
    
    @GET
    @Path("{id}")
    public Response findById(@PathParam("id") Long id) {
        Localizacao localizacao = localizacaoRepository.findBy(id);
        return Response.ok(localizacao).build();
    }

    @GET
    public Response findAllLocalizacaoByProjetoId() {
        List<Localizacao> localizacaoList = localizacaoRepository.findAllByProjetoId(idProjeto);
        return Response.ok(localizacaoList).build();
    }

    @GET
    @Path("findLocalizacoes/{paramStr}")
    public Response findLocalizacoesEmpreendimento(@PathParam("paramStr") String paramStr) {
        List<LocalizacaoEmpreendimentoDTO> localizacoes = localizacaoService.findLocalizacoesEmpreendimentos(paramStr);
        return Response.ok(localizacoes).build();
    }

}
