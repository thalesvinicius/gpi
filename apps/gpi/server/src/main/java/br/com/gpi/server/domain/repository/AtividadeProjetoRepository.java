package br.com.gpi.server.domain.repository;

import br.com.gpi.server.domain.entity.AtividadeProjeto;
import org.apache.deltaspike.data.api.EntityRepository;
import org.apache.deltaspike.data.api.Repository;
import org.apache.deltaspike.data.api.criteria.CriteriaSupport;

@Repository(forEntity = AtividadeProjeto.class)
public abstract class AtividadeProjetoRepository implements CriteriaSupport<AtividadeProjeto>,EntityRepository<AtividadeProjeto, Long>{

}
