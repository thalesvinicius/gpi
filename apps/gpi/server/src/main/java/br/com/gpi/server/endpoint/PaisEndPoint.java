package br.com.gpi.server.endpoint;

import br.com.gpi.server.domain.service.PaisService;
import br.com.gpi.server.to.PaisTO;
import java.util.List;
import javax.annotation.security.PermitAll;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/pais")
@Stateless
@PermitAll
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
public class PaisEndPoint {
    
    @Inject
    PaisService paisService;
            
    @GET
    public Response findPaisesDiferentedeBrasil() {
        List<PaisTO> paises = paisService.buscaPaisesDiferentedeBrasil();
        return Response.ok(paises).build();
    }
}
