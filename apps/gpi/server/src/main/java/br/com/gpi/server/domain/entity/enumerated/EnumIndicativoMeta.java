package br.com.gpi.server.domain.entity.enumerated;

import br.com.gpi.server.core.exception.EnumException;

public enum EnumIndicativoMeta {

    INVALID(0, "invalid"),  
    PROTOCOLOS_ASSINADOS_ANO(1, "Formalização de Investimento"),
    INDICE_SATISFACAO(2, "Índice de Satisfação"),
    QUANTIDADE_PROJETOS(3, "Quantidade de Projetos"),
    INVESTIMENTO(4, "Investimento"),
    EMPREGOS_DIRETOS(5, "Empregos Diretos"),
    FATURAMENTO_PLENO(6, "Faturamento Pleno");

    private final Integer id;
    private final String descricao;

    private EnumIndicativoMeta(Integer id, String descricao) {
        this.id = id;
        this.descricao = descricao;
    }

    public Integer getId() {
        return id;
    }

    public String getDescricao() {
        return descricao;
    }

    public static EnumIndicativoMeta getIndicativoMetaById(Integer id) {
        for(EnumIndicativoMeta indicativo : values()){
            if (indicativo.getId().equals(id)){
                return indicativo;
            }
        }
        throw new EnumException(String.format("Não foi possível recuperar o IndicativoMeta com o id %s", id));
    }
}
