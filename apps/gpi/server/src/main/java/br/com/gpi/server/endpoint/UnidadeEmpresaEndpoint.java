package br.com.gpi.server.endpoint;

import br.com.gpi.server.domain.entity.Empresa;
import br.com.gpi.server.domain.entity.UnidadeEmpresa;
import br.com.gpi.server.domain.entity.User;
import br.com.gpi.server.domain.repository.EmpresaRepository;
import br.com.gpi.server.domain.repository.UnidadeEmpresaRepository;
import br.com.gpi.server.domain.repository.UserRepository;
import br.com.gpi.server.domain.service.UnidadeEmpresaService;
import br.com.gpi.server.domain.user.model.SecurityUser;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.inject.Inject;
import javax.transaction.SystemException;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

@Path("/empresa/{idEmpresa}/unidade")
@Stateless
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
@TransactionManagement(TransactionManagementType.BEAN)
public class UnidadeEmpresaEndpoint {

    @Inject
    private UnidadeEmpresaService unidadeEmpresaService;
    @Inject
    private UnidadeEmpresaRepository unidadeEmpresaRepository;
    @Inject
    private EmpresaRepository empresaRepository;
    @Inject
    private UserRepository userRepository;
    
    @PathParam("idEmpresa")
    private Long idEmpresa;

    @POST
    public Response saveOrUpdateUnidadeEmpresa(UnidadeEmpresa unidadeEmpresa, @Context SecurityContext securityContext) {
        Empresa empresa = empresaRepository.findBy(idEmpresa);                
        unidadeEmpresa.setEmpresa(empresa);        
        SecurityUser loggedUserWrap = (SecurityUser) securityContext.getUserPrincipal();
        User loggedUser = userRepository.findBy(loggedUserWrap.getId());
        unidadeEmpresa.setUsuarioResponsavel(loggedUser);
        unidadeEmpresa.setDataUltimaAlteracao(new Date());        
        unidadeEmpresaRepository.save(unidadeEmpresa);                
        return Response.ok(unidadeEmpresa).build();
    }

    @GET
    @Path("{id}")
    public UnidadeEmpresa getUnidadeEmpresa(@PathParam("id") Long id) {
        return unidadeEmpresaRepository.findBy(id);
    }

    @GET
    public List<UnidadeEmpresa> findAllByEmpresaId() {
        List<UnidadeEmpresa> unidadeEmpresas = unidadeEmpresaService.findByEmpresaId(idEmpresa);
        return unidadeEmpresas;
    }
    
    @POST
    @Path("removeUnidade")
    public Response removeUnidade(List<Long> ids) throws Exception {
        try {
            unidadeEmpresaService.removeUnidadeEmpresa(ids);
            return Response.ok("Tudo Ok!").build();
        } catch (SystemException ex) {
            Logger.getLogger(EmpresaEndpoint.class.getName()).log(Level.SEVERE, null, ex);
            throw new RuntimeException(ex);
        }
    }
    
    @GET
    @Path("getUnidadeComNome")
    public Long getUnidadeComNome(@QueryParam("nomeUnidade") String nomeUnidade){
        List<UnidadeEmpresa> unidadesList = unidadeEmpresaRepository.getUnidadeComNome(nomeUnidade, idEmpresa);
        if(unidadesList == null || unidadesList.isEmpty()){
            return null;
        }else{
            return unidadesList.get(0).getId();
        }
    }
}
