package br.com.gpi.server.domain.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import org.hibernate.envers.Audited;

@Audited
@Entity
@DiscriminatorValue("2")
@NamedQueries({
    @NamedQuery(name = UsuarioExterno.Query.locateByLogin, query = "SELECT u FROM UsuarioExterno u WHERE u.email = :login"),
    @NamedQuery(name = UsuarioExterno.Query.locateByLogin2, query = "SELECT u FROM UsuarioExterno u WHERE u.email = ?1 AND u.empresaId <> ?2"),
    @NamedQuery(name = UsuarioExterno.Query.updateSituacaoByEmpresa, query = "update UsuarioExterno u SET u.ativo = ?1 WHERE u.empresaId = ?2"),
    @NamedQuery(name = UsuarioExterno.Query.locateByEmpresa, query = "SELECT u FROM UsuarioExterno u WHERE u.empresaId = ?1"),
    @NamedQuery(name = UsuarioExterno.Query.locateByUnidade, query = "SELECT u FROM UsuarioExterno u WHERE u.unidadeId = ?1"),
    @NamedQuery(name = UsuarioExterno.Query.countByLoginEqualsAndIdNotEquals, query = "SELECT COUNT(u) FROM UsuarioExterno u WHERE u.email = ?1 AND u.id <> ?2 AND u.empresaId = ?3 AND u.ativo = ?4")
})
public class UsuarioExterno extends User {

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "empresa_id", insertable = false, updatable = false)
    private Empresa empresa;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "unidade_id", insertable = false, updatable = false)
    private UnidadeEmpresa unidade;
    
    @Column(name = "empresa_id")
    private Long empresaId;

    @Column(name = "unidade_id")
    private Long unidadeId;
    
    @NotNull
    @Column(name = "cargoExterno", nullable = false)
    private String cargoExterno;

    @Temporal(TemporalType.TIMESTAMP)
    private Date dataBloqueioTemporario;

    public Long getEmpresaId() {
        return empresaId;
    }

    public void setEmpresaId(Long empresaId) {
        this.empresaId = empresaId;
    }

    public Long getUnidadeId() {
        return unidadeId;
    }

    public void setUnidadeId(Long unidadeId) {
        this.unidadeId = unidadeId;
    }

    public Date getDataBloqueioTemporario() {
        return dataBloqueioTemporario;
    }

    public void setDataBloqueioTemporario(Date dataBloqueioTemporario) {
        this.dataBloqueioTemporario = dataBloqueioTemporario;
    }
    
    public Boolean isFromUnidade () {
        return this.getUnidadeId() != null;
    }

    public Empresa getEmpresa() {
        return empresa;
    }

    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }

    public UnidadeEmpresa getUnidade() {
        return unidade;
    }

    public void setUnidade(UnidadeEmpresa unidade) {
        this.unidade = unidade;
    }

    @Override
    public String getLogin() {
        return super.getEmail();
    }

    public String getCargoExterno() {
        return cargoExterno;
    }

    public void setCargoExterno(String cargoExterno) {
        this.cargoExterno = cargoExterno;
    }
    
    public static class Query {

        public static final String locateByLogin = "UsuarioExterno.locateByLogin";
        public static final String locateByLogin2 = "UsuarioExterno.locateByLogin2";
        public static final String updateSituacaoByEmpresa = "UsuarioExterno.updateSituacaoByEmpresa";
        public static final String locateByEmpresa = "UsuarioExterno.locateByEmpresa";
        public static final String locateByUnidade = "UsuarioExterno.locateByUnidade";
        public static final String countByLoginEqualsAndIdNotEquals = "UsuarioExterno.countByLoginEqualsAndIdNotEquals";
    }

}
