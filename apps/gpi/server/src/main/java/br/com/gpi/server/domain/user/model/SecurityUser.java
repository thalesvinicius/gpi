package br.com.gpi.server.domain.user.model;

import br.com.gpi.server.domain.entity.Departamento;
import br.com.gpi.server.domain.entity.GrupoUsuario;
import br.com.gpi.server.domain.entity.User;
import br.com.gpi.server.domain.entity.UserGroup;
import br.com.gpi.server.domain.entity.UsuarioExterno;
import br.com.gpi.server.domain.entity.UsuarioInterno;
import java.security.Principal;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.Length;

/**
 *
 * @author <a href="mailto:pviniciusfm@gmail.com">Paulo Vinicius F. Machado</a>
 */
@XmlRootElement
public class SecurityUser implements Principal {

    private Long id;

    @Length(max = 50)
    private String name;

    @NotNull
    @Email
    private String email;
    private Integer tipo;

    private Long empresaId;

    private Long unidadeId;

    private Boolean ativo;

    private GrupoUsuario role;

    private Departamento departamento;

    public SecurityUser() {
    }

    public SecurityUser(Long userId) {
        this.id = userId;
    }

    public SecurityUser(User user) {
        this.id = user.getId();
        this.email = user.getEmail();
        this.name = user.getNome();
        this.tipo = user.getTipo();
        this.role = user.getRole();

        this.ativo = user.isAtivo();

        if (user instanceof UsuarioExterno) {
            this.empresaId = ((UsuarioExterno) user).getEmpresaId();
            this.unidadeId = ((UsuarioExterno) user).getUnidadeId();
        } else {
            this.departamento = ((UsuarioInterno) user).getDepartamento();
        }
    }

    public GrupoUsuario getRole() {
        return role;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Long getId() {
        return id;
    }

    public Integer getTipo() {
        return tipo;
    }

    public void setTipo(Integer tipo) {
        this.tipo = tipo;
    }

    public Long getEmpresaId() {
        return empresaId;
    }

    public void setEmpresaId(Long empresaId) {
        this.empresaId = empresaId;
    }

    public Long getUnidadeId() {
        return unidadeId;
    }

    public void setUnidadeId(Long unidadeId) {
        this.unidadeId = unidadeId;
    }

    public Boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(Boolean ativo) {
        this.ativo = ativo;
    }

    public Boolean isFromUnidade() {
        return this.getUnidadeId() != null;
    }

    public Departamento getDepartamento() {
        return departamento;
    }

    public void setDepartamento(Departamento departamento) {
        this.departamento = departamento;
    }

    @Override
    public String getName() {
        return this.name;
    }

    public boolean hasRole(UserGroup role) {
        return this.getRole().getDescricao().equals(role.getDescricao());
    }

    public void setRole(GrupoUsuario role) {
        this.role = role;
    }

}
