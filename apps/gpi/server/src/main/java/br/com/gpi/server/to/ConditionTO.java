package br.com.gpi.server.to;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ConditionTO {
    
    private String[] values;
    private String conditionKey;
    private ConditionTO dependency;
    
    public ConditionTO () {
        
    }
    public ConditionTO(String[] values, String conditionKey) {
        this(values, conditionKey, null);
        
    }
    
    public ConditionTO(String[] values, String conditionKey, ConditionTO dependency) {
        this.values = values;
        this.conditionKey = conditionKey;
        this.dependency = dependency;
    }

    public String[] getValues() {
        return values;
    }

    public void setValues(String[] values) {
        this.values = values;
    }

    public String getConditionKey() {
        return conditionKey;
    }

    public void setConditionKey(String conditionKey) {
        this.conditionKey = conditionKey;
    }

    public ConditionTO getDependency() {
        return dependency;
    }

    public void setDependency(ConditionTO dependency) {
        this.dependency = dependency;
    }
}
