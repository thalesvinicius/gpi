package br.com.gpi.server.exception;

import br.com.gpi.server.core.exception.BaseWebApplicationException;

public class EmpresaAlreadyExistsException extends BaseWebApplicationException {

    public EmpresaAlreadyExistsException() {
        super(409, "40901", "MSG79_EMPRESA_CADASTRADA", "An attempt was made to request contact for a empresa that already exists");
    }

}
