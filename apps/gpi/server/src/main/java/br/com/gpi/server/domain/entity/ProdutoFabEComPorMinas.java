package br.com.gpi.server.domain.entity;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import org.hibernate.envers.Audited;

@Audited
@Entity
@DiscriminatorValue(Produto.PRODUTO_FAB_E_COM_POR_MINAS)
public class ProdutoFabEComPorMinas extends Produto {
    
}
