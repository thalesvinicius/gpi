package br.com.gpi.server.endpoint;

import br.com.gpi.server.domain.entity.InsumoProduto;
import br.com.gpi.server.domain.repository.InsumoProdutoRepository;
import br.com.gpi.server.domain.service.InsumoProdutoService;
import br.com.gpi.server.domain.user.model.SecurityUser;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.inject.Inject;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;
import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

@Path("/projeto/{idProjeto}/insumoProduto")
@Stateless
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
@TransactionManagement(TransactionManagementType.BEAN)
public class InsumoProdutoEndpoint {

    @PathParam("idProjeto")
    private Long idProjeto;
    
    @Inject
    private InsumoProdutoService insumoProdutoService;
    
    @Inject
    private InsumoProdutoRepository insumoProdutoRepository;

    @Inject
    private UserTransaction tx;

    @GET
    public Response findByProjetoId(@PathParam("idProjeto") Long id) {
        InsumoProduto result = insumoProdutoRepository.findByProjetoId(id);
        return Response.ok(result).build();
    }

    @POST
    public Response save(@Valid InsumoProduto insumoProduto, @Context SecurityContext securityContext, @PathParam("idProjeto") Long idProjeto) {
        try {
            SecurityUser loggedUser = (SecurityUser) securityContext.getUserPrincipal();
            Long id = insumoProdutoService.saveInsumoProduto(insumoProduto, loggedUser.getId(), idProjeto);
            return Response.ok(id).build();
        } catch (Exception ex) {
            Logger.getLogger(InsumoProdutoEndpoint.class.getName()).log(Level.SEVERE, null, ex);
            throw new RuntimeException(ex);
        }
    }

    @PUT
    @Path("{id}")
    public Response edit(@Valid InsumoProduto insumoProduto, @Context SecurityContext securityContext, @PathParam("idProjeto") Long idProjeto) {
        return this.save(insumoProduto, securityContext, idProjeto);
    }

    @GET
    @Path("findChange")
    public Response findChange() {
        Object insumoProduto = null;
        try {
            insumoProduto = insumoProdutoService.verifyChange(idProjeto);
        } catch (SystemException ex) {
            Logger.getLogger(InsumoProdutoEndpoint.class.getName()).log(Level.SEVERE, null, ex);
        }
        return Response.ok(insumoProduto).build();
    }
}
