package br.com.gpi.server.domain.entity;

import br.com.gpi.server.domain.common.BaseDomainTableEntity;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import org.hibernate.envers.Audited;

/**
 *
 * @author lucasgom
 */
@Audited
@Entity
@Table(name = "NaturezaJuridica")
public class NaturezaJuridica extends BaseDomainTableEntity {

    @NotNull
    @Column(name = "descricao", length = 50, nullable = false)
    private String descricao;

    @Override
    public String getDescricao() {
        return this.descricao;
    }

    @Override
    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

}
