package br.com.gpi.server.endpoint;

import br.com.gpi.server.domain.entity.CadeiaProdutiva;
import br.com.gpi.server.domain.repository.CadeiaProdutivaRepository;
import java.util.List;
import javax.annotation.security.PermitAll;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/cadeiaProdutiva")
@Stateless
@PermitAll
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
@TransactionManagement(TransactionManagementType.BEAN)
public class CadeiaProdutivaEndpoint {

    @Inject
    private CadeiaProdutivaRepository cadeiaProdutivaRepository;

    @GET
    public Response findByFilters(@QueryParam("descricao") String descricao,
            @QueryParam("departamento") Integer departamentoId) {
        List<CadeiaProdutiva> result = cadeiaProdutivaRepository.findByFilters(descricao, departamentoId);
        return Response.ok(result).build();
    }
    
    @GET()
    @Path("byDepartamentos")
    public Response findByDepartamentos(@QueryParam("departamentos") List<Long> departamentos) {
        List<CadeiaProdutiva> result = cadeiaProdutivaRepository.findByDepartamentos(departamentos);
        return Response.ok(result).build();
    }
    
    

}
