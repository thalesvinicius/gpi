package br.com.gpi.server.endpoint;

import br.com.gpi.server.domain.entity.InstrumentoFormalizacao;
import br.com.gpi.server.domain.entity.SituacaoInstrFormalizacao;
import br.com.gpi.server.domain.entity.TipoInstrFormalizacao;
import br.com.gpi.server.domain.repository.InstrumentoFormalizacaoRepository;
import br.com.gpi.server.domain.service.InstrumentoFormalizacaoService;
import br.com.gpi.server.domain.service.reports.nt.NotaTecnicaReportService;
import br.com.gpi.server.domain.user.model.SecurityUser;
import br.com.gpi.server.to.InstrumentoFormalizacaoTO;
import br.com.gpi.server.to.ProjetoTO;
import java.io.ByteArrayInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Collection;
import java.util.List;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.inject.Inject;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import net.sf.jasperreports.engine.JRException;

@Path("/instrumento")
@Stateless
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
@TransactionManagement(TransactionManagementType.BEAN)
public class InstrumentoFormalizacaoEndpoint {

    @Inject
    private InstrumentoFormalizacaoService instrFormalizacaoService;

    @Inject
    private InstrumentoFormalizacaoRepository formalizacaoRepository;

    @Inject
    private NotaTecnicaReportService notaTecnicaReportService;

    @GET
    public Collection<InstrumentoFormalizacaoTO> findByFilters(@QueryParam("tipoInstrumento") TipoInstrFormalizacao tipoInstrFormalizacao,
            @QueryParam("situacao") Integer situacaoInstrumentoFormalizacaoId,
            @QueryParam("empresa") String nomeEmpresa,
            @QueryParam("protocolo") String protocolo,
            @QueryParam("titulo") String tituloInstrumento,
            @QueryParam("nomeProjeto") String nomeProjeto) {
        return instrFormalizacaoService.findByFilters(tipoInstrFormalizacao, situacaoInstrumentoFormalizacaoId, nomeEmpresa, protocolo, tituloInstrumento, nomeProjeto);
    }

    @GET
    @Path("{id}")
    public InstrumentoFormalizacao findById(@PathParam("id") Long id) {
        return instrFormalizacaoService.locateByIdForEdition(id);
    }

    @GET
    @Path("findTOById/{id}")
    public InstrumentoFormalizacaoTO findTOById(@PathParam("id") Long id) {
        return instrFormalizacaoService.findTOById(id);
    }

    @GET
    @Path("original")
    public InstrumentoFormalizacao findByProcoloVersaoOriginal(@QueryParam("protocolo") String protocolo) {
        return instrFormalizacaoService.locateProcoloVersaoOriginal(protocolo);
    }
    
    @GET
    @Path("originalProjetosAtuais")
    public Response findByProcoloVersaoOriginalProjetosAtuais(@QueryParam("protocolo") String protocolo, @QueryParam("instrumentoId") Long instrumentoId) {
        InstrumentoFormalizacao instrOriginal = instrFormalizacaoService.locateByProcoloVersaoOriginalProjetosAtuais(protocolo);
        if (instrOriginal == null) {
            return Response.status(HttpServletResponse.SC_BAD_REQUEST).entity("message.error.MSG46_CODIGO_PROTOCOLO_INVALIDO").build();
        }
        
        Long numTermosAditivos = new Long(0L);
        Integer situacaoId = SituacaoInstrFormalizacao.EM_NEGOCIACAO.getId();
        if (instrumentoId != null) {
            numTermosAditivos = formalizacaoRepository.countByProtocoloAndTipoAndSituacaoAtualAndIdNotEquals(protocolo, TipoInstrFormalizacao.TERMO_ADITIVO, situacaoId, instrumentoId);
        }else {
            numTermosAditivos = formalizacaoRepository.countByProtocoloAndTipoAndSituacaoAtual(protocolo, TipoInstrFormalizacao.TERMO_ADITIVO, situacaoId);            
        }
        
        if (numTermosAditivos > 0) {
            return Response.status(HttpServletResponse.SC_BAD_REQUEST).entity("message.error.MSG64_TERMO_ADITIVO_NÃO_PERMITIDO").build();
        }
        
        return Response.ok(instrOriginal).build();
    }

    @GET
    @Path("recoverSituacaoById/{id}")
    public Response recoverSituacaoById(@PathParam("id") Long id) {        
        return Response.ok(SituacaoInstrFormalizacao.getSituacaoInstrFormalizacao(formalizacaoRepository.recoverSituacaoById(id))).build();
    }

    @GET
    @Path("recoverTipoById/{id}")
    public Response recoverTipoById(@PathParam("id") Long id) {
        return Response.ok(formalizacaoRepository.recoverTipoById(id)).build();
    }

    @GET
    @Path("recoverCodigoProtocoloById/{id}")
    public Response recoverCodigoProtocoloById(@PathParam("id") Long id) {
        return Response.ok(formalizacaoRepository.recoverCodigoProtocoloById(id)).build();
    }

    @POST
    public Response saveOrUpdateInstrumentoFormalizacao(InstrumentoFormalizacao instrumentoFormalizacao, @Context SecurityContext securityContext) {
        SecurityUser loggedUser = (SecurityUser) securityContext.getUserPrincipal();
        Long savedInstrumentoFormalizacaoId = instrFormalizacaoService.saveInstrumentoFormalizacao(instrumentoFormalizacao, loggedUser.getId());
        return Response.ok(savedInstrumentoFormalizacaoId).build();
    }

    @GET
    @Path("report")
    @Produces("application/vnd.openxmlformats-officedocument.wordprocessingml.document")
    public Response findByIdToReport(@QueryParam("id") Long idInstrFormalizacao) throws JRException, FileNotFoundException, JRException, JRException, SQLException, IOException, Exception {
        ByteArrayInputStream reportInputStream = new ByteArrayInputStream(notaTecnicaReportService.gerarNotaTecnica(idInstrFormalizacao));
        String filename = "NotaTecnica.docx";
        return Response.ok()
                .entity(reportInputStream)
                .header("Content-Disposition", "attachment; filename='" + filename + "'")
                .build();
    }

    @GET
    @Path("avaibleProjects/unidade")
    public Response findAvaibleProjectsByUnidadeForInclude(@QueryParam("idUnidade") List<Long> idUnidades) throws JRException, FileNotFoundException, JRException, JRException, SQLException, IOException, Exception {
        List<ProjetoTO> locatedProjects = instrFormalizacaoService.locateAvaibleProjectsByUnidade(idUnidades);
        return Response.ok()
                .entity(locatedProjects)
                .build();
    }

    @GET
    @Path("avaibleProjects/empresa")
    public Response findAvaibleProjectsByEmpresaForInclude(@QueryParam("idEmpresa") List<Long> idEmpresas) throws JRException, FileNotFoundException, JRException, JRException, SQLException, IOException, Exception {
        List<ProjetoTO> locatedProjects = instrFormalizacaoService.locateAvaibleProjectsByEmpresa(idEmpresas);
        return Response.ok()
                .entity(locatedProjects)
                .build();
    }
}