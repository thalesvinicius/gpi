package br.com.gpi.server.domain.service;

import br.com.gpi.server.domain.common.BaseService;
import br.com.gpi.server.domain.entity.Cronograma;
import br.com.gpi.server.domain.entity.DescricaoFaseEnum;
import br.com.gpi.server.domain.entity.Emprego;
import br.com.gpi.server.domain.entity.Estagio;
import br.com.gpi.server.domain.entity.EstagioProjeto;
import br.com.gpi.server.domain.entity.FaturamentoPrevisto;
import br.com.gpi.server.domain.entity.Financeiro;
import br.com.gpi.server.domain.entity.InvestimentoPrevisto;
import br.com.gpi.server.domain.entity.Projeto;
import br.com.gpi.server.domain.entity.Projeto_;
import br.com.gpi.server.domain.entity.RelatorioAcompanhamento;
import br.com.gpi.server.domain.entity.RelatorioAcompanhamento_;
import br.com.gpi.server.domain.entity.SituacaoRelatorioEnum;
import br.com.gpi.server.domain.entity.TipoEmprego;
import br.com.gpi.server.domain.entity.User;
import br.com.gpi.server.domain.entity.enumerated.EnumCadeiaProdutiva;
import br.com.gpi.server.domain.entity.enumerated.EnumTipoFaturamento;
import br.com.gpi.server.domain.entity.enumerated.EnumTipoInvestimento;
import br.com.gpi.server.domain.user.model.SecurityUser;
import br.com.gpi.server.helper.FinanceiroHelper;
import br.com.gpi.server.to.AcompanhamentoAnualTO;
import br.com.gpi.server.to.CalendarioAcompanhamentoAnualTO;
import br.com.gpi.server.to.EmpregoAcompanhamentoAnualTO;
import br.com.gpi.server.to.FaturamentoAcompanhamentoAnualTO;
import br.com.gpi.server.to.InvestimentoAcompanhamentoAnualTO;
import br.com.gpi.server.to.RelatorioAcompanhamentoAnualTO;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.transaction.NotSupportedException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;
import javax.ws.rs.core.SecurityContext;

public class RelatorioAcompanhamentoAnualService extends BaseService<RelatorioAcompanhamento> {

    @Inject
    ProjetoService projetoService;

    @Inject
    private UserTransaction userTransaction;

    @Inject
    CronogramaService cronogramaService;

    @Inject
    private EmailService emailService;

    @Inject
    public RelatorioAcompanhamentoAnualService(EntityManager em) {
        super(RelatorioAcompanhamento.class, em);
    }

    public List<RelatorioAcompanhamentoAnualTO> findAllRelatorioAcompanhamentoByFamiliaIdAndAnoLessThan(Long familiaId, Integer ano) {
        CriteriaBuilder builder = getEm().getCriteriaBuilder();
        CriteriaQuery<RelatorioAcompanhamentoAnualTO> query = builder.createQuery(RelatorioAcompanhamentoAnualTO.class);
        Root<RelatorioAcompanhamento> fromRelatorioAcompanhamento = query.from(RelatorioAcompanhamento.class);
        List<Predicate> conditions = new ArrayList();

        if (ano != null) {
            conditions.add(builder.lessThan(fromRelatorioAcompanhamento.get(RelatorioAcompanhamento_.ano), ano));
        }

        if (familiaId != null) {
            Join<RelatorioAcompanhamento, Projeto> joinProjeto = fromRelatorioAcompanhamento.join(RelatorioAcompanhamento_.projeto);
            conditions.add(builder.equal(joinProjeto.get(Projeto_.versaoPai), familiaId));
        }

        TypedQuery<RelatorioAcompanhamentoAnualTO> typedQuery = getEm().createQuery(query
                .multiselect(fromRelatorioAcompanhamento.get(RelatorioAcompanhamento_.id),
                        fromRelatorioAcompanhamento.get(RelatorioAcompanhamento_.ano),
                        fromRelatorioAcompanhamento.get(RelatorioAcompanhamento_.nomeResponsavelRelatorio),
                        fromRelatorioAcompanhamento.get(RelatorioAcompanhamento_.telefoneResponsavelRelatorio),
                        fromRelatorioAcompanhamento.get(RelatorioAcompanhamento_.emailResponsavelRelatorio),
                        fromRelatorioAcompanhamento.get(RelatorioAcompanhamento_.observacao))
                .where(conditions.toArray(new Predicate[]{}))
                .orderBy(builder.desc(fromRelatorioAcompanhamento.get(RelatorioAcompanhamento_.ano)))
        );

        return typedQuery.getResultList();
    }

    public AcompanhamentoAnualTO mountRelatorioAcompanhamentoTO(Long familiaId, Long projetoId) throws Exception {
        AcompanhamentoAnualTO acompanhamento = new AcompanhamentoAnualTO();

        Projeto prjVCorrente = new Projeto();
        prjVCorrente = getEm().find(Projeto.class, projetoId);

        if (prjVCorrente.getRelatoriosAcompanhamento().isEmpty()) {
            throw new Exception("Não há relatórios associados ao projeto " + projetoId);
        }

        Financeiro finVCorrente = new Financeiro();
        Set<Emprego> empVCorrente = new HashSet<>();
        Cronograma croVCorrente = new Cronograma();
        
        if (prjVCorrente.getFinanceiro() != null) {
            finVCorrente = prjVCorrente.getFinanceiro();
        }
        if (prjVCorrente.getEmpregos() != null) {
            empVCorrente = prjVCorrente.getEmpregos();
        }
        if (prjVCorrente.getCronogramas() != null && !prjVCorrente.getCronogramas().isEmpty()) {
            croVCorrente = prjVCorrente.getCronogramas().stream().findFirst().get();
        }

        if (prjVCorrente.getEmpresa().getResponsavel() != null) {
            acompanhamento.setUsuarioExternoResponsavelId(prjVCorrente.getEmpresa().getResponsavel().getId());
        }

        acompanhamento.setVersaoCorrenteProjetoId(prjVCorrente.getId());

        Date dataAtual = new Date();
        RelatorioAcompanhamento r = prjVCorrente.getRelatoriosAcompanhamento().stream().max(Comparator.comparing(i -> i.getAno())).get();
        acompanhamento.setRelatorioAcompanhamento(r);

        Projeto prjVCongelada = projetoService.findProjetoVersaoCongeladaByFamiliaId(familiaId);

        Financeiro finVCongelada = new Financeiro();
        Set<Emprego> empVCongelada = new HashSet<>();
        Cronograma croVCongelada = new Cronograma();
        
        if (prjVCongelada != null) {
            if (prjVCongelada.getFinanceiro() != null) {
                finVCongelada = prjVCongelada.getFinanceiro();
            }
            if (prjVCongelada.getEmpregos() != null) {
                empVCongelada = prjVCongelada.getEmpregos();
            }
            if (prjVCongelada.getCronogramas() != null && !prjVCongelada.getCronogramas().isEmpty()) {
                croVCongelada = prjVCongelada.getCronogramas().stream().findFirst().get();
            }

            acompanhamento.setVersaoCongeladaProjetoId(prjVCongelada.getId());
        }

        setDadosFinanceirosInAcompanhamento(finVCongelada, finVCorrente, acompanhamento, prjVCorrente.getCadeiaProdutiva().getId());
        setDadosEmpregosInAcompanhamento(empVCongelada, empVCorrente, acompanhamento);
        setDadosCronogramaInAcompanhamento(croVCongelada, prjVCorrente, acompanhamento);
            
        return acompanhamento;
    }

    public void setDadosFinanceirosInAcompanhamento(Financeiro finVCongelada, Financeiro finVCorrente, AcompanhamentoAnualTO acompanhamento, Long cadeiaProdutivaId) {

        List tipoFatCadeiaProdutiva = new ArrayList<>();

        if (cadeiaProdutivaId.compareTo(EnumCadeiaProdutiva.ELETRICO_ELETROELETRONICOS.getId().longValue()) == 0) {
            tipoFatCadeiaProdutiva = FinanceiroHelper.fatEletroeletronico;
        } else {
            tipoFatCadeiaProdutiva = FinanceiroHelper.fatExcetoEletroeletronico;
        }

        FaturamentoAcompanhamentoAnualTO faturamento = null;
        Map<Integer, FaturamentoAcompanhamentoAnualTO> faturamentoPrevistoAnoList = new HashMap<Integer, FaturamentoAcompanhamentoAnualTO>();

        if (finVCongelada.getFaturamentoPrevistoList() != null) {
            for (FaturamentoPrevisto fat : finVCongelada.getFaturamentoPrevistoList()) {
                if (tipoFatCadeiaProdutiva.contains(fat.getTipoFaturamento())) {
                    if (faturamentoPrevistoAnoList.get(fat.getAno()) == null) {
                        faturamento = new FaturamentoAcompanhamentoAnualTO();
                        faturamento.setAno(fat.getAno());
                        faturamento.setValorPrevisto(fat.getValor());
                        faturamentoPrevistoAnoList.put(fat.getAno(), faturamento);
                    } else {
                        faturamento = faturamentoPrevistoAnoList.get(fat.getAno());
                        faturamento.setValorPrevisto(faturamento.getValorPrevisto() + fat.getValor());
                        faturamentoPrevistoAnoList.put(fat.getAno(), faturamento);
                    }
                }
            }
        }

        if (finVCorrente.getFaturamentoPrevistoList() != null) {
            for (FaturamentoPrevisto f : finVCorrente.getFaturamentoPrevistoList()) {
                if (f.getTipoFaturamento().equals(EnumTipoFaturamento.FATURAMENTO_TOTAL_REALIZADO)) {
                    if (faturamentoPrevistoAnoList.get(f.getAno()) == null) {
                        faturamento = new FaturamentoAcompanhamentoAnualTO();
                        faturamento.setAno(f.getAno());
                        faturamento.setValorPrevisto(0D);
                    } else {
                        faturamento = faturamentoPrevistoAnoList.get(f.getAno());
                    }

                    faturamento.setId(f.getId());
                    faturamento.setValorRealizado(f.getValorRealizado());
                    faturamentoPrevistoAnoList.put(faturamento.getAno(), faturamento);
                }
            }
        }

        acompanhamento.setFaturamentos(new ArrayList(faturamentoPrevistoAnoList.values()));

        List tipoInvCadeiaProdutiva = new ArrayList<>();

        if (cadeiaProdutivaId.compareTo(EnumCadeiaProdutiva.SUCROENERGETICO.getId().longValue()) == 0) {
            tipoInvCadeiaProdutiva = FinanceiroHelper.invSucroenergetico;
        } else {
            tipoInvCadeiaProdutiva = FinanceiroHelper.invExcetoSucroenergetico;
        }

        InvestimentoAcompanhamentoAnualTO investimento = null;
        Map<Integer, InvestimentoAcompanhamentoAnualTO> investimentoPrevistoAnoList = new HashMap<Integer, InvestimentoAcompanhamentoAnualTO>();

        if (finVCongelada.getInvestimentoPrevistoList() != null) {
            for (InvestimentoPrevisto inv : finVCongelada.getInvestimentoPrevistoList()) {
                if (tipoInvCadeiaProdutiva.contains(inv.getTipoInvestimento())) {
                    if (investimentoPrevistoAnoList.get(inv.getAno()) == null) {
                        investimento = new InvestimentoAcompanhamentoAnualTO();
                        investimento.setAno(inv.getAno());
                        investimento.setValorPrevisto(inv.getValor());
                        investimentoPrevistoAnoList.put(inv.getAno(), investimento);
                    } else {
                        investimento = investimentoPrevistoAnoList.get(inv.getAno());
                        investimento.setValorPrevisto(investimento.getValorPrevisto() + inv.getValor());
                        investimentoPrevistoAnoList.put(inv.getAno(), investimento);
                    }
                }
            }
        }

        if (finVCorrente.getInvestimentoPrevistoList() != null) {
            for (InvestimentoPrevisto i : finVCorrente.getInvestimentoPrevistoList()) {
                if (i.getTipoInvestimento().equals(EnumTipoInvestimento.INVESTIMENTO_TOTAL_REALIZADO)) {
                    if (investimentoPrevistoAnoList.get(i.getAno()) == null) {
                        investimento = new InvestimentoAcompanhamentoAnualTO();
                        investimento.setAno(i.getAno());
                        investimento.setValorPrevisto(0D);
                    } else {
                        investimento = investimentoPrevistoAnoList.get(i.getAno());
                    }

                    investimento.setId(i.getId());
                    investimento.setValorRealizado(i.getValorRealizado());
                    investimentoPrevistoAnoList.put(investimento.getAno(), investimento);
                }
            }
        }

        acompanhamento.setInvestimentos(new ArrayList(investimentoPrevistoAnoList.values()));
    }

    public void setDadosEmpregosInAcompanhamento(Set<Emprego> empVCongelada, Set<Emprego> empVCorrente, AcompanhamentoAnualTO acompanhamento) {
        List tiposEmpPermanentes = new ArrayList<>();
        tiposEmpPermanentes.add(TipoEmprego.PERMANENTE);
        tiposEmpPermanentes.add(TipoEmprego.PERMANENTE_MATURACAO_AGRICOLA);
        tiposEmpPermanentes.add(TipoEmprego.PERMANENTE_MATURACAO_INDUSTRIAL);
        tiposEmpPermanentes.add(TipoEmprego.PERMANENTE_OPERACAO);

        EmpregoAcompanhamentoAnualTO emprego = null;
        Map<Integer, EmpregoAcompanhamentoAnualTO> empregoAnoList = new HashMap<Integer, EmpregoAcompanhamentoAnualTO>();

        if (empVCongelada != null) {
            for (Emprego emp : empVCongelada) {
                if (tiposEmpPermanentes.contains(emp.getTipoEmprego())) {
                    if (empregoAnoList.get(emp.getAno()) == null) {
                        emprego = new EmpregoAcompanhamentoAnualTO();
                        emprego.setAno(emp.getAno());
                        emprego.setNumeroPrevisto(emp.getDireto());
                        empregoAnoList.put(emp.getAno(), emprego);
                    } else {
                        emprego = empregoAnoList.get(emp.getAno());
                        emprego.setNumeroPrevisto(emprego.getNumeroPrevisto() + emp.getDireto());
                        empregoAnoList.put(emp.getAno(), emprego);
                    }
                }
            }
        }

        if (empVCorrente != null) {
            for (Emprego e : empVCorrente) {
                if (e.getTipoEmprego().equals(TipoEmprego.PERMANENTE_TOTAL_REALIZADO)) {
                    if (empregoAnoList.get(e.getAno()) == null) {
                        emprego = new EmpregoAcompanhamentoAnualTO();
                        emprego.setAno(e.getAno());
                        emprego.setNumeroPrevisto(0);
                    } else {
                        emprego = empregoAnoList.get(e.getAno());
                    }

                    emprego.setId(e.getId());
                    emprego.setNumeroRealizado(e.getDiretoRealizado());

                    empregoAnoList.put(emprego.getAno(), emprego);
                }
            }
        }

        acompanhamento.setEmpregos(new ArrayList(empregoAnoList.values()));
    }

    public void setDadosCronogramaInAcompanhamento(Cronograma croVCongelada, Projeto prjVCorrente, AcompanhamentoAnualTO acompanhamento) {
        CalendarioAcompanhamentoAnualTO calFormalizada = new CalendarioAcompanhamentoAnualTO();
        calFormalizada.setDescricaoFaseEnum(DescricaoFaseEnum.DECISAO_FORMALIZADA);

        CalendarioAcompanhamentoAnualTO calInicioOperacao = new CalendarioAcompanhamentoAnualTO();
        calInicioOperacao.setDescricaoFaseEnum(DescricaoFaseEnum.INICIO_OPERACAO);
        calInicioOperacao.setDataPrevisto(croVCongelada.getInicioOperacao());

        CalendarioAcompanhamentoAnualTO calInicioImplantacao = new CalendarioAcompanhamentoAnualTO();
        calInicioImplantacao.setDescricaoFaseEnum(DescricaoFaseEnum.INICIO_IMPLANTACAO);

        if (prjVCorrente.getCadeiaProdutiva() != null
                && prjVCorrente.getCadeiaProdutiva().getId().compareTo(EnumCadeiaProdutiva.SUCROENERGETICO.getId().longValue()) == 0) {
            calInicioImplantacao.setDataPrevisto(croVCongelada.getInicioImplantacaoAgricola());
        } else {
            calInicioImplantacao.setDataPrevisto(croVCongelada.getInicioImplantacao());
        }

        CalendarioAcompanhamentoAnualTO calTributario = new CalendarioAcompanhamentoAnualTO();
        calTributario.setDescricaoFaseEnum(DescricaoFaseEnum.INICIO_TRATAMENTO_TRIBUTARIO);

        if (prjVCorrente.getCronogramas() != null && !prjVCorrente.getCronogramas().isEmpty()) {
            calTributario.setDataPrevisto(prjVCorrente.getCronogramas().stream().findFirst().get().getInicioTratamentoTributarioPrevisto());
            calTributario.setDataRealizado(prjVCorrente.getCronogramas().stream().findFirst().get().getInicioTratamentoTributarioRealizado());
            calTributario.setId(prjVCorrente.getCronogramas().stream().findFirst().get().getId());
        }

        if (prjVCorrente.getEstagios() != null) {
            for (EstagioProjeto e : prjVCorrente.getEstagios()) {
                if (e.getEstagioId() == Estagio.FORMALIZADA.getId().longValue()) {
                    calFormalizada.setDataRealizado(e.getDataInicioEstagio());
                } else if (e.getEstagioId() == Estagio.IMPLANTACAO_INICIADO.getId().longValue()) {
                    calInicioImplantacao.setDataRealizado(e.getDataInicioEstagio());
                } else if (e.getEstagioId() == Estagio.OPERACAO_INICIADA.getId().longValue()) {
                    calInicioOperacao.setDataRealizado(e.getDataInicioEstagio());
                }
            }
        }

        acompanhamento.addToCalendarios(calFormalizada);
        acompanhamento.addToCalendarios(calInicioOperacao);
        acompanhamento.addToCalendarios(calInicioImplantacao);
        acompanhamento.addToCalendarios(calTributario);
    }

    public Long sendFirstCampaign(String ano, Long projetoId, Long idUsuario, int DuracaoPrimeiraCampanhaEmDias) throws SystemException, NotSupportedException {
        Boolean gravouDados = false;
        Date dataInicioPrimeiraCampanha = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
        Calendar dataFimPrimeiraCampanha = Calendar.getInstance();
        dataFimPrimeiraCampanha.setTime(new Date());
        dataFimPrimeiraCampanha.add(Calendar.DATE, DuracaoPrimeiraCampanhaEmDias);

        Projeto prjVCorrente = projetoService.findById(projetoId);

        TypedQuery<RelatorioAcompanhamento> query = getEm().createNamedQuery(RelatorioAcompanhamento.Query.findCampanhaParaProjetoNoAno, RelatorioAcompanhamento.class);
        query.setParameter(1, prjVCorrente.getId());
        query.setParameter(2, Integer.parseInt(ano));
        List<RelatorioAcompanhamento> relatorios = query.getResultList();

        try {
            if (relatorios.isEmpty()) {
                StringBuffer stb = new StringBuffer();
                userTransaction.begin();
                this.getEm().joinTransaction();
                stb.append(" Insert Into RelatorioAcompanhamento ");
                stb.append(" (projeto_id,familia_id,ano,dataInicioPrimeiraCampanha,dataFimPrimeiraCampanha,usuarioResponsavel_id,dataUltimaAlteracao,situacaoAtual_id,termoConfiabilidade,aceiteTermo) ");
                stb.append(" Values ( " + prjVCorrente.getId() + ", " + prjVCorrente.getVersaoPai() + ", ");
                stb.append(ano + ", '");
                stb.append(sdf.format(dataInicioPrimeiraCampanha) + "', '");
                stb.append(sdf.format(dataFimPrimeiraCampanha.getTime()) + "', ");
                stb.append(idUsuario + ", '");
                stb.append(sdf.format(dataInicioPrimeiraCampanha) + "', ");
                stb.append(" 1, '.', 'false') ");
                this.getEm().createNativeQuery(stb.toString()).executeUpdate();
                userTransaction.commit();
                gravouDados = true;
            }
        } catch (Exception ex) {
            userTransaction.rollback();
            Logger.getLogger(ProjetoService.class.getName()).log(Level.SEVERE, null, ex);
            throw new RuntimeException(ex);
        }

        emailService.sendCampaign(prjVCorrente.getEmpresa().getResponsavel().getEmail(), "0072004",
                prjVCorrente.getEmpresa().getRazaoSocial(), new Date(), ano,
                new Date(), prjVCorrente.getUsuarioInvestimento().getNome());

        if (gravouDados) {
            return 1L;
        } else {
            return 0L;
        }
    }

    public Long sendSecondCampaign(String ano, Long projetoId, Long idUsuario, int DuracaoSegundaCampanhaEmDias) throws SystemException, NotSupportedException {
        Boolean gravouDados = false;
        Date dataInicioSegundaCampanha = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
        Calendar dataFimSegundaCampanha = Calendar.getInstance();
        dataFimSegundaCampanha.setTime(new Date());
        dataFimSegundaCampanha.add(Calendar.DATE, DuracaoSegundaCampanhaEmDias);

        Projeto prjVCorrente = projetoService.findById(projetoId);

        TypedQuery<RelatorioAcompanhamento> query = getEm().createNamedQuery(RelatorioAcompanhamento.Query.findCampanhaParaProjetoNoAno, RelatorioAcompanhamento.class);
        query.setParameter(1, prjVCorrente.getId());
        query.setParameter(2, Integer.parseInt(ano));
        List<RelatorioAcompanhamento> relatorios = query.getResultList();
        try {
            if (!relatorios.isEmpty()) {
                StringBuffer stb = new StringBuffer();
                userTransaction.begin();
                this.getEm().joinTransaction();
                stb.append(" Update RelatorioAcompanhamento set dataInicioSegundaCampanha = '" + sdf.format(dataInicioSegundaCampanha) + "', ");
                stb.append(" dataFimSegundaCampanha = '" + sdf.format(dataFimSegundaCampanha.getTime()) + "' ");
                stb.append(" Where projeto_id = " + prjVCorrente.getId() + " ");
                stb.append(" And ano = " + ano);
                this.getEm().createNativeQuery(stb.toString()).executeUpdate();
                userTransaction.commit();
                gravouDados = true;
            }
        } catch (Exception ex) {
            userTransaction.rollback();
            Logger.getLogger(ProjetoService.class.getName()).log(Level.SEVERE, null, ex);
            throw new RuntimeException(ex);
        }

        emailService.sendCampaign(prjVCorrente.getEmpresa().getResponsavel().getEmail(), "0072004",
                prjVCorrente.getEmpresa().getRazaoSocial(), new Date(), ano,
                new Date(), prjVCorrente.getUsuarioInvestimento().getNome());

        if (gravouDados) {
            return 1L;
        } else {
            return 0L;
        }
    }

    public void updateRelatorioAcompanhamentoAnual(AcompanhamentoAnualTO acompanhamento, SecurityContext context) throws SystemException {

        if (acompanhamento.getRelatorioAcompanhamento() == null
                || acompanhamento.getRelatorioAcompanhamento().getId() == null) {
            throw new RuntimeException("Report ID was not found in parameters!");
        }

        try {
            userTransaction.begin();
            getEm().joinTransaction();

            Projeto projetoVCorrente = projetoService.findById(acompanhamento.getVersaoCorrenteProjetoId());

            SecurityUser loggedUserWrap = (SecurityUser) context.getUserPrincipal();
            User user = this.getEm().find(User.class, loggedUserWrap.getId());

            List<CalendarioAcompanhamentoAnualTO> cronogramas = acompanhamento.getCalendarios();

            for (CalendarioAcompanhamentoAnualTO c : cronogramas) {

                if (c.getDescricaoFaseEnum().equals(DescricaoFaseEnum.INICIO_TRATAMENTO_TRIBUTARIO)) {

                    Cronograma cronograma = new Cronograma();

                    if (c.getId() != null) {
                        cronograma = cronogramaService.findById(c.getId());
                    } else {
                        cronograma.setProjeto(projetoVCorrente);
                    }

                    cronograma.setInicioTratamentoTributarioPrevisto(c.getDataPrevisto());
                    cronograma.setInicioTratamentoTributarioRealizado(c.getDataRealizado());

                    cronogramaService.merge(cronograma);
                }

            }

            List<FaturamentoAcompanhamentoAnualTO> faturamentos = acompanhamento.getFaturamentos();

            for (FaturamentoAcompanhamentoAnualTO f : faturamentos) {

                FaturamentoPrevisto faturamento = new FaturamentoPrevisto();

                if (f.getId() != null) {
                    faturamento = getEm().find(FaturamentoPrevisto.class, f.getId());
                } else {
                    faturamento.setFinanceiro(projetoVCorrente.getFinanceiro());
                }

                faturamento.setAno(f.getAno());
                faturamento.setTipoFaturamento(EnumTipoFaturamento.FATURAMENTO_TOTAL_REALIZADO);
                faturamento.setValor(0D);
                faturamento.setValorRealizado(f.getValorRealizado());

                getEm().persist(faturamento);

            }

            List<InvestimentoAcompanhamentoAnualTO> investimentos = acompanhamento.getInvestimentos();

            for (InvestimentoAcompanhamentoAnualTO i : investimentos) {

                InvestimentoPrevisto investimento = new InvestimentoPrevisto();

                if (i.getId() != null) {
                    investimento = getEm().find(InvestimentoPrevisto.class, i.getId());
                } else {
                    investimento.setFinanceiro(projetoVCorrente.getFinanceiro());
                }

                investimento.setAno(i.getAno());
                investimento.setTipoInvestimento(EnumTipoInvestimento.INVESTIMENTO_TOTAL_REALIZADO);
                investimento.setValor(0D);
                investimento.setValorRealizado(i.getValorRealizado());

                getEm().persist(investimento);
            }

            List<EmpregoAcompanhamentoAnualTO> empregos = acompanhamento.getEmpregos();

            for (EmpregoAcompanhamentoAnualTO e : empregos) {

                Emprego emprego = new Emprego();

                if (e.getId() != null) {
                    emprego = getEm().find(Emprego.class, e.getId());
                } else {
                    emprego.setProjeto(projetoVCorrente);
                }

                emprego.setUsuarioResponsavel(user);
                emprego.setDataUltimaAlteracao(new Date());
                emprego.setAno(e.getAno());
                emprego.setTipoEmprego(TipoEmprego.PERMANENTE_TOTAL_REALIZADO);
                emprego.setDireto(0);
                emprego.setIndireto(0);
                emprego.setDiretoRealizado(e.getNumeroRealizado());

                getEm().persist(emprego);
            }

            RelatorioAcompanhamento relatorio = acompanhamento.getRelatorioAcompanhamento();
            relatorio.setProjeto(projetoVCorrente);
            relatorio.setDataUltimaAlteracao(new Date());
            relatorio.setUsuarioResponsavel(user);
            relatorio.setSituacaoAtual(SituacaoRelatorioEnum.PENDENTE_VALIDACAO);

            getEm().merge(relatorio);

            userTransaction.commit();
        } catch (Exception ex) {
            userTransaction.rollback();
            Logger.getLogger(RelatorioAcompanhamentoAnualService.class.getName()).log(Level.SEVERE, null, ex);
            throw new RuntimeException(ex);
        }

    }

    public RelatorioAcompanhamento validaRelatorio(Long relatorioId) throws SystemException {
        RelatorioAcompanhamento relatorio = null;
        try {
            userTransaction.begin();
            getEm().joinTransaction();

            relatorio = getEm().find(RelatorioAcompanhamento.class, relatorioId);
            relatorio.setDataValidacao(new Date());
            relatorio.setSituacaoAtual(SituacaoRelatorioEnum.VALIDADO);
            getEm().merge(relatorio);

            if (relatorio.isAceiteTermo() != null && relatorio.isAceiteTermo()) {
                TypedQuery<RelatorioAcompanhamento> query = getEm().createNamedQuery(RelatorioAcompanhamento.Query.findAllByProjetoVersaoPaiAndIdNotEquals, RelatorioAcompanhamento.class);
                query.setParameter(1, relatorio.getProjeto().getVersaoPai());
                query.setParameter(2, relatorio.getId());

                List<RelatorioAcompanhamento> relatorios = query.getResultList();
                for (RelatorioAcompanhamento r : relatorios) {
                    r.setAceiteTermo(Boolean.TRUE);
                    getEm().merge(r);
                }
            }
            getEm().flush();

            if (relatorio.getProjeto().getEmpresa().getResponsavel() != null && relatorio.getProjeto().getEmpresa().getResponsavel().getEmail() != null) {
                emailService.sendAnnualMonitoringValidateEmail(relatorio.getProjeto().getEmpresa().getResponsavel().getEmail());
            }
            userTransaction.commit();
        } catch (Exception ex) {
            userTransaction.rollback();
            Logger.getLogger(RelatorioAcompanhamentoAnualService.class.getName()).log(Level.SEVERE, null, ex);
            throw new RuntimeException(ex);
        }

        return relatorio;
    }

}
