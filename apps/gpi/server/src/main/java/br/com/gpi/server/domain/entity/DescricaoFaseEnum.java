package br.com.gpi.server.domain.entity;

import br.com.gpi.server.core.exception.EnumException;

public enum DescricaoFaseEnum {
    
    DECISAO_FORMALIZADA(1, "Decisão Formalizada"),
    INICIO_IMPLANTACAO(2, "Inicio de implantação do projeto"),
    INICIO_OPERACAO(3, "início de operação"),
    INICIO_TRATAMENTO_TRIBUTARIO(4, "Inicio da utilização de tratamento tributário");
        
    private final Integer id;
    private final String descricao;

    private DescricaoFaseEnum(Integer id, String descricao) {
        this.id = id;
        this.descricao = descricao;
    }

    public Integer getId() {
        return id;
    }

    public String getDescricao() {
        return descricao;
    }

    public static DescricaoFaseEnum DescricaoFaseEnum(Integer id) {
        for (DescricaoFaseEnum tipo : values()) {
            if (tipo.getId().equals(id)) {
                return tipo;
            }
        }
        throw new EnumException(String.format("Not recognized value %d for DescricaoFase enum", id));
    }            
    
}
