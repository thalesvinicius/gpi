package br.com.gpi.server.domain.common;

import br.com.gpi.server.core.exception.ValidationException;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.enterprise.inject.Alternative;
import javax.inject.Inject;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.validation.ConstraintViolation;
import javax.validation.Validator;

/**
 * Generic DAO implementation that provides helper methods for using the entity
 * repository This implementation can be extended or for a simple utilization,
 * can be used by injection with will be produced by
 *
 * @param <T> entity class
 * @author <a href="mailto:pviniciusfm@gmail.com">Paulo Vinicius F. Machado</a>
 */
@Alternative
public class BaseService<T extends BaseEntity> {

    private static final int NO_LIMIT = 0;
    private EntityManager em;
    private final Class<T> entityClass;

    /**
     * Creates a GenericDAO according with EntityManager and entity class
     * parameters
     *
     * @param entityClass
     * @param em
     */
    public BaseService(Class<T> entityClass, EntityManager em) {
        this.entityClass = entityClass;
        this.em = em;
    }

    /**
     * Insert new parameterized entity
     *
     * @param entity
     * @see EntityManager#persist(java.lang.Object)
     */
    public void persist(BaseEntity entity) {
        em.joinTransaction();
        em.persist(entity);
    }

    /**
     * Delete the parameterized entity
     *
     * @param entity
     * @see EntityManager#remove(java.lang.Object)
     */
    public void remove(Persistable entity) {
        em.joinTransaction();
        em.remove(entity);
    }

    /**
     * Updates a entity changes in database.
     *
     * @param entity
     * @return
     * @see EntityManager#merge(java.lang.Object)
     */
    public BaseEntity merge(BaseEntity entity) {
        em.joinTransaction();
        return em.merge(entity);
    }

    public Object merge(Object entity) {
        if (entity.getClass().getAnnotation(Entity.class) == null) {
            throw new RuntimeException("BaseService says: The object passed doesn't have @Entity annotation!");
        }
        em.joinTransaction();
        return em.merge(entity);
    }

    /**
     *
     * @param <E>
     * @param _class
     * @param namedQueryName
     * @param parameters
     * @param resultLimit
     * @return
     */
    public <E extends Object> TypedQuery<E> createTypedQuery(Class<E> _class, String namedQueryName, Map<String, Object> parameters, int resultLimit) {
        Set<Map.Entry<String, Object>> rawParameters = parameters.entrySet();
        TypedQuery<E> query = this.em.createNamedQuery(namedQueryName, _class);
        if (resultLimit > NO_LIMIT) {
            query.setMaxResults(resultLimit);
        }
        for (Map.Entry<String, Object> entry : rawParameters) {
            query.setParameter(entry.getKey(), entry.getValue());
        }
        return query;
    }

    /**
     *
     * @param namedQueryName
     * @param parameters
     * @param resultLimit
     * @return
     */
    public TypedQuery<T> createTypedQuery(String namedQueryName, Map<String, Object> parameters, int resultLimit) {
        return this.createTypedQuery(entityClass, namedQueryName, parameters, resultLimit);
    }

    public TypedQuery<T> createTypedQuery(String namedQueryName, Map<String, Object> parameters) {
        return createTypedQuery(namedQueryName, parameters, NO_LIMIT);
    }

    public <E extends Object> TypedQuery<E> createTypedQuery(Class<E> _class, String namedQueryName, Map<String, Object> parameters) {
        return createTypedQuery(_class, namedQueryName, parameters, NO_LIMIT);
    }

    /**
     * Return all entities in database. SELECT * FROM "EntityTable"
     *
     * @return result list of query
     */
    public Collection<T> findAll() {
        CriteriaQuery<T> query = em.getCriteriaBuilder().createQuery(entityClass);
        query.from(entityClass);
        return (Collection<T>) em.createQuery(query).getResultList();
    }

    /**
     * Find the entity with parameterized id
     *
     * @param id of sought entity
     * @return sought entity
     */
    public T findById(Object id) {
        return em.find(entityClass, id);
    }

    /**
     * Query database utilizing the parameterized named query
     *
     * @param namedQuery
     * @param parametersValues optional parameters values specified by named
     * query
     * @return result list of executed query
     */
    public List<T> findByNamedQuery(String namedQuery, Object... parametersValues) {
        return createTypedQuery(namedQuery, NO_LIMIT, parametersValues).getResultList();
    }

    public T locateByNamedQuery(String namedQuery, Map<String, Object> parameters) {
        TypedQuery<T> query = createTypedQuery(namedQuery, parameters);
        try {
            return query.getSingleResult();
        } catch (NoResultException ex) {
            return null;
        }
    }

    /**
     * Query database utilizing the parameterized named query, limiting the
     * results by the resultLimit parameter
     *
     * @param namedQuery
     * @param resultLimit max number of results
     * @param parametersValues optional parameters values specified by named
     * query
     * @return
     */
    public List<T> findByNamedQueryWithLimitedResult(String namedQuery, int resultLimit, Object... parametersValues) {
        return createTypedQuery(namedQuery, resultLimit, parametersValues).getResultList();
    }

    /**
     * Helper method for creating a named typed query with capacity to limit the
     * number of result rows
     *
     * @param <E>
     * @param _class
     * @param namedQuery id of namedQuery
     * @param resultLimit limit the number of result rows if the value is
     * greater than 0
     * @param parameters optional values of named query parameters
     * @return TypedQuery
     * @see TypedQuery
     */
    public <E extends Object> TypedQuery<E> createTypedQuery(Class<E> _class, String namedQuery, int resultLimit, Object... parameters) {
        TypedQuery<E> query = em.createNamedQuery(namedQuery, _class);
        int count = 1;
        for (Object value : parameters) {
            query.setParameter(count, value);
            count++;
        }
        if (resultLimit > NO_LIMIT) {
            query.setMaxResults(resultLimit);
        }
        return query;
    }

    @Deprecated
    public Query createUpdateDeleteQuery(String namedQuery, Object... parameters) {
        return this.createQuery(namedQuery, parameters);
    }

    public Query createQuery(String namedQuery, Object... parameters) {
        Query query = em.createNamedQuery(namedQuery);
        int count = 1;
        if (parameters != null) {
            for (Object value : parameters) {
                query.setParameter(count, value);
                count++;
            }
        }
        return query;
    }

    public TypedQuery<T> createTypedQuery(String namedQuery, int resultLimit, Object... parameters) {
        return createTypedQuery(entityClass, namedQuery, resultLimit, parameters);
    }

    /**
     * Helper method for creating a named typed query
     *
     * @param namedQuery id of namedQuery
     * @param parameters optional values of named query parameters
     * @return TypedQuery
     * @see TypedQuery
     */
    public TypedQuery<T> createTypedQuery(String namedQuery, Object... parameters) {
        return createTypedQuery(namedQuery, NO_LIMIT, parameters);
    }

    public <E extends Object> TypedQuery<E> createTypedQuery(Class<E> _class, String namedQuery, Object... parameters) {
        return createTypedQuery(_class, namedQuery, NO_LIMIT, parameters);
    }

    public List<T> findByListIDs(List<Object> ids) {
        if (ids == null || ids.isEmpty()) {
            return Collections.EMPTY_LIST;
        }

        CriteriaBuilder builder = this.getEm().getCriteriaBuilder();
        CriteriaQuery<T> query = builder.createQuery(entityClass);
        Root<T> root = query.from(entityClass);
        query.where(root.get("id").in(ids));
        return this.getEm().createQuery(query).getResultList();

    }

    /**
     * Return entity manager
     *
     * @return entityManager instance
     */
    public EntityManager getEm() {
        return em;
    }

    /**
     * Sets the Entity Manager This method is used for entity manager injection.
     *
     * @param em EntityManager
     */
    @Inject
    public void setEm(EntityManager em) {
        this.em = em;
    }

    protected void validate(Validator validator, Object request) {
        Set<? extends ConstraintViolation<?>> constraintViolations = validator.validate(request);
        if (constraintViolations.size() > 0) {
            throw new ValidationException(constraintViolations);
        }
    }

    public Class<T> getEntityClass() {
        return entityClass;
    }

}
