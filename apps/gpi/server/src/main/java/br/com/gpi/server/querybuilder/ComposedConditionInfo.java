package br.com.gpi.server.querybuilder;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ComposedConditionInfo extends ConditionInfo {

    private Map<String, Field> fieldMap;

    public ComposedConditionInfo(Class instance, ConditionValueTypeEnum type, ConditionComparisonTypeEnum comparisonType, Map<String, Field> fields) {
        super(instance, type, comparisonType);
        this.fieldMap = fields;
    }

    public ComposedConditionInfo(Class instance, ConditionValueTypeEnum type, Map<String, Field> fieldMap) {
        super(instance, type);
        this.fieldMap = fieldMap;
    }

    public Map<String, Field> getFieldMap() {
        return fieldMap;
    }

    public void setFieldMap(Map<String, Field> fieldMap) {
        this.fieldMap = fieldMap;
    }

    public List<Field> getFields(List<String> keys) {
        List<Field> fields = new ArrayList<>();
        for (String key : keys) {
            fields.add(getFieldMap().get(key));
        }
        return fields;
    }

    @Override
    public List<Field> getFields() {
        return new ArrayList<>(getFieldMap().values());
    }
}
