package br.com.gpi.server.domain.view;

import br.com.gpi.server.core.jpa.converter.TipoMeioAmbienteEnumConverter;
import br.com.gpi.server.domain.common.BaseEntity;
import br.com.gpi.server.domain.entity.Estagio;
import br.com.gpi.server.domain.entity.SituacaoProjetoEnum;
import br.com.gpi.server.domain.entity.enumerated.EnumTipoMeioAmbiente;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.Transient;

@Entity
@Table(name = "VIEW_RELATORIO_ACOMPANHAMENTO_AMBIENTAL")
public class RelatorioAcompanhamentoAmbiental extends BaseEntity<Long> implements Serializable {

    @Id
    private String nomePrincipal;
    private String projeto;
    private String cidade;
    private String regiao;
    private Long regiaoId;
    private String cadeiaProdutiva;
    private Long cadeiaProdutivaId;
    @Id
    private Integer classeEmpreendimento;
    private String etapas;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date dataEntregaEstudos;
        
    @Transient
    private transient Date expectativaEmissaoLicenca;    
    @Transient
    private transient Integer situacaoProcessoId;
    @Transient
    private transient Integer diasForaDoPrazo;
    
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date expectativaOI;
    private Estagio estagioId;
    private SituacaoProjetoEnum situacaoId;
    private String analista;
    private Long analistaId;
    private String departamento;
    private Long departamentoId;
    @Convert(converter = TipoMeioAmbienteEnumConverter.class)
    private EnumTipoMeioAmbiente etapaMeioAmbienteId;

    public RelatorioAcompanhamentoAmbiental() {
    }
    
    public RelatorioAcompanhamentoAmbiental(String nomePrincipal, String projeto, String cidade, String regiao, 
            String cadeiaProdutiva, Integer classeEmpreendimento, String etapas, Date dataEntregaEstudos, 
            Date expectativaEmissaoLicenca, Date expectativaOI, Integer situacaoProcessoId, Integer diasForaDoPrazo, 
            SituacaoProjetoEnum situacaoId, Estagio estagioId, String analista, String departamento) {
        this.nomePrincipal = nomePrincipal;
        this.projeto = projeto;
        this.cidade = cidade;
        this.regiao = regiao;
        this.cadeiaProdutiva = cadeiaProdutiva;
        this.classeEmpreendimento = classeEmpreendimento;
        this.etapas = etapas;
        this.dataEntregaEstudos = dataEntregaEstudos;
        this.expectativaEmissaoLicenca = expectativaEmissaoLicenca;
        this.expectativaOI = expectativaOI;
        this.situacaoProcessoId = situacaoProcessoId;
        this.diasForaDoPrazo = diasForaDoPrazo;
        this.situacaoId= situacaoId;
        this.estagioId = estagioId;
        this.analista = analista;
        this.departamento = departamento;
    }
    
    public RelatorioAcompanhamentoAmbiental(String nomePrincipal, String projeto, String cidade, String regiao, 
            String cadeiaProdutiva, Integer classeEmpreendimento, String etapas, Date dataEntregaEstudos, 
            Date expectativaOI, SituacaoProjetoEnum situacaoId, Estagio estagioId, String analista, String departamento) {
        this.nomePrincipal = nomePrincipal;
        this.projeto = projeto;
        this.cidade = cidade;
        this.regiao = regiao;
        this.cadeiaProdutiva = cadeiaProdutiva;
        this.classeEmpreendimento = classeEmpreendimento;
        this.etapas = etapas;
        this.dataEntregaEstudos = dataEntregaEstudos;
        this.expectativaOI = expectativaOI;
        this.situacaoId= situacaoId;
        this.estagioId = estagioId;
        this.analista = analista;
        this.departamento = departamento;
    }

    /**
     * @return the situacaoProcesso
     */
    public String getSituacaoProcesso() {
        if (situacaoProcessoId == null) {
            return null;
        }
        return situacaoProcessoId.equals(1) ? "Dentro do Prazo" : "Fora do Prazo";
    }

    public Integer getSituacaoProcessoId() {
        return situacaoProcessoId;
    }

    public void setSituacaoProcessoId(Integer situacaoProcessoId) {
        this.situacaoProcessoId = situacaoProcessoId;
    }

    /**
     * @return the projeto
     */
    public String getProjeto() {
        return projeto;
    }

    /**
     * @param projeto the projeto to set
     */
    public void setProjeto(String projeto) {
        this.projeto = projeto;
    }

    /**
     * @return the cidade
     */
    public String getCidade() {
        return cidade;
    }

    /**
     * @param cidade the cidade to set
     */
    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    /**
     * @return the regiao
     */
    public String getRegiao() {
        return regiao;
    }

    /**
     * @param regiao the regiao to set
     */
    public void setRegiao(String regiao) {
        this.regiao = regiao;
    }

    /**
     * @return the regiaoId
     */
    public Long getRegiaoId() {
        return regiaoId;
    }

    /**
     * @param regiaoId the regiaoId to set
     */
    public void setRegiaoId(Long regiaoId) {
        this.regiaoId = regiaoId;
    }

    /**
     * @return the cadeiaProdutiva
     */
    public String getCadeiaProdutiva() {
        return cadeiaProdutiva;
    }

    /**
     * @param cadeiaProdutiva the cadeiaProdutiva to set
     */
    public void setCadeiaProdutiva(String cadeiaProdutiva) {
        this.cadeiaProdutiva = cadeiaProdutiva;
    }

    /**
     * @return the cadeiaProdutivaId
     */
    public Long getCadeiaProdutivaId() {
        return cadeiaProdutivaId;
    }

    /**
     * @param cadeiaProdutivaId the cadeiaProdutivaId to set
     */
    public void setCadeiaProdutivaId(Long cadeiaProdutivaId) {
        this.cadeiaProdutivaId = cadeiaProdutivaId;
    }

    /**
     * @return the classeEmpreendimento
     */
    public Integer getClasseEmpreendimento() {
        return classeEmpreendimento;
    }

    /**
     * @param classeEmpreendimento the classeEmpreendimento to set
     */
    public void setClasseEmpreendimento(Integer classeEmpreendimento) {
        this.classeEmpreendimento = classeEmpreendimento;
    }

    /**
     * @return the etapas
     */
    public String getEtapas() {
        return etapas;
    }

    /**
     * @param etapas the etapas to set
     */
    public void setEtapas(String etapas) {
        this.etapas = etapas;
    }

    /**
     * @return the dataEntregaEstudos
     */
    public Date getDataEntregaEstudos() {
        return dataEntregaEstudos;
    }

    /**
     * @param dataEntregaEstudos the dataEntregaEstudos to set
     */
    public void setDataEntregaEstudos(Date dataEntregaEstudos) {
        this.dataEntregaEstudos = dataEntregaEstudos;
    }

    /**
     * @return the expectativaEmissaoLicenca
     */
    public Date getExpectativaEmissaoLicenca() {
        return expectativaEmissaoLicenca;
    }

    /**
     * @param expectativaEmissaoLicenca the expectativaEmissaoLicenca to set
     */
    public void setExpectativaEmissaoLicenca(Date expectativaEmissaoLicenca) {
        this.expectativaEmissaoLicenca = expectativaEmissaoLicenca;
    }

    /**
     * @return the expectativaOI
     */
    public Date getExpectativaOI() {
        return expectativaOI;
    }

    /**
     * @param expectativaOI the expectativaOI to set
     */
    public void setExpectativaOI(Date expectativaOI) {
        this.expectativaOI = expectativaOI;
    }

    /**
     * @return the estagioAtual
     */
    public String getEstagioAtual() {
        if(estagioId == null){
            return null;
        }
        return estagioId.getDescricao();
    }

    /**
     * @return the estagioId
     */
    public Estagio getEstagioId() {
        return estagioId;
    }

    /**
     * @param estagioId the estagioId to set
     */
    public void setEstagioId(Estagio estagioId) {
        this.estagioId = estagioId;
    }

    /**
     * @return the situacaoAtual
     */
    public String getSituacaoAtual() {
        if (situacaoId == null) {
            return null;
        }
        return situacaoId.getDescription();
    }

    /**
     * @return the situacaoId
     */
    public SituacaoProjetoEnum getSituacaoId() {
        return situacaoId;
    }

    /**
     * @param situacaoId the situacaoId to set
     */
    public void setSituacaoId(SituacaoProjetoEnum situacaoId) {
        this.situacaoId = situacaoId;
    }

    /**
     * @return the analista
     */
    public String getAnalista() {
        return analista;
    }

    /**
     * @param analista the analista to set
     */
    public void setAnalista(String analista) {
        this.analista = analista;
    }

    /**
     * @return the analistaId
     */
    public Long getAnalistaId() {
        return analistaId;
    }

    /**
     * @param analistaId the analistaId to set
     */
    public void setAnalistaId(Long analistaId) {
        this.analistaId = analistaId;
    }

    /**
     * @return the departamento
     */
    public String getDepartamento() {
        return departamento;
    }

    /**
     * @param departamento the departamento to set
     */
    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    /**
     * @return the departamentoId
     */
    public Long getDepartamentoId() {
        return departamentoId;
    }

    /**
     * @param departamentoId the departamentoId to set
     */
    public void setDepartamentoId(Long departamentoId) {
        this.departamentoId = departamentoId;
    }

    /**
     * @return the etapaMeioAmbienteId
     */
    public EnumTipoMeioAmbiente getEtapaMeioAmbienteId() {
        return etapaMeioAmbienteId;
    }

    /**
     * @param etapaMeioAmbienteId the etapaMeioAmbienteId to set
     */
    public void setEtapaMeioAmbienteId(EnumTipoMeioAmbiente etapaMeioAmbienteId) {
        this.etapaMeioAmbienteId = etapaMeioAmbienteId;
    }

    /**
     * @return the nomePrincipal
     */
    public String getNomePrincipal() {
        return nomePrincipal;
    }

    /**
     * @param nomePrincipal the nomePrincipal to set
     */
    public void setNomePrincipal(String nomePrincipal) {
        this.nomePrincipal = nomePrincipal;
    }

    /**
     * @return the diasForaDoPrazo
     */
    public Integer getDiasForaDoPrazo() {
        return diasForaDoPrazo;
    }

    /**
     * @param diasForaDoPrazo the diasForaDoPrazo to set
     */
    public void setDiasForaDoPrazo(Integer diasForaDoPrazo) {
        this.diasForaDoPrazo = diasForaDoPrazo;
    }

    public static class Filtro {

        public static final String GERENCIA = "gerencia";
        public static final String CADEIA_PRODUTIVA = "cadeiaProdutiva";
        public static final String ANALISTA = "analista";
        public static final String ESTAGIO = "estagio";
        public static final String STATUS_ATUAL = "statusAtual";
        public static final String REGIAO = "regiao";
        public static final String STATUS_PROCESSO = "statusProcesso";
        public static final String ETAPA = "etapa";
    }

}
