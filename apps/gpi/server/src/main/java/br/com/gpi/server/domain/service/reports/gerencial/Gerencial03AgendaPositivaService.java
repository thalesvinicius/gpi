/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.gpi.server.domain.service.reports.gerencial;

import br.com.gpi.server.domain.entity.CadeiaProdutiva;
import br.com.gpi.server.domain.entity.Departamento;
import br.com.gpi.server.domain.entity.Estagio;
import br.com.gpi.server.domain.entity.RegiaoPlanejamento;
import br.com.gpi.server.domain.entity.RelatorioAcompanhamentoDataEstagios;
import br.com.gpi.server.domain.entity.User;
import br.com.gpi.server.domain.service.reports.BaseReportService;
import br.com.gpi.server.util.functional.FunctionalInterfaces;
import br.com.gpi.server.util.reports.ReportUtil;
import static br.com.gpi.server.util.reports.ReportUtil.*;
import java.io.ByteArrayOutputStream;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.ws.rs.core.SecurityContext;
import static net.sf.dynamicreports.report.builder.DynamicReports.cmp;
import static net.sf.dynamicreports.report.builder.DynamicReports.stl;
import net.sf.dynamicreports.report.builder.component.ComponentBuilder;
import net.sf.dynamicreports.report.constant.HorizontalAlignment;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.export.JRXlsExporterParameter;
import net.sf.jasperreports.engine.export.ooxml.JRXlsxExporter;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

/**
 *
 * @author rafaelbfs
 */
public class Gerencial03AgendaPositivaService extends BaseReportService<Object> {
    
   private static final List<Estagio> estagiosValidos = Arrays.asList(Estagio.FORMALIZADA,
            Estagio.IMPLANTACAO_INICIADO, Estagio.OPERACAO_INICIADA);
    
    @Inject
    public Gerencial03AgendaPositivaService(EntityManager em) {
        super(em);
    }

    private static final String BASE_QUERY = "SELECT DISTINCT AP.*, E.Descricao AS ESTAGIO, ap.CadeiaProdutiva AS CADEIA_PRODUTIVA, (YEAR(AP.DT_PREVISAO) * 100 + MONTH(AP.DT_PREVISAO)) AS MES_PROJETO\n"
            + "FROM VIEW_RELATORIO_AGENDA_POSITIVA AP \n"
            + "LEFT JOIN ESTAGIO E ON E.ID = AP.ESTAGIO_ID \n"
            + "WHERE 1 = 1 \n";
            //+ "WHERE RegiaoPlanejamento_id IS NOT NULL\n";
    private static final String ORDER_BY = "\n ORDER BY ap.EstagioPrevisto, AP.DT_PREVISAO ,AP.ID";

    public byte[] generatePDF(FiltroAgendaPositiva filtro, SecurityContext securityContext) throws Exception {
        return JasperExportManager.exportReportToPdf(generateReport(filtro,Boolean.FALSE,securityContext));
    }

    public byte[] generateXSL(FiltroAgendaPositiva filtro) throws Exception {
        JRXlsxExporter exporter = new JRXlsxExporter();
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, baos);
        exporter.setParameter(JRExporterParameter.IGNORE_PAGE_MARGINS, true);
        exporter.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_COLUMNS,Boolean.FALSE );
        exporter.setParameter(JRXlsExporterParameter.IS_FONT_SIZE_FIX_ENABLED,Boolean.FALSE);
        exporter.setParameter(JRXlsExporterParameter.IS_COLLAPSE_ROW_SPAN,Boolean.TRUE);
        exporter.setParameter(JRXlsExporterParameter.IS_ONE_PAGE_PER_SHEET,Boolean.TRUE);        
        exporter.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE,Boolean.TRUE);
        exporter.setParameter(JRXlsExporterParameter.JASPER_PRINT, generateReport(filtro,Boolean.TRUE));
        exporter.exportReport();
        return baos.toByteArray();
    }
    
    private JasperPrint generateReport(FiltroAgendaPositiva pfiltro, Boolean ignorarPaginacao) throws Exception{
        return generateReport(pfiltro, ignorarPaginacao, null);
    }
    
    private JasperPrint generateReport(FiltroAgendaPositiva pfiltro, Boolean ignorarPaginacao, SecurityContext securityContext) throws Exception {
        
        FunctionalInterfaces.ParameterizedSelectBuilder queryBuilder = new FunctionalInterfaces.ParameterizedSelectBuilder() {
            
            @Override
            public Pair<String, List<Object>> build() throws Exception {
                List<Object> parametros = new LinkedList<>();
                StringBuffer sb = new StringBuffer(BASE_QUERY);
                final FiltroAgendaPositiva filtro = pfiltro;

                sb.append(" AND ap.dt_real is NULL ");
                
                if (filtro.getPeriodoInicio() != null) {
                    sb.append("\n AND DT_PREVISAO >= ? ");
                    parametros.add(new Timestamp(filtro.getPeriodoInicio().getTime()));
                }
                if (filtro.getPeriodoFim() != null) {
                    sb.append("\n AND  DT_PREVISAO <= ? ");
                    
                    Calendar mesAnoFim = Calendar.getInstance();
                    mesAnoFim.setTimeInMillis(filtro.getPeriodoFim().getTime());
                    mesAnoFim.set(Calendar.DAY_OF_MONTH, mesAnoFim.getActualMaximum(Calendar.DAY_OF_MONTH));
                    mesAnoFim.set(Calendar.HOUR, 23);
                    mesAnoFim.set(Calendar.MINUTE, 59);
                    mesAnoFim.set(Calendar.SECOND, 59);
                    java.util.Date date = mesAnoFim.getTime();
                    parametros.add(new Timestamp(date.getTime()));
                }
                if (filtro.getEstagios() != null && !filtro.getEstagios().isEmpty()) {
                    List<String> estagios = new ArrayList<>();
                    for (Estagio estagio : filtro.getEstagios()) {
                        estagios.add(estagio.getSigla(estagio.getId()));
                    }
                    sb.append("\n AND AP.EstagioPrevisto IN ( "
                            + filtro.getEstagios().stream().map(Estagio::getId).map((i) -> " ? ").collect(Collectors.joining(",") ) + " ) ");
                    parametros.addAll(estagios);
                }
                if (filtro.getIdsUsuarios() != null && !filtro.getIdsUsuarios().isEmpty()) {
                    sb.append("\n AND ap.UsuarioResponsavel_id IN " + filtro.getIdsUsuarios().stream().map((i) -> " ? ").collect(Collectors.joining(",", "(", ")")));
                    parametros.addAll(filtro.getIdsUsuarios());
                }
                if (filtro.getIdsGerencias() != null && !filtro.getIdsGerencias().isEmpty()) {
                    sb.append("\n AND ap.departamento_id IN " + filtro.getIdsGerencias().stream().map((id) -> "?").collect(Collectors.joining(",","(" ,")")));
                    parametros.addAll(filtro.getIdsGerencias());
                }
                if (filtro.getCadeiasProdutivas() != null && !filtro.getCadeiasProdutivas().isEmpty()) {
                    sb.append("\n AND cadeiaprodutiva_id IN " + filtro.getCadeiasProdutivas().stream().map(CadeiaProdutiva::getId).map((i) -> " ? ").collect(Collectors.joining(",", "(", ")")));
                    parametros.addAll(filtro.getCadeiasProdutivas().stream().map(CadeiaProdutiva::getId).collect(Collectors.toList()));
                }
                if(filtro.getRegiao() != null && !filtro.getRegiao().isEmpty()){
                    sb.append("\n AND RegiaoPlanejamento_id IN " + filtro.getRegiao().stream().map((rp)-> "?").collect(Collectors.joining(",", "(",")")));
                    parametros.addAll(filtro.getRegiao().stream().map(RegiaoPlanejamento::getId).collect(Collectors.toList()));
                }
                if(filtro.getVersaoCorrente() != null){
                    sb.append("\n AND ap.isLastVersion = ? ");
                    parametros.add(filtro.getVersaoCorrente() );
                }
                if(filtro.getDiretoria() != null){
                    sb.append("\n AND AP.diretoriaId = ? ");
                    parametros.add(filtro.getDiretoria());
                }

                sb.append(ORDER_BY);
                return new ImmutablePair<String,List<Object>>(sb.toString(),parametros);
            }
        };
        
        Map<String, Object> params = new HashMap<>();
            params.put("REPORT_LOCALE", new Locale("pt","BR"));
            params.put(PERIODO, periodoString.apply(pfiltro.getPeriodoInicio(), pfiltro.getPeriodoFim()));
            params.put(CADEIA_PRODUTIVA, stringCollection(CadeiaProdutiva::getDescricao,  pfiltro.isBuscarTodasCadeiasProdutivas() ? null : pfiltro.getCadeiasProdutivas()));
            params.put(GERENCIA, stringCollection(Departamento::getDescricao, pfiltro.isBuscarTodasGerencias() ? null : pfiltro.getGerencias()));
            params.put(ESTAGIO_PREVISTO, stringCollection(Estagio::getDescricao, pfiltro.isBuscarTodosEstagios() ? null : pfiltro.getEstagios()));
            params.put(ANALISTA, stringCollection(User::getNome, pfiltro.isBuscarTodosAnalistas() ? null : pfiltro.getAnalistas()));
            params.put(REGIAO_PLANEJAMENTO, stringCollection(RegiaoPlanejamento::getNome, pfiltro.isBuscarTodasRegioes() ? null : pfiltro.getRegiao()));
            params.put("TITULO", "Agenda Positiva");
            params.put("VERSAO",Boolean.TRUE.equals(pfiltro.getVersaoCorrente())?"Corrente":"Congelada");
            params.put("LOGO_INDI",getImage(ReportUtil.LOGO_INDI_NOME));
            params.put("SUB_CABECALHO_AGENDA_POSITIVA",loadObject(resourceAsStream(CABECALHO_REL_AGENDA_POSITIVA)));
            params.put(JRParameter.IS_IGNORE_PAGINATION,ignorarPaginacao);
            params.put("EXPORTA_EXCEL", ignorarPaginacao );      
            
            Map reponsavelInformacao = generateResponsibleForReportDS().apply(securityContext);            
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            Date dataGeracao = (reponsavelInformacao.get("DATA_GERACAO") != null) ? (Date) reponsavelInformacao.get("DATA_GERACAO") : new Date();
            reponsavelInformacao.put("DATA_GERACAO", sdf.format(dataGeracao));
            params.putAll(reponsavelInformacao);
        
        return super.generateReport(params, 
         resourceAsStream(RELATORIO_GERENCIAL_03_AGENDA_POSITIVA),queryBuilder);
    }
    
    
    
    
    

}
