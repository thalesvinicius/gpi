package br.com.gpi.server.domain.repository;

import br.com.gpi.server.domain.entity.Departamento;
import java.util.List;
import org.apache.deltaspike.data.api.EntityRepository;
import org.apache.deltaspike.data.api.Query;
import org.apache.deltaspike.data.api.Repository;
import org.apache.deltaspike.data.api.criteria.CriteriaSupport;

@Repository(forEntity = Departamento.class)
public abstract class DepartamentoRepository  implements CriteriaSupport<Departamento>,EntityRepository<Departamento, Long>{

    @Query(named = Departamento.Query.findAllByDepartamentoSuperiorIsNull)
    public abstract List<Departamento> findAllByDepartamentoSuperiorIsNull();
    
    @Query(named = Departamento.Query.findAllByDepartamentoSuperiorId)
    public abstract List<Departamento> findAllByDepartamentoSuperiorId(Long departamentoSuperiorId);
}
