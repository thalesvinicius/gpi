package br.com.gpi.server.config;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import org.apache.deltaspike.core.api.exclude.Exclude;
import org.apache.deltaspike.core.api.projectstage.ProjectStage;

@Singleton
@Startup
@Exclude(exceptIfProjectStage = ProjectStage.Development.class)
public class BootstrapBean {

    @PostConstruct
    public void bootstrapDataBase() {
        
    }
}
