package br.com.gpi.server.core.jpa.converter;

import br.com.gpi.server.domain.entity.TipoEmprego;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = false)
public class TipoEmpregoEnumConverter implements AttributeConverter<TipoEmprego, Integer> {

    @Override
    public Integer convertToDatabaseColumn(TipoEmprego group) {
        return group.getId();
    }

    @Override
    public TipoEmprego convertToEntityAttribute(Integer id) {
        return TipoEmprego.getTipoEmprego(id);
    }
}
