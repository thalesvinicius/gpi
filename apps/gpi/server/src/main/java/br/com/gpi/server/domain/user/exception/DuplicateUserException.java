package br.com.gpi.server.domain.user.exception;

import br.com.gpi.server.core.exception.BaseWebApplicationException;

public class DuplicateUserException extends BaseWebApplicationException {

    public DuplicateUserException() {
        super(409, "40901", "MSG80_LOGIN_DUPLICADO", "An attempt was made to create a user that already exists");
    }
}
