package br.com.gpi.server.domain.entity;

import br.com.gpi.server.domain.common.BaseEntity;
import com.fasterxml.jackson.annotation.JsonIgnore;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import org.hibernate.envers.Audited;
import org.hibernate.validator.constraints.Email;

@Audited
@Entity
public class Signatario extends BaseEntity<Long> {

    @ManyToOne
    @JoinColumn(name = "instrumentoFormalizacao_id")
    @JsonIgnore
    private InstrumentoFormalizacao instrFormalizacao;
    private String nome;
    private String cargo;
    private String telefone1;
    private String telefone2;
    @Email
    private String email;

    public Signatario() {
    }

    public InstrumentoFormalizacao getInstrFormalizacao() {
        return instrFormalizacao;
    }

    public void setInstrFormalizacao(InstrumentoFormalizacao instrFormalizacao) {
        this.instrFormalizacao = instrFormalizacao;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCargo() {
        return cargo;
    }

    public void setCargo(String cargo) {
        this.cargo = cargo;
    }

    public String getTelefone1() {
        return telefone1;
    }

    public void setTelefone1(String telefone1) {
        this.telefone1 = telefone1;
    }

    public String getTelefone2() {
        return telefone2;
    }

    public void setTelefone2(String telefone2) {
        this.telefone2 = telefone2;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

}
