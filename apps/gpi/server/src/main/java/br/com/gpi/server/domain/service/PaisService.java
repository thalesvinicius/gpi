package br.com.gpi.server.domain.service;

import br.com.gpi.server.domain.common.BaseService;
import br.com.gpi.server.domain.entity.Pais;
import br.com.gpi.server.domain.entity.Pais_;
import br.com.gpi.server.to.PaisTO;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

public class PaisService extends BaseService<Pais> {

    @Inject
    public PaisService(EntityManager em) {
        super(Pais.class, em);
    }

    public List<PaisTO> buscaPaisesDiferentedeBrasil() {
        CriteriaBuilder builder = getEm().getCriteriaBuilder();
        CriteriaQuery<PaisTO> query = builder.createQuery(PaisTO.class);
        Root<Pais> fromPais = query.from(Pais.class);
        List<Predicate> conditions = new ArrayList();
        conditions.add(builder.notEqual(builder.lower(fromPais.get(Pais_.nome)), "brasil"));

        TypedQuery<PaisTO> typedQuery = getEm().createQuery(query
                .multiselect(fromPais.get(Pais_.id),
                        fromPais.get(Pais_.nome))
                .where(conditions.toArray(new Predicate[]{}))
                .orderBy(builder.asc(fromPais.get(Pais_.nome)))
                .distinct(true)
        );

        List<PaisTO> paises = typedQuery.getResultList();
        
        return paises;
    }
}
