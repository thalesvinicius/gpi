package br.com.gpi.server.domain.entity;

import br.com.gpi.server.domain.common.AuditedEntity;
import br.com.gpi.server.domain.entity.Template.Query;
import br.com.gpi.server.domain.entity.enumerated.EnumTipoTemplate;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import org.hibernate.envers.Audited;

@Audited
@Entity
@Table(name = "Template")
@NamedQueries({
    @NamedQuery(name = Query.findAllByStatusAndTipo, query = "select t from Template t where t.status = ? and t.tipo = ? ")
})
public class Template extends AuditedEntity<Long> {
    
    @NotNull    
    @Column(name = "nome", nullable = false, length = 100)    
    private String nome;
    
    @NotNull    
    @Column(name = "tipo", nullable = false)    
    private EnumTipoTemplate tipo;
    
    @NotNull
    @Column(name = "status", columnDefinition = "bit", nullable = false)
    private Boolean status;

    @OrderBy("posicao ASC")
    @OneToMany(mappedBy = "template", fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
    private List<AtividadeTemplate> atividades;
    
    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public EnumTipoTemplate getTipo() {
        return tipo;
    }

    public void setTipo(EnumTipoTemplate tipo) {
        this.tipo = tipo;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public List<AtividadeTemplate> getAtividades() {
        return atividades;
    }

    public void setAtividadeTemplateList(List<AtividadeTemplate> atividades) {
        this.atividades = atividades;
    }
    
    public static class Query {
        public static final String findAllByStatusAndTipo = "Template.findAllByStatusAndTipo";
    }

}
