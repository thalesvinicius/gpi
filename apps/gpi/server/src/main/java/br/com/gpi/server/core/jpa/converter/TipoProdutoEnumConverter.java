package br.com.gpi.server.core.jpa.converter;

import br.com.gpi.server.domain.entity.enumerated.EnumTipoProduto;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = false)
public class TipoProdutoEnumConverter implements AttributeConverter<EnumTipoProduto, Integer> {

    @Override
    public Integer convertToDatabaseColumn(EnumTipoProduto tipoProduto) {
        return tipoProduto.getId();
    }

    @Override
    public EnumTipoProduto convertToEntityAttribute(Integer dbData) {
        return EnumTipoProduto.getEnumTipoProduto(dbData);
    } 
}
