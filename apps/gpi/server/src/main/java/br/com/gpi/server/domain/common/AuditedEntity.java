package br.com.gpi.server.domain.common;

import br.com.gpi.server.domain.entity.User;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Date;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.apache.deltaspike.data.api.audit.ModifiedOn;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.hibernate.envers.Audited;

@Audited 
@MappedSuperclass
public class AuditedEntity<T> extends BaseEntity<T> {

    @Temporal(TemporalType.TIMESTAMP)
    @ModifiedOn(onCreate = true)
    private Date dataUltimaAlteracao;

    @ManyToOne
    @JoinColumn(name = "usuarioResponsavel_id")
    @NotFound(action = NotFoundAction.IGNORE)
    @JsonIgnore
    private User usuarioResponsavel;

    public Date getDataUltimaAlteracao() {
        return dataUltimaAlteracao;
    }

    public void setDataUltimaAlteracao(Date dataUltimaAlteracao) {
        this.dataUltimaAlteracao = dataUltimaAlteracao;
    }

    @JsonProperty("usuarioResponsavel")
    public User getUsuarioResponsavel() {
        return usuarioResponsavel;
    }

    @JsonIgnore
    public void setUsuarioResponsavel(User usuarioResponsavel) {
        this.usuarioResponsavel = usuarioResponsavel;
    }

}