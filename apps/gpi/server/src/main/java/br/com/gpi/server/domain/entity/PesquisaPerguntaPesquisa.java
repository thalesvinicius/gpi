package br.com.gpi.server.domain.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import org.hibernate.envers.Audited;

@Audited
@IdClass(PesquisaPerguntaPesquisaPK.class)
@Entity
@Table(name = "PesquisaPerguntaPesquisa")
public class PesquisaPerguntaPesquisa implements Serializable {

    @Id
    @Column(name = "pesquisa_id")
    private Long pesquisaId;
    @Id
    @Column(name = "perguntaPesquisa_id")
    private Long perguntaPesquisaId;

    @JsonIgnore
    @Transient
    @ManyToOne
    @JoinColumn(name = "pesquisa_id", insertable = false, updatable = false)
    private Pesquisa pesquisa;

    @ManyToOne
    @JoinColumn(name = "perguntaPesquisa_id", insertable = false, updatable = false)
    private PerguntaPesquisa perguntaPesquisa;

    private Integer resposta;
    private String justificativa;

    public Long getPesquisaId() {
        return pesquisaId;
    }

    public void setPesquisaId(Long pesquisaId) {
        this.pesquisaId = pesquisaId;
    }

    public Long getPerguntaPesquisaId() {
        return perguntaPesquisaId;
    }

    public void setPerguntaPesquisaId(Long perguntaPesquisaId) {
        this.perguntaPesquisaId = perguntaPesquisaId;
    }

    public Pesquisa getPesquisa() {
        return pesquisa;
    }

    public void setPesquisa(Pesquisa pesquisa) {
        this.pesquisa = pesquisa;
    }

    public PerguntaPesquisa getPerguntaPesquisa() {
        return perguntaPesquisa;
    }

    public void setPerguntaPesquisa(PerguntaPesquisa perguntaPesquisa) {
        this.perguntaPesquisa = perguntaPesquisa;
    }

    public Integer getResposta() {
        return resposta;
    }

    public void setResposta(Integer resposta) {
        this.resposta = resposta;
    }

    public String getJustificativa() {
        return justificativa;
    }

    public void setJustificativa(String justificativa) {
        this.justificativa = justificativa;
    }

    

}
