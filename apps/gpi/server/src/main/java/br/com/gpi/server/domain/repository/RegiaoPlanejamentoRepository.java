package br.com.gpi.server.domain.repository;

import br.com.gpi.server.domain.entity.RegiaoPlanejamento;
import org.apache.deltaspike.data.api.EntityRepository;
import org.apache.deltaspike.data.api.Repository;
import org.apache.deltaspike.data.api.criteria.CriteriaSupport;

@Repository(forEntity = RegiaoPlanejamento.class)
public abstract class RegiaoPlanejamentoRepository implements CriteriaSupport<RegiaoPlanejamento>, EntityRepository<RegiaoPlanejamento, Long>{
    
}
