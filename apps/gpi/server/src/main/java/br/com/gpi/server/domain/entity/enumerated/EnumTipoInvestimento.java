package br.com.gpi.server.domain.entity.enumerated;

import br.com.gpi.server.core.exception.EnumException;
import br.com.gpi.server.core.jpa.typesafe.EnumerationEntityType;

public enum EnumTipoInvestimento implements EnumerationEntityType<Integer> {

    INVALID(0, "invalid"),  
    INVESTIMENTO_MAQ_EQUIP_TERRENO_OBRAS(1, "Investimento em máquinas, equipamentos, terrenos e obras"),
    INVESTIMENTO_CAPITAL_GIRO_E_OUTROS(2, "Capital de Giro e Outros investimentos (em R$)"),
    INVESTIMENTO_INDUSTRIAL(3, "Investimento Industrial"),
    INVESTIMENTO_AGRICOLA(4, "Investimento Agrícola"),
    INVESTIMENTO_CAPACITACAO_PROFISSIONAL(5, "Investimento em capacitação profissional dos empregados"),
    INVESTIMENTO_PROPRIOS(6, "Investimentos Próprios"),
    INVESTIMENTO_TERRENO(7, "Terreno"),
    INVESTIMENTO_ESTUDO_PROJETO_DESP_PRE_OPERACIONAIS(8, "Estudos/Projetos e Despesas Pré-Operacionais"),
    INVESTIMENTO_OBRAS_CIVIS_INSTALACOES(9, "Obras Civis/Instalações"),
    INVESTIMENTO_MAQ_EQUIP_NACIONAIS(10, "Máquinas/Equip. Nacionais"),
    INVESTIMENTO_MAQ_EQUIP_IMPORTADOS(11, "Máquinas/Equip. Importados"),
    INVESTIMENTO_MAQ_EQUIP_USADOS(12, "Máquinas/Equip. Usados"),
    INVESTIMENTO_CAPITAL_GIRO(13, "Capital de Giro"),
    OUTROS_INVESTIMENTOS(14, "Outros investimentos"),
    RECURSOS_PROPRIOS(15, "Recursos próprios"),
    BDMG(16, "BDMG"),
    OUTRAS_FONTES(17, "Outras Fontes"),
    INVESTIMENTO_TOTAL_REALIZADO(18, "Investimento total realizado");
    
    private final int id;
    private final String descricao;

    private EnumTipoInvestimento(int id, String descricao) {
        this.id = id;
        this.descricao = descricao;
    }

    @Override
    public Integer getId() {
        return this.id;
    }

    @Override
    public String getDescription() {
        return this.descricao;
    }
    
    public static EnumTipoInvestimento getEnumTipoInvestimento(Integer id) {
        for (EnumTipoInvestimento tipoInvestimento : values()) {
            if (tipoInvestimento.id == id) {
                return tipoInvestimento;
            }
        }
        throw new EnumException("EnumTipoInvestiment couldn't load.");
    }

}
