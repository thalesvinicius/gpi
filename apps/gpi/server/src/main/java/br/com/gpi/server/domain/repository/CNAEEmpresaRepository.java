package br.com.gpi.server.domain.repository;

import br.com.gpi.server.domain.entity.CNAEEmpresa;
import org.apache.deltaspike.data.api.EntityRepository;
import org.apache.deltaspike.data.api.Repository;
import org.apache.deltaspike.data.api.criteria.CriteriaSupport;

@Repository(forEntity = CNAEEmpresa.class)
public abstract class CNAEEmpresaRepository implements CriteriaSupport<CNAEEmpresa>, EntityRepository<CNAEEmpresa, String> {

}
