package br.com.gpi.server.domain.entity;

import br.com.gpi.server.core.jpa.converter.InsumoProdutoOrigemEnumConverter;
import br.com.gpi.server.core.jpa.converter.TipoInstrFormalizacaoEnumConverter;
import br.com.gpi.server.domain.common.BaseEntity;
import com.fasterxml.jackson.annotation.JsonIgnore;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.envers.Audited;

@Audited
@Entity
@Table(name = "Insumo")
public class Insumo extends BaseEntity<Long> {

    @JsonIgnore
    @ManyToOne(cascade = {CascadeType.DETACH, CascadeType.REFRESH}, optional = false, fetch = FetchType.LAZY)
    @JoinColumn(name = "insumoProduto_id")
    private InsumoProduto insumoProduto;

    private String Nome;

    @ManyToOne(cascade = {CascadeType.DETACH, CascadeType.REFRESH})
    @JoinColumn(name = "ncm_id")
    private NCM ncm;
    
    @Enumerated
    @Column(name = "origem", insertable = false, updatable = false)
    private InsumoProdutoOrigem origem;
    
    @Column(name = "origem")
    private Integer origemInt;

    private Integer quantidadeAno;

    private String unidadeMedida;

    private Double valorEstimadoAno;

    private String empresaFornecedor;

    public InsumoProduto getInsumoProduto() {
        return insumoProduto;
    }

    public void setInsumoProduto(InsumoProduto insumoProduto) {
        this.insumoProduto = insumoProduto;
    }

    public String getNome() {
        return Nome;
    }

    public void setNome(String Nome) {
        this.Nome = Nome;
    }

    public NCM getNcm() {
        return ncm;
    }

    public void setNcm(NCM ncm) {
        this.ncm = ncm;
    }

    public InsumoProdutoOrigem getOrigem() {
        return origem;
    }

    public void setOrigem(InsumoProdutoOrigem Origem) {
        this.origem = Origem;
        this.origemInt = (new InsumoProdutoOrigemEnumConverter()).convertToDatabaseColumn(Origem);
    }

    public Integer getOrigemInt() {
        return origemInt;
    }

    public void setOrigemInt(Integer origemInt) {
        this.origemInt = origemInt;
        this.origem = (new InsumoProdutoOrigemEnumConverter()).convertToEntityAttribute(origemInt);
    }
    
    

    public Integer getQuantidadeAno() {
        return quantidadeAno;
    }

    public void setQuantidadeAno(Integer quantidadeAno) {
        this.quantidadeAno = quantidadeAno;
    }

    public String getUnidadeMedida() {
        return unidadeMedida;
    }

    public void setUnidadeMedida(String unidadeMedida) {
        this.unidadeMedida = unidadeMedida;
    }

    public Double getValorEstimadoAno() {
        return valorEstimadoAno;
    }

    public void setValorEstimadoAno(Double valorEstimadoAno) {
        this.valorEstimadoAno = valorEstimadoAno;
    }

    public String getEmpresaFornecedor() {
        return empresaFornecedor;
    }

    public void setEmpresaFornecedor(String empresaFornecedor) {
        this.empresaFornecedor = empresaFornecedor;
    }

}
