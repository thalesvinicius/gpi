package br.com.gpi.server.endpoint;

import br.com.gpi.server.domain.entity.CNAE;
import br.com.gpi.server.domain.entity.Empresa;
import br.com.gpi.server.domain.repository.CNAERepository;
import br.com.gpi.server.domain.service.CNAEService;
import br.com.gpi.server.to.CnaeWrapper;
import java.util.List;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/cnae")
@Stateless
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
@TransactionManagement(TransactionManagementType.BEAN)
public class CNAEEndpoint {

    @Inject
    private CNAEService cnaeService;
    @Inject
    private CNAERepository cnaeRepository;

    @GET
    public Response findAllDivisoes() {
        List<CNAE> cnaesResponse = cnaeRepository.findAllDivisoes();
        return Response.ok(cnaesResponse).build();
    }

    @POST
    @Path("/findById")
    public Response findById(List<String> cnaes) {
        List<CNAE> cnaesResponse = cnaeRepository.findById(cnaes);
        return Response.ok(cnaesResponse).build();
    }

    @POST
    @Path("/findByParentId")
    public Response findByParentId(List<String> cnaes) {
        List<CNAE> cnaesResponse = cnaeRepository.findByParentId(cnaes);
        return Response.ok(cnaesResponse).build();
    }

    @POST
    @Path("/cnaeWrapper")
    public Response getCNAEWrapper(@Valid Empresa empresa) {
        CnaeWrapper wrapper = cnaeService.getCNAEWrapper(empresa);
        return Response.ok(wrapper).build();
    }

}
