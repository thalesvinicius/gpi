package br.com.gpi.server.domain.service;

import br.com.gpi.server.domain.common.BaseService;
import br.com.gpi.server.domain.entity.Municipio;
import javax.inject.Inject;
import javax.persistence.EntityManager;

public class MunicipioService  extends BaseService<Municipio>{
    
    @Inject
    public MunicipioService(EntityManager em) {
        super(Municipio.class, em);
    }
    
}
