/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.gpi.server.helper;

import br.com.gpi.server.to.AlertaTO;
import br.com.gpi.server.to.DescricaoAlertaTO;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author lucasgom
 */
public class AlertaHelper {
    public enum AlertaTypes {
        EMPRESA_AGUARDANDO_VALIDACAO("Existe uma nova empresa aguardando validação do INDI:", "{1}", "#/empresa/cadastro/editar?id={1}"),
        PROJETO_PENDENTE_ACEITE("Existe um projeto de sua responsabilidade com situação Pendente de Aceite:", "{1}", "#/projeto/cadastro/editar?id={1}"),
        PROJETO_PENDENTE_VALIDACAO("Existe um projeto de sua responsabilidade com situação Pendente de Validação:", "{1}", "#/projeto/cadastro/editar?id={1}"),
        PROJETO_ESTAGIO_INICIAL_MAIOR_120_DIAS("O estágio \"Início de Projeto\" ocorreu a mais de 120 dias para o projeto:", "{1}", "#/projeto/cadastro/editar?id={1}"),
        PROJETO_INICIO_IMPLANTACAO_VENCIDO("Verificar se a data prevista de Início de implantação (II) do projeto está vencida. Caso positivo, reprogramá-la. Projeto:", "{1}", "#/projeto/cadastro/editar?id={1}"),
        PROJETO_INICIO_OPERACAO_VENCIDO("Verificar se a data prevista de Início de operação (OI) do projeto está vencida. Caso positivo, reprogramá-la. Projeto:", "{1}", "#/projeto/cadastro/editar?id={1}"),
        INSTRUMENTO_EM_NEGOCIACAO_VENCIDO("A data prevista de assinatura do Protocolo/TA está vencida:", "{1}", "#/instrumento-formalizacao/cadastro/editar?id={1}"),
        RELATORIO_ANUAL_PREENCHIMENTO_PENDENTE("Relatório de Acompanhamento do Projeto está disponível para validação:", "{1}", "#/projeto/relatorio/acompanhamentoAnual/editar?id={1}");
        
        private final String titulo;
        private final String descricao;
        private final String link;
        
        AlertaTypes(String titulo, String descricao, String link){
            this.titulo= titulo;
            this.descricao = descricao;
            this.link = link;
        }

        public String getTitulo() {
            return titulo;
        }

        public String getDescricao() {
            return descricao;
        }

        public String getLink() {
            return link;
        }
    }
    
    public static List<AlertaTO> buildAlertasByTitle(AlertaTO alerta, AlertaTypes type) {
        List<AlertaTO> alertas = new ArrayList<>();
        for(int i = 0; i<alerta.getAlertas().size(); i++) {
            String descricao = replaceItems(type.descricao, alerta.getAlertas().get(i).getDescricao());
            String link = replaceItems(type.link, alerta.getAlertas().get(i).getId().toString());
            Long id = alerta.getAlertas().get(i).getId();
            
            alertas.add(new AlertaTO(type.titulo, Arrays.asList(new DescricaoAlertaTO[]{new DescricaoAlertaTO(descricao, id, link)})));
        }
        
        return alertas;
    }
    
    public static void buildAlertas(AlertaTO alerta, AlertaTypes type) {
        for(int i = 0; i<alerta.getAlertas().size(); i++) {
            alerta.getAlertas().get(i).setDescricao(replaceItems(type.descricao, alerta.getAlertas().get(i).getDescricao()));
            alerta.getAlertas().get(i).setLink(replaceItems(type.link, alerta.getAlertas().get(i).getId().toString()));
        }
    }
    
    private static String replaceItems(String descricao, String... replace) {
        for (int i = 0; i<replace.length; i++) {
            String item = replace[i];
            descricao = descricao.replace("{"+(i+1)+"}", item);
        }
        
        return descricao;
    }
    
}
