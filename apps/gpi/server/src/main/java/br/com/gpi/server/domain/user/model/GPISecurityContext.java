package br.com.gpi.server.domain.user.model;

import br.com.gpi.server.domain.entity.UserGroup;
import java.security.Principal;
import javax.ws.rs.core.SecurityContext;

/**
 *
 * @author <a href="mailto:pviniciusfm@gmail.com">Paulo Vinicius F. Machado</a>
 */
public class GPISecurityContext implements SecurityContext {

    private final SecurityUser user;

    public GPISecurityContext(SecurityUser user) {
        this.user = user;
    }

    @Override
    public String getAuthenticationScheme() {
        return SecurityContext.BASIC_AUTH;
    }

    @Override
    public Principal getUserPrincipal() {
        return user;
    }

    @Override
    public boolean isSecure() {
        return user != null;
    }

    @Override
    public boolean isUserInRole(String role) {
        try {
            UserGroup userGroup = UserGroup.valueOf(role);
            return user.hasRole(userGroup);
        } catch (Exception e) {
        }

        return false;
    }

    public static SecurityContext createDefaultSecurityContext() {
        return new SecurityContext() {
            SecurityUser user = new SecurityUser();

            @Override
            public Principal getUserPrincipal() {
                return user;
            }

            @Override
            public boolean isUserInRole(String string) {
                return string.equals("ANONYMOUS");
            }

            @Override
            public boolean isSecure() {
                return false;
            }

            @Override
            public String getAuthenticationScheme() {
                return SecurityContext.BASIC_AUTH;
            }
        };
    }
}
