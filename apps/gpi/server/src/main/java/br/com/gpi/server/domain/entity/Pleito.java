package br.com.gpi.server.domain.entity;

import br.com.gpi.server.domain.common.AuditedEntity;
import com.fasterxml.jackson.annotation.JsonIgnore;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import org.hibernate.envers.Audited;

@Audited
@Entity
@Table(name = "Pleito")
@NamedQueries({
    @NamedQuery(name = Pleito.Query.locateById, query = "SELECT p FROM Pleito p WHERE p.id = :id"),
    @NamedQuery(name = Pleito.Query.locateByProjetoId, query = "SELECT p FROM Pleito p WHERE p.projeto.id = ?")
})
public class Pleito extends AuditedEntity<Long> {

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "projeto_id")
    private Projeto projeto;
    private String pleitos;
    private String observacoes;

    public Projeto getProjeto() {
        return projeto;
    }

    public void setProjeto(Projeto projeto) {
        this.projeto = projeto;
    }

    public String getPleitos() {
        return pleitos;
    }

    public void setPleitos(String pleitos) {
        this.pleitos = pleitos;
    }

    public String getObservacoes() {
        return observacoes;
    }

    public void setObservacoes(String observacoes) {
        this.observacoes = observacoes;
    }

    public static class Query {

        public static final String locateById = "Pleito.locateById";
        public static final String locateByProjetoId = "Pleito.locateByProjetoId";
    }

}
