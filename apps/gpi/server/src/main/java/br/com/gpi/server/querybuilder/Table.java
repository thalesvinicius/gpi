package br.com.gpi.server.querybuilder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class Table implements Comparable {

    private String name;
    private String alias;
    private String pkName;
    private Class entity;
    private Integer nivel;
    private TableLink dependency;
    private Map<String, TableLink> possibleLinks;
    private List<Table> aditionalTables;
    private Boolean rootTable;

    public Table(String name, String alias, String pkName, Integer nivel, TableLink dependency) {
        this.alias = alias;
        this.name = name;
        this.pkName = pkName;
        this.nivel = nivel;
        this.dependency = dependency;
    }

    public Table(String name, String alias, String pkName, Integer nivel, TableLink dependency, Boolean rootTable) {
        this(name, alias, pkName, nivel, dependency);
        this.rootTable = rootTable;
    }

    public TableLink getDependency() {
        return dependency;
    }

    public void setDependency(TableLink dependency) {
        this.dependency = dependency;
    }

    public Map<String, TableLink> getPossibleLinks() {
        if (possibleLinks == null) {
            possibleLinks = new HashMap<>();
        }
        return possibleLinks;
    }

    public void setPossibleLinks(Map<String, TableLink> possibleLinks) {
        this.possibleLinks = possibleLinks;
    }

    public String getName() {
        return name;
    }

    public Integer getNivel() {
        return nivel;
    }

    public void setNivel(Integer nivel) {
        this.nivel = nivel;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPkName() {
        return pkName;
    }

    public void setPkName(String pkName) {
        this.pkName = pkName;
    }

    public Class getEntity() {
        return entity;
    }

    public void setEntity(Class entity) {
        this.entity = entity;
    }

    public String getAlias() {
        return alias;
    }

    public String getSQLFromDeclaration() {
        return getName() + " as " + getAlias();
    }

    public boolean isRootTable() {
        return rootTable != null ? rootTable : false;
    }

    public Boolean getRootTable() {
        return rootTable;
    }

    public void setRootTable(Boolean rootTable) {
        this.rootTable = rootTable;
    }
    
    /**
     * Used to force including another tables in the query like cnae all levels for example.
     * @return 
     */
    public List<Table> getAditionalTables() {
        if (aditionalTables == null) {
            aditionalTables = new ArrayList<>();
        }
        return aditionalTables;
    }

    public void setAditionalTables(List<Table> aditionalTables) {
        this.aditionalTables = aditionalTables;
    }
    
    @Override
    public int compareTo(Object o) {
        try {
            Table t = (Table) o;
            return this.getNivel().compareTo(t.getNivel());
        } catch (NullPointerException ne) {
            throw new NullPointerException("Value to compare can not be null");
        } catch (ClassCastException ce) {
            throw new ClassCastException("Value to compare can not be cast to TabelaBuscaAvancada");
        }
    }

    public void addPossibleLink(TableLink tableDependency) {
        if (tableDependency.getToTable().getAlias() == null) {
            throw new IllegalArgumentException("TableDependency must have a valid table name");
        } else {
            getPossibleLinks().put(tableDependency.getToTable().getAlias(), tableDependency);
        }
    }
    
    public TableLink getLink(String alias) {
        return getPossibleLinks().get(alias);
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 83 * hash + Objects.hashCode(this.name);
        hash = 83 * hash + Objects.hashCode(this.alias);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Table other = (Table) obj;
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        if (!Objects.equals(this.alias, other.alias)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return this.name + " " + this.alias; //To change body of generated methods, choose Tools | Templates.
    }

}
