package br.com.gpi.server.domain.service;

import br.com.gpi.server.domain.common.QueryParameter;
import br.com.gpi.server.domain.entity.Estagio;
import br.com.gpi.server.domain.entity.SituacaoProjetoEnum;
import br.com.gpi.server.domain.entity.enumerated.EnumTipoMeioAmbiente;
import br.com.gpi.server.domain.service.reports.BaseReportService;
import br.com.gpi.server.domain.view.RelatorioAcompanhamentoAmbiental;
import br.com.gpi.server.domain.view.RelatorioAcompanhamentoAmbiental_;
import br.com.gpi.server.util.DateUtils;
import br.com.gpi.server.util.EnumMimeTypes;
import br.com.gpi.server.util.reports.ReportUtil;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Supplier;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.ws.rs.core.Response;
import net.sf.dynamicreports.jasper.builder.JasperReportBuilder;
import static net.sf.dynamicreports.report.builder.DynamicReports.col;
import static net.sf.dynamicreports.report.builder.DynamicReports.type;
import net.sf.dynamicreports.report.builder.column.ColumnBuilder;
import net.sf.dynamicreports.report.datasource.DRDataSource;
import net.sf.jasperreports.engine.JRDataSource;

public class RelatorioAcompanhamentoAmbientalService extends BaseReportService<RelatorioAcompanhamentoAmbiental> {

    @Inject
    public RelatorioAcompanhamentoAmbientalService(EntityManager em) {
        super(em);
    }

    public List<RelatorioAcompanhamentoAmbiental> findRelatorioAcompanhamentoAmbientalByFilters(QueryParameter filtros) {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<RelatorioAcompanhamentoAmbiental> query = builder.createQuery(RelatorioAcompanhamentoAmbiental.class);
        Root<RelatorioAcompanhamentoAmbiental> fromRelatorioAcompanhamentoAmbiental = query.from(RelatorioAcompanhamentoAmbiental.class);

        List<Predicate> conditions = new ArrayList<>();

        if (filtros.exists(RelatorioAcompanhamentoAmbiental.Filtro.ESTAGIO)) {
            conditions.add(fromRelatorioAcompanhamentoAmbiental.get(RelatorioAcompanhamentoAmbiental_.estagioId).in(filtros.get(RelatorioAcompanhamentoAmbiental.Filtro.ESTAGIO, List.class)));
        }

        if (filtros.exists(RelatorioAcompanhamentoAmbiental.Filtro.STATUS_ATUAL)) {
            conditions.add(fromRelatorioAcompanhamentoAmbiental.get(RelatorioAcompanhamentoAmbiental_.situacaoId).in(filtros.get(RelatorioAcompanhamentoAmbiental.Filtro.STATUS_ATUAL, List.class)));
        } else {
            List<SituacaoProjetoEnum> status = new ArrayList<>();
            status.add(SituacaoProjetoEnum.ATIVO);
            status.add(SituacaoProjetoEnum.SUSPENSO);
            status.add(SituacaoProjetoEnum.CANCELADO);
            conditions.add(fromRelatorioAcompanhamentoAmbiental.get(RelatorioAcompanhamentoAmbiental_.situacaoId).in(status));
        }

        if (filtros.exists(RelatorioAcompanhamentoAmbiental.Filtro.REGIAO)) {
            conditions.add(fromRelatorioAcompanhamentoAmbiental.get(RelatorioAcompanhamentoAmbiental_.regiaoId).in(filtros.get(RelatorioAcompanhamentoAmbiental.Filtro.REGIAO, List.class)));
        }

        if (filtros.exists(RelatorioAcompanhamentoAmbiental.Filtro.GERENCIA)) {
            conditions.add(fromRelatorioAcompanhamentoAmbiental.get(RelatorioAcompanhamentoAmbiental_.departamentoId).in(filtros.get(RelatorioAcompanhamentoAmbiental.Filtro.GERENCIA, List.class)));
            if (filtros.exists(RelatorioAcompanhamentoAmbiental.Filtro.ANALISTA)) {
                conditions.add(fromRelatorioAcompanhamentoAmbiental.get(RelatorioAcompanhamentoAmbiental_.analistaId).in(filtros.get(RelatorioAcompanhamentoAmbiental.Filtro.ANALISTA, List.class)));
            }
        }

        if (filtros.exists(RelatorioAcompanhamentoAmbiental.Filtro.CADEIA_PRODUTIVA)) {
            conditions.add(fromRelatorioAcompanhamentoAmbiental.get(RelatorioAcompanhamentoAmbiental_.cadeiaProdutivaId).in(filtros.get(RelatorioAcompanhamentoAmbiental.Filtro.CADEIA_PRODUTIVA, List.class)));
        }

        if (filtros.exists(RelatorioAcompanhamentoAmbiental.Filtro.ETAPA)) {
            conditions.add(fromRelatorioAcompanhamentoAmbiental.get(RelatorioAcompanhamentoAmbiental_.etapaMeioAmbienteId).in(filtros.get(RelatorioAcompanhamentoAmbiental.Filtro.ETAPA, List.class)));
        }

        TypedQuery<RelatorioAcompanhamentoAmbiental> typedQuery = entityManager.createQuery(query
                .multiselect(fromRelatorioAcompanhamentoAmbiental.get(RelatorioAcompanhamentoAmbiental_.nomePrincipal),
                        fromRelatorioAcompanhamentoAmbiental.get(RelatorioAcompanhamentoAmbiental_.projeto),
                        fromRelatorioAcompanhamentoAmbiental.get(RelatorioAcompanhamentoAmbiental_.cidade),
                        fromRelatorioAcompanhamentoAmbiental.get(RelatorioAcompanhamentoAmbiental_.regiao),
                        fromRelatorioAcompanhamentoAmbiental.get(RelatorioAcompanhamentoAmbiental_.cadeiaProdutiva),
                        fromRelatorioAcompanhamentoAmbiental.get(RelatorioAcompanhamentoAmbiental_.classeEmpreendimento),
                        fromRelatorioAcompanhamentoAmbiental.get(RelatorioAcompanhamentoAmbiental_.etapas),
                        fromRelatorioAcompanhamentoAmbiental.get(RelatorioAcompanhamentoAmbiental_.dataEntregaEstudos),
                        fromRelatorioAcompanhamentoAmbiental.get(RelatorioAcompanhamentoAmbiental_.expectativaOI),
                        fromRelatorioAcompanhamentoAmbiental.get(RelatorioAcompanhamentoAmbiental_.situacaoId), 
                        fromRelatorioAcompanhamentoAmbiental.get(RelatorioAcompanhamentoAmbiental_.estagioId), 
                        fromRelatorioAcompanhamentoAmbiental.get(RelatorioAcompanhamentoAmbiental_.analista), 
                        fromRelatorioAcompanhamentoAmbiental.get(RelatorioAcompanhamentoAmbiental_.departamento))
                .distinct(true)
                .where(conditions.toArray(new Predicate[]{}))
                .orderBy(builder.desc(fromRelatorioAcompanhamentoAmbiental.get(RelatorioAcompanhamentoAmbiental_.nomePrincipal)))
        );
        
        List<RelatorioAcompanhamentoAmbiental> relatorioAcompanhamentoAmbientalFiltradoList = new ArrayList<>();
        List<RelatorioAcompanhamentoAmbiental> relatorioAcompanhamentoAmbientalList = typedQuery.getResultList();
        
        HashMap<Integer,Integer> mapa = new HashMap();
        mapa.put(1, 30);
        mapa.put(2, 30);
        mapa.put(3, 360);
        mapa.put(4, 360);
        mapa.put(5, 540);
        mapa.put(6, 540);
        
        for (RelatorioAcompanhamentoAmbiental r : relatorioAcompanhamentoAmbientalList) {
            Integer diasASomar = 0;            
            Calendar cal = Calendar.getInstance();
            
            /* Se o usuário informar o campo "Qual a classe do empreendimento?" e não informar o campo 
            "Data de entrega dos estudos", o GPI deverá considerar a data do sistema para contar o 
            prazo legal do licenciamento de acordo com a Classe do empreendimento informada */           
            if(r.getDataEntregaEstudos() != null) {                                
                cal.setTime(r.getDataEntregaEstudos());
            }                                    
            
            if (r.getClasseEmpreendimento() != null) {
                if  (mapa.get(r.getClasseEmpreendimento()) != null) {
                    diasASomar = mapa.get(r.getClasseEmpreendimento());
                    cal.add(Calendar.DAY_OF_MONTH, diasASomar);
                    r.setExpectativaEmissaoLicenca(cal.getTime());                                
                }
            }                        
                           
            // situacaoProcessoId: 0 - Fora do prazo  1 - Dentro do prazo
            if (r.getExpectativaOI() == null) {
                r.setSituacaoProcessoId(1);
            } else if(r.getExpectativaEmissaoLicenca() != null) {
                Calendar expectativaOIAConsiderar = Calendar.getInstance();
                expectativaOIAConsiderar.setTime(r.getExpectativaOI());
                expectativaOIAConsiderar.set(Calendar.DAY_OF_MONTH, 1);
                   
                cal.setTime(r.getExpectativaEmissaoLicenca());
                cal.add(Calendar.DAY_OF_MONTH, 1);
                
                if (expectativaOIAConsiderar.compareTo(cal) >= 0) {
                    r.setSituacaoProcessoId(1);
                } else {
                    r.setSituacaoProcessoId(0);
                }                                
            }
                        
            if (r.getExpectativaOI() == null) {
                r.setDiasForaDoPrazo(0);
            } else if(r.getExpectativaEmissaoLicenca() != null) {
                Calendar expectativaOIAConsiderar = Calendar.getInstance();
                expectativaOIAConsiderar.setTime(r.getExpectativaOI());
                expectativaOIAConsiderar.set(Calendar.DAY_OF_MONTH, 1);                   

                cal.setTime(r.getExpectativaEmissaoLicenca());
                cal.add(Calendar.DAY_OF_MONTH, 1);
                
                if (expectativaOIAConsiderar.compareTo(cal) > 0) {
                    r.setDiasForaDoPrazo(0);
                } else {
                    Long diasForaPrazoEmMillis = cal.getTimeInMillis() - expectativaOIAConsiderar.getTimeInMillis();
                    diasForaPrazoEmMillis = diasForaPrazoEmMillis / (1000 * 60 * 60 * 24);
                    r.setDiasForaDoPrazo(diasForaPrazoEmMillis.intValue());
                }
            }
            
            if ( (filtros.exists(RelatorioAcompanhamentoAmbiental.Filtro.STATUS_PROCESSO) 
                    && filtros.get(RelatorioAcompanhamentoAmbiental.Filtro.STATUS_PROCESSO, List.class).contains(r.getSituacaoProcessoId())) 
                    || !filtros.exists(RelatorioAcompanhamentoAmbiental.Filtro.STATUS_PROCESSO)) {
                relatorioAcompanhamentoAmbientalFiltradoList.add(r);
            }            
                        
        }                
                        
        return relatorioAcompanhamentoAmbientalFiltradoList;      
    }

    public Response generateReport(Supplier<Map<String,Object>> parametros,String format, List<Estagio> estagios, List<Long> analistas, List<Long> cadeiasProdutivas, List<Long> gerencias, List<Estagio> estagios0, 
            List<SituacaoProjetoEnum> status, List<Long> regioesPlanejamento, List<Integer> statusProcesso, 
            List<EnumTipoMeioAmbiente> etapas) throws IOException {
        QueryParameter filtros = QueryParameter.with(RelatorioAcompanhamentoAmbiental.Filtro.ESTAGIO, estagios)
                .and(RelatorioAcompanhamentoAmbiental.Filtro.ANALISTA, analistas)
                .and(RelatorioAcompanhamentoAmbiental.Filtro.CADEIA_PRODUTIVA, cadeiasProdutivas)
                .and(RelatorioAcompanhamentoAmbiental.Filtro.GERENCIA, gerencias)
                .and(RelatorioAcompanhamentoAmbiental.Filtro.ESTAGIO, estagios)
                .and(RelatorioAcompanhamentoAmbiental.Filtro.STATUS_ATUAL, status)
                .and(RelatorioAcompanhamentoAmbiental.Filtro.REGIAO, regioesPlanejamento)
                .and(RelatorioAcompanhamentoAmbiental.Filtro.STATUS_PROCESSO, statusProcesso)
                .and(RelatorioAcompanhamentoAmbiental.Filtro.ETAPA, etapas);

        List<RelatorioAcompanhamentoAmbiental> result = findRelatorioAcompanhamentoAmbientalByFilters(filtros);
        
        
        
        JasperReportBuilder report = buildAmbiental(parametros.get(),format,result);
        
        
        
        return buildResponse(report, EnumMimeTypes.valueOf(format.toUpperCase()).mimeType, format);
    }

    private JasperReportBuilder buildAmbiental(Map<String,Object> parametros, String format,List<RelatorioAcompanhamentoAmbiental> result) throws IOException {

        parametros.put("LOGO_INDI",getImage(ReportUtil.LOGO_INDI_NOME));

        String titulo = "Relatório de Acompanhamento Ambiental dos Projetos";
        // Colunas do instrumento
        ColumnBuilder[] cb = {
            col.column("Empresa", "empresa", type.stringType()).setWidth(12),
            col.column("Projeto", "projeto", type.stringType()).setWidth(23),
            col.column("Cidade", "cidade", type.stringType()).setWidth(11),
            col.column("Região Planejamento", "regiaoPlanejamento", type.stringType()).setWidth(10),
            col.column("Cadeia Produtiva", "cadeiaProdutiva", type.stringType()).setWidth(9),
            col.column("Classe", "classe", type.stringType()).setWidth(5),
            col.column("Etapa", "etapa", type.stringType()).setWidth(5),
            col.column("Entrega Estudos", "dataEntrega", type.stringType()).setWidth(8),
            col.column("Expec. Emissão Licença", "licenca", type.stringType()).setWidth(8),
            col.column("Expec. OI", "expectativaOI", type.stringType()).setWidth(8),
            col.column("Status Processo", "statusProcesso", type.stringType()).setWidth(8),
            col.column("Dias Fora Prazo", "diasForaPrazo", type.stringType()).setWidth(5),
            col.column("Status Atual", "status", type.stringType()).setWidth(8),
            col.column("Estágio Atual", "estagio", type.stringType()).setWidth(9),
            col.column("Analista", "analista", type.stringType()).setWidth(11),
            col.column("Gerência", "gerencia", type.stringType()).setWidth(8)
        };

       
        return generatorChain(()-> defaultInit.init(cb,getAmbientalDataSource(result)),
                resolveHeader(format,parametros));
    }

    /*
     *   Recupera o datasource do relatório
     */
    private JRDataSource getAmbientalDataSource(List<RelatorioAcompanhamentoAmbiental> result) {
        DRDataSource dataSource = new DRDataSource("empresa", "projeto", "cidade", "regiaoPlanejamento", "cadeiaProdutiva", "classe", "etapa", "dataEntrega", "licenca", "expectativaOI", "statusProcesso", "diasForaPrazo", "status", "estagio", "analista", "gerencia");
        result.stream().forEach((to) -> {
            dataSource.add(getLinhaAmbiental(to));
        });
        return dataSource;
    }

    private String[] getLinhaAmbiental(RelatorioAcompanhamentoAmbiental to) {
        String[] linha = {to.getNomePrincipal(), 
            to.getProjeto(), to.getCidade(), to.getRegiao(), 
            to.getCadeiaProdutiva(), fromIntegerToString(to.getClasseEmpreendimento()), 
            to.getEtapas(), formatDate(to.getDataEntregaEstudos()), formatDate(to.getExpectativaEmissaoLicenca()), 
            formatDate(to.getExpectativaOI()), to.getSituacaoProcesso(), 
            fromIntegerToString(to.getDiasForaDoPrazo()), to.getSituacaoAtual(),
            to.getEstagioAtual(), to.getAnalista(), to.getDepartamento()};
        return linha;
    }

    private String fromIntegerToString(Integer i) {
        return i == null ? "" : i.toString();
    }
        
    private String formatDate(Date date) {
        return DateUtils.fmtString(date, DateUtils.ddMMyyyy);
    }
}
