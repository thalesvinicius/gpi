package br.com.gpi.server.domain.repository;

import br.com.gpi.server.domain.entity.CNAE;
import java.util.List;
import org.apache.deltaspike.data.api.EntityRepository;
import org.apache.deltaspike.data.api.Query;
import org.apache.deltaspike.data.api.Repository;
import org.apache.deltaspike.data.api.criteria.CriteriaSupport;

@Repository(forEntity = CNAE.class)
public abstract class CNAERepository implements CriteriaSupport<CNAE>, EntityRepository<CNAE, String> {
    @Query(named = CNAE.Query.findAllDivisoes)
    public abstract List<CNAE> findAllDivisoes();
    
    @Query(named = CNAE.Query.findById)
    public abstract List<CNAE> findById(List<String> ids);
    
    @Query(named = CNAE.Query.findByParentId)
    public abstract List<CNAE> findByParentId(List<String> ids);
    
    @Query(named = CNAE.Query.findByNivel)
    public abstract List<CNAE> findByNivel(Integer nivel);

}
