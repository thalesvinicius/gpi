package br.com.gpi.server.domain.entity;

import br.com.gpi.server.domain.common.BaseEntity;
import br.com.gpi.server.domain.entity.enumerated.EnumTipoPergunta;
import javax.persistence.Entity;
import javax.persistence.Table;
import org.hibernate.envers.Audited;

@Audited
@Entity
@Table(name = "PerguntaPesquisa")
public class PerguntaPesquisa extends BaseEntity<Long> {
    
    private String pergunta;
    private EnumTipoPergunta tipo;

    public String getPergunta() {
        return pergunta;
    }

    public void setPergunta(String pergunta) {
        this.pergunta = pergunta;
    }

    public EnumTipoPergunta getTipo() {
        return tipo;
    }

    public void setTipo(EnumTipoPergunta tipo) {
        this.tipo = tipo;
    }
       
}
