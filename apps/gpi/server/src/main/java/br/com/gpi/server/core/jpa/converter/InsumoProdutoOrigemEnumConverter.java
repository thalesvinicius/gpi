package br.com.gpi.server.core.jpa.converter;

import br.com.gpi.server.domain.entity.InsumoProdutoOrigem;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = false)
public class InsumoProdutoOrigemEnumConverter implements AttributeConverter<InsumoProdutoOrigem, Integer> {

    @Override
    public Integer convertToDatabaseColumn(InsumoProdutoOrigem insumoProdutoOrigem) {
        return insumoProdutoOrigem.getId();
    }

    @Override
    public InsumoProdutoOrigem convertToEntityAttribute(Integer dbData) {
        return InsumoProdutoOrigem.getInsumoProdutoOrigem(dbData);
    }

}
