package br.com.gpi.server.domain.service;

import br.com.gpi.server.domain.common.BaseDomainTableEntity;
import br.com.gpi.server.domain.common.BaseEntity;
import java.util.Collections;
import java.util.List;
import javax.inject.Inject;
import javax.persistence.CacheStoreMode;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.persistence.metamodel.EntityType;
import javax.persistence.metamodel.Metamodel;

public class DomainTableService {

    @Inject
    private EntityManager em;

    public <T extends BaseDomainTableEntity> T findByDescricao(Class<T> klass, String descricao) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery cq = cb.createQuery(klass);
        Root<T> root = cq.from(klass);
        cq.where(cb.equal(root.get("descricao"), descricao));
        TypedQuery<T> query = em.createQuery(cq);
        
        try {
            return query.getSingleResult();
        } catch(NoResultException e) {
            return null;
        }
    }

    public <T extends BaseDomainTableEntity> List<T> findAll(Class<T> klass) {
        return findAll(klass, null, null);
    }

    public <T extends BaseEntity> List<T> findAll(Class<T> klass, String orderBy, String direction) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery cq = cb.createQuery(klass);
        Metamodel m = em.getMetamodel();
        EntityType<T> T_ = m.entity(klass);
        Root<T> root = cq.from(klass);

        if (orderBy != null) {
            String fields[] = orderBy.split(",");
            for (String field : fields) {
                if (direction != null) {
                    if (direction.equals("desc")) {
                        cq.orderBy(cb.desc(root.get(T_.getSingularAttribute(field))));
                    } else {
                        cq.orderBy(cb.asc(root.get(T_.getSingularAttribute(field))));
                    }
                } else {
                    cq.orderBy(cb.asc(root.get(T_.getSingularAttribute(field))));
                }
            }
        }

        TypedQuery<T> query = em.createQuery(cq);
        query.setHint("javax.persistence.cache.storeMode", CacheStoreMode.USE);
        return query.getResultList();
    }

    public <T extends BaseDomainTableEntity> T findById(Class<T> klass, Long id) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery cq = cb.createQuery(klass);
        Root<T> root = cq.from(klass);
        cq.where(cb.equal(root.get("id"), id));
        TypedQuery<T> query = em.createQuery(cq);
        return query.getSingleResult();
    }
    
    public <T extends BaseDomainTableEntity> List<T> findByIds(Class<T> klass, List<Long> ids) {
        if(ids == null || ids.isEmpty()) return Collections.emptyList();
        
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery cq = cb.createQuery(klass);
        Root<T> root = cq.from(klass);
        cq.where(root.get("id").in(ids));
        TypedQuery<T> query = em.createQuery(cq);
        return query.getResultList();
    }
}
