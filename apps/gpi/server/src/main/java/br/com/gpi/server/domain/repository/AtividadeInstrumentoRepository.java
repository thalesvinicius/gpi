package br.com.gpi.server.domain.repository;

import br.com.gpi.server.domain.entity.AtividadeInstrumento;
import java.util.List;
import org.apache.deltaspike.data.api.EntityRepository;
import org.apache.deltaspike.data.api.Modifying;
import org.apache.deltaspike.data.api.Query;
import org.apache.deltaspike.data.api.Repository;
import org.apache.deltaspike.data.api.criteria.CriteriaSupport;

@Repository(forEntity = AtividadeInstrumento.class)
public abstract class AtividadeInstrumentoRepository  implements CriteriaSupport<AtividadeInstrumento>,EntityRepository<AtividadeInstrumento, Long>{

    @Modifying
    @Query(named = AtividadeInstrumento.Query.findAllByInstFormId)
    public abstract List<AtividadeInstrumento> findAllByInstFormId(Long id);

    
}
