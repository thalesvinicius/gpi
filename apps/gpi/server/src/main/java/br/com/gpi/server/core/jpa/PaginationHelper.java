package br.com.gpi.server.core.jpa;

public class PaginationHelper {

    private int paginationSize;
    private int pageNumber;

    public PaginationHelper(int paginationSize, int pageNumber) {
        this.paginationSize = paginationSize;
        this.pageNumber = pageNumber;
    }
    
    public int getPaginationSize() {
        return paginationSize;
    }

    public void setPaginationSize(int paginationSize) {
        this.paginationSize = paginationSize;
    }

    public int getPageNumber() {
        return pageNumber;
    }

    public void setPageNumber(int pageNumber) {
        this.pageNumber = pageNumber;
    }

    public int getFirstResult() {
        return (pageNumber-1) * (paginationSize);
    }

    public int getLastResult() {
        return pageNumber * paginationSize;
    }

}
