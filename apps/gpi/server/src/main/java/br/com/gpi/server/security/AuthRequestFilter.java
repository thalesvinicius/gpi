package br.com.gpi.server.security;

import br.com.gpi.server.config.ApplicationSettings;
import br.com.gpi.server.domain.common.UserConversation;
import br.com.gpi.server.domain.entity.AccessToken;
import br.com.gpi.server.domain.service.SessionService;
import br.com.gpi.server.domain.user.exception.TokenHasExpiredException;
import br.com.gpi.server.domain.user.exception.TokenNotFoundException;
import br.com.gpi.server.domain.user.model.GPISecurityContext;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Priority;
import javax.enterprise.inject.spi.Bean;
import javax.enterprise.inject.spi.BeanManager;
import javax.inject.Inject;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;
import org.apache.oltu.oauth2.common.OAuth;
import org.apache.oltu.oauth2.common.error.OAuthError;
import org.apache.oltu.oauth2.common.exception.OAuthProblemException;
import org.apache.oltu.oauth2.common.exception.OAuthSystemException;
import org.apache.oltu.oauth2.common.message.OAuthResponse;
import org.apache.oltu.oauth2.common.message.types.ParameterStyle;
import org.apache.oltu.oauth2.common.utils.OAuthUtils;
import org.apache.oltu.oauth2.rs.request.OAuthAccessResourceRequest;
import org.apache.oltu.oauth2.rs.response.OAuthRSResponse;

@Provider
@Priority(Priorities.AUTHENTICATION)
public class AuthRequestFilter implements ContainerRequestFilter {

    @Context
    private HttpServletRequest webRequest;
    @Inject
    private ApplicationSettings config;
    @Inject
    private SessionService sessionService;

    @Inject
    private UserConversation userConversation;
    
    
    public static String RESOURCE_SERVER_NAME = "resource";

    @Override
    public void filter(ContainerRequestContext requestContext) {
        String method = requestContext.getMethod();
        if (method.equalsIgnoreCase("OPTIONS")) {
            requestContext.abortWith(Response.ok("oauth/token").build());
        }
        doAuthenticatation(requestContext);
    }

    private void doAuthenticatation(ContainerRequestContext requestContext) {
        try {
            
            // Make the OAuth Request out of this request
            OAuthAccessResourceRequest oauthRequest = new OAuthAccessResourceRequest(webRequest, ParameterStyle.HEADER);
            // Get the access token
            String accessToken = oauthRequest.getAccessToken();

            // Validate the access token
            try {
                AccessToken recoveredSession = sessionService.recoverSession(accessToken);
                recoveredSession.getUser();
                requestContext.setSecurityContext(recoveredSession.getSecurityContext());
                if (webRequest.getUserPrincipal() != null) {
                    webRequest.login("bypass", "bypass");
                }
                userConversation.setUser(recoveredSession.getUser()); 
            } catch (TokenHasExpiredException | TokenNotFoundException ex) {
                // Return the OAuth error message
                OAuthResponse oauthResponse = OAuthRSResponse
                        .errorResponse(HttpServletResponse.SC_UNAUTHORIZED)
                        .setRealm(RESOURCE_SERVER_NAME)
                        .setError(OAuthError.ResourceResponse.INVALID_TOKEN)
                        .buildHeaderMessage();

                requestContext.abortWith(Response.status(Response.Status.UNAUTHORIZED)
                        .header(OAuth.HeaderType.WWW_AUTHENTICATE,
                                oauthResponse.getHeader(OAuth.HeaderType.WWW_AUTHENTICATE))
                        .build());
            } catch (ServletException ex) {
                Logger.getLogger(AuthRequestFilter.class.getName()).log(Level.SEVERE, null, ex);
            }

        } catch (OAuthProblemException e) {
            // Check if the error code has been set
            String errorCode = e.getError();
            if (OAuthUtils.isEmpty(errorCode)) {
                requestContext.setSecurityContext(GPISecurityContext.createDefaultSecurityContext());
            } else {
                try {
                    OAuthResponse oauthResponse;
                    oauthResponse = OAuthRSResponse
                            .errorResponse(HttpServletResponse.SC_UNAUTHORIZED)
                            .setRealm(RESOURCE_SERVER_NAME)
                            .setError(e.getError())
                            .setErrorDescription(e.getDescription())
                            .setErrorUri(e.getUri())
                            .buildHeaderMessage();

                    requestContext.abortWith(Response.status(Response.Status.BAD_REQUEST)
                            .header(OAuth.HeaderType.WWW_AUTHENTICATE, oauthResponse.getHeader(OAuth.HeaderType.WWW_AUTHENTICATE))
                            .build());
                } catch (OAuthSystemException ex) {
                    Logger.getLogger(AuthRequestFilter.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        } catch (OAuthSystemException ex) {
            Logger.getLogger(AuthRequestFilter.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
