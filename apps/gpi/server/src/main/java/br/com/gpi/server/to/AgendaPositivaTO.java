package br.com.gpi.server.to;

public class AgendaPositivaTO {
    private String nomeEmpresa;
    private String nomeProjeto;
    private String cidade;

    public AgendaPositivaTO(String nomeEmpresa, String nomeProjeto, String cidade) {
        this.nomeEmpresa = nomeEmpresa;
        this.nomeProjeto = nomeProjeto;
        this.cidade = cidade;
    }

    public String getNomeEmpresa() {
        return nomeEmpresa;
    }

    public void setNomeEmpresa(String nomeEmpresa) {
        this.nomeEmpresa = nomeEmpresa;
    }

    public String getNomeProjeto() {
        return nomeProjeto;
    }

    public void setNomeProjeto(String nomeProjeto) {
        this.nomeProjeto = nomeProjeto;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }
        
}
