package br.com.gpi.server.core.jpa.converter;

import br.com.gpi.server.domain.entity.SituacaoInstrFormalizacao;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = false)
public class SituacaoInstrFormalizacaoEnumConverter implements AttributeConverter<SituacaoInstrFormalizacao, Integer> {

    @Override
    public Integer convertToDatabaseColumn(SituacaoInstrFormalizacao situacao) {
        return situacao.getId();
    }

    @Override
    public SituacaoInstrFormalizacao convertToEntityAttribute(Integer id) {
        return SituacaoInstrFormalizacao.getSituacaoInstrFormalizacao(id);
    }

}
