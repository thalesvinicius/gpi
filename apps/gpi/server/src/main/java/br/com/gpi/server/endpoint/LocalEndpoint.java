package br.com.gpi.server.endpoint;

import br.com.gpi.server.domain.service.LocalService;
import br.com.gpi.server.to.LocalTO;
import java.util.List;
import javax.annotation.security.PermitAll;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/local")
@Stateless
@PermitAll
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
@TransactionManagement(TransactionManagementType.BEAN)
public class LocalEndpoint {
    
    @Inject
    LocalService localService;
            
    @GET
    public Response findAllActiveWithAtividades() {
        List<LocalTO> locais = localService.findAllActiveWithAtividades();
        return Response.ok(locais).build();
    }    
    
}
