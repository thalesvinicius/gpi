package br.com.gpi.server.to;

import br.com.gpi.server.domain.entity.Estagio;
import br.com.gpi.server.domain.entity.SituacaoRelatorioEnum;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.util.Date;

@JsonIgnoreProperties(ignoreUnknown = true)
public class RelatorioAcompanhamentoAnualTO {

    private Long id;
    private Long projeto_id;
    private String nomeEmpresa;
    private String nomeProjeto;
    private String emailResponsavelAreaInvestidor;
    private String situacaoRelatorioEnum;
    private String estagio;
    private String nomeUsuarioInterno;
    private Date dataValidacao;
    private Date dataUltimaAtualizacao;
    private Date dataInicioPrimeiraCampanha;
    private Date dataFimPrimeiraCampanha;
    private Date dataInicioSegundaCampanha;
    private Date dataFimSegundaCampanha;
    private Integer ano;
    private String nomeResponsavelRelatorio;
    private String telefoneResponsavelRelatorio;
    private String emailResponsavelRelatorio;
    private String observacao;

    private SituacaoRelatorioEnum situacaoRelatorio;
    private Estagio estagioProjeto;

    public RelatorioAcompanhamentoAnualTO(Long projeto_id, String nomeProjeto,
            Integer ano, String nomeEmpresa, String emailResponsavelAreaInvestidor,
            SituacaoRelatorioEnum situacaoRelatorio, Estagio estagioProjeto,
            Date dataValidacao, Date dataUltimaAtualizacao,
            Date dataInicioPrimeiraCampanha, Date dataFimPrimeiraCampanha,
            Date dataInicioSegundaCampanha, Date dataFimSegundaCampanha,
            String nomeResponsavelRelatorio) {
        this.projeto_id = projeto_id;
        this.nomeEmpresa = nomeEmpresa;
        this.nomeProjeto = nomeProjeto;
        this.emailResponsavelAreaInvestidor = emailResponsavelAreaInvestidor;
        this.dataValidacao = dataValidacao;
        this.dataUltimaAtualizacao = dataUltimaAtualizacao;
        this.dataInicioPrimeiraCampanha = dataInicioPrimeiraCampanha;
        this.dataFimPrimeiraCampanha = dataFimPrimeiraCampanha;
        this.dataInicioSegundaCampanha = dataInicioSegundaCampanha;
        this.dataFimSegundaCampanha = dataFimSegundaCampanha;
        this.ano = ano;
        this.nomeResponsavelRelatorio = nomeResponsavelRelatorio;
        this.situacaoRelatorio = situacaoRelatorio;
        this.estagioProjeto = estagioProjeto;
    }

    public RelatorioAcompanhamentoAnualTO(Integer ano, String nomeResponsavelRelatorio, String telefoneResponsavelRelatorio, String emailResponsavelRelatorio) {
        this.ano = ano;
        this.nomeResponsavelRelatorio = nomeResponsavelRelatorio;
        this.telefoneResponsavelRelatorio = telefoneResponsavelRelatorio;
        this.emailResponsavelRelatorio = emailResponsavelRelatorio;
    }

    public RelatorioAcompanhamentoAnualTO(Long id, Integer ano, String nomeResponsavelRelatorio, String telefoneResponsavelRelatorio, String emailResponsavelRelatorio, String observacao) {
        this.id = id;
        this.ano = ano;
        this.nomeResponsavelRelatorio = nomeResponsavelRelatorio;
        this.telefoneResponsavelRelatorio = telefoneResponsavelRelatorio;
        this.emailResponsavelRelatorio = emailResponsavelRelatorio;
        this.observacao = observacao;
    }

    public RelatorioAcompanhamentoAnualTO() {
    }

    public String getNomeEmpresa() {
        return nomeEmpresa;
    }

    public void setNomeEmpresa(String nomeEmpresa) {
        this.nomeEmpresa = nomeEmpresa;
    }

    public String getNomeProjeto() {
        return nomeProjeto;
    }

    public void setNomeProjeto(String nomeProjeto) {
        this.nomeProjeto = nomeProjeto;
    }

    public String getEmailResponsavelAreaInvestidor() {
        return emailResponsavelAreaInvestidor;
    }

    public void setEmailResponsavelAreaInvestidor(String emailResponsavelAreaInvestidor) {
        this.emailResponsavelAreaInvestidor = emailResponsavelAreaInvestidor;
    }

    public String getSituacaoRelatorioEnum() {
        if (situacaoRelatorioEnum == null || situacaoRelatorioEnum.trim().equals("")) {
            situacaoRelatorioEnum = SituacaoRelatorioEnum.NAO_SOLICITADO.getDescription();
        }
        return situacaoRelatorioEnum;
    }

    public void setSituacaoRelatorioEnum(String situacaoRelatorioEnum) {
        this.situacaoRelatorioEnum = situacaoRelatorioEnum;
    }

    public String getEstagio() {
        return estagio;
    }

    public void setEstagio(String estagio) {
        this.estagio = estagio;
    }

    public Date getDataValidacao() {
        return dataValidacao;
    }

    public void setDataValidacao(Date dataValidacao) {
        this.dataValidacao = dataValidacao;
    }

    public Date getDataUltimaAtualizacao() {
        return dataUltimaAtualizacao;
    }

    public void setDataUltimaAtualizacao(Date dataUltimaAtualizacao) {
        this.dataUltimaAtualizacao = dataUltimaAtualizacao;
    }

    public Date getDataInicioPrimeiraCampanha() {
        return dataInicioPrimeiraCampanha;
    }

    public void setDataInicioPrimeiraCampanha(Date dataInicioPrimeiraCampanha) {
        this.dataInicioPrimeiraCampanha = dataInicioPrimeiraCampanha;
    }

    public Date getDataFimPrimeiraCampanha() {
        return dataFimPrimeiraCampanha;
    }

    public void setDataFimPrimeiraCampanha(Date dataFimPrimeiraCampanha) {
        this.dataFimPrimeiraCampanha = dataFimPrimeiraCampanha;
    }

    public Date getDataInicioSegundaCampanha() {
        return dataInicioSegundaCampanha;
    }

    public void setDataInicioSegundaCampanha(Date dataInicioSegundaCampanha) {
        this.dataInicioSegundaCampanha = dataInicioSegundaCampanha;
    }

    public Date getDataFimSegundaCampanha() {
        return dataFimSegundaCampanha;
    }

    public void setDataFimSegundaCampanha(Date dataFimSegundaCampanha) {
        this.dataFimSegundaCampanha = dataFimSegundaCampanha;
    }

    public Integer getAno() {
        return ano;
    }

    public void setAno(Integer ano) {
        this.ano = ano;
    }

    public String getNomeResponsavelRelatorio() {
        return nomeResponsavelRelatorio;
    }

    public void setNomeResponsavelRelatorio(String nomeResponsavelRelatorio) {
        this.nomeResponsavelRelatorio = nomeResponsavelRelatorio;
    }

    public String getTelefoneResponsavelRelatorio() {
        return telefoneResponsavelRelatorio;
    }

    public void setTelefoneResponsavelRelatorio(String telefoneResponsavelRelatorio) {
        this.telefoneResponsavelRelatorio = telefoneResponsavelRelatorio;
    }

    public String getEmailResponsavelRelatorio() {
        return emailResponsavelRelatorio;
    }

    public void setEmailResponsavelRelatorio(String emailResponsavelRelatorio) {
        this.emailResponsavelRelatorio = emailResponsavelRelatorio;
    }

    public String getNomeUsuarioInterno() {
        return nomeUsuarioInterno;
    }

    public void setNomeUsuarioInterno(String nomeUsuarioInterno) {
        this.nomeUsuarioInterno = nomeUsuarioInterno;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public Long getProjeto_id() {
        return projeto_id;
    }

    public void setProjeto_id(Long projeto_id) {
        this.projeto_id = projeto_id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public SituacaoRelatorioEnum getSituacaoRelatorio() {
        return situacaoRelatorio;
    }

    public void setSituacaoRelatorio(SituacaoRelatorioEnum situacaoRelatorio) {
        this.situacaoRelatorio = situacaoRelatorio;
    }

    public Estagio getEstagioProjeto() {
        return estagioProjeto;
    }

    public void setEstagioProjeto(Estagio estagioProjeto) {
        this.estagioProjeto = estagioProjeto;
    }
}
