/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.gpi.server.util.functional;

import br.com.gpi.server.util.EnumMimeTypes;
import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.List;
import java.util.function.Supplier;
import javassist.bytecode.ByteArray;
import net.sf.dynamicreports.jasper.builder.JasperReportBuilder;
import net.sf.dynamicreports.report.builder.column.ColumnBuilder;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperPrint;
import org.apache.commons.lang3.tuple.Pair;

/**
 *
 * @author rafaelbfs
 */
public interface FunctionalInterfaces {
    
    @FunctionalInterface
    public interface BiFunctionChecked<T1,T2,R> {
        public R apply(T1 p1, T2 p2) throws Exception;
    } 
    
    @FunctionalInterface
    public interface QueryExecutor {
        public PreparedStatement execute(Connection connection) throws Exception;
        
    }
    
    @FunctionalInterface
    public interface ParameterizedSelectBuilder {
        public Pair<String,List<Object>> build() throws Exception;
        
    }
    
    @FunctionalInterface
    public interface JaspeReportBuilderFunction{
        public JasperReportBuilder build(JasperReportBuilder previous);
    }
    
    @FunctionalInterface
    public interface  ReportInitializer {
        public JasperReportBuilder init(ColumnBuilder[] columns, JRDataSource dataSource);
    }
    
    @FunctionalInterface
    public interface TriFunction<T1,T2,T3,R> {
        public R apply(T1 p1, T2 p2, T3 p3);
    }
    
    @FunctionalInterface
    public interface MultiPrintExporter {
        public ByteArrayOutputStream export(List<JasperPrint> prints) throws JRException;
        
    }
    
    @FunctionalInterface
    public interface BinarySupplier {
        public ByteArrayOutputStream get() throws Exception;
    }
    
    
    
}
