package br.com.gpi.server.domain.entity;

import br.com.gpi.server.domain.common.BaseEntity;
import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import org.hibernate.envers.Audited;

@Audited
@Entity
@Table(name = "Municipio")
@NamedQueries({
    @NamedQuery(name = Municipio.Query.FIND_BY_ID, query = "SELECT m from Municipio m where m.uf.id in ?1")
})
@Cacheable
public class Municipio extends BaseEntity<Long> {

    @NotNull
    @Column(length = 255, nullable = false)
    private String nome;

    @ManyToOne(cascade = {CascadeType.DETACH, CascadeType.REFRESH})
    @JoinColumn(name = "uf_id")
    private UF uf;

    @ManyToOne(cascade = {CascadeType.DETACH, CascadeType.REFRESH})
    @JoinColumn(name = "microRegiao_id", updatable = false, insertable = false)
    private MicroRegiao microRegiao;

    @ManyToOne(cascade = {CascadeType.DETACH, CascadeType.REFRESH})
    @JoinColumn(name = "regiaoPlanejamento_id", updatable = false, insertable = false)
    private RegiaoPlanejamento regiaoPlanejamento;

    public Municipio() {
    }

    public String getNome() {
        return this.nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public UF getUf() {
        return uf;
    }

    public void setUf(UF uf) {
        this.uf = uf;
    }

    public MicroRegiao getMicroRegiao() {
        return microRegiao;
    }

    public void setMicroRegiao(MicroRegiao microRegiao) {
        this.microRegiao = microRegiao;
    }

    public RegiaoPlanejamento getRegiaoPlanejamento() {
        return regiaoPlanejamento;
    }

    public void setRegiaoPlanejamento(RegiaoPlanejamento regiaoPlanejamento) {
        this.regiaoPlanejamento = regiaoPlanejamento;
    }

    public static class Query {
        public static final String FIND_BY_ID = "Municipio.findById";
       
    }

}
