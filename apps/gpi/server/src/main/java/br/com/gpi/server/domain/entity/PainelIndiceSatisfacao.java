package br.com.gpi.server.domain.entity;

import br.com.gpi.server.domain.common.BaseEntity;
import javax.persistence.Entity;
import javax.persistence.Table;
import org.hibernate.envers.Audited;

@Entity
@Table(name = "view_painel_indice_satisfacao")
public class PainelIndiceSatisfacao extends BaseEntity<Long>{
    private String mesAno;
    private Integer mes;
    private Integer ano;
    private String diretoria;
    private Long diretoriaId;
    private Double percentual;

    public String getMesAno() {
        return mesAno;
    }

    public void setMesAno(String mesAno) {
        this.mesAno = mesAno;
    }

    public Integer getMes() {
        return mes;
    }

    public void setMes(Integer mes) {
        this.mes = mes;
    }

    public Integer getAno() {
        return ano;
    }

    public void setAno(Integer ano) {
        this.ano = ano;
    }
    
    public String getDiretoria() {
        return diretoria;
    }

    public void setDiretoria(String diretoria) {
        this.diretoria = diretoria;
    }

    public Long getDiretoriaId() {
        return diretoriaId;
    }

    public void setDiretoriaId(Long diretoriaId) {
        this.diretoriaId = diretoriaId;
    }
    
    public Double getPercentual() {
        return percentual;
    }

    public void setPercentual(Double percentual) {
        this.percentual = percentual;
    }

}
