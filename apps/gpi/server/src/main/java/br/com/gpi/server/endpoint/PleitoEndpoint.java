package br.com.gpi.server.endpoint;

import br.com.gpi.server.domain.entity.Pleito;
import br.com.gpi.server.domain.entity.User;
import br.com.gpi.server.domain.repository.PleitoRepository;
import br.com.gpi.server.domain.repository.ProjetoRepository;
import br.com.gpi.server.domain.repository.UserRepository;
import br.com.gpi.server.domain.service.PleitoService;
import br.com.gpi.server.domain.user.model.SecurityUser;
import java.util.Collection;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.inject.Inject;
import javax.transaction.SystemException;
import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

@Path("/projeto/{idProjeto}/pleito")
@Stateless
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
@TransactionManagement(TransactionManagementType.BEAN)
public class PleitoEndpoint {

    @Inject
    private PleitoService pleitoService;
    @Inject
    private PleitoRepository pleitoRepository;
    @Inject
    private ProjetoRepository projetoRepository;
    @Inject
    private UserRepository userRepository;

    @PathParam("idProjeto")
    private Long idProjeto;

    @POST
    public Response saveOrUpdate(@Valid Pleito pleito, @Context SecurityContext context) {
        try {
            SecurityUser loggedUserWrap = (SecurityUser) context.getUserPrincipal();
            User user = userRepository.findBy(loggedUserWrap.getId());
            return Response.ok(pleitoService.saveOrUpdate(pleito, user, idProjeto)).build();
        } catch (IllegalStateException | SecurityException | SystemException ex) {
            Logger.getLogger(PleitoEndpoint.class.getName()).log(Level.SEVERE, null, ex);
            throw new RuntimeException(ex);
        }
    }

    @GET
    @Path("{id}")
    public Pleito getPleito(@PathParam("id") Long id) {
        return pleitoRepository.findBy(id);
    }

    @GET
    public Response findAllPleitosByIdProjeto() {
        List<Pleito> pleitos = pleitoRepository.locateByProjetoId(idProjeto);
        return Response.ok(pleitos).build();
    }

    @POST
    @Path("validate")
    public Response validateICE(@Context SecurityContext context) {
        SecurityUser loggedUserWrap = (SecurityUser) context.getUserPrincipal();
        Collection<String> messages = pleitoService.validation(idProjeto, loggedUserWrap.getId());
        if (messages.isEmpty()) {
            return Response.ok().build();
        }
        return Response.ok(messages).build();
    }

    @POST
    @Path("accept")
    public Response acceptICE(@Context SecurityContext context) {
        SecurityUser loggedUserWrap = (SecurityUser) context.getUserPrincipal();
        pleitoService.acceptICE(idProjeto, loggedUserWrap.getId());
        return Response.ok().build();
    }

        @GET
    @Path("findChange")
    public Response findChange() {
        Object pleito = null;
        try {
            pleito = pleitoService.verifyChange(idProjeto);
        } catch (SystemException ex) {
            Logger.getLogger(PleitoEndpoint.class.getName()).log(Level.SEVERE, null, ex);
        }
        return Response.ok(pleito).build();
    }
}
