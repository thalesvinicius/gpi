package br.com.gpi.server.domain.entity;

import br.com.gpi.server.domain.common.BaseEntity;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "Parametro")
@NamedQueries({
    @NamedQuery(name = Parametro.Query.locateById, query = "SELECT p FROM Parametro p WHERE p.id = ?"),
    @NamedQuery(name = Parametro.Query.locateByName, query = "SELECT p FROM Parametro p WHERE p.nome = ?")
})
public class Parametro extends BaseEntity<Long> {
    
    private String nome;
    private String valor;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }
    
    public static class Query {
        public static final String locateById = "Parametro.locateById";
        public static final String locateByName = "Parametro.locateByName";
    }
}
