package br.com.gpi.server.domain.repository;

import br.com.gpi.server.domain.entity.Emprego;
import java.util.List;
import org.apache.deltaspike.data.api.EntityRepository;
import org.apache.deltaspike.data.api.Modifying;
import org.apache.deltaspike.data.api.Query;
import org.apache.deltaspike.data.api.Repository;
import org.apache.deltaspike.data.api.criteria.CriteriaSupport;

@Repository(forEntity = Emprego.class)
public abstract class EmpregoRepository implements CriteriaSupport<Emprego>, EntityRepository<Emprego, Long> {


    @Modifying
    @Query(value = "DELETE FROM Emprego e WHERE e.id IN ?1")
    public abstract void removeByIds(List<Long> ids);
    
    @Modifying
    @Query(named = Emprego.Query.locateByProjetoId)
    public abstract List<Emprego> locateByProjetoId(Long id);
}
