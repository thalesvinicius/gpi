package br.com.gpi.server.to;

public class DescricaoAlertaTO {
    private String descricao;
    private Long id;
    private String link;
    
    public DescricaoAlertaTO(){
    }
    
    public DescricaoAlertaTO(Object [] descricao, String link) {
        this.descricao = (String) descricao[0];
        this.id = (Long) descricao[1];
        this.link = link;
    }
    
    public DescricaoAlertaTO(String descricao, Long id, String link) {
        this.descricao = descricao;
        this.id = id;
        this.link = link;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }
    
}
