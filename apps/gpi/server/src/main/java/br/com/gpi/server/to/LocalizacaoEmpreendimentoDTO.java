package br.com.gpi.server.to;

import br.com.gpi.server.domain.entity.MicroRegiao;
import br.com.gpi.server.domain.entity.Municipio;
import br.com.gpi.server.domain.entity.RegiaoPlanejamento;

public class LocalizacaoEmpreendimentoDTO {

    private RegiaoPlanejamento regiaoPlanejamento;
    private MicroRegiao microRegiao;
    private Municipio municipio;

    public LocalizacaoEmpreendimentoDTO() {
    }

    public LocalizacaoEmpreendimentoDTO(RegiaoPlanejamento regiaoDePlanejamento, MicroRegiao microRegiao, Municipio municipio) {
        this.regiaoPlanejamento = regiaoDePlanejamento;
        this.microRegiao = microRegiao;
        this.municipio = municipio;
    }

    public LocalizacaoEmpreendimentoDTO(RegiaoPlanejamento regiaoDePlanejamento, MicroRegiao microRegiao) {
        this.regiaoPlanejamento = regiaoDePlanejamento;
        this.microRegiao = microRegiao;
    }

    public LocalizacaoEmpreendimentoDTO(RegiaoPlanejamento regiaoDePlanejamento) {
        this.regiaoPlanejamento = regiaoDePlanejamento;
    }

    public RegiaoPlanejamento getRegiaoPlanejamento() {
        return regiaoPlanejamento;
    }

    public void setRegiaoPlanejamento(RegiaoPlanejamento regiaoDePlanejamento) {
        this.regiaoPlanejamento = regiaoDePlanejamento;
    }

    public MicroRegiao getMicroRegiao() {
        return microRegiao;
    }

    public void setMicroRegiao(MicroRegiao microRegiao) {
        this.microRegiao = microRegiao;
    }

    public Municipio getMunicipio() {
        return municipio;
    }

    public void setMunicipio(Municipio municipio) {
        this.municipio = municipio;
    }
    
    public String getFormatedName() {        
        StringBuilder sb = new StringBuilder();
        
        if (this.getMunicipio() == null && this.getMicroRegiao() == null) {           
            sb.append(" [ Região ] ");
            sb.append(this.getRegiaoPlanejamento().getNome());
        } else if (this.getMunicipio() == null) {
            sb.append(" [ Microrregião ] ");
            sb.append(this.getMicroRegiao().getNome());            
        } else {
            sb.append(" [ Município ] ");
            sb.append(this.getMunicipio().getNome());                        
        }
        return sb.toString();
    }

}
