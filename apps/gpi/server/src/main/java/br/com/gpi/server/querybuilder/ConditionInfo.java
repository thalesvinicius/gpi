package br.com.gpi.server.querybuilder;

import java.util.List;

public abstract class ConditionInfo {

    private Class instance;
    private ConditionValueTypeEnum valueType;
    private ConditionComparisonTypeEnum comparisonType;
    
    public ConditionInfo(Class instance, ConditionValueTypeEnum type, ConditionComparisonTypeEnum comparisonType) {
        this(instance, type);
        this.comparisonType = comparisonType;
    }
    
    public ConditionInfo(Class instance, ConditionValueTypeEnum type) {
        this.instance = instance;
        this.valueType = type;
    }

    public Class getInstance() {
        return instance;
    }

    public void setInstance(Class instance) {
        this.instance = instance;
    }

    /**
     * Used to determine if will be neccessary to use '' or not around value
 Returns the valueType of the value that will be used on query
     *
     * @return
     */
    public ConditionValueTypeEnum getType() {
        return valueType;
    }

    public void setType(ConditionValueTypeEnum type) {
        this.valueType = type;
    }

    public ConditionComparisonTypeEnum getComparisonType() {
        return comparisonType;
    }

    public void setComparisonType(ConditionComparisonTypeEnum comparisonType) {
        this.comparisonType = comparisonType;
    }
    
    public abstract List<Field> getFields();
    
}
