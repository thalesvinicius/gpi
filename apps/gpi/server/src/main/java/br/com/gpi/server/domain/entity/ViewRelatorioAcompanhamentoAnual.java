package br.com.gpi.server.domain.entity;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "VIEW_RELATORIO_ACOPANHAMENTO_ANUAL")
public class ViewRelatorioAcompanhamentoAnual implements Serializable {
    
    @Id
    @Column(name = "idUltimaVersao")
    private Long id;
    
    private String nomeProjeto;
    @Id
    private Integer ano;
    
    @Column(name = "nomePrincipal")
    private String nomeEmpresa;
    
    @Column(name = "email")
    private String emailResponsavelAreaInvestidor;
    
    @Column(name = "situacaoAtual_id")
    private SituacaoRelatorioEnum situacaoAtual;
    
    @Column(name = "estagioAtual_id")
    private Estagio estagio;
    
    @Column(name = "cadeiaProdutiva_id")
    private Long cadeiaProdutiva;
    
    @Column(name = "departamento_id")
    private Long departamento;
    
    @Column(name = "usuario_id")
    private Long usuario;
    
    private Integer anoEnviado;

    private Date dataValidacao;
    
    private Date dataUltimaAlteracao;

    private Date dataInicioPrimeiraCampanha;
    
    private Date dataFimPrimeiraCampanha;
    
    private Date dataInicioSegundaCampanha;
    
    private Date dataFimSegundaCampanha;
    
    @Column(name = "nomeUsuario")
    private String nomeUsuarioInterno;
    
    public ViewRelatorioAcompanhamentoAnual copiarNovoComAnoCorrente(int ano_, SituacaoRelatorioEnum situacao_){
        return new ViewRelatorioAcompanhamentoAnual(id, nomeProjeto, ano_, nomeEmpresa, 
                emailResponsavelAreaInvestidor, situacao_, estagio, cadeiaProdutiva, departamento, usuario, 
                nomeUsuarioInterno);
    }

    public ViewRelatorioAcompanhamentoAnual(Long id, String nomeProjeto, Integer ano, String nomeEmpresa, String emailResponsavelAreaInvestidor, SituacaoRelatorioEnum situacaoAtual, Estagio estagio, Long cadeiaProdutiva, Long departamento, Long usuario, String nomeUsuarioInterno) {
        this.id = id;
        this.nomeProjeto = nomeProjeto;
        this.ano = ano;
        this.nomeEmpresa = nomeEmpresa;
        this.emailResponsavelAreaInvestidor = emailResponsavelAreaInvestidor;
        this.situacaoAtual = situacaoAtual;
        this.estagio = estagio;
        this.cadeiaProdutiva = cadeiaProdutiva;
        this.departamento = departamento;
        this.usuario = usuario;
        this.nomeUsuarioInterno = nomeUsuarioInterno;
    }
    
    
    public ViewRelatorioAcompanhamentoAnual(){}

    public Long getId() {
        return id;
    }

    public void setId(Long familiaId) {
        this.id = familiaId;
    }

    public String getNomeProjeto() {
        return nomeProjeto;
    }

    public void setNomeProjeto(String nomeProjeto) {
        this.nomeProjeto = nomeProjeto;
    }

    public Integer getAno() {
        return ano;
    }

    public void setAno(Integer ano) {
        this.ano = ano;
    }

    public String getNomeEmpresa() {
        return nomeEmpresa;
    }

    public void setNomeEmpresa(String nomeEmpresa) {
        this.nomeEmpresa = nomeEmpresa;
    }

    public String getEmailResponsavelAreaInvestidor() {
        return emailResponsavelAreaInvestidor;
    }

    public void setEmailResponsavelAreaInvestidor(String emailResponsavelAreaInvestidor) {
        this.emailResponsavelAreaInvestidor = emailResponsavelAreaInvestidor;
    }

    public Estagio getEstagio() {
        return estagio;
    }

    public void setEstagio(Estagio estagio) {
        this.estagio = estagio;
    }

    public SituacaoRelatorioEnum getSituacaoAtual() {
        return situacaoAtual;
    }

    public String getSituacaoRelatorioEnum() {
        if (situacaoAtual == null || situacaoAtual.getDescription().trim().equals("")) {
            situacaoAtual = SituacaoRelatorioEnum.NAO_SOLICITADO;
        }
        return situacaoAtual.getDescription();
    }
        
    public void setSituacaoAtual(SituacaoRelatorioEnum situacaoAtual) {
        this.situacaoAtual = situacaoAtual;
    }

    public Estagio getEstagioAtual() {
        return estagio;
    }

    public String getDescricaoEstagioAtual() {
        return estagio.getDescricao();
    }
        
    public void setEstagioAtual(Estagio estagioAtual) {
        this.estagio = estagioAtual;
    }

    public Date getDataValidacao() {
        return dataValidacao;
    }

    public void setDataValidacao(Date dataValidacao) {
        this.dataValidacao = dataValidacao;
    }

    public Date getDataUltimaAlteracao() {
        return dataUltimaAlteracao;
    }

    public void setDataUltimaAlteracao(Date dataUltimaAlteracao) {
        this.dataUltimaAlteracao = dataUltimaAlteracao;
    }

    public Date getDataInicioPrimeiraCampanha() {
        return dataInicioPrimeiraCampanha;
    }

    public void setDataInicioPrimeiraCampanha(Date dataInicioPrimeiraCampanha) {
        this.dataInicioPrimeiraCampanha = dataInicioPrimeiraCampanha;
    }

    public Date getDataFimPrimeiraCampanha() {
        return dataFimPrimeiraCampanha;
    }

    public void setDataFimPrimeiraCampanha(Date dataFimPrimeiraCampanha) {
        this.dataFimPrimeiraCampanha = dataFimPrimeiraCampanha;
    }

    public Date getDataInicioSegundaCampanha() {
        return dataInicioSegundaCampanha;
    }

    public void setDataInicioSegundaCampanha(Date dataInicioSegundaCampanha) {
        this.dataInicioSegundaCampanha = dataInicioSegundaCampanha;
    }

    public Date getDataFimSegundaCampanha() {
        return dataFimSegundaCampanha;
    }

    public void setDataFimSegundaCampanha(Date dataFimSegundaCampanha) {
        this.dataFimSegundaCampanha = dataFimSegundaCampanha;
    }

    public String getNomeUsuarioInterno() {
        return nomeUsuarioInterno;
    }

    public void setNomeUsuarioInterno(String nomeUsuarioInterno) {
        this.nomeUsuarioInterno = nomeUsuarioInterno;
    }

    public Long getCadeiaProdutiva() {
        return cadeiaProdutiva;
    }

    public void setCadeiaProdutiva(Long cadeiaProdutiva) {
        this.cadeiaProdutiva = cadeiaProdutiva;
    }

    public Long getDepartamento() {
        return departamento;
    }

    public void setDepartamento(Long departamento) {
        this.departamento = departamento;
    }

    public Long getUsuario() {
        return usuario;
    }

    public void setUsuario(Long usuario) {
        this.usuario = usuario;
    }

    public String getPeriodoPrimeiraCampanha() {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        String periodoPrimeiraCampanha = "";
        if (dataInicioPrimeiraCampanha != null && dataFimPrimeiraCampanha != null) {
            periodoPrimeiraCampanha = sdf.format(dataInicioPrimeiraCampanha) + " a " + sdf.format(dataFimPrimeiraCampanha);
        }
        
        return periodoPrimeiraCampanha;
    }

    public String getPeriodoSegundaCampanha() {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        String periodoSegundaCampanha = "";
        if (this.dataInicioSegundaCampanha != null && this.dataFimSegundaCampanha != null) {
            periodoSegundaCampanha = sdf.format(this.dataInicioSegundaCampanha) + " a " + sdf.format(this.dataFimSegundaCampanha);
        }
        
        return periodoSegundaCampanha;
    }
    
    public String getEnviarTerceiraCampanha() {
        Date dataAtual = new Date();
        if (this.dataFimSegundaCampanha != null && 
                dataAtual.after(this.dataFimSegundaCampanha)) {
            return "Sim";
        }
        return "";
    }

    public Integer getAnoEnviado() {
        return anoEnviado;
    }

    public void setAnoEnviado(Integer anoEnviado) {
        this.anoEnviado = anoEnviado;
    }
    
}
