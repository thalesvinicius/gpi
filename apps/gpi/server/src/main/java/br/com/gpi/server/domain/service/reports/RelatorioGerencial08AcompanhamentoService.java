/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.gpi.server.domain.service.reports;

import br.com.gpi.server.domain.common.QueryParameter;
import br.com.gpi.server.domain.entity.Estagio;
import br.com.gpi.server.domain.entity.SituacaoProjetoEnum;
import br.com.gpi.server.domain.entity.RelatorioGerencial08Acompanhamento;
import br.com.gpi.server.domain.entity.RelatorioGerencial08Acompanhamento_;
import br.com.gpi.server.util.EnumMimeTypes;
import br.com.gpi.server.util.functional.FunctionalInterfaces;
import br.com.gpi.server.util.reports.ReportUtil;
import java.awt.Color;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.function.BiFunction;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.ws.rs.core.Response;
import net.sf.dynamicreports.jasper.builder.JasperReportBuilder;
import net.sf.dynamicreports.report.builder.DynamicReports;
import static net.sf.dynamicreports.report.builder.DynamicReports.cmp;
import static net.sf.dynamicreports.report.builder.DynamicReports.col;
import static net.sf.dynamicreports.report.builder.DynamicReports.type;
import static net.sf.dynamicreports.report.builder.DynamicReports.grp;
import static net.sf.dynamicreports.report.builder.DynamicReports.sbt;
import static net.sf.dynamicreports.report.builder.DynamicReports.stl;
import net.sf.dynamicreports.report.builder.column.TextColumnBuilder;
import net.sf.dynamicreports.report.builder.column.ValueColumnBuilder;
import net.sf.dynamicreports.report.builder.component.ComponentBuilder;
import net.sf.dynamicreports.report.builder.group.ColumnGroupBuilder;
import net.sf.dynamicreports.report.builder.style.StyleBuilder;
import net.sf.dynamicreports.report.builder.subtotal.SubtotalBuilder;
import net.sf.dynamicreports.report.constant.GroupHeaderLayout;
import net.sf.dynamicreports.report.constant.HorizontalAlignment;
import net.sf.dynamicreports.report.constant.LineStyle;
import net.sf.dynamicreports.report.datasource.DRDataSource;
import net.sf.jasperreports.engine.JRDataSource;

/**
 *
 * @author rafaelbfs
 */
public class RelatorioGerencial08AcompanhamentoService extends BaseReportService<RelatorioGerencial08AcompanhamentoService> {
    
    @Inject
    public RelatorioGerencial08AcompanhamentoService(EntityManager em) {
        super(em);
    }
    
    public List<RelatorioGerencial08Acompanhamento> pesquisar(List<Estagio> estagios, List<Long> cadeiasProdutivas,
             List<SituacaoProjetoEnum> situacoesProjeto, List<Long> departamentos, List<Long> regioesPlanejamento,
             List<Long> usuariosResponsaveis, String nomeEmpresa){
        
        QueryParameter filtros = QueryParameter.with(
            RelatorioGerencial08Acompanhamento.Filtro.ESTAGIO, estagios)
            .and(RelatorioGerencial08Acompanhamento.Filtro.EMPRESA, nomeEmpresa)
            .and(RelatorioGerencial08Acompanhamento.Filtro.ANALISTA,usuariosResponsaveis)
            .and(RelatorioGerencial08Acompanhamento.Filtro.CADEIA_PRODUTIVA,cadeiasProdutivas)
            .and(RelatorioGerencial08Acompanhamento.Filtro.GERENCIA,departamentos)
            .and(RelatorioGerencial08Acompanhamento.Filtro.STATUS_ATUAL,
                    situacoesProjeto.stream().map(SituacaoProjetoEnum::getId).collect(Collectors.toList()))
            .and(RelatorioGerencial08Acompanhamento.Filtro.REGIAO,regioesPlanejamento);
        
        List<RelatorioGerencial08Acompanhamento> resultado = pesquisar(filtros);
        return resultado;
    }
    
    
    public Response relatorio(List<RelatorioGerencial08Acompanhamento> resultado,String format,Supplier<Map<String,Object>> parametros){

        final TextColumnBuilder<BigDecimal> investPrev = col.column("Invest. Previsto R$(mil)", "investimentoPrevisto", type.bigDecimalType()).setPattern("#,##0.00").setWidth(9);

        final TextColumnBuilder<BigDecimal> investRealizado = col.column("Invest. Realizado R$(mil)", "investimentoRealizado", type.bigDecimalType()).setPattern("#,##0.00").setWidth(9);
            

        final TextColumnBuilder<BigDecimal> fatPrevisto = col.column("Fat. Previsto R$(mil)", "faturamentoPrevisto", type.bigDecimalType()).setPattern("#,##0.00").setWidth(9);

        final TextColumnBuilder<BigDecimal> fatRealizado = col.column("Fat. Realizado R$(mil)", "faturamentoRealizado", type.bigDecimalType()).setPattern("#,##0.00").setWidth(9);

        final TextColumnBuilder<Integer> empDiretos = col.column("Empregos Diretos Previstos", "empregosPermanentesDiretos", type.integerType()).setWidth(6);
        final TextColumnBuilder<Integer> empInd = col.column("Empregos Diretos Realizados", "empregoPermanentesDiretosRealizados", type.integerType()).setWidth(7);
        
        
        ValueColumnBuilder[] cb = {col.column("Empresa", "empresa", type.stringType()).setWidth(15),
            col.column("Projeto", "nomeProjeto", type.stringType()).setWidth(20),
            col.column("Cidade", "cidade", type.stringType()).setWidth(9),
            col.column("Região Planejamento", "regiaoPlanejamento", type.stringType()).setWidth(10),
            col.column("Cadeia Produtiva", "cadeiaProdutiva", type.stringType()).setWidth(9),
            col.column("Status", "statusAtual", type.stringType()).setWidth(5),
            col.column("Estagio Atual", "estagioAtual", type.stringType()).setWidth(9),
            col.column("Analista", "analista", type.stringType()).setWidth(8),
            col.column("Gerência", "gerencia", type.stringType()).setWidth(7),
            col.column("Ano", "ano", type.integerType()).setWidth(5).setPattern("0000"),            
            investPrev,
            investRealizado,
            fatPrevisto,
            fatRealizado,
            empDiretos,
            empInd
        };      
        
        StyleBuilder stlLineSubTotal = stl.style(stl.fontArialBold().setFontSize(7)).setLeftIndent(3).setRightIndent(3).setRightPadding(2).setTopPadding(4).setBottomPadding(2).setBorder(stl.pen(new Float("0.25"), LineStyle.SOLID));
        StyleBuilder stlSubTotal = stl.style(stl.fontArialBold().setFontSize(7)).setLeftIndent(3).setRightIndent(3).setRightPadding(2).setTopPadding(4).setBottomPadding(2).setBorder(stl.pen(new Float("0.25"), LineStyle.SOLID)).setBackgroundColor(Color.LIGHT_GRAY);
        
        Supplier<JasperReportBuilder> initializer = () -> defaultInit.init(cb, dataSource(resultado));
        
        ColumnGroupBuilder agrEmpresa = grp.group(cb[1]).setHideColumn(Boolean.FALSE);
        agrEmpresa.setHeaderWithSubtotal(Boolean.FALSE).setHeaderLayout(GroupHeaderLayout.EMPTY);

        
        FunctionalInterfaces.JaspeReportBuilderFunction subTotalGrouper = (rpt) -> {
            
            List<SubtotalBuilder<?,?>> subs = new ArrayList<>();
            for (int i = 0; i < 9; i++) {
                subs.add(sbt.text(" ",cb[i]).setStyle(stlLineSubTotal));
            }
            subs.add(sbt.text("Total",cb[9]).setStyle(stlSubTotal));
            subs.addAll(Arrays.asList(sbt.sum(investPrev).setStyle(stlSubTotal),
                    sbt.sum(investRealizado).setStyle(stlSubTotal),sbt.sum(fatPrevisto).setStyle(stlSubTotal),
                    sbt.sum(fatRealizado).setStyle(stlSubTotal),sbt.sum(empDiretos).setStyle(stlSubTotal),
                    sbt.sum(empInd).setStyle(stlSubTotal)));
            
            rpt.groupBy(agrEmpresa);
            rpt.subtotalsAtGroupFooter(agrEmpresa,subs.toArray(new SubtotalBuilder[]{}));      
            
            return rpt;
        };
        
        Supplier<Map<String, Object>> parametrosFinais = () -> {
            Map<String, Object> params = parametros.get();
            //params.putAll(gerarParametrosTotais(result));
            params.put("LOGO_INDI",getImage(ReportUtil.LOGO_INDI_NOME));
            params.put("TITULO","Relatório de Acompanhamento" );
            return params;
        };     
        
        
        
        final Map<String, Object> params = parametrosFinais.get();
        
         FunctionalInterfaces.JaspeReportBuilderFunction sumary = format.equalsIgnoreCase("XLSX") ? 
                (noop) -> noop :
                (re) -> re.addSummary(buildSummary(params));
        
        
        JasperReportBuilder finalReport = generatorChain(initializer,
                resolveHeader(format, params),subTotalGrouper,
                sumary);
        
        final EnumMimeTypes mimetype = EnumMimeTypes.valueOf(format.toUpperCase());
        
        FunctionalInterfaces.BinarySupplier exporter = () -> {
            if(EnumMimeTypes.XLSX.equals(mimetype)){
                return ReportUtil.xlsxExporter.export(Arrays.asList(finalReport.ignorePagination().toJasperPrint(),
                        gerarPrintAvulso(params, ReportUtil.CABECALHO_REL_CARTEIRA_PROJETO ),
                        DynamicReports.report().addDetail(buildSummary(params)).title(cmp.text("Total")).toJasperPrint()));
            }else {
                return mimetype.exportReport(finalReport).get();
            }
        };
        
        
//        FunctionalInterfaces.BinarySupplier sup = dynamicReportsBinarySupplier(finalReport, ReportUtil.CABECALHO_REL_CARTEIRA_PROJETO, 
//                ReportUtil.TOTAIS_RELATORIO_CART_PROJETOS, mimetype, params);
        return buildResponse(exporter,mimetype.mimeType);        
    }
    
    public JRDataSource dataSource(List<RelatorioGerencial08Acompanhamento> resultado){
        DRDataSource ds = new DRDataSource("empresa", "nomeProjeto","cidade", "regiaoPlanejamento", 
                "cadeiaProdutiva" , "statusAtual", "estagioAtual", "analista" , "gerencia", "ano",
                "investimentoPrevisto", "investimentoRealizado", "faturamentoPrevisto", 
                "faturamentoRealizado", "empregosPermanentesDiretos", "empregoPermanentesDiretosRealizados");
        
        resultado.stream().map((e) -> { 
            Object[] valores = {e.getEmpresa(),e.getNomeProjeto(),e.getCidade(),e.getRegiaoPlanejamento(),
            e.getCadeiaProdutiva(),e.getStatusAtual().getDescription(),e.getEstagioAtual().getDescricao(),e.getAnalista(),e.getGerencia(),
            e.getAno(),e.getInvestimentoPrevisto(),e.getInvestimentoRealizado(),e.getFaturamentoPrevisto(),
            e.getFaturamentoRealizado(),e.getEmpregosPermanentesDiretos(),e.getEmpregoPermanentesDiretosRealizados()};
            return valores;
        }).forEach((item) -> ds.add(item));
        return ds;
    }
    
    
    
    
    public List<RelatorioGerencial08Acompanhamento> pesquisar(QueryParameter filtros){
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<RelatorioGerencial08Acompanhamento> query = builder.createQuery(RelatorioGerencial08Acompanhamento.class).distinct(true);
        Root<RelatorioGerencial08Acompanhamento> fromRelatorioCateira = query.from(RelatorioGerencial08Acompanhamento.class);

        List<Predicate> conditions = builFiltersConditions(fromRelatorioCateira, filtros, builder);
        
        TypedQuery<RelatorioGerencial08Acompanhamento> typedQuery = entityManager.createQuery(query
                .where(conditions.toArray(new Predicate[]{}))
                .orderBy(builder.asc(fromRelatorioCateira.get(RelatorioGerencial08Acompanhamento_.empresa))
                ,builder.asc(fromRelatorioCateira.get(RelatorioGerencial08Acompanhamento_.idProjeto)),
                builder.asc(fromRelatorioCateira.get(RelatorioGerencial08Acompanhamento_.ano))));        

        return typedQuery.getResultList();
    }
    
    private List<Predicate> builFiltersConditions(Root<RelatorioGerencial08Acompanhamento> fromRelatorioCateira, QueryParameter filtros, CriteriaBuilder cb) {
        ArrayList<Predicate> conditions = new ArrayList<>();
        if(filtros.exists(RelatorioGerencial08Acompanhamento.Filtro.ESTAGIO)) {
            conditions.add(fromRelatorioCateira.get(RelatorioGerencial08Acompanhamento_.estagioAtual).in(filtros.get(RelatorioGerencial08Acompanhamento.Filtro.ESTAGIO, List.class)));
        }
        
        if(filtros.exists(RelatorioGerencial08Acompanhamento.Filtro.CADEIA_PRODUTIVA)) {
            conditions.add(fromRelatorioCateira.get(RelatorioGerencial08Acompanhamento_.cadeiaProdutivaId).in(filtros.get(RelatorioGerencial08Acompanhamento.Filtro.CADEIA_PRODUTIVA, List.class)));            
        }

        if(filtros.exists(RelatorioGerencial08Acompanhamento.Filtro.STATUS_ATUAL)) {
            conditions.add(fromRelatorioCateira.get(RelatorioGerencial08Acompanhamento_.statusId).in(filtros.get(RelatorioGerencial08Acompanhamento.Filtro.STATUS_ATUAL, List.class)));
        }
        
        if(filtros.exists(RelatorioGerencial08Acompanhamento.Filtro.REGIAO)) {
            conditions.add(fromRelatorioCateira.get(RelatorioGerencial08Acompanhamento_.regiaoPlanejamentoId).in(filtros.get(RelatorioGerencial08Acompanhamento.Filtro.REGIAO, List.class)));            
        }
        
        if(filtros.exists(RelatorioGerencial08Acompanhamento.Filtro.EMPRESA)){
            Predicate like = cb.like(fromRelatorioCateira.get(RelatorioGerencial08Acompanhamento_.empresa),
                    cb.literal("%" + filtros.get(RelatorioGerencial08Acompanhamento.Filtro.EMPRESA,String.class)+ "%"));
            
            conditions.add(like);
        }

        if(filtros.exists(RelatorioGerencial08Acompanhamento.Filtro.GERENCIA)) {
            conditions.add(fromRelatorioCateira.get(RelatorioGerencial08Acompanhamento_.gerenciaId).in(filtros.get(RelatorioGerencial08Acompanhamento.Filtro.GERENCIA, List.class)));            
            if(filtros.exists(RelatorioGerencial08Acompanhamento.Filtro.ANALISTA)) {
                conditions.add(fromRelatorioCateira.get(RelatorioGerencial08Acompanhamento_.analistaId).in(filtros.get(RelatorioGerencial08Acompanhamento.Filtro.ANALISTA, List.class)));            
            }
        } else {
            if(filtros.exists(RelatorioGerencial08Acompanhamento.Filtro.ANALISTA)) {
                conditions.add(fromRelatorioCateira.get(RelatorioGerencial08Acompanhamento_.analistaId).in(filtros.get(RelatorioGerencial08Acompanhamento.Filtro.ANALISTA, List.class)));
            }
        }
        return conditions;
    }   
    
    
    private ComponentBuilder buildSummary(Map<String,Object> parametros){
         DecimalFormat df = new DecimalFormat();
        df.setGroupingUsed(true);df.setMaximumFractionDigits(0);df.setMinimumFractionDigits(0);
        return cmp.horizontalList(
                        cmp.text(parametros.get(ReportUtil.INFO_RESPONSAVEL).toString()).setHorizontalAlignment(HorizontalAlignment.RIGHT))
                        .setBaseStyle(stl.style(stl.fontArial().setFontSize(9)));
    }
    
    public List<RelatorioGerencial08Acompanhamento> totalizarPorEmpresa(List<RelatorioGerencial08Acompanhamento> resultado){
        Map<Long, List<RelatorioGerencial08Acompanhamento>> map = resultado.stream().collect(Collectors.groupingBy(RelatorioGerencial08Acompanhamento::getIdProjeto));
        BinaryOperator<BigDecimal>  addBigDecimal = (fst,snd) -> fst.add(snd);
        BiFunction<List<RelatorioGerencial08Acompanhamento>,Function<RelatorioGerencial08Acompanhamento,BigDecimal>,Stream<BigDecimal>> mappedStream = (lst,fun) -> lst.stream().map(fun);
        map.forEach((key,val) -> {
            RelatorioGerencial08Acompanhamento totalizador = new RelatorioGerencial08Acompanhamento(mappedStream.apply(val,RelatorioGerencial08Acompanhamento::getInvestimentoPrevisto ).collect(Collectors.reducing(BigDecimal.ZERO, addBigDecimal))
                    ,mappedStream.apply(val,RelatorioGerencial08Acompanhamento::getInvestimentoRealizado).collect(Collectors.reducing(BigDecimal.ZERO, addBigDecimal))
                    , mappedStream.apply(val, RelatorioGerencial08Acompanhamento::getFaturamentoPrevisto).collect(Collectors.reducing(BigDecimal.ZERO, addBigDecimal))
                    , mappedStream.apply(val, RelatorioGerencial08Acompanhamento::getFaturamentoRealizado).collect(Collectors.reducing(BigDecimal.ZERO, addBigDecimal))
                    ,val.stream().collect(Collectors.summingInt(RelatorioGerencial08Acompanhamento::getEmpregosPermanentesDiretos))
                    , val.stream().collect(Collectors.summingInt(RelatorioGerencial08Acompanhamento::getEmpregoPermanentesDiretosRealizados)));
            totalizador.setEmpresa(val.get(0).getEmpresa());
            totalizador.setIdProjeto(val.get(0).getIdProjeto());
            totalizador.setAno(9999);
            val.add(totalizador);
        });
        Stream<RelatorioGerencial08Acompanhamento> result = map.values().stream().flatMap((lst) -> lst.stream());
        Comparator<RelatorioGerencial08Acompanhamento> comp = (r1,r2) -> {
            if(r1.getEmpresa().compareTo(r2.getEmpresa()) != 0){
                return r1.getEmpresa().compareTo(r2.getEmpresa());
            } 
            if(r1.getIdProjeto().compareTo(r2.getIdProjeto()) != 0){
                return r1.getIdProjeto().compareTo(r2.getIdProjeto());
            }
            return r1.getAno().compareTo(r2.getAno());
        } ;
        return result.sorted(comp).collect(Collectors.toList());
    }
    
   
    
    
    
    
}
