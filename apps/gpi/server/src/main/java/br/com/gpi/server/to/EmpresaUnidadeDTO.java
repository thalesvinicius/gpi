package br.com.gpi.server.to;

import br.com.gpi.server.domain.entity.Endereco;
import br.com.gpi.server.domain.entity.UsuarioExterno;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.util.List;
import java.util.Objects;

@JsonIgnoreProperties(ignoreUnknown = true)
public class EmpresaUnidadeDTO {

    Long empresaId;

    private Long unidadeId;

    private String nomePrincipal;

    private String razaoSocial;

    private List<String> nomesEmpresa;
    
    String nomeEmpresa;
    
    private String enderecoFormatado;

    private static final String ENDERECO_VAZIO = "- , -, -, - CEP: -, -";
    
    private Endereco enderecoEmpresa;
    
    private UsuarioExterno responsavelInvestimento;
    
    public EmpresaUnidadeDTO() {

    }

    public EmpresaUnidadeDTO(EmpresaDTO empresaDTO) {
        this(empresaDTO.getEmpresaId(), null, empresaDTO.getNomeEmpresa(), null, empresaDTO.getEnderecoEmpresa(), null);
    }

    public EmpresaUnidadeDTO(Long empresaId, Long unidadeId, String nomeEmpresa, String nomeUnidade, Endereco enderecoEmpresa, Endereco enderecoUnidade) {
        this.empresaId = empresaId;
        this.unidadeId = unidadeId;
        this.nomeEmpresa = nomeEmpresa;
        this.enderecoEmpresa = enderecoEmpresa;
        if (unidadeId == null) {
            this.nomePrincipal = nomeEmpresa;
            if (enderecoEmpresa != null) {
                this.enderecoFormatado = enderecoEmpresa.getEnderecoFormatado();
            } else {
                this.enderecoFormatado = ENDERECO_VAZIO;
            }
        } else {
            this.nomePrincipal = nomeUnidade;
            if (enderecoUnidade != null) {
                this.enderecoFormatado = enderecoUnidade.getEnderecoFormatado();
            } else {
                this.enderecoFormatado = ENDERECO_VAZIO;
            }

        }
    }

    public EmpresaUnidadeDTO(Long empresaId, Long unidadeId, String nomePrincipal, String razaoSocial, String enderecoFormatado) {
        this.empresaId = empresaId;
        this.unidadeId = unidadeId;
        this.nomePrincipal = nomePrincipal;
        this.razaoSocial = razaoSocial;
        this.enderecoFormatado = enderecoFormatado;
    }
    
    public EmpresaUnidadeDTO(Long empresaId, Long unidadeId, String nomePrincipal, String razaoSocial, String enderecoFormatado, UsuarioExterno responsavelInvestimento) {
        this(empresaId, unidadeId, nomePrincipal, razaoSocial, enderecoFormatado);
        this.responsavelInvestimento = responsavelInvestimento;
    }

    public EmpresaUnidadeDTO(Long empresaId, Long unidadeId, String nomePrincipal, String razaoSocial, List<String> nomesEmpresa, String enderecoFormatado) {
        this(empresaId, unidadeId, nomePrincipal, razaoSocial, enderecoFormatado);
        this.nomesEmpresa = nomesEmpresa;
    }
    
    public EmpresaUnidadeDTO(Long empresaId, Long unidadeId, String nomePrincipal, String razaoSocial, List<String> nomesEmpresa, String enderecoFormatado, UsuarioExterno responsavelInvestimento) {
        this(empresaId, unidadeId, nomePrincipal, razaoSocial, enderecoFormatado, responsavelInvestimento);
        this.nomesEmpresa = nomesEmpresa;
    }

    public Long getEmpresaId() {
        return empresaId;
    }

    public void setEmpresaId(Long empresaId) {
        this.empresaId = empresaId;
    }

    public Long getUnidadeId() {
        return unidadeId;
    }

    public void setUnidadeId(Long unidadeId) {
        this.unidadeId = unidadeId;
    }

    public String getNomePrincipal() {
        return nomePrincipal;
    }

    public void setNomePrincipal(String nomePrincipal) {
        this.nomePrincipal = nomePrincipal;
    }

    public String getRazaoSocial() {
        return razaoSocial;
    }

    public void setRazaoSocial(String razaoSocial) {
        this.razaoSocial = razaoSocial;
    }

    public List<String> getNomesEmpresa() {
        return nomesEmpresa;
    }

    public void setNomesEmpresa(List<String> nomesEmpresa) {
        this.nomesEmpresa = nomesEmpresa;
    }

    public String getEnderecoFormatado() {
        //Incluido temporáriamente para resolver erro crítico
        //FIXME: Remover a necessidade de criar uma string padrão.
        if (enderecoFormatado == null) {
            return ENDERECO_VAZIO;
        }
        return enderecoFormatado;
    }

    public void setEnderecoFormatado(String enderecoFormatado) {
        this.enderecoFormatado = enderecoFormatado;
    }

    public UsuarioExterno getResponsavelInvestimento() {
        return responsavelInvestimento;
    }

    public void setResponsavelInvestimento(UsuarioExterno responsavelInvestimento) {
        this.responsavelInvestimento = responsavelInvestimento;
    }
    
    public String getNomeExibicao() {
        StringBuilder nomeExibicaoSb = new StringBuilder();
        if (this.unidadeId != null) {
            nomeExibicaoSb.append("Unidade | ").append(nomePrincipal).append(" | ").append(getEnderecoFormatado());
        } else {
            nomeExibicaoSb.append("Empresa | ").append(nomePrincipal).append(" | ").append(getEnderecoFormatado());
        }
        return nomeExibicaoSb.toString();
    }

    public EmpresaDTO mapEmpresaDTO() {
        return new EmpresaDTO(this.empresaId, this.nomeEmpresa, this.enderecoEmpresa);
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 89 * hash + Objects.hashCode(this.empresaId);
        hash = 89 * hash + Objects.hashCode(this.unidadeId);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final EmpresaUnidadeDTO other = (EmpresaUnidadeDTO) obj;
        if (!Objects.equals(this.empresaId, other.empresaId)) {
            return false;
        }
        if (!Objects.equals(this.unidadeId, other.unidadeId)) {
            return false;
        }
        return true;
    }

}
