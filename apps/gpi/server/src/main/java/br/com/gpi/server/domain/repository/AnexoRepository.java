package br.com.gpi.server.domain.repository;

import br.com.gpi.server.domain.entity.Anexo;
import org.apache.deltaspike.data.api.EntityRepository;
import org.apache.deltaspike.data.api.Repository;
import org.apache.deltaspike.data.api.criteria.CriteriaSupport;

@Repository(forEntity = Anexo.class)
public abstract class AnexoRepository implements CriteriaSupport<Anexo>,EntityRepository<Anexo, Long> {

}
