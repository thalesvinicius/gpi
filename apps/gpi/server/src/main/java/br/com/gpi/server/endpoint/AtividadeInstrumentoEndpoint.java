package br.com.gpi.server.endpoint;

import br.com.gpi.server.domain.entity.AtividadeInstrumento;
import br.com.gpi.server.domain.repository.AtividadeInstrumentoRepository;
import br.com.gpi.server.domain.repository.AtividadeLocalRepository;
import br.com.gpi.server.domain.service.AtividadeInstrumentoService;
import br.com.gpi.server.domain.user.model.SecurityUser;
import br.com.gpi.server.to.LocalTO;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.security.PermitAll;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.inject.Inject;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;
import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

@Path("/instrumento/{idInstrumentoFormalizacao}/atividadeInstrumento")
@Stateless
@PermitAll
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
@TransactionManagement(TransactionManagementType.BEAN)
public class AtividadeInstrumentoEndpoint {

    @Inject
    private AtividadeInstrumentoRepository atividadeInstrumentoRepository;

    @Inject
    private AtividadeLocalRepository atividadeLocalRepository;

    @Inject
    private AtividadeInstrumentoService atividadeInstrumentoService;

    @Inject
    private UserTransaction userTransaction;

    @PathParam("idInstrumentoFormalizacao")
    private Long idInstrumentoFormalizacao;

    @GET
    public Response findAllByInstFormId() {
        List<AtividadeInstrumento> atividades = atividadeInstrumentoRepository.findAllByInstFormId(idInstrumentoFormalizacao);
        for (AtividadeInstrumento atividade : atividades) {
            atividade.setLocalAtividade(new LocalTO(atividade.getAtividadeLocal().getLocal().getId(), atividade.getAtividadeLocal().getLocal().getDescricao()));
            atividade.getLocalAtividade().setAtividades(atividadeLocalRepository.findAllByLocalId(atividade.getAtividadeLocal().getLocal().getId()));
        };
        return Response.ok(atividades).build();
    }

    @POST
    public Response save(@Valid AtividadeInstrumento atividade, @Context SecurityContext securityContext) throws SystemException {
        try {
            SecurityUser loggedUser = (SecurityUser) securityContext.getUserPrincipal();
            Boolean projetoVersionado = atividadeInstrumentoService.save(loggedUser.getId(), atividade, idInstrumentoFormalizacao);

            return Response.ok(projetoVersionado).build();
        } catch (Exception ex) {
            Logger.getLogger(AtividadeInstrumentoEndpoint.class.getName()).log(Level.SEVERE, null, ex);
            return Response.serverError().build();
        }
    }

    @POST
    @Path("removeOne")
    public Response removeOne(@Valid AtividadeInstrumento atividade) {
        atividadeInstrumentoRepository.remove(atividadeInstrumentoRepository.findBy(atividade.getId()));
        return Response.ok("Tudo Ok!").build();
    }

    @POST
    @Path("copyFromTemplate")
    public Response copyTemplateAtividades(@Context SecurityContext securityContext) {
        try {
            SecurityUser loggedUser = (SecurityUser) securityContext.getUserPrincipal();
            List<AtividadeInstrumento> atividadesInstrumento = atividadeInstrumentoService.copyTemplateAtividades(idInstrumentoFormalizacao, loggedUser.getId());
            return Response.ok(atividadesInstrumento).build();
        } catch (Exception ex) {
            Logger.getLogger(AtividadeInstrumentoEndpoint.class.getName()).log(Level.SEVERE, null, ex);
            return Response.serverError().build();
        }
    }

}
