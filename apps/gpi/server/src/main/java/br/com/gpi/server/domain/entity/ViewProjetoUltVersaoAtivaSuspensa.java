package br.com.gpi.server.domain.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "VIEW_PROJETO_ULT_VERSAO_ATIVA_SUSPENSA")
public class ViewProjetoUltVersaoAtivaSuspensa implements Serializable {
    
    @Id
    private Long id;
    
    @Column(name = "familia_id")
    private Long versaoPai;
    

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getVersaoPai() {
        return versaoPai;
    }

    public void setVersaoPai(Long versaoPai) {
        this.versaoPai = versaoPai;
    }

}
