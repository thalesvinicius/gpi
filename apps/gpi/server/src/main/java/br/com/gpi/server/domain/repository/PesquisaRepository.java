package br.com.gpi.server.domain.repository;

import br.com.gpi.server.domain.entity.Pesquisa;
import java.util.List;
import org.apache.deltaspike.data.api.EntityRepository;
import org.apache.deltaspike.data.api.Query;
import org.apache.deltaspike.data.api.Repository;
import org.apache.deltaspike.data.api.criteria.CriteriaSupport;


@Repository(forEntity = Pesquisa.class)
public abstract class PesquisaRepository implements CriteriaSupport<Pesquisa>,EntityRepository<Pesquisa, Long>{
        
    @Query(named = Pesquisa.Query.locateByInstrumentoFormalizacaoId)
    public abstract List<Pesquisa> locateByInstrumentoFormalizacaoId(Long id);

    @Query(named = Pesquisa.Query.locateLastByInstrumentoFormalizacaoId)
    public abstract Pesquisa locateLastByInstrumentoFormalizacaoId(Long id);
    
    @Query(named = Pesquisa.Query.locateByEmpresaId)
    public abstract List<Pesquisa> locateByEmpresaId(Long id);

    @Query(named = Pesquisa.Query.locateById)
    public abstract Pesquisa locateById(Long id);
}
