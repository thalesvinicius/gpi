package br.com.gpi.server.to;

import br.com.gpi.server.domain.entity.AtividadeProjeto;
import java.util.List;

public class AtividadeProjetoTO {

    private ProjetoTO cabecalho;
    private CronogramaAtividadesTO cronograma;
    private List<AtividadeProjeto> atividades;

    public AtividadeProjetoTO() {
    }

    public ProjetoTO getCabecalho() {
        return cabecalho;
    }

    public void setCabecalho(ProjetoTO cabecalho) {
        this.cabecalho = cabecalho;
    }

    public List<AtividadeProjeto> getAtividades() {
        return atividades;
    }

    public void setAtividades(List<AtividadeProjeto> atividades) {
        this.atividades = atividades;
    }

    public CronogramaAtividadesTO getCronograma() {
        return cronograma;
    }

    public void setCronograma(CronogramaAtividadesTO cronograma) {
        this.cronograma = cronograma;
    }

}
