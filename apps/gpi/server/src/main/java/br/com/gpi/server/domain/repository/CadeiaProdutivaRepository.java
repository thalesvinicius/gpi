package br.com.gpi.server.domain.repository;

import br.com.gpi.server.domain.entity.CadeiaProdutiva;
import br.com.gpi.server.domain.entity.CadeiaProdutiva_;
import br.com.gpi.server.domain.entity.Departamento;
import br.com.gpi.server.domain.entity.Departamento_;
import br.com.gpi.server.domain.entity.UsuarioInterno;
import java.util.List;
import org.apache.deltaspike.data.api.EntityRepository;
import org.apache.deltaspike.data.api.Query;
import org.apache.deltaspike.data.api.QueryParam;
import org.apache.deltaspike.data.api.Repository;
import org.apache.deltaspike.data.api.SingleResultType;
import org.apache.deltaspike.data.api.criteria.Criteria;
import org.apache.deltaspike.data.api.criteria.CriteriaSupport;

@Repository(forEntity = CadeiaProdutiva.class)
public abstract class CadeiaProdutivaRepository implements CriteriaSupport<CadeiaProdutiva>, EntityRepository<CadeiaProdutiva, Long> {

    public List<CadeiaProdutiva> findByFilters(String descricao, Integer departamentoId) {
        Criteria<CadeiaProdutiva, CadeiaProdutiva> criteria = criteria();

        if (descricao != null) {
            criteria = criteria.like(CadeiaProdutiva_.descricao, "%" + descricao + "%");
        }

        if (departamentoId != null) {
            criteria = criteria.join(CadeiaProdutiva_.departamento, where(Departamento.class).eq(Departamento_.id, departamentoId));
        }

        return criteria.distinct().getResultList();
    }

    @Query(named = CadeiaProdutiva.Query.locateResponsavelById,singleResult = SingleResultType.ANY)
    public abstract UsuarioInterno locateResponsavel(Long idCadeiaProdutiva);
    
    @Query(named = CadeiaProdutiva.Query.locateByDepartamentos)
    public abstract List<CadeiaProdutiva> findByDepartamentos(@QueryParam("departamentos") List<Long> departamentos);
    
    @Query(named = CadeiaProdutiva.Query.locateByProjetoId)
    public abstract List<CadeiaProdutiva> locateByProjetoId(Long idProjeto);    
}
