package br.com.gpi.server.domain.repository;

import br.com.gpi.server.domain.entity.MicroRegiao;
import org.apache.deltaspike.data.api.EntityRepository;
import org.apache.deltaspike.data.api.Repository;
import org.apache.deltaspike.data.api.criteria.CriteriaSupport;

@Repository(forEntity = MicroRegiao.class)
public abstract class MicroRegiaoRepository implements CriteriaSupport<MicroRegiao>, EntityRepository<MicroRegiao, Long>{
    
}
