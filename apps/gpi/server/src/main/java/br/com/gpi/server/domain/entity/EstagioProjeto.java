package br.com.gpi.server.domain.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;
import org.hibernate.envers.Audited;

@Audited
@Entity
@Table(name = "EstagioProjeto")
@IdClass(EstagioProjetoPK.class)
@NamedQueries({
    @NamedQuery(name = EstagioProjeto.Query.findAllByProjeto, query = "SELECT e FROM EstagioProjeto e WHERE e.projetoId = ?1")
})
public class EstagioProjeto implements Serializable {

    @Id
    @Column(name = "projeto_id")
    private long projetoId;

    @Id
    @Column(name = "estagio_id")
    private long estagioId;

    @Transient
    @ManyToOne
    @JoinColumn(name = "projeto_id", insertable = false, updatable = false)
    @JsonIgnore
    private Projeto projeto;

    @Transient
    @ManyToOne
    @JoinColumn(name = "estagio_id", insertable = false, updatable = false)
    private Estagio estagio;
    
    @Id
    private Date dataInicioEstagio;

    private Boolean ativo;

    public long getProjetoId() {
        return projetoId;
    }

    public void setProjetoId(long projetoId) {
        this.projetoId = projetoId;
    }

    public long getEstagioId() {
        return estagioId;
    }

    public void setEstagioId(long estagioId) {
        this.estagioId = estagioId;
    }
    
    public Projeto getProjeto() {
        return projeto;
    }

    public void setProjeto(Projeto projeto) {
        this.projeto = projeto;
    }

    public Estagio getEstagio() {
        return estagio;
    }

    public void setEstagio(Estagio estagio) {
        this.estagio = estagio;
    }

    public Date getDataInicioEstagio() {
        return dataInicioEstagio;
    }

    public void setDataInicioEstagio(Date dataInicioEstagio) {
        this.dataInicioEstagio = dataInicioEstagio;
    }

    public Boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(Boolean ativo) {
        this.ativo = ativo;
    }
    
    public static class Query {

        public static final String findAllByProjeto = "EstagioProjeto.findAllByProjeto";

        public Query() {
        }
    }
}
