package br.com.gpi.server.domain.entity;

import br.com.gpi.server.domain.common.BaseEntity;
import com.fasterxml.jackson.annotation.JsonIgnore;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.envers.Audited;

@Audited
@Entity
@Table(name = "AtividadeTemplate")
public class AtividadeTemplate extends BaseEntity<Long> {
    
    @ManyToOne
    @JoinColumn(name = "atividadeLocal_id")
    private AtividadeLocal atividadeLocal;
    
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "template_id")
    private Template template;
    
    private Integer posicao;    
    @Column(name = "obrigatorio", columnDefinition = "bit")    
    private Boolean obrigatorio;

    @Column(name = "bloqueiaICE", columnDefinition = "bit")    
    private Boolean bloqueiaICE;  
    
    @Column(name = "df", columnDefinition = "bit")    
    private Boolean df;
    
    @Column(name = "ii", columnDefinition = "bit")    
    private Boolean ii;

    @Column(name = "oi", columnDefinition = "bit")    
    private Boolean oi;

    @Column(name = "cc", columnDefinition = "bit")    
    private Boolean cc;   

    public AtividadeLocal getAtividadeLocal() {
        return atividadeLocal;
    }

    public void setAtividadeLocal(AtividadeLocal atividadeLocal) {
        this.atividadeLocal = atividadeLocal;
    }

    public Template getTemplate() {
        return template;
    }

    public void setTemplate(Template template) {
        this.template = template;
    }

    public Integer getPosicao() {
        return posicao;
    }

    public void setPosicao(Integer posicao) {
        this.posicao = posicao;
    }

    public Boolean getObrigatorio() {
        return obrigatorio;
    }

    public void setObrigatorio(Boolean obrigatorio) {
        this.obrigatorio = obrigatorio;
    }

    public Boolean getBloqueiaICE() {
        return bloqueiaICE;
    }

    public void setBloqueiaICE(Boolean bloqueiaICE) {
        this.bloqueiaICE = bloqueiaICE;
    }

    public Boolean getDf() {
        return df;
    }

    public void setDf(Boolean df) {
        this.df = df;
    }

    public Boolean getIi() {
        return ii;
    }

    public void setIi(Boolean ii) {
        this.ii = ii;
    }

    public Boolean getOi() {
        return oi;
    }

    public void setOi(Boolean oi) {
        this.oi = oi;
    }

    public Boolean getCc() {
        return cc;
    }

    public void setCc(Boolean cc) {
        this.cc = cc;
    }
    
}
