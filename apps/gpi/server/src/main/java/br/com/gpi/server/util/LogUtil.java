package br.com.gpi.server.util;

public class LogUtil {
    
    /**
     * Monta a SQL na entidade de LOG com o filtro do projeto e alterações 
     * realizada apenas pelo usuário externo.
     * @param entidade
     * @param atributos
     * @param idProjeto
     * @return 
     */
    public static StringBuffer montaConsultaLog(String entidade, 
            String atributos, Long idProjeto) {
        StringBuffer stb = new StringBuffer();
        stb.append(" Select ");
        stb.append(atributos);
        stb.append(" " + " From ");
        stb.append(entidade + " log ");
        stb.append(" " + " Where (log.logVersao = (Select Max(logVersao) From ");
        stb.append(entidade).append(" )) ");
        stb.append(" And ((Select count(id) From UsuarioExterno Where id = log.usuarioResponsavel_id) >= 1)");
        stb.append(" And (log.projeto_id = ");
        stb.append(idProjeto).append(") ");
        stb.append(" And ((Select Max(data) From HistoricoSituacaoProjeto Where situacaoProjeto_id = 3 And projeto_id = ");
        stb.append(idProjeto).append(") < log.dataUltimaAlteracao) ");        
        return stb;
    }
}
