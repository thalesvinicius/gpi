package br.com.gpi.server.endpoint;

import br.com.gpi.server.domain.common.QueryParameter;
import br.com.gpi.server.domain.entity.Departamento;
import br.com.gpi.server.domain.entity.Local;
import br.com.gpi.server.domain.entity.TipoInstrFormalizacao;
import br.com.gpi.server.domain.entity.User;
import br.com.gpi.server.domain.service.DomainTableService;
import br.com.gpi.server.domain.service.LocalService;
import br.com.gpi.server.domain.service.UserService;
import br.com.gpi.server.domain.service.reports.gerencial.RelatorioGerencial06InstrumentoFormalizacaoService;
import br.com.gpi.server.domain.user.model.SecurityUser;
import br.com.gpi.server.domain.view.RelatorioAcompanhamentoInstrumento;
import br.com.gpi.server.util.GenerateReportUtil;
import br.com.gpi.server.util.reports.ReportUtil;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import static java.util.stream.Collectors.joining;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

@Path("relatorio/instrumento")
@Stateless
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
@TransactionManagement(TransactionManagementType.BEAN)
public class InstrumentoFormalizacaotReportEndpoint {
    
    @Inject
    private RelatorioGerencial06InstrumentoFormalizacaoService relatorio;
    @Inject 
    private DomainTableService domainTable;
    @Inject
    private UserService userService;     
    @Inject
    private LocalService localizacao;

    @GET
    @Path("export/{format}")
    @Produces({"application/pdf", "application/vnd.ms-excel"})
    public Response buildAcompanhamentoInstrumento(@PathParam("format") String format, 
            @QueryParam("gerencias") List<Long> gerencias,
            @QueryParam("analistas") List<Long> analistas,
            @QueryParam("tipos") Set<TipoInstrFormalizacao> tipos,
            @QueryParam("locais") List<Long> locais,
            @QueryParam("previsaoAssinaturaDe") Long previsaoAssinaturaDe, 
            @QueryParam("previsaoAssinaturaAte") Long previsaoAssinaturaAte,
            @HeaderParam("todosLocais") String todosLocais,
            @QueryParam("todosTipos") Set<TipoInstrFormalizacao> todosTipos,
            @HeaderParam("todasGerencias") String todasGerencias,
            @HeaderParam("todosAnalistas") String todosAnalistas,
            @Context SecurityContext sc) throws FileNotFoundException, IOException {
        
         ReportUtil ru = new ReportUtil(){};
        Function<UserService, String> auth = ru.responsibleForReportInfo((SecurityUser) sc.getUserPrincipal());
        String informacoes = auth.apply(userService);
        
        Supplier<Map<String,Object>> parametrosExpr = () -> { 
            
            Boolean buscarTodosLocais = ru.buscarTodos(todosLocais, locais);
            Boolean buscarTodasGerencias = ru.buscarTodos(todasGerencias,gerencias);
            Boolean buscarTodosAnalistas = ru.buscarTodos(todosAnalistas, analistas);
            Boolean buscarTodosTipos = tipos.containsAll(todosTipos);
            String titulo = "Relatório de Acompanhamento dos Instrumentos de Formalização";
            Map<String,Object> params = new HashMap<>();
            params.put("TITULO", titulo);   
        
            params.put("ANALISTA", buscarTodosAnalistas ? "Todos" : 
                    userService.findByListIDs(new ArrayList<>(analistas)).stream().map(User::getNome).collect(joining(", ")));
            params.put("LOCAIS",buscarTodosLocais ? "Todos":
                localizacao.findByListIDs(new ArrayList<>(locais)).stream().map(Local::getDescricao).collect(joining(", ")));
            params.put("TIPO_INSTRUM",buscarTodosTipos ? "Todos" :
                    tipos.stream().map(TipoInstrFormalizacao::getDescricao).collect(Collectors.joining(",")));
            params.put("GERENCIA",ru.stringCollection(Departamento::getDescricao, buscarTodasGerencias ? null:
                    domainTable.findByIds(Departamento.class, gerencias),false ) );
            params.put("LOGO_INDI",GenerateReportUtil.getImage(ReportUtil.LOGO_INDI_NOME));
            params.put("PERIODO",ReportUtil.periodoString.apply(new Date(previsaoAssinaturaDe),new Date(previsaoAssinaturaAte)));
            params.put(ReportUtil.INFO_RESPONSAVEL,informacoes);
                return params;
        };

        
        return relatorio.generateReport(parametrosExpr,format, gerencias, analistas,
                new ArrayList<TipoInstrFormalizacao>(tipos), locais, previsaoAssinaturaDe, previsaoAssinaturaAte);
    }
    
    @GET
    @Path("generate")
    public Response generate(@QueryParam("gerencias") List<Integer> gerencias,
            @QueryParam("analistas") List<Integer> analistas,
            @QueryParam("tipos") List<TipoInstrFormalizacao> tipos,
            @QueryParam("locais") List<Integer> locais,
            @QueryParam("previsaoAssinaturaDe") Long previsaoAssinaturaDe, 
            @QueryParam("previsaoAssinaturaAte") Long previsaoAssinaturaAte) {
        
        QueryParameter filtros = QueryParameter.with(RelatorioAcompanhamentoInstrumento.Filtro.GERENCIA, gerencias)
                .and(RelatorioAcompanhamentoInstrumento.Filtro.ANALISTA,analistas)
                .and(RelatorioAcompanhamentoInstrumento.Filtro.TIPOS,tipos)
                .and(RelatorioAcompanhamentoInstrumento.Filtro.LOCAIS,locais)                
                .and(RelatorioAcompanhamentoInstrumento.Filtro.PREVISAO_ASSINATURA_DE, previsaoAssinaturaDe)
                .and(RelatorioAcompanhamentoInstrumento.Filtro.PREVISAO_ASSINATURA_ATE, previsaoAssinaturaAte);
        List<RelatorioAcompanhamentoInstrumento> result = relatorio.generate(filtros);
        return Response.ok(result).build();
    }
}