package br.com.gpi.server.domain.service;

import br.com.gpi.server.config.ApplicationSettings;
import br.com.gpi.server.domain.entity.Parametro;
import br.com.gpi.server.domain.repository.ParametroRepository;
import br.com.gpi.server.domain.user.model.EmailServiceTokenModel;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.annotation.Resource;
import javax.inject.Inject;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import org.apache.velocity.exception.VelocityException;

/**
 * @author <a href="mailto:pviniciusfm@gmail.com">Paulo Vinicius F. Machado</a>
 */
public class EmailService {

    @Resource(mappedName = "java:/mail/gpi")
    private Session mailSession;
    
    @Inject
    private ApplicationSettings config;

    @Inject
    private ParametroRepository parametroRepository;
        
    public void sendEmail(String to, String subject, String body) {
        MimeMessage message = new MimeMessage(mailSession);
        try {
            InternetAddress[] address = {new InternetAddress(to)};
            message.setRecipients(Message.RecipientType.TO, address);
            message.setSubject(subject, "UTF-8");
            message.setContent(body, "text/html; charset=utf-8");
            message.setSentDate(new Date());
            Transport.send(message);
        } catch (MessagingException ex) {
            throw new RuntimeException(ex);
        }
    }

    public EmailServiceTokenModel sendVerificationEmail(final EmailServiceTokenModel emailVerificationModel) {
        Map<String, String> resources = new HashMap<>();
        return sendVerificationEmail(emailVerificationModel, "GPI - Ativação de Usuário",
                "email/VerifyEmail.vm", resources);
    }

    public EmailServiceTokenModel sendRegistrationEmail(final EmailServiceTokenModel emailVerificationModel) {
        Map<String, String> resources = new HashMap<>();
        return sendVerificationEmail(emailVerificationModel, "GPI - Bem vindo!",
                "email//RegistrationEmail.vm", resources);
    }

    public EmailServiceTokenModel sendLostPasswordEmail(final EmailServiceTokenModel emailServiceTokenModel) {
        Map<String, String> resources = new HashMap<>();
        return sendVerificationEmail(emailServiceTokenModel, "GPI - Recuperação de Senha",
                "email/LostPasswordEmail.vm", resources);
    }

    public void sendInviteSatisfactionSurveyEmail(String to) {
        sendGenericEmail("GPI - Pesquisa de Satisfação", to, "email/inviteSatisfactionSurveyEmail.vm");
    }

    public void sendAnswerSatisfactionSurveyEmail(String to, String instrumentoFormalizacao) {
        Map<String, Object> model = new HashMap<>();
        model.put("instrumentoFormalizacao", instrumentoFormalizacao);
        sendGenericEmailWithModel("GPI - Pesquisa de Satisfação", to, "email/answerSatisfactionSurveyEmail.vm", model);
    }

    public void sendThankSatisfactionSurveyEmail(String to) {
        sendGenericEmail("GPI - Pesquisa de Satisfação", to, "email/thankSatisfactionSurveyEmail.vm");
    }
    
    public void sendValidateCompanyEmail(String to) {
        sendGenericEmail("GPI - Validação de Cadastro de Empresa", to, "email/validateCompanyEmail.vm");
    }
    
    public void sendSuccessfullyRegistrationProjectEmail(String to) {
        sendGenericEmail("GPI - Projeto Cadastrado com Sucesso", to, "email/successfullyRegistrationProjectEmail.vm");
    }
    
    public void sendSuccessfullyRegistrationClaimEmail(String to) {
        sendGenericEmail("GPI - Informações de Caracterização do Empreendimento Gravadas Com Sucesso", to, "email/successfullyRegistrationClaimEmail.vm");
    }
    
    public void sendSuccessfullyICEValidationEmail(String to) {
        sendGenericEmail("GPI - Informações de Caracterização do Empreendimento Validadas Com Sucesso", to, "email/successfullyICEValidationEmail.vm");
    }
    
    public void sendSuccessfullyICEAcceptationEmail(String to) {
        sendGenericEmail("GPI - Informações de Caracterização do Empreendimento Aceitas Com Sucesso", to, "email/successfullyICEAcceptationEmail.vm");
    }
    
    public void sendSuccessfullyProtocolPublicationEmail(String to, String empresa) {
        Map<String, Object> model = new HashMap<>();
        model.put("empresa", empresa);
        sendGenericEmailWithModel("GPI - Protocolo Publicado Com Sucesso", to, "email/successfullyProtocolPublicationEmail.vm", model);
    }
    
    public void sendSuccessfullyFirstContactEmail(String to) {
        sendGenericEmail("GPI - Cadastro de Empresa Realizado Com Sucesso", to, "email/successfullyFirstContactEmail.vm");
    }
    
    public void sendSuccessfullyFirstContactToManagerEmail(String to) {
        sendGenericEmail("GPI - Novo Cadastro de Empresa Realizado na Área do Investidor", to, "email/successfullyFirstContactToManagerEmail.vm");
    }
    
    public void sendContactRequest(String to, String nomeEmpresa, Long idEmpresa) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        Map<String, Object> model = new HashMap<>();
        model.put("nomeEmpresa", nomeEmpresa);
        model.put("dataSolicitacao", sdf.format(new Date()));
        model.put("linkEmpresa", config.getBaseUrl() + "#/empresa/cadastro/editar?id=" + idEmpresa);
        sendGenericEmailWithModel("GPI - Nova solicitação de contato recebida", to, "email/contactRequest.vm", model);
    }

    private EmailServiceTokenModel sendVerificationEmail(final EmailServiceTokenModel emailVerificationModel, final String emailSubject, final String velocityModel, final Map<String, String> resources) {
        Map<String, Object> model = new HashMap<>();
        model.put("model", emailVerificationModel);
        model.put("url", config.getBaseUrl());
        sendGenericEmailWithModel(emailSubject, emailVerificationModel.getEmailAddress(), velocityModel, model);
        return emailVerificationModel;
    }

    public void sendEnvironmentAlert(String nomeProjeto) {
        String to = config.getReceiverIceEnvironment();
        Map<String, Object> model = new HashMap<>();
        model.put("nomeProjeto", nomeProjeto);
        sendGenericEmailWithModel("Alerta Meio Ambiente", to, "email/sendEnvironmentAlert.vm", model);
    }

    private static String generateHTMLFromVelocityModel(final Map<String, Object> mapObjects, final String velocityModel) throws VelocityException {
        return VelocityEngineUtils.mergeTemplateIntoString(velocityModel, "UTF-8", mapObjects);
    }
    
    private void sendGenericEmail(String title, String to, String velocityEmailModelPath) {
        sendGenericEmailWithModel(title, to, velocityEmailModelPath, null);
    }
    
    private void sendGenericEmailWithModel(String title, String to, String velocityEmailModelPath, Map<String, Object> model) {
        String text = generateHTMLFromVelocityModel(model, velocityEmailModelPath);
        sendEmail(to, title, text);
    }       
    
    public void sendCampaign(String to, String numeroProtocolo,
            String nomeEmpresa, Date dataAssinatura, String ano,
            Date dataFimCampanha, String responsavelProjeto) {
        Parametro parametro = parametroRepository.locateByParametroName("ConteudoEMailCampanha");
        
        StringBuffer email = new StringBuffer();        
        email.append("<!DOCTYPE html>");
        email.append("<html>");
        email.append("");
        email.append("<head>");
        email.append("    <title>GPI - Relatório anual de acompanhamento</title>");
        email.append("    <meta charset='UTF-8'>");
        email.append("</head>");
        email.append("");
        email.append("<body style='margin: 0px; font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; color: #333333; background-color: #FFF;'>");
        email.append("    <div style='padding:0;width:100%!important;margin:0' marginheight='0' marginwidth='0'>");
        email.append("        <center>");
        email.append("            <table cellpadding='8' cellspacing='0' style='background:#ffffff;width:100%!important;margin:0;padding:0' border='0'>");
        email.append("                <tbody>");
        email.append("                    <tr>");
        email.append("                        <td valign='top'>");
        email.append("                            <table cellpadding='0' cellspacing='0' style='border-radius:4px;border:1px #E2F5DC solid' border='0' align='center'>");
        email.append("                                <tbody>");
        email.append("                                    <tr>");
        email.append("                                        <td colspan='3' height='6'></td>");
        email.append("                                    </tr>");
        email.append("                                    <tr style='line-height:0px'>");
        email.append("                                        <td width='100%' style='font-size:0px' align='center' height='1'>");
        email.append("<img width='57' height='75' alt='' src='http://www.indi.mg.gov.br/site_indi_en/wp-content/themes/indi/static/imgs/logo_indi.png'>");
        email.append("                                        </td>");
        email.append("                                    </tr>");
        email.append("                                    <tr>");
        email.append("                                        <td>");
        email.append("                                            <table cellpadding='0' cellspacing='0' style='line-height:25px' border='0' align='center'>");
        email.append("                                                <tbody>");
        email.append("                                                    <tr>");
        email.append("                                                        <td colspan='3' height='30'></td>");
        email.append("                                                    </tr>");
        email.append("                                                    <tr>");
        email.append("                                                        <td width='36'></td>");
        email.append("<td width='454' align='left' style='color:#444444;border-collapse:collapse;font-size:11pt;font-family:proxima_nova,'Open Sans','Lucida Grande','Segoe UI',Arial,Verdana,'Lucida Sans Unicode',Tahoma,'Sans Serif';max-width:454px' valign='top'>");
        email.append(parametro.getValor());
        email.append("                                                        <td width='36'></td>");
        email.append("                                                    </tr>");
        email.append("                                                    <tr>");
        email.append("                                                        <td colspan='3' height='36'></td>");
        email.append("                                                    </tr>");
        email.append("                                                </tbody>");
        email.append("                                            </table>");
        email.append("                                        </td>");
        email.append("                                    </tr>");
        email.append("                                </tbody>");
        email.append("                            </table>");
        email.append("                            <table cellpadding='0' cellspacing='0' align='center' border='0'>");
        email.append("                                <tbody>");
        email.append("                                    <tr>");
        email.append("                                        <td height='10'></td>");
        email.append("                                    </tr>");
        email.append("                                    <tr>");
        email.append("                                        <td style='padding:0;border-collapse:collapse'>");
        email.append("                                            <table cellpadding='0' cellspacing='0' align='center' border='0'>");
        email.append("                                                <tbody>");
        email.append("<tr style='color:#a8b9c6;font-size:11px;font-family:proxima_nova,'Open Sans','Lucida Grande','Segoe UI',Arial,Verdana,'Lucida Sans Unicode',Tahoma,'Sans Serif''>");
        email.append("                                                        <td width='150' align='left'></td>");
        email.append("                                                        <td width='378' align='right'>©INDI - Instituto de Desenvolvimento Integrado de Minas Gerais</td>");
        email.append("                                                    </tr>");
        email.append("                                                </tbody>");
        email.append("                                            </table>");
        email.append("                                        </td>");
        email.append("                                    </tr>");
        email.append("                                </tbody>");
        email.append("                            </table>");
        email.append("                        </td>");
        email.append("                    </tr>");
        email.append("                </tbody>");
        email.append("            </table>");
        email.append("        </center>");
        email.append("    </div>");
        email.append("</body>");
        email.append("");
        email.append("</html>");
        
        Map<String, Object> model = new HashMap<>();
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        model.put("nomeEmpresa", nomeEmpresa);
        
        if (email.indexOf("{nomeEmpresa}") >  1) {
            email.replace(email.indexOf("{nomeEmpresa}"), email.indexOf("{nomeEmpresa}") + 13, nomeEmpresa);
        }
        
        if (email.indexOf("{numeroProtocolo}") >  1) {
            email.replace(email.indexOf("{numeroProtocolo}"), email.indexOf("{numeroProtocolo}") + 17, numeroProtocolo);
        }
        
        if (email.indexOf("{dataAssinatura}") >  1) {
            email.replace(email.indexOf("{dataAssinatura}"), email.indexOf("{dataAssinatura}") + 16, sdf.format(dataAssinatura));
        }
        
        if (email.indexOf("{ano}") >  1) {
            email.replace(email.indexOf("{ano}"), email.indexOf("{ano}") + 5, ano);
        }
        
        if (email.indexOf("{dataFimCampanha}") >  1) {
            email.replace(email.indexOf("{dataFimCampanha}"), email.indexOf("{dataFimCampanha}") + 17, sdf.format(dataFimCampanha));
        }
        
        if (email.indexOf("{responsavelProjeto}") >  1) {
            email.replace(email.indexOf("{responsavelProjeto}"), email.indexOf("{responsavelProjeto}") + 20, responsavelProjeto);
        }
        
        sendEmail(to, "GPI - Relatório anual de acompanhamento", email.toString());
    }
    
    public void sendAnnualMonitoringValidateEmail(String to) {
        sendGenericEmail("GPI - Relatório de Acompanhamento", to, "email/validateAnnualMonitoringProject.vm");
    }
    
}
