package br.com.gpi.server.to;

import java.util.ArrayList;
import java.util.List;

public class AlertaTO {

    private String titulo;
    private List<DescricaoAlertaTO> alertas;

    public AlertaTO() {

    }

    public AlertaTO(String titulo, List<DescricaoAlertaTO> alertas) {
        this.titulo = titulo;
        this.alertas = alertas;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public List<DescricaoAlertaTO> getAlertas() {
        return alertas;
    }

    public void setAlertas(List<DescricaoAlertaTO> alertas) {
        this.alertas = alertas;
    }

    public void setAlertas(List<Object[]> alertas, String link) {
        List<DescricaoAlertaTO> descricoes = new ArrayList<>();
        for (Object[] descricao : alertas) {
            descricoes.add(new DescricaoAlertaTO(descricao, link));
        }

        this.alertas = descricoes;
    }

}
