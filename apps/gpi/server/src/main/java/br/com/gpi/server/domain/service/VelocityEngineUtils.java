package br.com.gpi.server.domain.service;

import java.io.StringWriter;
import java.io.Writer;
import java.util.Map;

import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.exception.MethodInvocationException;
import org.apache.velocity.exception.ParseErrorException;
import org.apache.velocity.exception.ResourceNotFoundException;
import org.apache.velocity.exception.VelocityException;
import org.apache.velocity.runtime.RuntimeConstants;
import org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader;

/**
 * Utility class for working with a VelocityEngine. Provides convenience methods
 * to merge a Velocity template with a model.
 */
public abstract class VelocityEngineUtils {

    public static VelocityEngine velocityEngine;

    /**
     * Merge the specified Velocity template with the given model and write the
     * result to the given Writer.
     *
     * @param templateLocation the location of template, relative to Velocity's
     * resource loader path
     * @param model the Map that contains model names as keys and model objects
     * as values
     * @param writer the Writer to write the result to
     * @throws VelocityException if the template wasn't found or rendering
     * failed
     */
    public static void mergeTemplate(
            String templateLocation, Map model, Writer writer)
            throws VelocityException {

        try {
            VelocityContext velocityContext = new VelocityContext(model);
            getVelocityEngine().mergeTemplate(templateLocation, velocityContext, writer);
        } catch (MethodInvocationException | ParseErrorException | ResourceNotFoundException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new VelocityException(ex.toString());
        }
    }

    /**
     * Merge the specified Velocity template with the given model and write the
     * result to the given Writer.
     *
     * @param templateLocation the location of template, relative to Velocity's
     * resource loader path
     * @param encoding the encoding of the template file
     * @param model the Map that contains model names as keys and model objects
     * as values
     * @param writer the Writer to write the result to
     * @throws VelocityException if the template wasn't found or rendering
     * failed
     */
    public static void mergeTemplate(
            String templateLocation, String encoding, Map model, Writer writer)
            throws VelocityException {

        try {
            VelocityContext velocityContext = new VelocityContext(model);
            getVelocityEngine().mergeTemplate(templateLocation, encoding, velocityContext, writer);

        } catch (ResourceNotFoundException | ParseErrorException | MethodInvocationException ex) {
            throw new VelocityException(ex.toString());
        }
    }

    /**
     * Merge the specified Velocity template with the given model into a String.
     * <p>
     * When using this method to prepare a text for a mail to be sent with
     * Spring's mail support, consider wrapping VelocityException in
     * MailPreparationException.
     *
     * @param templateLocation the location of template, relative to Velocity's
     * resource loader path
     * @param model the Map that contains model names as keys and model objects
     * as values
     * @return the result as String
     * @throws VelocityException if the template wasn't found or rendering
     * failed     
     */
    public static String mergeTemplateIntoString(
            String templateLocation, Map model)
            throws VelocityException {

        StringWriter result = new StringWriter();
        mergeTemplate(templateLocation, model, result);
        return result.toString();
    }

    /**
     * Merge the specified Velocity template with the given model into a String.
     * <p>
     * When using this method to prepare a text for a mail to be sent with
     * Spring's mail support, consider wrapping VelocityException in
     * MailPreparationException.
     *
     * @param templateLocation the location of template, relative to Velocity's
     * resource loader path
     * @param encoding the encoding of the template file
     * @param model the Map that contains model names as keys and model objects
     * as values
     * @return the result as String
     * @throws VelocityException if the template wasn't found or rendering
     * failed     
     */
    public static String mergeTemplateIntoString(
            String templateLocation, String encoding, Map model)
            throws VelocityException {

        StringWriter result = new StringWriter();
        mergeTemplate(templateLocation, encoding, model, result);
        return result.toString();
    }

    private static VelocityEngine getVelocityEngine() {
        if (VelocityEngineUtils.velocityEngine == null) {
            VelocityEngine newVe = new VelocityEngine();
            newVe.setProperty(RuntimeConstants.RESOURCE_LOADER, "class");
            newVe.setProperty("class.resource.loader.class", ClasspathResourceLoader.class.getName());
            newVe.setProperty("runtime.log.logsystem.class", "org.apache.velocity.runtime.log.NullLogSystem");
            newVe.init();
            VelocityEngineUtils.velocityEngine = newVe;
        }
        return VelocityEngineUtils.velocityEngine;
    }
}
