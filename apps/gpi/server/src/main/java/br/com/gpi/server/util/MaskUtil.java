
package br.com.gpi.server.util;

import java.text.ParseException;
import javax.swing.text.MaskFormatter;

/**
 * @author <a href="mailto:lksbos@gmail.com">Lucas Gomes da Silva</a>
 */
public class MaskUtil {

    public static void main(String[] args) {
        
    }
    
    public static String format(String pattern, String value) {
        MaskFormatter mask;
        try {
            mask = new MaskFormatter(pattern);
            mask.setValueContainsLiteralCharacters(false);
            return mask.valueToString(value);
        } catch (ParseException e) {
            return value;
        }
    }
}

