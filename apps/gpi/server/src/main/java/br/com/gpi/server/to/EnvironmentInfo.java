package br.com.gpi.server.to;

import javax.enterprise.inject.Model;

@Model
public class EnvironmentInfo {

    private String version;
    private String environment;
    private String generatedDateTime;

    public EnvironmentInfo(String version, String environment, String generatedDateTime) {
        this.version = version;
        this.environment = environment;
        this.generatedDateTime = generatedDateTime;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getEnvironment() {
        return environment;
    }

    public void setEnvironment(String environment) {
        this.environment = environment;
    }

    public String getGeneratedDateTime() {
        return generatedDateTime;
    }

    public void setGeneratedDateTime(String generatedDateTime) {
        this.generatedDateTime = generatedDateTime;
    }

}
