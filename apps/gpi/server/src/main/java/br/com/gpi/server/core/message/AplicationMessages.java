package br.com.gpi.server.core.message;

import org.apache.deltaspike.core.api.message.MessageBundle;
import org.apache.deltaspike.core.api.message.MessageContextConfig;
import org.apache.deltaspike.core.api.message.MessageTemplate;

@MessageBundle
@MessageContextConfig(messageInterpolator = MessageInterpolatorImpl.class, messageSource = "messages")
public interface AplicationMessages {

    @MessageTemplate("{user.password.changed}")
    public String passwordChanged();

    @MessageTemplate("{user.password.lost.sended}")
    public String passwordLostSended();

    @MessageTemplate("{domain.exception.domain_not_founded}")
    public String domainNotFoundedMessage();
}
