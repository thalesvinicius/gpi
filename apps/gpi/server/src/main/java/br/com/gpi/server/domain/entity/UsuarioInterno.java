package br.com.gpi.server.domain.entity;

import br.com.gpi.server.domain.user.model.SecurityUser;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.validation.constraints.NotNull;
import org.hibernate.envers.Audited;

@Audited
@Entity
@DiscriminatorValue("1")
@NamedQueries({
    @NamedQuery(name = UsuarioInterno.Query.locateByLogin, query = "SELECT u FROM UsuarioInterno u WHERE u.matricula = :login"),
    @NamedQuery(name = UsuarioInterno.Query.locateByDepartamento, query = "SELECT u FROM UsuarioInterno u WHERE u.departamento.id in :departamentos ORDER BY u.nome")
})
public class UsuarioInterno extends User {

    @NotNull
    @Column(name = "matricula", length = 15, nullable = false)
    private String matricula;

    @NotNull
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    @JoinColumn(name = "departamento_id", nullable = false)
    private Departamento departamento;

    public UsuarioInterno() {
    }

    public UsuarioInterno(SecurityUser externalUser) {
        super(externalUser);
    }

    public String getMatricula() {
        return matricula;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }

    public Departamento getDepartamento() {
        return departamento;
    }

    public void setDepartamento(Departamento departamento) {
        this.departamento = departamento;
    }
    
    @Override
    public String getLogin() {
        return matricula;
    }

    public static class Query {

        public static final String locateByLogin = "UsuarioInterno.locateByLogin";
        public static final String locateByDepartamento = "UsuarioInterno.locateByDepartamento";
    }
}
