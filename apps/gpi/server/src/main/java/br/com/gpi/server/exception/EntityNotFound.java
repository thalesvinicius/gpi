package br.com.gpi.server.exception;

import br.com.gpi.server.core.exception.BaseWebApplicationException;

/**
 * @author <a href="mailto:pviniciusfm@gmail.com">Paulo Vinicius F. Machado</a>
 */
public class EntityNotFound extends BaseWebApplicationException {

    private static final long serialVersionUID = 1L;

    public EntityNotFound(int establishmentID) {
        super(404, "404", "Establishment Error", String.format("Establishment %s not found", establishmentID));
    }

}
