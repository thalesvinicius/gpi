package br.com.gpi.server.domain.service.reports.ice;

import br.com.gpi.server.domain.entity.Infraestrutura;
import br.com.gpi.server.domain.entity.MeioAmbiente;
import br.com.gpi.server.domain.entity.Projeto;
import br.com.gpi.server.domain.entity.enumerated.EnumCadeiaProdutiva;
import br.com.gpi.server.domain.service.ProjetoService;
import br.com.gpi.server.domain.service.reports.BaseReportService;
import br.com.gpi.server.to.report.UsoFonteReportTO;
import static br.com.gpi.server.util.reports.ReportUtil.*;
import java.io.FileNotFoundException;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.BiFunction;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.ws.rs.core.SecurityContext;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperPrint;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

/**
 *
 * @author rafaelbfs
 */
public class ImpressaoICEReportService extends BaseReportService<Projeto> {

    private final String RELATORIO_IMPRIMIR_ICE = "/jasper/ImprimirICE.jasper";

    @Inject
    private ProjetoService projetoService;
    
    private Map<String,Object> subRelatorios(){
        Map<String, Object> subs = subReportsMapLogoExpr.apply(defaultStyle(),
                getINDILogo());
        subs.put("empregos", loadObject(resourceAsStream(RELATORIO_EMPREGO_ICE)));
        subs.put("empresa", loadObject(resourceAsStream(RELATORIO_DADOS_EMPRESA)));
        subs.put("cnaeICE",loadObject(resourceAsStream(RELATORIO_CNAE_ICE)));
        subs.put(CHAVE_CONTATOS_EMPRESA_ICE,loadObject(resourceAsStream(RELATORIO_CONTAOS_EMPRESA_ICE)));
        subs.put(CHAVE_RELATORIO_SUCRO_ENERG, loadObject(resourceAsStream(RELATORIO_EMPREGO_SUCRO_ENERG)));
        subs.put(CHAVE_RELATORIO_PRODUTOS_ICE, loadObject(resourceAsStream(RELATORIO_PRODUTOS_INSUMOS)));
        subs.put(CHAVE_RELATORIO_FINANCEIRO,loadObject(resourceAsStream(RELATORIO_FINANCEIRO)));
        subs.put(CHAVE_RELATORIO_CRONOGRAMA, loadObject(resourceAsStream(RELATORIO_CRONOGRAMA)));
        subs.put(CHAVE_RELATORIO_PROJETO_ICE, loadObject(resourceAsStream(RELATORIO_PROJETO_ICE)));
        subs.put(CHAVE_RELATORIO_INFO_COMPLEMENTARES7_1, loadObject(resourceAsStream(RELATORIO_INFO_COMPLEMENTARES7_1)));
        subs.put(CHAVE_INFORMACOES_CONPLEMENTARES_OUTRAS_INFORMACOES, loadObject(resourceAsStream(RELATORIO_INFORMACOES_CONPLEMENTARES_OUTRAS_INFORMACOES)));
        subs.put(CHAVE_SUB_RELATORIO_LOCALIZACAO, loadObject(resourceAsStream(SUBRELATORIO_LOCALIZACAO)));
        return subs;        
    }
    
    private Map<String,Object> parametros(){
        Map<String, Object> parametros = paramsExpr.init(subRelatorios());
        parametros.put("HIDRO_MINERACAO_SIDERURGIA",
                new HashSet<Integer>(Arrays.asList(EnumCadeiaProdutiva.ENERGIAS_HIDRICAS.getId(),EnumCadeiaProdutiva.MINERACAO_FERRO.getId(),EnumCadeiaProdutiva.SIDERURGIA.getId())));
        parametros.put("ENERGIA_HIDRICA",EnumCadeiaProdutiva.ENERGIAS_HIDRICAS.getId().longValue());
        parametros.put("SUCRO_ENERGETICO",EnumCadeiaProdutiva.SUCROENERGETICO.getId().longValue());
        parametros.put("ELETROELETRONICO",EnumCadeiaProdutiva.ELETRICO_ELETROELETRONICOS.getId().longValue() );
        parametros.put("SERVICOS", EnumCadeiaProdutiva.SERVICOS.getId().longValue());
        parametros.put("SERVICOS_COMERCIALIZACAO",new HashSet<Integer>(
                Arrays.asList(EnumCadeiaProdutiva.SERVICOS.getId(),EnumCadeiaProdutiva.COMERCIO_SERVICOS.getId())));
       return parametros;
    }
    
    private Map<Long,Pair<Infraestrutura,MeioAmbiente>> infoComplementar7_1(Collection<Projeto> projetos){
        Map<Long,Pair<Infraestrutura,MeioAmbiente>> ret = new HashMap<>();
        projetos.stream().forEach((pro) -> {
            ret.put(pro.getId(),
                    new ImmutablePair<>(pro.getInfraestrutura(),pro.getMeioAmbiente()) );
        });
        return ret;
    }
    
    private Map<Long,List<UsoFonteReportTO>> mapUsoFonteListByProjectID(final List<Projeto> projetos){
        HashMap<Long, List<UsoFonteReportTO>> map = new HashMap<>();
        projetos.stream().forEach((pro) -> {
            map.put(pro.getId(),
              UsoFonteReportTO.toOrderedList(
                      Optional.ofNullable(pro.getFinanceiro())
                              .flatMap((fin)->Optional.ofNullable(fin.getUsoFonteList()))
                              .orElse(Collections.emptySet())));
            
        });
        return map;
    }
    
    private Map<Long,Projeto> mapProjetosById(final Collection<Projeto> projetos){
        Map<Long,Projeto> ret = new HashMap<>();
        projetos.stream().forEach((pro)-> {
            ret.put(pro.getId(), pro);
        });
        return ret;
    }
    
    private void tratarProjeto(Collection<Projeto> projetos){
        projetos.stream().forEach((pro)->{
            BiFunction<String,String,String> coordProcessor = (coord,last) -> {
                if(coord != null && !coord.isEmpty()){
                    String formated = String.format("%6s", coord.replaceAll("[^0-9]", "")).replace(' ','0' );
                    return formated.substring(0,2) + "°" + formated.substring(2,4) + "'" + formated.substring(4) + "'' " + last;
                }
                return "---";
            };
            Optional.ofNullable(pro.getLocalizacao()).ifPresent((loc)-> {
                loc.setLatitude(coordProcessor.apply(loc.getLatitude() ,"S"));
                loc.setLongetude(coordProcessor.apply(loc.getLongetude() ,"W"));
            });
        });
    }
    
    
    
    

    @Inject
    public ImpressaoICEReportService(EntityManager em) {
        super(em);                
    }
    
    public byte[] imprimirICE(List<Long> idProjetos,SecurityContext securityContext) throws JRException, FileNotFoundException, SQLException {
        List<Projeto> projetos = projetoService.findByIdToReport(idProjetos);
        tratarProjeto(projetos);
         Map<String, Object> params = parametros();
                params.put(USO_FONTE_DATA_SET_KEY,
                        mapUsoFonteListByProjectID(projetos));
                params.put(INFORMACOES_COMPLEMENTARES7_1_DS_KEY,
                        infoComplementar7_1(projetos));
                params.put("PROJETOS_POR_ID",mapProjetosById(projetos) );
                //params.put(CHAVE_RESPONSAVEL_DATA_DS,generateResponsibleForReportDS(securityContext));
                params.putAll(generateResponsibleForReportDS().apply(securityContext));
        JasperPrint jp = generateReport(params,resourceAsStream(RELATORIO_IMPRIMIR_ICE),
            jrBeanCollection(projetos));
        return JasperExportManager.exportReportToPdf(jp);
    }
}
