package br.com.gpi.server.domain.entity;

import java.io.Serializable;

public class AnexoLocalizacaoPK implements Serializable {
    
    private long anexo;
    private long localizacao;

    public long getAnexo() {
        return anexo;
    }

    public void setAnexo(long anexoId) {
        this.anexo = anexoId;
    }

    public long getLocalizacao() {
        return localizacao;
    }

    public void setLocalizacao(long localizacao) {
        this.localizacao = localizacao;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 73 * hash + (int) (this.anexo ^ (this.anexo >>> 32));
        hash = 73 * hash + (int) (this.localizacao ^ (this.localizacao >>> 32));
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AnexoLocalizacaoPK other = (AnexoLocalizacaoPK) obj;
        if (this.anexo != other.anexo) {
            return false;
        }
        if (this.localizacao != other.localizacao) {
            return false;
        }
        return true;
    }
    
}
