package br.com.gpi.server.domain.entity;

import java.io.Serializable;

public class ProjetoRecentePK implements Serializable {

    private long projetoId;
    private long usuarioId;

    public ProjetoRecentePK() {
    }

    public long getProjetoId() {
        return projetoId;
    }

    public void setProjetoId(long projetoId) {
        this.projetoId = projetoId;
    }

    public long getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(long usuarioId) {
        this.usuarioId = usuarioId;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 97 * hash + (int) (this.projetoId ^ (this.projetoId >>> 32));
        hash = 97 * hash + (int) (this.usuarioId ^ (this.usuarioId >>> 32));
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ProjetoRecentePK other = (ProjetoRecentePK) obj;
        if (this.projetoId != other.projetoId) {
            return false;
        }
        if (this.usuarioId != other.usuarioId) {
            return false;
        }
        return true;
    }

}
