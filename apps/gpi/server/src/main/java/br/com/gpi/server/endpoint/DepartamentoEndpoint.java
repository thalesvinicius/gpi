package br.com.gpi.server.endpoint;

import br.com.gpi.server.domain.repository.DepartamentoRepository;
import javax.annotation.security.PermitAll;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;


@Path("/departamento")
@Stateless
@PermitAll
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
@TransactionManagement(TransactionManagementType.BEAN)
public class DepartamentoEndpoint {
    
    @Inject
    DepartamentoRepository departamentoRepository;
    
    @GET
    @Path("diretorias")
    public Response findAllDiretorias() {
        return Response.ok(departamentoRepository.findAllByDepartamentoSuperiorIsNull()).build();
    }
    
    @GET
    @Path("departamentoSuperiorOf")
    public Response findAllDiretorias(@QueryParam("departamentoSuperiorId") Long departamentoSuperiorId) {
        return Response.ok(departamentoRepository.findAllByDepartamentoSuperiorId(departamentoSuperiorId)).build();
    }
    
    
}
