package br.com.gpi.server.domain.repository;

import br.com.gpi.server.domain.entity.Local;
import org.apache.deltaspike.data.api.EntityRepository;
import org.apache.deltaspike.data.api.Repository;
import org.apache.deltaspike.data.api.criteria.CriteriaSupport;

@Repository(forEntity = Local.class)
public abstract class LocalRepository  implements CriteriaSupport<Local>,EntityRepository<Local, Long>{
    
}
