package br.com.gpi.server.domain.entity;

import br.com.gpi.server.core.jpa.converter.LocalDateConverter;
import br.com.gpi.server.domain.common.BaseEntity;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Converter;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.hibernate.envers.Audited;

/**
 * @since 01/07/2014
 * @author <a href="mailto:thalesvd@algartech.com">Thales Dias</a>
 */
@Audited
@Entity
@Table(name = "Anexo")
public class Anexo extends BaseEntity<Long> {

    @Column(name = "nome", length = 255)
    private String nome;

    @Column(name = "local", length = 255)
    private String path;

    @Column(name = "extencao", length = 5)
    private String extencao;
    @Convert(converter = LocalDateConverter.class)
    @Temporal(TemporalType.DATE)
    @Column(name = "data")
    private Date dataAnexo;

//    @Lob
//    @Basic(fetch = FetchType.LAZY)
//    @Column(name = "storage")
//    private InputStream storage;
    public Anexo() {
    }

    public Anexo(String nome, String path, String extencao) {
        this.nome = nome;
        this.path = path;
        this.extencao = extencao;
        this.dataAnexo = new Date();
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getExtencao() {
        return extencao;
    }

    public void setExtencao(String extencao) {
        this.extencao = extencao;
    }

    public LocalDate getDataAnexo() {
        if(dataAnexo == null){
            return null;
        }
        Instant instant = Instant.ofEpochMilli(dataAnexo.getTime());
        return LocalDateTime.ofInstant(instant, ZoneId.systemDefault()).toLocalDate();
    }

    public void setDataAnexo(LocalDate dataAnexo) {
         if (dataAnexo == null){
            this.dataAnexo = null;
        }else{
            Instant instant = dataAnexo.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant();
            this.dataAnexo = Date.from(instant);
        }
        
    }

}
