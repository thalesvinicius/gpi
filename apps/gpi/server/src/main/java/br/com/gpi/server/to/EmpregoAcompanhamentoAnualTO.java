package br.com.gpi.server.to;

public class EmpregoAcompanhamentoAnualTO extends AcompanhamentoTO{
    private Long id;
    private Integer ano;
    private Integer numeroPrevisto;
    private Integer numeroRealizado;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getAno() {
        return ano;
    }

    public void setAno(Integer ano) {
        this.ano = ano;
    }

    public Integer getNumeroPrevisto() {
        return numeroPrevisto;
    }

    public void setNumeroPrevisto(Integer numeroPrevisto) {
        this.numeroPrevisto = numeroPrevisto;
    }

    public Integer getNumeroRealizado() {
        return numeroRealizado;
    }

    public void setNumeroRealizado(Integer numeroRealizado) {
        this.numeroRealizado = numeroRealizado;
    }    
    
}
