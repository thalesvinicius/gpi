package br.com.gpi.server.endpoint;

import br.com.gpi.server.domain.entity.AtividadeLocal;
import br.com.gpi.server.domain.repository.AtividadeLocalRepository;
import java.util.List;
import javax.annotation.security.PermitAll;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/atividadeLocal")
@Stateless
@PermitAll
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
@TransactionManagement(TransactionManagementType.BEAN)
public class AtividadeLocalEndpoint {
    
    @Inject
    AtividadeLocalRepository atividadeLocalRepository;
            
    @GET
    @Path("findAllByLocalId/{id}")
    public Response findAllByLocalId(@PathParam("id") Long id) {
        List<AtividadeLocal> atividades = atividadeLocalRepository.findAllByLocalId(id);
        return Response.ok(atividades).build();
    }
    
    @GET
    @Path("findAllActiveByLocalId/{id}")
    public Response findAllActiveByLocalId(@PathParam("id") Long id) {
        List<AtividadeLocal> atividades = atividadeLocalRepository.findAllActiveByLocalId(id);
        return Response.ok(atividades).build();
    }
    
    @GET
    @Path("findAllByLocalAtivo")
    public Response findAllByLocalAtivo() {
        List<AtividadeLocal> atividades = atividadeLocalRepository.findAllByLocalAtivo();
        return Response.ok(atividades).build();
    }
}