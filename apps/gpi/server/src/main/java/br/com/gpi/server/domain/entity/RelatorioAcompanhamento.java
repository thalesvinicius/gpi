package br.com.gpi.server.domain.entity;

import br.com.gpi.server.domain.common.AuditedEntity;
import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

@Audited
@Entity
@Table(name = "RelatorioAcompanhamento")
@NamedQueries({
    @NamedQuery(name = RelatorioAcompanhamento.Query.findAllByProjetoVersaoPaiAndIdNotEquals, query = "SELECT ra FROM RelatorioAcompanhamento ra where ra.projeto.versaoPai = ?1 and ra.id <> ?2"),
    @NamedQuery(name = RelatorioAcompanhamento.Query.findCampanhaParaProjetoNoAno, query = "SELECT ra FROM RelatorioAcompanhamento ra where ra.projeto.id = ?1 and ra.ano = ?2"),
    @NamedQuery(name = RelatorioAcompanhamento.Query.findPendenteValidacaoByResponsavelInvestimento, query = "SELECT ra.projeto.nome, ra.projeto.id FROM RelatorioAcompanhamento ra where ra.projeto.usuarioInvestimento = ?1 and ra.situacaoAtual = 2")            
})
public class RelatorioAcompanhamento extends AuditedEntity<Long> implements Serializable {

    @JsonIgnore
    @ManyToOne(cascade = {CascadeType.DETACH, CascadeType.REFRESH}, fetch = FetchType.LAZY)
    @JoinColumn(name = "projeto_id")
    private Projeto projeto;

    @Column(name = "familia_id", nullable = false)
    private Long familiaId;
    
    @Column(name = "ano", nullable = false)
    private int ano;

    @Column(name = "dataInicioPrimeiraCampanha")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataInicioPrimeiraCampanha;

    @Column(name = "dataFimPrimeiraCampanha")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataFimPrimeiraCampanha;

    @Column(name = "dataInicioSegundaCampanha")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataInicioSegundaCampanha;

    @Column(name = "dataFimSegundaCampanha")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataFimSegundaCampanha;

    @Column(name = "dataValidacao")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataValidacao;

    @Column(name = "termoConfiabilidade", length = 255, nullable = false)
    private String termoConfiabilidade;

    @Column(name = "aceiteTermo")
    private Boolean aceiteTermo;

    @Column(name = "observacao")
    private String observacao;

    @Column(name = "nomeResponsavelRelatorio", length = 100)
    private String nomeResponsavelRelatorio;

    @Column(name = "telefoneResponsavelRelatorio", length = 18)
    private String telefoneResponsavelRelatorio;

    @Column(name = "emailResponsavelRelatorio", length = 50)
    private String emailResponsavelRelatorio;

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(name = "AnexoRelatorioAcompanhamento", joinColumns = {
        @JoinColumn(name = "relatorioAcompanhamento_id")}, inverseJoinColumns = {
        @JoinColumn(name = "anexo_id")})
    private Set<Anexo> anexos;
    
    @NotAudited
    @OneToMany(mappedBy = "relatorioAcompanhamentoId", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<RelatorioAcompanhamentoSituacao> situacao;
    
    @Column(name = "situacaoAtual_id")
    private SituacaoRelatorioEnum situacaoAtual;

    public Projeto getProjeto() {
        return projeto;
    }

    public void setProjeto(Projeto projeto) {
        this.projeto = projeto;
    }

    public int getAno() {
        return ano;
    }

    public void setAno(int ano) {
        this.ano = ano;
    }

    public Date getDataInicioPrimeiraCampanha() {
        return dataInicioPrimeiraCampanha;
    }

    public void setDataInicioPrimeiraCampanha(Date dataInicioPrimeiraCampanha) {
        this.dataInicioPrimeiraCampanha = dataInicioPrimeiraCampanha;
    }

    public Date getDataFimPrimeiraCampanha() {
        return dataFimPrimeiraCampanha;
    }

    public void setDataFimPrimeiraCampanha(Date dataFimPrimeiraCampanha) {
        this.dataFimPrimeiraCampanha = dataFimPrimeiraCampanha;
    }

    public Date getDataInicioSegundaCampanha() {
        return dataInicioSegundaCampanha;
    }

    public void setDataInicioSegundaCampanha(Date dataInicioSegundaCampanha) {
        this.dataInicioSegundaCampanha = dataInicioSegundaCampanha;
    }

    public Date getDataFimSegundaCampanha() {
        return dataFimSegundaCampanha;
    }

    public void setDataFimSegundaCampanha(Date dataFimSegundaCampanha) {
        this.dataFimSegundaCampanha = dataFimSegundaCampanha;
    }

    public String getTermoConfiabilidade() {
        return termoConfiabilidade;
    }

    public void setTermoConfiabilidade(String termoConfiabilidade) {
        this.termoConfiabilidade = termoConfiabilidade;
    }

    public Boolean isAceiteTermo() {
        return aceiteTermo;
    }

    public void setAceiteTermo(Boolean aceiteTermo) {
        this.aceiteTermo = aceiteTermo;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public String getNomeResponsavelRelatorio() {
        return nomeResponsavelRelatorio;
    }

    public void setNomeResponsavelRelatorio(String nomeResponsavelRelatorio) {
        this.nomeResponsavelRelatorio = nomeResponsavelRelatorio;
    }

    public String getTelefoneResponsavelRelatorio() {
        return telefoneResponsavelRelatorio;
    }

    public void setTelefoneResponsavelRelatorio(String telefoneResponsavelRelatorio) {
        this.telefoneResponsavelRelatorio = telefoneResponsavelRelatorio;
    }

    public String getEmailResponsavelRelatorio() {
        return emailResponsavelRelatorio;
    }

    public void setEmailResponsavelRelatorio(String emailResponsavelRelatorio) {
        this.emailResponsavelRelatorio = emailResponsavelRelatorio;
    }

    public Set<Anexo> getAnexos() {
        return anexos;
    }

    public void setAnexos(Set<Anexo> anexos) {
        this.anexos = anexos;
    }

    public Date getDataValidacao() {
        return dataValidacao;
    }

    public void setDataValidacao(Date dataValidacao) {
        this.dataValidacao = dataValidacao;
    }

    public List<RelatorioAcompanhamentoSituacao> getSituacao() {
        return situacao;
    }

    public void setSituacao(List<RelatorioAcompanhamentoSituacao> situacao) {
        this.situacao = situacao;
    }

    public SituacaoRelatorioEnum getSituacaoAtual() {
        return situacaoAtual;
    }

    public void setSituacaoAtual(SituacaoRelatorioEnum situacaoAtual) {
        this.situacaoAtual = situacaoAtual;
    }

    public Long getFamiliaId() {
        return familiaId;
    }

    public void setFamiliaId(Long familiaId) {
        this.familiaId = familiaId;
    }

    public static class Filters {

        public Filters() {
        }

        public static final String ANO = "ano";
        public static final String SITUACAO_ACOMPANHAMENTO = "situacao";
        public static final String NOME_EMPRESA = "nomeEmpresa";
        public static final String NOME_PROJETO = "nomeProjeto";
        public static final String ESTAGIO = "estagio";
        public static final String CADEIA_PRODUTIVA = "cadeiaProdutivaId";
        public static final String DEPARTAMENTO = "departamentos";
        public static final String USUARIO_RESPONSAVEL = "usuariosResponsaveis";
        
    }    
    
    public static class Query {

        public static final String findAllByProjetoVersaoPaiAndIdNotEquals = "RelatorioAcompanhamento.findAllByProjetoVersaoPaiAndIdNotEquals";
        public static final String findCampanhaParaProjetoNoAno = "RelatorioAcompanhamento.findCampanhaParaProjetoNoAno";
        public static final String findPendenteValidacaoByResponsavelInvestimento = "RelatorioAcompanhamento.findPendenteValidacaoByResponsavelInvestimento"; 
        
        public Query(){            
        }
    }
    
}
