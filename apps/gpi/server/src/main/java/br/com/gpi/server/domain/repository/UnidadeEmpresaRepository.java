package br.com.gpi.server.domain.repository;

import br.com.gpi.server.domain.entity.UnidadeEmpresa;
import java.util.List;
import org.apache.deltaspike.data.api.EntityRepository;
import org.apache.deltaspike.data.api.Modifying;
import org.apache.deltaspike.data.api.Query;
import org.apache.deltaspike.data.api.Repository;
import org.apache.deltaspike.data.api.criteria.CriteriaSupport;


@Repository(forEntity = UnidadeEmpresa.class)
public abstract class UnidadeEmpresaRepository   implements CriteriaSupport<UnidadeEmpresa>,EntityRepository<UnidadeEmpresa, Long> {

    
    @Query(value = "DELETE FROM UnidadeEmpresa u WHERE u.id IN ?1")
    @Modifying
    public abstract void deleteById(List<Long> ids);
    
    @Query(named=UnidadeEmpresa.Query.deleteByEmpresaId)
    @Modifying
    public abstract void deleteByEmpresaId(List<Long> ids);
    
    @Query(named=UnidadeEmpresa.Query.getUnidadeComNome)
    public abstract List<UnidadeEmpresa> getUnidadeComNome(String nomeUnidade, Long idEmpresa);
}
