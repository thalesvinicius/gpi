package br.com.gpi.server.core.jpa.converter;

import br.com.gpi.server.domain.entity.Estagio;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = false)
public class EstagioEnumConverter implements AttributeConverter<Estagio, Integer> {

    @Override
    public Integer convertToDatabaseColumn(Estagio estagio) {
        return estagio.getId();
    }

    @Override
    public Estagio convertToEntityAttribute(Integer id) {
        return Estagio.getEstagioById(id);
    }

}
