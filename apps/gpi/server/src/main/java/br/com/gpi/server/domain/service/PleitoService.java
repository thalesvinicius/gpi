package br.com.gpi.server.domain.service;

import br.com.gpi.server.domain.common.BaseService;
import br.com.gpi.server.domain.entity.Emprego;
import br.com.gpi.server.domain.entity.Estagio;
import br.com.gpi.server.domain.entity.FaturamentoPrevisto;
import br.com.gpi.server.domain.entity.Financeiro;
import br.com.gpi.server.domain.entity.InvestimentoPrevisto;
import br.com.gpi.server.domain.entity.Pleito;
import br.com.gpi.server.domain.entity.Projeto;
import br.com.gpi.server.domain.entity.SituacaoProjetoEnum;
import br.com.gpi.server.domain.entity.TipoEmprego;
import br.com.gpi.server.domain.entity.User;
import br.com.gpi.server.domain.entity.UsoFonte;
import br.com.gpi.server.domain.entity.UsuarioExterno;
import br.com.gpi.server.domain.entity.enumerated.EnumCadeiaProdutiva;
import br.com.gpi.server.domain.entity.enumerated.EnumTipoInvestimento;
import br.com.gpi.server.domain.repository.PleitoRepository;
import br.com.gpi.server.domain.repository.ProjetoRepository;
import br.com.gpi.server.domain.repository.UserRepository;
import br.com.gpi.server.util.LogUtil;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

public class PleitoService extends BaseService<Pleito> {

    private static final Set<EnumTipoInvestimento> investimentosSucroEnerg = new HashSet<>(
            Arrays.asList(EnumTipoInvestimento.INVESTIMENTO_AGRICOLA,
                    EnumTipoInvestimento.INVESTIMENTO_CAPACITACAO_PROFISSIONAL,
                    EnumTipoInvestimento.INVESTIMENTO_CAPACITACAO_PROFISSIONAL,
                    EnumTipoInvestimento.INVESTIMENTO_INDUSTRIAL));
    private static final Set<EnumTipoInvestimento> investimentosOutros = new HashSet<>(
            Arrays.asList(EnumTipoInvestimento.INVESTIMENTO_MAQ_EQUIP_TERRENO_OBRAS,
                    EnumTipoInvestimento.INVESTIMENTO_CAPITAL_GIRO_E_OUTROS));

    private static final Set<EnumTipoInvestimento> tipoInvestUsoFonte = new HashSet<EnumTipoInvestimento>(
            Arrays.asList(EnumTipoInvestimento.BDMG, EnumTipoInvestimento.INVESTIMENTO_ESTUDO_PROJETO_DESP_PRE_OPERACIONAIS,
                    EnumTipoInvestimento.INVESTIMENTO_MAQ_EQUIP_NACIONAIS, EnumTipoInvestimento.INVESTIMENTO_MAQ_EQUIP_IMPORTADOS,
                    EnumTipoInvestimento.INVESTIMENTO_MAQ_EQUIP_USADOS,
                    EnumTipoInvestimento.INVESTIMENTO_TERRENO, EnumTipoInvestimento.OUTROS_INVESTIMENTOS)
    );

    private static final Set<TipoEmprego> empregosOutro = new HashSet<>(Arrays.asList(
            TipoEmprego.TEMPORARIO,
            TipoEmprego.PERMANENTE));
    private static final Set<TipoEmprego> empregosHidricos = new HashSet<>(Arrays.asList(
            TipoEmprego.TEMPORARIO_OBRA,
            TipoEmprego.PERMANENTE_OPERACAO));

    private static final Set<TipoEmprego> empregosSucroEnergetico = new HashSet<>(Arrays.asList(
            TipoEmprego.PERMANENTE_MATURACAO_AGRICOLA,
            TipoEmprego.TEMPORARIO_IMPLANTACAO_INDUSTRIAL,
            TipoEmprego.PERMANENTE_MATURACAO_INDUSTRIAL,
            TipoEmprego.TEMPORARIO_IMPLANTACAO_AGRICOLA));

    private static final Set<Integer> setorHidricoMineracaoSiderurgia = new HashSet<>(
            Arrays.asList(EnumCadeiaProdutiva.ENERGIAS_HIDRICAS.getId(),
                    EnumCadeiaProdutiva.MINERACAO_FERRO.getId(),
                    EnumCadeiaProdutiva.SIDERURGIA.getId()));

    @Inject
    private ProjetoService projetoService;

    @Inject
    private UserService userService;

    @Inject
    private UserRepository userRepository;

    @Inject
    private ProjetoRepository projetoRepository;

    @Inject
    private PleitoRepository pleitoRepository;

    @Inject
    private UserTransaction transaction;

    @Inject
    private EmailService emailService;

    @Inject
    public PleitoService(EntityManager em) {
        super(Pleito.class, em);
    }

    public Collection<String> validation(Long projeto, Long idUsuario) {
        Projeto pros = projetoService.findById(projeto);
        Collection<String> validacoes = validateAll(createValidators(), pros);
        if (validacoes.isEmpty()) {
            if (pros.getEstagioAtual().getId().equals(Estagio.INICIO_PROJETO.getId())) {
                projetoService.updateSituacaoAndEstagioProjeto(SituacaoProjetoEnum.ATIVO, Estagio.PROJETO_PROMISSOR, projeto, idUsuario);
            } else {
                projetoService.updateSituacaoProjeto(SituacaoProjetoEnum.ATIVO, projeto, idUsuario);
            }

            User responsavel = pros.getEmpresa().getResponsavel();
            if (responsavel != null && responsavel.getEmail() != null) {
                emailService.sendSuccessfullyICEValidationEmail(responsavel.getEmail());
            }
        }
        return validacoes;
    }

    public Collection<String> dryValidation(Projeto pro, Long idUsuario) {
        return validateAll(createValidators(), pro);
    }

    private Collection<ICEValidator> createValidators() {
        ArrayList<ICEValidator> validators = new ArrayList<>();
        validators.add(createEmpregosValidator());
        validators.add(createCronogramaValidator());
        validators.add(getUsoFonteValidator());
        validators.add(createInvestPrevistoVlidator());
        validators.add(createFaturamentoValidator());
        validators.add(createContatoValidator());
        validators.add(createPleitoValidator());
        return validators;

    }

    private Collection<String> validateAll(Collection<ICEValidator> lista, final Projeto subject) {
        Stream<String> valid = lista.stream().flatMap((validator) -> {
            return validator.validate(subject).stream();
        });
        return valid.collect(Collectors.toList());
    }

    private ICEValidator createContatoValidator() {
        return (pro) -> {
            return Optional.ofNullable(pro.getEmpresa()).map((e) -> {
                return Optional.ofNullable(userService.findUserExternalByFilters(e.getId())).orElse(Collections.EMPTY_LIST).isEmpty()
                        ? Collections.singleton(msgAoMenos1Registro("Contato"))
                        : Collections.EMPTY_SET;
            }).orElse(Collections.EMPTY_SET);
        };
    }

    /*
     Ano de Realização, Investimento em Máquinas, 
     Equipamentos, Terreno e Obras Civis (em R$),Capital de giro e Outros  
     Investimentos  (em R$) ,Investimento Total (em R$) e Somátório.
     */
    public ICEValidator createInvestPrevistoVlidator() {

        ICEValidator valid = (pro) -> {
            Set<EnumTipoInvestimento> investimentosEsperados = EnumCadeiaProdutiva.SUCROENERGETICO.getId().longValue() == pro.getCadeiaProdutiva().getId() ? investimentosSucroEnerg : investimentosOutros;
            Set<EnumTipoInvestimento> encontrados = new HashSet<>();
            Set<String> faltando = new HashSet<>();
            if (pro.getFinanceiro() != null && pro.getFinanceiro().getInvestimentoPrevistoList() != null) {
                for (InvestimentoPrevisto ip : pro.getFinanceiro().getInvestimentoPrevistoList()) {
                    if (ip.getValor() >= 0) {
                        encontrados.add(ip.getTipoInvestimento());
                    }
                }
            }
            for (EnumTipoInvestimento esperado : investimentosEsperados) {
                if (!encontrados.contains(esperado)) {
                    faltando.add(msgCampoObrigatorio(esperado.getDescription()));
                }
            }
            return faltando;
        };
        ICEValidator valFunc = (pro) -> {
            final Set<EnumTipoInvestimento> investimentosEsperados = EnumCadeiaProdutiva.SUCROENERGETICO.getId().equals(pro.getCadeiaProdutiva().getId().intValue()) ? investimentosSucroEnerg : investimentosOutros;
            final Set<InvestimentoPrevisto> encontrados = Optional.ofNullable(pro.getFinanceiro()).map(Financeiro::getInvestimentoPrevistoList).orElse(Collections.emptySet());
            final Set<EnumTipoInvestimento> tiposInvestEncontrados = encontrados.stream().filter((ip) -> ip.getValor() != null && ip.getValor() >= 0)
                    .collect(Collectors.groupingBy(InvestimentoPrevisto::getTipoInvestimento)).keySet();
            Stream<EnumTipoInvestimento> faltando = investimentosEsperados.stream().filter((esperado) -> !tiposInvestEncontrados.contains(esperado));
            return faltando.map((tpInv) -> msgAoMenos1Registro(tpInv.getDescription())).collect(Collectors.toSet());
        };
        return valFunc;
    }

    /**
     * Ver RNE 112.
     *
     * @return Validator que implementa esta regra.
     */
    public ICEValidator createEmpregosValidator() {
        return (Projeto pro) -> {
            final Set<TipoEmprego> empregosEsperados = (EnumCadeiaProdutiva.SUCROENERGETICO.getId().equals(pro.getCadeiaProdutiva().getId().intValue())) ? empregosSucroEnergetico
                    : ((setorHidricoMineracaoSiderurgia.contains(pro.getCadeiaProdutiva().getId().intValue())) ? empregosHidricos : empregosOutro);
            return Optional.ofNullable(pro.getEmpregos()).map((emps) -> {
                Set<TipoEmprego> empregosEncontrados = emps.stream().filter((emp) -> {
                    return emp.getDireto() >= 0;
                }).collect(Collectors.groupingBy(Emprego::getTipoEmprego)).keySet();
                return empregosEsperados.stream().filter((tp) -> !empregosEncontrados.contains(tp));
            }).orElse(empregosEsperados.stream()).map((te) -> msgAoMenos1Registro(" Empregos " + te.getDescription())).collect(Collectors.toSet());
        };
    }

    public ICEValidator createCronogramaValidator() {
        return (Projeto pro) -> {
            Set<String> required = new HashSet<>();
            if (pro.getCadeiaProdutiva().getId() == EnumCadeiaProdutiva.SUCROENERGETICO.getId().longValue()) {
                required.add("Início do Projeto Excutivo");
                required.add("Término do Projeto Excutivo");
                required.add("Início de implantação (Agrícola,Industrial)");
                required.add("Término de implantação (Agrícola,Industrial)");
                required.add("Início de contratação de equipamentos");
                required.add("Início de Operação");
            } else {
                required.add("Início da implantação");
                required.add("Início de Operação");
                required.add("Início do Projeto");
                required.add("Término do Projeto");
            }
            return Optional.ofNullable(pro.getCronogramas()).map((cronogramas) -> {
                Set<String> found = new HashSet<>();
                cronogramas.stream().forEach((cron) -> {
                    if (cron.getInicioImplantacao() != null) {
                        found.add("Início da implantação");
                    }
                    if (cron.getInicioImplantacaoAgricola() != null && cron.getInicioImplantacaoIndustrial() != null) {
                        found.add("Início de implantação (Agrícola,Industrial)");
                    }
                    if (cron.getInicioOperacao() != null || cron.getInicioOperacaoSucroenergetico() != null) {
                        found.add("Início de Operação");
                    }
                    if (cron.getTerminoProjeto() != null) {
                        found.add("Término do Projeto");
                    }
                    if (cron.getInicioProjeto() != null) {
                        found.add("Início do Projeto");
                    }
                    if (cron.getTerminoImplantacaoAgricola() != null && cron.getTerminoImplantacaoIndustrial() != null) {
                        found.add("Término de implantação (Agrícola,Industrial)");
                    }
                    if (cron.getTerminoProjetoExecutivo() != null) {
                        found.add("Término do Projeto Excutivo");
                    }
                    if (cron.getInicioProjetoExecutivo() != null) {
                        found.add("Início do Projeto Excutivo");
                    }
                    if (cron.getInicioContratacaoEquipamentos() != null) {
                        found.add("Início de contratação de equipamentos");
                    }

                });
                return required.stream().filter((exp) -> !found.contains(exp));
            }).orElse(required.stream()).map((field) -> msgCampoObrigatorio(field)).collect(Collectors.toSet());
        };
    }

    public ICEValidator createPleitoValidator() {
        return (Projeto pro) -> {
            Set<String> required = new HashSet<>();
            if (pro.getPleito() == null || pro.getPleito().getPleitos() == null || pro.getPleito().getPleitos().trim().isEmpty()) {
                required.add(msgCampoObrigatorio("Pleitos"));
            }
            return required;
        };
    }

    private ICEValidator getUsoFonteValidator() {
        return (pro) -> {
            Set<EnumTipoInvestimento> investimentosEsperados = tipoInvestUsoFonte;
            return Optional.ofNullable(pro.getFinanceiro()).map((fin) -> {
                Set invetEncontrados = Optional.ofNullable(fin.getUsoFonteList()).map((usos) -> {
                    return usos.stream().filter((e) -> (Optional.ofNullable(e.getValorARealizar()).orElse(0.0D)
                            + Optional.ofNullable(e.getValorRealizado()).orElse(Double.NaN)) >= 0.0d)
                            .collect(Collectors.groupingBy(UsoFonte::getTipo)).keySet();
                }).orElse(Collections.EMPTY_SET);
                return investimentosEsperados.stream().filter((p) -> !invetEncontrados.contains(p));
            }).orElse(investimentosEsperados.stream()).map((t) -> msgCampoObrigatorio("A realizar ou realizado da coluna " + t.getDescription()))
                    .collect(Collectors.toSet());
        };
    }

    private ICEValidator createFaturamentoValidator() {
        String registros = "Necessário ao menos um registro de faturamento";
        return (pro) -> {
            return Optional.ofNullable(pro.getFinanceiro()).map((fin) -> {
                return Optional.ofNullable(fin.getFaturamentoPrevistoList()).map((fatsCol) -> {
                    Map<Integer, Long> m = fatsCol.stream().collect(
                            Collectors.groupingBy(FaturamentoPrevisto::getAno, Collectors.counting()));
                    return m.values().stream().reduce((e1, e2) -> e1 + e2).orElse(0L);
                }).map((l) -> (l > 0) ? Collections.EMPTY_LIST : Collections.singleton(registros)).orElse(Collections.EMPTY_LIST);
            }).orElse(Collections.EMPTY_LIST);
        };
    }

    private Optional<String> validateUsoFonte(EnumTipoInvestimento tipo, Optional<UsoFonte> uso) {
        Optional<String> ret = Optional.of(msgCampoObrigatorio("Realizado ou a \"Realizar\" "
                + "da Coluna \"" + tipo.getDescription() + " "));
        if (!uso.isPresent()) {
            return ret;
        }
        if (uso.get().getValorARealizar() == null || uso.get().getValorRealizado() == null) {
            return ret;
        }
        return Optional.empty();
    }

    private Optional<String> validateInvestPrevisto(EnumTipoInvestimento tipo, Optional<InvestimentoPrevisto> ip) {
        Optional<String> ret = Optional.of(msgCampoObrigatorio("Ano de Realização para \"" + tipo.getDescription()));
        if (!ip.isPresent()) {
            return ret;
        }
        if (ip.get().getAno() <= 1000) {
            return ret;
        }
        return Optional.empty();
    }

    public void acceptICE(Long idProjeto, Long userId) {
        projetoService.updateSituacaoProjeto(SituacaoProjetoEnum.PEDENTE_VALIDACAO, idProjeto, userId, new Date());
        Projeto pros = projetoService.findById(idProjeto);
        User responsavel = pros.getEmpresa().getResponsavel();
        if (responsavel != null && responsavel.getEmail() != null) {
            emailService.sendSuccessfullyICEAcceptationEmail(responsavel.getEmail());
        }
    }

    @FunctionalInterface
    public interface ICEValidator {

        Collection<String> validate(Projeto projeto);
    }

    private String msgCampoObrigatorio(String nomeCampo) {
        return "O campo \"" + nomeCampo + "\" é obrigatório";
    }

    private String msgAoMenos1Registro(String nome) {
        return "Deve haver ao menos um registro de \"" + nome + "\".";
    }

    public Pleito saveOrUpdate(Pleito pleito, User user, Long idProjeto) throws IllegalStateException, SecurityException, SystemException {
        try {
            transaction.begin();
            this.getEm().joinTransaction();

            pleito.setUsuarioResponsavel(user);
            pleito.setDataUltimaAlteracao(new Date());
            Projeto projeto = this.getEm().find(Projeto.class, idProjeto);
            pleito.setProjeto(projeto);
            pleito.setDataUltimaAlteracao(new Date());
            //Altera a situação do projeto se o estágio for INICIO_PROJETO.
            boolean isUsuarioExterno = user instanceof UsuarioExterno;
            if (projetoService.findEstagioAtualById(idProjeto).getEstagio() == Estagio.INICIO_PROJETO) {
                if (isUsuarioExterno) {
                    projetoService.updateSituacaoProjetoTransactional(SituacaoProjetoEnum.PENDENTE_ACEITE, idProjeto, user.getId(), new Date());
                } else {
                    projetoService.updateSituacaoProjetoTransactional(SituacaoProjetoEnum.PEDENTE_VALIDACAO, idProjeto, user.getId(), new Date());

                }
            }
            pleito = (Pleito) this.merge(pleito);
            if (isUsuarioExterno && projeto.getEmpresa().getResponsavel() != null && projeto.getEmpresa().getResponsavel().getEmail() != null) {
                emailService.sendSuccessfullyRegistrationClaimEmail(projeto.getEmpresa().getResponsavel().getEmail());
            }
            transaction.commit();
            return pleito;
        } catch (Exception ex) {
            transaction.rollback();
            Logger.getLogger(ProjetoService.class.getName()).log(Level.SEVERE, null, ex);
            throw new RuntimeException(ex);
        }
    }
    
    public Object verifyChange(Long idProjeto) throws SystemException {
        String atributos = " pleitos_MOD, observacoes_MOD ";
        Object result = this.getEm().createNativeQuery(
                LogUtil.montaConsultaLog("LogPleito", atributos, idProjeto).toString()).getResultList();

        return result;
    }     
}
