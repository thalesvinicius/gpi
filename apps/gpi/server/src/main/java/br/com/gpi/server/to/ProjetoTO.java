package br.com.gpi.server.to;

import br.com.gpi.server.domain.entity.AtividadeProjeto;
import br.com.gpi.server.domain.entity.CadeiaProdutiva;
import br.com.gpi.server.domain.entity.Endereco;
import br.com.gpi.server.domain.entity.Estagio;
import br.com.gpi.server.domain.entity.EstagioProjeto;
import br.com.gpi.server.domain.entity.SituacaoEmpresa;
import br.com.gpi.server.domain.entity.SituacaoProjeto;
import br.com.gpi.server.domain.entity.SituacaoProjetoEnum;
import br.com.gpi.server.domain.entity.User;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ProjetoTO {

    private Long id;
    private Integer versao;
    private Long versaoPai;
    private SituacaoProjetoEnum situacao;
    private Estagio estagio;
    private String nomeProjeto;
    private Long idEmpresa;
    private String nomeEmpresa;
    private String cnpjEmpresa;
    private Endereco enderecoEmpresa;
    private Long idUnidade;
    private String nomeUnidade;
    private Endereco enderecoUnidade;
    private List<ProjetoTO> versoes;
    private CadeiaProdutiva cadeiaProdutiva;
    private List<InstrumentoFormalizacaoTO> instrumentos;
    private List<EstagioProjeto> estagios;
    private SituacaoEmpresa situacaoEmpresa;
    private User usuarioInvestimento;
    private SituacaoProjeto situacaoProjeto;
    private List<SituacaoProjeto> situacoesProjeto;
    private AtividadeProjeto ultimaAtividade;
    private Date dataUltimaAlteracao;
    private User usuarioResponsavel;
    private User responsavelInvestimento;
    private Long usuarioExternoResponsavelId;
    private Long departamentoUsuarioInternoResponsavelId;

    public ProjetoTO() {
    }

    public ProjetoTO(Long id, String nomeProjeto) {
        this.id = id;
        this.nomeProjeto = nomeProjeto;
    }

    public ProjetoTO(Long id, String nomeProjeto, Long versaoPai) {
        this(id, nomeProjeto);
        this.versaoPai = versaoPai;
    }

    public ProjetoTO(Estagio estagio) {
        this.estagio = estagio;
    }

    public ProjetoTO(Long id, Integer versao, SituacaoProjetoEnum situacao, Estagio estagio, String nomeProjeto, Long idEmpresa, String nomeEmpresa, CadeiaProdutiva cadeiaProdutiva) {
        this(id, nomeProjeto);
        this.versao = versao;
        this.situacao = situacao;
        this.estagio = estagio;
        this.idEmpresa = idEmpresa;
        this.nomeEmpresa = nomeEmpresa;
        this.cadeiaProdutiva = cadeiaProdutiva;
    }

    public ProjetoTO(Long id, Integer versao, SituacaoProjetoEnum situacao, List<SituacaoProjeto> situacoesProjeto, Estagio estagio, String nomeProjeto, Long idEmpresa, String nomeEmpresa, CadeiaProdutiva cadeiaProdutiva) {
        this(id, versao, situacao, estagio, nomeProjeto, idEmpresa, nomeEmpresa, cadeiaProdutiva);
        this.situacoesProjeto = situacoesProjeto;
    }

    public ProjetoTO(Long id, Integer versao, SituacaoProjeto situacao, Estagio estagio, String nomeProjeto, Long idEmpresa, String nomeEmpresa, CadeiaProdutiva cadeiaProdutiva, Date dataUltimaAlteracao, User usuarioResponsavel) {
        this(id, versao, situacao.getSituacao(), estagio, nomeProjeto, idEmpresa, nomeEmpresa, cadeiaProdutiva);
        this.situacaoProjeto = situacao;
        this.dataUltimaAlteracao = dataUltimaAlteracao;
        this.usuarioResponsavel = usuarioResponsavel;
    }

    public ProjetoTO(Long id, Integer versao, Long versaoPai, SituacaoProjeto situacao, Estagio estagio, String nomeProjeto, Long idEmpresa, String nomeEmpresa, CadeiaProdutiva cadeiaProdutiva, Date dataUltimaAlteracao, User usuarioResponsavel) {
        this(id, versao, versaoPai, situacao.getSituacao(), estagio, nomeProjeto, idEmpresa, nomeEmpresa, cadeiaProdutiva);
        this.situacaoProjeto = situacao;
        this.dataUltimaAlteracao = dataUltimaAlteracao;
        this.usuarioResponsavel = usuarioResponsavel;
    }

    public ProjetoTO(Long id, Integer versao, Long versaoPai, SituacaoProjeto situacao, Estagio estagio, String nomeProjeto, Long idEmpresa, String nomeEmpresa, CadeiaProdutiva cadeiaProdutiva, Date dataUltimaAlteracao, User usuarioResponsavel, User responsavelInvestimento) {
        this(id, versao, versaoPai, situacao.getSituacao(), estagio, nomeProjeto, idEmpresa, nomeEmpresa, cadeiaProdutiva);
        this.dataUltimaAlteracao = dataUltimaAlteracao;
        this.usuarioResponsavel = usuarioResponsavel;
        this.responsavelInvestimento = responsavelInvestimento;
    }
    
    public ProjetoTO(Long id, Integer versao, Long versaoPai, SituacaoProjeto situacao, Estagio estagio, String nomeProjeto, Long idEmpresa, String nomeEmpresa, SituacaoEmpresa situacaoEmpresa, CadeiaProdutiva cadeiaProdutiva, Date dataUltimaAlteracao, User usuarioResponsavel, User responsavelInvestimento) {
        this(id, versao, versaoPai, situacao.getSituacao(), estagio, nomeProjeto, idEmpresa, nomeEmpresa, cadeiaProdutiva);
        this.dataUltimaAlteracao = dataUltimaAlteracao;
        this.usuarioResponsavel = usuarioResponsavel;
        this.responsavelInvestimento = responsavelInvestimento;
        this.situacaoEmpresa = situacaoEmpresa;
    }
    
    public ProjetoTO(Long id, Integer versao, Long versaoPai, SituacaoProjeto situacao, List<SituacaoProjeto> situacoesProjeto, Estagio estagio, String nomeProjeto, Long idEmpresa, String nomeEmpresa, CadeiaProdutiva cadeiaProdutiva, Date dataUltimaAlteracao, User usuarioResponsavel, User responsavelInvestimento) {
        this(id, versao, versaoPai, situacao, estagio, nomeProjeto, idEmpresa, nomeEmpresa, cadeiaProdutiva, dataUltimaAlteracao, usuarioResponsavel, responsavelInvestimento);
        this.situacoesProjeto = situacoesProjeto;
    }

    public ProjetoTO(Long id, Integer versao, Long versaoPai, SituacaoProjetoEnum situacao, Estagio estagio, String nomeProjeto, Long idEmpresa, String nomeEmpresa, CadeiaProdutiva cadeiaProdutiva) {
        this(id, versao, situacao, estagio, nomeProjeto, idEmpresa, nomeEmpresa, cadeiaProdutiva);
        this.versaoPai = versaoPai;
    }

    public ProjetoTO(Long id, Integer versao, Long versaoPai, SituacaoProjeto situacao, Estagio estagio, String nomeProjeto, Long idEmpresa, String nomeEmpresa, CadeiaProdutiva cadeiaProdutiva) {
        this(id, versao, versaoPai, situacao.getSituacao(), estagio, nomeProjeto, idEmpresa, nomeEmpresa, cadeiaProdutiva);
        this.situacaoProjeto = situacao;
    }

    public ProjetoTO(Long id, Integer versao, Long versaoPai, SituacaoProjetoEnum situacao, Estagio estagio, String nomeProjeto, Long idEmpresa, String nomeEmpresa, String cnpjEmpresa, Endereco enderecoEmpresa, Long idUnidade, String nomeUnidade, Endereco enderecoUnidade) {
        this(id, versao, versaoPai, situacao, estagio, nomeProjeto, idEmpresa, nomeEmpresa, null);
        this.cnpjEmpresa = cnpjEmpresa;
        this.enderecoEmpresa = enderecoEmpresa;
        this.idUnidade = idUnidade;
        this.nomeUnidade = nomeUnidade;
        this.enderecoUnidade = enderecoUnidade;

    }

    public ProjetoTO(Long id, Integer versao, Long versaoPai, SituacaoProjeto situacao, Estagio estagio, String nomeProjeto, Long idEmpresa, String nomeEmpresa, String cnpjEmpresa, Endereco enderecoEmpresa, Long idUnidade, String nomeUnidade, Endereco enderecoUnidade) {
        this(id, versao, versaoPai, situacao.getSituacao(), estagio, nomeProjeto, idEmpresa, nomeEmpresa, cnpjEmpresa, enderecoEmpresa, idUnidade, nomeUnidade, enderecoUnidade);
        this.situacaoProjeto = situacao;
    }

    public ProjetoTO(Long id, Integer versao, Long versaoPai, SituacaoProjeto situacao, Estagio estagio, String nomeProjeto, Long idEmpresa, String nomeEmpresa, String cnpjEmpresa, Endereco enderecoEmpresa, Long idUnidade, String nomeUnidade, Endereco enderecoUnidade, AtividadeProjeto atividadeProjeto) {
        this(id, versao, versaoPai, situacao.getSituacao(), estagio, nomeProjeto, idEmpresa, nomeEmpresa, cnpjEmpresa, enderecoEmpresa, idUnidade, nomeUnidade, enderecoUnidade);
        this.situacaoProjeto = situacao;
        this.ultimaAtividade = atividadeProjeto;
    }

    public ProjetoTO(Long id, Integer versao, Long versaoPai, SituacaoProjetoEnum situacao, Estagio estagio, String nomeProjeto, Long idEmpresa, String nomeEmpresa, String cnpjEmpresa, Endereco enderecoEmpresa, Long idUnidade, String nomeUnidade, Endereco enderecoUnidade, SituacaoEmpresa situacaoEmpresa, User usuarioInvestimento) {
        this(id, versao, versaoPai, situacao, estagio, nomeProjeto, idEmpresa, nomeEmpresa, cnpjEmpresa, enderecoEmpresa, idUnidade, nomeUnidade, enderecoUnidade);
        this.situacaoEmpresa = situacaoEmpresa;
        this.usuarioInvestimento = usuarioInvestimento;
    }

    public ProjetoTO(Long id, Integer versao, Long versaoPai, SituacaoProjeto situacao, Estagio estagio, String nomeProjeto, Long idEmpresa, String nomeEmpresa, String cnpjEmpresa, Endereco enderecoEmpresa, Long idUnidade, String nomeUnidade, Endereco enderecoUnidade, SituacaoEmpresa situacaoEmpresa, User usuarioInvestimento) {
        this(id, versao, versaoPai, situacao.getSituacao(), estagio, nomeProjeto, idEmpresa, nomeEmpresa, cnpjEmpresa, enderecoEmpresa, idUnidade, nomeUnidade, enderecoUnidade, situacaoEmpresa, usuarioInvestimento);
        this.situacaoProjeto = situacao;
    }

    public ProjetoTO(Long id, Integer versao, Long versaoPai, SituacaoProjeto situacao, Estagio estagio, String nomeProjeto, Long idEmpresa, String nomeEmpresa, String cnpjEmpresa, Endereco enderecoEmpresa, Long idUnidade, String nomeUnidade, Endereco enderecoUnidade, SituacaoEmpresa situacaoEmpresa, User usuarioInvestimento, AtividadeProjeto atividadeProjeto) {
        this(id, versao, versaoPai, (situacao != null) ? situacao.getSituacao() : null, estagio, nomeProjeto, idEmpresa, nomeEmpresa, cnpjEmpresa, enderecoEmpresa, idUnidade, nomeUnidade, enderecoUnidade, situacaoEmpresa, usuarioInvestimento);
        this.situacaoProjeto = situacao;
        this.ultimaAtividade = atividadeProjeto;
    }
    
    public ProjetoTO(Long id, Integer versao, Long versaoPai, SituacaoProjeto situacao, Estagio estagio, String nomeProjeto, Long idEmpresa, String nomeEmpresa, SituacaoEmpresa situacaoEmpresa, CadeiaProdutiva cadeiaProdutiva, Date dataUltimaAlteracao, User usuarioResponsavel, User responsavelInvestimento, Long usuarioExternoResponsavelId, Long departamentoUsuarioInternoResponsavelId) {
        this(id, versao, versaoPai, situacao.getSituacao(), estagio, nomeProjeto, idEmpresa, nomeEmpresa, cadeiaProdutiva);
        this.dataUltimaAlteracao = dataUltimaAlteracao;
        this.usuarioResponsavel = usuarioResponsavel;
        this.responsavelInvestimento = responsavelInvestimento;
        this.usuarioExternoResponsavelId = usuarioExternoResponsavelId;
        this.situacaoEmpresa = situacaoEmpresa;
        this.departamentoUsuarioInternoResponsavelId = departamentoUsuarioInternoResponsavelId;
    }    

    public String getVersaoCompleta() {
        return String.format("%d.%02d", (versaoPai == null) ? this.id : versaoPai, versao);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getVersao() {
        return versao;
    }

    public void setVersao(Integer versao) {
        this.versao = versao;
    }

    public Long getVersaoPai() {
        return versaoPai;
    }

    public void setVersaoPai(Long versaoPai) {
        this.versaoPai = versaoPai;
    }

    public SituacaoProjetoEnum getSituacao() {
        return situacao;
    }

    public void setSituacao(SituacaoProjetoEnum situacao) {
        this.situacao = situacao;
    }

    public Estagio getEstagio() {
        return estagio;
    }

    public void setEstagio(Estagio estagio) {
        this.estagio = estagio;
    }

    public String getNomeEmpresa() {
        return nomeEmpresa;
    }

    public void setNomeEmpresa(String nomeEmpresa) {
        this.nomeEmpresa = nomeEmpresa;
    }

    public String getCnpjEmpresa() {
        return cnpjEmpresa;
    }

    public void setCnpjEmpresa(String cnpjEmpresa) {
        this.cnpjEmpresa = cnpjEmpresa;
    }

    public Endereco getEnderecoEmpresa() {
        return enderecoEmpresa;
    }

    public void setEnderecoEmpresa(Endereco enderecoEmpresa) {
        this.enderecoEmpresa = enderecoEmpresa;
    }

    public String getNomeUnidade() {
        return nomeUnidade;
    }

    public void setNomeUnidade(String nomeUnidade) {
        this.nomeUnidade = nomeUnidade;
    }

    public Endereco getEnderecoUnidade() {
        //Incluido temporáriamente para resolver erro crítico
        //FIXME: Remover a necessidade de criar um objeto vazio caso o endereco da unidade esteja nulo
        if (enderecoUnidade != null) {
            return enderecoUnidade;
        } else {
            return new Endereco();
        }
    }

    public void setEnderecoUnidade(Endereco enderecoUnidade) {
        this.enderecoUnidade = enderecoUnidade;
    }

    public List<ProjetoTO> getVersoes() {
        return versoes;
    }

    public void setVersoes(List<ProjetoTO> versoes) {
        this.versoes = versoes;
    }

    public Long getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(Long idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public Long getIdUnidade() {
        return idUnidade;
    }

    public void setIdUnidade(Long idUnidade) {
        this.idUnidade = idUnidade;
    }

    public String getNomeProjeto() {
        return nomeProjeto;
    }

    public void setNomeProjeto(String nomeProjeto) {
        this.nomeProjeto = nomeProjeto;
    }

    public CadeiaProdutiva getCadeiaProdutiva() {
        return cadeiaProdutiva;
    }

    public void setCadeiaProdutiva(CadeiaProdutiva cadeiaProdutiva) {
        this.cadeiaProdutiva = cadeiaProdutiva;
    }

    public String getCadeiaProdutivaDescricao() {
        if (this.cadeiaProdutiva != null) {
            return this.cadeiaProdutiva.getDescricao();
        } else {
            return null;
        }
    }

    public String getEstagioDescricao() {
        if (this.estagio != null) {
            return this.estagio.getDescricao();
        } else {
            return null;
        }
    }

    public String getSituacaoDescricao() {
        if (this.situacao != null) {
            return this.situacao.getDescription();
        } else {
            return null;
        }
    }

    public List<InstrumentoFormalizacaoTO> getInstrumentos() {
        return instrumentos;
    }

    public void setInstrumentos(List<InstrumentoFormalizacaoTO> instrumentos) {
        this.instrumentos = instrumentos;
    }

    public List<EstagioProjeto> getEstagios() {
        return estagios;
    }

    public void setEstagios(List<EstagioProjeto> estagios) {
        this.estagios = estagios;
    }

    public SituacaoEmpresa getSituacaoEmpresa() {
        return situacaoEmpresa;
    }

    public void setSituacaoEmpresa(SituacaoEmpresa situacaoEmpresa) {
        this.situacaoEmpresa = situacaoEmpresa;
    }

    public User getUsuarioInvestimento() {
        return usuarioInvestimento;
    }

    public void setUsuarioInvestimento(User usuarioInvestimento) {
        this.usuarioInvestimento = usuarioInvestimento;
    }

    public SituacaoProjeto getSituacaoProjeto() {
        return situacaoProjeto;
    }

    public void setSituacaoProjeto(SituacaoProjeto situacaoProjeto) {
        this.situacaoProjeto = situacaoProjeto;
    }

    public AtividadeProjeto getUltimaAtividade() {
        return ultimaAtividade;
    }

    public void setUltimaAtividade(AtividadeProjeto ultimaAtividade) {
        this.ultimaAtividade = ultimaAtividade;
    }

    public Date getDataUltimaAlteracao() {
        return dataUltimaAlteracao;
    }

    public void setDataUltimaAlteracao(Date dataUltimaAlteracao) {
        this.dataUltimaAlteracao = dataUltimaAlteracao;
    }

    public User getUsuarioResponsavel() {
        return usuarioResponsavel;
    }

    public void setUsuarioResponsavel(User usuarioResponsavel) {
        this.usuarioResponsavel = usuarioResponsavel;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 67 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ProjetoTO other = (ProjetoTO) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

    public User getResponsavelInvestimento() {
        return responsavelInvestimento;
    }

    public void setResponsavelInvestimento(User responsavelInvestimento) {
        this.responsavelInvestimento = responsavelInvestimento;
    }

    public List<SituacaoProjeto> getSituacoesProjeto() {
        return situacoesProjeto;
    }

    public void setSituacoesProjeto(List<SituacaoProjeto> situacoesProjeto) {
        this.situacoesProjeto = situacoesProjeto;
    }

    public Long getUsuarioExternoResponsavelId() {
        return usuarioExternoResponsavelId;
    }

    public void setUsuarioExternoResponsavelId(Long usuarioExternoResponsavelId) {
        this.usuarioExternoResponsavelId = usuarioExternoResponsavelId;
    }

    public Long getDepartamentoUsuarioInternoResponsavelId() {
        return departamentoUsuarioInternoResponsavelId;
    }

    public void setDepartamentoUsuarioInternoResponsavelId(Long departamentoUsuarioInternoResponsavelId) {
        this.departamentoUsuarioInternoResponsavelId = departamentoUsuarioInternoResponsavelId;
    }

}
