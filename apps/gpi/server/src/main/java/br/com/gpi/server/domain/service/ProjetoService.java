package br.com.gpi.server.domain.service;

import br.com.gpi.server.domain.common.BaseEntity;
import br.com.gpi.server.domain.common.BaseService;
import br.com.gpi.server.domain.common.QueryParameter;
import br.com.gpi.server.domain.entity.Anexo;
import br.com.gpi.server.domain.entity.AtividadeProjeto;
import br.com.gpi.server.domain.entity.CadeiaProdutiva;
import br.com.gpi.server.domain.entity.CadeiaProdutiva_;
import br.com.gpi.server.domain.entity.Departamento;
import br.com.gpi.server.domain.entity.Departamento_;
import br.com.gpi.server.domain.entity.Emprego;
import br.com.gpi.server.domain.entity.Emprego_;
import br.com.gpi.server.domain.entity.Empresa;
import br.com.gpi.server.domain.entity.Empresa_;
import br.com.gpi.server.domain.entity.Endereco;
import br.com.gpi.server.domain.entity.Estagio;
import br.com.gpi.server.domain.entity.EstagioProjeto;
import br.com.gpi.server.domain.entity.EstagioProjeto_;
import br.com.gpi.server.domain.entity.FaturamentoPrevisto;
import br.com.gpi.server.domain.entity.FaturamentoPrevisto_;
import br.com.gpi.server.domain.entity.Financeiro;
import br.com.gpi.server.domain.entity.Financeiro_;
import br.com.gpi.server.domain.entity.Infraestrutura;
import br.com.gpi.server.domain.entity.InstrumentoFormalizacao;
import br.com.gpi.server.domain.entity.InstrumentoFormalizacao_;
import br.com.gpi.server.domain.entity.InsumoProduto;
import br.com.gpi.server.domain.entity.InvestimentoPrevisto;
import br.com.gpi.server.domain.entity.InvestimentoPrevisto_;
import br.com.gpi.server.domain.entity.Localizacao;
import br.com.gpi.server.domain.entity.MeioAmbiente;
import br.com.gpi.server.domain.entity.NomeEmpresa;
import br.com.gpi.server.domain.entity.NomeEmpresa_;
import br.com.gpi.server.domain.entity.Pleito;
import br.com.gpi.server.domain.entity.Projeto;
import br.com.gpi.server.domain.entity.ProjetoRecente;
import br.com.gpi.server.domain.entity.Projeto_;
import br.com.gpi.server.domain.entity.RelatorioAcompanhamento;
import br.com.gpi.server.domain.entity.RelatorioAcompanhamentoSituacao;
import br.com.gpi.server.domain.entity.RelatorioAgendaPositiva;
import br.com.gpi.server.domain.entity.RelatorioAgendaPositivaAbstract;
import br.com.gpi.server.domain.entity.SituacaoProjeto;
import br.com.gpi.server.domain.entity.SituacaoProjetoEnum;
import br.com.gpi.server.domain.entity.SituacaoProjeto_;
import br.com.gpi.server.domain.entity.SituacaoRelatorioEnum;
import br.com.gpi.server.domain.entity.UnidadeEmpresa;
import br.com.gpi.server.domain.entity.UnidadeEmpresa_;
import br.com.gpi.server.domain.entity.User;
import br.com.gpi.server.domain.entity.UsuarioExterno;
import br.com.gpi.server.domain.entity.UsuarioExterno_;
import br.com.gpi.server.domain.entity.UsuarioInterno;
import br.com.gpi.server.domain.entity.UsuarioInterno_;
import br.com.gpi.server.domain.entity.ViewRelatorioAcompanhamentoAnual;
import br.com.gpi.server.domain.repository.EstagioProjetoRepository;
import br.com.gpi.server.domain.repository.ProjetoRepository;
import br.com.gpi.server.domain.repository.SituacaoProjetoRepository;
import br.com.gpi.server.to.EmpregoTO;
import br.com.gpi.server.to.EstagioProjetoTO;
import br.com.gpi.server.to.InstrumentoFormalizacaoTO;
import br.com.gpi.server.to.InvestimentoPrevistoTO;
import br.com.gpi.server.to.ProjetoTO;
import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TemporalType;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.transaction.NotSupportedException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.beanutils.BeanUtilsBean;
import org.hibernate.proxy.HibernateProxyHelper;

public class ProjetoService extends BaseService<Projeto> {
    
    @Inject
    private ProjetoRepository projetoRepository;
    
    @Inject
    private EstagioProjetoRepository estagioProjetoRepository;
    
    @Inject
    private SituacaoProjetoRepository situacaoProjetoRepository;
    
    @Inject
    private InstrumentoFormalizacaoService instrumentoFormalizacaoService;
    
    @Inject
    private UserTransaction userTransaction;

    @Inject
    private EmailService emailService;
    
    @Inject
    public ProjetoService(EntityManager em) {
        super(Projeto.class, em);
    }

    public Long saveNewProjeto(Projeto projeto, Long idUsuario) throws SystemException {
        projeto.setDataCadastro(new Date());
        projeto.setUltimaVersao(Boolean.TRUE);
        projeto.setVersao(0);
        projeto.setSituacao(SituacaoProjetoEnum.ATIVO);
        projeto.setEstagioAtual(Estagio.INICIO_PROJETO);
        return this.save(projeto, idUsuario);
    }

    public Long save(Projeto projeto, Long idUsuario) throws SystemException {
        try {
            userTransaction.begin();
            this.getEm().joinTransaction();
            User user = getEm().find(User.class, idUsuario);
            projeto.setUsuarioResponsavel(user);

            if (projeto.getUsuarioInvestimento() != null && projeto.getUsuarioInvestimento().getId() != null) {
                projeto.setUsuarioInvestimento(getEm().find(UsuarioInterno.class, projeto.getUsuarioInvestimento().getId()));
            }

            projeto.setEmpresa(getEm().find(Empresa.class, projeto.getEmpresaUnidadeDTO().getEmpresaId()));
            if (projeto.getEmpresaUnidadeDTO().getUnidadeId() != null) {
                projeto.setUnidade(getEm().find(UnidadeEmpresa.class, projeto.getEmpresaUnidadeDTO().getUnidadeId()));
            } else {
                projeto.setUnidade(null);
            }

            if (projeto.getCadeiaProdutiva() != null && projeto.getCadeiaProdutiva().getId() != null) {
                projeto.setCadeiaProdutiva(getEm().find(CadeiaProdutiva.class, projeto.getCadeiaProdutiva().getId()));
            }

            boolean newProject = false;
            if (projeto.getId() != null) {
                Projeto projetoDB = getEm().find(Projeto.class, projeto.getId());
                projetoDB.setDescricao(projeto.getDescricao());
                projetoDB.setTipo(projeto.getTipo());
                projetoDB.setNome(projeto.getNome());
                projetoDB.setEmpresa(projeto.getEmpresa());
                projetoDB.setProspeccaoAtiva(projeto.getProspeccaoAtiva());
                projetoDB.setCadeiaProdutiva(projeto.getCadeiaProdutiva());
                projetoDB.setInformacaoPrimeiroContato(projeto.getInformacaoPrimeiroContato());
                projetoDB.setAnexos(projeto.getAnexos());
                projetoDB.setUsuarioInvestimento(projeto.getUsuarioInvestimento());
                projetoDB.setUnidade(projeto.getUnidade());
                projetoDB.setUsuarioResponsavel(projeto.getUsuarioResponsavel());
                projetoDB.setSituacao(projeto.getSituacao());
                projetoDB.setJustificativa(projeto.getJustificativa());
                projetoDB.setDataCadastro(projeto.getDataCadastro());
                projeto = projetoDB;
            } else {
                newProject = true;
            }

            projeto.setDataUltimaAlteracao(new Date());

            SituacaoProjeto situacao = null;
            if (projeto.getSituacaoAtual() != null && projeto.getSituacaoAtual().getId() != null) {
                if (projeto.getSituacaoAtual().getSituacao() != projeto.getSituacao()) {
                    situacao = new SituacaoProjeto(projeto);
                    nullifyId(projeto.getSituacaoAtual());
                    projeto.setSituacaoAtual(null);
                }
            } else {
                situacao = new SituacaoProjeto(projeto);
            }

            projeto = (Projeto) merge(projeto);

            if (situacao != null) {
                situacao.setProjeto(projeto);
                situacao = (SituacaoProjeto) this.merge(situacao);
                projeto.setSituacaoAtual(situacao);
                projeto = (Projeto) merge(projeto);

            }

            //FIXME paulovfm verificar uma maneira mais elegante de setar a versão pai
            if (projeto.getVersaoPai() == null) {
                projeto.setVersaoPai(projeto.getId());

                projeto = (Projeto) merge(projeto);
            }

            if (projeto.getEstagioAtual() != null) {
                this.saveHistoricoEstagioProjetoTransactional(projeto.getEstagioAtual(), projeto.getId(), new Date());
            }
            if (newProject && projeto.getEmpresa().getResponsavel() != null && projeto.getEmpresa().getResponsavel().getEmail() != null) {
                emailService.sendSuccessfullyRegistrationProjectEmail(projeto.getEmpresa().getResponsavel().getEmail());
            }
            userTransaction.commit();
            return projeto.getId();
        } catch (Exception ex) {
            userTransaction.rollback();
            Logger.getLogger(ProjetoService.class.getName()).log(Level.SEVERE, null, ex);
            throw new RuntimeException(ex);
        }

    }
    
    @Deprecated
    public void updateSituacaoAndEstagioProjeto(SituacaoProjetoEnum situacao, Estagio estagio, Long idProjeto, Long usuarioId) {
        this.updateSituacaoProjeto(situacao, idProjeto, usuarioId);
        this.updateEstagioProjeto(estagio, idProjeto, usuarioId);
    }
    
    public void updateSituacaoAndEstagioProjeto(SituacaoProjetoEnum situacao, Estagio estagio, Long idProjeto, Long usuarioId, Date dataSituacao, Date dataEstagio) {
        this.updateSituacaoProjeto(situacao, idProjeto, usuarioId, dataSituacao != null ? dataSituacao : new Date());
        this.updateEstagioProjeto(estagio, idProjeto, usuarioId, dataEstagio != null ? dataEstagio : new Date());
    }
    
    @Deprecated
    public void updateSituacaoProjeto(SituacaoProjetoEnum situacao, Long idProjeto, Long usuarioId) {
        this.updateSituacaoProjeto(situacao, idProjeto, usuarioId, null);
    }
    
    public void updateSituacaoProjeto(SituacaoProjetoEnum situacao, Long idProjeto, Long usuarioId, Date dataSituacao) {
        User usuario = this.getEm().find(User.class, usuarioId);
        SituacaoProjeto situacaoProjeto = new SituacaoProjeto(situacao, idProjeto, null);
        SituacaoProjetoEnum situacaoAtualEnum = this.createTypedQuery(SituacaoProjetoEnum.class, Projeto.Query.findSituacaoAtualEnumByProjetoId, idProjeto).getSingleResult();
        if (situacaoAtualEnum != null && situacaoAtualEnum != situacao) {
            situacaoProjeto = situacaoProjetoRepository.save(situacaoProjeto);
            projetoRepository.updateSituacaoProjeto(situacaoProjeto, idProjeto, usuario, dataSituacao != null ? dataSituacao : new Date(), situacao);
        }
    }
    
    public void updateSituacaoProjetoTransactional(SituacaoProjetoEnum situacao, Long idProjeto, Long usuarioId, Date dataSituacao) {
        this.getEm().joinTransaction();
        User usuario = this.getEm().find(User.class, usuarioId);
        SituacaoProjeto situacaoProjeto = new SituacaoProjeto(situacao, idProjeto, null);
        SituacaoProjetoEnum situacaoAtualEnum = this.createTypedQuery(SituacaoProjetoEnum.class, Projeto.Query.findSituacaoAtualEnumByProjetoId, idProjeto).getSingleResult();
        if (situacaoAtualEnum != null && situacaoAtualEnum != situacao) {
            situacaoProjeto = (SituacaoProjeto) this.merge(situacaoProjeto);
            this.createUpdateDeleteQuery(Projeto.Query.updateSituacaoProjeto, situacaoProjeto, idProjeto, usuario, dataSituacao != null ? dataSituacao : new Date(), situacao).executeUpdate();
        }
    }
    
    @Deprecated
    public void updateEstagioProjeto(Estagio estagio, Long idProjeto, Long usuarioId) {
        updateEstagioProjeto(estagio, idProjeto, usuarioId, new Date());
    }
    
    public final void updateEstagioProjeto(Estagio estagio, Long idProjeto, Long usuarioId, Date dataInicioEstagio) {
        List<EstagioProjeto> estagios = estagioProjetoRepository.findAllByProjeto(idProjeto);
        boolean estagioPP = estagio == Estagio.PROJETO_PROMISSOR && !estagios.isEmpty() && !estagios.stream().anyMatch(u -> u.getEstagioId() == Estagio.PROJETO_PROMISSOR.getId());
        boolean outrosEstagio = !estagio.getId().equals(Estagio.PROJETO_PROMISSOR.getId());
        
        if (estagioPP || outrosEstagio) {
            User usuario = this.getEm().find(User.class, usuarioId);
            projetoRepository.updateEstagioProjeto(estagio, idProjeto, usuario, new Date());
            this.saveHistoricoEstagioProjeto(estagio, idProjeto, dataInicioEstagio);
        }
    }
    
    public final void updateEstagioProjetoTransactional(Estagio estagio, Long idProjeto, Long usuarioId, Date dataInicioEstagio) {
        this.getEm().joinTransaction();
        List<EstagioProjeto> estagios = estagioProjetoRepository.findAllByProjeto(idProjeto);
        boolean estagioPP = estagio == Estagio.PROJETO_PROMISSOR && !estagios.isEmpty() && !estagios.stream().anyMatch(u -> u.getEstagioId() == Estagio.PROJETO_PROMISSOR.getId());
        boolean outrosEstagio = !estagio.getId().equals(Estagio.PROJETO_PROMISSOR.getId());
        
        if (estagioPP || outrosEstagio) {
            User usuario = this.getEm().find(User.class, usuarioId);
            this.createUpdateDeleteQuery(Projeto.Query.updateEstagioProjeto, estagio, idProjeto, usuario, new Date()).executeUpdate();
            this.saveHistoricoEstagioProjetoTransactional(estagio, idProjeto, dataInicioEstagio);
        }
    }
    
    public void updateUltimaAtividadeProjeto(Long idProjeto, AtividadeProjeto atividadeProjeto) {
        projetoRepository.updateUltimaAtividade(atividadeProjeto, idProjeto);
    }
    
    public void updateUsuarioeData(Long projetoId, User usuarioResponsavel, Date dataUltimaAlteracao) {
        projetoRepository.updateUsuarioeData(projetoId, usuarioResponsavel, dataUltimaAlteracao);
    }
    
    @Deprecated
    public void saveHistoricoEstagioProjeto(Estagio estagio, Long projetoId) {
        saveHistoricoEstagioProjeto(estagio, projetoId, new Date());
    }
    
    public void saveHistoricoEstagioProjeto(Estagio estagio, Long projetoId, Date dataInicioEstagio) {
        List<EstagioProjeto> estagios = estagioProjetoRepository.findAllByProjeto(projetoId);
        List<EstagioProjeto> estagioAtivo = estagios.stream().filter(u -> u.isAtivo()).collect(Collectors.toList());
        EstagioProjeto estagioAtual = !estagios.isEmpty() && !estagioAtivo.isEmpty() ? estagioAtivo.get(0) : null;
        
        if ((estagioAtual != null ? estagioAtual.getEstagioId() : -1) != estagio.getId()) {
            EstagioProjeto e = new EstagioProjeto();
            e.setAtivo(true);
            e.setDataInicioEstagio(dataInicioEstagio);
            e.setEstagioId(estagio.getId());
            e.setProjetoId(projetoId);
            
            estagios.stream().forEach(u -> u.setAtivo(false));
            estagios.add(e);
            estagios.stream().forEach(u -> estagioProjetoRepository.save(u));
        }
    }
    
    public void saveHistoricoEstagioProjetoTransactional(Estagio estagio, Long projetoId, Date dataInicioEstagio) {
        List<EstagioProjeto> estagios = estagioProjetoRepository.findAllByProjeto(projetoId);
        List<EstagioProjeto> estagioAtivo = estagios.stream().filter(u -> u.isAtivo()).collect(Collectors.toList());
        EstagioProjeto estagioAtual = !estagios.isEmpty() && !estagioAtivo.isEmpty() ? estagioAtivo.get(0) : null;
        
        if ((estagioAtual != null ? estagioAtual.getEstagioId() : -1) != estagio.getId()) {
            EstagioProjeto e = new EstagioProjeto();
            e.setAtivo(true);
            e.setDataInicioEstagio(dataInicioEstagio);
            e.setEstagioId(estagio.getId());
            e.setProjetoId(projetoId);
            
            estagios.stream().forEach(u -> u.setAtivo(false));
            estagios.add(e);
            estagios.stream().forEach(u -> this.getEm().merge(u));
        }
    }
    
    public List<ProjetoTO> findVersoesAnteriores(Long projetoAtualId, Long versaoPai, Long empresaId, Long unidadeEmpresaId) {
        CriteriaBuilder builder = getEm().getCriteriaBuilder();
        CriteriaQuery<ProjetoTO> query = builder.createQuery(ProjetoTO.class);
        Root<Projeto> fromProjeto = query.from(Projeto.class);
        List<Predicate> conditions = new ArrayList();
        Join<Projeto, Empresa> fromEmpresa = fromProjeto.join(Projeto_.empresa);
        Join<Projeto, UnidadeEmpresa> fromUnidadeEmpresa = fromProjeto.join(Projeto_.unidade, JoinType.LEFT);
        Join<Projeto, AtividadeProjeto> fromAtividadeProjeto = fromProjeto.join(Projeto_.ultimaAtividade, JoinType.LEFT);
        conditions.add(builder.notEqual(fromProjeto.get(Projeto_.id), projetoAtualId));
        conditions.add(builder.equal(fromProjeto.get(Projeto_.versaoPai), versaoPai));
        conditions.add(builder.equal(fromEmpresa.get(Empresa_.id), empresaId));
        
        if (unidadeEmpresaId != null) {
            conditions.add(builder.equal(fromUnidadeEmpresa.get(UnidadeEmpresa_.id), unidadeEmpresaId));
        }
        
        Join<UnidadeEmpresa, Endereco> fromEnderecoUnidade = fromUnidadeEmpresa.join(UnidadeEmpresa_.endereco, JoinType.LEFT);
        
        TypedQuery<ProjetoTO> typedQuery = getEm().createQuery(query
                .multiselect(fromProjeto.get(Projeto_.id),
                        fromProjeto.get(Projeto_.versao),
                        fromProjeto.get(Projeto_.versaoPai),
                        fromProjeto.get(Projeto_.situacaoAtual),
                        fromProjeto.get(Projeto_.estagioAtual),
                        fromProjeto.get(Projeto_.nome),
                        fromEmpresa.get(Empresa_.id),
                        fromEmpresa.get(Empresa_.nomePrincipal),
                        fromEmpresa.get(Empresa_.cnpj),
                        fromEmpresa.get(Empresa_.endereco),
                        fromUnidadeEmpresa.get(UnidadeEmpresa_.id),
                        fromUnidadeEmpresa.get(UnidadeEmpresa_.nome),
                        fromEnderecoUnidade,
                        fromAtividadeProjeto
                )
                .where(conditions.toArray(new Predicate[]{}))
                .orderBy(builder.asc(fromProjeto.get(Projeto_.versao)))
                .distinct(true)
        );
        return typedQuery.getResultList();
    }
    
    public Double findValorInvestimento(Long projetoAtualId) {
        CriteriaBuilder builder = getEm().getCriteriaBuilder();
        CriteriaQuery<InvestimentoPrevistoTO> query = builder.createQuery(InvestimentoPrevistoTO.class);
        Root<InvestimentoPrevisto> fromInvestimentoPrevisto = query.from(InvestimentoPrevisto.class);
        List<Predicate> conditions = new ArrayList();
        Join<InvestimentoPrevisto, Financeiro> fromFinanceiro = fromInvestimentoPrevisto.join(InvestimentoPrevisto_.financeiro);
        Join<Financeiro, Projeto> fromProjeto = fromFinanceiro.join(Financeiro_.projeto);
        
        conditions.add(builder.equal(fromProjeto.get(Projeto_.id), projetoAtualId));
        
        TypedQuery<InvestimentoPrevistoTO> typedQuery = getEm().createQuery(query
                .multiselect(builder.sum(fromInvestimentoPrevisto.get(InvestimentoPrevisto_.valor)))
                .where(conditions.toArray(new Predicate[]{}))
        );
        
        return typedQuery.getSingleResult().getValor();
    }
    
    public List<ProjetoTO> findProjetosRecentesUsuario(Long idUsuario) throws SystemException {
        
        TypedQuery<ProjetoTO> createTypedQuery = createTypedQuery(ProjetoTO.class, ProjetoRecente.Query.findProjetoRecenteTOByUsuario, idUsuario);
        deleteProjetosRecentes(idUsuario);
        
        return createTypedQuery.setMaxResults(10).getResultList();
        
    }
    
    public void deleteProjetosRecentes(Long idUsuario) throws SystemException {
        try {
            userTransaction.begin();
            this.getEm().joinTransaction();
            TypedQuery<ProjetoRecente> createTypedQuery = createTypedQuery(ProjetoRecente.class, ProjetoRecente.Query.findProjetoRecenteByUsuario, idUsuario);
            List<ProjetoRecente> projects = createTypedQuery.getResultList();
            if (projects != null && !projects.isEmpty() && projects.size() > 25) {
                List<ProjetoRecente> projectsToDelete = projects.subList(25, projects.size());
                projectsToDelete.forEach(u -> this.remove(u));
            }
            userTransaction.commit();
        } catch (Exception ex) {
            userTransaction.rollback();
            Logger.getLogger(ProjetoService.class.getName()).log(Level.SEVERE, null, ex);
            throw new RuntimeException(ex);
        }
    }
    
    public Double findValorFaturamentoInicial(Long projetoAtualId) {
        List<FaturamentoPrevistoTO> faturamentos = findValoresFaturamentos(projetoAtualId);
        if (faturamentos != null && !faturamentos.isEmpty()) {
            return faturamentos.get(0).getValor();
        }
        return null;
    }
    
    public Double findValorFaturamentoFinal(Long projetoAtualId) {
        List<FaturamentoPrevistoTO> faturamentos = findValoresFaturamentos(projetoAtualId);
        if (faturamentos != null && !faturamentos.isEmpty()) {
            return faturamentos.get(faturamentos.size() - 1).getValor();
        }
        return null;
    }
    
    public List<FaturamentoPrevistoTO> findValoresFaturamentos(Long projetoAtualId) {
        CriteriaBuilder builder = getEm().getCriteriaBuilder();
        CriteriaQuery<FaturamentoPrevistoTO> query = builder.createQuery(FaturamentoPrevistoTO.class);
        Root<FaturamentoPrevisto> fromFaturamentoPrevisto = query.from(FaturamentoPrevisto.class);
        List<Predicate> conditions = new ArrayList();
        Join<FaturamentoPrevisto, Financeiro> fromFinanceiro = fromFaturamentoPrevisto.join(FaturamentoPrevisto_.financeiro);
        Join<Financeiro, Projeto> fromProjeto = fromFinanceiro.join(Financeiro_.projeto);
        
        conditions.add(builder.equal(fromProjeto.get(Projeto_.id), projetoAtualId));
        
        TypedQuery<FaturamentoPrevistoTO> typedQuery = getEm().createQuery(query
                .multiselect(fromFaturamentoPrevisto.get(FaturamentoPrevisto_.valor))
                .where(conditions.toArray(new Predicate[]{}))
                .orderBy(builder.asc(fromFaturamentoPrevisto.get(FaturamentoPrevisto_.ano)))
        );
        
        return typedQuery.getResultList();
    }
    
    public EmpregoTO findEmpregos(Long projetoAtualId) {
        CriteriaBuilder builder = getEm().getCriteriaBuilder();
        CriteriaQuery<EmpregoTO> query = builder.createQuery(EmpregoTO.class);
        Root<Emprego> fromEmprego = query.from(Emprego.class);
        List<Predicate> conditions = new ArrayList();
        Join<Emprego, Projeto> fromProjeto = fromEmprego.join(Emprego_.projeto);
        
        conditions.add(builder.equal(fromProjeto.get(Projeto_.id), projetoAtualId));
        
        TypedQuery<EmpregoTO> typedQuery = getEm().createQuery(query
                .multiselect(builder.sum(fromEmprego.get(Emprego_.direto)),
                        builder.sum(fromEmprego.get(Emprego_.indireto)))
                .where(conditions.toArray(new Predicate[]{}))
        );
        
        return typedQuery.getSingleResult();
    }
    
    public List<InstrumentoFormalizacaoTO> findDatasInstrumentoFormalizacao(Long projetoAtualId) {
        CriteriaBuilder builder = getEm().getCriteriaBuilder();
        CriteriaQuery<InstrumentoFormalizacaoTO> query = builder.createQuery(InstrumentoFormalizacaoTO.class);
        Root<InstrumentoFormalizacao> fromInstr = query.from(InstrumentoFormalizacao.class);
        List<Predicate> conditions = new ArrayList();
        Join<InstrumentoFormalizacao, Projeto> fromProjeto = fromInstr.join(InstrumentoFormalizacao_.projetos);
        
        if (projetoAtualId != null) {
            conditions.add(builder.equal(fromProjeto.get(Projeto_.id), projetoAtualId));
        }
        
        TypedQuery<InstrumentoFormalizacaoTO> typedQuery = getEm().createQuery(query
                .multiselect(fromInstr.get(InstrumentoFormalizacao_.dataAssinaturaPrevista))
                .where(conditions.toArray(new Predicate[]{}))
                .orderBy(builder.asc(fromInstr.get(InstrumentoFormalizacao_.dataUltimaAlteracao)))
        );
        
        return typedQuery.getResultList();
    }
    
    public EstagioProjetoTO findDataEstagio(Long projetoAtualId, long idEstagio) {
        CriteriaBuilder builder = getEm().getCriteriaBuilder();
        CriteriaQuery<EstagioProjetoTO> query = builder.createQuery(EstagioProjetoTO.class);
        Root<Projeto> fromProjeto = query.from(Projeto.class);
        List<Predicate> conditions = new ArrayList();
        Join<Projeto, EstagioProjeto> fromEstagioProjeto = fromProjeto.join(Projeto_.estagios, JoinType.LEFT);
        
        conditions.add(builder.equal(fromEstagioProjeto.get(EstagioProjeto_.estagioId), idEstagio));
        
        TypedQuery<EstagioProjetoTO> typedQuery = getEm().createQuery(query
                .multiselect(fromEstagioProjeto.get(EstagioProjeto_.dataInicioEstagio))
                .where(conditions.toArray(new Predicate[]{}))
                .orderBy(builder.desc(fromEstagioProjeto.get(EstagioProjeto_.dataInicioEstagio)))
        );
        
        EstagioProjetoTO retorno;
        try {
            retorno = typedQuery.setMaxResults(1).getSingleResult();
        } catch (NoResultException e) {
            retorno = new EstagioProjetoTO();
        }
        return retorno;
    }
    
    public Long countFindByFilters(QueryParameter filters) {
        CriteriaBuilder builder = getEm().getCriteriaBuilder();
        CriteriaQuery<Long> initCq = builder.createQuery(Long.class);
        Root<Projeto> fromProjeto = initCq.from(Projeto.class);
        Join<Projeto, Empresa> fromEmpresa = fromProjeto.join(Projeto_.empresa);
        Join<Projeto, UnidadeEmpresa> fromUnidadeEmpresa = fromProjeto.join(Projeto_.unidade, JoinType.LEFT);
        Join<Projeto, SituacaoProjeto> fromSituacaoProjeto = fromProjeto.join(Projeto_.situacaoAtual, JoinType.LEFT);
        
        List<Predicate> filterConditions = buildFilterConditions(builder, fromProjeto, fromEmpresa, fromUnidadeEmpresa, fromSituacaoProjeto, filters);
        
        CriteriaQuery<Long> query = initCq.select(builder.countDistinct(fromProjeto)).where(filterConditions.toArray(new Predicate[]{}));
        return getEm().createQuery(query).getSingleResult();
    }
    
    private List<Predicate> buildFilterConditions(CriteriaBuilder builder, Root<Projeto> fromProjeto, Join<Projeto, Empresa> fromEmpresa,
            Join<Projeto, UnidadeEmpresa> fromUnidadeEmpresa, Join<Projeto, SituacaoProjeto> fromSituacaoProjeto, QueryParameter filters) {
        List<Predicate> conditions = new ArrayList<>();
        
        if (filters.exists(Projeto.Filters.NOME_EMPRESA)) {
            String nomeEmpresa = filters.getString(Projeto.Filters.NOME_EMPRESA);
            Join<Empresa, NomeEmpresa> fromNomesEmpresa = fromEmpresa.join(Empresa_.nomesEmpresa, JoinType.LEFT);
            conditions.add(builder.or(
                    builder.like(builder.lower(fromEmpresa.get(Empresa_.nomePrincipal)), "%" + nomeEmpresa.toLowerCase() + "%"),
                    builder.like(builder.lower(fromNomesEmpresa.get(NomeEmpresa_.nome)), "%" + nomeEmpresa.toLowerCase() + "%"),
                    builder.like(builder.lower(fromUnidadeEmpresa.get(UnidadeEmpresa_.nome)), "%" + nomeEmpresa.toLowerCase() + "%")
            ));
        }
        if (filters.exists(Projeto.Filters.SITUACAO_EMPRESA)) {
            conditions.add(builder.equal(fromEmpresa.get(Empresa_.situacao), filters.get(Projeto.Filters.SITUACAO_EMPRESA)));
        }
        if (filters.exists(Projeto.Filters.CNPJ)) {
            conditions.add(builder.equal(fromEmpresa.get(Empresa_.cnpj), filters.get(Projeto.Filters.CNPJ)));
        }
        if (filters.exists(Projeto.Filters.NOME_PROJETO)) {
            conditions.add(builder.like(builder.lower(fromProjeto.get(Projeto_.nome)), "%" + filters.getString(Projeto.Filters.NOME_PROJETO).toLowerCase() + "%"));
        }
        if (filters.exists(Projeto.Filters.ESTAGIO)) {
            conditions.add(builder.equal(fromProjeto.get(Projeto_.estagioAtual), filters.get(Projeto.Filters.ESTAGIO)));
        }
        if (filters.exists(Projeto.Filters.CADEIA_PRODUTIVA)) {
            Join<Projeto, CadeiaProdutiva> fromCadeiaProdutiva = fromProjeto.join(Projeto_.cadeiaProdutiva);
            conditions.add(builder.equal(fromCadeiaProdutiva.get(CadeiaProdutiva_.id), filters.get(Projeto.Filters.CADEIA_PRODUTIVA)));
        }
        if (filters.exists(Projeto.Filters.SITUACAO_PROJETO)) {
            conditions.add(builder.equal(fromSituacaoProjeto.get(SituacaoProjeto_.situacao), filters.get(Projeto.Filters.SITUACAO_PROJETO)));
        }
        if (filters.exists(Projeto.Filters.DEPARTAMENTOS)) {
            Join<Projeto, UsuarioInterno> fromUsuarioResponsavel = fromProjeto.join(Projeto_.usuarioInvestimento);
            Join<UsuarioInterno, Departamento> fromDepartamento = fromUsuarioResponsavel.join(UsuarioInterno_.departamento);
            conditions.add(fromDepartamento.get(Departamento_.id).in(filters.get(Projeto.Filters.DEPARTAMENTOS)));
            if (filters.exists(Projeto.Filters.USUARIO_RESPONSAVEL_PROJETO)) {
                conditions.add(fromUsuarioResponsavel.get(UsuarioInterno_.id).in(filters.get(Projeto.Filters.USUARIO_RESPONSAVEL_PROJETO)));
            }
        } else {
            if (filters.exists(Projeto.Filters.USUARIO_RESPONSAVEL_PROJETO)) {
                Join<Projeto, UsuarioInterno> fromUsuarioResponsavel = fromProjeto.join(Projeto_.usuarioInvestimento);
                conditions.add(fromUsuarioResponsavel.get(UsuarioInterno_.id).in(filters.get(Projeto.Filters.USUARIO_RESPONSAVEL_PROJETO)));
            }
        }
        if (filters.exists(Projeto.Filters.EMPRESA_ID)) {
            conditions.add(builder.equal(fromEmpresa.get(Empresa_.id), filters.get(Projeto.Filters.EMPRESA_ID)));
        }
        if (filters.exists(Projeto.Filters.UNIDADE_ID)) {
            conditions.add(builder.equal(fromUnidadeEmpresa.get(UnidadeEmpresa_.id), filters.get(Projeto.Filters.UNIDADE_ID)));
        }
        
        conditions.add(builder.equal(fromProjeto.get(Projeto_.ultimaVersao), true));
        
        return conditions;
    }
    
    public List<ProjetoTO> findByFilters(QueryParameter filters) {
        
        CriteriaBuilder builder = getEm().getCriteriaBuilder();
        CriteriaQuery<ProjetoTO> query = builder.createQuery(ProjetoTO.class);
        Root<Projeto> fromProjeto = query.from(Projeto.class);
        Join<Projeto, Empresa> fromEmpresa = fromProjeto.join(Projeto_.empresa);
        Join<Projeto, UnidadeEmpresa> fromUnidadeEmpresa = fromProjeto.join(Projeto_.unidade, JoinType.LEFT);
        Join<Projeto, SituacaoProjeto> fromSituacaoProjeto = fromProjeto.join(Projeto_.situacaoAtual, JoinType.LEFT);
        Join<Projeto, AtividadeProjeto> fromAtividadeProjeto = fromProjeto.join(Projeto_.ultimaAtividade, JoinType.LEFT);
        Join<UnidadeEmpresa, Endereco> fromEnderecoUnidade = fromUnidadeEmpresa.join(UnidadeEmpresa_.endereco, JoinType.LEFT);
        
        List<Predicate> filterConditions = buildFilterConditions(builder, fromProjeto, fromEmpresa, fromUnidadeEmpresa, fromSituacaoProjeto, filters);
        
        TypedQuery<ProjetoTO> typedQuery = getEm().createQuery(query
                .multiselect(fromProjeto.get(Projeto_.id),
                        fromProjeto.get(Projeto_.versao),
                        fromProjeto.get(Projeto_.versaoPai),
                        fromSituacaoProjeto,
                        fromProjeto.get(Projeto_.estagioAtual),
                        fromProjeto.get(Projeto_.nome),
                        fromEmpresa.get(Empresa_.id),
                        fromEmpresa.get(Empresa_.nomePrincipal),
                        fromEmpresa.get(Empresa_.cnpj),
                        fromEmpresa.get(Empresa_.endereco),
                        fromUnidadeEmpresa.get(UnidadeEmpresa_.id),
                        fromUnidadeEmpresa.get(UnidadeEmpresa_.nome),
                        fromEnderecoUnidade,
                        fromEmpresa.get(Empresa_.situacao),
                        fromProjeto.get(Projeto_.usuarioInvestimento),
                        fromAtividadeProjeto)
                .where(filterConditions.toArray(new Predicate[]{}))
                .orderBy(builder.asc(fromProjeto.get(Projeto_.id)))
                .distinct(true)
        );
        if (filters.hasPagination()) {
            typedQuery.setMaxResults(filters.getPagination().getPaginationSize());
            typedQuery.setFirstResult(filters.getPagination().getFirstResult());
        }
        
        List<ProjetoTO> resultList = typedQuery.getResultList();
        
        for (ProjetoTO projeto : resultList) {
            List<ProjetoTO> versoesAnteriores = findVersoesAnteriores(projeto.getId(), projeto.getVersaoPai(), projeto.getIdEmpresa(), projeto.getIdUnidade());
            if (versoesAnteriores != null && !versoesAnteriores.isEmpty()) {
                projeto.setVersoes(versoesAnteriores);
            }
        }
        
        return resultList;
    }
    
    public List<ProjetoTO> findByEmpresas(List<Long> ids) {
        CriteriaBuilder builder = getEm().getCriteriaBuilder();
        CriteriaQuery<ProjetoTO> query = builder.createQuery(ProjetoTO.class);
        Root<Projeto> fromProjeto = query.from(Projeto.class);
        List<Predicate> conditions = new ArrayList();
        Join<Projeto, Empresa> fromEmpresa = fromProjeto.join(Projeto_.empresa);
        
        conditions.add(fromEmpresa.get(Empresa_.id).in(ids));
        
        TypedQuery<ProjetoTO> typedQuery = getEm().createQuery(query
                .multiselect(fromProjeto.get(Projeto_.id),
                        fromProjeto.get(Projeto_.nome))
                .where(conditions.toArray(new Predicate[]{}))
                .orderBy(builder.asc(fromProjeto.get(Projeto_.id)))
                .distinct(true)
        );
        
        return typedQuery.getResultList();
    }
    
    public List<Projeto> findByIdToReport(List<Long> ids_Projetos) {
        StringBuilder builder = new StringBuilder("SELECT p FROM Projeto p ");
        builder.append(" left join fetch p.empresa e");
        builder.append(" WHERE p.id in (:id_projetos) ");
        
        TypedQuery<Projeto> query = getEm().createQuery(builder.toString(), Projeto.class);
        query.setParameter("id_projetos", ids_Projetos);
        return query.getResultList();
    }
    
    public List<ProjetoTO> findByUnidadesEmpresa(List<Long> ids) {
        CriteriaBuilder builder = getEm().getCriteriaBuilder();
        CriteriaQuery<ProjetoTO> query = builder.createQuery(ProjetoTO.class);
        Root<Projeto> fromProjeto = query.from(Projeto.class);
        List<Predicate> conditions = new ArrayList();
        Join<Projeto, UnidadeEmpresa> fromUnidadeEmpresa = fromProjeto.join(Projeto_.unidade);
        
        conditions.add(fromUnidadeEmpresa.get(UnidadeEmpresa_.id).in(ids));
        
        TypedQuery<ProjetoTO> typedQuery = getEm().createQuery(query
                .multiselect(fromProjeto.get(Projeto_.id),
                        fromProjeto.get(Projeto_.nome))
                .where(conditions.toArray(new Predicate[]{}))
                .orderBy(builder.asc(fromProjeto.get(Projeto_.id)))
                .distinct(true)
        );
        return typedQuery.getResultList();
    }
    
    public ProjetoTO findProjetoTOById(Long id) {
        CriteriaBuilder builder = getEm().getCriteriaBuilder();
        CriteriaQuery<ProjetoTO> query = builder.createQuery(ProjetoTO.class);
        Root<Projeto> fromProjeto = query.from(Projeto.class);
        List<Predicate> conditions = new ArrayList();
        Join<Projeto, Empresa> fromEmpresa = fromProjeto.join(Projeto_.empresa);
        Join<Empresa, UsuarioExterno> fromUsuarioExterno = fromEmpresa.join(Empresa_.responsavel, JoinType.LEFT);
        Join<Projeto, UsuarioInterno> fromUsuarioInterno = fromProjeto.join(Projeto_.usuarioInvestimento, JoinType.LEFT);
        Join<UsuarioInterno, Departamento> fromDepartamento = fromUsuarioInterno.join(UsuarioInterno_.departamento, JoinType.LEFT);
        conditions.add(builder.equal(fromProjeto.get(Projeto_.id), id));
        
        TypedQuery<ProjetoTO> typedQuery = getEm().createQuery(query
                .multiselect(fromProjeto.get(Projeto_.id),
                        fromProjeto.get(Projeto_.versao),
                        fromProjeto.get(Projeto_.versaoPai),
                        fromProjeto.get(Projeto_.situacaoAtual),
                        fromProjeto.get(Projeto_.estagioAtual),
                        fromProjeto.get(Projeto_.nome),
                        fromEmpresa.get(Empresa_.id),
                        fromEmpresa.get(Empresa_.nomePrincipal),
                        fromEmpresa.get(Empresa_.situacao),                        
                        fromProjeto.get(Projeto_.cadeiaProdutiva),
                        fromProjeto.get(Projeto_.dataUltimaAlteracao),
                        fromProjeto.get(Projeto_.usuarioResponsavel),
                        fromProjeto.get(Projeto_.usuarioInvestimento),
                        fromUsuarioExterno.get(UsuarioExterno_.id),
                        fromDepartamento.get(Departamento_.id)
                )
                .where(conditions.toArray(new Predicate[]{}))
                .distinct(true)
        );
        ProjetoTO projeto = typedQuery.getSingleResult();
        projeto.setSituacoesProjeto(this.createQuery(SituacaoProjeto.Query.findAllByProjeto, projeto.getId()).getResultList());
        
        return projeto;
    }
    
    public void cloneProject(Projeto projectToClone) {
        try {
            Projeto cloned = new Projeto();
            BeanUtilsBean.getInstance().getConvertUtils().register(false, false, 0);
            BeanUtils.copyProperties(cloned, projectToClone);
            nullifyId(cloned);
            cloned.setUltimaVersao(Boolean.TRUE);
            cloned.setProspeccaoAtiva(projectToClone.getProspeccaoAtiva());
            cloned.setVersao(projectToClone.getVersao() + 1);
            cloned.setInstrumentos(null);
            
            cloned.setAnexos(null);
            cloned.setEstagios(null);
            cloned.setAtividades(null);
            cloned.setCronogramas(null);
            cloned.setEmpregos(null);
            cloned.setInstrumentos(null);
            cloned.setSituacoesProjeto(null);
            cloned.setSituacaoAtual(null);
            cloned.setRelatoriosAcompanhamento(new ArrayList<>());
            
            if (cloned.getLocalizacao() != null) {
                cloned.setLocalizacao(new Localizacao());
                BeanUtils.copyProperties(cloned.getLocalizacao(), projectToClone.getLocalizacao());
                nullifyId(cloned.getLocalizacao());
                cloned.getLocalizacao().setProjeto(cloned);
                cloned.getLocalizacao().setAnexosLocalizacao(null);
            }
            
            if (cloned.getPleito() != null) {
                cloned.setPleito(new Pleito());
                BeanUtils.copyProperties(cloned.getPleito(), projectToClone.getPleito());
                nullifyId(cloned.getPleito());
                cloned.getPleito().setProjeto(cloned);
            }
            
            if (cloned.getInfraestrutura() != null) {
                cloned.setInfraestrutura(new Infraestrutura());
                BeanUtils.copyProperties(cloned.getInfraestrutura(), projectToClone.getInfraestrutura());
                nullifyId(cloned.getInfraestrutura());
                cloned.getInfraestrutura().setProjeto(cloned);
                
                cloned.getInfraestrutura().setEnergiaContratadas(null);
            }
            
            if (cloned.getFinanceiro() != null) {
                cloned.setFinanceiro(new Financeiro());
                BeanUtils.copyProperties(cloned.getFinanceiro(), projectToClone.getFinanceiro());
                nullifyId(cloned.getFinanceiro());
                cloned.getFinanceiro().setProjeto(cloned);
                
                cloned.getFinanceiro().setAnexos(null);
                cloned.getFinanceiro().setFaturamentoAnteriorList(null);
                cloned.getFinanceiro().setFaturamentoPrevistoList(null);
                cloned.getFinanceiro().setInvestimentoPrevistoList(null);
                cloned.getFinanceiro().setOrigemRecursoList(null);
                cloned.getFinanceiro().setUsoFonteList(null);
            }
            
            if (cloned.getInsumoProduto() != null) {
                cloned.setInsumoProduto(new InsumoProduto());
                BeanUtils.copyProperties(cloned.getInsumoProduto(), projectToClone.getInsumoProduto());
                nullifyId(cloned.getInsumoProduto());
                cloned.getInsumoProduto().setProjeto(cloned);
                
                cloned.getInsumoProduto().setConcorrentes(null);
                cloned.getInsumoProduto().setInsumos(null);
                cloned.getInsumoProduto().setParcerias(null);
                cloned.getInsumoProduto().setProducoes(null);
                cloned.getInsumoProduto().setProdutos(null);
                cloned.getInsumoProduto().setProdutosAdqEComPorMinas(null);
                cloned.getInsumoProduto().setProdutosAdqOutrosEstadosParaCom(null);
                cloned.getInsumoProduto().setProdutosFabEComPorMinas(null);
                cloned.getInsumoProduto().setProdutosImpParaCom(null);
                cloned.getInsumoProduto().setServicos(null);
            }
            
            if (cloned.getMeioAmbiente() != null) {
                cloned.setMeioAmbiente(new MeioAmbiente());
                BeanUtils.copyProperties(cloned.getMeioAmbiente(), projectToClone.getMeioAmbiente());
                nullifyId(cloned.getMeioAmbiente());
                cloned.getMeioAmbiente().setProjeto(cloned);
                
                cloned.getMeioAmbiente().setAnexos(null);
                cloned.getMeioAmbiente().setEtapasMeioAmbiente(null);
            }            
            
            if (!cloned.getRelatoriosAcompanhamento().isEmpty()) {
                
                List<RelatorioAcompanhamento> reportsToClone = projectToClone.getRelatoriosAcompanhamento();
                for (RelatorioAcompanhamento r : reportsToClone) {
                    getEm().detach(r);           
                    
                    r.setId(null);
                    r.setProjeto(cloned);

                    Set<Anexo> anexos =  r.getAnexos();                 
                    List<RelatorioAcompanhamentoSituacao> situacoes = r.getSituacao();
                    
                    r.setSituacao(null);
                    r.setAnexos(null);
                    
                    cloned.getRelatoriosAcompanhamento().add(r);
                                        
                    cloned = (Projeto) merge(cloned);
                    
                    situacoes.stream().forEach(it-> {it.setRelatorioAcompanhamento(r);});
                    r.setSituacao(situacoes);  
                    
                    r.setAnexos(anexos);  

                    cloned.getRelatoriosAcompanhamento().add(r);
                    
                    cloned = (Projeto) merge(cloned);                    
                }                
            }
            
            
            cloned = (Projeto) merge(cloned);
            
            this.getEm().refresh(cloned);
            this.getEm().refresh(projectToClone);
            HibernateProxyHelper.getClassWithoutInitializingProxy(projectToClone);

            // initializing lazy properties
            cloned.getEstagios();
            if (cloned.getLocalizacao() != null) {
                cloned.getLocalizacao().getLatitude();
            }
            if (cloned.getFinanceiro() != null) {
                cloned.getFinanceiro().getObservacao();
            }
            if (cloned.getInsumoProduto() != null) {
                cloned.getInsumoProduto().getPercentualClienteIndustria();
            }
            if (cloned.getPleito() != null) {
                cloned.getPleito().getObservacoes();
            }
            if (cloned.getInfraestrutura() != null) {
                cloned.getInfraestrutura().getAreaAlagada();
            }

            // initializing lazy properties
            projectToClone.getEstagios();
            if (projectToClone.getLocalizacao() != null) {
                projectToClone.getLocalizacao().getLatitude();
            }
            if (projectToClone.getFinanceiro() != null) {
                projectToClone.getFinanceiro().getObservacao();
            }
            if (projectToClone.getInsumoProduto() != null) {
                projectToClone.getInsumoProduto().getPercentualClienteIndustria();
            }
            if (projectToClone.getPleito() != null) {
                projectToClone.getPleito().getObservacoes();
            }
            if (projectToClone.getInfraestrutura() != null) {
                projectToClone.getInfraestrutura().getAreaAlagada();
            }
            
            cloneProjectDependencies(cloned, projectToClone);
        } catch (NotSupportedException | IllegalAccessException | SystemException | InvocationTargetException ex) {
            Logger.getLogger(ProjetoService.class.getName()).log(Level.SEVERE, null, ex);
            throw new RuntimeException(ex);
        }
    }
    
    public void cloneProjectDependencies(Projeto projectCloned, Projeto projectToClone) throws SystemException, NotSupportedException {
        this.getEm().joinTransaction();
        StringBuffer stb = new StringBuffer();
        
        stb.append("insert into AnexoProjeto ");
        stb.append("(anexo_id,projeto_id) select anexo_id,  ");
        stb.append(projectCloned.getId());
        stb.append(" as projeto_id from AnexoProjeto anexo where projeto_id =  ");
        stb.append(projectToClone.getId());
        
        this.getEm().createNativeQuery(stb.toString()).executeUpdate();
        
        stb = new StringBuffer();
        stb.append("insert into EstagioProjeto ");
        stb.append("(projeto_id, estagio_id, dataInicioEstagio, ativo) select  ");
        stb.append(projectCloned.getId());
        stb.append(" as projeto_id, est.estagio_id, est.dataInicioEstagio, est.ativo ");
        stb.append("from EstagioProjeto est where est.projeto_id =  ");
        stb.append(projectToClone.getId());
        
        this.getEm().createNativeQuery(stb.toString()).executeUpdate();
        
        stb = new StringBuffer();
        stb.append("insert into HistoricoSituacaoProjeto ");
        stb.append("(projeto_id, situacaoProjeto_id, data, justificativa) select   ");
        stb.append(projectCloned.getId());
        stb.append(" as projeto_id, sit.situacaoProjeto_id, sit.data, sit.justificativa ");
        stb.append("from HistoricoSituacaoProjeto sit where sit.projeto_id =  ");
        stb.append(projectToClone.getId());
        
        this.getEm().createNativeQuery(stb.toString()).executeUpdate();
        
        stb = new StringBuffer();
        stb.append("UPDATE Projeto SET ");
        stb.append("historicoSituacaoProjeto_id = (select max(id) as id from HistoricoSituacaoProjeto where projeto_id = ");
        stb.append(projectCloned.getId());
        stb.append(" ) WHERE Projeto.id =  ");
        stb.append(projectCloned.getId());
        
        this.getEm().createNativeQuery(stb.toString()).executeUpdate();
        
        stb = new StringBuffer();
        stb.append("insert into CRONOGRAMA ");
        stb.append("(projeto_id,inicioProjeto,inicioProjetoExecutivo,inicioContratacaoEquipamentos,inicioImplantacao,inicioImplantacaoAgricola");
        stb.append(",inicioImplantacaoIndustrial,terminoImplantacaoAgricola,terminoImplantacaoIndustrial,inicioOperacao");
        stb.append(",inicioOperacaoSucroenergetico,terminoProjetoExecutivo,terminoProjeto,usuarioResponsavel_id,dataUltimaAlteracao) SELECT   ");
        stb.append(projectCloned.getId());
        stb.append(" as projeto_id,inicioProjeto, ");
        stb.append("inicioProjetoExecutivo,inicioContratacaoEquipamentos,inicioImplantacao, ");
        stb.append("inicioImplantacaoAgricola,inicioImplantacaoIndustrial,terminoImplantacaoAgricola,terminoImplantacaoIndustrial, ");
        stb.append("inicioOperacao,inicioOperacaoSucroenergetico,terminoProjetoExecutivo,terminoProjeto, ");
        stb.append("usuarioResponsavel_id,dataUltimaAlteracao FROM Cronograma where projeto_id =  ");
        stb.append(projectToClone.getId());
        
        this.getEm().createNativeQuery(stb.toString()).executeUpdate();
        
        stb = new StringBuffer();
        stb.append("INSERT INTO ATIVIDADEPROJETO ");
        stb.append("(projeto_id,atividadeLocal_id,posicao,anexo_id,dataConclusao,observacao,obrigatorio,bloqueiaICE,df,ii,oi,cc,usuarioResponsavel_id,dataUltimaAlteracao) SELECT   ");
        stb.append(projectCloned.getId());
        stb.append(" as projeto_id,atividadeLocal_id,posicao,anexo_id,dataConclusao,observacao,obrigatorio,bloqueiaICE,df,ii,oi,cc,usuarioResponsavel_id,dataUltimaAlteracao  ");
        stb.append("FROM ATIVIDADEPROJETO where projeto_id = ");
        stb.append(projectToClone.getId());
        
        this.getEm().createNativeQuery(stb.toString()).executeUpdate();
        
        stb = new StringBuffer();
        stb.append("update projeto set ultimaAtividade_id = (select top 1 id from atividadeprojeto ");
        stb.append(" where projeto_id = ");
        stb.append(projectCloned.getId());
        stb.append(" order by dataconclusao desc, posicao desc) ");
        stb.append(" where id = ");
        stb.append(projectCloned.getId());
        
        this.getEm().createNativeQuery(stb.toString()).executeUpdate();
        
        if (projectCloned.getLocalizacao() != null) {
            stb = new StringBuffer();
            stb.append("INSERT INTO ANEXOLOCALIZACAO ");
            stb.append("(anexo_id, localizacao_id, anexoarealocal,tipolocalizacao) SELECT   ");
            stb.append("anexo_id, ");
            stb.append(projectCloned.getLocalizacao().getId());
            stb.append(" as localizacao_id, anexoarealocal,tipolocalizacao ");
            stb.append("FROM ANEXOLOCALIZACAO WHERE localizacao_id = ");
            stb.append(projectToClone.getLocalizacao().getId());
            
            this.getEm().createNativeQuery(stb.toString()).executeUpdate();
        }
        
        stb = new StringBuffer();
        stb.append("INSERT INTO EMPREGO ");
        stb.append("(projeto_id,ano,direto,indireto,tipo,usuarioResponsavel_id,dataUltimaAlteracao) SELECT   ");
        stb.append(projectCloned.getId());
        stb.append(" as projeto_id,ano,direto,indireto,tipo,usuarioResponsavel_id,dataUltimaAlteracao FROM EMPREGO where projeto_id =  ");
        stb.append(projectToClone.getId());
        
        this.getEm().createNativeQuery(stb.toString()).executeUpdate();
        
        if (projectCloned.getFinanceiro() != null) {
            stb = new StringBuffer();
            stb.append("INSERT INTO FINANCEIROANEXO ");
            stb.append("(financeiro_id,anexo_id) SELECT   ");
            stb.append(projectCloned.getFinanceiro().getId());
            stb.append(" as financeiro_id,anexo_id FROM FINANCEIROANEXO where financeiro_id =  ");
            stb.append(projectToClone.getFinanceiro().getId());
            
            this.getEm().createNativeQuery(stb.toString()).executeUpdate();
            
            stb = new StringBuffer();
            stb.append("INSERT INTO FATURAMENTOANTERIOR ");
            stb.append("(financeiro_id,tipo,ano,valor) SELECT   ");
            stb.append(projectCloned.getFinanceiro().getId());
            stb.append(" as financeiro_id,tipo,ano,valor FROM FATURAMENTOANTERIOR WHERE financeiro_id =  ");
            stb.append(projectToClone.getFinanceiro().getId());
            
            this.getEm().createNativeQuery(stb.toString()).executeUpdate();
            stb = new StringBuffer();
            stb.append("INSERT INTO FATURAMENTOPREVISTO ");
            stb.append("(financeiro_id,tipo,ano,valor) SELECT   ");
            stb.append(projectCloned.getFinanceiro().getId());
            stb.append(" as financeiro_id,tipo,ano,valor FROM FATURAMENTOPREVISTO where financeiro_id =  ");
            stb.append(projectToClone.getFinanceiro().getId());
            
            this.getEm().createNativeQuery(stb.toString()).executeUpdate();
            
            stb = new StringBuffer();
            stb.append("INSERT INTO INVESTIMENTOPREVISTO ");
            stb.append("(financeiro_id,tipo,ano,valor) SELECT   ");
            stb.append(projectCloned.getFinanceiro().getId());
            stb.append(" as financeiro_id,tipo,ano,valor FROM INVESTIMENTOPREVISTO where financeiro_id =  ");
            stb.append(projectToClone.getFinanceiro().getId());
            
            this.getEm().createNativeQuery(stb.toString()).executeUpdate();
            
            stb = new StringBuffer();
            stb.append("INSERT INTO ORIGEMRECURSO ");
            stb.append("(financeiro_id,origemNacional,uf_id,pais_id,percentualInvestimento,valorInvestimento) SELECT   ");
            stb.append(projectCloned.getFinanceiro().getId());
            stb.append(" as financeiro_id,origemNacional,uf_id,pais_id,percentualInvestimento,valorInvestimento FROM ORIGEMRECURSO where financeiro_id =  ");
            stb.append(projectToClone.getFinanceiro().getId());
            
            this.getEm().createNativeQuery(stb.toString()).executeUpdate();
            
            stb = new StringBuffer();
            stb.append("INSERT INTO USOFONTE ");
            stb.append("(financeiro_id,tipo,valorRealizado,valorARealizar) SELECT   ");
            stb.append(projectCloned.getFinanceiro().getId());
            stb.append(" as financeiro_id,tipo,valorRealizado,valorARealizar FROM USOFONTE where financeiro_id =  ");
            stb.append(projectToClone.getFinanceiro().getId());
            
            this.getEm().createNativeQuery(stb.toString()).executeUpdate();
        }
        
        if (projectCloned.getInsumoProduto() != null) {
            stb = new StringBuffer();
            stb.append("INSERT INTO SERVICO ");
            stb.append("(insumoProduto_id,descricao) SELECT   ");
            stb.append(projectCloned.getInsumoProduto().getId());
            stb.append(" as insumoProduto_id,descricao FROM SERVICO WHERE insumoProduto_id = ");
            stb.append(projectToClone.getInsumoProduto().getId());
            
            this.getEm().createNativeQuery(stb.toString()).executeUpdate();
            
            stb = new StringBuffer();
            stb.append("INSERT INTO CONCORRENTE ");
            stb.append("(insumoProduto_id,nome,descricaoProduto) SELECT   ");
            stb.append(projectCloned.getInsumoProduto().getId());
            stb.append(" as insumoProduto_id,nome,descricaoProduto FROM Concorrente WHERE insumoProduto_id = ");
            stb.append(projectToClone.getInsumoProduto().getId());
            
            this.getEm().createNativeQuery(stb.toString()).executeUpdate();
            
            stb = new StringBuffer();
            stb.append("INSERT INTO PARCERIA ");
            stb.append("(insumoProduto_id,descricao) SELECT   ");
            stb.append(projectCloned.getInsumoProduto().getId());
            stb.append(" as insumoProduto_id,descricao FROM PARCERIA WHERE insumoProduto_id = ");
            stb.append(projectToClone.getInsumoProduto().getId());
            
            this.getEm().createNativeQuery(stb.toString()).executeUpdate();
            
            stb = new StringBuffer();
            stb.append("INSERT INTO INSUMO ");
            stb.append("(insumoProduto_id,nome,ncm_id,origem,quantidadeAno,unidadeMedida,valorEstimadoAno,empresaFornecedor) SELECT   ");
            stb.append(projectCloned.getInsumoProduto().getId());
            stb.append(" as insumoProduto_id,nome,ncm_id,origem,quantidadeAno,unidadeMedida, ");
            stb.append("valorEstimadoAno,empresaFornecedor FROM INSUMO WHERE insumoProduto_id =  ");
            stb.append(projectToClone.getInsumoProduto().getId());
            
            this.getEm().createNativeQuery(stb.toString()).executeUpdate();
            
            stb = new StringBuffer();
            stb.append("INSERT INTO PRODUCAO ");
            stb.append("(insumoProduto_id,ano,areaPlantada,moagemCana,producaoEtanol,producaoAcucar,geracaoEnergia,outro) SELECT   ");
            stb.append(projectCloned.getInsumoProduto().getId());
            stb.append(" as insumoProduto_id,ano,areaPlantada,moagemCana,producaoEtanol,producaoAcucar,geracaoEnergia,outro FROM PRODUCAO WHERE insumoProduto_id =  ");
            stb.append(projectToClone.getInsumoProduto().getId());
            
            this.getEm().createNativeQuery(stb.toString()).executeUpdate();
            
            stb = new StringBuffer();
            stb.append("INSERT INTO PRODUTO ");
            stb.append("(insumoProduto_id,nome,ncm_id,sujeitoST,quantidade1,quantidade2,quantidade3,unidadeMedida,tipo) SELECT   ");
            stb.append(projectCloned.getInsumoProduto().getId());
            stb.append(" as insumoProduto_id,nome,ncm_id,sujeitoST,quantidade1,quantidade2,quantidade3,unidadeMedida,tipo FROM PRODUTO WHERE insumoProduto_id =  ");
            stb.append(projectToClone.getInsumoProduto().getId());
            
            this.getEm().createNativeQuery(stb.toString()).executeUpdate();
        }
        
        if (projectCloned.getInfraestrutura() != null) {
            stb = new StringBuffer();
            stb.append("INSERT INTO ENERGIACONTRATADA ");
            stb.append("(infraestrutura_id,contrato,nome,localizacao) SELECT   ");
            stb.append(projectCloned.getInfraestrutura().getId());
            stb.append(" as infraestrutura_id,contrato,nome,localizacao FROM ENERGIACONTRATADA WHERE infraestrutura_id =  ");
            stb.append(projectToClone.getInfraestrutura().getId());
            
            this.getEm().createNativeQuery(stb.toString()).executeUpdate();
        }
        
        if (projectCloned.getMeioAmbiente() != null) {
            stb = new StringBuffer();
            stb.append("INSERT INTO MEIOAMBIENTEANEXO ");
            stb.append("(meioAmbiente_id,anexo_id) SELECT ");
            stb.append(projectCloned.getMeioAmbiente().getId());
            stb.append(" as meioAmbiente_id,anexo_id FROM MEIOAMBIENTEANEXO where meioAmbiente_id =  ");
            stb.append(projectToClone.getMeioAmbiente().getId());
            
            this.getEm().createNativeQuery(stb.toString()).executeUpdate();
            
            stb = new StringBuffer();
            stb.append("INSERT INTO ETAPAMEIOAMBIENTE ");
            stb.append("(meioAmbiente_id,tipo) SELECT   ");
            stb.append(projectCloned.getMeioAmbiente().getId());
            stb.append(" as meioAmbiente_id,tipo FROM ETAPAMEIOAMBIENTE WHERE meioAmbiente_id = ");
            stb.append(projectToClone.getMeioAmbiente().getId());
                        
            this.getEm().createNativeQuery(stb.toString()).executeUpdate();
        }
        
        
//        for (RelatorioAcompanhamento r : reportsToClone) {
//            RelatorioAcompanhamento report = new RelatorioAcompanhamento();
//            report.setFamiliaId(r.getFamiliaId());
//            report.setAno(r.getAno());
//            report.setDataFimPrimeiraCampanha(r.getDataFimPrimeiraCampanha());
//            
//            report.setAceiteTermo(r.isAceiteTermo());
//            report.setAnexos(r.getAnexos());
//            
//        }
//        
//        stb = new StringBuffer();
//        stb.append("INSERT INTO RELATORIOACOMPANHAMENTO ");
//        stb.append("(projeto_id, familia_id, ano, dataInicioPrimeiraCampanha, dataFimPrimeiraCampanha, ");
//        stb.append("dataInicioSegundaCampanha, dataFimSegundaCampanha, dataValidacao, termoConfiabilidade, ");
//        stb.append("aceiteTermo, observacao, nomeResponsavelRelatorio, telefoneResponsavelRelatorio, ");
//        stb.append("emailResponsavelRelatorio, situacaoAtual_id) SELECT  ");
//        stb.append(projectCloned.getId());
//        stb.append(" as projeto_id,familia_id ano, dataInicioPrimeiraCampanha, dataFimPrimeiraCampanha, ");
//        stb.append("dataInicioSegundaCampanha, dataFimSegundaCampanha, dataValidacao, termoConfiabilidade, ");
//        stb.append("aceiteTermo, observacao, nomeResponsavelRelatorio, telefoneResponsavelRelatorio, ");
//        stb.append("emailResponsavelRelatorio, situacaoAtual_id ");        
//        stb.append("FROM RELATORIOACOMPANHAMENTO WHERE projeto_id = ");
//        stb.append(projectToClone.getId());      
//
//        this.getEm().createNativeQuery(stb.toString()).executeUpdate();
//
//        if (projectCloned.getRelatoriosAcompanhamento() != null) {
//            for(RelatorioAcompanhamento r : projectCloned.getRelatoriosAcompanhamento()) {
//                stb = new StringBuffer();
//                stb.append("INSERT INTO AnexoRelatorioAcompanhamento ");
//                stb.append("(relatorioacompanhamento_id,anexo_id) SELECT  ");
//                stb.append(r.getId());
//                stb.append(" as relatorioacompanhamento_id,anexo_id FROM AnexoRelatorioAcompanhamento WHERE projeto_id = ");
//                stb.append(projectToClone.getId());
//            }            
//            this.getEm().createNativeQuery(stb.toString()).executeUpdate();
//        }
        
    }
    
    public void versionProject(Set<Long> familiaIds) {
        List<Projeto> projetosToBlock = findByNamedQuery(Projeto.Query.findVersoesAtuaisByVersaoPai, familiaIds);
        projetosToBlock.forEach((proj) -> {
            HibernateProxyHelper.getClassWithoutInitializingProxy(proj);
            proj.getFinanceiro().getObservacao();
            proj.getInsumoProduto();
            if (proj.getInsumoProduto() != null) {
                proj.getInsumoProduto().getPercentualClienteIndustria();
            }
            proj.getPleito();
            if (proj.getPleito() != null) {
                proj.getPleito().getObservacoes();
            }
            proj.getInfraestrutura();
            if (proj.getInfraestrutura() != null) {
                proj.getInfraestrutura().getAreaAlagada();
            }
            
            versionProject(proj);
        });
    }
    
    public void versionProject(Projeto projetoLastVersion) {
        cloneProject(projetoLastVersion);
        
        projetoLastVersion.setUltimaVersao(Boolean.FALSE);
        this.merge(projetoLastVersion);
    }
    
    private <T extends BaseEntity> void nullifyId(T entity) {
        if (entity != null) {
            entity.setId(null);
            this.getEm().detach(entity);
        }
    }
    
    private void nullifyId(Collection<? extends BaseEntity> collection) {
        collection.forEach(e -> {
            e.setId(null);
            this.getEm().detach(e);
        });
    }
    
    public List<ProjetoTO> findProjetoTOByInstrumentoFormalizacaoId(Long instrumentoFormalizacaoId) {
        CriteriaBuilder builder = getEm().getCriteriaBuilder();
        
        CriteriaQuery<ProjetoTO> query = builder.createQuery(ProjetoTO.class);
        
        Root<Projeto> fromProjeto = query.from(Projeto.class);
        Join<Projeto, InstrumentoFormalizacao> fromInstrumento = fromProjeto.joinSet("instrumentos");
        Join<Projeto, Empresa> fromEmpresa = fromProjeto.join(Projeto_.empresa);
        
        List<Predicate> conditions = new ArrayList();
        
        conditions.add(builder.equal(fromInstrumento.get(InstrumentoFormalizacao_.id), instrumentoFormalizacaoId));
        
        TypedQuery<ProjetoTO> typedQuery = getEm().createQuery(query
                .multiselect(fromProjeto.get(Projeto_.id),
                        fromProjeto.get(Projeto_.versao),
                        fromProjeto.get(Projeto_.versaoPai),
                        fromProjeto.get(Projeto_.situacaoAtual),
                        fromProjeto.get(Projeto_.estagioAtual),
                        fromProjeto.get(Projeto_.nome),
                        fromEmpresa.get(Empresa_.id),
                        fromEmpresa.get(Empresa_.nomePrincipal),
                        fromProjeto.get(Projeto_.cadeiaProdutiva))
                .where(conditions.toArray(new Predicate[]{}))
                .distinct(true)
                .orderBy(builder.asc(fromProjeto.get(Projeto_.id)))
        );
        List<ProjetoTO> projetos = typedQuery.getResultList();
        
        for (ProjetoTO projeto : projetos) {
            projeto.setInstrumentos(instrumentoFormalizacaoService.findByProjetoIdWithFirstVersion(projeto.getId()));
            projeto.setEstagios(this.createTypedQuery(EstagioProjeto.class, EstagioProjeto.Query.findAllByProjeto, projeto.getId()).getResultList());
        }
        
        return projetos;
    }

    /**
     * Retorna o último dia do mês passado no parâmetro nowDate.
     *
     * @param nowDate
     * @return Date (último dia do mês)
     */
    private static Date getLastDayOfMonth(Date nowDate) {
        Calendar c = Calendar.getInstance();
        c.setTime(nowDate);
        c.add(Calendar.MONTH, 1);
        c.set(Calendar.MONTH, c.get(Calendar.MONTH) - 1);
        c.set(Calendar.DAY_OF_MONTH, c.getActualMaximum(Calendar.DAY_OF_MONTH));
        Date nextDate = c.getTime();
        return nextDate;
    }
    
    public synchronized List<RelatorioAgendaPositivaAbstract> findAgendaPositivaTOByFilters(String estagio,
            Date dataIni, Date dataFim, List<Long> regioesPlanejamento,
            List<Long> departamentos, List<Long> usuariosResponsaveis,
            List<Long> cadeiasProdutivas, Long diretoria, Boolean versao, Boolean notShowRealized) {
        StringBuilder builder = new StringBuilder();
        builder.append(" SELECT v FROM ");
        builder.append(RelatorioAgendaPositiva.RelatorioAgendaPositivaEnum.valueOf(estagio).getDescricaoClasse());
        builder.append(" v ");
        builder.append(" Where v.isLastVersion = :versao ");
        
        if (notShowRealized != null && notShowRealized) {
            builder.append(" AND v.dataReal is NULL ");
        }
        
//        if (estagio != null && !estagio.isEmpty()) {
//            builder.append(" And v.estagioPrevisto = :estagioPrevisto ");
//        }
        
        if (dataIni != null && dataFim != null) {
            if (notShowRealized != null && notShowRealized) {
                builder.append(" And (v.dataPrevisao between :dataIni and :dataFim) ");
            } else {
                builder.append(" And ( ");
                builder.append("  (v.dataPrevisao between :dataIni and :dataFim) OR (v.dataReal IS NULL and v.dataPrevisao < :dataIni) ");
                builder.append(" ) ");
            }
        }
        
        if (regioesPlanejamento != null && !regioesPlanejamento.isEmpty()) {
            builder.append(" And v.regiaoPlanejamento_id in (:regioesPlanejamento) ");
        }
        
        if (departamentos != null && !departamentos.isEmpty()) {
            builder.append(" And v.departamento_id in (:departamentos) ");
        }
        
        if (usuariosResponsaveis != null && !usuariosResponsaveis.isEmpty()) {
            builder.append(" And v.usuarioResponsavel_id in (:usuariosResponsaveis) ");
        }
        
        if (cadeiasProdutivas != null && !cadeiasProdutivas.isEmpty()) {
            builder.append(" And v.cadeiaProdutiva in (:cadeiasProdutivas) ");
        }
        
        if (diretoria != null) {
            builder.append(" And v.diretoriaId = :diretoriaId ");
        }
        
        builder.append(" Order By v.dataPrevisao, v.totalInvestimentoPrevisto desc ");        
        TypedQuery<RelatorioAgendaPositivaAbstract> query = getEm().createQuery(builder.toString(), RelatorioAgendaPositivaAbstract.class);
        
        if (dataIni != null && dataFim != null) {
            query.setParameter("dataIni", dataIni, TemporalType.DATE);
            Date dataFinal = getLastDayOfMonth(dataFim);
            query.setParameter("dataFim", dataFinal, TemporalType.DATE);
        }
        
//        if (estagio != null && !estagio.isEmpty()) {
//            query.setParameter("estagioPrevisto", estagio);
//        }
        
        if (regioesPlanejamento != null && !regioesPlanejamento.isEmpty()) {
            query.setParameter("regioesPlanejamento", regioesPlanejamento);
        }
        
        if (departamentos != null && !departamentos.isEmpty()) {
            query.setParameter("departamentos", departamentos);
        }
        
        if (usuariosResponsaveis != null && !usuariosResponsaveis.isEmpty()) {
            query.setParameter("usuariosResponsaveis", usuariosResponsaveis);
        }
        
        if (cadeiasProdutivas != null && !cadeiasProdutivas.isEmpty()) {
            query.setParameter("cadeiasProdutivas", cadeiasProdutivas);
        }
        
        if (diretoria != null) {
            query.setParameter("diretoriaId", diretoria.intValue());
        }
        
        query.setParameter("versao", false);
        final List<RelatorioAgendaPositivaAbstract> listaOriginal = query.getResultList();
        
        return montaListasAgendaPositiva(listaOriginal);
    }

    /**
     * Monta as listas necessárias para a apresentação do relatório.
     *
     * @param listaOriginal
     * @return List<RelatorioAgendaPositiva>
     */
    public List<RelatorioAgendaPositivaAbstract> montaListasAgendaPositiva(List<RelatorioAgendaPositivaAbstract> listaOriginal) {
        List<RelatorioAgendaPositivaAbstract> listaAgrupada = new ArrayList<>();
        int countPeriodProjects = 0;
        int countAllProjects = 0;
        Double countPeriodFat = 0.0;
        Double countAllFat = 0.0;
        Double countPeriodInvest = 0.0;
        Double countAllInvest = 0.0;
        int countPeriodEmpregos = 0;
        int countAllEmpregos = 0;
        Date dataPrev = null;
        for (RelatorioAgendaPositivaAbstract relatorioAgendaPositiva : listaOriginal) {
            if (dataPrev == null || relatorioAgendaPositiva.getDataPrevisao() == null || (dataPrev.getMonth() == relatorioAgendaPositiva.getDataPrevisao().getMonth()
                    && dataPrev.getYear() == relatorioAgendaPositiva.getDataPrevisao().getYear())) {
                listaAgrupada.add(relatorioAgendaPositiva);
                
                dataPrev = relatorioAgendaPositiva.getDataPrevisao();
                countPeriodEmpregos = countPeriodEmpregos + relatorioAgendaPositiva.getTotalEmpregoDireto();
                countAllEmpregos = countAllEmpregos + relatorioAgendaPositiva.getTotalEmpregoDireto();
                countPeriodFat += relatorioAgendaPositiva.getTotalFaturamentoPrevisto();
                countAllFat = countAllFat + relatorioAgendaPositiva.getTotalFaturamentoPrevisto();
                countPeriodInvest = countPeriodInvest + relatorioAgendaPositiva.getTotalInvestimentoPrevisto();
                countAllInvest = countAllInvest + relatorioAgendaPositiva.getTotalInvestimentoPrevisto();
                countPeriodProjects++;
                countAllProjects++;
            } else {
                RelatorioAgendaPositiva sumPeriod = new RelatorioAgendaPositiva();
                sumPeriod.setNomeProjeto(countPeriodProjects + " Projetos");
                sumPeriod.setTotalFaturamentoPrevisto(round(countPeriodFat, 2));
                sumPeriod.setTotalInvestimentoPrevisto(round(countPeriodInvest, 2));
                sumPeriod.setTotalEmpregoDireto(countPeriodEmpregos);
                listaAgrupada.add(sumPeriod);
                listaAgrupada.add(relatorioAgendaPositiva);
                countPeriodProjects = 1;
                countPeriodEmpregos = relatorioAgendaPositiva.getTotalEmpregoDireto();
                countPeriodFat = relatorioAgendaPositiva.getTotalFaturamentoPrevisto();
                countPeriodInvest = relatorioAgendaPositiva.getTotalInvestimentoPrevisto();
                countAllProjects++;
                countAllEmpregos = countAllEmpregos + relatorioAgendaPositiva.getTotalEmpregoDireto();
                countAllFat = countAllFat + relatorioAgendaPositiva.getTotalFaturamentoPrevisto();
                countAllInvest = countAllInvest + relatorioAgendaPositiva.getTotalInvestimentoPrevisto();
                dataPrev = relatorioAgendaPositiva.getDataPrevisao();
            }
        }
        
        RelatorioAgendaPositiva sumPeriod = new RelatorioAgendaPositiva();
        sumPeriod.setNomeProjeto(countPeriodProjects + " Projetos");
        sumPeriod.setTotalEmpregoDireto(countPeriodEmpregos);
        sumPeriod.setTotalFaturamentoPrevisto(round(countPeriodFat, 2));
        sumPeriod.setTotalInvestimentoPrevisto(round(countPeriodInvest, 2));
        listaAgrupada.add(sumPeriod);
        RelatorioAgendaPositiva sumAll = new RelatorioAgendaPositiva();
        sumAll.setNomeProjeto(countAllProjects + " Projetos");
        sumAll.setTotalEmpregoDireto(countAllEmpregos);
        sumAll.setTotalFaturamentoPrevisto(round(countAllFat, 2));
        sumAll.setTotalInvestimentoPrevisto(round(countAllInvest, 2));
        listaAgrupada.add(sumAll);
        
        return listaAgrupada;
    }

    /**
     * Método utilizado para arredondamentos de valores double.
     *
     * @param value - Valor Double
     * @param places - Número de casas decimais
     * @return - Valor arredondado.
     */
    private double round(double value, int places) {
        if (places < 0) {
            throw new IllegalArgumentException();
        }
        
        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }
    
    public ProjetoTO findEstagioAtualById(Long idProjeto) {
        CriteriaBuilder builder = getEm().getCriteriaBuilder();
        CriteriaQuery<ProjetoTO> query = builder.createQuery(ProjetoTO.class);
        Root<Projeto> fromProjeto = query.from(Projeto.class);
        List<Predicate> conditions = new ArrayList();
        conditions.add(builder.equal(fromProjeto.get(Projeto_.id), idProjeto));
        
        TypedQuery<ProjetoTO> typedQuery = getEm().createQuery(query
                .multiselect(fromProjeto.get(Projeto_.estagioAtual))
                .where(conditions.toArray(new Predicate[]{})));
        return typedQuery.getSingleResult();
    }
    
    private static final BigDecimal MIL = new BigDecimal("1000");
    
    public void insertProjetoRecente(Long projetoId, Long usuarioId) {
        ProjetoRecente projetoRecente = new ProjetoRecente();
        projetoRecente.setProjetoId(projetoId);
        projetoRecente.setUsuarioId(usuarioId);
        projetoRecente.setDataAcesso(new Date());
        merge(projetoRecente);
    }
    
    public List<ViewRelatorioAcompanhamentoAnual> buildRelatorioAcompanhamentoAnual(QueryParameter filters) {
        StringBuilder builder = new StringBuilder();
        builder.append(" Select r from ViewRelatorioAcompanhamentoAnual r where 1=1 ");
        
//        if (filters.exists(RelatorioAcompanhamento.Filters.ANO)) {
//            builder.append(" And (r.ano = :ano or r.ano is null) ");
//        }
        
//        if (filters.exists(RelatorioAcompanhamento.Filters.SITUACAO_ACOMPANHAMENTO)) {
//            if (filters.get(RelatorioAcompanhamento.Filters.SITUACAO_ACOMPANHAMENTO).toString().equals(
//                    SituacaoRelatorioEnum.TODOS.toString())) {
//                builder.append(" And (r.situacaoAtual in (0,1,2,3) or r.situacaoAtual is null) ");
//            } else if(!filters.get(RelatorioAcompanhamento.Filters.SITUACAO_ACOMPANHAMENTO).toString().equals(
//                    SituacaoRelatorioEnum.NAO_SOLICITADO.toString())) {
//                builder.append(" And (r.situacaoAtual = :situacao) ");
//            }
//        }
        
        if (filters.exists(Projeto.Filters.NOME_PROJETO)) {
            builder.append(" And (r.nomeProjeto like CONCAT('%', :nomeProjeto, '%')) ");
        }

        if (filters.exists(Projeto.Filters.NOME_EMPRESA)) {
            builder.append(" And (r.nomeEmpresa like CONCAT('%', :nomeEmpresa, '%')) ");
        }
        
        if (filters.exists(Projeto.Filters.ESTAGIO)) {
            builder.append(" And (r.estagio in (:estagios)) ");
        }
        
        if (filters.exists(Projeto.Filters.CADEIA_PRODUTIVA)) {
            builder.append(" And (r.cadeiaProdutiva in (:cadeiasProdutivas)) ");
        }

        if (filters.exists(Projeto.Filters.DEPARTAMENTOS)) {
            builder.append(" And (r.departamento in (:departamentos)) ");
        }
        
        if (filters.exists(Projeto.Filters.USUARIO_RESPONSAVEL_PROJETO)) {
            builder.append(" And (r.usuario in (:usuarios)) ");
        }            

        Query query = getEm().createQuery(builder.toString());
        
//        if (filters.exists(RelatorioAcompanhamento.Filters.ANO)) {
//            query.setParameter("ano", Integer.parseInt((filters.parameters().get(
//                    RelatorioAcompanhamento.Filters.ANO).toString())));
//        }
        
//        if (filters.exists(RelatorioAcompanhamento.Filters.SITUACAO_ACOMPANHAMENTO)) {
//            if (!filters.get(RelatorioAcompanhamento.Filters.SITUACAO_ACOMPANHAMENTO).toString().equals(
//                    SituacaoRelatorioEnum.TODOS.toString()) && !filters.get(RelatorioAcompanhamento.Filters.SITUACAO_ACOMPANHAMENTO)
//                            .toString().equals(SituacaoRelatorioEnum.NAO_SOLICITADO.toString())) {
//                query.setParameter("situacao",(filters.parameters().get(
//                        RelatorioAcompanhamento.Filters.SITUACAO_ACOMPANHAMENTO)));
//            }
//        }

        if (filters.exists(Projeto.Filters.NOME_PROJETO)) {
            query.setParameter("nomeProjeto",(filters.parameters().get(
                    RelatorioAcompanhamento.Filters.NOME_PROJETO)));
        }
        
        if (filters.exists(Projeto.Filters.NOME_EMPRESA)) {
            query.setParameter("nomeEmpresa",(filters.parameters().get(
                    RelatorioAcompanhamento.Filters.NOME_EMPRESA)));
        }
        
        if (filters.exists(Projeto.Filters.ESTAGIO)) {
            query.setParameter("estagios",(filters.parameters().get(
                    RelatorioAcompanhamento.Filters.ESTAGIO)));
        }
        
        if (filters.exists(Projeto.Filters.CADEIA_PRODUTIVA)) {
            query.setParameter("cadeiasProdutivas",(filters.parameters().get(
                    RelatorioAcompanhamento.Filters.CADEIA_PRODUTIVA)));            
        }
        
        if (filters.exists(Projeto.Filters.DEPARTAMENTOS)) {
            query.setParameter("departamentos",(filters.parameters().get(
                    Projeto.Filters.DEPARTAMENTOS)));     
        }
        
        if (filters.exists(Projeto.Filters.USUARIO_RESPONSAVEL_PROJETO)) {
            query.setParameter("usuarios",(filters.parameters().get(
                Projeto.Filters.USUARIO_RESPONSAVEL_PROJETO))); 
        }

        List<ViewRelatorioAcompanhamentoAnual> resultList = query.getResultList();
        Integer ano = new Integer(filters.get("ano", String.class));
        SituacaoRelatorioEnum situacao = filters.get("situacao",SituacaoRelatorioEnum.class);
        //resultList.stream().forEach((r) -> {if(r.getAno() == ano) r.setAno(ano);});

        return processarResultado(resultList,ano,situacao);
    }
    
    private List<ViewRelatorioAcompanhamentoAnual> processarResultado(List<ViewRelatorioAcompanhamentoAnual> res, Integer ano, SituacaoRelatorioEnum sre){
        Set<Map.Entry<Long, List<ViewRelatorioAcompanhamentoAnual>>> es = res.parallelStream().map((relat)-> {
            if(relat.getSituacaoAtual() == null) {
                relat.setSituacaoAtual(SituacaoRelatorioEnum.NAO_SOLICITADO);
            }
            if(relat.getAno() == null){
                relat.setAno(ano);
            } return relat;
        }).collect(Collectors.groupingBy(ViewRelatorioAcompanhamentoAnual::getId)).entrySet();
        List<ViewRelatorioAcompanhamentoAnual> listaResult = es.parallelStream().filter((ent) -> ent.getValue() != null && !ent.getValue().isEmpty()).map((t) -> {
            List<ViewRelatorioAcompanhamentoAnual> lst = t.getValue() ==  null ? new ArrayList<>(2):t.getValue();
            if(lst.stream().filter((r) -> (r.getSituacaoAtual() == null &&  sre.equals(SituacaoRelatorioEnum.NAO_SOLICITADO)))
                    .noneMatch((a)-> ano.equals(a.getAno()) || sre.equals(a.getSituacaoAtual()))){
                ViewRelatorioAcompanhamentoAnual novo = lst.stream().findFirst().orElseThrow(()-> new IllegalStateException(
                        "Estado incorreto. Era esperado pelo menos um relatório neste ponto. Impossível prosseguir."))
                        .copiarNovoComAnoCorrente(ano, SituacaoRelatorioEnum.NAO_SOLICITADO);
                lst.add(novo);
                t.setValue(lst);                
            }
            return t;
        }).flatMap((e)-> e.getValue().stream()).filter((e)-> (sre.equals(e.getSituacaoAtual()) || 
                sre.equals(SituacaoRelatorioEnum.TODOS)) && ano.equals(e.getAno())).collect(Collectors.toList());
        return listaResult;
    }
    
    public Projeto findProjetoVersaoCongeladaByFamiliaId(Long familiaId) {
        
        TypedQuery<Projeto> namedQuery = getEm().createNamedQuery(Projeto.Query.findByVersaoAndSituacaoNotEqualsAndUltimaVersaoAndVersaoPai, Projeto.class);
        namedQuery.setParameter(1, 0);
        namedQuery.setParameter(2, SituacaoProjetoEnum.PEDENTE_VALIDACAO);
        namedQuery.setParameter(3, Boolean.TRUE);
        namedQuery.setParameter(4, familiaId);        
        
        try {
            Projeto projeto = namedQuery.getSingleResult();
            if (projeto != null) {
                return projeto;
            }            
        } catch(Exception e) {            
        }
        
        CriteriaBuilder builder = getEm().getCriteriaBuilder();
        CriteriaQuery<Projeto> query = builder.createQuery(Projeto.class);
        Root<Projeto> fromProjeto = query.from(Projeto.class);
        List<Predicate> conditions = new ArrayList();
        
        conditions.add(builder.equal(fromProjeto.get(Projeto_.versaoPai), familiaId));
        conditions.add(builder.equal(fromProjeto.get(Projeto_.ultimaVersao), Boolean.FALSE));
        conditions.add(builder.notEqual(fromProjeto.get(Projeto_.situacao), SituacaoProjetoEnum.PEDENTE_VALIDACAO));
        
        TypedQuery<Projeto> typedQuery = getEm().createQuery(query
                .where(conditions.toArray(new Predicate[]{}))
                .orderBy(builder.desc(fromProjeto.get(Projeto_.versao))));
        
        List<Projeto> lista = typedQuery.getResultList(); 
        if (lista != null && !lista.isEmpty()) {
            return lista.get(0);
        }            
        
        return null;
    }            
    
}
