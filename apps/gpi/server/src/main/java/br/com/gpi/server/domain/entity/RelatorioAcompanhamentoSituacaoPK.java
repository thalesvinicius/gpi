package br.com.gpi.server.domain.entity;

import java.io.Serializable;

public class RelatorioAcompanhamentoSituacaoPK implements Serializable {
    
    private long relatorioAcompanhamentoId;
    private long situacaoRelatorioId;

    public long getRelatorioAcompanhamentoId() {
        return relatorioAcompanhamentoId;
    }

    public void setRelatorioAcompanhamentoId(long relatorioAcompanhamentoId) {
        this.relatorioAcompanhamentoId = relatorioAcompanhamentoId;
    }

    public long getSituacaoRelatorioId() {
        return situacaoRelatorioId;
    }

    public void setSituacaoRelatorioId(long situacaoRelatorioId) {
        this.situacaoRelatorioId = situacaoRelatorioId;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 61 * hash + (int) (this.relatorioAcompanhamentoId ^ (this.relatorioAcompanhamentoId >>> 32));
        hash = 61 * hash + (int) (this.situacaoRelatorioId ^ (this.situacaoRelatorioId >>> 32));
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final RelatorioAcompanhamentoSituacaoPK other = (RelatorioAcompanhamentoSituacaoPK) obj;
        if (this.relatorioAcompanhamentoId != other.relatorioAcompanhamentoId) {
            return false;
        }
        if (this.situacaoRelatorioId != other.situacaoRelatorioId) {
            return false;
        }
        return true;
    }

    
}
