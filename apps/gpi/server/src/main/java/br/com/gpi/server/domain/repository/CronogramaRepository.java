package br.com.gpi.server.domain.repository;

import br.com.gpi.server.domain.entity.Cronograma;
import org.apache.deltaspike.data.api.EntityRepository;
import org.apache.deltaspike.data.api.Query;
import org.apache.deltaspike.data.api.Repository;
import org.apache.deltaspike.data.api.criteria.CriteriaSupport;

@Repository(forEntity = Cronograma.class)
public abstract class CronogramaRepository implements CriteriaSupport<Cronograma>,EntityRepository<Cronograma, Long> {

    @Query(named=Cronograma.Query.locateByProject)
    public abstract Cronograma findByProject(Long id);
}
