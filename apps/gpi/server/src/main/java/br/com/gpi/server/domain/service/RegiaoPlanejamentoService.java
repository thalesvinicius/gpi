package br.com.gpi.server.domain.service;

import br.com.gpi.server.domain.common.BaseService;
import br.com.gpi.server.domain.entity.RegiaoPlanejamento;
import javax.inject.Inject;
import javax.persistence.EntityManager;

public class RegiaoPlanejamentoService extends BaseService<RegiaoPlanejamento>{
    
    @Inject
    public RegiaoPlanejamentoService(EntityManager em) {
        super(RegiaoPlanejamento.class, em);
    }
    
}
