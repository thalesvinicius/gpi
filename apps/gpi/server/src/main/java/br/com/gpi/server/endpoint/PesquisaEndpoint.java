package br.com.gpi.server.endpoint;

import br.com.gpi.server.domain.entity.InstrumentoFormalizacao;
import br.com.gpi.server.domain.entity.Pesquisa;
import br.com.gpi.server.domain.entity.User;
import br.com.gpi.server.domain.entity.UsuarioExterno;
import br.com.gpi.server.domain.repository.PesquisaRepository;
import br.com.gpi.server.domain.repository.UserExternalRepository;
import br.com.gpi.server.domain.repository.UserRepository;
import br.com.gpi.server.domain.service.InstrumentoFormalizacaoService;
import br.com.gpi.server.domain.service.PesquisaService;
import br.com.gpi.server.domain.user.model.SecurityUser;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

@Path("instrumento/pesquisa")
@Stateless
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
@TransactionManagement(TransactionManagementType.BEAN)
public class PesquisaEndpoint {
    
    @Inject
    private PesquisaRepository pesquisaRepository;
    
    @Inject
    private PesquisaService pesquisaService;    
        
    @Inject
    private UserRepository userRepository;
    
    @Inject
    private UserExternalRepository userExternalRepository;

    @Inject
    private InstrumentoFormalizacaoService instrumentoFormalizacaoService;
    
    
    @POST
    public Response saveOrUpdate(@Valid Pesquisa pesquisa, @Context SecurityContext context) {
        try {
            SecurityUser loggedUserWrap = (SecurityUser) context.getUserPrincipal();
            User user = userRepository.findBy(loggedUserWrap.getId());
            
            InstrumentoFormalizacao instrumentoFormalizacao = instrumentoFormalizacaoService.locateByIdForEdition(pesquisa.getInstrumentoFormalizacao().getId());
            pesquisa.setInstrumentoFormalizacao(instrumentoFormalizacao);
            
            return Response.ok(pesquisaService.saveOrUpdate(pesquisa, user)).build();
        } catch (Exception ex) {
            Logger.getLogger(PesquisaEndpoint.class.getName()).log(Level.SEVERE, null, ex);
            throw new RuntimeException(ex);
        }
    }
   
    @GET
    @Path("byInstrumentoId/{instrumentoId}")
    public Response locateByInstrumentoFormalizacaoId(@PathParam("instrumentoId") Long instrumentoId) {
        Pesquisa pesquisa = pesquisaRepository.locateLastByInstrumentoFormalizacaoId(instrumentoId);        
        
        if (pesquisa != null) {
            InstrumentoFormalizacao instrumentoFormalizacao = instrumentoFormalizacaoService.locateByIdForEdition(pesquisa.getInstrumentoFormalizacao().getId());
            pesquisa.setInstrumentoFormalizacao(instrumentoFormalizacao);            
        }        
        return Response.ok(pesquisa).build();
    }

    @GET
    @Path("byId/{pesquisaId}")
    public Response locateById(@PathParam("pesquisaId") Long idPesquisa) {
        Pesquisa pesquisa = null;
        pesquisa = pesquisaRepository.findBy(idPesquisa);                

        if (pesquisa != null) {
            InstrumentoFormalizacao instrumentoFormalizacao = instrumentoFormalizacaoService.locateByIdForEdition(pesquisa.getInstrumentoFormalizacao().getId());
            pesquisa.setInstrumentoFormalizacao(instrumentoFormalizacao);            
        }        
        return Response.ok(pesquisa).build();
    }

    @GET
    @Path("empresausuario")
    public Response findAllByUsuarioExternoEmpresa(@Context SecurityContext context) {
        SecurityUser loggedUserWrap = (SecurityUser) context.getUserPrincipal();
        User user = userRepository.findBy(loggedUserWrap.getId());        
        if (user instanceof UsuarioExterno) {
            UsuarioExterno usuarioExterno = userExternalRepository.findBy(loggedUserWrap.getId());
            List<Pesquisa> pesquisaList = pesquisaRepository.locateByEmpresaId(usuarioExterno.getEmpresaId());            
            return Response.ok(pesquisaList).build();
        } 
        return Response.ok().build();
    }
}


