package br.com.gpi.server.domain.common;

import br.com.gpi.server.core.jpa.PaginationHelper;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public final class QueryParameter {

    private final Map<String, Object> parameters;
    private PaginationHelper paginationHelper;

    private QueryParameter(String name, Object value) {
        this.parameters = new HashMap<>();
        this.parameters.put(name, extractValidParamValue(value));
    }

    public static QueryParameter with(String name, Object value) {
        return new QueryParameter(name, value);
    }

    public QueryParameter paginate(int paginationSize, int pageNumber) {
        this.paginationHelper = new PaginationHelper(paginationSize, pageNumber);
        return this;
    }

    public QueryParameter and(String name, Object value) {
        this.parameters.put(name, extractValidParamValue(value));
        return this;
    }

    public Map<String, Object> parameters() {
        return this.parameters;
    }

    public Object get(String paramName) {
        return this.parameters.get(paramName);
    }

    public <T extends Object> T get(String paramName, Class<T> classe) {
        return classe.cast(this.parameters.get(paramName));
    }

    public String getString(String paramName) {
        return (String) this.parameters.get(paramName);
    }

    public Long getLong(String paramName) {
        return (Long) this.parameters.get(paramName);
    }

    public boolean exists(String paramName) {
        if (this.parameters.get(paramName) != null) {
            Object obj = this.parameters.get(paramName);
            if (Collection.class.isAssignableFrom(obj.getClass())) {
                return !((Collection) obj).isEmpty();
            } else {
                return true;
            }
        }
        return false;
    }

    private Object extractValidParamValue(Object value) {
        if (value != null && (value instanceof String) && ((String) value).trim().isEmpty()) {
            return null;
        } else {
            return value;
        }
    }

    public boolean hasPagination() {
        return this.paginationHelper != null;
    }

    public PaginationHelper getPagination() {
        return this.paginationHelper;
    }
}
