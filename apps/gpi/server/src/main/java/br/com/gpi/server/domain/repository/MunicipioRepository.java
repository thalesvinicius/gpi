package br.com.gpi.server.domain.repository;

import br.com.gpi.server.domain.entity.Municipio;
import java.util.List;
import org.apache.deltaspike.data.api.EntityRepository;
import org.apache.deltaspike.data.api.Query;
import org.apache.deltaspike.data.api.Repository;
import org.apache.deltaspike.data.api.criteria.CriteriaSupport;

@Repository(forEntity = Municipio.class)
public abstract class MunicipioRepository implements CriteriaSupport<Municipio>, EntityRepository<Municipio, Long> {

    @Query(named = Municipio.Query.FIND_BY_ID)
    public abstract List<Municipio> findByUF(Long UFId);

}
