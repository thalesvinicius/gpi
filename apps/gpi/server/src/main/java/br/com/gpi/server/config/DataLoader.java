package br.com.gpi.server.config;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.InputStream;
import java.util.List;
import javax.persistence.EntityManager;

public class DataLoader {

    private final DataType dataType;
    private final EntityManager em;
    private ObjectMapper objectMapper;

    public DataLoader(DataType dataType, EntityManager entityManager) {
        this.dataType = dataType;
        this.em = entityManager;
    }

    public void loadScript(String jsonFile, Class<?> _class) throws Exception {
        InputStream in = getClass().getResourceAsStream(jsonFile);
        DataLoader.this.loadScript(in, _class);
    }

    private <T> void loadScript(InputStream in, Class<T> _class) throws Exception {
        JavaType type = getObjectMapper().getTypeFactory().
                constructCollectionType(List.class, _class);
        List<T> readValue = getObjectMapper().readValue(in, type);
        if (!readValue.isEmpty()) {
            for (Object object : readValue) {
                em.merge(object);
            }
        }
    }

    private ObjectMapper getObjectMapper() {
        if (this.objectMapper == null) {
            this.objectMapper = new ObjectMapper();
        }
        return this.objectMapper;
    }

    public static enum DataType {

        JSON, YML, SQL;
    }
}
