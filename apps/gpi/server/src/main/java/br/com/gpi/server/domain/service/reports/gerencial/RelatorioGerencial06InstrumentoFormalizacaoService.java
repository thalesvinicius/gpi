/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.gpi.server.domain.service.reports.gerencial;

import br.com.gpi.server.domain.common.QueryParameter;
import br.com.gpi.server.domain.entity.TipoInstrFormalizacao;
import br.com.gpi.server.domain.service.reports.BaseReportService;
import br.com.gpi.server.domain.view.RelatorioAcompanhamentoInstrumento;
import br.com.gpi.server.domain.view.RelatorioAcompanhamentoInstrumento_;
import br.com.gpi.server.util.DateUtils;
import br.com.gpi.server.util.EnumMimeTypes;
import br.com.gpi.server.util.Formato;
import br.com.gpi.server.util.functional.FunctionalInterfaces;
import br.com.gpi.server.util.functional.FunctionalInterfaces.JaspeReportBuilderFunction;
import br.com.gpi.server.util.reports.ReportUtil;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Supplier;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.ws.rs.core.Response;
import net.sf.dynamicreports.jasper.builder.JasperReportBuilder;
import net.sf.dynamicreports.report.builder.DynamicReports;
import static net.sf.dynamicreports.report.builder.DynamicReports.cmp;
import static net.sf.dynamicreports.report.builder.DynamicReports.col;
import static net.sf.dynamicreports.report.builder.DynamicReports.stl;
import static net.sf.dynamicreports.report.builder.DynamicReports.type;
import net.sf.dynamicreports.report.builder.column.ColumnBuilder;
import net.sf.dynamicreports.report.builder.component.ComponentBuilder;
import net.sf.dynamicreports.report.builder.style.StyleBuilder;
import net.sf.dynamicreports.report.constant.HorizontalAlignment;
import net.sf.dynamicreports.report.constant.VerticalAlignment;
import net.sf.dynamicreports.report.datasource.DRDataSource;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JREmptyDataSource;

/**
 *
 * @author rafaelbfs
 */
public class RelatorioGerencial06InstrumentoFormalizacaoService extends BaseReportService<RelatorioAcompanhamentoInstrumento>{

    @Inject
    public RelatorioGerencial06InstrumentoFormalizacaoService(EntityManager em) {
        super(em);
    }
    
    private final Comparator<Date> safeDateComparator = (d1,d2) -> {
        Date data1 = Optional.ofNullable(d1).orElse(new Date(Long.MAX_VALUE));
        Date data2 = Optional.ofNullable(d2).orElse(new Date(Long.MAX_VALUE));
        return data1.compareTo(data2);
    };
    
    public List<RelatorioAcompanhamentoInstrumento> generate(QueryParameter filtros) {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<RelatorioAcompanhamentoInstrumento> query = builder.createQuery(RelatorioAcompanhamentoInstrumento.class).distinct(true);
        Root<RelatorioAcompanhamentoInstrumento> fromRelatorioAcompanhamentoInstr = query.from(RelatorioAcompanhamentoInstrumento.class);

        List<Predicate> conditions = new ArrayList<>();
        
        if (filtros.exists(RelatorioAcompanhamentoInstrumento.Filtro.GERENCIA)) {
            conditions.add(fromRelatorioAcompanhamentoInstr.get(RelatorioAcompanhamentoInstrumento_.gerenciaId).in(filtros.get(RelatorioAcompanhamentoInstrumento.Filtro.GERENCIA, List.class)));
            if (filtros.exists(RelatorioAcompanhamentoInstrumento.Filtro.ANALISTA)) {
                conditions.add(fromRelatorioAcompanhamentoInstr.get(RelatorioAcompanhamentoInstrumento_.analistaId).in(filtros.get(RelatorioAcompanhamentoInstrumento.Filtro.ANALISTA, List.class)));
            }
        }
        
        if (filtros.exists(RelatorioAcompanhamentoInstrumento.Filtro.TIPOS)) {
            conditions.add(fromRelatorioAcompanhamentoInstr.get(RelatorioAcompanhamentoInstrumento_.tipo).in(filtros.get(RelatorioAcompanhamentoInstrumento.Filtro.TIPOS, List.class)));
        }
        
        if (filtros.exists(RelatorioAcompanhamentoInstrumento.Filtro.LOCAIS)) {
            conditions.add(fromRelatorioAcompanhamentoInstr.get(RelatorioAcompanhamentoInstrumento_.localAtividadeId).in(filtros.get(RelatorioAcompanhamentoInstrumento.Filtro.LOCAIS, List.class)));
        }

        if (filtros.exists(RelatorioAcompanhamentoInstrumento.Filtro.PREVISAO_ASSINATURA_DE)
                && filtros.exists(RelatorioAcompanhamentoInstrumento.Filtro.PREVISAO_ASSINATURA_ATE)) {
            Date previsaoDe = DateUtils.fromMillisecondsToDate(filtros.get(RelatorioAcompanhamentoInstrumento.Filtro.PREVISAO_ASSINATURA_DE, Long.class));
            Date previsaoAte = DateUtils.fromMillisecondsToDate(filtros.get(RelatorioAcompanhamentoInstrumento.Filtro.PREVISAO_ASSINATURA_ATE, Long.class));
            
            conditions.add(builder.between(fromRelatorioAcompanhamentoInstr.get(RelatorioAcompanhamentoInstrumento_.previsaoAssinatura), 
                    previsaoDe, previsaoAte));
        }        
        
        TypedQuery<RelatorioAcompanhamentoInstrumento> typedQuery = entityManager.createQuery(query
                .where(conditions.toArray(new Predicate[]{})).distinct(true));
        

        List<RelatorioAcompanhamentoInstrumento> result = typedQuery.getResultList();
        //Map<Long, List<RelatorioAcompanhamentoInstrumento>> grupos = result.stream().collect(Collectors.groupingBy(RelatorioAcompanhamentoInstrumento::getId));
//        
        return result;
    }    
    
    public Response generateReport(Supplier<Map<String,Object>> parametrosExpr,String format, 
            List<Long> gerencias,
            List<Long> analistas,
            List<TipoInstrFormalizacao> tipos,
            List<Long> locais,
            Long previsaoAssinaturaDe,
            Long previsaoAssinaturaAte) throws IOException{
        
        QueryParameter filtros = QueryParameter.with(RelatorioAcompanhamentoInstrumento.Filtro.GERENCIA, gerencias)
                .and(RelatorioAcompanhamentoInstrumento.Filtro.ANALISTA,analistas)
                .and(RelatorioAcompanhamentoInstrumento.Filtro.TIPOS,tipos)
                .and(RelatorioAcompanhamentoInstrumento.Filtro.LOCAIS,locais)                
                .and(RelatorioAcompanhamentoInstrumento.Filtro.PREVISAO_ASSINATURA_DE, previsaoAssinaturaDe)
                .and(RelatorioAcompanhamentoInstrumento.Filtro.PREVISAO_ASSINATURA_ATE, previsaoAssinaturaAte);
        
        List<RelatorioAcompanhamentoInstrumento> result = generate(filtros);

        
        // Colunas do instrumento
        ColumnBuilder[] cb = {            
            col.column("Empresa", "nomeEmpresa", type.stringType()).setWidth(15),
            col.column("Título do Instrumento", "tituloInstrumento", type.stringType()).setWidth(13),
            col.column("Previsão Assinatura", "previsaoAssinatura", type.stringType()).setWidth(6),
            col.column("Tipo", "tipo", type.stringType()).setWidth(8),
            col.column("Local", "local", type.stringType()).setWidth(8),
            col.column("Atividade", "atividade", type.stringType()).setWidth(14),
            col.column("Data Início Atividade", "dataInicioAtividade", type.stringType()).setWidth(6),
            col.column("Analista", "analista", type.stringType()).setWidth(7),
            col.column("Gerência", "gerente", type.stringType()).setWidth(5)
        };

        Formato formato = Formato.getFormatoComExtensao(format);
        if (formato == null) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
        
        Map<String, Object> parametros = parametrosExpr.get();
        
       

        JaspeReportBuilderFunction headerResolver = (report) -> {
            StyleBuilder boldCenteredStyle = stl.style().setFont(stl.fontArialBold().bold().setFontSize(7)).setHorizontalAlignment(HorizontalAlignment.CENTER).setVerticalAlignment(VerticalAlignment.MIDDLE);
            if("PDF".equalsIgnoreCase(format)){
                report.title((cmp.subreport(loadObject(resourceAsStream( ReportUtil.CABECALHO_REL_ACOMPANHAMENTO_IF))).setDataSource(new JREmptyDataSource(1)).setParameters(parametros).setMinHeight(100)));
                report.pageFooter(cmp.text("Página").setStyle(boldCenteredStyle), cmp.pageXslashY().setStyle(boldCenteredStyle));
                report.summary(buildSummary(parametros));
           }
           return report;
        };
        
        EnumMimeTypes mimetype = EnumMimeTypes.valueOf(format.toUpperCase());
        
        JasperReportBuilder report = generatorChain(()->defaultInit.init(cb, getInstrumentoDataSource(result)),
                headerResolver);
        FunctionalInterfaces.BinarySupplier exporter = () -> {
            if(EnumMimeTypes.XLS.equals(mimetype)){
                return ReportUtil.xlsxExporter.export(Arrays.asList(report.ignorePagination().toJasperPrint(),
                        gerarPrintAvulso(parametros, ReportUtil.CABECALHO_REL_AGENDA_POSITIVA ),
                        DynamicReports.report().addDetail(buildSummary(parametros)).title(cmp.text("Total")).toJasperPrint()));
            }else {
                return mimetype.exportReport(report).get();
            }
        };
        
        return buildResponse( exporter
                ,mimetype.mimeType);
    }
    
    
    /*
     *   Recupera o datasource do instrumento
     */
    private JRDataSource getInstrumentoDataSource(List<RelatorioAcompanhamentoInstrumento> result) {
        DRDataSource dataSource = new DRDataSource("nomeEmpresa", "tituloInstrumento", "previsaoAssinatura", "tipo", "local", "atividade", "dataInicioAtividade", "analista", "gerente");
        result.stream().forEach((to) -> {
            dataSource.add(getLinhaInstrumento(to));
        });
        return dataSource;
    }

    private String[] getLinhaInstrumento(RelatorioAcompanhamentoInstrumento to) {
        String[] linha = {to.getEmpresa(), to.getTitulo(), fmtDate(to.getPrevisaoAssinatura()), to.getTipo(), to.getLocalAtividade(), to.getAtividade(), fmtDate(to.getDataInicioAtividade()), to.getAnalista(), to.getGerencia()};
        return linha;
    }
    
    @Override
    protected String fmtDate(Date date) {
        return DateUtils.fmtString(date, DateUtils.ddMMyyyy);
    }       
    
    private ComponentBuilder buildSummary(Map<String,Object> parametros){
         DecimalFormat df = new DecimalFormat();
        df.setGroupingUsed(true);df.setMaximumFractionDigits(0);df.setMinimumFractionDigits(0);
        return cmp.horizontalList(cmp.text(parametros.get(ReportUtil.INFO_RESPONSAVEL).toString()).setHorizontalAlignment(HorizontalAlignment.RIGHT))
                        .setBaseStyle(stl.style(stl.fontArial().setFontSize(9)));
    }
}