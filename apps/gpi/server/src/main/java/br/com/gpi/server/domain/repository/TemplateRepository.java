package br.com.gpi.server.domain.repository;

import br.com.gpi.server.domain.entity.Template;
import br.com.gpi.server.domain.entity.Template_;
import br.com.gpi.server.domain.entity.enumerated.EnumTipoTemplate;
import java.util.List;
import org.apache.deltaspike.data.api.EntityRepository;
import org.apache.deltaspike.data.api.Modifying;
import org.apache.deltaspike.data.api.Query;
import org.apache.deltaspike.data.api.Repository;
import org.apache.deltaspike.data.api.criteria.Criteria;
import org.apache.deltaspike.data.api.criteria.CriteriaSupport;

@Repository(forEntity = Template.class)
public abstract class TemplateRepository implements CriteriaSupport<Template>, EntityRepository<Template, Long>{

    
    public List<Template> findByFilters(String nome, EnumTipoTemplate tipo, Boolean status) {
        Criteria<Template, Template> criteria = criteria();

        if (nome != null) {
            criteria = criteria.like(Template_.nome, "%" + nome + "%");
        }

        if (tipo != null) {
            criteria = criteria.eq(Template_.tipo, tipo);
        }

        if (status != null) {
            criteria = criteria.eq(Template_.status, status);
        }

        return criteria.getResultList();
    }    
    
    @Query(value = "DELETE FROM Template t WHERE t.id IN ?1")
    @Modifying
    public abstract void deleteById(List<Long> ids);
    
    
    @Query(named = Template.Query.findAllByStatusAndTipo)
    public abstract List<Template> findAllByStatusAndTipo(Boolean status, EnumTipoTemplate tipo);
    
}
