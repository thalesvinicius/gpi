package br.com.gpi.server.querybuilder;

public enum ConditionComparisonTypeEnum {

    EQUALS(1, " = {value}"), LIKE(2, " like '%{value}%'");
    private final Integer id;
    private final String descricao;

    ConditionComparisonTypeEnum(Integer id, String descricao) {
        this.id = id;
        this.descricao = descricao;
    }

    public Integer getId() {
        return id;
    }

    public String getDescricao() {
        return descricao;
    }
    
}
