/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.gpi.server.domain.service.reports.nt;

import br.com.gpi.server.domain.entity.Emprego;
import br.com.gpi.server.domain.entity.FaturamentoPrevisto;
import br.com.gpi.server.domain.entity.InstrumentoFormalizacao;
import br.com.gpi.server.domain.entity.InvestimentoPrevisto;
import br.com.gpi.server.domain.service.InstrumentoFormalizacaoService;
import br.com.gpi.server.domain.service.reports.BaseReportService;
import br.com.gpi.server.util.reports.ReportUtil;
import java.io.ByteArrayOutputStream;
import java.util.Collections;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.ooxml.JRDocxExporter;

/**
 *
 * @author rafaelbfs
 */
public class NotaTecnicaReportService extends 
        BaseReportService<InstrumentoFormalizacao> {
    
    private InstrumentoFormalizacaoService instrumentoFormalizacaoService;

    @Inject
    public NotaTecnicaReportService(InstrumentoFormalizacaoService instrumentoFormalizacaoService, EntityManager em) {
        super(em);
        this.instrumentoFormalizacaoService = instrumentoFormalizacaoService;
    }
    
    private Map<String,Object> subreports(){
        Map<String, Object> map = ReportUtil.subReportsMapLogoExpr.
                apply(defaultStyle(),getINDILogo());
        map.put("NT-Projetos",loadObject(resourceAsStream("/jasper/NotaTecnicaProjeto.jasper")));
        return map;
    }
    
    public byte[] gerarNotaTecnica(Long idInstrFormalizacao) throws Exception{
        InstrumentoFormalizacao instrum = instrumentoFormalizacaoService.findById(idInstrFormalizacao);
        
        instrum.getProjetos().parallelStream().forEach((pro) -> {
                Stream<Emprego> empregos = pro.getEmpregos().stream().
                        filter((e0)-> ReportUtil.EMPREGOS_PERMANENTES.contains(e0.getTipoEmprego())).
                        collect(Collectors.groupingBy(
                                (emprego)-> emprego.getAno()))
                        .values().stream().map((emps)->{
                            Emprego emp = new Emprego();
                            emp.setAno(emps.stream().mapToInt(Emprego::getAno).max().getAsInt());
                            emp.setDireto(emps.stream().mapToInt(Emprego::getDireto).sum());
                            emp.setIndireto(emps.stream().mapToInt(Emprego::getIndireto).sum());
                            return emp;
                        });
                        TreeSet<Emprego> emps = new TreeSet<>((e1,e2)->Integer.compare(e1.getAno(),e2.getAno()));
                        emps.addAll(empregos.collect(Collectors.toSet()));
                        pro.setEmpregos(emps);
                if(pro.getFinanceiro()!= null){
                    
                    InvestimentoPrevisto ip = new InvestimentoPrevisto();
                    ip.setAno(pro.getFinanceiro().getInvestimentoPrevistoList().stream().mapToInt(InvestimentoPrevisto::getAno).max().orElse(9999));
                    ip.setValor(pro.getFinanceiro().getInvestimentoPrevistoList().stream().mapToDouble(InvestimentoPrevisto::getValor).sum());
                    pro.getFinanceiro().setInvestimentoPrevistoList(Collections.singleton(ip));
                
                    Map<Integer, Double> map = pro.getFinanceiro().getFaturamentoPrevistoList().stream().collect(Collectors.groupingBy(FaturamentoPrevisto::getAno, Collectors.summingDouble(FaturamentoPrevisto::getValor)));
                    SortedSet<FaturamentoPrevisto> set = new TreeSet<>((f1,f2)->Integer.compare(f1.getAno(),f2.getAno()));
                    set.addAll(map.entrySet().stream().map((entry)-> new FaturamentoPrevisto(entry.getKey(),entry.getValue())).collect(Collectors.toList()));
                    pro.getFinanceiro().setFaturamentoPrevistoList(set);
                }
            
        });
        
        
        JasperPrint jp = super.generateReport(ReportUtil.paramsExpr.init(subreports()),
                resourceAsStream("/jasper/NotaTecnica.jasper"),
        ()-> new JRBeanCollectionDataSource(Collections.singleton(instrum)));
        
        Supplier<ByteArrayOutputStream> bytes =  () -> {
            try {
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                JRDocxExporter docxEx = new JRDocxExporter();
                docxEx.setParameter(JRExporterParameter.OUTPUT_STREAM, baos);
                docxEx.setParameter(JRExporterParameter.JASPER_PRINT, jp);
                docxEx.exportReport();
                return baos;
            } catch (Exception ex) {
                throw new RuntimeException("Erro ao exportar Nota Técnica para DOCX", ex);
            }
        };
        
        return bytes.get().toByteArray();
        
    }
    
    
    
    
    
    
    
    
    
}
