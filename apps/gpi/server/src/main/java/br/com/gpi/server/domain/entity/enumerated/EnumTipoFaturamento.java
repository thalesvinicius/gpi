package br.com.gpi.server.domain.entity.enumerated;

import br.com.gpi.server.core.exception.EnumException;
import br.com.gpi.server.core.jpa.typesafe.EnumerationEntityType;

public enum EnumTipoFaturamento implements EnumerationEntityType<Integer> {

    INVALID(0, "invalid"),  
    FATURAMENTO_DO_GRUPO(1, "Faturamento do Grupo"),
    FATURAMENTO_REALIZADO_EM_MG(2, "Faturamento Realizado em Minas Gerais"),
    FATURAMENTO_PREVISTO(3, "Faturamento"),
    FATURAMENTO_PRODUTOS_INDUSTRIALIZADOS(4, "Produtos Industrializados"),
    FATURAMENTO_PRODUTOS_ADQ_PARA_COMERCIALIZACAO(5, "Produtos Adquiridos Somente para Comercialização"),
    FATURAMENTO_TOTAL_REALIZADO(6, "Faturamento total realizado");


    private final int id;
    private final String descricao;

    private EnumTipoFaturamento(int id, String descricao) {
        this.id = id;
        this.descricao = descricao;
    }

    @Override
    public Integer getId() {
        return this.id;
    }

    @Override
    public String getDescription() {
        return this.descricao;
    }
    
    public static EnumTipoFaturamento getEnumTipoFaturamento(Integer id) {
        for (EnumTipoFaturamento tipoFaturamento : values()) {
            if (tipoFaturamento.id == id) {
                return tipoFaturamento;
            }
        }
        throw new EnumException("EnumTipoFaturamento couldn't load.");
    }    
}
