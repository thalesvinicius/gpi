package br.com.gpi.server.domain.entity;

import br.com.gpi.server.core.exception.EnumException;
import br.com.gpi.server.core.jpa.typesafe.EnumerationEntityType;

public enum SituacaoInstrFormalizacao implements EnumerationEntityType<Integer> {

    ASSINADO(1, "Assinado"),
    CANCELADO(2, "Cancelado"),
    EM_NEGOCIACAO(3, "Em Negociação"),
    EXTINTO(4, "Extinto");

    private final Integer id;
    private final String descricao;

    private SituacaoInstrFormalizacao(Integer id, String descricao) {
        this.id = id;
        this.descricao = descricao;
    }

    public static SituacaoInstrFormalizacao getSituacaoInstrFormalizacao(Integer id) {
        for (SituacaoInstrFormalizacao sit : values()) {
            if (sit.getId().equals(id)) {
                return sit;
            }
        }
        throw new EnumException(String.format("SituacaoInstrFormalizacao not recoginezed value %d ", id));
    }

    @Override
    public Integer getId() {
        return this.id;
    }

    @Override
    public String getDescription() {
        return this.descricao;
    }
}
