package br.com.gpi.server.domain.entity;

import br.com.gpi.server.domain.common.BaseEntity;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Date;
import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import org.hibernate.envers.Audited;

/**
 * A token that gives the user permission to carry out a specific task once
 * within a determined time period. An example would be a Lost Password token.
 * The user receives the token embedded in a link. They send the token back to
 * the server by clicking the link and the action is processed
 *
 * @version 1.0
 * @author <a href="mailto:pviniciusfm@gmail.com">Paulo Vinicius F. Machado</a>
 */
@Entity
@Audited
@Table(name = "VerificationToken")
@NamedQueries({
    @NamedQuery(name = VerificationToken.Query.findByToken, query = "select t from VerificationToken t where t.token = ?1")
})
public class VerificationToken extends BaseEntity<Long> {

    private static final int DEFAULT_EXPIRY_TIME_IN_MINS = 60 * 24; //24 hours
    private static final long serialVersionUID = 1L;

    @Column(length = 36)
    private final String token;

    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date expiryDate;

    @Enumerated(EnumType.STRING)
    private VerificationTokenType tokenType;

    private boolean verified;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    public VerificationToken() {
        super();
        this.token = UUID.randomUUID().toString();
        this.expiryDate = calculateExpiryDate(DEFAULT_EXPIRY_TIME_IN_MINS);
    }

    public VerificationToken(User user, VerificationTokenType tokenType, int expirationTimeInMinutes) {
        this();
        this.user = user;
        this.tokenType = tokenType;
        this.expiryDate = calculateExpiryDate(expirationTimeInMinutes);
    }

    public String getToken() {
        return token;
    }

    public LocalDateTime getExpiryDate() {
         if(expiryDate == null){
            return null;
        }
        Instant instant = Instant.ofEpochMilli(expiryDate.getTime());
        return LocalDateTime.ofInstant(instant, ZoneOffset.UTC);
    }

    public void setExpiryDate(LocalDateTime expiryDate) {
         if (expiryDate == null){
            this.expiryDate = null;
        }else{
            Instant instant = expiryDate.toInstant(ZoneOffset.UTC);
            this.expiryDate = Date.from(instant);
        }
    }

    public VerificationTokenType getTokenType() {
        return tokenType;
    }

    public void setTokenType(VerificationTokenType tokenType) {
        this.tokenType = tokenType;
    }

    public boolean isVerified() {
        return verified;
    }

    public void setVerified(boolean verified) {
        this.verified = verified;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    private Date calculateExpiryDate(int expiryTimeInMinutes) {
        LocalDateTime dt = LocalDateTime.now().plusMinutes(expiryTimeInMinutes);
        Instant instant = dt.toInstant(ZoneOffset.UTC);
        return Date.from(instant);
    }

    public static class Query {

        public static final String findByToken = "VerificationToken.findByToken";
    }

    public enum VerificationTokenType {

        lostPassword, emailVerification
    }

    public boolean hasExpired() {
        return expiryDate.before(new Date());
    }
}
