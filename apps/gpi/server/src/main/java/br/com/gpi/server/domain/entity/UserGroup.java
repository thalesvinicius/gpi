package br.com.gpi.server.domain.entity;

import br.com.gpi.server.core.exception.EnumException;

public enum UserGroup {

    USER(1, "Usuario"), ADMINISTRATOR(2, "Administrador");
    private final int id;
    private final String descricao;

    private UserGroup(int id, String descricao) {
        this.id = id;
        this.descricao = descricao;
    }

    public int getId() {
        return id;
    }

    public String getDescricao() {
        return descricao;
    }

    public static UserGroup getUserGroup(int value) throws EnumException {
        for (UserGroup userGroup : values()) {
            if (userGroup.getId() == value) {
                return userGroup;
            }
        }
        throw new EnumException("UserGroup not found!");
    }
}
