package br.com.gpi.server.domain.service;

import br.com.gpi.server.domain.common.BaseService;
import br.com.gpi.server.domain.entity.Servico;
import java.util.List;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

public class ServicoService extends BaseService<Servico> {

    @Inject
    public ServicoService(EntityManager em) {
        super(Servico.class, em);
    }

    public List<Servico> findAllServicoByInsumoProduto(List<Long> ids_insumoProduto) {
        StringBuilder builder = new StringBuilder("SELECT p FROM Servico p ");
        builder.append(" WHERE p.insumoProduto.id in (:ids_insumoProduto) ");
        
        TypedQuery<Servico> query = getEm().createQuery(builder.toString(), Servico.class);
        query.setParameter("ids_insumoProduto", ids_insumoProduto);
        return query.getResultList();
    }    
    
}
