package br.com.gpi.server.endpoint;

import br.com.gpi.server.domain.common.QueryParameter;
import br.com.gpi.server.domain.entity.Empresa;
import br.com.gpi.server.domain.entity.SituacaoEmpresa;
import br.com.gpi.server.domain.repository.EmpresaRepository;
import br.com.gpi.server.domain.repository.UnidadeEmpresaRepository;
import br.com.gpi.server.domain.service.EmpresaService;
import br.com.gpi.server.domain.user.model.SecurityUser;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.inject.Inject;
import javax.transaction.NotSupportedException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;
import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

@Path("/empresa")
@Stateless
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
@TransactionManagement(TransactionManagementType.BEAN)
public class EmpresaEndpoint {

    @Inject
    private EmpresaService empresaService;

    @Inject
    private EmpresaRepository empresaRepository;

    @Inject
    private UnidadeEmpresaRepository unidadeEmpresaRepository;
    
    @Inject
    private UserTransaction tx;

    @GET
    public Response findByFilters(@QueryParam("nomeEmpresa") String nomeEmpresa,
            @QueryParam("cnpj") String cnpj,
            @QueryParam("cadeiaProdutiva") Integer cadeiaProdutivaId,
            @QueryParam("situacaoEmpresa") SituacaoEmpresa situacaoEmpresa,
            @QueryParam("pageSize") Integer pageSize,
            @QueryParam("page") Integer page,
            @QueryParam("countResults") Boolean countBoolean,
            @QueryParam("municipioOuCidade") String municipioOuCidade,
            @QueryParam("estado") String estado,
            @QueryParam("pais") String pais
    ) throws Exception {

        QueryParameter queryParam = QueryParameter.with("cnpj", cnpj)
                .and("cadeiaProdutiva", cadeiaProdutivaId)                
                .and("nomeEmpresa", nomeEmpresa)
                .and("municipioOuCidade", municipioOuCidade)
                .and("estado", estado)
                .and("pais", pais);

        if(situacaoEmpresa != null){
            queryParam.and("situacaoEmpresa", situacaoEmpresa);
        }
        if (page != null) {
            queryParam.paginate(pageSize, page);
        }

        return Response.ok(empresaService.findByFilters(countBoolean, queryParam)).build();
    }

    @GET
    @Path("{id}")
    public Response findById(@PathParam("id") Long id) {
        Empresa result = empresaService.findByIdForEdition(id);
        return Response.ok(result).build();
    }

    @POST
    public Response save(@Valid Empresa empresa, @Context SecurityContext securityContext) {
        try {
            Response response;
            SecurityUser loggedUser = (SecurityUser) securityContext.getUserPrincipal();
            Long id = empresaService.saveEmpresa(empresa, loggedUser.getId());
            response = Response.ok(id).build();
            return response;
        } catch (SystemException ex) {
            Logger.getLogger(EmpresaEndpoint.class.getName()).log(Level.SEVERE, null, ex);
            return Response.serverError().build();
        }
    }

    @POST
    @Path("savePrimeiroContato")
    public Response savePrimeiroContato(Empresa empresa, @Context SecurityContext securityContext) {
        try {
            SecurityUser loggedUser = (SecurityUser) securityContext.getUserPrincipal();
            Long id = empresaService.savePrimeiroContato(empresa, loggedUser);
            return Response.ok(id).build();
        } catch (Exception ex) {
            Logger.getLogger(EmpresaEndpoint.class.getName()).log(Level.SEVERE, null, ex);
            return Response.serverError().entity(ex).build();
        }
    }

    @PUT
    @Path("{id}")
    public Response edit(@Valid Empresa empresa, @Context SecurityContext securityContext) {
        return this.save(empresa, securityContext);
    }

    @POST
    @Path("removeEmpresa")
    public Response removeEmpresa(List<Long> ids) {
        try {
            empresaService.removeEmpresa(ids);
            return Response.ok("Tudo Ok!").build();
        } catch (SystemException ex) {
            Logger.getLogger(EmpresaEndpoint.class.getName()).log(Level.SEVERE, null, ex);
            throw new RuntimeException(ex);
        }
    }

    @POST
    @Path("validarCadastro")
    public Response validarCadastro(Long empresaId) throws NotSupportedException {
        try {
            return Response.ok(empresaService.validarCadastro(empresaId)).build();
        } catch (SystemException ex) {
            Logger.getLogger(EmpresaEndpoint.class.getName()).log(Level.SEVERE, null, ex);
            throw new RuntimeException(ex);
        }
            
    }

    @GET
    @Path("allEmpresaUnidadeDTOAtivas")
    public Response findAllEmpresaUnidadeDTOAtivas(@QueryParam("nomeEmpresa") String searchNomeEmpresa) {
        return Response.ok(empresaService.findEmpresaUnidadeDTOAtivas(null, searchNomeEmpresa)).build();
    }

    @GET
    @Path("{id}/empresaUnidadeDTOAtivas")
    public Response findEmpresaUnidadeDTOAtivasById(@PathParam("id") Long idEmpresa, @QueryParam("nomeEmpresa") String searchNomeEmpresa) {
        return Response.ok(empresaService.findEmpresaUnidadeDTOAtivas(idEmpresa, searchNomeEmpresa)).build();
    }
}
