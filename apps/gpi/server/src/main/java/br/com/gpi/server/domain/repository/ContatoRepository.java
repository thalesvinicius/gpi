package br.com.gpi.server.domain.repository;

import br.com.gpi.server.domain.entity.Contato;
import java.util.List;
import org.apache.deltaspike.data.api.EntityRepository;
import org.apache.deltaspike.data.api.Modifying;
import org.apache.deltaspike.data.api.Query;
import org.apache.deltaspike.data.api.Repository;
import org.apache.deltaspike.data.api.criteria.CriteriaSupport;

@Repository(forEntity = Contato.class)
public abstract class ContatoRepository implements CriteriaSupport<Contato>,EntityRepository<Contato, Long> {

    @Query(named=Contato.Query.locateByCompany)
    public abstract List<Contato> findByCompany(Long id);
    
    @Modifying
    @Query(named = Contato.Query.deleteByid)
    public abstract void deleteById(List<Long> id);
}
