package br.com.gpi.server.querybuilder;

public class ChainedTableLink extends TableLink {

    private Table toTable2;
    private String fkName2;

    public ChainedTableLink(Table table1, String fkName1, Table table2, String fkName2) {
        super(table1, fkName1);
        
        this.toTable2 = table2;
        this.fkName2 = fkName2;

    }

    @Override
    public String createLinkStatement(Table fromTable) {
        StringBuilder result = new StringBuilder();

        result.append(" INNER JOIN ");
        result.append(toTable2.getName());
        result.append(" ");
        result.append(toTable2.getAlias());
        result.append(" ON ");
        result.append(toTable2.getAlias()).append(".").append(fkName2);
        result.append(" = ");
        result.append(fromTable.getAlias()).append(".").append(fromTable.getPkName());
        result.append(" INNER JOIN ");
        result.append(getToTable().getName());
        result.append(" ");
        result.append(getToTable().getAlias());
        result.append(" ON ");
        result.append(toTable2.getAlias()).append(".").append(getFkName());
        result.append(" = ");
        result.append(getToTable().getAlias()).append(".").append(getToTable().getPkName());

        return result.toString();
    }

    public Table getToTable2() {
        return toTable2;
    }

    public void setToTable2(Table toTable2) {
        this.toTable2 = toTable2;
    }

    public String getFkName2() {
        return fkName2;
    }

    public void setFkName2(String fkName2) {
        this.fkName2 = fkName2;
    }

}
