package br.com.gpi.server.domain.entity;

import br.com.gpi.server.domain.common.BaseEntity;
import com.fasterxml.jackson.annotation.JsonIgnore;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.envers.Audited;

/**
 *
 * @author thalesvd
 */
@Audited
@Entity
@Table(name = "ComposicaoSocietaria")
public class ComposicaoSocietaria extends BaseEntity<Long> {
    
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "empresa_id")
    private Empresa empresa;
    
    @Column(name = "socio", length = 50)
    private String socio;
    
    @Column(name = "empresa_grupo", length = 100)
    private String empresa_grupo;

    @Column(name = "participacao_societaria")
    private Double participacao_societaria;

    @Column(name = "cpf_cnpj", length = 20)
    private String cpf_cnpj;

    public Empresa getEmpresa() {
        return empresa;
    }

    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }

    public String getSocio() {
        return socio;
    }

    public void setSocio(String socio) {
        this.socio = socio;
    }

    public String getEmpresa_grupo() {
        return empresa_grupo;
    }

    public void setEmpresa_grupo(String empresa_grupo) {
        this.empresa_grupo = empresa_grupo;
    }

    public Double getParticipacao_societaria() {
        return participacao_societaria;
    }

    public void setParticipacao_societaria(Double participacao_societaria) {
        this.participacao_societaria = participacao_societaria;
    }

    public String getCpf_cnpj() {
        return cpf_cnpj;
    }

    public void setCpf_cnpj(String cpf_cnpj) {
        this.cpf_cnpj = cpf_cnpj;
    }
    
}
