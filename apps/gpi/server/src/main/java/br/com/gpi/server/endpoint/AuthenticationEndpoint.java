package br.com.gpi.server.endpoint;

import br.com.gpi.server.config.ApplicationSettings;
import br.com.gpi.server.domain.entity.TipoUsuario;
import br.com.gpi.server.domain.entity.User;
import br.com.gpi.server.domain.entity.UsuarioExterno;
import br.com.gpi.server.domain.repository.UserExternalRepository;
import br.com.gpi.server.domain.service.UserService;
import br.com.gpi.server.domain.service.VerificationTokenService;
import br.com.gpi.server.domain.user.model.ChangePasswordRequest;
import br.com.gpi.server.domain.user.model.LostPasswordRequest;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import org.apache.deltaspike.core.api.projectstage.ProjectStage;
import org.apache.oltu.oauth2.as.issuer.MD5Generator;
import org.apache.oltu.oauth2.as.issuer.OAuthIssuer;
import org.apache.oltu.oauth2.as.issuer.OAuthIssuerImpl;
import org.apache.oltu.oauth2.as.request.OAuthAuthzRequest;
import org.apache.oltu.oauth2.as.request.OAuthTokenRequest;
import org.apache.oltu.oauth2.as.response.OAuthASResponse;
import org.apache.oltu.oauth2.common.OAuth;
import org.apache.oltu.oauth2.common.error.OAuthError;
import org.apache.oltu.oauth2.common.exception.OAuthProblemException;
import org.apache.oltu.oauth2.common.exception.OAuthSystemException;
import org.apache.oltu.oauth2.common.message.OAuthResponse;
import org.apache.oltu.oauth2.common.message.types.GrantType;
import org.apache.oltu.oauth2.common.message.types.ResponseType;
import org.apache.oltu.oauth2.common.utils.OAuthUtils;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

@Path("/oauth")
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class AuthenticationEndpoint {

    public static final String INVALID_CLIENT_DESCRIPTION = "Client authentication failed (e.g., unknown client, no client authentication included, or unsupported authentication method).";

    @Inject
    private ApplicationSettings config;
    @Context
    private HttpHeaders httpHeaders;
    @Context
    private UriInfo uriInfo;
    @Inject
    private UserService userService;
    @Inject
    private VerificationTokenService verificationTokenService;
    @Inject
    private UserTransaction userTransaction;
    @Inject
    private Logger logger;
    @Inject
    private UserExternalRepository userExternalRepository;

    @Inject
    private ProjectStage stage;

    @GET
    @Path("auth")
    public Response authorize(@Context HttpServletRequest request)
            throws URISyntaxException, OAuthSystemException {
        try {
            OAuthAuthzRequest oauthRequest = new OAuthAuthzRequest(request);
            OAuthIssuerImpl oauthIssuerImpl = new OAuthIssuerImpl(new MD5Generator());

            //build response according to response_type
            String responseType = oauthRequest.getParam(OAuth.OAUTH_RESPONSE_TYPE);

            OAuthASResponse.OAuthAuthorizationResponseBuilder builder
                    = OAuthASResponse.authorizationResponse(request, HttpServletResponse.SC_FOUND);

            if (responseType.equals(ResponseType.CODE.toString())) {
                final String authorizationCode = oauthIssuerImpl.authorizationCode();
                builder.setCode(authorizationCode);
            }
            if (responseType.equals(ResponseType.TOKEN.toString())) {
                final String accessToken = oauthIssuerImpl.accessToken();
                builder.setAccessToken(accessToken);
                builder.setExpiresIn(3600l);
            }

            String redirectURI = oauthRequest.getParam(OAuth.OAUTH_REDIRECT_URI);
            final OAuthResponse response = builder.location(redirectURI).buildQueryMessage();
            URI url = new URI(response.getLocationUri());
            return Response.status(response.getResponseStatus()).location(url).build();
        } catch (OAuthProblemException e) {
            final Response.ResponseBuilder responseBuilder = Response.status(HttpServletResponse.SC_FOUND);
            String redirectUri = e.getRedirectUri();

            if (OAuthUtils.isEmpty(redirectUri)) {
                throw new WebApplicationException(
                        responseBuilder.entity("OAuth callback url needs to be provided by client!!!").build());
            }
            final OAuthResponse response
                    = OAuthASResponse.errorResponse(HttpServletResponse.SC_FOUND)
                    .error(e).location(redirectUri).buildQueryMessage();
            final URI location = new URI(response.getLocationUri());
            return responseBuilder.location(location).build();
        }
    }

    @POST
    @Path("token")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public Response token(@Context HttpServletRequest request) throws OAuthSystemException {
        try {
            OAuthTokenRequest oauthRequest = new OAuthTokenRequest(request);
            OAuthIssuer oauthIssuerImpl = new OAuthIssuerImpl(new MD5Generator());

            //For actual implementation we don't validate the client_id and client_secret
            if (!checkClientCredentials(oauthRequest.getClientId(), oauthRequest.getClientSecret())) {
                return buildInvalidClientIdResponse();
            }

            if (oauthRequest.getParam(OAuth.OAUTH_GRANT_TYPE).equals(GrantType.PASSWORD.toString())) {
                User user;
                boolean ldapAuthenticated = false;
                if ("externo".equalsIgnoreCase(oauthRequest.getParam("environment"))) {
                    user = userService.findExternalByLogin(oauthRequest.getUsername());
                } else {
                    ldapAuthenticated = tryToAuthenticateIntoLDAP(request, oauthRequest.getUsername(), oauthRequest.getPassword());
                    user = userService.findInternalByLogin(oauthRequest.getUsername());
                }

                if (user == null) {
                    return buildInvalidUserResponse();
                }

                Response credentialsIssues = checkCredentials(user, oauthRequest.getPassword(), ldapAuthenticated);
                if (credentialsIssues != null) {
                    return credentialsIssues;
                }

                final String accessToken = oauthIssuerImpl.accessToken();
                userService.createNewSession(user, accessToken);
                OAuthResponse response = OAuthASResponse
                        .tokenResponse(HttpServletResponse.SC_OK)
                        .setAccessToken(accessToken)
                        .setExpiresIn("3600")
                        .buildJSONMessage();
                return Response.status(response.getResponseStatus()).entity(response.getBody()).build();

            } else {
                return buildInvalidGrantType();
            }

        } catch (OAuthProblemException e) {
            OAuthResponse res = OAuthASResponse.errorResponse(HttpServletResponse.SC_BAD_REQUEST).error(e)
                    .buildJSONMessage();
            return Response.status(res.getResponseStatus()).entity(res.getBody()).build();
        }
    }

    @POST
    @Path("reset/password")
    @Produces(MediaType.TEXT_PLAIN)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response resetPassword(ChangePasswordRequest changePasswordRequest) {
        try {
            verificationTokenService.resetPassword(changePasswordRequest);
            return Response.ok().build();
        } catch (SystemException ex) {
            Logger.getLogger(AuthenticationEndpoint.class.getName()).log(Level.SEVERE, null, ex);
            throw new RuntimeException(ex);
        }
    }

    @POST
    @Path("reset/request")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public Response lostPasswordRequest(LostPasswordRequest lostPasswordRequest) {
        try {
            verificationTokenService.sendLostPasswordToken(lostPasswordRequest);
            return Response.ok().build();
        } catch (SystemException ex) {
            Logger.getLogger(AuthenticationEndpoint.class.getName()).log(Level.SEVERE, null, ex);
            throw new RuntimeException(ex);
        }
    }

    @GET
    @Path("debug")
    public String debugSecurity() {
        JSONObject object = new JSONObject();
        JSONObject headers = new JSONObject();
        JSONObject qp = new JSONObject();
        String json = "error!";
        try {
            for (Map.Entry<String, List<String>> entry : httpHeaders.getRequestHeaders().entrySet()) {
                headers.put(entry.getKey(), entry.getValue().get(0));
            }
            object.put("headers", headers);
            for (Map.Entry<String, List<String>> entry : uriInfo.getQueryParameters().entrySet()) {
                qp.put(entry.getKey(), entry.getValue().get(0));
            }
            object.put("queryParameters", qp);
            json = object.toString(4);
        } catch (JSONException ex) {
            Logger.getLogger(AuthenticationEndpoint.class.getName()).log(Level.SEVERE, null, ex);
        }
        return json;
    }

    private Response buildInvalidClientIdResponse() throws OAuthSystemException {
        OAuthResponse response
                = OAuthASResponse.errorResponse(HttpServletResponse.SC_BAD_REQUEST)
                .setError(OAuthError.TokenResponse.INVALID_CLIENT)
                .setErrorDescription(INVALID_CLIENT_DESCRIPTION)
                .buildJSONMessage();
        return Response.status(response.getResponseStatus()).entity(response.getBody()).build();
    }

    private Response buildInvalidUserPassResponse() throws OAuthSystemException {
        OAuthResponse response = OAuthASResponse
                .errorResponse(HttpServletResponse.SC_BAD_REQUEST)
                .setError(OAuthError.TokenResponse.UNAUTHORIZED_CLIENT)
                .setErrorDescription("message.error.MSG002_LOGIN_SENHA_INVALIDOS")
                .buildJSONMessage();
        return Response.status(response.getResponseStatus()).entity(response.getBody()).build();
    }

    private Response buildUserNotActivatedResponse() throws OAuthSystemException {
        OAuthResponse response = OAuthASResponse
                .errorResponse(HttpServletResponse.SC_BAD_REQUEST)
                .setError(OAuthError.TokenResponse.UNAUTHORIZED_CLIENT)
                .setErrorDescription("message.error.MSG76_PERMISSAO_ACESSO_NEGADO")
                .buildJSONMessage();
        return Response.status(response.getResponseStatus()).entity(response.getBody()).build();
    }

    private Response buildExternalUserTemporaryBlockResponse() throws OAuthSystemException {
        OAuthResponse response = OAuthASResponse
                .errorResponse(HttpServletResponse.SC_BAD_REQUEST)
                .setError(OAuthError.TokenResponse.UNAUTHORIZED_CLIENT)
                .setErrorDescription("message.error.MSG75_FALHA_AUTENTICACAO")
                .buildJSONMessage();
        return Response.status(response.getResponseStatus()).entity(response.getBody()).build();
    }

    private Response buildInvalidUserResponse() throws OAuthSystemException {
        OAuthResponse response = OAuthASResponse
                .errorResponse(HttpServletResponse.SC_BAD_REQUEST)
                .setError(OAuthError.TokenResponse.UNAUTHORIZED_CLIENT)
                .setErrorDescription("message.error.MSG66_LOGIN_INVALIDO")
                .buildJSONMessage();
        return Response.status(response.getResponseStatus()).entity(response.getBody()).build();
    }

    private boolean checkClientCredentials(String clientId, String secret) {
        return true;
    }

    private Response buildInvalidGrantType() {
        return Response.status(HttpServletResponse.SC_BAD_REQUEST).entity("This grant type is not supported by this oauth provider.").build();
    }

    private boolean tryToAuthenticateIntoLDAP(HttpServletRequest request, String username, String password) {
        try {
            if (request.getUserPrincipal() != null) {
                request.logout();
            }
            request.login(username, password);
            return true;
        } catch (ServletException ex) {
            logger.log(Level.SEVERE, ex.getMessage(), ex);
            return false;
        }
    }

    /**
     *
     * @param locatedUser
     * @param password
     * @param isLDAP
     * @return null if not any issues about the authentication is found
     * @throws OAuthSystemException
     */
    private Response checkCredentials(User locatedUser, String password, boolean isLDAP) throws OAuthSystemException {
        if (locatedUser != null) {
            if (locatedUser.getTipo() != null && locatedUser.getTipo().equals(TipoUsuario.EXTERNO.getId())) {
                UsuarioExterno usuarioExterno = (UsuarioExterno) userExternalRepository.findBy(locatedUser.getId());
                if (usuarioExterno.getDataBloqueioTemporario() != null
                        && usuarioExterno.getDataBloqueioTemporario().after(new Date())) {
                    return buildExternalUserTemporaryBlockResponse();
                }
            }

            if (!locatedUser.isAtivo()) {
                return buildUserNotActivatedResponse();
            }

            if (!isLDAP && userService.checkUserCredentials(locatedUser, password) && locatedUser.getTipo().equals(TipoUsuario.EXTERNO.getId())) {
//                if(stage.equals(ProjectStage.Development) && password.equals("algar123")) {
                return null;
            } else if (!isLDAP) {
                return buildInvalidUserPassResponse();
            }
        } else {
            return buildInvalidUserPassResponse();
        }

        return null;
    }

    @POST
    @Path("blockExternalUser")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public Response setExternalUserTemporaryBlock(String login) {
        if (login != null) {
            List<UsuarioExterno> usuarios = userExternalRepository.locateByLogin2(login);
            if (!usuarios.isEmpty()) {
                UsuarioExterno usuarioABloquear = usuarios.get(0);
                LocalDateTime daquiAVinteMinutos = LocalDateTime.now().plusMinutes(20);
                usuarioABloquear.setDataBloqueioTemporario(Date.from(daquiAVinteMinutos.atZone(ZoneId.systemDefault()).toInstant()));
                userExternalRepository.save(usuarioABloquear);
                return Response.ok().build();
            }
        }
        return Response.noContent().build();
    }

}
