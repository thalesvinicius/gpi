package br.com.gpi.server.domain.service;

import br.com.gpi.server.domain.common.BaseService;
import br.com.gpi.server.domain.entity.Estagio;
import br.com.gpi.server.domain.entity.FaturamentoAnterior;
import br.com.gpi.server.domain.entity.FaturamentoAnterior_;
import br.com.gpi.server.domain.entity.FaturamentoPrevisto;
import br.com.gpi.server.domain.entity.FaturamentoPrevisto_;
import br.com.gpi.server.domain.entity.Financeiro;
import br.com.gpi.server.domain.entity.Financeiro_;
import br.com.gpi.server.domain.entity.InvestimentoPrevisto;
import br.com.gpi.server.domain.entity.InvestimentoPrevisto_;
import br.com.gpi.server.domain.entity.Projeto;
import br.com.gpi.server.domain.entity.SituacaoProjetoEnum;
import br.com.gpi.server.domain.entity.User;
import br.com.gpi.server.domain.entity.UsuarioExterno;
import br.com.gpi.server.domain.entity.enumerated.EnumTipoFaturamento;
import br.com.gpi.server.domain.entity.enumerated.EnumTipoInvestimento;
import br.com.gpi.server.domain.repository.FinanceiroRepository;
import br.com.gpi.server.domain.user.model.SecurityUser;
import br.com.gpi.server.to.FaturamentoDoGrupoERealizadoEmMinasGeraisDTO;
import br.com.gpi.server.to.FaturamentoIndustrializadosEAdquiridosParaComercializacaoDTO;
import br.com.gpi.server.to.InvestimentoIndustrialAgricolaCapacitacaoEPropriosDTO;
import br.com.gpi.server.to.InvestimentoMaquinasEquipAndCapitalGiroEOutrosDTO;
import br.com.gpi.server.util.LogUtil;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;
import javax.ws.rs.core.SecurityContext;

public class FinanceiroService extends BaseService<Financeiro> {

    @Inject
    private FinanceiroRepository financeiroRepository;

    @Inject
    private ProjetoService projetoService;
    
    @Inject
    private UserTransaction transaction;

    @Inject
    public FinanceiroService(EntityManager em) {
        super(Financeiro.class, em);
    }

    public Collection<InvestimentoMaquinasEquipAndCapitalGiroEOutrosDTO> findInvestimentoMaquinasEquipAndCapitalGiroEOutrosByFinanceiroId(Long financeiroId) {
        EnumSet<EnumTipoInvestimento> enumList = EnumSet.of(EnumTipoInvestimento.INVESTIMENTO_MAQ_EQUIP_TERRENO_OBRAS, EnumTipoInvestimento.INVESTIMENTO_CAPITAL_GIRO_E_OUTROS);

        CriteriaBuilder builder = getEm().getCriteriaBuilder();
        CriteriaQuery<InvestimentoPrevisto> query = builder.createQuery(InvestimentoPrevisto.class);
        List<Predicate> conditions = new ArrayList();

        Root<InvestimentoPrevisto> fromInvestimento = query.from(InvestimentoPrevisto.class);
        Join<InvestimentoPrevisto, Financeiro> fromFinanceiro = fromInvestimento.join(InvestimentoPrevisto_.financeiro);

        conditions.add(builder.equal(fromFinanceiro.get(Financeiro_.id), financeiroId));
        conditions.add(fromInvestimento.get(InvestimentoPrevisto_.tipoInvestimento).in(enumList));

        TypedQuery<InvestimentoPrevisto> typedQuery = getEm().createQuery(query
                .where(conditions.toArray(new Predicate[]{}))
                .orderBy(builder.asc(fromInvestimento.get(InvestimentoPrevisto_.ano)))
        );
        List<InvestimentoPrevisto> resultList = typedQuery.getResultList();

        Map<Integer, InvestimentoMaquinasEquipAndCapitalGiroEOutrosDTO> mapInvestimentoPrevisto = new HashMap<>();
        for (InvestimentoPrevisto inv : resultList) {
            InvestimentoMaquinasEquipAndCapitalGiroEOutrosDTO investimentoDTO = new InvestimentoMaquinasEquipAndCapitalGiroEOutrosDTO();
            if (mapInvestimentoPrevisto.containsKey(inv.getAno())) {
                investimentoDTO = mapInvestimentoPrevisto.get(inv.getAno());
            } else {
                investimentoDTO = new InvestimentoMaquinasEquipAndCapitalGiroEOutrosDTO();
                mapInvestimentoPrevisto.put(inv.getAno(), investimentoDTO);
            }
            investimentoDTO.setAno(inv.getAno());

            if (inv.getTipoInvestimento().equals(EnumTipoInvestimento.INVESTIMENTO_CAPITAL_GIRO_E_OUTROS)) {
                investimentoDTO.setIdCapitalGiroEOutros(inv.getId());
                investimentoDTO.setValorCapitalGiroEOutros(inv.getValor());
            } else if (inv.getTipoInvestimento().equals(EnumTipoInvestimento.INVESTIMENTO_MAQ_EQUIP_TERRENO_OBRAS)) {
                investimentoDTO.setIdMaquinasEquip(inv.getId());
                investimentoDTO.setValorMaquinasEquip(inv.getValor());
            }
        }
        return mapInvestimentoPrevisto.values();
    }

    public Collection<InvestimentoIndustrialAgricolaCapacitacaoEPropriosDTO> findIndustrialAgricolaCapacitacaoEPropriosByFinanceiroId(Long financeiroId) {
        EnumSet<EnumTipoInvestimento> enumList = EnumSet.of(EnumTipoInvestimento.INVESTIMENTO_INDUSTRIAL, EnumTipoInvestimento.INVESTIMENTO_AGRICOLA, EnumTipoInvestimento.INVESTIMENTO_CAPACITACAO_PROFISSIONAL, EnumTipoInvestimento.INVESTIMENTO_PROPRIOS);

        CriteriaBuilder builder = getEm().getCriteriaBuilder();
        CriteriaQuery<InvestimentoPrevisto> query = builder.createQuery(InvestimentoPrevisto.class);
        List<Predicate> conditions = new ArrayList();

        Root<InvestimentoPrevisto> fromInvestimento = query.from(InvestimentoPrevisto.class);
        Join<InvestimentoPrevisto, Financeiro> fromFinanceiro = fromInvestimento.join(InvestimentoPrevisto_.financeiro);

        conditions.add(builder.equal(fromFinanceiro.get(Financeiro_.id), financeiroId));
        conditions.add(fromInvestimento.get(InvestimentoPrevisto_.tipoInvestimento).in(enumList));

        TypedQuery<InvestimentoPrevisto> typedQuery = getEm().createQuery(query
                .where(conditions.toArray(new Predicate[]{}))
                .orderBy(builder.asc(fromInvestimento.get(InvestimentoPrevisto_.ano)))
        );
        List<InvestimentoPrevisto> resultList = typedQuery.getResultList();

        Map<Integer, InvestimentoIndustrialAgricolaCapacitacaoEPropriosDTO> mapInvestimentoPrevisto = new HashMap<>();
        for (InvestimentoPrevisto inv : resultList) {
            InvestimentoIndustrialAgricolaCapacitacaoEPropriosDTO investimentoDTO = new InvestimentoIndustrialAgricolaCapacitacaoEPropriosDTO();
            if (mapInvestimentoPrevisto.containsKey(inv.getAno())) {
                investimentoDTO = mapInvestimentoPrevisto.get(inv.getAno());
            } else {
                investimentoDTO = new InvestimentoIndustrialAgricolaCapacitacaoEPropriosDTO();
                mapInvestimentoPrevisto.put(inv.getAno(), investimentoDTO);
            }
            investimentoDTO.setAno(inv.getAno());

            if (inv.getTipoInvestimento().equals(EnumTipoInvestimento.INVESTIMENTO_INDUSTRIAL)) {
                investimentoDTO.setIdIndustrial(inv.getId());
                investimentoDTO.setValorIndustrial(inv.getValor());
            } else if (inv.getTipoInvestimento().equals(EnumTipoInvestimento.INVESTIMENTO_AGRICOLA)) {
                investimentoDTO.setIdAgricola(inv.getId());
                investimentoDTO.setValorAgricola(inv.getValor());
            } else if (inv.getTipoInvestimento().equals(EnumTipoInvestimento.INVESTIMENTO_CAPACITACAO_PROFISSIONAL)) {
                investimentoDTO.setIdCapacitacao(inv.getId());
                investimentoDTO.setValorCapacitacao(inv.getValor());
            } else if (inv.getTipoInvestimento().equals(EnumTipoInvestimento.INVESTIMENTO_PROPRIOS)) {
                investimentoDTO.setIdProprios(inv.getId());
                investimentoDTO.setValorProprios(inv.getValor());
            }

        }
        return mapInvestimentoPrevisto.values();
    }

    public Collection<FaturamentoIndustrializadosEAdquiridosParaComercializacaoDTO> findFaturamentoIndustrializadosEAdquiridosParaComercializacaoByFinanceiroId(Long financeiroId) {
        EnumSet<EnumTipoFaturamento> enumList = EnumSet.of(EnumTipoFaturamento.FATURAMENTO_PRODUTOS_INDUSTRIALIZADOS, EnumTipoFaturamento.FATURAMENTO_PRODUTOS_ADQ_PARA_COMERCIALIZACAO);

        CriteriaBuilder builder = getEm().getCriteriaBuilder();
        CriteriaQuery<FaturamentoPrevisto> query = builder.createQuery(FaturamentoPrevisto.class);
        List<Predicate> conditions = new ArrayList();

        Root<FaturamentoPrevisto> fromFaturamento = query.from(FaturamentoPrevisto.class);
        Join<FaturamentoPrevisto, Financeiro> fromFinanceiro = fromFaturamento.join(FaturamentoPrevisto_.financeiro);

        conditions.add(builder.equal(fromFinanceiro.get(Financeiro_.id), financeiroId));
        conditions.add(fromFaturamento.get(FaturamentoPrevisto_.tipoFaturamento).in(enumList));

        TypedQuery<FaturamentoPrevisto> typedQuery = getEm().createQuery(query
                .where(conditions.toArray(new Predicate[]{}))
                .orderBy(builder.asc(fromFaturamento.get(FaturamentoPrevisto_.ano)))
        );
        List<FaturamentoPrevisto> resultList = typedQuery.getResultList();

        Map<Integer, FaturamentoIndustrializadosEAdquiridosParaComercializacaoDTO> mapFaturamentoAnterior = new HashMap<>();
        for (FaturamentoPrevisto fat : resultList) {
            FaturamentoIndustrializadosEAdquiridosParaComercializacaoDTO faturamentoDTO = new FaturamentoIndustrializadosEAdquiridosParaComercializacaoDTO();
            if (mapFaturamentoAnterior.containsKey(fat.getAno())) {
                faturamentoDTO = mapFaturamentoAnterior.get(fat.getAno());
            } else {
                faturamentoDTO = new FaturamentoIndustrializadosEAdquiridosParaComercializacaoDTO();
                mapFaturamentoAnterior.put(fat.getAno(), faturamentoDTO);
            }
            faturamentoDTO.setAno(fat.getAno());

            if (fat.getTipoFaturamento().equals(EnumTipoFaturamento.FATURAMENTO_PRODUTOS_INDUSTRIALIZADOS)) {
                faturamentoDTO.setValorIndustrializados(fat.getValor());
                faturamentoDTO.setIdIndustrializados(fat.getId());
            } else if (fat.getTipoFaturamento().equals(EnumTipoFaturamento.FATURAMENTO_PRODUTOS_ADQ_PARA_COMERCIALIZACAO)) {
                faturamentoDTO.setValorAdquiridosComercializacao(fat.getValor());
                faturamentoDTO.setIdAdquiridosComercializacao(fat.getId());
            }

        }
        return mapFaturamentoAnterior.values();
    }

    public Collection<FaturamentoDoGrupoERealizadoEmMinasGeraisDTO> findFaturamentoDoGrupoERealizadoEmMinasGeraisByFinanceiroId(Long financeiroId) {
        EnumSet<EnumTipoFaturamento> enumList = EnumSet.of(EnumTipoFaturamento.FATURAMENTO_DO_GRUPO, EnumTipoFaturamento.FATURAMENTO_REALIZADO_EM_MG);

        CriteriaBuilder builder = getEm().getCriteriaBuilder();
        CriteriaQuery<FaturamentoAnterior> query = builder.createQuery(FaturamentoAnterior.class);
        List<Predicate> conditions = new ArrayList();

        Root<FaturamentoAnterior> fromFaturamento = query.from(FaturamentoAnterior.class);
        Join<FaturamentoAnterior, Financeiro> fromFinanceiro = fromFaturamento.join(FaturamentoAnterior_.financeiro);

        conditions.add(builder.equal(fromFinanceiro.get(Financeiro_.id), financeiroId));
        conditions.add(fromFaturamento.get(FaturamentoAnterior_.tipoFaturamento).in(enumList));

        TypedQuery<FaturamentoAnterior> typedQuery = getEm().createQuery(query
                .where(conditions.toArray(new Predicate[]{}))
                .orderBy(builder.asc(fromFaturamento.get(FaturamentoAnterior_.ano)))
        );
        List<FaturamentoAnterior> resultList = typedQuery.getResultList();

        Map<Integer, FaturamentoDoGrupoERealizadoEmMinasGeraisDTO> mapFaturamentoPrevisto = new HashMap<>();
        for (FaturamentoAnterior fat : resultList) {
            FaturamentoDoGrupoERealizadoEmMinasGeraisDTO faturamentoDTO = new FaturamentoDoGrupoERealizadoEmMinasGeraisDTO();
            if (mapFaturamentoPrevisto.containsKey(fat.getAno())) {
                faturamentoDTO = mapFaturamentoPrevisto.get(fat.getAno());
            } else {
                faturamentoDTO = new FaturamentoDoGrupoERealizadoEmMinasGeraisDTO();
                mapFaturamentoPrevisto.put(fat.getAno(), faturamentoDTO);
            }
            faturamentoDTO.setAno(fat.getAno());

            if (fat.getTipoFaturamento().equals(EnumTipoFaturamento.FATURAMENTO_DO_GRUPO)) {
                faturamentoDTO.setValorGrupo(fat.getValor());
                faturamentoDTO.setIdGrupo(fat.getId());
            } else if (fat.getTipoFaturamento().equals(EnumTipoFaturamento.FATURAMENTO_REALIZADO_EM_MG)) {
                faturamentoDTO.setValorMinasGerais(fat.getValor());
                faturamentoDTO.setIdMinasGerais(fat.getId());
            }

        }
        return mapFaturamentoPrevisto.values();
    }

    public List<FaturamentoPrevisto> findFaturamentoPrevistoByTipoFaturamentoAndFinanceiroId(EnumTipoFaturamento tipoFaturamento, Long id) {
        TypedQuery<FaturamentoPrevisto> typedQuery = getEm().createNamedQuery("FaturamentoPrevisto.locateByTipoFaturamentoAndFinanceiroId", FaturamentoPrevisto.class);
        typedQuery.setParameter(1, tipoFaturamento);
        typedQuery.setParameter(2, id);
        return typedQuery.getResultList();
    }

    public Financeiro saveOrUpdate(Financeiro financeiro, SecurityContext context, Long idProjeto) throws SystemException {
        try {
            transaction.begin();
            this.getEm().joinTransaction();

            SecurityUser loggedUserWrap = (SecurityUser) context.getUserPrincipal();
            User user = this.getEm().find(User.class, loggedUserWrap.getId());
            financeiro.setUsuarioResponsavel(user);
            financeiro.setDataUltimaAlteracao(new Date());
            Projeto projeto = this.getEm().find(Projeto.class, idProjeto);

            financeiro.setProjeto(projeto);
            financeiro.setDataUltimaAlteracao(new Date());

            if (financeiro.getInvestimentoPrevistoList() != null && !financeiro.getInvestimentoPrevistoList().isEmpty()) {
                financeiro.getInvestimentoPrevistoList().stream().forEach(i -> i.setFinanceiro(financeiro));
            }
            if (financeiro.getOrigemRecursoList() != null && !financeiro.getOrigemRecursoList().isEmpty()) {
                financeiro.getOrigemRecursoList().stream().forEach(o -> o.setFinanceiro(financeiro));
            }
            if (financeiro.getUsoFonteList() != null && !financeiro.getUsoFonteList().isEmpty()) {
                financeiro.getUsoFonteList().stream().forEach(uf -> {
                    if (uf != null) {
                        uf.setFinanceiro(financeiro);
                    }
                });
            }
            if (financeiro.getFaturamentoAnteriorList() != null && !financeiro.getFaturamentoAnteriorList().isEmpty()) {
                financeiro.getFaturamentoAnteriorList().forEach(fa -> fa.setFinanceiro(financeiro));
            }
            if (financeiro.getFaturamentoPrevistoList() != null && !financeiro.getFaturamentoPrevistoList().isEmpty()) {
                financeiro.getFaturamentoPrevistoList().forEach(fp -> fp.setFinanceiro(financeiro));
            }

            //Altera a situação do projeto se o estágio for INICIO_PROJETO.
            if (projetoService.findEstagioAtualById(idProjeto).getEstagio() == Estagio.INICIO_PROJETO) {
                if(user instanceof UsuarioExterno) {
                    projetoService.updateSituacaoProjetoTransactional(SituacaoProjetoEnum.PENDENTE_ACEITE, idProjeto, loggedUserWrap.getId(), new Date());
                } else {
                    projetoService.updateSituacaoProjetoTransactional(SituacaoProjetoEnum.PEDENTE_VALIDACAO, idProjeto, loggedUserWrap.getId(), new Date());
                    
                }
            }
            
            this.merge(financeiro);
            
            transaction.commit();
            return financeiro;
        } catch (Exception ex) {
            transaction.rollback();
            Logger.getLogger(ProjetoService.class.getName()).log(Level.SEVERE, null, ex);
            throw new RuntimeException(ex);
        }
    }
    
    public Object verifyChange(Long idProjeto) throws SystemException {
        String atributos = " investimentoPrevistoList_MOD, origemRecursoList_MOD, "
                + "usoFonteList_MOD, faturamentoAnteriorList_MOD, faturamentoPrevistoList_MOD ";               

        StringBuilder query = new StringBuilder();
        query.append(" Select distinct " + atributos);
        query.append(" From view_logFinanceiro Where projetoId = " + idProjeto);
        query.append(" And ((Select Max(data) From HistoricoSituacaoProjeto "); 
        query.append(" Where situacaoProjeto_id = 3 And projeto_id = ");
        query.append(idProjeto); 
        query.append(" ) < dataUltimaAlteracao) ");
        
        Object result = this.getEm().createNativeQuery(query.toString()).getResultList();
        
        return result;
    }    

}
