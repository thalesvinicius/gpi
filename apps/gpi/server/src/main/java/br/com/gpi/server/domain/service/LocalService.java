package br.com.gpi.server.domain.service;

import br.com.gpi.server.domain.common.BaseService;
import br.com.gpi.server.domain.entity.Local;
import br.com.gpi.server.domain.entity.Local_;
import br.com.gpi.server.domain.repository.AtividadeLocalRepository;
import br.com.gpi.server.domain.repository.LocalRepository;
import br.com.gpi.server.to.LocalTO;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

public class LocalService extends BaseService<Local> {

    @Inject
    private LocalRepository localRepository;

    @Inject
    private AtividadeLocalRepository atividadeLocalRepository;

    @Inject
    public LocalService(EntityManager em) {
        super(Local.class, em);
    }

    public List<LocalTO> findAllActiveWithAtividades() {
        
        CriteriaBuilder builder = getEm().getCriteriaBuilder();
        CriteriaQuery<LocalTO> query = builder.createQuery(LocalTO.class);
        Root<Local> fromLocal = query.from(Local.class);
        List<Predicate> conditions = new ArrayList();
        conditions.add(builder.equal(fromLocal.get(Local_.ativo), Boolean.TRUE));
        
        TypedQuery<LocalTO> typedQuery = getEm().createQuery(query
                .multiselect(fromLocal.get(Local_.id),
                        fromLocal.get(Local_.descricao))
                .where(conditions.toArray(new Predicate[]{}))
                .orderBy(builder.asc(fromLocal.get(Local_.descricao)))
                .distinct(true)
        );

        List<LocalTO> locais = typedQuery.getResultList();
        locais.forEach(u -> u.setAtividades(atividadeLocalRepository.findAllActiveByLocalId(u.getId())));
        
        return locais;
    }
}
