package br.com.gpi.server.to;

import br.com.gpi.server.core.jpa.converter.SituacaoEmpresaEnumConverter;
import br.com.gpi.server.domain.entity.Endereco;
import br.com.gpi.server.domain.entity.SituacaoEmpresa;
import br.com.gpi.server.util.MaskUtil;
import java.util.List;
import javax.persistence.Convert;

public class SearchEmpresaUnidadeTO {

    private Long id;
    private String nome;
    private Endereco endereco;
    private String CNPJ;
    @Convert(converter = SituacaoEmpresaEnumConverter.class)
    private SituacaoEmpresa situacao;
    private List<SearchEmpresaUnidadeTO> unidades;

    public SearchEmpresaUnidadeTO() {
    }

    public SearchEmpresaUnidadeTO(Long id, String nome, Endereco endereco, String CNPJ, SituacaoEmpresa situacao) {
        this.id = id;
        this.nome = nome;
        this.endereco = endereco;
        this.CNPJ = CNPJ;
        this.situacao = situacao;
    }
    
    public SearchEmpresaUnidadeTO(Long id, String nome, Endereco endereco) {
        this.id = id;
        this.nome = nome;
        this.endereco = endereco;
    }
    
    
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Endereco getEndereco() {
        return endereco;
    }

    public void setEndereco(Endereco endereco) {
        this.endereco = endereco;
    }

    public String getCNPJ() {
        return CNPJ;
    }
    
    public String getCnpjFormatado(){
        if(CNPJ != null){
            return MaskUtil.format("##.###.###/####-##", CNPJ);
        }
        return null;
    }

    public void setCNPJ(String CNPJ) {
        this.CNPJ = CNPJ;
    }

    public List<SearchEmpresaUnidadeTO> getUnidades() {
        return unidades;
    }

    public void setUnidades(List<SearchEmpresaUnidadeTO> unidades) {
        this.unidades = unidades;
    }

    public SituacaoEmpresa getSituacao() {
        return situacao;
    }

    public void setSituacao(SituacaoEmpresa situacao) {
        this.situacao = situacao;
    }

    public String getNomePrincipal() {
        return nome;
    }
}
