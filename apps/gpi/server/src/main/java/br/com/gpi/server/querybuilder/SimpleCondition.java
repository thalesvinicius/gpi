package br.com.gpi.server.querybuilder;

import java.util.Arrays;

public class SimpleCondition extends Condition {

    private String value;
    private ConditionComparisonTypeEnum comparisonType;

    public SimpleCondition() {
    }

    public SimpleCondition(String value, String conditionKey, Field field, ConditionValueTypeEnum type, ConditionComparisonTypeEnum comparisonType) {
        super(conditionKey, Arrays.asList(new Field[]{field}), type);
        this.value = value;
        this.comparisonType = comparisonType;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public ConditionComparisonTypeEnum getComparisonType() {
        return comparisonType;
    }

    public void setComparisonType(ConditionComparisonTypeEnum comparisonType) {
        this.comparisonType = comparisonType;
    }

    @Override
    public String getSQL() {
        StringBuilder sb = new StringBuilder();
        sb.append(getField().getTabela().getAlias());
        sb.append(".");
        sb.append(getField().getNome());
        
        if (getValueType().equals(ConditionValueTypeEnum.STRING)) {
            sb.append(comparisonType.getDescricao().replace("{value}", comparisonType.equals(ConditionComparisonTypeEnum.LIKE) ? getValue() : "'".concat(getValue()).concat("'")));
        } else {
            if (comparisonType.equals(ConditionComparisonTypeEnum.LIKE)) {
                throw new RuntimeException("ConditionComparisonTypeEnum.LIKE is a wrong ConditionComparisonTypeEnum for ConditionValueTypeEnum.NON_STRING");
            }
            sb.append(comparisonType.getDescricao().replace("{value}", getValue()));
        }

        return sb.toString();
    }

}
