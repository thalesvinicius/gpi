package br.com.gpi.server.domain.entity;

import br.com.gpi.server.core.jpa.typesafe.EnumerationEntityType;

public enum InsumoProdutoOrigem implements EnumerationEntityType<Integer> {

    INVALID(0, "invalid"), MINAS_GERAIS(1, "Minas Gerais"), IMPORTADOS(2, "Importados"), OUTROS_ESTADOS(3, "Outros Estados");

    private InsumoProdutoOrigem(Integer id, String descricao) {
        this.id = id;
        this.descricao = descricao;
    }

    private final Integer id;
    private final String descricao;

    @Override
    public Integer getId() {
        return this.id;
    }

    @Override
    public String getDescription() {
        return this.descricao;
    }

    public static InsumoProdutoOrigem getInsumoProdutoOrigem(Integer id) {
        for (InsumoProdutoOrigem insumoProdutoOrigem : values()) {
            if (insumoProdutoOrigem.id.equals(id)) {
                return insumoProdutoOrigem;
            }
        }

        return null;
    }
}
