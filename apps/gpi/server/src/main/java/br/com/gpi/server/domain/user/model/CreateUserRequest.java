package br.com.gpi.server.domain.user.model;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import org.hibernate.validator.constraints.Length;

/**
 * @author <a href="mailto:pviniciusfm@gmail.com">Paulo Vinicius F. Machado</a>
 */
@XmlRootElement
public class CreateUserRequest {

    @NotNull
    @Valid
    private SecurityUser user;

    @Length(min=8, max=30)
    @NotNull
    private String password;


    public CreateUserRequest() {
    }

    public CreateUserRequest(final SecurityUser user, final String password) {
        this.user = user;
        this.password = password;
    }

    public SecurityUser getUser() {
        return user;
    }

    public void setUser(SecurityUser user) {
        this.user = user;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
