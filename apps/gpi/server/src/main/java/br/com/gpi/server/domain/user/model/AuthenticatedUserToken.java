package br.com.gpi.server.domain.user.model;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author <a href="mailto:pviniciusfm@gmail.com">Paulo Vinicius F. Machado</a>
 */
@XmlRootElement
public class AuthenticatedUserToken {

    private String userId;

    public AuthenticatedUserToken() {
    }

    public AuthenticatedUserToken(String userId) {
        this.userId = userId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
