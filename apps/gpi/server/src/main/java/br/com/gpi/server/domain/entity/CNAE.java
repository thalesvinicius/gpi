package br.com.gpi.server.domain.entity;

import br.com.gpi.server.domain.common.BaseEntity;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import org.hibernate.envers.Audited;

@Audited
@Entity
@Table(name = "CNAE")
@NamedQueries({
    @NamedQuery(name = CNAE.Query.findById, query = "SELECT c FROM CNAE c WHERE c.id in ?1"),
    @NamedQuery(name = CNAE.Query.findByParentId, query = "SELECT c FROM CNAE c WHERE c.cnaeSuperior.id in ?1"),
    @NamedQuery(name = CNAE.Query.findAllDivisoes, query = "SELECT c FROM CNAE c WHERE c.cnaeSuperior.id is null"),
    @NamedQuery(name = CNAE.Query.findByNivel, query = "SELECT c FROM CNAE c WHERE c.nivel = ?1")
})
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "nivel",
        discriminatorType = DiscriminatorType.INTEGER
)
public class CNAE extends BaseEntity<String> {
    public static final String DIVISAO = "1";
    public static final String GRUPO = "2";
    public static final String CLASSE = "3";
    public static final String SUBCLASSE = "4";
    

    @Basic(optional = false)
    @NotNull
    private String denominacao;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "CNAESuperior_id")
    private CNAE cnaeSuperior;
    
    @NotNull
    @Column(insertable = false, updatable = false)
    private Integer nivel;

    public String getDenominacao() {
        return denominacao;
    }

    public void setDenominacao(String denominacao) {
        this.denominacao = denominacao;
    }

    public CNAE getCnaeSuperior() {
        return cnaeSuperior;
    }

    public void setCnaeSuperior(CNAE cnaeSuperior) {
        this.cnaeSuperior = cnaeSuperior;
    }

    public static class Query {
        public static final String findById = "CNAE.findById";
        public static final String findByParentId = "CNAE.findByParentId";
        public static final String findAllDivisoes = "CNAE.findAllDivisoes";
        public static final String findByNivel = "CNAE.findByNivel";
    }
}
