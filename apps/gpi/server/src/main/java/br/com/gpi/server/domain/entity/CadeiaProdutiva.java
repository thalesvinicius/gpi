package br.com.gpi.server.domain.entity;

import br.com.gpi.server.domain.common.BaseDomainTableEntity;
import com.fasterxml.jackson.annotation.JsonIgnore;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import org.hibernate.envers.Audited;

/**
 * @since 01/07/2014
 * @author <a href="mailto:thalesvd@algartech.com">Thales Dias</a>
 */
@Audited
@Entity
@Table(name = "CadeiaProdutiva")
@NamedQueries({
    @NamedQuery(name = CadeiaProdutiva.Query.locateResponsavelById, query = "SELECT c.usuarioResponsavelCadeia FROM CadeiaProdutiva c WHERE c.id = ?1"),
    @NamedQuery(name = CadeiaProdutiva.Query.locateByDepartamentos, query = "SELECT c FROM CadeiaProdutiva c WHERE c.departamento.id in :departamentos"),
    @NamedQuery(name = CadeiaProdutiva.Query.locateByDepartamento, query = "SELECT c FROM CadeiaProdutiva c WHERE c.departamento.id = ?1"),
    @NamedQuery(name = CadeiaProdutiva.Query.locateByProjetoId, query = "SELECT p.cadeiaProdutiva from Projeto p WHERE p.id = ?1")    
})
@Cacheable
public class CadeiaProdutiva extends BaseDomainTableEntity {

    @Column(name = "descricao", length = 255)
    private String descricao;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "departamento_id")
    private Departamento departamento;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "usuarioResponsavel_id")
    private UsuarioInterno usuarioResponsavelCadeia;

    @Override
    public String getDescricao() {
        return descricao;
    }

    @Override
    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Departamento getDepartamento() {
        return departamento;
    }

    public void setDepartamento(Departamento departamento) {
        this.departamento = departamento;
    }

    public UsuarioInterno getUsuarioResponsavelCadeia() {
        return usuarioResponsavelCadeia;
    }

    public void setUsuarioResponsavelCadeia(UsuarioInterno usuarioResponsavelCadeia) {
        this.usuarioResponsavelCadeia = usuarioResponsavelCadeia;
    }

    public static class Query {

        public static final String locateResponsavelById = "CadeiaProdutiva.locateResponsavelById";
        public static final String locateByDepartamentos = "CadeiaProdutiva.locateByDepartamentos";
        public static final String locateByDepartamento = "CadeiaProdutiva.locateByDepartamento";
        public static final String locateByProjetoId = "CadeiaProdutiva.locateByProjetoId";        
        
        public Query() {
        }
    }

}
