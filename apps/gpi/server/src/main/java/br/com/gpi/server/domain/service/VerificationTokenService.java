package br.com.gpi.server.domain.service;

import br.com.gpi.server.config.ApplicationSettings;
import br.com.gpi.server.domain.common.BaseService;
import br.com.gpi.server.domain.entity.TipoUsuario;
import br.com.gpi.server.domain.entity.User;
import br.com.gpi.server.domain.entity.UsuarioExterno;
import br.com.gpi.server.domain.entity.VerificationToken;
import br.com.gpi.server.domain.user.exception.AlreadyUsedTokenException;
import br.com.gpi.server.domain.user.exception.AuthenticationException;
import br.com.gpi.server.domain.user.exception.AuthorizationException;
import br.com.gpi.server.domain.user.exception.TokenHasExpiredException;
import br.com.gpi.server.domain.user.exception.TokenNotFoundException;
import br.com.gpi.server.domain.user.exception.UserNotFoundException;
import br.com.gpi.server.domain.user.model.ChangePasswordRequest;
import br.com.gpi.server.domain.user.model.EmailServiceTokenModel;
import br.com.gpi.server.domain.user.model.LostPasswordRequest;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;
import javax.validation.Validator;
import org.apache.commons.codec.binary.Base64;

/**
 * @version 1.0
 * @author <a href="mailto:pviniciusfm@gmail.com">Paulo Vinicius F. Machado</a>
 */
public class VerificationTokenService extends BaseService<VerificationToken> {

    @Inject
    private UserService userRepository;
    @Inject
    private ApplicationSettings config;
    @Inject
    private EmailService emailServicesGateway;
    @Inject
    private Validator validator;

    @Inject
    private UserTransaction transaction;

    @Inject
    public VerificationTokenService(EntityManager em) {
        super(VerificationToken.class, em);
    }

    public VerificationToken sendEmailVerificationToken(UsuarioExterno user) throws SystemException {
        boolean isJoinedToTransaction = this.getEm().isJoinedToTransaction();
        try {
            if (!isJoinedToTransaction) {
                transaction.begin();
                this.getEm().joinTransaction();
            }

            VerificationToken token = new VerificationToken(user, VerificationToken.VerificationTokenType.emailVerification,
                    config.getEmailVerificationTokenExpiryTimeInMinutes());
            user.addVerificationToken(token);
            token = (VerificationToken) merge(token);
            emailServicesGateway.sendVerificationEmail(new EmailServiceTokenModel(user, token, config.getBaseUrl()));

            if (!isJoinedToTransaction) {
                transaction.commit();
            }
            return token;
        } catch (Exception ex) {
            if (!isJoinedToTransaction) {
                transaction.rollback();
            }
            Logger.getLogger(ProjetoService.class.getName()).log(Level.SEVERE, null, ex);
            throw new RuntimeException(ex);
        }
    }

    /**
     * generate token if user found otherwise do nothing
     *
     * @param lostPasswordRequests
     */
    public void sendLostPasswordToken(List<LostPasswordRequest> lostPasswordRequests) throws SystemException {
        try {
            transaction.begin();
            this.getEm().joinTransaction();
            for (LostPasswordRequest lpr : lostPasswordRequests) {
                validate(validator, lpr);
                VerificationToken token = null;
                UsuarioExterno user = (UsuarioExterno) userRepository.findByLogin(lpr.getEmailAddress(), TipoUsuario.EXTERNO);
                if (user != null) {
                    token = user.getActiveLostPasswordToken();
                    if (token == null) {
                        token = new VerificationToken(user, VerificationToken.VerificationTokenType.lostPassword,
                                config.getEmailLostPasswordTokenExpiryTimeInMinutes());
                        user.addVerificationToken(token);
                        user = (UsuarioExterno) this.merge(user);
                    }
                    // emailServicesGateway.sendLostPasswordEmail(new EmailServiceTokenModel(user, token, config.getBaseUrl()));
                    emailServicesGateway.sendVerificationEmail(new EmailServiceTokenModel(user, token, config.getBaseUrl()));
                } else {
                    throw new UserNotFoundException();
                }
            }
            transaction.commit();

        } catch (Exception ex) {
            transaction.rollback();
            Logger.getLogger(ProjetoService.class.getName()).log(Level.SEVERE, null, ex);
            throw new RuntimeException(ex);
        }
    }

    public void sendLostPasswordToken(LostPasswordRequest lostPasswordRequest) throws SystemException {
        try {
            sendLostPasswordToken(Arrays.asList(new LostPasswordRequest[]{lostPasswordRequest}));

        } catch (Exception ex) {
            transaction.rollback();
            Logger.getLogger(ProjetoService.class.getName()).log(Level.SEVERE, null, ex);
            throw new RuntimeException(ex);
        }
    }

    public VerificationToken verify(String base64EncodedToken, String email, String password) throws SystemException {
        try {
            transaction.begin();
            this.getEm().joinTransaction();

            VerificationToken token = loadToken(base64EncodedToken);

            if (!token.getUser().getEmail().equals(email)) {
                throw new AuthorizationException("The credentials is not valid for this operation.");
            }
            if (token.isVerified()) {
                throw new AlreadyUsedTokenException(VerificationToken.VerificationTokenType.emailVerification);
            }

            token.setVerified(true);
            try {
                token.getUser().setPassword(token.getUser().hashPassword(password));
            } catch (Exception e) {
                throw new AuthenticationException();
            }
            token.getUser().setAtivo(true);

            this.merge(token.getUser());

            transaction.commit();
            return token;
        } catch (Exception ex) {
            transaction.rollback();
            Logger.getLogger(ProjetoService.class.getName()).log(Level.SEVERE, null, ex);
            throw new RuntimeException(ex);
        }
    }

    public VerificationToken generateEmailVerificationToken(String emailAddress) throws SystemException {
        try {
            transaction.begin();
            this.getEm().joinTransaction();

            UsuarioExterno user = (UsuarioExterno) userRepository.findByLogin(emailAddress, TipoUsuario.EXTERNO);
            if (user == null) {
                throw new UserNotFoundException();
            }
            if (user.isAtivo()) {
                throw new AlreadyUsedTokenException(VerificationToken.VerificationTokenType.emailVerification);
            }
            //if token still active resend that
            VerificationToken token = user.getActiveEmailVerificationToken();
            if (token == null) {
                token = sendEmailVerificationToken(user);
            } else {
                emailServicesGateway.sendVerificationEmail(new EmailServiceTokenModel(user, token, config.getBaseUrl()));
            }

            transaction.commit();
            return token;
        } catch (Exception ex) {
            transaction.rollback();
            Logger.getLogger(ProjetoService.class.getName()).log(Level.SEVERE, null, ex);
            throw new RuntimeException(ex);
        }
    }

    public VerificationToken resetPassword(ChangePasswordRequest passwordRequest) throws SystemException {
        try {
            transaction.begin();
            this.getEm().joinTransaction();

            validate(validator, passwordRequest);
            VerificationToken token = loadToken(passwordRequest.getToken());
            if (token.isVerified()) {
                throw new AlreadyUsedTokenException(VerificationToken.VerificationTokenType.lostPassword);
            }
            if (!token.getUser().getEmail().equals(passwordRequest.getEmail())) {
                throw new AuthorizationException(passwordRequest.getToken());
            }
            token.setVerified(true);
            User user = token.getUser();
            try {
                user.setPassword(user.hashPassword(passwordRequest.getPassword()));
            } catch (Exception e) {
                throw new AuthenticationException();
            }
            user.setAtivo(true);
            this.merge(user);

            transaction.commit();
            return token;
        } catch (Exception ex) {
            transaction.rollback();
            Logger.getLogger(ProjetoService.class.getName()).log(Level.SEVERE, null, ex);
            throw new RuntimeException(ex);
        }
    }

    private VerificationToken loadToken(String base64EncodedToken) {
        String rawToken = new String(Base64.decodeBase64(base64EncodedToken));
        VerificationToken token = findByToken(rawToken);
        if (token == null) {
            throw new TokenNotFoundException();
        }
        if (token.hasExpired()) {
            throw new TokenHasExpiredException();
        }
        return token;
    }

    public VerificationToken findByToken(String token) {
        List<VerificationToken> result = this.findByNamedQuery(VerificationToken.Query.findByToken, token);
        return (result != null && !result.isEmpty()) ? result.get(0) : null;
    }
}
