package br.com.gpi.server.domain.service;

import br.com.gpi.server.domain.common.BaseService;
import br.com.gpi.server.domain.entity.InstrumentoFormalizacao;
import br.com.gpi.server.domain.entity.PerguntaPesquisa;
import br.com.gpi.server.domain.entity.Pesquisa;
import br.com.gpi.server.domain.entity.PesquisaPerguntaPesquisa;
import br.com.gpi.server.domain.entity.User;
import br.com.gpi.server.domain.entity.UsuarioExterno;
import br.com.gpi.server.domain.repository.PesquisaRepository;
import br.com.gpi.server.domain.repository.UserExternalRepository;
import br.com.gpi.server.to.ProjetoTO;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;
import org.apache.deltaspike.core.api.projectstage.ProjectStage;

public class PesquisaService extends BaseService<Pesquisa> {

    public static final Integer RESPOSTA_SIM = 1;
    public static final Integer RESPOSTA_NAO = 2;
    public static final Integer RESPOSTA_EXCELENTE = 3;
    public static final Integer RESPOSTA_BOA = 4;
    public static final Integer RESPOSTA_REGULAR = 5;
    public static final Integer RESPOSTA_RUIM = 6;

    public static final Float WEIGHT_SIM = 1F;
    public static final Float WEIGHT_NAO = 0F;
    public static final Float WEIGHT_EXCELENTE = 1F;
    public static final Float WEIGHT_BOA = 0.75F;
    public static final Float WEIGHT_REGULAR = 0.5F;
    public static final Float WEIGHT_RUIM = 0F;

    @Inject
    PesquisaRepository pesquisaRepository;

    @Inject
    private UserExternalRepository userExternalRepository;

    @Inject
    private EmailService emailService;

    @Inject
    private ProjectStage stage;

    @Inject
    private UserTransaction transaction;

    private static final Map<Integer, Float> ANSWER_MAP = new HashMap<>();
    
    @Inject
    public PesquisaService(EntityManager em) {
        super(Pesquisa.class, em);
    }

    static {
        ANSWER_MAP.put(PesquisaService.RESPOSTA_SIM, WEIGHT_SIM);
        ANSWER_MAP.put(PesquisaService.RESPOSTA_NAO, WEIGHT_NAO);
        ANSWER_MAP.put(PesquisaService.RESPOSTA_EXCELENTE, WEIGHT_EXCELENTE);
        ANSWER_MAP.put(PesquisaService.RESPOSTA_BOA, WEIGHT_BOA);
        ANSWER_MAP.put(PesquisaService.RESPOSTA_REGULAR, WEIGHT_REGULAR);
        ANSWER_MAP.put(PesquisaService.RESPOSTA_RUIM, WEIGHT_RUIM);
    }

    public Boolean createPesquisaPostInstrumentoSignatureTransactional(InstrumentoFormalizacao instrumentoFormalizacao) {
        this.getEm().joinTransaction();
        Date now = new Date();

        Pesquisa pesquisa = new Pesquisa();
        pesquisa.setDataSolicitacao(now);
        pesquisa.setDataUltimaAlteracao(now);
        pesquisa.setInstrumentoFormalizacao(instrumentoFormalizacao);

        this.getEm().persist(pesquisa);
        this.getEm().flush();

        TypedQuery<PerguntaPesquisa> query = getEm().createQuery("Select p FROM PerguntaPesquisa p", PerguntaPesquisa.class);
        List<PerguntaPesquisa> perguntas = query.getResultList();

        List<PesquisaPerguntaPesquisa> pesquisaPerguntaPesquisaList = new ArrayList<>();
        for (PerguntaPesquisa perguntaPesquisa : perguntas) {
            PesquisaPerguntaPesquisa pesquisaPerguntaPesquisa = new PesquisaPerguntaPesquisa();
            pesquisaPerguntaPesquisa.setPesquisaId(pesquisa.getId());
            pesquisaPerguntaPesquisa.setPesquisa(pesquisa);
            pesquisaPerguntaPesquisa.setPerguntaPesquisaId(perguntaPesquisa.getId());
            pesquisaPerguntaPesquisa.setPerguntaPesquisa(perguntaPesquisa);
            pesquisaPerguntaPesquisaList.add(pesquisaPerguntaPesquisa);
        }

        pesquisa.setPesquisaPerguntaPesquisaList(pesquisaPerguntaPesquisaList);

        this.getEm().merge(pesquisa);

        if (!stage.equals(ProjectStage.Production)) {
            instrumentoFormalizacao.getEmpresasSelecionadas().stream().forEach(u -> {
                if (u.getResponsavelInvestimento() != null
                        && u.getResponsavelInvestimento().getEmail() != null) {
                    emailService.sendInviteSatisfactionSurveyEmail(u.getResponsavelInvestimento().getEmail());
                }
            });
        }

        return true;
    }

    public Pesquisa saveOrUpdate(Pesquisa pesquisa, User user) throws SystemException {
        try {
            transaction.begin();
            this.getEm().joinTransaction();

            UsuarioExterno usuarioExterno = null;
            if (user instanceof UsuarioExterno) {
                usuarioExterno = userExternalRepository.findBy(user.getId());
                pesquisa.setContatoExternoResponsavel(user.getId());
            }
            Date now = new Date();

            for (PesquisaPerguntaPesquisa ppp : pesquisa.getPesquisaPerguntaPesquisaList()) {
                ppp.setPesquisa(pesquisa);
            }

            pesquisa.setDataUltimaAlteracao(now);
            pesquisa.setDataResposta(now);

            pesquisa.setIndiceSatisfacao(calculoPesquisaSatisfacao(pesquisa.getPesquisaPerguntaPesquisaList()));

            pesquisa = (Pesquisa) this.merge(pesquisa);

            if (usuarioExterno != null && usuarioExterno.getEmail() != null) {
                emailService.sendThankSatisfactionSurveyEmail(usuarioExterno.getEmail());
            }
            
            InstrumentoFormalizacao instrumentoFormalizacao = pesquisa.getInstrumentoFormalizacao();

            for (ProjetoTO projeto : instrumentoFormalizacao.getProjetosSelecionados()) {
                User responsavelInvestimento = projeto.getUsuarioInvestimento();
                if (responsavelInvestimento != null && responsavelInvestimento.getEmail() != null) {
                    emailService.sendAnswerSatisfactionSurveyEmail(projeto.getUsuarioInvestimento().getEmail(), instrumentoFormalizacao.getTitulo());
                }
            }

            transaction.commit();
            return pesquisa;
        } catch (Exception ex) {
            transaction.rollback();
            Logger.getLogger(ProjetoService.class.getName()).log(Level.SEVERE, null, ex);
            throw new RuntimeException(ex);
        }
    }

    /*
     RNE218 - Calculo Pesquisa Satisfação

     Após o Usuário Operador acionar o comando Enviar da Pesquisa de Satisfação, o GPI - Área do Investidor deverá realizar o cálculo e armazenar em banco de dados o seguIntegere calculo referente as respostas para medir a satisfação do atendimento prestado pelo INDI, estes dados deverão ficar armazenados em banco de dados, pois serão consultados por um relatório mensal (Painel de Reunião Gerencial Mensal) conforme abaixo:
     As respostas do investidor tem os seguIntegeres pesos:
     Peso das respostas de 1 a 4:
     Excelente = 1
     Bom = 0,75
     Regular = 0,5
     Ruim = 0
     Peso da resposta 5
     Sim= 1
     Não = 0
     */
    public Float calculoPesquisaSatisfacao(List<PesquisaPerguntaPesquisa> respostas) {
        Float indiceSatisfacao = 0F;

        for (PesquisaPerguntaPesquisa participacao : respostas) {
            indiceSatisfacao += ANSWER_MAP.get(participacao.getResposta());
        }

        return indiceSatisfacao;
    }
}
