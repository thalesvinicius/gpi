package br.com.gpi.server.util.reports;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.Date;

public class H2Functions {

    public static LocalDate calcularPrazo(Date dataInicioOperacao, Integer classeEmprend, Date dataEntregaEstudos) {
        if (dataInicioOperacao != null) {
            int prazoDias = 0;
            if (classeEmprend != null) {
                prazoDias = getQuantidadeDiasComId(classeEmprend);
            }

            if (dataEntregaEstudos != null) {
                Instant instant = Instant.ofEpochMilli(dataEntregaEstudos.getTime());
                LocalDate prazo = LocalDateTime.ofInstant(instant, ZoneId.systemDefault()).toLocalDate();
                prazo = prazo.plusDays(prazoDias);
                return prazo;
            }
        }
        return null;
    }

    public static Long calcularDiasForaPrazo(Date dataInicioOperacao, Integer classeEmprend, Date dataEntregaEstudos) {
        if (dataInicioOperacao != null) {
            int prazoDias = 0;
            if (classeEmprend != null) {
                prazoDias = getQuantidadeDiasComId(classeEmprend);
            }

            if (dataEntregaEstudos != null) {
                Instant instant = Instant.ofEpochMilli(dataEntregaEstudos.getTime());
                LocalDate prazo = LocalDateTime.ofInstant(instant, ZoneId.systemDefault()).toLocalDate();
                prazo = prazo.plusDays(prazoDias);

                instant = Instant.ofEpochMilli(dataInicioOperacao.getTime());
                LocalDate dio = LocalDateTime.ofInstant(instant, ZoneId.systemDefault()).toLocalDate();
                // Como o cronograma só salva mês e ano o sistema adotou como default o dia ser sempre o 1
                dio = dio.withDayOfMonth(1);

                Boolean isDentroPrazo = dio.isAfter(prazo);
                if (!isDentroPrazo) {
                    long diasForaPrazo = ChronoUnit.DAYS.between(dio, prazo);
                    return diasForaPrazo;
                }
            }
        }
        return 0L;
    }

    public static int getQuantidadeDiasComId(Integer classeEmprend) {
        switch (classeEmprend) {
            case 1:
            case 2:
                return 30;
            case 3:
            case 4:
                return 360;
            case 5:
            case 6:
                return 540;
            default:
                return 0;
        }
    }

    public static Date expectativaEmissaoLicensa(LocalDate prazo) {
        return Date.from(prazo.atStartOfDay(ZoneId.systemDefault()).toInstant());
    }
}
