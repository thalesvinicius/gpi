package br.com.gpi.server.to;

public class ProjetosCount {
    private String name;
    private Object size;
    public ProjetosCount() {}
    public ProjetosCount(String name, Object size) {
        this.name = name;
        this.size = size;
    }
    public String getName() { 
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public Object getSize() { 
        return size;
    }
    public void setSize(Object size) {
        this.size = size;
    }
}
