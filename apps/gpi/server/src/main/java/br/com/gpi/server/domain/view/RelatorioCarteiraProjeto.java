package br.com.gpi.server.domain.view;

import br.com.gpi.server.domain.common.BaseEntity;
import br.com.gpi.server.domain.entity.Estagio;
import br.com.gpi.server.domain.entity.SituacaoProjetoEnum;
import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "VIEW_RELATORIO_CARTEIRA_PROJETO")
public class RelatorioCarteiraProjeto extends BaseEntity<Long> implements Serializable {

    private String empresa;
    private String projeto;
    private String cidade;
    private String regiaoPlanejamento;
    private Long regiaoPlanejamentoId;
    private Double investimentoPrevisto;
    private Double faturamentoInicial;
    private Double faturamentoFinal;
    private Integer empregosDiretos;
    private Integer empregosIndiretos;
    private Long cadeiaProdutivaId;
    private String cadeiaProdutiva;
    private SituacaoProjetoEnum situacaoProjeto;
    private Estagio estagioAtual;
    private Long analistaId;    
    private String analista;
    private Long gerenciaId;
    private String gerencia;
    private Long diretoriaId;

    public RelatorioCarteiraProjeto() {
    }

    /**
     * @return the empresa
     */
    public String getEmpresa() {
        return empresa;
    }

    /**
     * @param empresa the empresa to set
     */
    public void setEmpresa(String empresa) {
        this.empresa = empresa;
    }

    /**
     * @return the projeto
     */
    public String getProjeto() {
        return projeto;
    }

    /**
     * @param projeto the projeto to set
     */
    public void setProjeto(String projeto) {
        this.projeto = projeto;
    }

    /**
     * @return the cidade
     */
    public String getCidade() {
        return cidade;
    }

    /**
     * @param cidade the cidade to set
     */
    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    /**
     * @return the regiaoPlanejamento
     */
    public String getRegiaoPlanejamento() {
        return regiaoPlanejamento;
    }

    /**
     * @param regiaoPlanejamento the regiaoPlanejamento to set
     */
    public void setRegiaoPlanejamento(String regiaoPlanejamento) {
        this.regiaoPlanejamento = regiaoPlanejamento;
    }

    /**
     * @return the regiaoPlanejamentoId
     */
    public Long getRegiaoPlanejamentoId() {
        return regiaoPlanejamentoId;
    }

    /**
     * @param regiaoPlanejamentoId the regiaoPlanejamentoId to set
     */
    public void setRegiaoPlanejamentoId(Long regiaoPlanejamentoId) {
        this.regiaoPlanejamentoId = regiaoPlanejamentoId;
    }

    /**
     * @return the investimentoPrevisto
     */
    public Double getInvestimentoPrevisto() {
        return investimentoPrevisto;
    }

    /**
     * @param investimentoPrevisto the investimentoPrevisto to set
     */
    public void setInvestimentoPrevisto(Double investimentoPrevisto) {
        this.investimentoPrevisto = investimentoPrevisto;
    }

    /**
     * @return the faturamentoInicial
     */
    public Double getFaturamentoInicial() {
        return faturamentoInicial;
    }

    /**
     * @param faturamentoInicial the faturamentoInicial to set
     */
    public void setFaturamentoInicial(Double faturamentoInicial) {
        this.faturamentoInicial = faturamentoInicial;
    }

    /**
     * @return the faturamentoFinal
     */
    public Double getFaturamentoFinal() {
        return faturamentoFinal;
    }

    /**
     * @param faturamentoFinal the faturamentoFinal to set
     */
    public void setFaturamentoFinal(Double faturamentoFinal) {
        this.faturamentoFinal = faturamentoFinal;
    }

    /**
     * @return the empregosDiretos
     */
    public Integer getEmpregosDiretos() {
        return empregosDiretos;
    }

    /**
     * @param empregosDiretos the empregosDiretos to set
     */
    public void setEmpregosDiretos(Integer empregosDiretos) {
        this.empregosDiretos = empregosDiretos;
    }

    /**
     * @return the empregosIndiretos
     */
    public Integer getEmpregosIndiretos() {
        return empregosIndiretos;
    }

    /**
     * @param empregosIndiretos the empregosIndiretos to set
     */
    public void setEmpregosIndiretos(Integer empregosIndiretos) {
        this.empregosIndiretos = empregosIndiretos;
    }

    /**
     * @return the cadeiaProdutivaId
     */
    public Long getCadeiaProdutivaId() {
        return cadeiaProdutivaId;
    }

    /**
     * @param cadeiaProdutivaId the cadeiaProdutivaId to set
     */
    public void setCadeiaProdutivaId(Long cadeiaProdutivaId) {
        this.cadeiaProdutivaId = cadeiaProdutivaId;
    }

    /**
     * @return the cadeiaProdutiva
     */
    public String getCadeiaProdutiva() {
        return cadeiaProdutiva;
    }

    /**
     * @param cadeiaProdutiva the cadeiaProdutiva to set
     */
    public void setCadeiaProdutiva(String cadeiaProdutiva) {
        this.cadeiaProdutiva = cadeiaProdutiva;
    }

    /**
     * @return the situacaoProjeto
     */
    public String getSituacaoProjeto() {
        return situacaoProjeto == null ? null : situacaoProjeto.getDescription();
    }

    /**
     * @param situacaoProjeto the situacaoProjeto to set
     */
    public void setSituacaoProjeto(SituacaoProjetoEnum situacaoProjeto) {
        this.situacaoProjeto = situacaoProjeto;
    }

    /**
     * @return the estagioAtual
     */
    public String getEstagioAtual() {
        return estagioAtual == null ? null : estagioAtual.getDescricao();
    }

    /**
     * @param estagioAtual the estagioAtual to set
     */
    public void setEstagioAtual(Estagio estagioAtual) {
        this.estagioAtual = estagioAtual;
    }

    /**
     * @return the analistaId
     */
    public Long getAnalistaId() {
        return analistaId;
    }

    /**
     * @param analistaId the analistaId to set
     */
    public void setAnalistaId(Long analistaId) {
        this.analistaId = analistaId;
    }

    /**
     * @return the analista
     */
    public String getAnalista() {
        return analista;
    }

    /**
     * @param analista the analista to set
     */
    public void setAnalista(String analista) {
        this.analista = analista;
    }

    /**
     * @return the gerenciaId
     */
    public Long getGerenciaId() {
        return gerenciaId;
    }

    /**
     * @param gerenciaId the gerenciaId to set
     */
    public void setGerenciaId(Long gerenciaId) {
        this.gerenciaId = gerenciaId;
    }

    /**
     * @return the gerencia
     */
    public String getGerencia() {
        return gerencia;
    }

    /**
     * @param gerencia the gerencia to set
     */
    public void setGerencia(String gerencia) {
        this.gerencia = gerencia;
    }

    /**
     * @return the diretoriaId
     */
    public Long getDiretoriaId() {
        return diretoriaId;
    }

    /**
     * @param diretoriaId the diretoriaId to set
     */
    public void setDiretoriaId(Long diretoriaId) {
        this.diretoriaId = diretoriaId;
    }

    public static class Filtro {

        public static final String GERENCIA = "gerencia";
        public static final String CADEIA_PRODUTIVA = "cadeiaProdutiva";
        public static final String ANALISTA = "analista";
        public static final String ESTAGIO = "estagio";
        public static final String STATUS_ATUAL = "statusAtual";
        public static final String REGIAO = "regiao";
    }
}