package br.com.gpi.server.endpoint;

import br.com.gpi.server.domain.entity.CadeiaProdutiva;
import br.com.gpi.server.domain.entity.Departamento;
import br.com.gpi.server.domain.entity.Estagio;
import br.com.gpi.server.domain.entity.GrupoUsuario;
import br.com.gpi.server.domain.entity.RelatorioAgendaPositivaAbstract;
import br.com.gpi.server.domain.entity.UsuarioInterno;
import br.com.gpi.server.domain.service.DomainTableService;
import br.com.gpi.server.domain.service.ProjetoService;
import br.com.gpi.server.domain.service.RegiaoPlanejamentoService;
import br.com.gpi.server.domain.service.UserService;
import br.com.gpi.server.domain.service.reports.gerencial.FiltroAgendaPositiva;
import br.com.gpi.server.domain.service.reports.gerencial.Gerencial03AgendaPositivaService;
import br.com.gpi.server.domain.user.model.SecurityUser;
import br.com.gpi.server.util.EnumMimeTypes;
import br.com.gpi.server.util.Formato;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

@Path("relatorio/agendaPositiva")
@Stateless
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
@TransactionManagement(TransactionManagementType.BEAN)
public class PositiveScheduleEndPoint {

    @Inject
    private Gerencial03AgendaPositivaService gerencial03AgendaPositivaService;
    @Inject
    private ProjetoService projetoService;
    @Context
    private SecurityContext securityContext;

    @Inject
    private RegiaoPlanejamentoService regiaoPlanejamentoService;
    @Inject
    private DomainTableService domainTable;
    @Inject
    private UserService user;
    @Inject
    private Logger logger;

    private static final Set<Long> todosOsProjetos = new HashSet<>(
            Arrays.asList(GrupoUsuario.ANALISTA_DE_INFORMACAO, GrupoUsuario.ANALISTA_DE_PORTFOLIO, GrupoUsuario.DIRETOR));

    private static final Set<Long> usuariosAutorizados = new HashSet<>(Arrays.asList(
            GrupoUsuario.ANALISTA_DE_PORTFOLIO, GrupoUsuario.GERENTE, GrupoUsuario.DIRETOR));
    private static final List<Estagio> estagiosValidos = Arrays.asList(Estagio.FORMALIZADA,
            Estagio.IMPLANTACAO_INICIADO, Estagio.OPERACAO_INICIADA);

    @GET
    @Path("gerar/{format}")
    @Produces({"application/pdf", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"})
    public Response buildAgendaPositiva(@PathParam("format") String format, @QueryParam("estagios") List<Estagio> estagios,
            @QueryParam("dataIni") Long dataIniMilliseconds,
            @QueryParam("dataFim") Long dataFimMilliseconds,            
            @QueryParam("diretoria") Long diretoria,            
            @QueryParam("todosEstagios") Set<Estagio> todosEstagios,
            @QueryParam("regioesPlanejamento") List<Long> regioesPlanejamento,
            @QueryParam("departamentos") List<Long> departamentos,
            @QueryParam("usuariosResponsaveis") List<Long> usuariosResponsaveis,
            @QueryParam("cadeiasProdutivas") List<Long> cadeiasProdutivas,
            @QueryParam("versao") Boolean versao,
            @HeaderParam("todasCadeiasProdutivas") String todasCadeiasProdutivas,
            @HeaderParam("todasGerencias") String todasGerencias,
            @HeaderParam("todasRegioesPlanejamento") String todasRegioesPlanejamento,
            @HeaderParam("todosAnalistas") String todosAnalistas, 
            @Context SecurityContext context) throws FileNotFoundException, IOException {

        try {
            Pair<List<Long>, List<Long>> filtrosFinais = validateAndRectifyFilter(usuariosResponsaveis, departamentos);
            
            Function<String,Set<Long>> parseLongSet = (param) -> Arrays.asList( param.split(",")).stream().map((p)-> Long.valueOf(p) ).collect(Collectors.toSet());
            
            Set<Long> todasAsGerencias = todasGerencias.equals("") ? null : parseLongSet.apply(todasGerencias);
            Set<Long> todasAsCadeiasProdutivas = todasCadeiasProdutivas == null || todasCadeiasProdutivas.equals("") ? null : parseLongSet.apply(todasCadeiasProdutivas) ;
            Set<Long> todasAsRegioesDePlanejamento = parseLongSet.apply(todasRegioesPlanejamento);
            Set<Long> todosOsAnalistas = todosAnalistas == null || todosAnalistas.equals("") ? null : parseLongSet.apply(todosAnalistas);
            
            FiltroAgendaPositiva filtroAgendaPositiva = new FiltroAgendaPositiva();
            
            Supplier<Boolean> buscarTodosEstagios = ()-> { Set<Estagio> estagiosSet = new HashSet<>(Optional.ofNullable(estagios).orElse(Collections.emptyList()));
                return estagiosSet.containsAll(todosEstagios);    
            };
            
            BiFunction<Set<Long>,List<Long>,Boolean> buscarTodos = (tudo,filtros) -> {
                Set<Long> filtroSet = filtros != null ? new HashSet<>(filtros) : Collections.emptySet();
                return filtroSet.containsAll(tudo);
            };  
            
            filtroAgendaPositiva.setEstagios(estagios);
            filtroAgendaPositiva.setPeriodoInicio(Optional.ofNullable(dataIniMilliseconds).map((l) -> new Date(l)).orElse(null));
            filtroAgendaPositiva.setPeriodoFim(Optional.ofNullable(dataFimMilliseconds).map((l) -> new Date(l)).orElse(null));
            filtroAgendaPositiva.setRegiao(new HashSet<>(regiaoPlanejamentoService.findByListIDs(new ArrayList(regioesPlanejamento))));
            filtroAgendaPositiva.setGerencias(new HashSet<>(domainTable.findByIds(Departamento.class, filtrosFinais.getRight())));
            filtroAgendaPositiva.setIdsGerencias(new HashSet<>(filtrosFinais.getRight()));
            filtroAgendaPositiva.setAnalistas(filtrosFinais.getLeft().isEmpty() ? Collections.EMPTY_SET
                    : new HashSet<>(user.findByListIDs(new ArrayList(filtrosFinais.getLeft()))));
            filtroAgendaPositiva.setIdsUsuarios(new HashSet<>(filtrosFinais.getLeft()));
            filtroAgendaPositiva.setCadeiasProdutivas(cadeiasProdutivas == null || cadeiasProdutivas.isEmpty() ? Collections.emptySet()
                : new HashSet<>(domainTable.findByIds(CadeiaProdutiva.class, cadeiasProdutivas)));
            filtroAgendaPositiva.setVersaoCorrente(versao);
            
            if (todasAsGerencias != null) {
                filtroAgendaPositiva.setBuscarTodasGerencias(buscarTodos.apply(todasAsGerencias, departamentos));
            }

            if (todasAsCadeiasProdutivas != null) {
                filtroAgendaPositiva.setBuscarTodasCadeiasProdutivas(buscarTodos.apply(todasAsCadeiasProdutivas,cadeiasProdutivas));
            }
            
            filtroAgendaPositiva.setBuscarTodasRegioes(buscarTodos.apply(todasAsRegioesDePlanejamento,regioesPlanejamento));
            
            if (todosOsAnalistas != null) {
                filtroAgendaPositiva.setBuscarTodosAnalistas(buscarTodos.apply(todosOsAnalistas,usuariosResponsaveis));
            }
            
            filtroAgendaPositiva.setBuscarTodosEstagios(buscarTodosEstagios.get());
            
            byte[] reportInputStream;
            if (format.equalsIgnoreCase(Formato.PDF.name())) {
                reportInputStream = gerencial03AgendaPositivaService.generatePDF(filtroAgendaPositiva, context);
            } else {
                reportInputStream = gerencial03AgendaPositivaService.generateXSL(filtroAgendaPositiva);
            }

        return Response.ok().type(EnumMimeTypes.valueOf(format.toUpperCase()).mimeType)
                .entity(reportInputStream)
                .header("Content-Disposition", "attachment; filename='AgendaPositiva'")
                .build();
        }catch (IllegalArgumentException e){
            logger.log(Level.SEVERE, e.getMessage(), e);            
            return Response.status(Response.Status.FORBIDDEN).entity(e.getMessage()).build();
        }catch (Exception e){
            logger.log(Level.SEVERE, e.getMessage(), e);            
            return Response.serverError().entity(e.getMessage()).build();
        }

    }

    @GET
    @Path("agendaPositivaTOByFilters")
    public Response findAgendaPositivaTOByFilters(@QueryParam("estagios") List<Estagio> estagios,
        @QueryParam("dataIni") Long dataIniMilliseconds,
        @QueryParam("dataFim") Long dataFimMilliseconds,
        @QueryParam("diretoria") Long diretoria,
        @QueryParam("regioesPlanejamento") List<Long> regioesPlanejamento,
        @QueryParam("departamentos") List<Long> departamentos,
        @QueryParam("usuariosResponsaveis") List<Long> usuariosResponsaveis,
        @QueryParam("cadeiasProdutivas") List<Long> cadeiasProdutivas,
        @QueryParam("versao") Boolean versao,
        @QueryParam("mesAno") Long mesAno,
        @QueryParam("notShowRealized") Boolean notShowRealized) {

        Calendar mesAnoIni = Calendar.getInstance();
        if (dataIniMilliseconds != null || mesAno != null) {
            if (dataIniMilliseconds != null) {
                mesAnoIni.setTimeInMillis(dataIniMilliseconds);
            } else {
                mesAnoIni.setTimeInMillis(mesAno);
            }            
            mesAnoIni.set(Calendar.DAY_OF_MONTH, 1);
            mesAnoIni.set(Calendar.HOUR, 0);
            mesAnoIni.set(Calendar.MINUTE, 0);
            mesAnoIni.set(Calendar.SECOND, 0);
        }
        
        Calendar mesAnoFim = Calendar.getInstance();
        if (dataFimMilliseconds != null || mesAno != null) {
            if (dataFimMilliseconds != null) {
                mesAnoFim.setTimeInMillis(dataFimMilliseconds);
            } else {
                mesAnoFim.setTimeInMillis(mesAno);                
            }            
            
            if (notShowRealized == null) {
                mesAnoFim.add(Calendar.MONTH, 1);
            }
            
            mesAnoFim.set(Calendar.DAY_OF_MONTH, mesAnoFim.getActualMaximum(Calendar.DAY_OF_MONTH));            
            mesAnoFim.set(Calendar.HOUR, mesAnoFim.getMaximum(Calendar.HOUR));
            mesAnoFim.set(Calendar.MINUTE, 59);
            mesAnoFim.set(Calendar.SECOND, 59);
        }
        
        Date dataIni = dataIniMilliseconds != null ? new Date(dataIniMilliseconds) : mesAno != null ? mesAnoIni.getTime() : null; 
        Date dataFim = dataFimMilliseconds != null ? new Date(dataFimMilliseconds) : mesAno != null ? mesAnoFim.getTime() : null;
        
        try {
            Pair<List<Long>, List<Long>> filtrosFinais = validateAndRectifyFilter(usuariosResponsaveis, departamentos);
            List<Estagio> estagiosPesquisa = Optional.ofNullable(estagios).orElse(estagiosValidos);
            if (estagiosPesquisa.isEmpty()) {
                estagiosPesquisa.addAll(estagiosValidos);
            }
            Map<Estagio, String> siglaPorEStagio = estagiosValidosPorSigla();
            final Set<String> siglasEstagiosPesquisa = Collections.unmodifiableSet(siglaPorEStagio.keySet().stream().filter((estagio) -> estagiosPesquisa.contains(estagio))
                    .map((ss) -> siglaPorEStagio.get(ss)).collect(Collectors.toSet()));
            Function<String, List<RelatorioAgendaPositivaAbstract>> searcher = (siglaEstagio) -> {
                return Optional.ofNullable(siglaEstagio).map((sigla) ->
                        projetoService.findAgendaPositivaTOByFilters(sigla, dataIni, dataFim, regioesPlanejamento,
                                filtrosFinais.getRight(), filtrosFinais.getLeft(), cadeiasProdutivas, diretoria, versao, notShowRealized)
                ).orElse(new ArrayList<>(0));
            };

            Stream<List<RelatorioAgendaPositivaAbstract>> response = estagiosValidos.stream()
                    .map((es) -> {
                        if (siglasEstagiosPesquisa.contains(siglaPorEStagio.get(es))) {
                            List<RelatorioAgendaPositivaAbstract> col = searcher.apply(siglaPorEStagio.get(es));
                            return col;
                        } else {
                            return new ArrayList<RelatorioAgendaPositivaAbstract>();
                        }

                    });
            List<List<RelatorioAgendaPositivaAbstract>> r = response.collect(Collectors.toList());
            return Response.ok(r).build();
        } catch (IllegalArgumentException e) {
           logger.log(Level.SEVERE, e.getMessage(), e);            
            return Response.status(Response.Status.FORBIDDEN).entity(e.getMessage()).build();
        }catch (Exception e){
            logger.log(Level.SEVERE, e.getMessage(), e);            
            return Response.serverError().entity(e.getMessage()).build();
        }

    }

    private Pair<List<Long>, List<Long>> validateAndRectifyFilter(List<Long> usuarios, List<Long> departamentos) {
        SecurityUser suser = (SecurityUser) securityContext.getUserPrincipal();
        Optional<Departamento> departamentoUsr = Optional.ofNullable(suser).map(SecurityUser::getDepartamento);
        Optional<GrupoUsuario> grupoUsr = Optional.ofNullable(suser).map(SecurityUser::getRole);
        List<Long> filtroInicialUsuarios = Optional.ofNullable(usuarios).orElse(Collections.emptyList());
        List<Long> filtroDepartamentos = Optional.ofNullable(departamentos).orElse(Collections.emptyList());
        List<Long> filtroFinalUsuarios = filtroInicialUsuarios;

        if (grupoUsr.isPresent() && !todosOsProjetos.contains(grupoUsr.get().getId())) {
            filtroDepartamentos = Collections.singletonList(departamentoUsr.map(Departamento::getId).orElse(0L));
            List<UsuarioInterno> usrsDepto = departamentoUsr.map((depto)
                    -> user.findAnalistasByDepartamentos(Collections.singletonList(depto.getId()))).orElse(Collections.emptyList());
            filtroDepartamentos = Collections.singletonList(departamentoUsr.map(Departamento::getId).orElse(0L));
            if (grupoUsr.get().getId() == GrupoUsuario.ANALISTA_DE_PROMOCAO) {
                filtroFinalUsuarios = Collections.singletonList(suser.getId());
            } else {
                Set<Long> setUsrsDept = usrsDepto.stream().map(UsuarioInterno::getId).collect(Collectors.toSet());
                //RETIRA DO FILTRO INICIAL, TODOS OS USUÁRIOS QUE NÃO SÃO DO DEPARTAMENTO EM QUESTÃO
                filtroFinalUsuarios = filtroInicialUsuarios.stream().filter((idUser) -> !setUsrsDept.contains(idUser)).collect(Collectors.toList());
            }
        } else if (!grupoUsr.isPresent() || !todosOsProjetos.contains(grupoUsr.get().getId())) {
            throw new IllegalArgumentException("Usuário do tipo \""
                    + grupoUsr.map(GrupoUsuario::getDescricao).orElse("Desconhecido") + "\" não possui privilégios para gerar este relatório");
        }

        return new ImmutablePair<>(filtroFinalUsuarios, filtroDepartamentos);

    }
    
    

    private Map<Estagio, String> estagiosValidosPorSigla() {
        Map<Estagio, String> ret = new HashMap<>();
        ret.put(Estagio.FORMALIZADA, "DF");
        ret.put(Estagio.IMPLANTACAO_INICIADO, "II");
        ret.put(Estagio.OPERACAO_INICIADA, "OI");
        return ret;
    }

}
