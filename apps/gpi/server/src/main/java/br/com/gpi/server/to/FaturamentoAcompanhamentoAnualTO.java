package br.com.gpi.server.to;

public class FaturamentoAcompanhamentoAnualTO extends AcompanhamentoTO{
    private Long id;
    private Long financeiroId;
    private Integer ano;
    private Double valorPrevisto;
    private Double valorRealizado;

    public Integer getAno() {
        return ano;
    }

    public void setAno(Integer ano) {
        this.ano = ano;
    }

    public Double getValorPrevisto() {
        return valorPrevisto;
    }

    public void setValorPrevisto(Double valorPrevisto) {
        this.valorPrevisto = valorPrevisto;
    }

    public Double getValorRealizado() {
        return valorRealizado;
    }

    public void setValorRealizado(Double valorRealizado) {
        this.valorRealizado = valorRealizado;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getFinanceiroId() {
        return financeiroId;
    }

    public void setFinanceiroId(Long financeiroId) {
        this.financeiroId = financeiroId;
    }
    
}
