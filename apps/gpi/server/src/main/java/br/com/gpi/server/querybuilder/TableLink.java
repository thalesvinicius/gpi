package br.com.gpi.server.querybuilder;

public class TableLink {

    private Table toTable;
    private String fkName;

    protected TableLink() {
        super();
    }

    public TableLink(Table toTable, String fkName) {
        this.toTable = toTable;
        this.fkName = fkName;
    }

    public String getFkName() {
        return fkName;
    }

    public void setFkName(String fkName) {
        this.fkName = fkName;
    }

    public Table getToTable() {
        return toTable;
    }

    public void setToTable(Table toTable) {
        this.toTable = toTable;
    }

    public String createLinkStatement(Table fromTable) {
        StringBuilder result = new StringBuilder();
        result.append(" INNER JOIN ");
        result.append(toTable.getName());
        result.append(" ");
        result.append(toTable.getAlias());
        result.append(" ON ");
        result.append(toTable.getAlias()).append(".").append(toTable.getPkName());
        result.append(" = ");
        result.append(fromTable.getAlias()).append(".").append(fkName);
        return result.toString();
    }

}
