package br.com.gpi.server.core.message;

import br.com.gpi.server.core.Custom;
import java.io.Serializable;
import java.text.MessageFormat;
import java.util.Locale;
import org.apache.deltaspike.core.api.message.MessageInterpolator;

@Custom
public class MessageInterpolatorImpl implements MessageInterpolator {

    @Override
    public String interpolate(String messageText, Serializable[] arguments, Locale locale) {
        return MessageFormat.format(messageText, (Object[]) arguments);
    }

}
