package br.com.gpi.server.domain.entity;

import br.com.gpi.server.domain.common.BaseDomainTableEntity;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import org.hibernate.envers.Audited;

@Audited
@Entity
@Table(name = "GrupoUsuario")
public class GrupoUsuario extends BaseDomainTableEntity {

    public final static Long ADMINISTRADOR_DE_TI = 1L;
    public final static Long ANALISTA_DE_INFRA_E_MEIO_AMBIENTE = 2L;
    public final static Long ANALISTA_DE_INFORMACAO = 3L;
    public final static Long ANALISTA_DE_PORTFOLIO = 4L;
    public final static Long ANALISTA_DE_PROMOCAO = 5L;
    public final static Long DIRETOR = 6L;
    public final static Long GERENTE = 7L;
    public final static Long SECRETARIA_GERAL = 8L;
    public final static Long USUARIO_EXTERNO_RESPONSAVEL = 9L;
    public final static Long USUARIO_EXTERNO_OUTROS = 10L;

    @NotNull
    @Column(name = "descricao", length = 60, nullable = false)
    private String descricao;

    public GrupoUsuario() {
    }

    public GrupoUsuario(Long id, String descricao) {
        this.setId(id);
        this.descricao = descricao;
    }

    @Override
    public String getDescricao() {
        return this.descricao;
    }

    @Override
    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 37 * hash + Objects.hashCode(this.descricao);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final GrupoUsuario other = (GrupoUsuario) obj;
        if (!Objects.equals(this.descricao, other.descricao)) {
            return false;
        }
        return true;
    }

}
