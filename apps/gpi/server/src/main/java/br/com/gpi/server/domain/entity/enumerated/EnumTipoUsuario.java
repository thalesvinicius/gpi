package br.com.gpi.server.domain.entity.enumerated;

import br.com.gpi.server.core.exception.EnumException;
import br.com.gpi.server.core.jpa.typesafe.EnumerationEntityType;

public enum EnumTipoUsuario implements EnumerationEntityType<Integer> {

    INTERNO(1), EXTERNO(2);

    private final int id;

    private EnumTipoUsuario(int id) {
        this.id = id;
    }

    @Override
    public Integer getId() {
        return this.id;
    }

    @Override
    public String getDescription() {
        return this.toString();
    }

    public static EnumTipoUsuario getEnumUsuario(Integer id) {
        for (EnumTipoUsuario tipoUsuario : values()) {
            if (tipoUsuario.id == id) {
                return tipoUsuario;
            }
        }
        throw new EnumException("EnumTipoUsuario couldn't load.");
    }
}
