package br.com.gpi.server.domain.entity;

import br.com.gpi.server.core.jpa.typesafe.EnumerationEntityType;

public enum SituacaoRelatorioEnum implements EnumerationEntityType<Integer> {

    NAO_SOLICITADO(0, "Não Solicitado pelo INDI"),
    NAO_RESPONDIDO(1, "Não Respondido pela Empresa"),
    PENDENTE_VALIDACAO(2, "Pendente de Validação"),
    VALIDADO(3, "Validado pelo Analista"),
    TODOS(4, "Todos");

    private SituacaoRelatorioEnum(int id, String descricao) {
        this.id = id;
        this.descricao = descricao;
    }

    private final int id;
    private final String descricao;

    @Override
    public Integer getId() {
        return this.id;
    }

    @Override
    public String getDescription() {
        return this.descricao;
    }
    
    public String getSigla(Integer id) {
        switch (id) {
            case 0: 
                return "Não Solicitado pelo INDI";
            case 1:
                return "Não Respondido pela Empresa";
            case 2:
                return "Pendente de Validação";
            case 3:
                return "Validado pelo Analista";
            default:
                return "";
        }
        
    }
}
