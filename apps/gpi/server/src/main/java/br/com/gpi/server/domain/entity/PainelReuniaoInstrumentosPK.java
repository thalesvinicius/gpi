package br.com.gpi.server.domain.entity;

import java.io.Serializable;
import java.util.Objects;

public class PainelReuniaoInstrumentosPK implements Serializable {   
    private Long instrumentoId;
    private Long projetoId;

    /**
     * @return the instrumentoId
     */
    public Long getInstrumentoId() {
        return instrumentoId;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 53 * hash + Objects.hashCode(this.instrumentoId);
        hash = 53 * hash + Objects.hashCode(this.projetoId);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final PainelReuniaoInstrumentosPK other = (PainelReuniaoInstrumentosPK) obj;
        if (!Objects.equals(this.instrumentoId, other.instrumentoId)) {
            return false;
        }
        if (!Objects.equals(this.projetoId, other.projetoId)) {
            return false;
        }
        return true;
    }

    /**
     * @param instrumentoId the instrumentoId to set
     */
    public void setInstrumentoId(Long instrumentoId) {
        this.instrumentoId = instrumentoId;
    }

    /**
     * @return the projetoId
     */
    public Long getProjetoId() {
        return projetoId;
    }

    /**
     * @param projetoId the projetoId to set
     */
    public void setProjetoId(Long projetoId) {
        this.projetoId = projetoId;
    }    
    
}