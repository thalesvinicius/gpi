package br.com.gpi.server.domain.service.reports.gerencial;

import br.com.gpi.server.domain.common.QueryParameter;
import br.com.gpi.server.domain.entity.Estagio;
import br.com.gpi.server.domain.entity.Meta;
import br.com.gpi.server.domain.entity.PainelIndiceSatisfacao;
import br.com.gpi.server.domain.entity.PainelIndiceSatisfacao_;
import br.com.gpi.server.domain.entity.PainelMetas;
import br.com.gpi.server.domain.entity.PainelMetas_;
import br.com.gpi.server.domain.entity.PainelReuniaoInstrumentos;
import br.com.gpi.server.domain.entity.PainelReuniaoInstrumentosAno;
import br.com.gpi.server.domain.entity.PainelReuniaoInstrumentosAno_;
import br.com.gpi.server.domain.entity.PainelReuniaoInstrumentosMesAno;
import br.com.gpi.server.domain.entity.PainelReuniaoInstrumentosMesAno_;
import br.com.gpi.server.domain.entity.PainelReuniaoInstrumentos_;
import br.com.gpi.server.domain.entity.PainelReuniaoProjetos;
import br.com.gpi.server.domain.entity.PainelReuniaoProjetos_;
import br.com.gpi.server.domain.entity.RelatorioPesquisasRecebidas;
import br.com.gpi.server.domain.entity.RelatorioPesquisasRecebidas_;
import br.com.gpi.server.domain.entity.SituacaoInstrFormalizacao;
import br.com.gpi.server.domain.entity.SituacaoProjetoEnum;
import br.com.gpi.server.domain.entity.TipoInstrFormalizacao;
import br.com.gpi.server.domain.entity.enumerated.EnumIndicativoMeta;
import br.com.gpi.server.domain.service.reports.BaseReportService;
import br.com.gpi.server.to.ProjetosCount;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.TreeSet;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

public class PainelReuniaoGerencialService extends BaseReportService<Object> {

    @Inject
    public PainelReuniaoGerencialService(EntityManager em) {
        super(em);
    }

    public List<PainelReuniaoProjetos> findProjetosByFilters(QueryParameter queryParam) {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<PainelReuniaoProjetos> query = builder.createQuery(PainelReuniaoProjetos.class);
        Root<PainelReuniaoProjetos> fromPainelReuniaoProjetos = query.from(PainelReuniaoProjetos.class);
        List<Predicate> conditions = new ArrayList();

        addProjectFilters(queryParam, fromPainelReuniaoProjetos, conditions, builder);
        addProjectPanelFilters(queryParam, fromPainelReuniaoProjetos, conditions, builder);

        if (queryParam.exists("estagioProjetoId")) {
            conditions.add(builder.equal(fromPainelReuniaoProjetos.get(PainelReuniaoProjetos_.projetoEstagioatualId), queryParam.get("estagioProjetoId")));
        }

        if (queryParam.exists("situacaoProjetoId")) {
            conditions.add(builder.equal(fromPainelReuniaoProjetos.get(PainelReuniaoProjetos_.situacaoProjetoId), queryParam.get("situacaoProjetoId")));
        }

        if (queryParam.exists("mesAno") || queryParam.exists("anual")) {
            if (queryParam.exists("estagioProjetoId")
                    && ((Integer) queryParam.get("estagioProjetoId")).compareTo(Estagio.INICIO_PROJETO.getId()) == 0) {
                Calendar dataComparacao = Calendar.getInstance();
                dataComparacao.add(Calendar.DAY_OF_MONTH, -120);
                conditions.add(builder.lessThan(fromPainelReuniaoProjetos.get(PainelReuniaoProjetos_.dataEstagioAtual), dataComparacao.getTime()));
            } else {
                Calendar hoje = Calendar.getInstance();

                Calendar dataInicial = Calendar.getInstance();
                dataInicial.setTimeInMillis((Long) queryParam.get("mesAno"));
                dataInicial.set(Calendar.DAY_OF_MONTH, 1);
                dataInicial.set(Calendar.HOUR_OF_DAY, 0);
                dataInicial.set(Calendar.MINUTE, 0);
                if (queryParam.exists("anual") && (Boolean) queryParam.get("anual")) {
                    dataInicial.set(Calendar.MONTH, Calendar.JANUARY);
                }

                Calendar dataFinal = Calendar.getInstance();
                dataFinal.setTimeInMillis((Long) queryParam.get("mesAno"));
                if (dataFinal.get(Calendar.MONTH) != hoje.get(Calendar.MONTH) || dataFinal.get(Calendar.YEAR) != hoje.get(Calendar.YEAR)) {
                    dataFinal.set(Calendar.DAY_OF_MONTH, dataFinal.getActualMaximum(Calendar.DAY_OF_MONTH));
                } else {
                    dataFinal.set(Calendar.DAY_OF_MONTH, hoje.get(Calendar.DAY_OF_MONTH));
                }
                dataFinal.set(Calendar.HOUR_OF_DAY, 23);
                dataFinal.set(Calendar.MINUTE, 59);

                if (queryParam.exists("situacaoProjetoId") && queryParam.get("situacaoProjetoId").equals(SituacaoProjetoEnum.CANCELADO.getId())) {
                    conditions.add(builder.between(fromPainelReuniaoProjetos.get(PainelReuniaoProjetos_.dataCancelamentoProjeto), dataInicial.getTime(), dataFinal.getTime()));
                } else {
                    conditions.add(builder.between(fromPainelReuniaoProjetos.get(PainelReuniaoProjetos_.dataEstagioAtual), dataInicial.getTime(), dataFinal.getTime()));
                }
            }
        }

        if (queryParam.exists("dataInicio") && queryParam.exists("dataFim")) {
            Instant instant = Instant.ofEpochMilli((Long) queryParam.get("dataInicio"));
            LocalDateTime dataInicio = LocalDateTime.ofInstant(instant, ZoneOffset.UTC);
            dataInicio = dataInicio.minusHours(dataInicio.getHour())
                    .minusMinutes(dataInicio.getMinute())
                    .minusSeconds(dataInicio.getSecond());

            Instant instant2 = Instant.ofEpochMilli((Long) queryParam.get("dataFim"));
            LocalDateTime dataFim = LocalDateTime.ofInstant(instant2, ZoneOffset.UTC);
            dataFim = LocalDateTime.of(dataFim.getYear(), dataFim.getMonth(), dataFim.getDayOfMonth(), 23, 59, 0);

            if (queryParam.exists("estagioId")) {
                conditions.add(builder.between(fromPainelReuniaoProjetos.get(PainelReuniaoProjetos_.dataEstagioDecisaoFormalizada), Date.from(dataInicio.toInstant(ZoneOffset.UTC)), Date.from(dataFim.toInstant(ZoneOffset.UTC))));
            } else {
                conditions.add(builder.between(fromPainelReuniaoProjetos.get(PainelReuniaoProjetos_.dataEstagioAtual), Date.from(dataInicio.toInstant(ZoneOffset.UTC)), Date.from(dataFim.toInstant(ZoneOffset.UTC))));
            }
        }

        TypedQuery<PainelReuniaoProjetos> typedQuery = entityManager.createQuery(query
                .where(conditions.toArray(new Predicate[]{}))
                .orderBy(builder.asc(fromPainelReuniaoProjetos.get(PainelReuniaoProjetos_.empresa)))
                .distinct(true)
        );

        List<PainelReuniaoProjetos> resultList = typedQuery.getResultList();

        return resultList;
    }

    public void addProjectPanelFilters(QueryParameter queryParam, Root<PainelReuniaoProjetos> fromPainelReuniaoProjetos, List<Predicate> conditions, CriteriaBuilder builder) {
        if (queryParam.exists("diretoria")) {
            conditions.add(builder.equal(fromPainelReuniaoProjetos.get(PainelReuniaoProjetos_.diretoriaId), queryParam.get("diretoria")));
//              conditions.add(builder.or(builder.equal(fromPainelReuniaoProjetos.get(PainelReuniaoProjetos_.diretoriaId), queryParam.get("diretoria")), 
//                builder.and(builder.isNull(fromPainelReuniaoProjetos.get(PainelReuniaoProjetos_.diretoriaId)), 
//                    builder.equal(fromPainelReuniaoProjetos.get(PainelReuniaoProjetos_.gerenciaId), queryParam.get("diretoria")))));              
        }
    }

    public void addProjectFilters(QueryParameter queryParam, Root<PainelReuniaoProjetos> fromPainelReuniaoProjetos, List<Predicate> conditions, CriteriaBuilder builder) {
        if (queryParam.exists("estagios")) {
            List<Estagio> estagios = (List<Estagio>) queryParam.get("estagios");
            List<Integer> estagiosId = new ArrayList<>();
            estagios.stream().forEach(e -> estagiosId.add(e.getId()));
            conditions.add(fromPainelReuniaoProjetos.get(PainelReuniaoProjetos_.projetoEstagioatualId).in(estagiosId));
        }
        if (queryParam.exists("regioesPlanejamento")) {
            conditions.add(fromPainelReuniaoProjetos.get(PainelReuniaoProjetos_.regiaoPlanejamentoId).in((List<Long>) queryParam.get("regioesPlanejamento")));
        }
        if (queryParam.exists("gerencias")) {
            conditions.add(fromPainelReuniaoProjetos.get(PainelReuniaoProjetos_.gerenciaId).in((List<Long>) queryParam.get("gerencias")));
        }
        if (queryParam.exists("cadeiasProdutivas")) {
            conditions.add(fromPainelReuniaoProjetos.get(PainelReuniaoProjetos_.cadeiaProdutivaId).in((List<Long>) queryParam.get("cadeiasProdutivas")));
        }
        if (queryParam.exists("prospeccaoAtiva")) {
            conditions.add(builder.equal(fromPainelReuniaoProjetos.get(PainelReuniaoProjetos_.prospeccaoAtiva), queryParam.get("prospeccaoAtiva")));
        }
    }

    public List<PainelReuniaoInstrumentos> findProtocolosByFilters(QueryParameter queryParam) {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<PainelReuniaoInstrumentos> query = builder.createQuery(PainelReuniaoInstrumentos.class);
        Root<PainelReuniaoInstrumentos> fromPainelReuniaoInstrumentos = query.from(PainelReuniaoInstrumentos.class);
        List<Predicate> conditions = new ArrayList();

        addProtocolFilters(queryParam, fromPainelReuniaoInstrumentos, conditions, builder);
        addProtocolPanelFilters(queryParam, fromPainelReuniaoInstrumentos, conditions, builder);

        if (queryParam.exists("situacaoProjetoId")) {
            conditions.add(builder.equal(fromPainelReuniaoInstrumentos.get(PainelReuniaoInstrumentos_.situacaoProjetoId), queryParam.get("situacaoProjetoId")));
        }

        if (queryParam.exists("situacaoInstrumentoId")) {
            conditions.add(builder.equal(fromPainelReuniaoInstrumentos.get(PainelReuniaoInstrumentos_.situacaoInstrumentoId), queryParam.get("situacaoInstrumentoId")));
        }

        if (queryParam.exists("tipoInstrumentoId")) {
            conditions.add(fromPainelReuniaoInstrumentos.get(PainelReuniaoInstrumentos_.tipoInstrumentoId).in(queryParam.get("tipoInstrumentoId")));
        }

        if (queryParam.exists("mesAno") || queryParam.exists("anual")) {
            Calendar hoje = Calendar.getInstance();

            Calendar dataInicial = Calendar.getInstance();
            dataInicial.setTimeInMillis((Long) queryParam.get("mesAno"));
            dataInicial.set(Calendar.DAY_OF_MONTH, 1);
            dataInicial.set(Calendar.HOUR_OF_DAY, 0);
            dataInicial.set(Calendar.MINUTE, 0);
            if (queryParam.exists("anual") && (Boolean) queryParam.get("anual")) {
                dataInicial.set(Calendar.MONTH, Calendar.JANUARY);
            }

            Calendar dataFinal = Calendar.getInstance();
            dataFinal.setTimeInMillis((Long) queryParam.get("mesAno"));
            if (dataFinal.get(Calendar.MONTH) != hoje.get(Calendar.MONTH) || dataFinal.get(Calendar.YEAR) != hoje.get(Calendar.YEAR)) {
                dataFinal.set(Calendar.DAY_OF_MONTH, dataFinal.getActualMaximum(Calendar.DAY_OF_MONTH));
            } else {
                dataFinal.set(Calendar.DAY_OF_MONTH, hoje.get(Calendar.DAY_OF_MONTH));
            }
            dataFinal.set(Calendar.HOUR_OF_DAY, 23);
            dataFinal.set(Calendar.MINUTE, 59);

            if (queryParam.exists("situacaoInstrumentoId")) {
                if (queryParam.get("situacaoInstrumentoId").equals(SituacaoInstrFormalizacao.CANCELADO.getId())) {
                    conditions.add(builder.between(fromPainelReuniaoInstrumentos.get(PainelReuniaoInstrumentos_.dataCancelamentoInstrumento), dataInicial.getTime(), dataFinal.getTime()));
                } else if (queryParam.get("situacaoInstrumentoId").equals(SituacaoInstrFormalizacao.EM_NEGOCIACAO.getId())) {
                    conditions.add(builder.lessThan(fromPainelReuniaoInstrumentos.get(PainelReuniaoInstrumentos_.dataAssinaturaPrevistaInstrumento), dataFinal.getTime()));
                    conditions.add(builder.isNull(fromPainelReuniaoInstrumentos.get(PainelReuniaoInstrumentos_.dataAssinaturaInstrumento)));
                }
            }
        }

        TypedQuery<PainelReuniaoInstrumentos> typedQuery = entityManager.createQuery(query
                .where(conditions.toArray(new Predicate[]{}))
                .orderBy(builder.asc(fromPainelReuniaoInstrumentos.get(PainelReuniaoInstrumentos_.empresa)))
                .distinct(true)
        );

        List<PainelReuniaoInstrumentos> resultList = typedQuery.getResultList();

        return resultList;
    }

    public void addProtocolPanelFilters(QueryParameter queryParam, Root<PainelReuniaoInstrumentos> fromPainelReuniaoInstrumentos, List<Predicate> conditions, CriteriaBuilder builder) {
        if (queryParam.exists("diretoria")) {
            conditions.add(builder.equal(fromPainelReuniaoInstrumentos.get(PainelReuniaoInstrumentos_.diretoriaId), queryParam.get("diretoria")));
        }
    }

    public void addProtocolFilters(QueryParameter queryParam, Root<PainelReuniaoInstrumentos> fromPainelReuniaoInstrumentos, List<Predicate> conditions, CriteriaBuilder builder) {
        if (queryParam.exists("regioesPlanejamento")) {
            conditions.add(fromPainelReuniaoInstrumentos.get(PainelReuniaoInstrumentos_.regiaoPlanejamentoId).in((List<Long>) queryParam.get("regioesPlanejamento")));
        }
        if (queryParam.exists("gerencias")) {
            conditions.add(fromPainelReuniaoInstrumentos.get(PainelReuniaoInstrumentos_.gerenciaId).in((List<Long>) queryParam.get("gerencias")));
        }
        if (queryParam.exists("cadeiasProdutivas")) {
            conditions.add(fromPainelReuniaoInstrumentos.get(PainelReuniaoInstrumentos_.cadeiaProdutivaId).in((List<Long>) queryParam.get("cadeiasProdutivas")));
        }
    }

    public List<RelatorioPesquisasRecebidas> findPesquisasRecebidasByFilters(QueryParameter queryParam) {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<RelatorioPesquisasRecebidas> query = builder.createQuery(RelatorioPesquisasRecebidas.class);
        Root<RelatorioPesquisasRecebidas> fromPesquisasRecebidas = query.from(RelatorioPesquisasRecebidas.class);
        List<Predicate> conditions = new ArrayList();

        if (queryParam.exists("diretoria")) {
            conditions.add(builder.equal(fromPesquisasRecebidas.get(RelatorioPesquisasRecebidas_.diretoriaId), queryParam.get("diretoria")));
        }

        if (queryParam.exists("gerencias")) {
            conditions.add(fromPesquisasRecebidas.get(RelatorioPesquisasRecebidas_.gerenciaId).in((List<Long>) queryParam.get("gerencias")));
        }

        if (queryParam.exists("mesAno")) {
            Calendar dataInicial = Calendar.getInstance();
            dataInicial.setTimeInMillis((Long) queryParam.get("mesAno"));
            dataInicial.set(Calendar.DAY_OF_MONTH, 1);
            dataInicial.set(Calendar.HOUR_OF_DAY, 0);
            dataInicial.set(Calendar.MINUTE, 0);
            if (queryParam.exists("anual") && (Boolean) queryParam.get("anual")) {
                dataInicial.set(Calendar.MONTH, Calendar.JANUARY);
            }

            Calendar dataFinal = Calendar.getInstance();
            dataFinal.setTimeInMillis((Long) queryParam.get("mesAno"));
            dataFinal.set(Calendar.DAY_OF_MONTH, dataFinal.getActualMaximum(Calendar.DAY_OF_MONTH));
            dataFinal.set(Calendar.HOUR_OF_DAY, 23);
            dataFinal.set(Calendar.MINUTE, 59);

            conditions.add(builder.between(fromPesquisasRecebidas.get(RelatorioPesquisasRecebidas_.dataRecebimento), dataInicial.getTime(), dataFinal.getTime()));
        }

        TypedQuery typedQuery = entityManager.createQuery(query
                .where(conditions.toArray(new Predicate[]{}))
                .orderBy(builder.asc(fromPesquisasRecebidas.get(RelatorioPesquisasRecebidas_.empresa))));

        return typedQuery.getResultList();
    }
    
    // Projetos no Ano (Cadeia Produtiva)
    public List<ProjetosCount> countProjectsByFiltersGroupByCadeiaProdutiva(QueryParameter queryParam) {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<ProjetosCount> query = builder.createQuery(ProjetosCount.class);
        Root<PainelReuniaoInstrumentos> fromPainelReuniaoInstrumentos = query.from(PainelReuniaoInstrumentos.class);
        List<Predicate> conditions = new ArrayList();

        Expression total = null;
        if (queryParam.exists("termo")) {
            String termo = (String) queryParam.get("termo");
            if (termo.equals("faturamentoPleno")) {
                total = builder.sum(fromPainelReuniaoInstrumentos.get(PainelReuniaoInstrumentos_.faturamentoPleno));
            } else if (termo.equals("investimentoPrevisto")) {
                total = builder.sum(fromPainelReuniaoInstrumentos.get(PainelReuniaoInstrumentos_.investimentoPrevisto));
            } else if (termo.equals("empregosDiretos")) {
                total = builder.sum(fromPainelReuniaoInstrumentos.get(PainelReuniaoInstrumentos_.empregosDiretos));
            } else if (termo.equals("protocolos")) {
                total = builder.countDistinct(fromPainelReuniaoInstrumentos.get(PainelReuniaoInstrumentos_.instrumentoId));
            }
        } else {
            total = builder.countDistinct(fromPainelReuniaoInstrumentos.get(PainelReuniaoInstrumentos_.instrumentoId));
        }

        if (queryParam.exists("diretoria")) {
            conditions.add(builder.equal(fromPainelReuniaoInstrumentos.get(PainelReuniaoInstrumentos_.diretoriaId), queryParam.get("diretoria")));
        }
        if (queryParam.exists("gerencias")) {
            conditions.add(fromPainelReuniaoInstrumentos.get(PainelReuniaoInstrumentos_.gerenciaId).in((List<Long>) queryParam.get("gerencias")));
        }
        if (queryParam.exists("cadeiasProdutivas")) {
            conditions.add(fromPainelReuniaoInstrumentos.get(PainelReuniaoInstrumentos_.cadeiaProdutivaId).in((List<Long>) queryParam.get("cadeiasProdutivas")));
        }

        /* Ao gerar o Relatório, o sistema deverá considerar como padrão o filtro de tipo 
         * como “Protocolo de Intenções” */
        if (queryParam.exists("tiposInstrumentoFormalizacao")) {
            List<Integer> tiposInstrumentoIds = new ArrayList<>();
            ((List<TipoInstrFormalizacao>) queryParam.get("tiposInstrumentoFormalizacao")).stream().forEach(tif -> {
                tiposInstrumentoIds.add(tif.getId());
            });
            conditions.add(fromPainelReuniaoInstrumentos.get(PainelReuniaoInstrumentos_.tipoInstrumentoId).in(tiposInstrumentoIds));
        }
        if (queryParam.exists("mesAno")) {
            Calendar dataInicial = Calendar.getInstance();
            dataInicial.setTimeInMillis((Long) queryParam.get("mesAno"));
            dataInicial.set(Calendar.DAY_OF_MONTH, 1);
            dataInicial.set(Calendar.MONTH, Calendar.JANUARY);

            Calendar dataFinal = Calendar.getInstance();
            dataFinal.setTimeInMillis((Long) queryParam.get("mesAno"));
            dataFinal.set(Calendar.DAY_OF_MONTH, dataFinal.getActualMaximum(Calendar.DAY_OF_MONTH));

            conditions.add(builder.between(fromPainelReuniaoInstrumentos.get(PainelReuniaoInstrumentos_.dataAssinaturaInstrumento), dataInicial.getTime(), dataFinal.getTime()));
        }

        TypedQuery<ProjetosCount> typedQuery = entityManager.createQuery(query
                .where(conditions.toArray(new Predicate[]{}))
                .multiselect(fromPainelReuniaoInstrumentos.get(PainelReuniaoInstrumentos_.cadeiaProdutiva), total)
                .groupBy(fromPainelReuniaoInstrumentos.get(PainelReuniaoInstrumentos_.cadeiaProdutiva))
        );

        return typedQuery.getResultList();
    }

    //Protocolos Assinados no Ano
    public List<Object[]> countProtocolosAssinadosByFiltersGroupByDiretoriaAndMesAndAnoAssinatura(QueryParameter queryParam) {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<PainelReuniaoInstrumentosMesAno> query = builder.createQuery(PainelReuniaoInstrumentosMesAno.class);
        Root<PainelReuniaoInstrumentosMesAno> fromPainelReuniaoInstrumentos = query.from(PainelReuniaoInstrumentosMesAno.class);
        List<Predicate> conditions = new ArrayList();

        Expression total = builder.sum(fromPainelReuniaoInstrumentos.get(PainelReuniaoInstrumentosMesAno_.numProtocolos));

        if (queryParam.exists("diretoria")) {
            conditions.add(builder.equal(fromPainelReuniaoInstrumentos.get(PainelReuniaoInstrumentosMesAno_.diretoriaId), queryParam.get("diretoria")));
        }
        if (queryParam.exists("gerencias")) {
            conditions.add(fromPainelReuniaoInstrumentos.get(PainelReuniaoInstrumentosMesAno_.gerenciaId).in((List<Long>) queryParam.get("gerencias")));
        }
        if (queryParam.exists("tiposInstrumentoFormalizacao")) {
            List<Integer> tiposInstrumentoIds = new ArrayList<>();
            ((List<TipoInstrFormalizacao>) queryParam.get("tiposInstrumentoFormalizacao")).stream().forEach(tif -> {
                tiposInstrumentoIds.add(tif.getId());
            });
            conditions.add(fromPainelReuniaoInstrumentos.get(PainelReuniaoInstrumentosMesAno_.tipoInstrumentoId).in(tiposInstrumentoIds));
        } else {
            conditions.add(builder.equal(fromPainelReuniaoInstrumentos.get(PainelReuniaoInstrumentosMesAno_.tipoInstrumentoId), TipoInstrFormalizacao.PROTOCOLO_INTENCAO.getId()));
        }
//        if (queryParam.exists("mesAno")) {
        Calendar mesAno = Calendar.getInstance();
        mesAno.setTimeInMillis((Long) queryParam.get("mesAno"));
        conditions.add(builder.between(fromPainelReuniaoInstrumentos.get(PainelReuniaoInstrumentosMesAno_.mesAssinaturaInstrumento), 1, mesAno.get(Calendar.MONTH) + 1));
        conditions.add(builder.equal(fromPainelReuniaoInstrumentos.get(PainelReuniaoInstrumentosMesAno_.anoAssinaturaInstrumento), mesAno.get(Calendar.YEAR)));
//        }

        TypedQuery<PainelReuniaoInstrumentosMesAno> typedQuery = entityManager.createQuery(query
                .where(conditions.toArray(new Predicate[]{}))
                .multiselect(fromPainelReuniaoInstrumentos.get(PainelReuniaoInstrumentosMesAno_.gerenciaId),
                        fromPainelReuniaoInstrumentos.get(PainelReuniaoInstrumentosMesAno_.gerencia),
                        fromPainelReuniaoInstrumentos.get(PainelReuniaoInstrumentosMesAno_.mesAssinaturaInstrumento),
                        fromPainelReuniaoInstrumentos.get(PainelReuniaoInstrumentosMesAno_.anoAssinaturaInstrumento), total)
                .groupBy(fromPainelReuniaoInstrumentos.get(PainelReuniaoInstrumentosMesAno_.gerenciaId),
                        fromPainelReuniaoInstrumentos.get(PainelReuniaoInstrumentosMesAno_.gerencia),
                        fromPainelReuniaoInstrumentos.get(PainelReuniaoInstrumentosMesAno_.mesAssinaturaInstrumento),
                        fromPainelReuniaoInstrumentos.get(PainelReuniaoInstrumentosMesAno_.anoAssinaturaInstrumento))
                .orderBy(builder.asc(fromPainelReuniaoInstrumentos.get(PainelReuniaoInstrumentosMesAno_.gerencia)),
                        builder.asc(fromPainelReuniaoInstrumentos.get(PainelReuniaoInstrumentosMesAno_.mesAssinaturaInstrumento)),
                        builder.asc(fromPainelReuniaoInstrumentos.get(PainelReuniaoInstrumentosMesAno_.anoAssinaturaInstrumento))));

        List<PainelReuniaoInstrumentosMesAno> resultadoQuery = typedQuery.getResultList();
        Map<String, Object[]> resultadoMap = new HashMap<>();

        for (PainelReuniaoInstrumentosMesAno tuplaQuery : resultadoQuery) {
            Object[] protocolos;
            if (resultadoMap.containsKey(tuplaQuery.getGerencia().split(" - ")[0].trim())) {
                protocolos = resultadoMap.get(tuplaQuery.getGerencia().split(" - ")[0].trim());
            } else {
                protocolos = new Object[14];
                resultadoMap.put(tuplaQuery.getGerencia().split(" - ")[0].trim(), protocolos);
            }

            protocolos[tuplaQuery.getMesAssinaturaInstrumento() + 1] = tuplaQuery.getNumProtocolos();
        }

        countProtocolosAssinadosByFiltersGroupByDiretoriaAndAnoAssinatura(queryParam, resultadoMap, mesAno.get(Calendar.YEAR) - 1, 1);
        countProtocolosAssinadosByFiltersGroupByDiretoriaAndAnoAssinatura(queryParam, resultadoMap, mesAno.get(Calendar.YEAR) - 2, 0);

        generateGoalResults(resultadoMap, EnumIndicativoMeta.PROTOCOLOS_ASSINADOS_ANO, mesAno.get(Calendar.YEAR), mesAno.get(Calendar.MONTH) + 1, 2);

        return painelMapObjectToListObject(resultadoMap, 15);
    }

    private void countProtocolosAssinadosByFiltersGroupByDiretoriaAndAnoAssinatura(QueryParameter queryParam, Map<String, Object[]> resultadoMap, Integer ano, Integer posicao) {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<PainelReuniaoInstrumentosAno> query = builder.createQuery(PainelReuniaoInstrumentosAno.class);
        Root<PainelReuniaoInstrumentosAno> fromPainelReuniaoInstrumentos = query.from(PainelReuniaoInstrumentosAno.class);
        List<Predicate> conditions = new ArrayList();

        Expression total = builder.sum(fromPainelReuniaoInstrumentos.get(PainelReuniaoInstrumentosAno_.numProtocolos));

        if (queryParam.exists("diretoria")) {
            conditions.add(builder.equal(fromPainelReuniaoInstrumentos.get(PainelReuniaoInstrumentosAno_.diretoriaId), queryParam.get("diretoria")));
        }
        if (queryParam.exists("gerencias")) {
            conditions.add(fromPainelReuniaoInstrumentos.get(PainelReuniaoInstrumentosAno_.gerenciaId).in((List<Long>) queryParam.get("gerencias")));
        }
        if (queryParam.exists("tiposInstrumentoFormalizacao")) {
            List<Integer> tiposInstrumentoIds = new ArrayList<>();
            ((List<TipoInstrFormalizacao>) queryParam.get("tiposInstrumentoFormalizacao")).stream().forEach(tif -> {
                tiposInstrumentoIds.add(tif.getId());
            });
            conditions.add(fromPainelReuniaoInstrumentos.get(PainelReuniaoInstrumentosAno_.tipoInstrumentoId).in(tiposInstrumentoIds));
        } else {
            conditions.add(builder.equal(fromPainelReuniaoInstrumentos.get(PainelReuniaoInstrumentosAno_.tipoInstrumentoId), TipoInstrFormalizacao.PROTOCOLO_INTENCAO.getId()));
        }

        conditions.add(builder.equal(fromPainelReuniaoInstrumentos.get(PainelReuniaoInstrumentosAno_.anoAssinaturaInstrumento), ano));

        TypedQuery<PainelReuniaoInstrumentosAno> typedQuery = entityManager.createQuery(query
                .where(conditions.toArray(new Predicate[]{}))
                .multiselect(fromPainelReuniaoInstrumentos.get(PainelReuniaoInstrumentosAno_.gerenciaId),
                        fromPainelReuniaoInstrumentos.get(PainelReuniaoInstrumentosAno_.gerencia),
                        fromPainelReuniaoInstrumentos.get(PainelReuniaoInstrumentosAno_.anoAssinaturaInstrumento),
                        total)
                .groupBy(fromPainelReuniaoInstrumentos.get(PainelReuniaoInstrumentosAno_.gerenciaId),
                        fromPainelReuniaoInstrumentos.get(PainelReuniaoInstrumentosAno_.gerencia),
                        fromPainelReuniaoInstrumentos.get(PainelReuniaoInstrumentosAno_.anoAssinaturaInstrumento))
                .orderBy(builder.asc(fromPainelReuniaoInstrumentos.get(PainelReuniaoInstrumentosAno_.gerencia)),
                        builder.asc(fromPainelReuniaoInstrumentos.get(PainelReuniaoInstrumentosAno_.anoAssinaturaInstrumento))));

        List<PainelReuniaoInstrumentosAno> resultadoQuery = typedQuery.getResultList();

        for (PainelReuniaoInstrumentosAno tuplaQuery : resultadoQuery) {
            Object[] protocolos;
            if (tuplaQuery.getGerencia() != null) {
                if (resultadoMap.containsKey(tuplaQuery.getGerencia().split(" - ")[0].trim())) {
                    protocolos = resultadoMap.get(tuplaQuery.getGerencia().split(" - ")[0].trim());
                } else {
                    protocolos = new Object[14];
                    resultadoMap.put(tuplaQuery.getGerencia().split(" - ")[0].trim(), protocolos);
                }
                protocolos[posicao] = tuplaQuery.getNumProtocolos();                
            }
        }
    }

    public List<Object[]> painelMapObjectToListObject(Map<String, Object[]> painelMap) {
        return painelMapObjectToListObject(painelMap, 13);
    }

    public List<Object[]> painelMapObjectToListObject(Map<String, Object[]> painelMap, int qtde) {
        List<Object[]> result = new ArrayList<>();
        for (String key : painelMap.keySet()) {
            Object[] obj = new Object[qtde];
            obj[0] = key;
            Object[] values = painelMap.get(key);
            for (int i = 0; i < values.length; i++) {
                obj[i + 1] = values[i] != null ? values[i] : (key.equals("Meta") ? null : 0);
            }
            result.add(obj);
        }
        return result;
    }

    private void generateGoalResults(Map<String, Object[]> resultadoMap, EnumIndicativoMeta enumIndicativoMeta, Integer ano, Integer mes, Integer howManyFieldsBeforeGoal) {
        generateGoalResults("Meta", resultadoMap, enumIndicativoMeta, ano, mes, howManyFieldsBeforeGoal, null);
    }

    private void generateGoalResults(Map<String, Object[]> resultadoMap, EnumIndicativoMeta enumIndicativoMeta, Integer ano, Integer mes) {
        generateGoalResults("Meta", resultadoMap, enumIndicativoMeta, ano, mes, 0, null);
    }

    private void generateGoalResults(String identifier, Map<String, Object[]> resultadoMap, EnumIndicativoMeta enumIndicativoMeta, Integer ano, Integer mes) {
        generateGoalResults(identifier, resultadoMap, enumIndicativoMeta, ano, mes, 0, null);
    }

    private void generateGoalResults(String identifier, Map<String, Object[]> resultadoMap, EnumIndicativoMeta enumIndicativoMeta, Integer ano, Integer mes, Estagio estagio) {
        generateGoalResults(identifier, resultadoMap, enumIndicativoMeta, ano, mes, 0, estagio);
    }

    private void generateGoalResults(String identifier, Map<String, Object[]> resultadoMap, EnumIndicativoMeta enumIndicativoMeta, Integer ano, Integer mes, Integer howManyFieldsBeforeGoal, Estagio estagio) {
        TypedQuery<Meta> query = null;
        List<Meta> metas = new ArrayList<>();
        if (estagio != null) {
            query = entityManager.createNamedQuery(Meta.Query.FIND_BY_INDICADOR_AND_ESTAGIO, Meta.class);
            query.setParameter(1, enumIndicativoMeta);
            query.setParameter(2, ano);
            query.setParameter(3, estagio);
        } else {
            query = entityManager.createNamedQuery(Meta.Query.FIND_BY_INDICADOR, Meta.class);
            query.setParameter(1, enumIndicativoMeta);
            query.setParameter(2, ano);
        }
        metas = query.getResultList();
        Object[] result = new Object[howManyFieldsBeforeGoal + 12];
        for (Meta meta : metas) {
            // done to get just the goals till the month passed as filter 
//            if (meta.getMes() > mes) {
//                continue;
//            }

            result[meta.getMes() + howManyFieldsBeforeGoal - 1] = meta.getValor();
        }
        resultadoMap.put(identifier, result);
    }

    public List<Object[]> findIndiceSatisfacaoByFilters(QueryParameter queryParam) {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<PainelIndiceSatisfacao> query = builder.createQuery(PainelIndiceSatisfacao.class);
        Root<PainelIndiceSatisfacao> fromIndiceSatisfacao = query.from(PainelIndiceSatisfacao.class);
        List<Predicate> conditions = new ArrayList();

//        if (queryParam.exists("diretoria")) {
//            conditions.add(builder.equal(fromIndiceSatisfacao.get(PainelIndiceSatisfacao_.diretoriaId), queryParam.get("diretoria")));
//        }
        Calendar data = Calendar.getInstance();
        data.setTimeInMillis((Long) queryParam.get("mesAno"));
        Integer month = data.get(Calendar.MONTH) + 1;
        Integer year = data.get(Calendar.YEAR);

        //getting all months of the year to calculate INDI percents
        conditions.add(builder.equal(fromIndiceSatisfacao.get(PainelIndiceSatisfacao_.ano), year));
        conditions.add(builder.between(fromIndiceSatisfacao.get(PainelIndiceSatisfacao_.mes), 1, month));

        TypedQuery typedQuery = entityManager.createQuery(query
                .where(conditions.toArray(new Predicate[]{}))
                .orderBy(builder.asc(fromIndiceSatisfacao.get(PainelIndiceSatisfacao_.ano)), builder.asc(fromIndiceSatisfacao.get(PainelIndiceSatisfacao_.mes))));

        List<PainelIndiceSatisfacao> indexes = typedQuery.getResultList();

        Map<String, Object[]> resultadoMap = new HashMap<>();
        //calculating INDI percents by all months
        resultadoMap.put("INDI", getIndiMonthSatisfactionIndexPercent(indexes, month));
        //filtering list by month and diretoria if it exists
        List<PainelIndiceSatisfacao> indexesFiltered = indexes.stream().filter(u -> u.getMes() <= month && (queryParam.exists("diretoria") ? Objects.equals(u.getDiretoriaId(), queryParam.get("diretoria")) : 1 == 1)).collect(Collectors.toList());

        Object[] porcentagens = new Object[12];
        for (PainelIndiceSatisfacao line : indexesFiltered) {
            if (resultadoMap.containsKey(line.getDiretoria())) {
                porcentagens = resultadoMap.get(line.getDiretoria());
            } else {
                porcentagens = new Object[12];
                resultadoMap.put(line.getDiretoria(), porcentagens);
            }

            porcentagens[line.getMes() - 1] = line.getPercentual();
        }

        generateGoalResults(resultadoMap, EnumIndicativoMeta.INDICE_SATISFACAO, year, month);

        return painelMapObjectToListObject(resultadoMap);
    }

    private Object[] getIndiMonthSatisfactionIndexPercent(List<PainelIndiceSatisfacao> indexes, Integer month) {
        Stream<PainelIndiceSatisfacao> indexesStream = indexes.stream().
                collect(Collectors.groupingBy(
                                (index) -> index.getMesAno()))
                .values().stream().map((ind) -> {
                    PainelIndiceSatisfacao i = new PainelIndiceSatisfacao();
                    i.setDiretoria("INDI");
                    i.setMesAno(ind.get(0).getMesAno());
                    i.setMes(ind.get(0).getMes());
                    i.setAno(ind.get(0).getAno());
                    i.setPercentual(new BigDecimal(ind.stream().mapToDouble(PainelIndiceSatisfacao::getPercentual).sum() / ind.size())
                            .setScale(2, RoundingMode.HALF_EVEN).doubleValue());
                    return i;
                });
        TreeSet<PainelIndiceSatisfacao> emps = new TreeSet<>((e1, e2) -> e1.getMesAno().compareToIgnoreCase(e2.getMesAno()));
        emps.addAll(indexesStream.collect(Collectors.toSet()));
        Object[] collecteds = emps.toArray();
        Object[] returned = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
        for (Object collected1 : collecteds) {
            // setting collected of the month on the right position on returned list.
            PainelIndiceSatisfacao collected = (PainelIndiceSatisfacao) collected1;
            if (collected.getMes() <= month) {
                returned[collected.getMes() - 1] = collected.getPercentual();
            }
        }

        return returned;
    }

    public List countPainelMetasByFilters(QueryParameter queryParam) {

        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<PainelMetas> query = builder.createQuery(PainelMetas.class);
        Root<PainelMetas> fromPainelMetasEmprego = query.from(PainelMetas.class);
        List<Predicate> conditions = new ArrayList();

        Expression totalEmpregosDiretos = builder.sum(fromPainelMetasEmprego.get(PainelMetas_.empregosDiretos));
        Expression totalFaturamentoPleno = builder.sum(fromPainelMetasEmprego.get(PainelMetas_.faturamentoPlenoMilhoes));
        Expression totalInvestimentoPrevisto = builder.sum(fromPainelMetasEmprego.get(PainelMetas_.investimentoPrevistoMilhoes));
        Expression totalProjetos = builder.count(fromPainelMetasEmprego.get(PainelMetas_.diretoriaId));

        if (queryParam.exists("estagio")) {
            Integer estagioId = ((Estagio) queryParam.get("estagio")).getId();
            conditions.add(builder.equal(fromPainelMetasEmprego.get(PainelMetas_.estagioId), estagioId));
        } else {
            conditions.add(builder.equal(fromPainelMetasEmprego.get(PainelMetas_.estagioId), Estagio.FORMALIZADA.getId()));
        }
        if (queryParam.exists("diretoria")) {
            conditions.add(builder.equal(fromPainelMetasEmprego.get(PainelMetas_.diretoriaId), queryParam.get("diretoria")));
        }
        if (queryParam.exists("gerencias")) {
            conditions.add(fromPainelMetasEmprego.get(PainelMetas_.gerenciaId).in((List<Long>) queryParam.get("gerencias")));
        }
        if (queryParam.exists("dataFim")) {
            Calendar mesAno = Calendar.getInstance();
            mesAno.setTimeInMillis((Long) queryParam.get("dataFim"));
            conditions.add(builder.between(fromPainelMetasEmprego.get(PainelMetas_.mesCadastro), 1, mesAno.get(Calendar.MONTH) + 1));
            conditions.add(builder.equal(fromPainelMetasEmprego.get(PainelMetas_.anoCadastro), mesAno.get(Calendar.YEAR)));
        }
        if (queryParam.exists("prospeccaoAtiva")) {
            conditions.add(builder.equal(fromPainelMetasEmprego.get(PainelMetas_.prospeccaoAtiva), queryParam.get("prospeccaoAtiva")));
        }

        TypedQuery<PainelMetas> typedQuery = entityManager.createQuery(query
                .where(conditions.toArray(new Predicate[]{}))
                .multiselect(fromPainelMetasEmprego.get(PainelMetas_.mesCadastro),
                        fromPainelMetasEmprego.get(PainelMetas_.anoCadastro),
                        totalEmpregosDiretos, totalFaturamentoPleno, totalInvestimentoPrevisto, totalProjetos)
                .groupBy(fromPainelMetasEmprego.get(PainelMetas_.mesCadastro),
                        fromPainelMetasEmprego.get(PainelMetas_.anoCadastro))
                .orderBy(builder.asc(fromPainelMetasEmprego.get(PainelMetas_.mesCadastro)),
                        builder.asc(fromPainelMetasEmprego.get(PainelMetas_.anoCadastro))));

        List<PainelMetas> resultadoQuery = typedQuery.getResultList();
        Map<String, Object[]> resultadoMap = new HashMap<>();

        for (PainelMetas tuplaQuery : resultadoQuery) {
            Object[] coluna;

            Map<String, Supplier<Object>> mapCategoria = new HashMap<>();
            mapCategoria.put("Empregos", tuplaQuery::getEmpregosDiretos);
            mapCategoria.put("Investimento", tuplaQuery::getInvestimentoPrevistoMilhoes);
            mapCategoria.put("Faturamento", tuplaQuery::getFaturamentoPlenoMilhoes);
            mapCategoria.put("Projetos", tuplaQuery::getProjetos);

            for (String categoria : mapCategoria.keySet()) {
                if (resultadoMap.containsKey(categoria)) {
                    coluna = resultadoMap.get(categoria);
                } else {
                    coluna = new Object[12];
                    resultadoMap.put(categoria, coluna);
                }

                coluna[tuplaQuery.getMesCadastro() - 1] = mapCategoria.get(categoria).get();
            }
        }
        Calendar mesAno = Calendar.getInstance();
        mesAno.setTimeInMillis((Long) queryParam.get("dataFim"));
        Estagio estagio = queryParam.get("estagio") != null ? (Estagio) queryParam.get("estagio") : Estagio.FORMALIZADA;
        generateGoalResults("MetaEmpregos", resultadoMap, EnumIndicativoMeta.EMPREGOS_DIRETOS, mesAno.get(Calendar.YEAR), mesAno.get(Calendar.MONTH) + 1, estagio);
        generateGoalResults("MetaFaturamento", resultadoMap, EnumIndicativoMeta.FATURAMENTO_PLENO, mesAno.get(Calendar.YEAR), mesAno.get(Calendar.MONTH) + 1, estagio);
        generateGoalResults("MetaInvestimento", resultadoMap, EnumIndicativoMeta.INVESTIMENTO, mesAno.get(Calendar.YEAR), mesAno.get(Calendar.MONTH) + 1, estagio);
        generateGoalResults("MetaProjetos", resultadoMap, EnumIndicativoMeta.QUANTIDADE_PROJETOS, mesAno.get(Calendar.YEAR), mesAno.get(Calendar.MONTH) + 1, estagio);

        return painelMapObjectToListObject(resultadoMap);
    }

}
