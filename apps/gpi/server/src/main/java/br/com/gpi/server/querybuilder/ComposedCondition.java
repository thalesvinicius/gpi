package br.com.gpi.server.querybuilder;

import java.util.Arrays;
import java.util.List;

public class ComposedCondition extends Condition {

    private List<String> values;
    private Condition filtroDependencia;
    
    public ComposedCondition () {
    }
    
    public ComposedCondition(List<String> values, String conditionKey, List<Field> fields, ConditionValueTypeEnum type, Condition filtroDependencia) {
        super(conditionKey, fields, type);
        this.values = values;
        this.filtroDependencia = filtroDependencia;
    }

    public List<String> getValues() {
        return values;
    }

    public void setValues(List<String> values) {
        this.values = values;
    }

    public Condition getFiltroDependencia() {
        return filtroDependencia;
    }

    public void setFiltroDependencia(Condition filtroDependencia) {
        this.filtroDependencia = filtroDependencia;
    }
    @Override
    public String getSQL() {
        StringBuilder sb = new StringBuilder();
        for(Field field : getFields()) {
            filtroDependencia.setFields(Arrays.asList(new Field[]{field}));
            sb.append(filtroDependencia.getSQL());
        }

        return sb.toString();
    }

}
