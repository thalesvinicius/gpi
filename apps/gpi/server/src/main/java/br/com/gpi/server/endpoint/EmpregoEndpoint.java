package br.com.gpi.server.endpoint;

import br.com.gpi.server.domain.entity.Emprego;
import br.com.gpi.server.domain.repository.EmpregoRepository;
import br.com.gpi.server.domain.repository.ProjetoRepository;
import br.com.gpi.server.domain.repository.UserRepository;
import br.com.gpi.server.domain.service.EmpregoService;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.SystemException;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

@Path("/projeto/{idProjeto}/emprego")
@Stateless
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
@TransactionManagement(TransactionManagementType.BEAN)
public class EmpregoEndpoint {

    @Inject
    private EmpregoService empregoService;
    @Inject
    private EmpregoRepository empregoRepository;
    @Inject
    private ProjetoRepository projetoRepository;
    @Inject 
    private UserRepository userRepository;
    @Inject
    private EntityManager entityManager;
    @PathParam("idProjeto")
    private Long idProjeto;
    
    @POST
    public Response saveOrUpdateEmprego(List<Emprego> empregosList, @Context SecurityContext context) {
        try {
            empregoService.saveOrUpdateEmprego(empregosList, context, idProjeto);
            return Response.ok().build();
        } catch (Exception ex) {
            Logger.getLogger(EmpregoEndpoint.class.getName()).log(Level.SEVERE, null, ex);
            throw new RuntimeException(ex);
        }
    }

    @GET
    @Path("{id}")
    public Response findById(@PathParam("id") Long id) {
        return Response.ok(empregoRepository.findBy(id)).build();
    }
    @GET    
    public Response findAllEmpregosByIdProjeto() {
        List<Emprego> empregos = empregoRepository.locateByProjetoId(idProjeto);
        return Response.ok(empregos).build();
    }

    @POST
    @Path("removeEmprego")
    public Response removeEmprego(List<Long> ids) throws Exception{
        empregoRepository.removeByIds(ids);
        return Response.ok().build();   
    }
    
    @GET
    @Path("allEmpregoAgricolaIndustriaDTOPermanente")
    public Response findAllEmpregoAgricolaIndustriaDTOPermanente(){
        return Response.ok(empregoService.findAllEmpregoAgricolaIndustriaDTOPermanente(idProjeto)).build();
    }
    
    @GET
    @Path("allEmpregoAgricolaIndustriaDTOTemporario")
    public Response findAllEmpregoAgricolaIndustriaDTOTemporario(){
        return Response.ok(empregoService.findAllEmpregoAgricolaIndustriaDTOTemporario(idProjeto)).build();
    }
    
    @GET
    @Path("allEmpregosByTipoEmprego/{idTipoEmprego}")
    public Response findallEmpregosByTipoEmprego(@PathParam("idTipoEmprego") Integer idTipoEmprego){
        return Response.ok(empregoService.findAllEmpregosByTipoEmprego(idProjeto, idTipoEmprego)).build();
    }
    
    @GET
    @Path("findChange")
    public Response findChange() {
        Object emprego = null;
        try {
            emprego = empregoService.verifyChange(idProjeto);
        } catch (SystemException ex) {
            Logger.getLogger(EmpregoEndpoint.class.getName()).log(Level.SEVERE, null, ex);
        }
        return Response.ok(emprego).build();
    }
}
