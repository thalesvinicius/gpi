package br.com.gpi.server.domain.service;

import br.com.gpi.server.domain.common.BaseService;
import br.com.gpi.server.domain.entity.Produto;
import java.util.List;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

public class ProdutoService extends BaseService<Produto> {

    @Inject
    public ProdutoService(EntityManager em) {
        super(Produto.class, em);
    }

    public List<Produto> findAllProdutosByInsumoProduto(List<Long> ids_insumoProduto) {
        StringBuilder builder = new StringBuilder("SELECT p FROM Produto p ");
        builder.append(" WHERE p.insumoProduto.id in (:ids_insumoProduto) ");
        
        TypedQuery<Produto> query = getEm().createQuery(builder.toString(), Produto.class);
        query.setParameter("ids_insumoProduto", ids_insumoProduto);
        return query.getResultList();
    }    
    
}
