package br.com.gpi.server.domain.service;

import br.com.gpi.server.domain.common.BaseService;
import br.com.gpi.server.domain.entity.Estagio;
import br.com.gpi.server.domain.entity.Infraestrutura;
import br.com.gpi.server.domain.entity.Projeto;
import br.com.gpi.server.domain.entity.SituacaoProjetoEnum;
import br.com.gpi.server.domain.entity.User;
import br.com.gpi.server.domain.entity.UsuarioExterno;
import br.com.gpi.server.domain.repository.InfraestruturaRepository;
import br.com.gpi.server.domain.repository.ProjetoRepository;
import br.com.gpi.server.domain.repository.UserRepository;
import br.com.gpi.server.domain.user.model.SecurityUser;
import br.com.gpi.server.util.LogUtil;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;
import javax.ws.rs.core.SecurityContext;

public class InfraestruturaService extends BaseService<Infraestrutura> {

    @Inject
    private InfraestruturaRepository infraestruturaRepository;

    @Inject
    private ProjetoRepository projetoRepository;

    @Inject
    private UserRepository userRepository;

    @Inject
    private ProjetoService projetoService;

    @Inject
    private UserTransaction transaction;

    @Inject
    public InfraestruturaService(EntityManager em) {
        super(Infraestrutura.class, em);
    }

    public Infraestrutura saveOrUpdate(Infraestrutura infraestrutura, SecurityContext context, Long idProjeto) throws SystemException {
        try {
            transaction.begin();
            this.getEm().joinTransaction();

            SecurityUser loggedUserWrap = (SecurityUser) context.getUserPrincipal();
            User user = this.getEm().find(User.class, loggedUserWrap.getId());
            infraestrutura.setUsuarioResponsavel(user);
            infraestrutura.setDataUltimaAlteracao(new Date());
            Projeto projeto = projetoRepository.findBy(idProjeto);

            infraestrutura.setProjeto(projeto);

            if (infraestrutura.getEnergiaContratadas() != null) {
                infraestrutura.getEnergiaContratadas().forEach(u -> u.setInfraestrutura(infraestrutura));
            }

            this.getEm().merge(infraestrutura);

           //Altera a situação do projeto se o estágio for INICIO_PROJETO.
            if (projetoService.findEstagioAtualById(idProjeto).getEstagio() == Estagio.INICIO_PROJETO) {
                if(user instanceof UsuarioExterno) {
                    projetoService.updateSituacaoProjetoTransactional(SituacaoProjetoEnum.PENDENTE_ACEITE, idProjeto, loggedUserWrap.getId(), new Date());
                } else {
                    projetoService.updateSituacaoProjetoTransactional(SituacaoProjetoEnum.PEDENTE_VALIDACAO, idProjeto, loggedUserWrap.getId(), new Date());
                    
                }
            }

            transaction.commit();
            return infraestrutura;
        } catch (Exception ex) {
            transaction.rollback();
            Logger.getLogger(ProjetoService.class.getName()).log(Level.SEVERE, null, ex);
            throw new RuntimeException(ex);
        }
    }

    public Object verifyChange(Long idProjeto) throws SystemException {
        String atributos = " possuiTerreno_MOD, potenciaEnergeticaEstimada_MOD, " +
            "areaAlagada_MOD, areaConstruida_MOD, areaNecessaria_MOD, consumoAguaEstimado_MOD, " +
            "consumoCombustivelEstimado_MOD, consumoGasEstimado_MOD, contatoCemig_MOD, " +
            "energiaContratadas_MOD, energiaJaContratada_MOD, demandaEnergiaEstimada_MOD, " +
            "geracaoEnergiaPropria_MOD, geracaoEnergiaVenda_MOD, ofertaEnergiaVendaEstimada_MOD ";
        Object result = this.getEm().createNativeQuery(
                LogUtil.montaConsultaLog("LogInfraestrutura", atributos, idProjeto).toString()).getResultList();

        return result;
    }
}
