package br.com.gpi.server.helper;

import br.com.gpi.server.domain.entity.enumerated.EnumTipoFaturamento;
import br.com.gpi.server.domain.entity.enumerated.EnumTipoInvestimento;
import java.util.ArrayList;
import java.util.List;

public class FinanceiroHelper {
            
    public static final List<EnumTipoFaturamento> fatEletroeletronico = new ArrayList<>();
    public static final List<EnumTipoFaturamento> fatExcetoEletroeletronico = new ArrayList<>();
    public static final List<EnumTipoInvestimento> invSucroenergetico = new ArrayList();
    public static final List<EnumTipoInvestimento> invExcetoSucroenergetico = new ArrayList();
            
    static{
        List<EnumTipoFaturamento> fatElt = new ArrayList<>();
        fatElt.add(EnumTipoFaturamento.FATURAMENTO_PRODUTOS_ADQ_PARA_COMERCIALIZACAO);
        fatElt.add(EnumTipoFaturamento.FATURAMENTO_PRODUTOS_INDUSTRIALIZADOS);
        fatEletroeletronico.addAll(fatElt);
                
        List<EnumTipoFaturamento> fatExcElt = new ArrayList<>();
        fatExcElt.add(EnumTipoFaturamento.FATURAMENTO_PREVISTO);
        fatExcetoEletroeletronico.addAll(fatExcElt);
        
        List<EnumTipoInvestimento> invSucr = new ArrayList();
        invSucr.add(EnumTipoInvestimento.INVESTIMENTO_AGRICOLA);
        invSucr.add(EnumTipoInvestimento.INVESTIMENTO_INDUSTRIAL);
        invSucr.add(EnumTipoInvestimento.INVESTIMENTO_CAPACITACAO_PROFISSIONAL);
        invSucr.add(EnumTipoInvestimento.INVESTIMENTO_PROPRIOS);
        invSucroenergetico.addAll(invSucr);
        
        List<EnumTipoInvestimento> invExcSucr = new ArrayList();
        invExcSucr.add(EnumTipoInvestimento.INVESTIMENTO_MAQ_EQUIP_TERRENO_OBRAS);
        invExcSucr.add(EnumTipoInvestimento.INVESTIMENTO_CAPITAL_GIRO_E_OUTROS);     
        invExcetoSucroenergetico.addAll(invExcSucr);
    }

}
