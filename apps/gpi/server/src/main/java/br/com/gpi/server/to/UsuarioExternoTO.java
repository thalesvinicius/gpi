package br.com.gpi.server.to;

import br.com.gpi.server.domain.entity.Cargo;
import br.com.gpi.server.domain.entity.User;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.util.Date;

@JsonIgnoreProperties(ignoreUnknown = true)
public class UsuarioExternoTO {
    
    private Long id;
    private String nomeEmpresa;
    private String nomeUnidade;
    private String nome;
    private Cargo cargo;
    private String cargoExterno;
    private String telefone;
    private String telefone2;
    private String email;
    private Boolean status;
    private Boolean mailing;
    private User usuarioResponsavel;
    private Date dataUltimaAlteracao;
    private Boolean ativo;
    private String password;
    private Long empresaId;
    private Long unidadeId;

    public UsuarioExternoTO(Long id, String nomeEmpresa, String nomeUnidade, String nome, 
            String email, 
            Boolean status, String cargoExterno, String telefone, String telefone2, Boolean mailing,
            Date dataUltimaAlteracao, User usuarioResponsavel, Boolean ativo, String password, Long empresaId, Long unidadeId) {
        this.id = id;
        this.nomeEmpresa = nomeEmpresa;
        this.nomeUnidade = nomeUnidade;
        this.nome = nome;
        this.email = email;
        this.status = status;
        this.cargoExterno = cargoExterno;
        this.telefone = telefone;
        this.telefone2 = telefone2;
        this.mailing = mailing;
        this.dataUltimaAlteracao = dataUltimaAlteracao;
        this.usuarioResponsavel = usuarioResponsavel;
        this.ativo = ativo;
        this.password = password;
        this.empresaId = empresaId;
        this.unidadeId = unidadeId;
    }

    public String getNomeEmpresa() {
        return nomeEmpresa + (nomeUnidade != null ? " / " + nomeUnidade : "");
    }

    public void setNomeEmpresa(String nomeEmpresa) {
        this.nomeEmpresa = nomeEmpresa;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Cargo getCargo() {
        return cargo;
    }

    public void setCargo(Cargo cargo) {
        this.cargo = cargo;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCargoExterno() {
        return cargoExterno;
    }

    public void setCargoExterno(String cargoExterno) {
        this.cargoExterno = cargoExterno;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getTelefone2() {
        return telefone2;
    }

    public void setTelefone2(String telefone2) {
        this.telefone2 = telefone2;
    }

    public Boolean isMailing() {
        return mailing;
    }

    public void setMailing(Boolean mailing) {
        this.mailing = mailing;
    }

    public User getUsuarioResponsavel() {
        return usuarioResponsavel;
    }

    public void setUsuarioResponsavel(User usuarioResponsavel) {
        this.usuarioResponsavel = usuarioResponsavel;
    }

    public Date getDataUltimaAlteracao() {
        return dataUltimaAlteracao;
    }

    public void setDataUltimaAlteracao(Date dataUltimaAlteracao) {
        this.dataUltimaAlteracao = dataUltimaAlteracao;
    }

    public Boolean getAtivo() {
        return ativo;
    }

    public void setAtivo(Boolean ativo) {
        this.ativo = ativo;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getNomeUnidade() {
        return nomeUnidade;
    }

    public void setNomeUnidade(String nomeUnidade) {
        this.nomeUnidade = nomeUnidade;
    }

    public Long getEmpresaId() {
        return empresaId;
    }

    public void setEmpresaId(Long empresaId) {
        this.empresaId = empresaId;
    }

    public Long getUnidadeId() {
        return unidadeId;
    }

    public void setUnidadeId(Long unidadeId) {
        this.unidadeId = unidadeId;
    }
    
}
