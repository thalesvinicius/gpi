package br.com.gpi.server.core.jpa.converter;

import br.com.gpi.server.domain.entity.enumerated.EnumTipoFaturamento;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = false)
public class TipoFaturamentoEnumConverter implements AttributeConverter<EnumTipoFaturamento, Integer> {

    @Override
    public Integer convertToDatabaseColumn(EnumTipoFaturamento tipoFaturamento) {
        return tipoFaturamento.getId();
    }

    @Override
    public EnumTipoFaturamento convertToEntityAttribute(Integer dbData) {
        return EnumTipoFaturamento.getEnumTipoFaturamento(dbData);
    }
}
