package br.com.gpi.server.domain.service;

import br.com.gpi.server.domain.common.BaseService;
import br.com.gpi.server.domain.entity.Estagio;
import br.com.gpi.server.domain.entity.EtapaMeioAmbiente;
import br.com.gpi.server.domain.entity.MeioAmbiente;
import br.com.gpi.server.domain.entity.Projeto;
import br.com.gpi.server.domain.entity.SituacaoProjetoEnum;
import br.com.gpi.server.domain.entity.User;
import br.com.gpi.server.domain.entity.UsuarioExterno;
import br.com.gpi.server.domain.entity.enumerated.EnumTipoMeioAmbiente;
import br.com.gpi.server.domain.user.model.SecurityUser;
import br.com.gpi.server.util.LogUtil;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;
import javax.ws.rs.core.SecurityContext;

public class MeioAmbienteService extends BaseService<MeioAmbiente> {

    @Inject
    private ProjetoService projetoService;

    @Inject
    private EmailService emailService;
    
    @Inject
    @RequestScoped
    private UserTransaction transaction;
    
    @Inject
    public MeioAmbienteService(EntityManager em) {
        super(MeioAmbiente.class, em);
    }

    public MeioAmbiente saveOrUpdate(MeioAmbiente meioAmbiente, SecurityContext context, Long idProjeto) throws SystemException {
        try {
            transaction.begin();
            this.getEm().joinTransaction();

            SecurityUser loggedUserWrap = (SecurityUser) context.getUserPrincipal();
            User user = this.getEm().find(User.class, loggedUserWrap.getId());
            meioAmbiente.setUsuarioResponsavel(user);
            meioAmbiente.setDataUltimaAlteracao(new Date());

            Projeto projeto = this.getEm().find(Projeto.class, idProjeto);

            meioAmbiente.setProjeto(projeto);
            
            if(meioAmbiente.getEtapasMeioAmbiente() == null) meioAmbiente.setEtapasMeioAmbiente(new HashSet<>());

            if (meioAmbiente.getEtapasMeioAmbiente() != null) {
                meioAmbiente.getEtapasMeioAmbiente().forEach(e -> e.setMeioAmbiente(meioAmbiente));
            }
            
            Set<EnumTipoMeioAmbiente> novos = meioAmbiente.getSelectedEtapasMeioAmbiente();
            meioAmbiente.setEtapasMeioAmbiente(new HashSet<>());
            meioAmbiente.getEtapasMeioAmbiente().addAll(novos.stream().map((tp)-> {
                    EtapaMeioAmbiente etapa = new EtapaMeioAmbiente();
                    etapa.setTipoMeioAmbiente(tp); etapa.setTipoMeioAmbienteInt(tp.getId());
                    etapa.setMeioAmbiente(meioAmbiente);
                    return etapa;
                }).collect(Collectors.toSet()));

            if (meioAmbiente.isRealizadaVistoria() == null || !meioAmbiente.isRealizadaVistoria()) {
                if (meioAmbiente.isRealizadaVistoria() == null) {
                    meioAmbiente.setRealizadaVistoria(false);
                }
                meioAmbiente.setDataRealizadaVistoria(null);
            }

            if (meioAmbiente.isPedidoInformacoesComplementar() == null || !meioAmbiente.isPedidoInformacoesComplementar()) {
                if (meioAmbiente.isPedidoInformacoesComplementar() == null) {
                    meioAmbiente.setPedidoInformacoesComplementar(false);
                }
                meioAmbiente.setDataPedidoInformacao(null);
                meioAmbiente.setDataEntregaInformacao(null);
            }

            //Altera a situação do projeto se o estágio for INICIO_PROJETO.
            if (projetoService.findEstagioAtualById(idProjeto).getEstagio() == Estagio.INICIO_PROJETO) {
                if(user instanceof UsuarioExterno) {
                    projetoService.updateSituacaoProjetoTransactional(SituacaoProjetoEnum.PENDENTE_ACEITE, idProjeto, loggedUserWrap.getId(), new Date());
                } else {
                    projetoService.updateSituacaoProjetoTransactional(SituacaoProjetoEnum.PEDENTE_VALIDACAO, idProjeto, loggedUserWrap.getId(), new Date());
                    
                }
            }

            if (meioAmbiente.isDentroDoPrazo() != null && !meioAmbiente.isDentroDoPrazo()) {
                emailService.sendEnvironmentAlert(projeto.getNome());
            }
            this.merge(meioAmbiente);
            
            transaction.commit();
            
            return meioAmbiente;
        } catch (Exception ex) {
            transaction.rollback();
            Logger.getLogger(ProjetoService.class.getName()).log(Level.SEVERE, null, ex);
            throw new RuntimeException(ex);
        }
    }
    
    public Object verifyChange(Long idProjeto) throws SystemException {
        String atributos = " realizadaVistoria_MOD, necessidadeEIARIMA_MOD, dataEntregaInformacao_MOD, " +
            "pedidoInformacoesComplementar_MOD, observacao_MOD, licenciamentoIniciado_MOD, " +
            "etapasMeioAmbiente_MOD, anexos_MOD, classeEmpreendimento_MOD, " +
            "dataPedidoInformacao_MOD, dataRealizadaVistoria_MOD, dataEntregaEstudos_MOD ";
        Object result = this.getEm().createNativeQuery(
                LogUtil.montaConsultaLog("LogMeioAmbiente", atributos, idProjeto).toString()).getResultList();

        return result;
    }
}
