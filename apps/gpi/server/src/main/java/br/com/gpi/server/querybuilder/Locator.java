package br.com.gpi.server.querybuilder;

import java.util.HashMap;
import java.util.Map;

public class Locator {

    //Tables
    public static final Table tabelaEmpresa = new Table("VIEW_EMPRESA_BUSCA_AVANCADA", "emp", "id", 1, null, true);
    public static final Table tabelaProjeto = new Table("VIEW_PROJETO_BUSCA_AVANCADA", "proj", "id", 2, null, true);
    public static final Table tabelaInstrumentoFormalizacao = new Table("VIEW_INSTRUMENTO_FORMALIZACAO_BUSCA_AVANCADA", "instr", "id", 4, new TableLink(tabelaProjeto, "projeto_id"), true);

    public static final Table tabelaUnidadeEmpresa = new Table("VIEW_UNIDADEEMPRESA_BUSCA_AVANCADA", "und", "id", 5, new TableLink(tabelaEmpresa, "empresa_id"));
    public static final Table tabelaCnaeEmpresaDivisao = new Table("VIEW_CNAE_BUSCA_AVANCADA_NIVEL_1", "cnaeEmpN1", "id", 9, new TableLink(tabelaEmpresa, "empresa_id"));
    public static final Table tabelaCnaeEmpresaGrupo = new Table("VIEW_CNAE_BUSCA_AVANCADA_NIVEL_2", "cnaeEmpN2", "id", 8, new TableLink(tabelaEmpresa, "empresa_id"));
    public static final Table tabelaCnaeEmpresaClasse = new Table("VIEW_CNAE_BUSCA_AVANCADA_NIVEL_3", "cnaeEmpN3", "id", 7, new TableLink(tabelaEmpresa, "empresa_id"));
    public static final Table tabelaCnaeEmpresaSubclasse = new Table("VIEW_CNAE_BUSCA_AVANCADA_NIVEL_4", "cnaeEmpN4", "id", 6, new TableLink(tabelaEmpresa, "empresa_id"));
    public static final Table tabelaInformacaoAdicionalEmpresa = new Table("VIEW_INFORMACAO_ADICIONAL_BUSCA_AVANCADA", "infoAd", "empresa_id", 10, new TableLink(tabelaEmpresa, "empresa_id"));
    public static final Table tabelaContatoEmpresaUnidade = new Table("VIEW_CONTATOS_BUSCA_AVANCADA", "con", "empresa_id", 11, new TableLink(tabelaEmpresa, "empresa_id"));
    public static final Table tabelaLocalizacaoProjeto = new Table("VIEW_LOCALIZACAO_BUSCA_AVANCADA", "loc", "id", 12, new TableLink(tabelaProjeto, "projeto_id"));
    public static final Table tabelaInvestimentoProjeto = new Table("VIEW_INVESTIMENTO_BUSCA_AVANCADA", "inv", "id", 13, new TableLink(tabelaProjeto, "projeto_id"));
    public static final Table tabelaFaturamentoProjeto = new Table("VIEW_FATURAMENTO_BUSCA_AVANCADA", "fat", "id", 14, new TableLink(tabelaProjeto, "projeto_id"));
    public static final Table tabelaEmpregoProjeto = new Table("VIEW_EMPREGO_BUSCA_AVANCADA", "empgo", "id", 15, new TableLink(tabelaProjeto, "projeto_id"));
    public static final Table tabelaInfraEstruturaProjeto = new Table("VIEW_INFRAESTRUTURA_BUSCA_AVANCADA", "inf", "id", 16, new TableLink(tabelaProjeto, "projeto_id"));
    
    public static final Table tabelaNomeEmpresa = new Table("NomeEmpresa", "nomEmp", "id", 17, new TableLink(tabelaEmpresa, "empresa_id"));
    public static final Table tabelaHistoricoSituacaoProjeto = new Table("HistoricoSituacaoProjeto", "hsp", "id", 18, new TableLink(tabelaProjeto, "projeto_id"));
    public static final Table tabelaHistoricoEstagioProjeto = new Table("EstagioProjeto", "hep", "id", 19, new TableLink(tabelaProjeto, "projeto_id"));
    
    public static final Table tabelaInsumoProduto = new Table("InsumoProduto", "ip", "id", 20, new TableLink(tabelaProjeto, "projeto_id"));
    public static final Table tabelaProduto = new Table("Produto", "prod", "id", 21, new TableLink(tabelaInsumoProduto, "insumoProduto_id"));
    public static final Table tabelaInsumo = new Table("Insumo", "insu", "id", 22, new TableLink(tabelaInsumoProduto, "insumoProduto_id"));

    public static final Table tabelaFinanceiro = new Table("Financeiro", "fin", "id", 23, new TableLink(tabelaProjeto, "projeto_id"));
    public static final Table tabelaOrigemRecurso = new Table("OrigemRecurso", "oriRec", "id", 24, new TableLink(tabelaFinanceiro, "financeiro_id"));
    
    public static final Table tabelaAtividadeInstrumento = new Table("VIEW_ATIVIDADE_INSTRUMENTO_BUSCA_AVANCADA", "ai", "id", 25, new TableLink(tabelaInstrumentoFormalizacao, "instrumentoFormalizacao_id"));
    
    public static final Table tabelaEtapaMeioAmbiente = new Table("EtapaMeioAmbiente", "ema", "id", 26, new TableLink(tabelaProjeto, "meioAmbiente_id"));
    
    //Fields 
    //Empresa
    public static final Field campoEmpresaNomePrincipal = new Field(Category.EMPRESA, "nomePrincipal", "Empresa - Nome principal", tabelaEmpresa);
    public static final Field campoEmpresaRazaoSocial = new Field(Category.EMPRESA, "razaoSocial", "Empresa - Razão social", tabelaEmpresa);
    public static final Field campoEmpresaCnpj = new Field(Category.EMPRESA, "cnpj", "Empresa - CNPJ", tabelaEmpresa);
    public static final Field campoEmpresaInscricaoEstadual = new Field(Category.EMPRESA, "inscricaoEstadual", "Empresa - Inscrição Estadual", tabelaEmpresa);
    public static final Field campoEmpresaOrigem = new Field(Category.EMPRESA, "origemEmpresaDescricao", "Empresa - Origem", tabelaEmpresa);
    public static final Field campoEmpresaOrigem_id = new Field(Category.EMPRESA, "origemEmpresa", "origemEmpresa", tabelaEmpresa, false);
    public static final Field campoEmpEnderecoCompleto = new Field(Category.EMPRESA, "enderecoCompleto", "Empresa - Endereço Completo", tabelaEmpresa);
    public static final Field campoEmpEnderecoPais = new Field(Category.EMPRESA, "pais", "Empresa - País", tabelaEmpresa);
    public static final Field campoEmpEnderecoMunicipioCidade = new Field(Category.EMPRESA, "municipioCidade", "Empresa - Município/Cidade", tabelaEmpresa);
    
    public static final Field campoEmpEnderecoCidade = new Field(Category.EMPRESA, "cidade", "", tabelaEmpresa, false);
    public static final Field campoEmpEnderecoBairro = new Field(Category.EMPRESA, "bairro", "", tabelaEmpresa, false);
    public static final Field campoEmpEnderecoMunicipio = new Field(Category.EMPRESA, "municipio", "", tabelaEmpresa, false);
    
    public static final Field campoEmpEnderecoUF = new Field(Category.EMPRESA, "siglaEstado", "Empresa - Estado (UF)", tabelaEmpresa);
    public static final Field campoEmpRegiaoPlanejamento = new Field(Category.EMPRESA, "regiaoPlanejamento", "Empresa - Região de Planejamento", tabelaEmpresa);
    public static final Field campoEmpMicroregiao = new Field(Category.EMPRESA, "microregiao", "Empresa - Microrregião", tabelaEmpresa);
    public static final Field campoEmpresaCadProdutiva = new Field(Category.EMPRESA, "cadeiaProdutiva", "Empresa - Cadeia Produtiva", tabelaEmpresa);
    public static final Field campoEmpresaNatJuridica = new Field(Category.EMPRESA, "naturezaJuridica", "Empresa - Natureza Jurídica", tabelaEmpresa);
    public static final Field campoEmpresaNatJuridica_id = new Field(Category.EMPRESA, "naturezaJuridica_id", "naturezaJuridica_id", tabelaEmpresa, false);
    public static final Field campoEmpresaCnaeSubclasse = new Field(Category.EMPRESA, "cnae", "Empresa - Cnae", tabelaCnaeEmpresaSubclasse);

    public static final Field campoEmpresaCnaeDivisao_id = new Field(Category.EMPRESA, "id", "divisao", tabelaCnaeEmpresaDivisao, false);
    public static final Field campoEmpresaCnaeGrupo_id = new Field(Category.EMPRESA, "id", "grupo", tabelaCnaeEmpresaGrupo, false);
    public static final Field campoEmpresaCnaeClasse_id = new Field(Category.EMPRESA, "id", "clsase", tabelaCnaeEmpresaClasse, false);
    public static final Field campoEmpresaCnaeSubclasse_id = new Field(Category.EMPRESA, "id", "subclasse", tabelaCnaeEmpresaSubclasse, false);

    public static final Field campoEmpresaComposicaoSocietaria = new Field(Category.EMPRESA, "composicaoSocietaria", "Empresa - Composição Societária", tabelaEmpresa);

    public static final Field campoUnidadeNome = new Field(Category.EMPRESA, "nome", "Unidade - Nome", tabelaUnidadeEmpresa);
    public static final Field campoUnidadeEmail = new Field(Category.EMPRESA, "email", "Unidade - E-mail de contato", tabelaUnidadeEmpresa);
    public static final Field campoUnidadeTelefone = new Field(Category.EMPRESA, "telefone", "Unidade - Telefone de contato", tabelaUnidadeEmpresa);
    public static final Field campoUnidadeEnderecoCompleto = new Field(Category.EMPRESA, "enderecoCompleto", "Unidade - Endereço completo", tabelaUnidadeEmpresa);
    public static final Field campoUnidadePais = new Field(Category.EMPRESA, "pais", "Unidade - País", tabelaUnidadeEmpresa);
    public static final Field campoUnidadeMunicipio = new Field(Category.EMPRESA, "municipioCidade", "Unidade - Município/Cidade", tabelaUnidadeEmpresa);
    public static final Field campoUnidadeEstado = new Field(Category.EMPRESA, "siglaEstado", "Unidade - Estado (UF)", tabelaUnidadeEmpresa);

    public static final Field campoContatoEmpresaUnidade = new Field(Category.EMPRESA, "empresaUnidade", "Contatos - Empresa/Unidade", tabelaContatoEmpresaUnidade);
    public static final Field campoContatoNome = new Field(Category.EMPRESA, "nome", "Contatos - Nome", tabelaContatoEmpresaUnidade);
    public static final Field campoContatoCargo = new Field(Category.EMPRESA, "cargoExterno", "Contatos - Cargo", tabelaContatoEmpresaUnidade);
    public static final Field campoContatoTelefone1 = new Field(Category.EMPRESA, "telefone", "Contatos - Telefone 1", tabelaContatoEmpresaUnidade);
    public static final Field campoContatoTelefone2 = new Field(Category.EMPRESA, "telefone2", "Contatos - Telefone 2", tabelaContatoEmpresaUnidade);
    public static final Field campoContatoEmail = new Field(Category.EMPRESA, "email", "Contatos - E-mail", tabelaContatoEmpresaUnidade);
    public static final Field campoContatoMailing = new Field(Category.EMPRESA, "mailing", "Contatos - Mailing", tabelaContatoEmpresaUnidade);
    public static final Field campoContatoStatus = new Field(Category.EMPRESA, "status", "Contato - Status", tabelaContatoEmpresaUnidade);

    public static final Field campoEmpresaInfoAdTipoContato = new Field(Category.EMPRESA, "tipoContato", "Informações Adicionais - Tipo do Contato", tabelaInformacaoAdicionalEmpresa);
    public static final Field campoEmpresaInfoAdTipoContato_id = new Field(Category.EMPRESA, "tipoContato_id", "tipoContato_id", tabelaInformacaoAdicionalEmpresa, false);
    public static final Field campoEmpresaInfoAdAssunto = new Field(Category.EMPRESA, "assunto", "Informações Adicionais - Assunto", tabelaInformacaoAdicionalEmpresa);
    public static final Field campoEmpresaInfoAdData = new Field(Category.EMPRESA, "data", "Informações Adicionais - Data", tabelaInformacaoAdicionalEmpresa);
    public static final Field campoEmpresaInfoAdData_full = new Field(Category.EMPRESA, "data_full", "data_full", tabelaInformacaoAdicionalEmpresa);
    public static final Field campoEmpresaInfoAdDescricao = new Field(Category.EMPRESA, "descricao", "Informações Adicionais - Descrição", tabelaInformacaoAdicionalEmpresa);
    public static final Field campoEmpresaOutrosNomes = new Field(Category.EMPRESA, "nome", "OutrosNomes", tabelaNomeEmpresa, false);
    public static final Field campoEmpresaPossuiProjetos = new Field(Category.EMPRESA, "possuiProjetos", "possuiProjetos", tabelaEmpresa, false);
    public static final Field campoEmpresaNovaMG = new Field(Category.EMPRESA, "empresaNovaMG", "empresaNovaMG", tabelaEmpresa, false);

    public static final Field campoEmpresaSituacaoAtual_id = new Field(Category.EMPRESA, "situacao", "situacaoAtualEnum", tabelaEmpresa, false);
    public static final Field campoEmpresaCadeiaProdutiva_id = new Field(Category.EMPRESA, "cadeiaProdutiva_id", "cadeiaProdutiva_id", tabelaEmpresa, false);

    //Projeto
    public static final Field campoProjetoVersao = new Field(Category.PROJETO, "versaoCompleta", "Versão do projeto", tabelaProjeto);
    public static final Field campoProjetoNome = new Field(Category.PROJETO, "nome", "Nome do projeto", tabelaProjeto);
    public static final Field campoProjetoDescricao = new Field(Category.PROJETO, "descricao", "Projeto - Descrição", tabelaProjeto);
    public static final Field campoProjetoProspeccaoAtiva = new Field(Category.PROJETO, "prospeccaoAtiva", "Projeto - Prospecção ativa?", tabelaProjeto);
    public static final Field campoProjetoTipo = new Field(Category.PROJETO, "tipo", "Projeto - Tipo", tabelaProjeto);
    public static final Field campoProjetoTipo_id = new Field(Category.PROJETO, "tipo_id", "tipo_id", tabelaProjeto, false);
    public static final Field campoProjetoCadeiaProdutiva = new Field(Category.PROJETO, "cadeiaProdutiva", "Projeto - Cadeia Produtiva", tabelaProjeto);
    public static final Field campoProjetoCadeiaProdutiva_id = new Field(Category.PROJETO, "cadeiaProdutiva_id", "cadeiaProdutiva_id", tabelaProjeto);
    public static final Field campoProjetoDataCadastro = new Field(Category.PROJETO, "dataCadastro", "Projeto - Data do cadastro", tabelaProjeto);
    public static final Field campoProjetoInformacaoPrimeiroContato = new Field(Category.PROJETO, "informacaoPrimeiroContato", "Projeto - Informações do primeiro contato", tabelaProjeto);
    public static final Field campoProjetoEstagioAtual = new Field(Category.PROJETO, "estagioAtual", "Projeto - Estagio atual", tabelaProjeto);
    public static final Field campoProjetoEstagioAtual_id = new Field(Category.PROJETO, "estagioAtual_id", "estagioAtual_id", tabelaProjeto, false);
    public static final Field campoProjetoDataEstagioAtual = new Field(Category.PROJETO, "dataEstagioAtual", "Projeto - Data do Estágio atual", tabelaProjeto);
    public static final Field campoProjetoSituacaoAtual = new Field(Category.PROJETO, "situacaoAtual", "Projeto - Situação atual", tabelaProjeto);
    public static final Field campoProjetoSituacaoAtual_id = new Field(Category.PROJETO, "situacaoAtual_id", "situacaoAtual_id", tabelaProjeto, false);
    public static final Field campoProjetoGerencia_id = new Field(Category.PROJETO, "gerencia_id", "gerencia_id", tabelaProjeto, false);

    public static final Field campoProjetoUltimaVersao = new Field(Category.PROJETO, "ultimaVersao", "ultimaVersao", tabelaProjeto, false);
    public static final Field campoProjetoProspeccaoAtiva_id = new Field(Category.PROJETO, "prospeccaoAtiva_id", "prospeccaoAtiva_id", tabelaProjeto, false);

    public static final Field campoProjetoDataSituacaoAtual = new Field(Category.PROJETO, "dataSituacaoAtual", "Projeto - Data da Situação atual", tabelaProjeto);
    public static final Field campoProjetoResponsavel = new Field(Category.PROJETO, "responsavel", "Projeto - Responsável", tabelaProjeto);
    public static final Field campoProjetoResponsavel_id = new Field(Category.PROJETO, "usuarioInvestimento_id", "usuarioInvestimento_id", tabelaProjeto);
    public static final Field campoProjetoResponsavelCadeiaProdutiva_id = new Field(Category.PROJETO, "responsavelCadeiaProdutiva_id", "responsavelCadeiaProdutiva_id", tabelaProjeto);
    public static final Field campoProjetoDataRealIP = new Field(Category.PROJETO, "dataRealIP", "Projeto - Data real IP", tabelaProjeto);
    public static final Field campoProjetoDataRealPP = new Field(Category.PROJETO, "dataRealPP", "Projeto - Data real PP", tabelaProjeto);
    public static final Field campoProjetoDataPrevistaDF = new Field(Category.PROJETO, "dataPrevistaDF", "Projeto - Data prevista DF", tabelaProjeto);
    public static final Field campoProjetoDataRealDF = new Field(Category.PROJETO, "dataRealDF", "Projeto - Data real DF", tabelaProjeto);
    public static final Field campoProjetoDataPrevistaII = new Field(Category.PROJETO, "dataPrevistaII", "Projeto - Data prevista II", tabelaProjeto);
    public static final Field campoProjetoDataRealII = new Field(Category.PROJETO, "dataRealII", "Projeto - Data real II", tabelaProjeto);
    public static final Field campoProjetoDataPrevistaOI = new Field(Category.PROJETO, "dataPrevistaOI", "Projeto - Data prevista OI", tabelaProjeto);
    public static final Field campoProjetoDataRealOI = new Field(Category.PROJETO, "dataRealOI", "Projeto - Data real OI", tabelaProjeto);
    public static final Field campoProjetoDataRealCC = new Field(Category.PROJETO, "dataRealCC", "Projeto - Data real CC", tabelaProjeto);

    public static final Field campoLocalizacaoLocalizacaoADefinir = new Field(Category.PROJETO, "localizacaoADefinir", "Localização - a definir?", tabelaLocalizacaoProjeto);
    public static final Field campoLocalizacaoMunicipio = new Field(Category.PROJETO, "municipio", "Localização - Município?", tabelaLocalizacaoProjeto);
    public static final Field campoLocalizacaoRegiaoPlanejamento = new Field(Category.PROJETO, "regiaoPlanejamento", "Localização - Região de Planejamento", tabelaLocalizacaoProjeto);
    public static final Field campoLocalizacaoMicroRegiao = new Field(Category.PROJETO, "microRegiao", "Localização - Microrregião", tabelaLocalizacaoProjeto);
    public static final Field campoLocalizacaoLatitude = new Field(Category.PROJETO, "latitude", "Localização - Latitude", tabelaLocalizacaoProjeto);
    public static final Field campoLocalizacaoLongitude = new Field(Category.PROJETO, "longitude", "Localização - Longitude", tabelaLocalizacaoProjeto);

    public static final Field campoInvestimentoTotalPrevisto = new Field(Category.PROJETO, "investimentoTotalPrevisto", "Investimento - Total Previsto (R$ mil)", tabelaInvestimentoProjeto);
    public static final Field campoInvestimentoTotalReal = new Field(Category.PROJETO, "investimentoTotalRealizado", "Investimento - Total Real (R$ mil)", tabelaInvestimentoProjeto);

    public static final Field campoFaturamentoInicialPrevisto = new Field(Category.PROJETO, "faturamentoInicialPrevisto", "Faturamento - Inicial Previsto (R$ mil)", tabelaFaturamentoProjeto);
    public static final Field campoFaturamentoPlenoPrevisto = new Field(Category.PROJETO, "faturamentoPlenoPrevisto", "Faturamento Pleno Previsto (R$ mil)", tabelaFaturamentoProjeto);
    public static final Field campoFaturamentoInicialReal = new Field(Category.PROJETO, "faturamentoInicialReal", "Faturamento Inicial Real (R$ mil)", tabelaFaturamentoProjeto);
    public static final Field campoFaturamentoPlenoReal = new Field(Category.PROJETO, "faturamentoPlenoReal", "Faturamento Pleno Real (R$ mil)", tabelaFaturamentoProjeto);

    public static final Field campoEmpregoDirPermanenteTotalPrevisto = new Field(Category.PROJETO, "empregoDiretoPermanenteTotalPrevisto", "Emprego - Direto Permanente Total Previsto", tabelaEmpregoProjeto);
    public static final Field campoEmpregoDirPermanenteTotalReal = new Field(Category.PROJETO, "empregoDiretoPermanenteTotalReal", "Emprego - Direto Permanente Total Real", tabelaEmpregoProjeto);

    public static final Field campoInfraEstruturaAreaNecessaria = new Field(Category.PROJETO, "areaNecessaria", "Infraestrutura - Area necessária para o projeto (m2)", tabelaInfraEstruturaProjeto);
    public static final Field campoInfraEstruturaDemandaEnergNecessaria = new Field(Category.PROJETO, "demandaEnergiaEstimada", "Infraestrutura - Demanda Estimada de Energia Elétrica (KW)", tabelaInfraEstruturaProjeto);

    public static final Field campoHistoricoSituacaoProjeto = new Field(Category.PROJETO, "situacaoProjeto_id", "situacao", tabelaHistoricoSituacaoProjeto, false);
    public static final Field campoHistoricoSituacaoProjetoData = new Field(Category.PROJETO, "data", "situacaoData", tabelaHistoricoSituacaoProjeto, false);
    public static final Field campoHistoricoEstagioProjeto = new Field(Category.PROJETO, "estagio_id", "estagio", tabelaHistoricoEstagioProjeto, false);
    public static final Field campoHistoricoEstagioProjetoData = new Field(Category.PROJETO, "dataInicioEstagio", "estagioData", tabelaHistoricoEstagioProjeto, false);
    
    public static final Field campoProdutoNCM_id = new Field(Category.PROJETO, "ncm_id", "ncm_id", tabelaProduto, false);
    public static final Field campoInsumoNCM_id = new Field(Category.PROJETO, "ncm_id", "ncm_id", tabelaInsumo, false);
    public static final Field campoOrigemRecursoFinanceiro = new Field(Category.PROJETO, "origemNacional", "origemNacional", tabelaOrigemRecurso, false);
    public static final Field campoProjetoNecessidadeFinanciamentoBDMG = new Field(Category.PROJETO, "necessidadeFinanciamentoBDMG", "necessidadeFinanciamentoBDMG", tabelaProjeto, false);
    public static final Field campoProjetoClasseEmpreendimento = new Field(Category.PROJETO, "classeEmpreendimento", "classeEmpreendimento", tabelaProjeto, false);
    public static final Field campoEtapaMeioAmbienteTipo = new Field(Category.PROJETO, "tipo", "tipo", tabelaEtapaMeioAmbiente, false);

    //Instrumento Formalizacao
    public static final Field campoInstFormTipo = new Field(Category.INSTRUMENTO_FORMALIZACAO, "tipoDescricao", "Instrumento Formalização - Tipo do Instrumento de Formalização", tabelaInstrumentoFormalizacao);
    public static final Field campoInstFormTipo_id = new Field(Category.INSTRUMENTO_FORMALIZACAO, "tipo", "Instrumento Formalização - Tipo do Instrumento de Formalização", tabelaInstrumentoFormalizacao);
    public static final Field campoInstFormTitulo = new Field(Category.INSTRUMENTO_FORMALIZACAO, "titulo", "Instrumento Formalização - Título", tabelaInstrumentoFormalizacao);
    public static final Field campoInstFormProtocolo = new Field(Category.INSTRUMENTO_FORMALIZACAO, "protocolo", "Instrumento Formalização - Código do Protocolo de Intenções", tabelaInstrumentoFormalizacao);
    public static final Field campoInstFormDataPrevistaAssinatura = new Field(Category.INSTRUMENTO_FORMALIZACAO, "dataPrevistaAssinatura", "Instrumento Formalização - Data Prevista de Assinatura", tabelaInstrumentoFormalizacao);
    public static final Field campoInstFormDataRealAssinatura = new Field(Category.INSTRUMENTO_FORMALIZACAO, "dataRealAssinatura", "Instrumento Formalização - Data Real de Assinatura", tabelaInstrumentoFormalizacao);
    public static final Field campoInstFormSituacao = new Field(Category.INSTRUMENTO_FORMALIZACAO, "situacao", "Instrumento Formalização - Situação atual", tabelaInstrumentoFormalizacao);
    public static final Field campoInstFormSituacao_id = new Field(Category.INSTRUMENTO_FORMALIZACAO, "situacaoAtual_id", "Instrumento Formalização - Situação atual", tabelaInstrumentoFormalizacao);
    public static final Field campoInstFormDataInicioSituacao = new Field(Category.INSTRUMENTO_FORMALIZACAO, "dataInicioSituacao", "Instrumento Formalização - Data da Situação atual", tabelaInstrumentoFormalizacao);
    public static final Field campoInstFormDataPublicacao = new Field(Category.INSTRUMENTO_FORMALIZACAO, "datapublicacao", "Instrumento Formalização - Data de Publicação", tabelaInstrumentoFormalizacao);
    public static final Field campoPesquisaSatisfacaoRespondida = new Field(Category.INSTRUMENTO_FORMALIZACAO, "pesquisaSatisfacaoRespondida", "pesquisaSatisfacaoRespondida", tabelaInstrumentoFormalizacao);
    
    public static final Field campoInstFormDataPrevistaAssinatura_full = new Field(Category.INSTRUMENTO_FORMALIZACAO, "dataPrevistaAssinatura_full", "Instrumento Formalização - Data Prevista de Assinatura", tabelaInstrumentoFormalizacao, false);
    public static final Field campoInstFormDataRealAssinatura_full = new Field(Category.INSTRUMENTO_FORMALIZACAO, "dataRealAssinatura_full", "Instrumento Formalização - Data Real de Assinatura", tabelaInstrumentoFormalizacao, false);
    public static final Field campoInstFormDataInicioSituacao_full = new Field(Category.INSTRUMENTO_FORMALIZACAO, "dataInicioSituacao_full", "Instrumento Formalização - Data da Situação atual", tabelaInstrumentoFormalizacao, false);
    public static final Field campoInstFormDataPublicacao_full = new Field(Category.INSTRUMENTO_FORMALIZACAO, "datapublicacao_full", "Instrumento Formalização - Data de Publicação", tabelaInstrumentoFormalizacao, false);

    public static final Field campoAtividadeInstrumentoLocal = new Field(Category.INSTRUMENTO_FORMALIZACAO, "local_id", "local_id", tabelaAtividadeInstrumento);
    
    public static final Map<String, Field> CAMPOS_MAP = new HashMap<>();

    public static final Map<String, Condition> FILTROS_MAP = new HashMap();

    public static final Map<String, ConditionInfo> FILTROS = new HashMap();

    static {
        tabelaEmpresa.addPossibleLink(new TableLink(tabelaProjeto, "empresa_id"));
//        tabelaEmpresa.addPossibleLink(new TableLink(tabelaInstrumentoFormalizacao, "empresa_id"));
//        ChainedTableLink linkProjetoInstrFormalizacao = new ChainedTableLink(tabelaInstrumentoFormalizacao, "instrumentoFormalizacao_id", tabelaProjetoInstrumentoFormalizacao, "projeto_id");
        tabelaProjeto.addPossibleLink(new TableLink(tabelaInstrumentoFormalizacao, "projeto_id"));
//        tabelaProjetoInstrumentoFormalizacao.addPossibleLink(new TableLink(tabelaInstrumentoFormalizacao, "instrumentoFormalizacao_id"));

        tabelaCnaeEmpresaSubclasse.getAditionalTables().add(tabelaCnaeEmpresaClasse);
        tabelaCnaeEmpresaSubclasse.getAditionalTables().add(tabelaCnaeEmpresaGrupo);
        tabelaCnaeEmpresaSubclasse.getAditionalTables().add(tabelaCnaeEmpresaDivisao);

        tabelaInstrumentoFormalizacao.addPossibleLink(new TableLink(tabelaProjeto, "projeto_id"));

    }

    static {
        //Empresa
        FILTROS.put("empresaUnidade", new StandardConditionInfo(OrCondition.class, ConditionValueTypeEnum.STRING, ConditionComparisonTypeEnum.LIKE, campoEmpresaNomePrincipal, campoEmpresaRazaoSocial, campoUnidadeNome, campoEmpresaOutrosNomes));
        FILTROS.put("empresaNaturezaJuridica", new StandardConditionInfo(InCondition.class, ConditionValueTypeEnum.OTHER, campoEmpresaNatJuridica_id));
        FILTROS.put("empresaOrigem", new StandardConditionInfo(InCondition.class, ConditionValueTypeEnum.OTHER, campoEmpresaOrigem_id));
        FILTROS.put("empresaLocalizacao", new StandardConditionInfo(OrCondition.class, ConditionValueTypeEnum.STRING, ConditionComparisonTypeEnum.LIKE, campoEmpEnderecoCidade, campoEmpEnderecoBairro, campoEmpEnderecoMunicipio, campoEmpEnderecoPais));
        FILTROS.put("empresaPossuiProjetos", new StandardConditionInfo(SimpleCondition.class, ConditionValueTypeEnum.OTHER, ConditionComparisonTypeEnum.EQUALS, campoEmpresaPossuiProjetos));
        FILTROS.put("empresaNovaMG", new StandardConditionInfo(SimpleCondition.class, ConditionValueTypeEnum.OTHER, ConditionComparisonTypeEnum.EQUALS, campoEmpresaNovaMG));

        FILTROS.put("empresaSituacaoAtual", new StandardConditionInfo(InCondition.class, ConditionValueTypeEnum.OTHER, campoEmpresaSituacaoAtual_id));
        FILTROS.put("empresaCadeiaProdutiva", new StandardConditionInfo(InCondition.class, ConditionValueTypeEnum.OTHER, campoEmpresaCadeiaProdutiva_id));
        FILTROS.put("infoAdTipoRelacionamento", new StandardConditionInfo(InCondition.class, ConditionValueTypeEnum.OTHER, campoEmpresaInfoAdTipoContato_id));
        FILTROS.put("infoAdTipoRelacionamentoDatas", new StandardConditionInfo(BetweenCondition.class, ConditionValueTypeEnum.STRING, campoEmpresaInfoAdData_full));

        FILTROS.put("empresaDivisoesCnae", new StandardConditionInfo(InCondition.class, ConditionValueTypeEnum.STRING, campoEmpresaCnaeDivisao_id));
        FILTROS.put("empresaGruposCnae", new StandardConditionInfo(InCondition.class, ConditionValueTypeEnum.STRING, campoEmpresaCnaeGrupo_id));
        FILTROS.put("empresaClassesCnae", new StandardConditionInfo(InCondition.class, ConditionValueTypeEnum.STRING, campoEmpresaCnaeClasse_id));
        FILTROS.put("empresaSubclassesCnae", new StandardConditionInfo(InCondition.class, ConditionValueTypeEnum.STRING, campoEmpresaCnaeSubclasse_id));

        //Projeto
        FILTROS.put("projetoEstagioAtual", new StandardConditionInfo(InCondition.class, ConditionValueTypeEnum.OTHER, campoProjetoEstagioAtual_id));
        FILTROS.put("projetoSituacaoAtual", new StandardConditionInfo(InCondition.class, ConditionValueTypeEnum.OTHER, campoProjetoSituacaoAtual_id));
        FILTROS.put("projetoCadeiaProdutiva", new StandardConditionInfo(InCondition.class, ConditionValueTypeEnum.OTHER, campoProjetoCadeiaProdutiva_id));
        FILTROS.put("projetoResponsavelInvestimento", new StandardConditionInfo(InCondition.class, ConditionValueTypeEnum.OTHER, campoProjetoResponsavel_id));
        FILTROS.put("projetoResponsavelCadeiaProdutiva", new StandardConditionInfo(SimpleCondition.class, ConditionValueTypeEnum.OTHER, ConditionComparisonTypeEnum.EQUALS, campoProjetoResponsavelCadeiaProdutiva_id));
        FILTROS.put("projetoTipo", new StandardConditionInfo(InCondition.class, ConditionValueTypeEnum.OTHER, campoProjetoTipo_id));
        FILTROS.put("projetoGerencia", new StandardConditionInfo(InCondition.class, ConditionValueTypeEnum.OTHER, campoProjetoGerencia_id));
        FILTROS.put("projetoLocalizacao", new StandardConditionInfo(OrCondition.class, ConditionValueTypeEnum.STRING, ConditionComparisonTypeEnum.LIKE, campoLocalizacaoMunicipio, campoLocalizacaoMicroRegiao, campoLocalizacaoRegiaoPlanejamento));
        FILTROS.put("projetoUltimaVersao", new StandardConditionInfo(SimpleCondition.class, ConditionValueTypeEnum.OTHER, ConditionComparisonTypeEnum.EQUALS, campoProjetoUltimaVersao));
        FILTROS.put("projetoProspeccaoAtiva", new StandardConditionInfo(SimpleCondition.class, ConditionValueTypeEnum.OTHER, ConditionComparisonTypeEnum.EQUALS, campoProjetoProspeccaoAtiva_id));
        FILTROS.put("projetoEstagiosAlteracao", new StandardConditionInfo(InCondition.class, ConditionValueTypeEnum.OTHER, campoHistoricoEstagioProjeto));
        FILTROS.put("projetoEstagioAlteracaoDatas", new StandardConditionInfo(BetweenCondition.class, ConditionValueTypeEnum.STRING, campoHistoricoEstagioProjetoData));
        //        TODO implement this kind of condition
        Map<String, Field> projetoEstagiosPrevisaoFields = new HashMap<>();
        projetoEstagiosPrevisaoFields.put("1", campoProjetoDataPrevistaDF);
        projetoEstagiosPrevisaoFields.put("2", campoProjetoDataPrevistaII);
        projetoEstagiosPrevisaoFields.put("4", campoProjetoDataPrevistaOI);
        FILTROS.put("projetoEstagiosPrevisao", new ComposedConditionInfo(BetweenCondition.class, ConditionValueTypeEnum.STRING, projetoEstagiosPrevisaoFields));
//        FILTROS.put("projetoEstagioPrevisaoDatas", new StandardConditionInfo(BetweenCondition.class, ConditionValueTypeEnum.STRING, campoHistoricoSituacaoProjetoData));
        
        FILTROS.put("projetoSituacaoAlteracao", new StandardConditionInfo(InCondition.class, ConditionValueTypeEnum.OTHER, campoHistoricoSituacaoProjeto));
        FILTROS.put("projetoSituacaoAlteracaoDatas", new StandardConditionInfo(BetweenCondition.class, ConditionValueTypeEnum.STRING, campoHistoricoSituacaoProjetoData));
        
        FILTROS.put("empregoDirPermanenteTotalPrevisto", new StandardConditionInfo(BetweenCondition.class, ConditionValueTypeEnum.OTHER, campoEmpregoDirPermanenteTotalPrevisto));
        FILTROS.put("investimentoTotalPrevisto", new StandardConditionInfo(BetweenCondition.class, ConditionValueTypeEnum.OTHER, campoInvestimentoTotalPrevisto));
        FILTROS.put("faturamentoPlenoPrevisto", new StandardConditionInfo(BetweenCondition.class, ConditionValueTypeEnum.OTHER, campoFaturamentoPlenoPrevisto));
        FILTROS.put("faturamentoInicialPrevisto", new StandardConditionInfo(BetweenCondition.class, ConditionValueTypeEnum.OTHER, campoFaturamentoInicialPrevisto));
        
        FILTROS.put("empregoDirPermanenteTotalReal", new StandardConditionInfo(BetweenCondition.class, ConditionValueTypeEnum.OTHER, campoEmpregoDirPermanenteTotalReal));
        FILTROS.put("investimentoTotalReal", new StandardConditionInfo(BetweenCondition.class, ConditionValueTypeEnum.OTHER, campoInvestimentoTotalReal));
        FILTROS.put("faturamentoPlenoReal", new StandardConditionInfo(BetweenCondition.class, ConditionValueTypeEnum.OTHER, campoFaturamentoPlenoReal));
        FILTROS.put("faturamentoInicialReal", new StandardConditionInfo(BetweenCondition.class, ConditionValueTypeEnum.OTHER, campoFaturamentoInicialReal));
        
        FILTROS.put("projetoNecessidadeFinanciamentoBDMG", new StandardConditionInfo(InCondition.class, ConditionValueTypeEnum.OTHER, campoProjetoNecessidadeFinanciamentoBDMG));
        FILTROS.put("infraEstruturaEnergiaContratada", new StandardConditionInfo(BetweenCondition.class, ConditionValueTypeEnum.OTHER, campoInfraEstruturaDemandaEnergNecessaria));
        FILTROS.put("projetoClasseEmpreendimento", new StandardConditionInfo(InCondition.class, ConditionValueTypeEnum.OTHER, campoProjetoClasseEmpreendimento));
        FILTROS.put("projetoEtapaLicensiamentoAmbiental", new StandardConditionInfo(InCondition.class, ConditionValueTypeEnum.OTHER, campoEtapaMeioAmbienteTipo));
        
        FILTROS.put("insumoNCM", new StandardConditionInfo(SimpleCondition.class, ConditionValueTypeEnum.OTHER, ConditionComparisonTypeEnum.EQUALS, campoInsumoNCM_id));
        FILTROS.put("produtoNCM", new StandardConditionInfo(SimpleCondition.class, ConditionValueTypeEnum.OTHER, ConditionComparisonTypeEnum.EQUALS, campoProdutoNCM_id));
        FILTROS.put("financeiroOrigemRecurso", new StandardConditionInfo(InCondition.class, ConditionValueTypeEnum.OTHER, ConditionComparisonTypeEnum.EQUALS, campoOrigemRecursoFinanceiro));
        
        //InstrumentoFormalizacao
        FILTROS.put("instrumentoTipo", new StandardConditionInfo(InCondition.class, ConditionValueTypeEnum.OTHER, campoInstFormTipo_id));
        FILTROS.put("instrumentoSituacaoAtual", new StandardConditionInfo(InCondition.class, ConditionValueTypeEnum.OTHER, campoInstFormSituacao_id));
        FILTROS.put("instrumentoLocalDocumento", new StandardConditionInfo(InCondition.class, ConditionValueTypeEnum.OTHER, campoAtividadeInstrumentoLocal));
        FILTROS.put("instrumentoDataPrevistaAssinatura", new StandardConditionInfo(BetweenCondition.class, ConditionValueTypeEnum.STRING, campoInstFormDataPrevistaAssinatura_full));
        FILTROS.put("instrumentoDataAssinatura", new StandardConditionInfo(BetweenCondition.class, ConditionValueTypeEnum.STRING, campoInstFormDataRealAssinatura_full));
        FILTROS.put("instrumentoDataPublicacao", new StandardConditionInfo(BetweenCondition.class, ConditionValueTypeEnum.STRING, campoInstFormDataPublicacao_full));
        FILTROS.put("pesquisaSatisfacaoRespondida", new StandardConditionInfo(SimpleCondition.class, ConditionValueTypeEnum.OTHER, ConditionComparisonTypeEnum.EQUALS, campoPesquisaSatisfacaoRespondida));
        
        //TODO mudar o restante das datas dos filtros para a data do banco de dados
    }

    static {
        CAMPOS_MAP.put("empresaNomePrincipal", campoEmpresaNomePrincipal);
        CAMPOS_MAP.put("empresaRazaoSocial", campoEmpresaRazaoSocial);
        CAMPOS_MAP.put("empresaCnpj", campoEmpresaCnpj);
        CAMPOS_MAP.put("empresaInscricaoEstadual", campoEmpresaInscricaoEstadual);
        CAMPOS_MAP.put("empresaOrigem", campoEmpresaOrigem);
        CAMPOS_MAP.put("empresaEnderecoCompleto", campoEmpEnderecoCompleto);
        CAMPOS_MAP.put("empresaEnderecoPais", campoEmpEnderecoPais);
        CAMPOS_MAP.put("empresaEnderecoMunicipio", campoEmpEnderecoMunicipioCidade);
        CAMPOS_MAP.put("empresaEnderecoUF", campoEmpEnderecoUF);
        CAMPOS_MAP.put("empresaRegiaoPlanejamento", campoEmpRegiaoPlanejamento);
        CAMPOS_MAP.put("empresaMicroregiao", campoEmpMicroregiao);
        CAMPOS_MAP.put("empresaCnae", campoEmpresaCnaeSubclasse);
        CAMPOS_MAP.put("empresaComposicaoSocietaria", campoEmpresaComposicaoSocietaria);
        CAMPOS_MAP.put("empresaNaturezaJuridica", campoEmpresaNatJuridica);
        CAMPOS_MAP.put("empresaCadeiaProdutiva", campoEmpresaCadProdutiva);

        CAMPOS_MAP.put("unidadeNome", campoUnidadeNome);
        CAMPOS_MAP.put("unidadeEmail", campoUnidadeEmail);
        CAMPOS_MAP.put("unidadeTelefone", campoUnidadeTelefone);
        CAMPOS_MAP.put("unidadeEnderecoCompleto", campoUnidadeEnderecoCompleto);
        CAMPOS_MAP.put("unidadePais", campoUnidadePais);
        CAMPOS_MAP.put("unidadeMunicipio", campoUnidadeMunicipio);
        CAMPOS_MAP.put("unidadeEstado", campoUnidadeEstado);

        CAMPOS_MAP.put("empresaInfoAdTipoContato", campoEmpresaInfoAdTipoContato);
        CAMPOS_MAP.put("empresaInfoAdAssunto", campoEmpresaInfoAdAssunto);
        CAMPOS_MAP.put("empresaInfoAdData", campoEmpresaInfoAdData);
        CAMPOS_MAP.put("empresaInfoAdDescricao", campoEmpresaInfoAdDescricao);

        CAMPOS_MAP.put("empresaContatoEmpresaUnidade", campoContatoEmpresaUnidade);
        CAMPOS_MAP.put("empresaContatoNome", campoContatoNome);
        CAMPOS_MAP.put("empresaContatoCargo", campoContatoCargo);
        CAMPOS_MAP.put("empresaContatoTelefone1", campoContatoTelefone1);
        CAMPOS_MAP.put("empresaContatoTelefone2", campoContatoTelefone2);
        CAMPOS_MAP.put("empresaContatoEmail", campoContatoEmail);
        CAMPOS_MAP.put("empresaContatoMailing", campoContatoMailing);
        CAMPOS_MAP.put("empresaContatoStatus", campoContatoStatus);

        CAMPOS_MAP.put("projetoVersao", campoProjetoVersao);
        CAMPOS_MAP.put("projetoNome", campoProjetoNome);
        CAMPOS_MAP.put("projetoDescricao", campoProjetoDescricao);
        CAMPOS_MAP.put("projetoProspeccaoAtiva", campoProjetoProspeccaoAtiva);
        CAMPOS_MAP.put("projetoTipo", campoProjetoTipo);
        CAMPOS_MAP.put("projetoCadeiaProdutiva", campoProjetoCadeiaProdutiva);
        CAMPOS_MAP.put("projetoDataCadastro", campoProjetoDataCadastro);
        CAMPOS_MAP.put("projetoInformacaoPrimeiroContato", campoProjetoInformacaoPrimeiroContato);
        CAMPOS_MAP.put("projetoEstagioAtual", campoProjetoEstagioAtual);
        CAMPOS_MAP.put("projetoDataEstagioAtual", campoProjetoDataEstagioAtual);
        CAMPOS_MAP.put("projetoSituacaoAtual", campoProjetoSituacaoAtual);
        CAMPOS_MAP.put("projetoDataSituacaoAtual", campoProjetoDataSituacaoAtual);
        CAMPOS_MAP.put("projetoResponsavel", campoProjetoResponsavel);
        CAMPOS_MAP.put("projetoDataRealIP", campoProjetoDataRealIP);
        CAMPOS_MAP.put("projetoDataRealPP", campoProjetoDataRealPP);
        CAMPOS_MAP.put("projetoDataPrevistaDF", campoProjetoDataPrevistaDF);
        CAMPOS_MAP.put("projetoDataRealDF", campoProjetoDataRealDF);
        CAMPOS_MAP.put("projetoDataPrevistaII", campoProjetoDataPrevistaII);
        CAMPOS_MAP.put("projetoDataRealII", campoProjetoDataRealII);
        CAMPOS_MAP.put("projetoDataPrevistaOI", campoProjetoDataPrevistaOI);
        CAMPOS_MAP.put("projetoDataRealOI", campoProjetoDataRealOI);
        CAMPOS_MAP.put("projetoDataRealCC", campoProjetoDataRealCC);

        CAMPOS_MAP.put("localizacaoLocalizacaoADefinir", campoLocalizacaoLocalizacaoADefinir);
        CAMPOS_MAP.put("localizacaoMunicipio", campoLocalizacaoMunicipio);
        CAMPOS_MAP.put("localizacaoRegiaoPlanejamento", campoLocalizacaoRegiaoPlanejamento);
        CAMPOS_MAP.put("localizacaoMicroRegiao", campoLocalizacaoMicroRegiao);
        CAMPOS_MAP.put("localizacaoLatitude", campoLocalizacaoLatitude);
        CAMPOS_MAP.put("localizacaoLongetude", campoLocalizacaoLongitude);

        CAMPOS_MAP.put("investimentoTotalPrevisto", campoInvestimentoTotalPrevisto);
        CAMPOS_MAP.put("investimentoTotalRealizado", campoInvestimentoTotalReal);
        CAMPOS_MAP.put("faturamentoInicialTotalPrevisto", campoFaturamentoInicialPrevisto);
        CAMPOS_MAP.put("faturamentoPlenoPrevisto", campoFaturamentoPlenoPrevisto);
        CAMPOS_MAP.put("faturamentoInicialReal", campoFaturamentoInicialReal);
        CAMPOS_MAP.put("faturamentoPlenoReal", campoFaturamentoPlenoReal);
        CAMPOS_MAP.put("empregoDirPermanenteTotalPrevisto", campoEmpregoDirPermanenteTotalPrevisto);
        CAMPOS_MAP.put("empregoDirPermanenteTotalReal", campoEmpregoDirPermanenteTotalReal);

        CAMPOS_MAP.put("infraEstrAreaNecessaria", campoInfraEstruturaAreaNecessaria);
        CAMPOS_MAP.put("infraEstrDemandaEnergNecessaria", campoInfraEstruturaDemandaEnergNecessaria);

        CAMPOS_MAP.put("instFormTipo", campoInstFormTipo);
        CAMPOS_MAP.put("instFormTitulo", campoInstFormTitulo);
        CAMPOS_MAP.put("instFormProtocolo", campoInstFormProtocolo);
        CAMPOS_MAP.put("instFormDataPrevistaAssinatura", campoInstFormDataPrevistaAssinatura);
        CAMPOS_MAP.put("instFormDataRealAssinatura", campoInstFormDataRealAssinatura);
        CAMPOS_MAP.put("instFormSituacao", campoInstFormSituacao);
        CAMPOS_MAP.put("instFormDataInicioSituacao", campoInstFormDataInicioSituacao);
        CAMPOS_MAP.put("instFormDatapublicacao", campoInstFormDataPublicacao);
    }
}
