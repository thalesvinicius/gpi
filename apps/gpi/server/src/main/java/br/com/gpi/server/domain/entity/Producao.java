package br.com.gpi.server.domain.entity;

import br.com.gpi.server.domain.common.BaseEntity;
import com.fasterxml.jackson.annotation.JsonIgnore;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.envers.Audited;

@Audited
@Entity
@Table(name = "Producao")
public class Producao extends BaseEntity<Long>{
    
    @JsonIgnore
    @ManyToOne(cascade = {CascadeType.DETACH, CascadeType.REFRESH}, optional = false, fetch = FetchType.LAZY)
    @JoinColumn(name = "insumoProduto_id")    
    private InsumoProduto insumoProduto;
    
    private Integer ano;
    
    private Double areaPlantada;
    
    private Double moagemCana;
    
    private Double producaoEtanol;
    
    private Double producaoAcucar;
    
    private Double geracaoEnergia;
    
    private Double outro;

    public InsumoProduto getInsumoProduto() {
        return insumoProduto;
    }

    public void setInsumoProduto(InsumoProduto insumoProduto) {
        this.insumoProduto = insumoProduto;
    }

    public Integer getAno() {
        return ano;
    }

    public void setAno(Integer ano) {
        this.ano = ano;
    }

    public Double getAreaPlantada() {
        return areaPlantada;
    }

    public void setAreaPlantada(Double areaPlantada) {
        this.areaPlantada = areaPlantada;
    }

    public Double getMoagemCana() {
        return moagemCana;
    }

    public void setMoagemCana(Double moagemCana) {
        this.moagemCana = moagemCana;
    }

    public Double getProducaoEtanol() {
        return producaoEtanol;
    }

    public void setProducaoEtanol(Double producaoEtanol) {
        this.producaoEtanol = producaoEtanol;
    }

    public Double getProducaoAcucar() {
        return producaoAcucar;
    }

    public void setProducaoAcucar(Double producaoAcucar) {
        this.producaoAcucar = producaoAcucar;
    }

    public Double getGeracaoEnergia() {
        return geracaoEnergia;
    }

    public void setGeracaoEnergia(Double geracaoEnergia) {
        this.geracaoEnergia = geracaoEnergia;
    }

    public Double getOutro() {
        return outro;
    }

    public void setOutro(Double outro) {
        this.outro = outro;
    }
    
    
}
