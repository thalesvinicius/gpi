package br.com.gpi.server.domain.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import org.hibernate.envers.Audited;

@Audited
@Entity
@Table(name = "CNAEEmpresa")
@IdClass(CNAEEmpresaPK.class)
public class CNAEEmpresa implements Serializable  {
    
    public CNAEEmpresa() {
    }
    
    public CNAEEmpresa(Long empresaId, String cnaeId) {
        this.empresaId = empresaId;
        this.cnaeId = cnaeId;
    }
    
    
    @Column(name = "empresa_id", insertable = false, updatable = false)
    private Long empresaId;
    
    @Id
    @MapsId("cnaeId")
    @Column(name = "CNAE_id")
    private String cnaeId;
    
    @Id
    @MapsId("empresaId")
    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "empresa_id")
    private Empresa empresa;
    
    @JsonIgnoreProperties(value = {"denominacao", "cnaeSuperior"})
    @ManyToOne
    @JoinColumn(name = "CNAE_id", insertable = false, updatable = false)
    private CNAE cnae;
    
    @NotNull
    private Integer nivel;
    
    public Long getEmpresaId() {
        return empresaId;
    }

    public void setEmpresaId(Long empresaId) {
        this.empresaId = empresaId;
    }

    public String getCnaeId() {
        return cnaeId;
    }

    public void setCnaeId(String cnaeId) {
        this.cnaeId = cnaeId;
    }

    public Empresa getEmpresa() {
        return empresa;
    }

    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }

    public CNAE getCnae() {
        return cnae;
    }

    public void setCnae(CNAE cnae) {
        this.cnae = cnae;
    }

    public Integer getNivel() {
        return nivel;
    }

    public void setNivel(Integer nivel) {
        this.nivel = nivel;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 89 * hash + Objects.hashCode(this.empresaId);
        hash = 89 * hash + Objects.hashCode(this.cnaeId);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final CNAEEmpresa other = (CNAEEmpresa) obj;
        if (!Objects.equals(this.empresaId, other.empresaId)) {
            return false;
        }
        if (!Objects.equals(this.cnaeId, other.cnaeId)) {
            return false;
        }
        return true;
    }
    
}
