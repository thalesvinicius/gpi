package br.com.gpi.server.domain.service;

import br.com.gpi.server.domain.common.BaseService;
import br.com.gpi.server.domain.common.QueryParameter;
import br.com.gpi.server.domain.entity.UnidadeEmpresa;
import br.com.gpi.server.to.SearchEmpresaUnidadeTO;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

public class UnidadeEmpresaService extends BaseService<UnidadeEmpresa> {

    @Inject
    private UserTransaction transaction;

    @Inject
    private Logger logger;

    @Inject
    public UnidadeEmpresaService(EntityManager em) {
        super(UnidadeEmpresa.class, em);
    }

    public List<UnidadeEmpresa> findByEmpresaId(Long empresaId) {
        return findByNamedQuery(UnidadeEmpresa.Query.findAllByEmpresaId, empresaId);
    }

    public List<SearchEmpresaUnidadeTO> findWithEnderecoByEmpresaId(Long empresaId) {
        TypedQuery<SearchEmpresaUnidadeTO> createTypedQuery = createTypedQuery(SearchEmpresaUnidadeTO.class, UnidadeEmpresa.Query.findUnidadeEmpresaWithEndereco, QueryParameter.with("empresaId", empresaId).parameters());
        return createTypedQuery.getResultList();
    }

    public void removeUnidadeEmpresa(List<Long> ids) throws SystemException {
        try {
            transaction.begin();
            this.getEm().joinTransaction();

            List<UnidadeEmpresa> unidadesToDelete = this.findByNamedQuery(UnidadeEmpresa.Query.findAllByIds, ids);
            unidadesToDelete.forEach(u -> this.getEm().remove(u));

            transaction.commit();
        } catch (Exception ex) {
            transaction.rollback();
            logger.log(Level.SEVERE, null, ex);
            throw new RuntimeException(ex);
        }
    }
}
