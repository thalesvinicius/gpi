package br.com.gpi.server.domain.entity;

import br.com.gpi.server.core.jpa.typesafe.EnumerationEntityType;
import br.com.gpi.server.core.exception.EnumException;
import java.util.Objects;

public enum TipoEmprego implements EnumerationEntityType<Integer> {
    INVALID(0, "invalid"),  
    TEMPORARIO(1, "Temporário"), 
    PERMANENTE(2, "Permanente"),
    TEMPORARIO_OBRA(3, "Temporário durante Obra"), 
    PERMANENTE_OPERACAO(4, "Permanente após início Operação"),
    TEMPORARIO_IMPLANTACAO_AGRICOLA(5, "Temporário durante Implantação - Agrícola"),
    TEMPORARIO_IMPLANTACAO_INDUSTRIAL(6, "Temporário durante Implantação - Industrial"),
    PERMANENTE_MATURACAO_AGRICOLA(7, "Permanente após Maturação - Agrícola"),
    PERMANENTE_MATURACAO_INDUSTRIAL(8, "Permanente após Maturação - Industrial"),
    PERMANENTE_TOTAL_REALIZADO(9, "Permanente total realizado");

    private TipoEmprego(Integer id, String descricao) {
        this.id = id;
        this.descricao = descricao;
    }
    private final Integer id;

    private final String descricao;

    @Override
    public Integer getId() {
        return this.id;
    }

    @Override
    public String getDescription() {
        return descricao;
    }

    public static TipoEmprego getTipoEmprego(Integer id) {
        for (TipoEmprego tipoEmprego : values()) {
            if (Objects.equals(tipoEmprego.id, id)) {
                return tipoEmprego;
            }
        }
        throw new EnumException(String.format("TipoEmprego %s is not recognized", id));
    }
}
