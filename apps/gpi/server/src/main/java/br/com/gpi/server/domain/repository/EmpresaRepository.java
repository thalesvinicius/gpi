package br.com.gpi.server.domain.repository;

import br.com.gpi.server.domain.entity.Empresa;
import br.com.gpi.server.domain.entity.SituacaoEmpresa;
import br.com.gpi.server.domain.entity.UsuarioExterno;
import java.util.List;
import org.apache.deltaspike.data.api.EntityRepository;
import org.apache.deltaspike.data.api.Modifying;
import org.apache.deltaspike.data.api.Query;
import org.apache.deltaspike.data.api.Repository;
import org.apache.deltaspike.data.api.criteria.CriteriaSupport;

@Repository(forEntity = Empresa.class)
public abstract class EmpresaRepository implements CriteriaSupport<Empresa>, EntityRepository<Empresa, Long> {

    @Modifying
    @Query(named = Empresa.Query.deleteByid)
    public abstract void deleteById(List<Long> id);

    @Modifying
    @Query(named = Empresa.Query.updateSituacaoEmpresa)
    public abstract void updateSituacaoEmpresa(SituacaoEmpresa situacao, Long idEmpresa);

    @Modifying
    @Query(named = Empresa.Query.updateUsuarioExternoResponsavel)
    public abstract void updateUsuarioExternoResponsavel(UsuarioExterno responsavel, Long empresaId);
}
