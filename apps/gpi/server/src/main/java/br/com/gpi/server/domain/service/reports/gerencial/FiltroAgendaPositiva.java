/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.gpi.server.domain.service.reports.gerencial;

import br.com.gpi.server.domain.entity.CadeiaProdutiva;
import br.com.gpi.server.domain.entity.Departamento;
import br.com.gpi.server.domain.entity.Estagio;
import br.com.gpi.server.domain.entity.RegiaoPlanejamento;
import br.com.gpi.server.domain.entity.User;
import com.fasterxml.jackson.annotation.JsonRootName;
import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 *
 * @author rafaelbfs
 */
@JsonRootName("Filtro")
public class FiltroAgendaPositiva {
    
    private Date periodoInicio,periodoFim;
    private Set<CadeiaProdutiva> cadeiasProdutivas ;
    private Set<Departamento> gerencias ;
    private Set<Long> idsGerencias ;
    private Set<User> analistas ;    
    private Set<Long> idsUsuarios ;
    private Set<RegiaoPlanejamento> regiao ;
    private List<Estagio> estagios ;
    private Boolean versaoCorrente;
    private Long diretoria;
    private boolean buscarTodasGerencias;
    private boolean buscarTodasCadeiasProdutivas;
    private boolean buscarTodasRegioes;
    private boolean buscarTodosAnalistas;
    private boolean buscarTodosEstagios;

    public boolean isBuscarTodosEstagios() {
        return buscarTodosEstagios;
    }

    public void setBuscarTodosEstagios(boolean buscarTodosEstagios) {
        this.buscarTodosEstagios = buscarTodosEstagios;
    }

    public boolean isBuscarTodosAnalistas() {
        return buscarTodosAnalistas;
    }

    public void setBuscarTodosAnalistas(boolean buscarTodosAnalistas) {
        this.buscarTodosAnalistas = buscarTodosAnalistas;
    }

    public boolean isBuscarTodasGerencias() {
        return buscarTodasGerencias;
    }

    public void setBuscarTodasGerencias(boolean buscarTodasGerencias) {
        this.buscarTodasGerencias = buscarTodasGerencias;
    }

    public boolean isBuscarTodasCadeiasProdutivas() {
        return buscarTodasCadeiasProdutivas;
    }

    public void setBuscarTodasCadeiasProdutivas(boolean buscarTodasCadeiasProdutivas) {
        this.buscarTodasCadeiasProdutivas = buscarTodasCadeiasProdutivas;
    }

    public boolean isBuscarTodasRegioes() {
        return buscarTodasRegioes;
    }

    public void setBuscarTodasRegioes(boolean buscarTodasRegioes) {
        this.buscarTodasRegioes = buscarTodasRegioes;
    }

    public Long getDiretoria() {
        return diretoria;
    }

    public void setDiretoria(Long diretoria) {
        this.diretoria = diretoria;
    }

    public Boolean getVersaoCorrente() {
        return versaoCorrente;
    }

    public void setVersaoCorrente(Boolean versaoCorrente) {
        this.versaoCorrente = versaoCorrente;
    }

    

    public Date getPeriodoInicio() {
        return periodoInicio;
    }

    public void setPeriodoInicio(Date periodoInicio) {
        this.periodoInicio = periodoInicio;
    }

    public Date getPeriodoFim() {
        return periodoFim;
    }

    public void setPeriodoFim(Date periodoFim) {
        this.periodoFim = periodoFim;
    }

    public Set<CadeiaProdutiva> getCadeiasProdutivas() {
        return cadeiasProdutivas;
    }

    public void setCadeiasProdutivas(Set<CadeiaProdutiva> cadeiasProdutivas) {
        this.cadeiasProdutivas = cadeiasProdutivas;
    }

    public Set<Departamento> getGerencias() {
        return gerencias;
    }

    public void setGerencias(Set<Departamento> gerencias) {
        this.gerencias = gerencias;
    }

    public Set<User> getAnalistas() {
        return analistas;
    }

    public void setAnalistas(Set<User> analistas) {
        this.analistas = analistas;
    }

    public Set<RegiaoPlanejamento> getRegiao() {
        return regiao;
    }

    public void setRegiao(Set<RegiaoPlanejamento> regiao) {
        this.regiao = regiao;
    }

    public List<Estagio> getEstagios() {
        return estagios;
    }

    public void setEstagios(List<Estagio> estagios) {
        this.estagios = estagios;
    }
    
    
    public Set<Long> getIdsGerencias() {
        return idsGerencias;
    }

    public void setIdsGerencias(Set<Long> idsGerencias) {
        this.idsGerencias = idsGerencias;
    }

    public Set<Long> getIdsUsuarios() {
        return idsUsuarios;
    }

    public void setIdsUsuarios(Set<Long> idsUsuarios) {
        this.idsUsuarios = idsUsuarios;
    }
    
    

   
    
    
    
}
