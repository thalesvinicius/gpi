package br.com.gpi.server.domain.entity;

import br.com.gpi.server.core.jpa.converter.EstagioEnumConverter;
import br.com.gpi.server.domain.common.BaseEntity;
import br.com.gpi.server.domain.entity.enumerated.EnumIndicativoMeta;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import org.hibernate.envers.Audited;

@Audited
@Entity
@NamedQueries({
    @NamedQuery(name = Meta.Query.FIND_BY_INDICADOR , query = "SELECT m FROM Meta m WHERE m.indicador = ?1 and m.ano = ?2"),
    @NamedQuery(name = Meta.Query.FIND_BY_INDICADOR_AND_ESTAGIO , query = "SELECT m FROM Meta m WHERE m.indicador = ?1 and m.ano = ?2 and m.estagio = ?3")
})
public class Meta extends BaseEntity<Long> {
    
    @Column(name = "indicador_id")
    @Enumerated
    private EnumIndicativoMeta indicador;
    
    private Integer ano;
    private Integer mes;
    
    @Column(name = "meta")
    private Double valor;
    
    @Enumerated
    @Column(name = "estagio_id", updatable = false, insertable = false)
    private Estagio estagio;

    @Column(name = "estagio_id")
    private Integer estagioInt;
    
    public Meta() {
    }

    public EnumIndicativoMeta getIndicador() {
        return indicador;
    }

    public void setIndicador(EnumIndicativoMeta indicador) {
        this.indicador = indicador;
    }

    public Integer getAno() {
        return ano;
    }

    public void setAno(Integer ano) {
        this.ano = ano;
    }

    public Integer getMes() {
        return mes;
    }

    public void setMes(Integer mes) {
        this.mes = mes;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public Estagio getEstagio() {
        return estagio;
    }

    public void setEstagio(Estagio estagio) {
        this.estagio = estagio;
        this.estagioInt = (new EstagioEnumConverter()).convertToDatabaseColumn(estagio);
    }

    public Integer getEstagioInt() {
        return estagioInt;
    }

    public void setEstagioInt(Integer estagioInt) {
        this.estagioInt = estagioInt;
        this.estagio = (new EstagioEnumConverter()).convertToEntityAttribute(estagioInt);
    }
    
    
    
    public static class Query{
        public static final String FIND_BY_INDICADOR = "Meta.findByIndicador";
        public static final String FIND_BY_INDICADOR_AND_ESTAGIO = "Meta.findByIndicadorAndEstagio";
    }
}
