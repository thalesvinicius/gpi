package br.com.gpi.server.core.jpa.converter;

import br.com.gpi.server.domain.entity.enumerated.EnumCadeiaProdutiva;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = false)
public class CadeiaProdutivaEnumConverter implements AttributeConverter<EnumCadeiaProdutiva, Integer> {

    @Override
    public Integer convertToDatabaseColumn(EnumCadeiaProdutiva cadeiaProdutiva) {
        return cadeiaProdutiva.getId();
    }

    @Override
    public EnumCadeiaProdutiva convertToEntityAttribute(Integer dbData) {
        return EnumCadeiaProdutiva.getCadeiaProdutivaById(dbData);
    }

}
