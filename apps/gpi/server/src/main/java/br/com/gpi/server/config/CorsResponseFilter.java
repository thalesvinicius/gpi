package br.com.gpi.server.config;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Priority;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.ext.Provider;

@Provider
@Priority(Priorities.HEADER_DECORATOR)
public class CorsResponseFilter implements ContainerResponseFilter {

    @Override
    public void filter(ContainerRequestContext requestContext, ContainerResponseContext responseContext) throws IOException {
        String origin = requestContext.getHeaderString("Origin");
        responseContext.getHeaders()
                .putSingle("Access-Control-Allow-Credentials", "true");
        responseContext.getHeaders()
                .putSingle("Access-Control-Allow-Origin", (origin != null) ? origin : "*");
        responseContext.getHeaders()
                .putSingle("Access-Control-Allow-Methods",
                        "GET, POST, PUT, DELETE,OPTIONS");
        List<String> reqHead = requestContext.getHeaders()
                .get("Access-Control-Request-Headers");
        if (null != reqHead) {
            responseContext.getHeaders()
                    .put("Access-Control-Allow-Headers",
                            new ArrayList<>(reqHead));
        }
    }
}
