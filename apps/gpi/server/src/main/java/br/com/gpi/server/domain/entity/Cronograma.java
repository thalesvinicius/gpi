package br.com.gpi.server.domain.entity;

import br.com.gpi.server.domain.common.AuditedEntity;
import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.hibernate.envers.Audited;


@Audited
@Entity
@Table(name = "Cronograma")
@NamedQueries({
    @NamedQuery(name = Cronograma.Query.locateByProject, query = "SELECT c FROM Cronograma c WHERE c.projeto.id = ?1")
})
public class Cronograma extends AuditedEntity<Long> {

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "projeto_id")
    private Projeto projeto;
    @Temporal(TemporalType.DATE)
    private Date inicioProjeto;
    @Temporal(TemporalType.DATE)
    private Date inicioProjetoExecutivo;
    @Temporal(TemporalType.DATE)
    private Date inicioContratacaoEquipamentos;
    @Temporal(TemporalType.DATE)
    private Date inicioImplantacao;
    @Temporal(TemporalType.DATE)
    private Date inicioImplantacaoAgricola;
    @Temporal(TemporalType.DATE)
    private Date inicioImplantacaoIndustrial;
    @Temporal(TemporalType.DATE)
    private Date terminoImplantacaoIndustrial;
    @Temporal(TemporalType.DATE)
    private Date terminoImplantacaoAgricola;
    @Temporal(TemporalType.DATE)
    private Date inicioOperacao;
    @Temporal(TemporalType.DATE)
    private Date inicioOperacaoSucroenergetico;    
    @Temporal(TemporalType.DATE)
    private Date terminoProjetoExecutivo;
    @Temporal(TemporalType.DATE)
    private Date terminoProjeto;
    @Temporal(TemporalType.DATE)
    private Date inicioTratamentoTributarioPrevisto;
    @Temporal(TemporalType.DATE)
    private Date inicioTratamentoTributarioRealizado;

    public Cronograma() {
    }

    public static class Query {

        public static final String locateByProject = "Projeto.locateByProject";
    }

    public Projeto getProjeto() {
        return projeto;
    }

    public void setProjeto(Projeto projeto) {
        this.projeto = projeto;
    }

    public Date getInicioProjeto() {
        return inicioProjeto;
    }

    public void setInicioProjeto(Date inicioProjeto) {
        this.inicioProjeto = inicioProjeto;
    }

    public Date getInicioProjetoExecutivo() {
        return inicioProjetoExecutivo;
    }

    public void setInicioProjetoExecutivo(Date inicioProjetoExecutivo) {
        this.inicioProjetoExecutivo = inicioProjetoExecutivo;
    }

    public Date getInicioContratacaoEquipamentos() {
        return inicioContratacaoEquipamentos;
    }

    public void setInicioContratacaoEquipamentos(Date inicioContratacaoEquipamentos) {
        this.inicioContratacaoEquipamentos = inicioContratacaoEquipamentos;
    }

    public Date getInicioImplantacao() {
        return inicioImplantacao;
    }

    public void setInicioImplantacao(Date inicioImplantacao) {
        this.inicioImplantacao = inicioImplantacao;
    }

    public Date getInicioImplantacaoAgricola() {
        return inicioImplantacaoAgricola;
    }

    public void setInicioImplantacaoAgricola(Date inicioImplantacaoAgricola) {
        this.inicioImplantacaoAgricola = inicioImplantacaoAgricola;
    }

    public Date getInicioImplantacaoIndustrial() {
        return inicioImplantacaoIndustrial;
    }

    public void setInicioImplantacaoIndustrial(Date inicioImplantacaoIndustrial) {
        this.inicioImplantacaoIndustrial = inicioImplantacaoIndustrial;
    }

    public Date getTerminoImplantacaoIndustrial() {
        return terminoImplantacaoIndustrial;
    }

    public void setTerminoImplantacaoIndustrial(Date terminoImplantacaoIndustrial) {
        this.terminoImplantacaoIndustrial = terminoImplantacaoIndustrial;
    }

    public Date getTerminoImplantacaoAgricola() {
        return terminoImplantacaoAgricola;
    }

    public void setTerminoImplantacaoAgricola(Date terminoImplantacaoAgricola) {
        this.terminoImplantacaoAgricola = terminoImplantacaoAgricola;
    }

    public Date getInicioOperacao() {
        return inicioOperacao;
    }

    public void setInicioOperacao(Date inicioOperacao) {
        this.inicioOperacao = inicioOperacao;
    }
    
    public Date getInicioOperacaoSucroenergetico() {
        return inicioOperacaoSucroenergetico;
    }

    public void setInicioOperacaoSucroenergetico(Date inicioOperacaoSucroenergetico) {
        this.inicioOperacaoSucroenergetico = inicioOperacaoSucroenergetico;
    } 
    
    public Date getTerminoProjetoExecutivo() {
        return terminoProjetoExecutivo;
    }

    public void setTerminoProjetoExecutivo(Date terminoProjetoExecutivo) {
        this.terminoProjetoExecutivo = terminoProjetoExecutivo;
    }

    public Date getTerminoProjeto() {
        return terminoProjeto;
    }

    public void setTerminoProjeto(Date terminoProjeto) {
        this.terminoProjeto = terminoProjeto;
    }

    public Date getInicioTratamentoTributarioPrevisto() {
        return inicioTratamentoTributarioPrevisto;
    }

    public void setInicioTratamentoTributarioPrevisto(Date inicioTratamentoTributarioPrevisto) {
        this.inicioTratamentoTributarioPrevisto = inicioTratamentoTributarioPrevisto;
    }

    public Date getInicioTratamentoTributarioRealizado() {
        return inicioTratamentoTributarioRealizado;
    }

    public void setInicioTratamentoTributarioRealizado(Date inicioTratamentoTributarioRealizado) {
        this.inicioTratamentoTributarioRealizado = inicioTratamentoTributarioRealizado;
    }

}
