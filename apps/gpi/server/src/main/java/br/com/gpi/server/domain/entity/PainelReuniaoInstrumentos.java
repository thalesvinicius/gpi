package br.com.gpi.server.domain.entity;

import br.com.gpi.server.domain.common.BaseEntity;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.IdClass;
import javax.persistence.Table;
import javax.persistence.Temporal;
import org.hibernate.envers.Audited;

@Entity
//@IdClass(PainelReuniaoInstrumentosPK.class)
@Table(name = "VIEW_RELATORIO_PAINEL_REUNIOES_PROTOCOLOS")
public class PainelReuniaoInstrumentos extends BaseEntity<Long>{
    private Long instrumentoId;
    private Long projetoId;    
    private String projeto;    
    private String empresa;
    private String cidade;
    private String regiaoPlanejamento;
    private String cadeiaProdutiva;
    private String estagioAtual;    
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date dataEstagioAtual;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date dataEstagioDecisaoFormalizada;    
    private String analista;    
    private Long gerenciaId;    
    private String gerencia;   
    private Long diretoriaId;    
    private String diretoria;
    private Long cadeiaProdutivaId;
    private Boolean prospeccaoAtiva;
    private Long regiaoPlanejamentoId;
    private Long projetoEstagioatualId;    
    private Long tipoInstrumentoId;
    private String tipoInstrumento;    
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date dataAssinaturaInstrumento;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date dataAssinaturaPrevistaInstrumento;
    private Long situacaoInstrumentoId;    
    private String situacaoInstrumento;
    private String  justificativaInstrumento;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date dataCancelamentoInstrumento;	    
    private Long empregosDiretos;
    private Double investimentoPrevisto;
    private Double faturamentoInicial;
    private Double faturamentoPleno;
    private String situacaoProjeto;
    private Long situacaoProjetoId;
    private String justificativaProjeto;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date dataCancelamentoProjeto;    
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date dataCadastroProjeto;
    
    

    public String getEmpresa() {
        return empresa;
    }

    public void setEmpresa(String empresa) {
        this.empresa = empresa;
    }

    public String getProjeto() {
        return projeto;
    }

    public void setProjeto(String projeto) {
        this.projeto = projeto;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public String getRegiaoPlanejamento() {
        return regiaoPlanejamento;
    }

    public void setRegiaoPlanejamento(String regiaoPlanejamento) {
        this.regiaoPlanejamento = regiaoPlanejamento;
    }

    public String getCadeiaProdutiva() {
        return cadeiaProdutiva;
    }

    public void setCadeiaProdutiva(String cadeiaProdutiva) {
        this.cadeiaProdutiva = cadeiaProdutiva;
    }

    public String getEstagioAtual() {
        return estagioAtual;
    }

    public void setEstagioAtual(String estagioAtual) {
        this.estagioAtual = estagioAtual;
    }

    public Date getDataEstagioAtual() {
        return dataEstagioAtual;
    }

    public void setDataEstagioAtual(Date dataEstagioAtual) {
        this.dataEstagioAtual = dataEstagioAtual;
    }

    public String getAnalista() {
        return analista;
    }

    public void setAnalista(String analista) {
        this.analista = analista;
    }

    public String getGerencia() {
        return gerencia;
    }

    public void setGerencia(String gerencia) {
        this.gerencia = gerencia;
    }

    public Long getCadeiaProdutivaId() {
        return cadeiaProdutivaId;
    }

    public void setCadeiaProdutivaId(Long cadeiaProdutivaId) {
        this.cadeiaProdutivaId = cadeiaProdutivaId;
    }

    public Boolean getProspeccaoAtiva() {
        return prospeccaoAtiva;
    }

    public void setProspeccaoAtiva(Boolean prospeccaoAtiva) {
        this.prospeccaoAtiva = prospeccaoAtiva;
    }

    public Long getRegiaoPlanejamentoId() {
        return regiaoPlanejamentoId;
    }

    public void setRegiaoPlanejamentoId(Long regiaoPlanejamentoId) {
        this.regiaoPlanejamentoId = regiaoPlanejamentoId;
    }

    public Long getProjetoEstagioatualId() {
        return projetoEstagioatualId;
    }

    public void setProjetoEstagioatualId(Long projetoEstagioatualId) {
        this.projetoEstagioatualId = projetoEstagioatualId;
    }

    public Long getEmpregosDiretos() {
        return empregosDiretos;
    }

    public void setEmpregosDiretos(Long empregosDiretos) {
        this.empregosDiretos = empregosDiretos;
    }

    public Double getInvestimentoPrevisto() {
        return investimentoPrevisto;
    }

    public void setInvestimentoPrevisto(Double investimentoPrevisto) {
        this.investimentoPrevisto = investimentoPrevisto;
    }

    public Double getFaturamentoInicial() {
        return faturamentoInicial;
    }

    public void setFaturamentoInicial(Double faturamentoInicial) {
        this.faturamentoInicial = faturamentoInicial;
    }

    public Double getFaturamentoPleno() {
        return faturamentoPleno;
    }

    public void setFaturamentoPleno(Double faturamentoPleno) {
        this.faturamentoPleno = faturamentoPleno;
    }

    public Long getInstrumentoId() {
        return instrumentoId;
    }

    public void setInstrumentoId(Long instrumentoId) {
        this.instrumentoId = instrumentoId;
    }    

    public Long getGerenciaId() {
        return gerenciaId;
    }

    public void setGerenciaId(Long gerenciaId) {
        this.gerenciaId = gerenciaId;
    }

    public Long getDiretoriaId() {
        return diretoriaId;
    }

    public void setDiretoriaId(Long diretoriaId) {
        this.diretoriaId = diretoriaId;
    }

    public String getSituacaoProjeto() {
        return situacaoProjeto;
    }

    public void setSituacaoProjeto(String situacaoProjeto) {
        this.situacaoProjeto = situacaoProjeto;
    }

    public Long getSituacaoProjetoId() {
        return situacaoProjetoId;
    }

    public void setSituacaoProjetoId(Long situacaoProjetoId) {
        this.situacaoProjetoId = situacaoProjetoId;
    }

    public Date getDataAssinaturaInstrumento() {
        return dataAssinaturaInstrumento;
    }

    public void setDataAssinaturaInstrumento(Date dataAssinaturaInstrumento) {
        this.dataAssinaturaInstrumento = dataAssinaturaInstrumento;
    }

    public Long getSituacaoInstrumentoId() {
        return situacaoInstrumentoId;
    }

    public void setSituacaoInstrumentoId(Long situacaoInstrumentoId) {
        this.situacaoInstrumentoId = situacaoInstrumentoId;
    }

    public String getSituacaoInstrumento() {
        return situacaoInstrumento;
    }

    public void setSituacaoInstrumento(String situacaoInstrumento) {
        this.situacaoInstrumento = situacaoInstrumento;
    }

    public String getJustificativaInstrumento() {
        return justificativaInstrumento;
    }

    public void setJustificativaInstrumento(String justificativaInstrumento) {
        this.justificativaInstrumento = justificativaInstrumento;
    }

    public Date getDataCancelamentoInstrumento() {
        return dataCancelamentoInstrumento;
    }

    public void setDataCancelamentoInstrumento(Date dataCancelamentoInstrumento) {
        this.dataCancelamentoInstrumento = dataCancelamentoInstrumento;
    }

    public Date getDataCancelamentoProjeto() {
        return dataCancelamentoProjeto;
    }

    public void setDataCancelamentoProjeto(Date dataCancelamentoProjeto) {
        this.dataCancelamentoProjeto = dataCancelamentoProjeto;
    }

    public String getJustificativaProjeto() {
        return justificativaProjeto;
    }

    public void setJustificativaProjeto(String justificativaProjeto) {
        this.justificativaProjeto = justificativaProjeto;
    }

    public Long getTipoInstrumentoId() {
        return tipoInstrumentoId;
    }

    public void setTipoInstrumentoId(Long tipoInstrumentoId) {
        this.tipoInstrumentoId = tipoInstrumentoId;
    }

    public String getTipoInstrumento() {
        return tipoInstrumento;
    }

    public void setTipoInstrumento(String tipoInstrumento) {
        this.tipoInstrumento = tipoInstrumento;
    }

    public Date getDataAssinaturaPrevistaInstrumento() {
        return dataAssinaturaPrevistaInstrumento;
    }

    public void setDataAssinaturaPrevistaInstrumento(Date dataAssinaturaPrevistaInstrumento) {
        this.dataAssinaturaPrevistaInstrumento = dataAssinaturaPrevistaInstrumento;
    }

    public Date getDataCadastroProjeto() {
        return dataCadastroProjeto;
    }

    public void setDataCadastroProjeto(Date dataCadastroProjeto) {
        this.dataCadastroProjeto = dataCadastroProjeto;
    }

    public String getDiretoria() {
        return diretoria;
    }

    public void setDiretoria(String diretoria) {
        this.diretoria = diretoria;
    }

    public Date getDataEstagioDecisaoFormalizada() {
        return dataEstagioDecisaoFormalizada;
    }

    public void setDataEstagioDecisaoFormalizada(Date dataEstagioDecisaoFormalizada) {
        this.dataEstagioDecisaoFormalizada = dataEstagioDecisaoFormalizada;
    }

    /**
     * @return the projetoId
     */
    public Long getProjetoId() {
        return projetoId;
    }

    /**
     * @param projetoId the projetoId to set
     */
    public void setProjetoId(Long projetoId) {
        this.projetoId = projetoId;
    }
    
}