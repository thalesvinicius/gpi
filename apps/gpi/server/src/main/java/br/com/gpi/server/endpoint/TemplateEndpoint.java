package br.com.gpi.server.endpoint;

import br.com.gpi.server.domain.entity.Template;
import br.com.gpi.server.domain.entity.enumerated.EnumTipoTemplate;
import br.com.gpi.server.domain.repository.TemplateRepository;
import br.com.gpi.server.domain.repository.UserRepository;
import br.com.gpi.server.domain.user.model.SecurityUser;
import java.util.Date;
import java.util.List;
import javax.annotation.security.PermitAll;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

@Path("/template")
@Stateless
@PermitAll
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
@TransactionManagement(TransactionManagementType.BEAN)
public class TemplateEndpoint {

    @Inject
    TemplateRepository templateRepository;
    @Inject
    private UserRepository userRepository;

    @POST
    public Response saveOrUpdate(@Valid Template template, @Context SecurityContext context) {
        SecurityUser loggedUserWrap = (SecurityUser) context.getUserPrincipal();
        template.setUsuarioResponsavel(userRepository.findBy(loggedUserWrap.getId()));
        template.setDataUltimaAlteracao(new Date());
        template.setDataUltimaAlteracao(new Date());
        template.getAtividades().stream().forEach(a -> a.setTemplate(template));
        templateRepository.save(template);
        return Response.ok(template).build();
    }

    @GET
    public Response findAllByFilters(@QueryParam("nome") String nome,
            @QueryParam("tipo") EnumTipoTemplate tipo,
            @QueryParam("status") Boolean status) {
        List<Template> result = templateRepository.findByFilters(nome, tipo, status);
        return Response.ok(result).build();
    }

    @GET
    @Path("{id}")
    public Response findById(@PathParam("id") Long id) {
        Template template = templateRepository.findBy(id);
        return Response.ok(template).build();
    }

    @GET
    @Path("ativosByTipo/{tipo}")
    public Response findAtivosByTipo(@PathParam("tipo") EnumTipoTemplate tipoTemplate) {
        List<Template> templatesAtivos = templateRepository.findAllByStatusAndTipo(true, tipoTemplate);
        return Response.ok(templatesAtivos).build();
    }

    @POST
    @Path("removeTemplate")
    public Response removeUser(List<Long> ids) throws Exception {
        templateRepository.deleteById(ids);
        return Response.ok().build();
    }

}
