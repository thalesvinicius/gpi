package br.com.gpi.server.domain.entity;

import br.com.gpi.server.domain.common.AuditedEntity;
import br.com.gpi.server.domain.user.model.SecurityUser;
import br.com.gpi.server.util.HashUtil;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonSubTypes.Type;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.hibernate.validator.constraints.Email;

/**
 *
 * @author <a href="mailto:pviniciusfm@gmail.com">Paulo Vinicius F. Machado</a>
 */
@Audited
@Entity
@Table(name = "Usuario")
@Inheritance(strategy = InheritanceType.JOINED)
@DiscriminatorColumn(name = "tipo", length = 1, discriminatorType = DiscriminatorType.INTEGER)

@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        include = JsonTypeInfo.As.PROPERTY,
        property = "tipo")
@JsonSubTypes({
    @Type(value = UsuarioInterno.class, name = "1"),
    @Type(value = UsuarioExterno.class, name = "2")})
@NamedQueries({
    @NamedQuery(name = User.Query.findByIds, query = "select u FROM User u WHERE u.id in ?1")
})
public abstract class User extends AuditedEntity<Long> {

    private static final long serialVersionUID = 1L;
    private static final String HASH_SALT = "d8a8e885-ecce-42bb-8332-894f20f0d8ed";
    private static final int HASH_ITERATIONS = 1000;

    @Column(name = "senha", nullable = false, length = 128) //sha-512 + hex
    private String password;
    @Transient
    private transient String confirmPassword;

    @NotNull
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    @JoinColumn(name = "grupousuario_id", nullable = false)
    private GrupoUsuario role;

    @NotNull
    @Column(name = "ativo", nullable = false, columnDefinition = "bit")
    private Boolean ativo;

//    @NotNull
    @NotNull
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    @JoinColumn(name = "cargo_id", nullable = false)
    private Cargo cargo;

    //PERSON
    @NotNull
    @Column(name = "nome", length = 100, nullable = false)
    private String nome;

    @NotNull
    @Column(name = "telefone", length = 18, nullable = false)
    private String telefone;
    
    @Column(name = "telefone2", length = 18)
    private String telefone2;    

    @Email
    @Size(max = 50)
    @Column(name = "email", length = 50, unique = true, nullable = false)
    private String email;

    @JsonIgnore
    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<VerificationToken> verificationTokens;

    @JsonIgnore
    @NotAudited
    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Set<AccessToken> accessTokens;

    @Column(nullable = false, insertable = false, updatable = false)
    private Integer tipo;
    
    @Column(name = "mailing", columnDefinition = "bit")
    private Boolean mailing;

    public User() {
    }

    public User(Long id) {
        setId(id);
    }

    public User(String login, String password) {
        this();
        this.password = hashPassword(password);
    }

    public User(SecurityUser externalUser) {
        this();
        this.setId(externalUser.getId());
        this.email = externalUser.getEmail();
    }

    public String getTelefone2() {
        return telefone2;
    }

    public void setTelefone2(String telefone2) {
        this.telefone2 = telefone2;
    }
    
    @Override
    public String toString() {
        return "User [email=" + email + ", senha=" + password;
    }

    /**
     * Hash the password using salt values See
     * https://www.owasp.org/index.php/Hashing_Java
     *
     * @param passwordToHash
     * @return hashed password
     */
    public final String hashPassword(String passwordToHash) {
        return hashToken(passwordToHash, getLogin() + HASH_SALT);
    }

    private String hashToken(String token, String salt) {
        return HashUtil.byteToBase64(getHash(HASH_ITERATIONS, token, salt.getBytes()));
    }

    public byte[] getHash(int numberOfIterations, String password, byte[] salt) {
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            digest.reset();
            digest.update(salt);
            byte[] input = digest.digest(password.getBytes("UTF-8"));
            for (int i = 0; i < numberOfIterations; i++) {
                digest.reset();
                input = digest.digest(input);
            }
            return input;
        } catch (NoSuchAlgorithmException | UnsupportedEncodingException ex) {
            throw new RuntimeException("Actual implementation of JVM doesn't accepts SHA-256 Encoding or UTF-8 encoding. Call the system administrator.", ex);
        }
    }

    /**
     * If the user has a VerificationToken of type
     * VerificationTokenType.lostPassword that is active return it otherwise
     * return null
     *
     * @return verificationToken
     */
    @JsonIgnore
    public VerificationToken getActiveLostPasswordToken() {
        return getActiveToken(VerificationToken.VerificationTokenType.lostPassword);
    }

    /**
     * If the user has a VerificationToken of type
     * VerificationTokenType.emailVerification that is active return it
     * otherwise return null
     *
     * @return verificationToken
     */
    @JsonIgnore
    public VerificationToken getActiveEmailVerificationToken() {
        return getActiveToken(VerificationToken.VerificationTokenType.emailVerification);
    }

    private VerificationToken getActiveToken(VerificationToken.VerificationTokenType tokenType) {
        VerificationToken activeToken = null;
        for (VerificationToken token : getVerificationTokens()) {
            if (token.getTokenType().equals(tokenType)
                    && !token.hasExpired() && !token.isVerified()) {
                activeToken = token;
                break;
            }
        }
        return activeToken;
    }

    public void addVerificationToken(VerificationToken token) {
        if (this.verificationTokens == null) {
            this.verificationTokens = new ArrayList<>();
        }
        getVerificationTokens().add(token);
        token.setUser(this);
    }

    public boolean hasRole(GrupoUsuario role) {
        return this.role.equals(role);
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    public GrupoUsuario getRole() {
        return role;
    }

    public void setRole(GrupoUsuario role) {
        this.role = role;
    }

    public List<VerificationToken> getVerificationTokens() {
        return verificationTokens;
    }

    public void setVerificationTokens(List<VerificationToken> verificationTokens) {
        this.verificationTokens = verificationTokens;
    }

    public Set<AccessToken> getAccessTokens() {
        return accessTokens;
    }

    public void setAccessTokens(Set<AccessToken> accessTokens) {
        this.accessTokens = accessTokens;
    }

    public Boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(Boolean ativo) {
        this.ativo = ativo;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Cargo getCargo() {
        return cargo;
    }

    public void setCargo(Cargo cargo) {
        this.cargo = cargo;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getTipo() {
        return tipo;
    }

    public void setTipo(Integer tipo) {
        this.tipo = tipo;
    }

    public static class Query {

        public static final String locateByUUID = "User.locateByUUID";
        public static final String findByIds = "User.findByIds";
    }
    
    public void setRoleByEnum(UserGroup role) {
        this.setRole(new GrupoUsuario((long) role.getId(), role.getDescricao()));
    }
    
    public String generatePassword() {
        Random r = new Random();
        return Long.toString(r.nextLong(), 4);
    }

    public Boolean isMailing() {
        return mailing;
    }

    public void setMailing(Boolean mailing) {
        this.mailing = mailing;
    }

    public abstract String getLogin();
}
