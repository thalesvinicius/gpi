package br.com.gpi.server.domain.service;

import br.com.gpi.server.domain.common.BaseEntity;
import br.com.gpi.server.domain.common.BaseService;
import br.com.gpi.server.domain.entity.Estagio;
import br.com.gpi.server.domain.entity.InsumoProduto;
import br.com.gpi.server.domain.entity.Projeto;
import br.com.gpi.server.domain.entity.SituacaoProjetoEnum;
import br.com.gpi.server.domain.entity.User;
import br.com.gpi.server.domain.entity.UsuarioExterno;
import br.com.gpi.server.domain.repository.InsumoProdutoRepository;
import br.com.gpi.server.domain.repository.UserRepository;
import br.com.gpi.server.util.LogUtil;
import java.util.Collection;
import java.util.Date;
import java.util.function.Consumer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

public class InsumoProdutoService extends BaseService<InsumoProduto> {

    @Inject
    private InsumoProdutoRepository insumoProdutoRepository;

    @Inject
    private UserRepository userRepository;

    @Inject
    ProjetoService projetoService;
    
    @Inject
    private UserTransaction transaction;

    @Inject
    public InsumoProdutoService(EntityManager em) {
        super(InsumoProduto.class, em);
    }

    public Long saveInsumoProduto(InsumoProduto insumoProduto, Long idUsuario, Long idProjeto) throws SystemException {
        try {
            transaction.begin();
            this.getEm().joinTransaction();

            User user = userRepository.findBy(idUsuario);
            preSaveInsumoProduto(insumoProduto, user, idProjeto);
            insumoProduto = (InsumoProduto) this.merge(insumoProduto);

            //Altera a situação do projeto se o estágio for INICIO_PROJETO.
            if (projetoService.findEstagioAtualById(idProjeto).getEstagio() == Estagio.INICIO_PROJETO) {
                if(user instanceof UsuarioExterno) {
                    projetoService.updateSituacaoProjetoTransactional(SituacaoProjetoEnum.PENDENTE_ACEITE, idProjeto, user.getId(), new Date());
                } else {
                    projetoService.updateSituacaoProjetoTransactional(SituacaoProjetoEnum.PEDENTE_VALIDACAO, idProjeto, user.getId(), new Date());
                    
                }
            }

            transaction.commit();
            return insumoProduto.getId();
        } catch (NotSupportedException | SystemException | RollbackException | HeuristicMixedException | HeuristicRollbackException | SecurityException | IllegalStateException ex) {
            transaction.rollback();
            Logger.getLogger(ProjetoService.class.getName()).log(Level.SEVERE, null, ex);
            throw new RuntimeException(ex);
        }
    }

    private void preSaveInsumoProduto(InsumoProduto insumoProduto, User user, Long idProjeto) {

        insumoProduto.setUsuarioResponsavel(user);

        Projeto projeto = new Projeto();
        projeto.setId(idProjeto);

        insumoProduto.setProjeto(projeto);
        insumoProduto.setDataUltimaAlteracao(new Date());

        setBackReference(insumoProduto.getConcorrentes(), u -> u.setInsumoProduto(insumoProduto));
        setBackReference(insumoProduto.getInsumos(), u -> u.setInsumoProduto(insumoProduto));
        setBackReference(insumoProduto.getParcerias(), u -> u.setInsumoProduto(insumoProduto));
        setBackReference(insumoProduto.getProducoes(), u -> u.setInsumoProduto(insumoProduto));
        setBackReference(insumoProduto.getServicos(), u -> u.setInsumoProduto(insumoProduto));
        setBackReference(insumoProduto.getProdutosImpParaCom(), u -> u.setInsumoProduto(insumoProduto));
        setBackReference(insumoProduto.getProdutosFabEComPorMinas(), u -> u.setInsumoProduto(insumoProduto));
        setBackReference(insumoProduto.getProdutosAdqOutrosEstadosParaCom(), u -> u.setInsumoProduto(insumoProduto));
        setBackReference(insumoProduto.getProdutosAdqEComPorMinas(), u -> u.setInsumoProduto(insumoProduto));
    }

    private <T extends BaseEntity> void setBackReference(Collection<T> collection, Consumer<T> action) {
        if (collection != null) {
            collection.forEach(action);
        }
    }
    
    public Object verifyChange(Long idProjeto) throws SystemException {
        String atributos = " parcerias_MOD, percentualAdquiridoOutro_MOD, percentualClienteComercial_MOD, " +
            "percentualClienteConsumidor_MOD, percentualClienteIndustria_MOD, " +
            "percentualFabricaMinas_MOD, percentualICMS_MOD, percentualImportado_MOD, " +
            "insumos_MOD, interesseParceria_MOD, importacaoProduto_MOD, " +
            "contribuinteSubstituto_MOD, concorrentes_MOD, aquisicaoOutroEstado_MOD, " +
            "servicos_MOD, producoes_MOD, produtos_MOD, produtosAdqEComPorMinas_MOD, " +
            "produtosAdqOutrosEstadosParaCom_MOD, produtosFabEComPorMinas_MOD, produtosImpParaCom_MOD, " +
            "percentualMercadoExterior_MOD, percentualMercadoMinas_MOD, " +
            "percentualMercadoNacional_MOD, percentualOrigemImportado_MOD, percentualOrigemMinas_MOD, " +
            "percentualOrigemOutros_MOD, percentualTributoEfetivo_MOD, percentualValorAgregado_MOD ";

        StringBuilder query = new StringBuilder();
        query.append(" Select distinct " + atributos);
        query.append(" From view_logInsumoProduto Where projetoId = " + idProjeto);
        query.append(" And ((Select Max(data) From HistoricoSituacaoProjeto "); 
        query.append(" Where situacaoProjeto_id = 3 And projeto_id = ");
        query.append(idProjeto); 
        query.append(" ) < dataUltimaAlteracao) ");

        Object result = this.getEm().createNativeQuery(query.toString()).getResultList();
        
        return result;
    }    
}
