package br.com.gpi.server.endpoint;

import br.com.gpi.server.domain.service.BuscaAvancadaService;
import br.com.gpi.server.domain.user.model.SecurityUser;
import br.com.gpi.server.querybuilder.Locator;
import br.com.gpi.server.to.AdvancedSearchRequest;
import br.com.gpi.server.to.FieldTO;
import java.util.Collection;
import java.util.List;
import static java.util.stream.Collectors.toList;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

@Path("/buscaAvancada")
@Stateless
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
public class BuscaAvancadaEndpoint {

    @Inject
    private BuscaAvancadaService buscaAvancadaService;

    @GET
    @Path("getCampos")
    public Response getAllCampos() {        
        Collection<String> avaibleFields = Locator.CAMPOS_MAP.keySet();
        List<FieldTO> fields = avaibleFields.stream().map((f) -> new FieldTO(f,Locator.CAMPOS_MAP.get(f).getDescricao(),Locator.CAMPOS_MAP.get(f).getCategory())).collect(toList());
        return Response.ok(fields).build();
    }

    @POST
    public Response buscaAvancada(AdvancedSearchRequest request, @Context SecurityContext securityContext) {
        SecurityUser loggedUser = (SecurityUser) securityContext.getUserPrincipal();
        List buscaAvancada = buscaAvancadaService.buscaAvancada(request, loggedUser.getId());
        return Response.ok(buscaAvancada).build();
    }

}
