package br.com.gpi.server.to;

import br.com.gpi.server.domain.entity.Financeiro;
import br.com.gpi.server.domain.entity.enumerated.EnumTipoInvestimento;

public class InvestimentoPrevistoTO {
 
    private Financeiro financeiro;
    private EnumTipoInvestimento tipoInvestimento;
    private int ano;
    private Double valor;

    public InvestimentoPrevistoTO() {
    }

    public InvestimentoPrevistoTO(Double valor) {
        this.valor = valor;
    }

    public Financeiro getFinanceiro() {
        return financeiro;
    }

    public void setFinanceiro(Financeiro financeiro) {
        this.financeiro = financeiro;
    }

    public EnumTipoInvestimento getTipoInvestimento() {
        return tipoInvestimento;
    }

    public void setTipoInvestimento(EnumTipoInvestimento tipoInvestimento) {
        this.tipoInvestimento = tipoInvestimento;
    }

    public int getAno() {
        return ano;
    }

    public void setAno(int ano) {
        this.ano = ano;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }
    
}
