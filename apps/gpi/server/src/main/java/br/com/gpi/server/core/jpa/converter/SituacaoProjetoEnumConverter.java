package br.com.gpi.server.core.jpa.converter;

import br.com.gpi.server.domain.entity.SituacaoProjetoEnum;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = false)
public class SituacaoProjetoEnumConverter implements AttributeConverter<SituacaoProjetoEnum, Integer> {

    @Override
    public Integer convertToDatabaseColumn(SituacaoProjetoEnum situacao) {
        return situacao.getId();
    }

    @Override
    public SituacaoProjetoEnum convertToEntityAttribute(Integer id) {
        return SituacaoProjetoEnum.getSituacaoProjeto(id);
    }

}
