package br.com.gpi.server.domain.entity;

import br.com.gpi.server.domain.common.Persistable;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.hibernate.envers.Audited;

@Audited
@Table(name = "UsuarioProjetoRecente")
@Entity
@NamedQueries({
    @NamedQuery(name = ProjetoRecente.Query.findProjetoRecenteTOByUsuario, query = "SELECT new br.com.gpi.server.to.ProjetoTO(pr.projeto.id, pr.projeto.nome) FROM ProjetoRecente pr WHERE pr.usuarioId = ?1 ORDER BY pr.dataAcesso DESC"),
    @NamedQuery(name = ProjetoRecente.Query.findProjetoRecenteByUsuario, query = "SELECT pr From ProjetoRecente pr WHERE pr.usuarioId = ?1 ORDER BY pr.dataAcesso DESC")
})
@IdClass(ProjetoRecentePK.class)
public class ProjetoRecente implements Serializable, Persistable {

    @Id
    @Column(name = "projeto_id")
    private Long projetoId;
    @Id
    @Column(name = "usuario_id")
    private Long usuarioId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "projeto_id", insertable = false, updatable = false)
    private Projeto projeto;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "usuario_id", insertable = false, updatable = false)
    private User usuario;

    @Temporal(TemporalType.TIMESTAMP)
    private Date dataAcesso;

    public ProjetoRecente() {
    }

    public Long getProjetoId() {
        return projetoId;
    }

    public void setProjetoId(Long projetoId) {
        this.projetoId = projetoId;
    }

    public Long getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(Long usuarioId) {
        this.usuarioId = usuarioId;
    }

    public Projeto getProjeto() {
        return projeto;
    }

    public void setProjeto(Projeto projeto) {
        this.projeto = projeto;
    }

    public User getUsuario() {
        return usuario;
    }

    public void setUsuario(User usuario) {
        this.usuario = usuario;
    }

    public Date getDataAcesso() {
        return dataAcesso;
    }

    public void setDataAcesso(Date dataAcesso) {
        this.dataAcesso = dataAcesso;
    }

    public static class Query {

        public static final String findProjetoRecenteTOByUsuario = "UsuarioProjetoRecente.findProjetoRecenteTOByUsuario";
        public static final String findProjetoRecenteByUsuario = "UsuarioProjetoRecente.findProjetoRecenteByUsuario";

        public Query() {
        }
    }

}
