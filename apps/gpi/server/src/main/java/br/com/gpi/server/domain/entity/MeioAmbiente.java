package br.com.gpi.server.domain.entity;

import br.com.gpi.server.domain.common.AuditedEntity;
import br.com.gpi.server.domain.entity.enumerated.EnumTipoMeioAmbiente;
import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.Date;
import java.util.List;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import org.hibernate.envers.Audited;

@Audited
@Entity
@Table(name = "MeioAmbiente")
@NamedQueries({
    @NamedQuery(name = MeioAmbiente.Query.locateByProjetoIdForEdition, query = "SELECT ma FROM MeioAmbiente ma left join fetch ma.anexos left join fetch ma.etapasMeioAmbiente WHERE ma.projeto.id = ?")
})
public class MeioAmbiente extends AuditedEntity<Long> {

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "projeto_id")
    private Projeto projeto;

    @Column(columnDefinition = "bit")
    private Boolean licenciamentoIniciado;

    @Column(columnDefinition = "bit")
    private Boolean necessidadeEIARIMA;

    @Column(columnDefinition = "bit")
    private Boolean realizadaVistoria;

    @Column(columnDefinition = "bit")
    private Boolean pedidoInformacoesComplementar;

    private Date dataEntregaEstudos;

    private Date dataRealizadaVistoria;

    private Date dataPedidoInformacao;

    private Date dataEntregaInformacao;

    private int classeEmpreendimento;

    @Column(length = 10000)
    private String observacao;

    @ManyToMany(fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinTable(name = "MeioAmbienteAnexo", joinColumns = {
        @JoinColumn(name = "MeioAmbiente_id")}, inverseJoinColumns = {
        @JoinColumn(name = "anexo_id")})
    private Set<Anexo> anexos;

    @OneToMany(mappedBy = "meioAmbiente", fetch = FetchType.LAZY, orphanRemoval = true, cascade = CascadeType.ALL)
    private Set<EtapaMeioAmbiente> etapasMeioAmbiente;

    @Transient
    private Boolean dentroDoPrazo;

    @Transient
    private Set<EnumTipoMeioAmbiente> selectedEtapasMeioAmbiente;

    public Projeto getProjeto() {
        return projeto;
    }

    public void setProjeto(Projeto projeto) {
        this.projeto = projeto;
    }

    public Boolean isLicenciamentoIniciado() {
        return licenciamentoIniciado;
    }

    public Boolean getLicenciamentoIniciado() {
        return licenciamentoIniciado;
    }

    public void setLicenciamentoIniciado(Boolean licenciamentoIniciado) {
        this.licenciamentoIniciado = licenciamentoIniciado;
    }

    public Boolean isNecessidadeEIARIMA() {
        return necessidadeEIARIMA;
    }

    public void setNecessidadeEIARIMA(Boolean necessidadeEIARIMA) {
        this.necessidadeEIARIMA = necessidadeEIARIMA;
    }

    public Boolean isRealizadaVistoria() {
        return realizadaVistoria;
    }

    public void setRealizadaVistoria(Boolean realizadaVistoria) {
        this.realizadaVistoria = realizadaVistoria;
    }

    public Boolean isPedidoInformacoesComplementar() {
        return pedidoInformacoesComplementar;
    }

    public void setPedidoInformacoesComplementar(Boolean pedidoInformacoesComplementar) {
        this.pedidoInformacoesComplementar = pedidoInformacoesComplementar;
    }

    public Date getDataEntregaEstudos() {
        return dataEntregaEstudos;
    }

    public void setDataEntregaEstudos(Date dataEntregaEstudos) {
        this.dataEntregaEstudos = dataEntregaEstudos;
    }

    public Date getDataRealizadaVistoria() {
        return dataRealizadaVistoria;
    }

    public void setDataRealizadaVistoria(Date dataRealizadaVistoria) {
        this.dataRealizadaVistoria = dataRealizadaVistoria;
    }

    public Set<EnumTipoMeioAmbiente> getSelectedEtapasMeioAmbiente() {
        return selectedEtapasMeioAmbiente;
    }

    public void setSelectedEtapasMeioAmbiente(Set<EnumTipoMeioAmbiente> selectedEtapasMeioAmbiente) {
        this.selectedEtapasMeioAmbiente = selectedEtapasMeioAmbiente;
    }

    public Date getDataPedidoInformacao() {
        return dataPedidoInformacao;
    }

    public void setDataPedidoInformacao(Date dataPedidoInformacao) {
        this.dataPedidoInformacao = dataPedidoInformacao;
    }

    public Date getDataEntregaInformacao() {
        return dataEntregaInformacao;
    }

    public void setDataEntregaInformacao(Date dataEntregaInformacao) {
        this.dataEntregaInformacao = dataEntregaInformacao;
    }

    public int getClasseEmpreendimento() {
        return classeEmpreendimento;
    }

    public void setClasseEmpreendimento(int classeEmpreendimento) {
        this.classeEmpreendimento = classeEmpreendimento;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public Set<Anexo> getAnexos() {
        return anexos;
    }

    public void setAnexos(Set<Anexo> anexos) {
        this.anexos = anexos;
    }

    public Set<EtapaMeioAmbiente> getEtapasMeioAmbiente() {
        return etapasMeioAmbiente;
    }

    public void setEtapasMeioAmbiente(Set<EtapaMeioAmbiente> etapasMeioAmbiente) {
        this.etapasMeioAmbiente = etapasMeioAmbiente;
    }

    public Boolean isDentroDoPrazo() {
        return dentroDoPrazo;
    }

    public void setDentroDoPrazo(Boolean dentroDoPrazo) {
        this.dentroDoPrazo = dentroDoPrazo;
    }
    
    
    
    public static class Query {

        public static final String locateByProjetoIdForEdition = "MeioAmbiente.locateByProjetoId";
    }
}
