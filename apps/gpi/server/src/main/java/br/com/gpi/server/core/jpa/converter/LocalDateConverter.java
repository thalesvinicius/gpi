package br.com.gpi.server.core.jpa.converter;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = false)
public class LocalDateConverter implements AttributeConverter<LocalDate, Date> {

    @Override
    public Date convertToDatabaseColumn(LocalDate ld) {
        Date date = null;
        if (ld != null) {
            Instant instant = ld.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant();
            date = Date.from(instant);
        }
        return date;
    }

    @Override
    public LocalDate convertToEntityAttribute(Date date) {
        LocalDate localDate = null;
        if (date != null) {
            Instant instant = Instant.ofEpochMilli(date.getTime());
            localDate = LocalDateTime.ofInstant(instant, ZoneId.systemDefault()).toLocalDate();
        }
        return localDate;
    }
}
