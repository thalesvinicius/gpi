package br.com.gpi.server.domain.repository;

import br.com.gpi.server.domain.entity.HistoricoSituacaoInstrumentoFormalizacao;
import org.apache.deltaspike.data.api.EntityRepository;
import org.apache.deltaspike.data.api.Query;
import org.apache.deltaspike.data.api.Repository;
import org.apache.deltaspike.data.api.SingleResultType;
import org.apache.deltaspike.data.api.criteria.CriteriaSupport;

@Repository(forEntity = HistoricoSituacaoInstrumentoFormalizacao.class)
public abstract class HistoricoSituacaoInstrumentoFormalizacaoRepository  implements CriteriaSupport<HistoricoSituacaoInstrumentoFormalizacao>, EntityRepository<HistoricoSituacaoInstrumentoFormalizacao, Long>{

    @Query(named = HistoricoSituacaoInstrumentoFormalizacao.Query.findUltimoHistoricoSituacaoByInstrumentoId,singleResult = SingleResultType.ANY)
    public abstract HistoricoSituacaoInstrumentoFormalizacao findUltimoHistoricoSituacaoByInstrumentoId(Long InstrumentoFormalizacaoId);
    
    
}
