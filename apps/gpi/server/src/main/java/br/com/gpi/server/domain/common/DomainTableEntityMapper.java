package br.com.gpi.server.domain.common;

import br.com.gpi.server.core.message.AplicationMessages;
import br.com.gpi.server.domain.entity.AtividadeLocal;
import br.com.gpi.server.domain.entity.CadeiaProdutiva;
import br.com.gpi.server.domain.entity.Cargo;
import br.com.gpi.server.domain.entity.Departamento;
import br.com.gpi.server.domain.entity.EstagioProjeto;
import br.com.gpi.server.domain.entity.EtapaMeioAmbiente;
import br.com.gpi.server.domain.entity.GrupoUsuario;
import br.com.gpi.server.domain.entity.Local;
import br.com.gpi.server.domain.entity.MicroRegiao;
import br.com.gpi.server.domain.entity.Municipio;
import br.com.gpi.server.domain.entity.NCM;
import br.com.gpi.server.domain.entity.NaturezaJuridica;
import br.com.gpi.server.domain.entity.Pais;
import br.com.gpi.server.domain.entity.RegiaoPlanejamento;
import br.com.gpi.server.domain.entity.SituacaoInstrumentoFormalizacao;
import br.com.gpi.server.domain.entity.SituacaoProjetoEnum;
import br.com.gpi.server.domain.entity.UF;
import java.util.HashMap;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.WebApplicationException;

@ApplicationScoped
public class DomainTableEntityMapper {

    @Inject
    private AplicationMessages messages;

    private final Map<String, Class> domainTableClassMap;

    public DomainTableEntityMapper() {
        domainTableClassMap = new HashMap<>();
    }

    public DomainTableEntityMapper(Map<String, Class> domainTableClassMap) {
        this.domainTableClassMap = domainTableClassMap;
    }

    @PostConstruct
    public void initialize() {
        domainTableClassMap.put("cargo", Cargo.class);
        domainTableClassMap.put("departamento", Departamento.class);
        domainTableClassMap.put("cadeiaProdutiva", CadeiaProdutiva.class);
        domainTableClassMap.put("grupoUsuario", GrupoUsuario.class);
        domainTableClassMap.put("municipio", Municipio.class); 
        domainTableClassMap.put("uf", UF.class);
        domainTableClassMap.put("regiaoPlanejamento", RegiaoPlanejamento.class);
        domainTableClassMap.put("microRegiao", MicroRegiao.class);
        domainTableClassMap.put("naturezaJuridica", NaturezaJuridica.class);
        domainTableClassMap.put("pais", Pais.class);
        domainTableClassMap.put("estagioProjeto", EstagioProjeto.class);
        domainTableClassMap.put("situacaoProjeto", SituacaoProjetoEnum.class);
        domainTableClassMap.put("ncm", NCM.class);
        domainTableClassMap.put("etapaMeioAmbiente", EtapaMeioAmbiente.class);
        domainTableClassMap.put("atividadeLocal", AtividadeLocal.class);
        domainTableClassMap.put("local", Local.class);
        domainTableClassMap.put("situacaoInstrumentoFormalizacao", SituacaoInstrumentoFormalizacao.class);
    }

    public Class getClassOfEntity(String className) {

        Class _class = domainTableClassMap.get(className);
        if (_class != null) {
            return _class;
        } else {
            throw new WebApplicationException(messages.domainNotFoundedMessage());
        }
    }
}
