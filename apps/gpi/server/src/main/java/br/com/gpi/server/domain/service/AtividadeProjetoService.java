package br.com.gpi.server.domain.service;

import br.com.gpi.server.domain.common.BaseService;
import br.com.gpi.server.domain.entity.AtividadeProjeto;
import br.com.gpi.server.domain.entity.AtividadeTemplate;
import br.com.gpi.server.domain.entity.Cronograma;
import br.com.gpi.server.domain.entity.Estagio;
import br.com.gpi.server.domain.entity.Projeto;
import br.com.gpi.server.domain.entity.Template;
import br.com.gpi.server.domain.entity.User;
import br.com.gpi.server.domain.entity.enumerated.EnumTipoTemplate;
import br.com.gpi.server.domain.repository.AtividadeLocalRepository;
import br.com.gpi.server.domain.repository.AtividadeProjetoRepository;
import br.com.gpi.server.domain.repository.TemplateRepository;
import br.com.gpi.server.to.AtividadeProjetoTO;
import br.com.gpi.server.to.CronogramaAtividadesTO;
import br.com.gpi.server.to.InstrumentoFormalizacaoTO;
import br.com.gpi.server.to.LocalTO;
import br.com.gpi.server.util.DateUtils;
import java.util.Date;
import java.util.List;
import javax.inject.Inject;
import javax.persistence.EntityManager;

public class AtividadeProjetoService extends BaseService<AtividadeProjeto> {

    @Inject
    private TemplateRepository templateRepository;
    @Inject
    private AtividadeProjetoRepository atividadeProjetoRepository;
    @Inject
    private ProjetoService projetoService;
    @Inject
    private CronogramaService cronogramaService;
    @Inject
    private InstrumentoFormalizacaoService intrFormalizacaoService;
    @Inject
    private AtividadeLocalRepository atividadeLocalRepository;

    @Inject
    public AtividadeProjetoService(EntityManager em) {
        super(AtividadeProjeto.class, em);
    }

    public List<AtividadeProjeto> copyTemplateAtividadesToProjeto(Long projetoId, User loggedUser) {
        List<Template> templateAtivo = templateRepository.findAllByStatusAndTipo(Boolean.TRUE, EnumTipoTemplate.PROJETO);
        List<AtividadeProjeto> atividades = null;
        if (!templateAtivo.isEmpty()) {
            Template templateAtividadesProjeto = templateAtivo.get(0);

            Projeto projeto = projetoService.findById(projetoId);

            if (templateAtividadesProjeto.getAtividades() != null && !templateAtividadesProjeto.getAtividades().isEmpty()) {
                for (AtividadeTemplate atividadeTemplate : templateAtividadesProjeto.getAtividades()) {
                    AtividadeProjeto atividadeProjeto = new AtividadeProjeto();
                    atividadeProjeto.setAtividadeLocal(atividadeTemplate.getAtividadeLocal());
                    atividadeProjeto.setBloqueiaICE(atividadeTemplate.getBloqueiaICE());
                    atividadeProjeto.setCc(atividadeTemplate.getCc());
                    atividadeProjeto.setDf(atividadeTemplate.getDf());
                    atividadeProjeto.setIi(atividadeTemplate.getIi());
                    atividadeProjeto.setObrigatorio(atividadeTemplate.getObrigatorio());
                    atividadeProjeto.setOi(atividadeTemplate.getOi());
                    atividadeProjeto.setPosicao(atividadeTemplate.getPosicao());
                    atividadeProjeto.setUsuarioResponsavel(loggedUser);
                    projeto.addAtividade(atividadeProjeto);
                    atividadeProjetoRepository.save(atividadeProjeto);
                }
            }

            for (AtividadeProjeto atividade : projeto.getAtividades()) {
                atividade.setLocalAtividade(new LocalTO(atividade.getAtividadeLocal().getLocal().getId(), atividade.getAtividadeLocal().getLocal().getDescricao()));
                atividade.getLocalAtividade().setAtividades(atividadeLocalRepository.findAllByLocalId(atividade.getAtividadeLocal().getLocal().getId()));
            };

            atividades = projeto.getAtividades();
        }
        return atividades;

    }

    public AtividadeProjetoTO findByProjetoIdForEdit(Long projetoId) {
        AtividadeProjetoTO atividadeProjetoTO = new AtividadeProjetoTO();
        List<AtividadeProjeto> savedAtividadesProjeto = findByNamedQuery(AtividadeProjeto.Query.findByProjetoId, projetoId);
        for (AtividadeProjeto atividade : savedAtividadesProjeto) {
            atividade.setLocalAtividade(new LocalTO(atividade.getAtividadeLocal().getLocal().getId(), atividade.getAtividadeLocal().getLocal().getDescricao()));
            atividade.getLocalAtividade().setAtividades(atividadeLocalRepository.findAllByLocalId(atividade.getAtividadeLocal().getLocal().getId()));
        };
        atividadeProjetoTO.setAtividades(savedAtividadesProjeto);
        atividadeProjetoTO.setCabecalho(projetoService.findProjetoTOById(projetoId));
        atividadeProjetoTO.setCronograma(recoverAndBuildCronogramaAtividade(projetoId));

        return atividadeProjetoTO;
    }

    public AtividadeProjeto saveAtividadesProjeto(AtividadeProjeto atividade, Long projetoId, User loggedUser) {
        atividade.setUsuarioResponsavel(loggedUser);
        Projeto projeto = new Projeto();
        projeto.setId(projetoId);
        boolean isAtividadeConcluida = atividade.getDataConclusao() != null && atividade.isConcluido();

        atividade.setProjeto(projeto);
        atividadeProjetoRepository.save(atividade);
        
        if (isAtividadeConcluida) {
            modificaEstagioEStatusProjeto(atividade, projetoId, loggedUser, atividade.getDataConclusao());
            projetoService.updateUltimaAtividadeProjeto(projetoId, atividade);
        }

        projetoService.updateUsuarioeData(projetoId, loggedUser, new Date());

        return atividade;
    }

    public CronogramaAtividadesTO recoverAndBuildCronogramaAtividade(Long projetoId) {
        Projeto projeto = projetoService.findById(projetoId);
        Cronograma cronograma = null;
        if (projeto.getCronogramas() != null && !projeto.getCronogramas().isEmpty()) {
            cronograma = projeto.getCronogramas().iterator().next();
        }
        CronogramaAtividadesTO cronogramaTO = new CronogramaAtividadesTO(cronograma, projeto.getEstagios(), projeto.getSituacao());

        List<InstrumentoFormalizacaoTO> instrumentoFormalizacoes = projetoService.findDatasInstrumentoFormalizacao(projetoId);
        if (instrumentoFormalizacoes != null && !instrumentoFormalizacoes.isEmpty()) {
            InstrumentoFormalizacaoTO instrTO = instrumentoFormalizacoes.get(0);
            cronogramaTO.setDfPrevisto(DateUtils.convertToLocalDate(instrTO.getDataAssinaturaPrevista()));
        }
        return cronogramaTO;

    }

    private void modificaEstagioEStatusProjeto(AtividadeProjeto atividade, Long projetoId, User loggedUser, Date dataConclusao) {
        if (atividade.getCc() != null && atividade.getCc()) {
            projetoService.updateEstagioProjeto(Estagio.COMPROMISSO_CUMPRIDO, projetoId, loggedUser.getId(), dataConclusao);
        } else if (atividade.getIi() != null && atividade.getIi()) {
            projetoService.updateEstagioProjeto(Estagio.IMPLANTACAO_INICIADO, projetoId, loggedUser.getId(), dataConclusao);
        } else if (atividade.getOi() != null && atividade.getOi()) {
            projetoService.updateEstagioProjeto(Estagio.OPERACAO_INICIADA, projetoId, loggedUser.getId(), dataConclusao);
        }
    }

}
