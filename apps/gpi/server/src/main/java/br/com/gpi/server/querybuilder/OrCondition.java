package br.com.gpi.server.querybuilder;

import java.util.List;

public class OrCondition extends Condition {

    private String value;
    private ConditionComparisonTypeEnum comparisonType;

    public OrCondition() {
    }

    public OrCondition(String value, String conditionKey, List<Field> fields, ConditionValueTypeEnum type, ConditionComparisonTypeEnum comparisonType) {
        super(conditionKey, fields, type);
        this.value = value;
        this.comparisonType = comparisonType;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public ConditionComparisonTypeEnum getComparisonType() {
        return comparisonType;
    }

    public void setComparisonType(ConditionComparisonTypeEnum comparisonType) {
        this.comparisonType = comparisonType;
    }

    @Override
    public String getSQL() {
        StringBuilder sb = new StringBuilder();
        sb.append("(");
        getFields().stream().forEachOrdered(u -> {
            sb.append(u.getTabela().getAlias());
            sb.append(".");
            sb.append(u.getNome());
            
            if (getValueType().equals(ConditionValueTypeEnum.STRING)) {
                sb.append(comparisonType.getDescricao().replace("{value}", comparisonType.equals(ConditionComparisonTypeEnum.LIKE) ? getValue() : "'".concat(getValue()).concat("'")));
            } else {
                if(comparisonType.equals(ConditionComparisonTypeEnum.LIKE)){
                    throw new RuntimeException("ConditionComparisonTypeEnum.LIKE is a wrong ConditionComparisonTypeEnum for ConditionValueTypeEnum.NON_STRING");
                }
                sb.append(comparisonType.getDescricao().replace("{value}", getValue()));
            }
            
            sb.append(" OR ");
        });
        
        sb.delete(sb.length() - 4, sb.length());
        sb.append(")");
        
        return sb.toString();
    }

}
