package br.com.gpi.server.domain.entity.enumerated;

import br.com.gpi.server.core.exception.EnumException;
import br.com.gpi.server.core.jpa.typesafe.EnumerationEntityType;

public enum EnumTipoMeioAmbiente implements EnumerationEntityType<Integer> {
    INVALID(0, "invalid"),  
    LP(1, "LP - Licença Prévia"),
    LI(2, "LI - Licença de Instalação"),
    LO(3, "LO - Licença de Operação"),
    AAF(4, "AAF - Autorização Ambiental de Funcionamento"),
    OUTROS(5, "Outros");
    
    private final int id;
    private final String descricao;

    private EnumTipoMeioAmbiente(int id, String descricao) {
        this.id = id;
        this.descricao = descricao;
    }

    @Override
    public Integer getId() {
        return this.id;
    }

    @Override
    public String getDescription() {
        return this.descricao;
    }

    public static EnumTipoMeioAmbiente getEnumTipoMeioAmbiente(Integer id) {
        for (EnumTipoMeioAmbiente tipoMeioAmbiente : values()) {
            if (tipoMeioAmbiente.id == id) {
                return tipoMeioAmbiente;
            }
        }
        throw new EnumException("EnumTipoMeioAmbiente couldn't load.");
    }    
}
