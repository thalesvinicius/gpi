/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.gpi.server.domain.common;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import javax.annotation.Resource;
import javax.enterprise.context.spi.CreationalContext;
import javax.enterprise.inject.Any;
import javax.enterprise.inject.spi.Bean;
import javax.enterprise.inject.spi.BeanManager;
import javax.enterprise.util.AnnotationLiteral;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.SecurityContext;
import org.hibernate.envers.RevisionListener;

/**
 * Listener criado para obter o usuario
 *
 * @author lucasmfe
 */
public class LogRevisaoListener implements RevisionListener {

    @Override
    public void newRevision(Object entidade) {

        LogRevisao revision = (LogRevisao) entidade;
        String userName = "login";

        try {
            InitialContext ic = new InitialContext();
            BeanManager bm
                    = (BeanManager) ic.lookup("java:comp/BeanManager");
        
            Bean<UserConversation> userBean = (Bean<UserConversation>) bm.resolve(bm.getBeans(UserConversation.class)); 
            UserConversation userConversation = (UserConversation) bm.getReference(userBean, userBean.getBeanClass(), bm.createCreationalContext(userBean));

            if (userConversation != null && userConversation.getUser() != null &&
                    userConversation.getUser().getNome() != null){
                userName = userConversation.getUser().getNome();
            }
            
        } catch (NamingException ex) {
            ex.printStackTrace();
        }

        revision.setResponsavel(userName);

    }

}
