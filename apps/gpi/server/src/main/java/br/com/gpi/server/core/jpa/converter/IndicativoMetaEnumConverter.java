/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.gpi.server.core.jpa.converter;

import br.com.gpi.server.domain.entity.Estagio;
import br.com.gpi.server.domain.entity.enumerated.EnumIndicativoMeta;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = false)
public class IndicativoMetaEnumConverter implements AttributeConverter<EnumIndicativoMeta, Integer> {

    @Override
    public Integer convertToDatabaseColumn(EnumIndicativoMeta indicativoMeta) {
        return indicativoMeta.getId();
    }

    @Override
    public EnumIndicativoMeta convertToEntityAttribute(Integer id) {
        return EnumIndicativoMeta.getIndicativoMetaById(id);
    }

}
