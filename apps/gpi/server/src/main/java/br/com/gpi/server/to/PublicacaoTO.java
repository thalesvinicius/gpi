package br.com.gpi.server.to;

import br.com.gpi.server.domain.entity.Anexo;
import br.com.gpi.server.domain.entity.TipoInstrFormalizacao;
import br.com.gpi.server.domain.entity.User;
import java.util.Date;
import java.util.List;
import java.util.Set;

public class PublicacaoTO {
    
    private Long id;  
    private Long idFormalizacao;
    private String protocolo;
    private TipoInstrFormalizacao tipo;
    private List<String> projetos;
    private Set<String> empresas;
    private Date dataPublicacao;
    private Set<Anexo> anexos;
    private User usuarioResponsavel;
    private Date dataUltimaAlteracao;    
    private String titulo;

    public PublicacaoTO() {
    }

    public PublicacaoTO(Long id, Long idFormalizacao, String protocolo, TipoInstrFormalizacao tipo, String titulo, Date dataPublicacao, User usuarioResponsavel, Date dataUltimaAlteracao) {
        this.id = id;    
        this.idFormalizacao = idFormalizacao;    
        this.protocolo = protocolo;
        this.tipo = tipo;
        this.titulo = titulo;
        this.dataPublicacao = dataPublicacao;
        this.usuarioResponsavel = usuarioResponsavel;
        this.dataUltimaAlteracao = dataUltimaAlteracao;
    }

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the idFormalizacao
     */
    public Long getIdFormalizacao() {
        return idFormalizacao;
    }

    /**
     * @param idFormalizacao the idFormalizacao to set
     */
    public void setIdFormalizacao(Long idFormalizacao) {
        this.idFormalizacao = idFormalizacao;
    }

    /**
     * @return the protocolo
     */
    public String getProtocolo() {
        return protocolo;
    }

    /**
     * @param protocolo the protocolo to set
     */
    public void setProtocolo(String protocolo) {
        this.protocolo = protocolo;
    }

    /**
     * @return the tipo
     */
    public TipoInstrFormalizacao getTipo() {
        return tipo;
    }

    /**
     * @param tipo the tipo to set
     */
    public void setTipo(TipoInstrFormalizacao tipo) {
        this.tipo = tipo;
    }

    /**
     * @return the projetos
     */
    public List<String> getProjetos() {
        return projetos;
    }

    /**
     * @param projetos the projetos to set
     */
    public void setProjetos(List<String> projetos) {
        this.projetos = projetos;
    }
    
    /**
     * @return the empresas
     */
    public Set<String> getEmpresas() {
        return empresas;
    }
    
    /**
     * @param empresas the empresas to set
     */
    public void setEmpresas(Set<String> empresas) {
        this.empresas = empresas;
    }

    /**
     * @return the dataPublicacao
     */
    public Date getDataPublicacao() {
        return dataPublicacao;
    }

    /**
     * @param dataPublicacao the dataPublicacao to set
     */
    public void setDataPublicacao(Date dataPublicacao) {
        this.dataPublicacao = dataPublicacao;
    }

    /**
     * @return the anexos
     */
    public Set<Anexo> getAnexos() {
        return anexos;
    }

    /**
     * @param anexos the anexos to set
     */
    public void setAnexos(Set<Anexo> anexos) {
        this.anexos = anexos;
    }

    /**
     * @return the usuarioResponsavel
     */
    public User getUsuarioResponsavel() {
        return usuarioResponsavel;
    }

    /**
     * @param usuarioResponsavel the usuarioResponsavel to set
     */
    public void setUsuarioResponsavel(User usuarioResponsavel) {
        this.usuarioResponsavel = usuarioResponsavel;
    }

    /**
     * @return the dataUltimaAlteracao
     */
    public Date getDataUltimaAlteracao() {
        return dataUltimaAlteracao;
    }

    /**
     * @param dataUltimaAlteracao the dataUltimaAlteracao to set
     */
    public void setDataUltimaAlteracao(Date dataUltimaAlteracao) {
        this.dataUltimaAlteracao = dataUltimaAlteracao;
    }

    /**
     * @return the titulo
     */
    public String getTitulo() {
        return titulo;
    }

    /**
     * @param titulo the titulo to set
     */
    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    
}