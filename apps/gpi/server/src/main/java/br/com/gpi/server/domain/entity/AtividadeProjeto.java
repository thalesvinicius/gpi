package br.com.gpi.server.domain.entity;

import br.com.gpi.server.domain.common.AuditedEntity;
import br.com.gpi.server.to.LocalTO;
import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.PostLoad;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import org.hibernate.envers.Audited;

@Audited
@Entity
@Table(name = "AtividadeProjeto")
@NamedQueries({
    @NamedQuery(name = AtividadeProjeto.Query.findByProjetoId, query = "SELECT a FROM AtividadeProjeto a WHERE a.projeto.id = ? order by a.posicao")
})
public class AtividadeProjeto extends AuditedEntity<Long> {

    private Integer posicao;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "projeto_id")
    private Projeto projeto;

    @ManyToOne
    @JoinColumn(name = "atividadeLocal_id")
    private AtividadeLocal atividadeLocal;

    private transient LocalTO localAtividade;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "anexo_id", unique = true, nullable = true)
    private Anexo anexo;

    @Temporal(TemporalType.TIMESTAMP)
    private Date dataConclusao;

    private String observacao;

    @Column(name = "obrigatorio", columnDefinition = "bit")
    private Boolean obrigatorio;

    @Column(name = "bloqueiaICE", columnDefinition = "bit")
    private Boolean bloqueiaICE;

    @Column(name = "df", columnDefinition = "bit")
    private Boolean df;

    @Column(name = "ii", columnDefinition = "bit")
    private Boolean ii;

    @Column(name = "oi", columnDefinition = "bit")
    private Boolean oi;

    @Column(name = "cc", columnDefinition = "bit")
    private Boolean cc;

    @Transient
    private transient Boolean concluido;

    public Integer getPosicao() {
        return posicao;
    }

    public void setPosicao(Integer posicao) {
        this.posicao = posicao;
    }

    public Projeto getProjeto() {
        return projeto;
    }

    public void setProjeto(Projeto projeto) {
        this.projeto = projeto;
    }

    public AtividadeLocal getAtividadeLocal() {
        return atividadeLocal;
    }

    public void setAtividadeLocal(AtividadeLocal atividadeLocal) {
        this.atividadeLocal = atividadeLocal;
    }

    public LocalTO getLocalAtividade() {
        return localAtividade;
    }

    public void setLocalAtividade(LocalTO localAtividade) {
        this.localAtividade = localAtividade;
    }

    public Anexo getAnexo() {
        return anexo;
    }

    public void setAnexo(Anexo anexo) {
        this.anexo = anexo;
    }

    public Date getDataConclusao() {
        return dataConclusao;
    }

    public void setDataConclusao(Date dataConclusao) {
        this.dataConclusao = dataConclusao;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public Boolean getObrigatorio() {
        return obrigatorio;
    }

    public void setObrigatorio(Boolean obrigatorio) {
        this.obrigatorio = obrigatorio;
    }

    public Boolean getBloqueiaICE() {
        return bloqueiaICE;
    }

    public void setBloqueiaICE(Boolean bloqueiaICE) {
        this.bloqueiaICE = bloqueiaICE;
    }

    public Boolean getDf() {
        return df;
    }

    public void setDf(Boolean df) {
        this.df = df;
    }

    public Boolean getIi() {
        return ii;
    }

    public void setIi(Boolean ii) {
        this.ii = ii;
    }

    public Boolean getOi() {
        return oi;
    }

    public void setOi(Boolean oi) {
        this.oi = oi;
    }

    public Boolean getCc() {
        return cc;
    }

    public void setCc(Boolean cc) {
        this.cc = cc;
    }

    public Boolean isConcluido() {
        return concluido;
    }

    public void setConcluido(Boolean concluido) {
        this.concluido = concluido;
    }

    @PostLoad
    private void postLoad() {
        this.localAtividade = new LocalTO(atividadeLocal.getLocal().getId(), atividadeLocal.getLocal().getDescricao());
    }

    public static class Query {

        public static final String findByProjetoId = "AtividadeProjeto.findByProjetoId";

        public Query() {
        }

    }

}
