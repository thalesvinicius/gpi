package br.com.gpi.server.to;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.util.ArrayList;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class AdvancedSearchRequest {

    private List<FieldTO> selectedFields;
    private List<ConditionTO> selectedConditions;

    public List<FieldTO> getSelectedFields() {
        if (selectedFields == null) {
            selectedFields = new ArrayList<>();
        }
        return selectedFields;
    }

    public void setSelectedFields(List<FieldTO> selectedFields) {
        this.selectedFields = selectedFields;
    }

    public List<ConditionTO> getSelectedConditions() {
        if (selectedConditions == null) {
            selectedConditions = new ArrayList<>();
        }
        return selectedConditions;
    }

    public void setSelectedConditions(List<ConditionTO> selectedConditions) {
        this.selectedConditions = selectedConditions;
    }
}
