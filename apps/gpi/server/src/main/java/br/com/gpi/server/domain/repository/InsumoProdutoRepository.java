package br.com.gpi.server.domain.repository;

import br.com.gpi.server.domain.entity.InsumoProduto;
import org.apache.deltaspike.data.api.EntityRepository;
import org.apache.deltaspike.data.api.Modifying;
import org.apache.deltaspike.data.api.Query;
import org.apache.deltaspike.data.api.Repository;
import org.apache.deltaspike.data.api.SingleResultType;
import org.apache.deltaspike.data.api.criteria.CriteriaSupport;

@Repository(forEntity = InsumoProduto.class)
public abstract class InsumoProdutoRepository implements CriteriaSupport<InsumoProduto>, EntityRepository<InsumoProduto, Long> {
    @Modifying
    @Query(named = InsumoProduto.Query.findByProjetoId, singleResult = SingleResultType.OPTIONAL)
    public abstract InsumoProduto findByProjetoId(Long id);
}
