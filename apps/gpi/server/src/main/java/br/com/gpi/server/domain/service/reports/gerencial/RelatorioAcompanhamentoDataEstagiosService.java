package br.com.gpi.server.domain.service.reports.gerencial;

import br.com.gpi.server.domain.entity.Estagio;
import br.com.gpi.server.domain.entity.RelatorioAcompanhamentoDataEstagios;
import br.com.gpi.server.domain.entity.RelatorioAcompanhamentoDataEstagios_;
import br.com.gpi.server.domain.entity.SituacaoProjetoEnum;
import br.com.gpi.server.domain.service.reports.BaseReportService;
import br.com.gpi.server.util.EnumMimeTypes;
import br.com.gpi.server.util.functional.FunctionalInterfaces;
import br.com.gpi.server.util.reports.ReportUtil;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.ws.rs.core.Response;
import net.sf.dynamicreports.jasper.builder.JasperReportBuilder;
import net.sf.dynamicreports.report.builder.DynamicReports;
import static net.sf.dynamicreports.report.builder.DynamicReports.col;
import static net.sf.dynamicreports.report.builder.DynamicReports.type;
import static net.sf.dynamicreports.report.builder.DynamicReports.cmp;
import static net.sf.dynamicreports.report.builder.DynamicReports.stl;
import net.sf.dynamicreports.report.builder.column.ColumnBuilder;
import net.sf.dynamicreports.report.builder.component.ComponentBuilder;
import net.sf.dynamicreports.report.constant.HorizontalAlignment;
import net.sf.dynamicreports.report.datasource.DRDataSource;
import net.sf.jasperreports.engine.JRDataSource;

public class RelatorioAcompanhamentoDataEstagiosService extends BaseReportService<RelatorioAcompanhamentoDataEstagios> {

    
    
    @Inject
    public RelatorioAcompanhamentoDataEstagiosService(EntityManager em) {
        super(em);
    }

    public List<RelatorioAcompanhamentoDataEstagios> findProjectsByFilters(List<Estagio> estagios, List<Long> cadeiasProdutivas, List<SituacaoProjetoEnum> situacoesProjeto,
            List<Long> departamentos, List<Long> regioesPlanejamento, List<Long> usuariosResponsaveis, Boolean ultimaVersao) {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<RelatorioAcompanhamentoDataEstagios> query = builder.createQuery(RelatorioAcompanhamentoDataEstagios.class);
        Root<RelatorioAcompanhamentoDataEstagios> fromRelatorioAcompanhamentoDataEstagios = query.from(RelatorioAcompanhamentoDataEstagios.class);
        List<Predicate> conditions = new ArrayList();

        buildFindProjectsByFiltersConditions(fromRelatorioAcompanhamentoDataEstagios, conditions, builder, estagios, cadeiasProdutivas, situacoesProjeto, regioesPlanejamento, departamentos, usuariosResponsaveis, ultimaVersao);
        
        TypedQuery<RelatorioAcompanhamentoDataEstagios> typedQuery = entityManager.createQuery(query
                .where(conditions.toArray(new Predicate[]{}))
                .orderBy(builder.asc(fromRelatorioAcompanhamentoDataEstagios.get(RelatorioAcompanhamentoDataEstagios_.nomeEmpresa)))
                .distinct(true)
        );

        List<RelatorioAcompanhamentoDataEstagios> resultList = typedQuery.getResultList();

        return resultList;
    }
    
    public Response generateReport(String format, List<Estagio> estagios, List<Long> cadeiasProdutivas,
             List<SituacaoProjetoEnum> situacoesProjeto, List<Long> departamentos,
            List<Long> regioesPlanejamento, List<Long> usuariosResponsaveis, Boolean ultimaVersao,Map<String,Object> parametros) 
            throws FileNotFoundException, IOException {
         
            parametros.put("LOGO_INDI",getImage(ReportUtil.LOGO_INDI_NOME));
            parametros.put("TITULO", "Relatório de Acompanhamento de Datas dos Estágios");
           
        
        List<RelatorioAcompanhamentoDataEstagios> result = findProjectsByFilters(estagios, cadeiasProdutivas, situacoesProjeto, departamentos, regioesPlanejamento, usuariosResponsaveis, ultimaVersao);
        JasperReportBuilder report = buildEstagio(format,result,parametros);
        EnumMimeTypes mimetype = EnumMimeTypes.valueOf(format.toUpperCase());
        
        FunctionalInterfaces.BinarySupplier exporter = () -> {
            if(EnumMimeTypes.XLSX.equals(mimetype)){
                return ReportUtil.xlsxExporter.export(Arrays.asList(report.ignorePagination().toJasperPrint(),
                        gerarPrintAvulso(parametros, ReportUtil.CABECALHO_REL_AGENDA_POSITIVA ),
                        DynamicReports.report().addDetail(buildSummary(result, parametros)).title(cmp.text("Total")).toJasperPrint()));
            }else {
                return mimetype.exportReport(report).get();
            }
        };
        
        return buildResponse(exporter,mimetype.mimeType);
    }
    
    
    public JasperReportBuilder buildEstagio(String format, List<RelatorioAcompanhamentoDataEstagios> result, Map<String,Object> parametros) 
            throws FileNotFoundException, IOException {
        
       
        String nomeArquivo = "RelatorioEstagio";
        // Colunas do estagio
        ColumnBuilder[] cb = {col.column("Empresa", "nomeEmpresa", type.stringType()).setWidth(14),
            col.column("Projeto", "nomeProjeto", type.stringType()).setWidth(22),
            col.column("Cidade", "cidade", type.stringType()).setWidth(11),
//            col.column("Região Planejamento", "regiaoPlanejamento", type.stringType()).setWidth(9),
            col.column("Cadeia  Produtiva", "cadeiaProdutiva", type.stringType()).setWidth(10),
            col.column("Status  Atual", "situacaoAtual", type.stringType()).setWidth(7),
            col.column("Estágio   Atual", "estagioAtual", type.stringType()).setWidth(8),
            col.column("Analista", "analista", type.stringType()).setWidth(8),
            col.column("Gerência", "departamento", type.stringType()).setWidth(7),
            col.column("Data Prevista  DF", "dataPrevistaDF", type.stringType()).setWidth(7),
            col.column("Data Prevista     II", "dataPrevistaII", type.stringType()).setWidth(7),
            col.column("Data Prevista    OI", "dataPrevistaOI", type.stringType()).setWidth(7),
            col.column("Data Real IP", "dataRealIP", type.stringType()).setWidth(7),
            col.column("Data Real PP", "dataRealPP", type.stringType()).setWidth(7),
            col.column("Data Real DF", "dataRealDF", type.stringType()).setWidth(7),
            col.column("Data Real   II", "dataRealII", type.stringType()).setWidth(7),
            col.column("Data Real OI", "dataRealOI", type.stringType()).setWidth(7),
            col.column("Data Real CC", "dataRealCC", type.stringType()).setWidth(7)};       
        
        FunctionalInterfaces.JaspeReportBuilderFunction sumary = format.equalsIgnoreCase("XLSX") ? 
                (noop) -> noop :
                (re) -> re.addSummary(buildSummary(result, parametros));
        
        return generatorChain(() -> defaultInit.init(cb,getEstagioDataSource(result))
                , resolveHeader(format,parametros),sumary);
    }
    
     /*
     *   Recupera o datasource do estágio
     */
    private JRDataSource getEstagioDataSource(List<RelatorioAcompanhamentoDataEstagios> result) {
        DRDataSource dataSource = new DRDataSource("nomeEmpresa", "nomeProjeto", "cidade", "regiaoPlanejamento", "cadeiaProdutiva", "situacaoAtual", "estagioAtual", "analista", "departamento", "dataPrevistaDF", "dataPrevistaII", "dataPrevistaOI", "dataRealIP", "dataRealPP", "dataRealDF", "dataRealII", "dataRealOI", "dataRealCC");
        result.stream().forEach((to) -> {
            dataSource.add(getLinhaEstagio(to));
        });
        return dataSource;
    }

    private String[] getLinhaEstagio(RelatorioAcompanhamentoDataEstagios relatorio) {
        String[] linha = {relatorio.getNomeEmpresa(), relatorio.getNomeProjeto(), relatorio.getCidade(),
            relatorio.getRegiaoPlanejamento(), relatorio.getCadeiaProdutiva(), relatorio.getSituacaoAtual().getDescription(),
            relatorio.getEstagioAtual().getDescricao(), relatorio.getAnalista(), relatorio.getDepartamento(), fmtDate(relatorio.getDataPrevistaDF()), fmtDate(relatorio.getDataPrevistaII()),
            fmtDate(relatorio.getDataPrevistaOI()), fmtDate(relatorio.getDataRealIP()), fmtDate(relatorio.getDataRealPP()), fmtDate(relatorio.getDataRealDF()), fmtDate(relatorio.getDataRealII()), fmtDate(relatorio.getDataRealOI()), fmtDate(relatorio.getDataRealCC())};
        return linha;
    }
    
    private void buildFindProjectsByFiltersConditions(Root<RelatorioAcompanhamentoDataEstagios> fromRelatorioAcompanhamentoDataEstagios, List<Predicate> conditions, CriteriaBuilder builder, List<Estagio> estagios, List<Long> cadeiasProdutivas, List<SituacaoProjetoEnum> situacoesProjeto, List<Long> regioesPlanejamento, List<Long> departamentos, List<Long> usuariosResponsaveis, Boolean ultimaVersao) {
        if (estagios != null && estagios.size() > 0) {
            conditions.add(fromRelatorioAcompanhamentoDataEstagios.get(RelatorioAcompanhamentoDataEstagios_.estagioAtual).in(estagios));
        }

        if (cadeiasProdutivas != null && cadeiasProdutivas.size() > 0) {
            conditions.add(fromRelatorioAcompanhamentoDataEstagios.get(RelatorioAcompanhamentoDataEstagios_.cadeiaProdutivaId).in(cadeiasProdutivas));
        }

        if (situacoesProjeto != null && situacoesProjeto.size() > 0) {
            conditions.add(fromRelatorioAcompanhamentoDataEstagios.get(RelatorioAcompanhamentoDataEstagios_.situacaoAtual).in(situacoesProjeto));
        }

        if (regioesPlanejamento != null && regioesPlanejamento.size() > 0) {
            conditions.add(fromRelatorioAcompanhamentoDataEstagios.get(RelatorioAcompanhamentoDataEstagios_.regiaoPlanejamentoId).in(regioesPlanejamento));
        }

        if (departamentos != null && !departamentos.isEmpty()) {
            conditions.add(fromRelatorioAcompanhamentoDataEstagios.get(RelatorioAcompanhamentoDataEstagios_.departamentoId).in(departamentos));
            if (usuariosResponsaveis != null && !usuariosResponsaveis.isEmpty()) {
                conditions.add(fromRelatorioAcompanhamentoDataEstagios.get(RelatorioAcompanhamentoDataEstagios_.usuarioResponsavelId).in(usuariosResponsaveis));
            }
        } else {
            if (usuariosResponsaveis != null && !usuariosResponsaveis.isEmpty()) {
                conditions.add(fromRelatorioAcompanhamentoDataEstagios.get(RelatorioAcompanhamentoDataEstagios_.usuarioResponsavelId).in(usuariosResponsaveis));
            }
        }
        
        if(ultimaVersao != null) {
            conditions.add(builder.equal(fromRelatorioAcompanhamentoDataEstagios.get(RelatorioAcompanhamentoDataEstagios_.ultimaVersao), ultimaVersao));
        } else {
            conditions.add(
                builder.or(// somente projetos cuja a ultima versão é a zero (não possuem versão congelada)
                    builder.and(
                            builder.equal(
                                    fromRelatorioAcompanhamentoDataEstagios.get(RelatorioAcompanhamentoDataEstagios_.ultimaVersao), Boolean.TRUE
                            ), builder.equal(fromRelatorioAcompanhamentoDataEstagios.get(RelatorioAcompanhamentoDataEstagios_.versao), 0)
                    ),// somente projetos que possuem versão congelada pegando a ultima versão congelada deste projeto (condição validada pela view)
                    builder.and(
                            builder.equal(fromRelatorioAcompanhamentoDataEstagios.get(RelatorioAcompanhamentoDataEstagios_.ultimaVersao), Boolean.FALSE)
                    )
                )
            );
        }
    }
    private ComponentBuilder buildSummary(List<RelatorioAcompanhamentoDataEstagios> result, Map<String,Object> parametros){
         DecimalFormat df = new DecimalFormat();
        df.setGroupingUsed(true);df.setMaximumFractionDigits(0);df.setMinimumFractionDigits(0);
        return cmp.horizontalList(cmp.text("Quantidade de Projetos "),
                        cmp.text( df.format(result.stream().collect(Collectors.counting()).longValue())),
                        cmp.text(parametros.get(ReportUtil.INFO_RESPONSAVEL).toString()).setHorizontalAlignment(HorizontalAlignment.RIGHT))
                        .setBaseStyle(stl.style(stl.fontArial().setFontSize(9)));
    }

}
