package br.com.gpi.server.domain.entity;

import br.com.gpi.server.core.exception.EnumException;

public enum RespostaPesquisaEnum {
    
    SIM (1, "Sim"), 
    NAO(2, "Não"), 
    EXCELENTE(3, "Excelente"), 
    BOA(4, "Boa"),
    REGULAR(5, "Regular"),
    RUIM(6, "Ruim");
    
    private final Integer id;
    private final String descricao;

    private RespostaPesquisaEnum(Integer id, String descricao) {
        this.id = id;
        this.descricao = descricao;
    }

    public Integer getId() {
        return id;
    }

    public String getDescricao() {
        return descricao;
    }

    public static RespostaPesquisaEnum RespostaPesquisaEnum(Integer id) {
        for (RespostaPesquisaEnum tipo : values()) {
            if (tipo.getId().equals(id)) {
                return tipo;
            }
        }
        throw new EnumException(String.format("Not recognized value %d for TipoInstrFormalizacao enum", id));
    }        
    
}
