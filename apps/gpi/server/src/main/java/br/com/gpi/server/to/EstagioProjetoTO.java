package br.com.gpi.server.to;

import br.com.gpi.server.domain.entity.Estagio;
import br.com.gpi.server.domain.entity.Projeto;
import java.util.Date;

public class EstagioProjetoTO {
    
    private long projetoId;
    private long estagioId;
    private Projeto projeto;
    private Estagio estagio;
    private Date dataInicioEstagio;
    private Boolean ativo;

    public EstagioProjetoTO() {
    }

    public EstagioProjetoTO(Date dataInicioEstagio) {
        this.dataInicioEstagio = dataInicioEstagio;
    }

    public long getProjetoId() {
        return projetoId;
    }

    public void setProjetoId(long projetoId) {
        this.projetoId = projetoId;
    }

    public long getEstagioId() {
        return estagioId;
    }

    public void setEstagioId(long estagioId) {
        this.estagioId = estagioId;
    }

    public Projeto getProjeto() {
        return projeto;
    }

    public void setProjeto(Projeto projeto) {
        this.projeto = projeto;
    }

    public Estagio getEstagio() {
        return estagio;
    }

    public void setEstagio(Estagio estagio) {
        this.estagio = estagio;
    }

    public Date getDataInicioEstagio() {
        return dataInicioEstagio;
    }

    public void setDataInicioEstagio(Date dataInicioEstagio) {
        this.dataInicioEstagio = dataInicioEstagio;
    }

    public Boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(Boolean ativo) {
        this.ativo = ativo;
    }
    
    
}
