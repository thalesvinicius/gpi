package br.com.gpi.server.domain.entity;

import br.com.gpi.server.domain.common.BaseDomainTableEntity;
import javax.persistence.Cacheable;
import javax.persistence.Entity;
import javax.persistence.Table;
import org.hibernate.envers.Audited;

@Audited
@Entity
@Table(name = "Cargo")
@Cacheable
public class Cargo extends BaseDomainTableEntity {
    
    private String descricao;
    
    public Cargo() {
    }

    public Cargo(Long id) {
        this.setId(id);
    }
    
    @Override
    public String getDescricao() {
        return descricao;
    }

    @Override
    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

}
