package br.com.gpi.server.core.jpa.converter;

import br.com.gpi.server.core.exception.EnumException;
import br.com.gpi.server.domain.entity.UserGroup;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = false)
public class UserGroupEnumConverter implements AttributeConverter<UserGroup, Integer> {

    @Override
    public Integer convertToDatabaseColumn(UserGroup group) {
        return group.getId();
    }

    @Override
    public UserGroup convertToEntityAttribute(Integer id) {
        UserGroup userGroup = null;
        try{
            userGroup =  UserGroup.getUserGroup(id);
        } catch (EnumException e) {
            //TODO implementar log e chamar aqui.
        }
        
        return userGroup;
    }
}
