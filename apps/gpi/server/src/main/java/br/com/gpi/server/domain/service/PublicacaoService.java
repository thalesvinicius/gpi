package br.com.gpi.server.domain.service;

import br.com.gpi.server.domain.common.BaseService;
import br.com.gpi.server.domain.entity.Anexo;
import br.com.gpi.server.domain.entity.CadeiaProdutiva;
import br.com.gpi.server.domain.entity.Empresa;
import br.com.gpi.server.domain.entity.Empresa_;
import br.com.gpi.server.domain.entity.InstrumentoFormalizacao;
import br.com.gpi.server.domain.entity.InstrumentoFormalizacao_;
import br.com.gpi.server.domain.entity.Projeto;
import br.com.gpi.server.domain.entity.Projeto_;
import br.com.gpi.server.domain.entity.Publicacao;
import br.com.gpi.server.domain.entity.Publicacao_;
import br.com.gpi.server.domain.entity.TipoInstrFormalizacao;
import br.com.gpi.server.domain.entity.User;
import br.com.gpi.server.domain.repository.InstrumentoFormalizacaoRepository;
import br.com.gpi.server.domain.repository.PublicacaoRepository;
import br.com.gpi.server.to.PublicacaoTO;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;
import javax.ws.rs.core.Response;

public class PublicacaoService extends BaseService<Publicacao> {

    @Inject
    public PublicacaoService(EntityManager em) {
        super(Publicacao.class, em);
    }
    @Inject
    PublicacaoRepository publicacaoRepository;

    @Inject
    private InstrumentoFormalizacaoRepository formalizacaoRepository;

    @Inject
    private UserTransaction transaction;

    @Inject
    private EmailService emailService;

    public PublicacaoTO getPublicacaoByIdFormalizacao(Long idFormalizacao) {

        CriteriaBuilder builder = getEm().getCriteriaBuilder();
        CriteriaQuery<PublicacaoTO> query = builder.createQuery(PublicacaoTO.class);
        Root<Publicacao> fromPub = query.from(Publicacao.class);
        List<Predicate> conditions = new ArrayList();
        Join<Publicacao, InstrumentoFormalizacao> fromInstr = fromPub.join(Publicacao_.instrumentoFormalizacao);

        if (idFormalizacao != null) {
            conditions.add(builder.equal(fromInstr.get(InstrumentoFormalizacao_.id), idFormalizacao));
        }

        TypedQuery<PublicacaoTO> typedQuery = getEm().createQuery(query
                .multiselect(fromPub.get(Publicacao_.id),
                        fromInstr.get(InstrumentoFormalizacao_.id),
                        fromInstr.get(InstrumentoFormalizacao_.protocolo),
                        fromInstr.get(InstrumentoFormalizacao_.tipo),
                        fromInstr.get(InstrumentoFormalizacao_.titulo),
                        fromPub.get(Publicacao_.dataPublicacao),
                        fromPub.get(Publicacao_.usuarioResponsavel),
                        fromPub.get(Publicacao_.dataUltimaAlteracao))
                .where(conditions.toArray(new Predicate[]{})).distinct(true)
        );
        List<PublicacaoTO> resultList = new ArrayList<>(typedQuery.getResultList());

        PublicacaoTO publicacaoTO;
        if (resultList.isEmpty()) {
            publicacaoTO = new PublicacaoTO();
            publicacaoTO.setIdFormalizacao(idFormalizacao);
            publicacaoTO.setTipo(formalizacaoRepository.recoverTipoById(idFormalizacao));
            publicacaoTO.setProtocolo(formalizacaoRepository.recoverCodigoProtocoloById(idFormalizacao));
        } else {
            publicacaoTO = resultList.get(0);
        }
        publicacaoTO.setEmpresas(getEmpresas(publicacaoTO));
        publicacaoTO.setProjetos(getProjetos(publicacaoTO));
        publicacaoTO.setAnexos(getAnexos(publicacaoTO));
        return publicacaoTO;
    }

    private Set<String> getEmpresas(PublicacaoTO publicacaoTO) {
        CriteriaBuilder builder = getEm().getCriteriaBuilder();
        CriteriaQuery<String> query = builder.createQuery(String.class);
        Root<InstrumentoFormalizacao> fromInstr = query.from(InstrumentoFormalizacao.class);
        List<Predicate> conditions = new ArrayList();
        Join<InstrumentoFormalizacao, Projeto> fromProjeto = fromInstr.join(InstrumentoFormalizacao_.projetos);
        Join<Projeto, Empresa> fromEmpresa = fromProjeto.join(Projeto_.empresa);

        if (publicacaoTO.getIdFormalizacao() != null) {
            conditions.add(builder.equal(fromInstr.get(InstrumentoFormalizacao_.id), publicacaoTO.getIdFormalizacao()));
        }

        TypedQuery<String> typedQuery = getEm().createQuery(query
                .multiselect(fromEmpresa.get(Empresa_.nomePrincipal))
                .where(conditions.toArray(new Predicate[]{})).distinct(true)
        );

        return new HashSet<>(typedQuery.getResultList());
    }

    private List<String> getProjetos(PublicacaoTO publicacaoTO) {
        CriteriaBuilder builder = getEm().getCriteriaBuilder();
        CriteriaQuery<String> query = builder.createQuery(String.class);
        Root<InstrumentoFormalizacao> fromInstr = query.from(InstrumentoFormalizacao.class);
        List<Predicate> conditions = new ArrayList();
        Join<InstrumentoFormalizacao, Projeto> fromProjeto = fromInstr.join(InstrumentoFormalizacao_.projetos);

        if (publicacaoTO.getIdFormalizacao() != null) {
            conditions.add(builder.equal(fromInstr.get(InstrumentoFormalizacao_.id), publicacaoTO.getIdFormalizacao()));
        }

        TypedQuery<String> typedQuery = getEm().createQuery(query
                .multiselect(fromProjeto.get(Projeto_.nome))
                .where(conditions.toArray(new Predicate[]{})).distinct(true)
        );

        return new ArrayList<>(typedQuery.getResultList());
    }

    private Set<Anexo> getAnexos(PublicacaoTO publicacaoTO) {
        List<Anexo> anexos = publicacaoRepository.findAnexosByPublicacao(publicacaoTO.getId());
        return new HashSet<>(anexos);
    }

    public Response saveOrUpdate(PublicacaoTO publicacaoTO, User user, Long idFormalizacao) throws 
        SystemException {
        try {
            transaction.begin();
            this.getEm().joinTransaction();

            Publicacao publicacao = new Publicacao();
            publicacao.setDataPublicacao(publicacaoTO.getDataPublicacao());
            publicacao.setAnexos(publicacaoTO.getAnexos());
            publicacao.setId(publicacaoTO.getId());

            publicacao.setUsuarioResponsavel(user);
            publicacao.setDataUltimaAlteracao(new Date());

            InstrumentoFormalizacao formalizacao = this.getEm().find(InstrumentoFormalizacao.class, idFormalizacao);

            if (publicacaoTO.getProtocolo() != null && idFormalizacao != null && formalizacao.getTipo() == TipoInstrFormalizacao.PROTOCOLO_INTENCAO) {
                Long numProtocolos = (Long) this.createQuery(InstrumentoFormalizacao.Query.countByProtocoloAndInstrumentoNotEquals, publicacaoTO.getProtocolo(), idFormalizacao).getSingleResult();
                if (numProtocolos > 0) {
                    transaction.rollback();
                    return buildAlreadyExistProtocol();
                }
            }

            formalizacao.setProtocolo(publicacaoTO.getProtocolo());
            formalizacao = (InstrumentoFormalizacao) this.merge(formalizacao);
            publicacao.setInstrumentoFormalizacao(formalizacao);

            publicacao = (Publicacao) this.merge(publicacao);

            for (Projeto p : formalizacao.getProjetos()) {
                User responsavel = p.getUsuarioResponsavel();

                if (responsavel != null && responsavel.getEmail() != null) {
                    emailService.sendSuccessfullyProtocolPublicationEmail(responsavel.getEmail(), p.getEmpresa().getNomePrincipal());
                }

                User gerenteResponsavelCadeia = (User) this.createQuery(CadeiaProdutiva.Query.locateResponsavelById, p.getCadeiaProdutiva().getId()).getSingleResult();

                if (gerenteResponsavelCadeia != null && gerenteResponsavelCadeia.getEmail() != null) {
                    if ((responsavel.getEmail() != null && !responsavel.getEmail().equals(gerenteResponsavelCadeia.getEmail())) 
                            || responsavel.getEmail() == null) {
                       emailService.sendSuccessfullyProtocolPublicationEmail(gerenteResponsavelCadeia.getEmail(), p.getEmpresa().getNomePrincipal());                      
                    }
                }

            }

            transaction.commit();
            return Response.ok(publicacao).build();
        } catch (Exception ex) {
            transaction.rollback();
            throw new RuntimeException(ex);
        }
    }

    private Response buildAlreadyExistProtocol() {
        return Response.status(HttpServletResponse.SC_BAD_REQUEST).entity("message.error.MSG_ALREADY_EXIST_PROTOCOL").build();
    }
}
