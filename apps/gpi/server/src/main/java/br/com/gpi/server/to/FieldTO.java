package br.com.gpi.server.to;

import br.com.gpi.server.querybuilder.Category;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class FieldTO {

    private String identifier;
    private String description;
    private Category category;

    public FieldTO() {
    }

    public FieldTO(String identifier, String description, Category category) {
        this.identifier = identifier;
        this.description = description;
        this.category = category;
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    @Override
    public String toString() {
        return this.category + " " + this.description; //To change body of generated methods, choose Tools | Templates.
    }

}
