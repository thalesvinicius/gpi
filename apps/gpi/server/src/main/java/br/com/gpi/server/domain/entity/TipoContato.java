package br.com.gpi.server.domain.entity;

import br.com.gpi.server.core.jpa.typesafe.EnumerationEntityType;

public enum TipoContato implements EnumerationEntityType<Integer> {
    
    EMAIL(1, "Troca de E-mail"), REUNIAO(2, "Reunião"), TELEFONE(3, "Telefonema"), OUTROS(4, "Outros");

    private final int id;
    private final String descricao;

    private TipoContato(int id, String descricao) {
        this.id = id;
        this.descricao = descricao;
    }
    
    @Override
    public Integer getId() {
        return this.id;
    }

    @Override
    public String getDescription() {
        return this.descricao;
    }
    
}
