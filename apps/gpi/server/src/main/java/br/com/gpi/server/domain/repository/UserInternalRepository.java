package br.com.gpi.server.domain.repository;

import br.com.gpi.server.domain.entity.UsuarioInterno;
import java.util.List;
import org.apache.deltaspike.data.api.EntityRepository;
import org.apache.deltaspike.data.api.Query;
import org.apache.deltaspike.data.api.QueryParam;
import org.apache.deltaspike.data.api.Repository;
import org.apache.deltaspike.data.api.criteria.CriteriaSupport;

@Repository(forEntity = UsuarioInterno.class)
public abstract class UserInternalRepository  implements CriteriaSupport<UsuarioInterno>, EntityRepository<UsuarioInterno, Long>  {

    @Query(named = UsuarioInterno.Query.locateByLogin)
    public abstract List<UsuarioInterno> LocateByLogin(@QueryParam("login") String login);
    
    @Query(named = UsuarioInterno.Query.locateByDepartamento)
    public abstract List<UsuarioInterno> LocateByDepartamento(@QueryParam("departamentos") List<Long> departamentos);
   
}
