package br.com.gpi.server.domain.repository;

import br.com.gpi.server.domain.entity.User;
import java.util.List;
import org.apache.deltaspike.data.api.EntityRepository;
import org.apache.deltaspike.data.api.Modifying;
import org.apache.deltaspike.data.api.Query;
import org.apache.deltaspike.data.api.Repository;
import org.apache.deltaspike.data.api.criteria.CriteriaSupport;

@Repository(forEntity = User.class)
public abstract class UserRepository implements CriteriaSupport<User>, EntityRepository<User, Long> {

    @Query(value = "DELETE FROM User u WHERE u.id IN ?1")
    @Modifying
    public abstract void deleteById(List<Long> ids);
}
