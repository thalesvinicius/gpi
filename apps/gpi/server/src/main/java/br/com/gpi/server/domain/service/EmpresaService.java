package br.com.gpi.server.domain.service;

import br.com.gpi.server.domain.common.BaseService;
import br.com.gpi.server.domain.common.QueryParameter;
import br.com.gpi.server.domain.entity.CNAEEmpresa;
import br.com.gpi.server.domain.entity.CadeiaProdutiva;
import br.com.gpi.server.domain.entity.CadeiaProdutiva_;
import br.com.gpi.server.domain.entity.Contato;
import br.com.gpi.server.domain.entity.Empresa;
import br.com.gpi.server.domain.entity.Empresa_;
import br.com.gpi.server.domain.entity.Endereco;
import br.com.gpi.server.domain.entity.Endereco_;
import br.com.gpi.server.domain.entity.GrupoUsuario;
import br.com.gpi.server.domain.entity.Municipio;
import br.com.gpi.server.domain.entity.Municipio_;
import br.com.gpi.server.domain.entity.NomeEmpresa;
import br.com.gpi.server.domain.entity.NomeEmpresa_;
import br.com.gpi.server.domain.entity.OrigemEmpresaEnum;
import br.com.gpi.server.domain.entity.Pais;
import br.com.gpi.server.domain.entity.Pais_;
import br.com.gpi.server.domain.entity.SituacaoEmpresa;
import br.com.gpi.server.domain.entity.TipoContato;
import br.com.gpi.server.domain.entity.TipoUsuario;
import br.com.gpi.server.domain.entity.UF;
import br.com.gpi.server.domain.entity.UF_;
import br.com.gpi.server.domain.entity.UnidadeEmpresa;
import br.com.gpi.server.domain.entity.UnidadeEmpresa_;
import br.com.gpi.server.domain.entity.User;
import br.com.gpi.server.domain.entity.UsuarioExterno;
import br.com.gpi.server.domain.repository.CNAEEmpresaRepository;
import br.com.gpi.server.domain.repository.CNAERepository;
import br.com.gpi.server.domain.repository.CadeiaProdutivaRepository;
import br.com.gpi.server.domain.repository.EmpresaRepository;
import br.com.gpi.server.domain.repository.UserExternalRepository;
import br.com.gpi.server.domain.repository.UserRepository;
import br.com.gpi.server.domain.user.model.SecurityUser;
import br.com.gpi.server.exception.EmpresaAlreadyExistsException;
import br.com.gpi.server.to.EmpresaDTO;
import br.com.gpi.server.to.EmpresaUnidadeDTO;
import br.com.gpi.server.to.SearchEmpresaUnidadeTO;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;
import org.apache.commons.lang3.StringUtils;

public class EmpresaService extends BaseService<Empresa> {

    @Inject
    private EmpresaRepository empresaRepository;

    @Inject
    private UserRepository userRepository;

    @Inject
    private UserExternalRepository userExternalRepository;

    @Inject
    private CNAERepository cnaeRepository;

    @Inject
    private CNAEEmpresaRepository cnaeEmpresaRepository;

    @Inject
    private CNAEService cnaeService;

    @Inject
    private EmailService emailService;

    @Inject
    private CadeiaProdutivaRepository cadeiaProdutivaRepository;
    @Inject
    private VerificationTokenService verificationTokenService;

    @Inject
    private UserTransaction transaction;

    @Inject
    private UserService userService;

    @Inject
    private UnidadeEmpresaService unidadeEmpresaService;

    @Inject
    private Logger logger;

    @Inject
    public EmpresaService(EntityManager em) {
        super(Empresa.class, em);
    }

    public Long saveEmpresa(Empresa empresa, Long idUsuario) throws SystemException {
        try {
            transaction.begin();
            this.getEm().joinTransaction();

            preSaveEmpresa(empresa, idUsuario);

            boolean sendTokenAfter = false;

            if (empresa.getResponsavelPersistidoId() == null && empresa.getResponsavel() != null && empresa.getResponsavel().getEmail() != null) {
                sendTokenAfter = true;
            } else if ((empresa.getResponsavel() != null && empresa.getResponsavelPersistidoId() != null)
                    && (!empresa.getResponsavel().getId().equals(empresa.getResponsavelPersistidoId()))) {
                UsuarioExterno usuarioPersistido = this.getEm().find(UsuarioExterno.class, empresa.getResponsavelPersistidoId());
                usuarioPersistido.setRole(new GrupoUsuario(GrupoUsuario.USUARIO_EXTERNO_OUTROS, null));
                this.merge(usuarioPersistido);

                empresa.getResponsavel().setRole(new GrupoUsuario(GrupoUsuario.USUARIO_EXTERNO_RESPONSAVEL, null));

                sendTokenAfter = true;
            }
            List<CNAEEmpresa> cnaes = new ArrayList<>();
            boolean saveCnaesAfter = false;

            if (empresa.getId() == null) {
                saveCnaesAfter = true;
                cnaes.addAll(empresa.getDivisoesCnae());
                cnaes.addAll(empresa.getGruposCnae());
                cnaes.addAll(empresa.getClassesCnae());
                cnaes.addAll(empresa.getSubclassesCnae());

                empresa.setDivisoesCnae(null);
                empresa.setGruposCnae(null);
                empresa.setClassesCnae(null);
                empresa.setSubclassesCnae(null);
            }

            if (empresa.getSituacaoOld() == SituacaoEmpresa.CADASTRO_ARQUIVADO) {
                empresa.setSituacao(SituacaoEmpresa.PEDENTE_VALIDACAO);
                empresa.setJustificativaArquivamento(null);
            }   
            
            empresa = (Empresa) this.merge(empresa);

            if (saveCnaesAfter) {
                for (CNAEEmpresa cnae : cnaes) {
                    cnae.setEmpresa(empresa);
                    cnae.setEmpresaId(empresa.getId());
                    this.merge(cnae);
                }
            }

            if (empresa.getSituacao() == SituacaoEmpresa.CADASTRO_ARQUIVADO) {
                this.createQuery(UsuarioExterno.Query.updateSituacaoByEmpresa, Boolean.FALSE, idUsuario).executeUpdate();
            }
            if (sendTokenAfter && empresa.getResponsavel().getEmail() != null) {
                verificationTokenService.sendEmailVerificationToken(empresa.getResponsavel());
            }
            transaction.commit();
            return empresa.getId();
        } catch (Exception ex) {
            transaction.rollback();
            logger.log(Level.SEVERE, null, ex);
            throw new RuntimeException(ex);
        }
    }

    private void preSaveEmpresa(Empresa empresa, Long idUsuario) {
        User user = null;
        if (idUsuario != null) {
            user = userRepository.findBy(idUsuario);
        }
        empresa.setUsuarioResponsavel(user);

        empresa.setDataUltimaAlteracao(new Date());

        setBackReference(empresa.getUnidades(), u -> u.setEmpresa(empresa));
        setBackReference(empresa.getContatos(), u -> u.setEmpresa(empresa));
        setBackReference(empresa.getNomesEmpresa(), u -> u.setEmpresa(empresa));
        setBackReference(empresa.getComposicaoSocietarias(), u -> u.setEmpresa(empresa));

        setBackReference(empresa.getDivisoesCnae(), u -> {
            u.setEmpresa(empresa);
            u.setCnae(cnaeRepository.findBy(u.getCnaeId()));
        });

        setBackReference(empresa.getGruposCnae(), u -> {
            u.setEmpresa(empresa);
            u.setCnae(cnaeRepository.findBy(u.getCnaeId()));
        });
        setBackReference(empresa.getClassesCnae(), u -> {
            u.setEmpresa(empresa);
            u.setCnae(cnaeRepository.findBy(u.getCnaeId()));
        });
        setBackReference(empresa.getSubclassesCnae(), u -> {
            u.setEmpresa(empresa);
            u.setCnae(cnaeRepository.findBy(u.getCnaeId()));
        });

        prepareEndereco(empresa);

        if (empresa.getCadeiaProdutiva() != null && empresa.getCadeiaProdutiva().getId() == null) {
            empresa.setCadeiaProdutiva(null);
        }

        if (empresa.getLogo() != null && empresa.getLogo().getExtencao() == null) {
            empresa.setLogo(null);
        }

        if (empresa.getNaturezaJuridica() != null && empresa.getNaturezaJuridica().getId() == null) {
            empresa.setNaturezaJuridica(null);
        }
    }

    private void prepareEndereco(Empresa empresa) {
        if (empresa.getEndereco().getMunicipio() != null && empresa.getEndereco().getMunicipio().getId() == null) {
            empresa.getEndereco().setMunicipio(null);
        }

        if (empresa.getEndereco().getUf() != null && empresa.getEndereco().getUf().getId() == null) {
            empresa.getEndereco().setUf(null);
        }

        if (empresa.getEndereco().getPais() != null && empresa.getEndereco().getPais().getId() == null) {
            empresa.getEndereco().setPais(null);
        }
    }

    private Predicate[] createFiltersCondition(CriteriaBuilder builder, Root<Empresa> fromEmpresa, QueryParameter filters) {
        Join<Empresa, UnidadeEmpresa> fromUnidade = fromEmpresa.join(Empresa_.unidades, JoinType.LEFT);
        Join<Empresa, NomeEmpresa> fromNomesEmpresa = fromEmpresa.join(Empresa_.nomesEmpresa, JoinType.LEFT);
        Join<Empresa, Endereco> fromEndereco = fromEmpresa.join(Empresa_.endereco, JoinType.LEFT);
        Join<Endereco, Municipio> fromMunicipio = fromEndereco.join(Endereco_.municipio, JoinType.LEFT);
        Join<Endereco, UF> fromEstado = fromEndereco.join(Endereco_.uf, JoinType.LEFT);
        Join<Endereco, Pais> fromPais = fromEndereco.join(Endereco_.pais, JoinType.LEFT);
        List<Predicate> conditions = new ArrayList();

        if (filters.exists("nomeEmpresa")) {
            String nomeEmpresa = filters.getString("nomeEmpresa");
            conditions.add(builder.or(
                    builder.like(builder.lower(fromUnidade.get(UnidadeEmpresa_.nome)), "%" + nomeEmpresa + "%"),
                    builder.like(builder.lower(fromEmpresa.get(Empresa_.nomePrincipal)), "%" + nomeEmpresa + "%"),
                    builder.like(builder.lower(fromNomesEmpresa.get(NomeEmpresa_.nome)), "%" + nomeEmpresa + "%"),
                    builder.like(builder.lower(fromEmpresa.get(Empresa_.razaoSocial)), "%" + nomeEmpresa + "%")
            ));
        }

        if (filters.exists("cnpj")) {
            conditions.add(builder.like(fromEmpresa.get(Empresa_.cnpj), "%" + filters.getString("cnpj") + "%"));
        }

        if (filters.exists("cadeiaProdutiva")) {
            Join<Empresa, CadeiaProdutiva> joinCadeiaProdutiva = fromEmpresa.join(Empresa_.cadeiaProdutiva, JoinType.INNER);
            conditions.add(builder.equal(joinCadeiaProdutiva.get(CadeiaProdutiva_.id), filters.get("cadeiaProdutiva")));
        }

        if (filters.exists("situacaoEmpresa")) {
            conditions.add(builder.equal(fromEmpresa.get(Empresa_.situacao), filters.get("situacaoEmpresa")));
        }

        if (filters.exists("municipioOuCidade")) {

            conditions.add(builder.or(
                    builder.like(builder.lower(fromEndereco.get(Endereco_.cidade)), "%" + filters.get("municipioOuCidade") + "%"),
                    builder.like(builder.lower(fromMunicipio.get(Municipio_.nome)), "%" + filters.get("municipioOuCidade") + "%")
            )
            );
        }

        if (filters.exists("estado")) {
            conditions.add(builder.equal(fromEstado.get(UF_.nome), filters.get("estado")));
        }

        if (filters.exists("pais")) {
            conditions.add(builder.equal(fromPais.get(Pais_.nome), filters.get("pais")));
        }

        return conditions.toArray(new Predicate[]{});

    }

    public Long countFindByFilters(QueryParameter queryParameters) {
        CriteriaBuilder builder = getEm().getCriteriaBuilder();
        CriteriaQuery<Long> initCq = builder.createQuery(Long.class);
        Root<Empresa> fromEmpresa = initCq.from(Empresa.class);
        CriteriaQuery<Long> query = initCq.select(builder.countDistinct(fromEmpresa)).where(createFiltersCondition(builder, fromEmpresa, queryParameters));
        return getEm().createQuery(query).getSingleResult();
    }

    public List<SearchEmpresaUnidadeTO> findByFilters(QueryParameter queryParams) {
        CriteriaBuilder builder = getEm().getCriteriaBuilder();
        CriteriaQuery<SearchEmpresaUnidadeTO> query = builder.createQuery(SearchEmpresaUnidadeTO.class);
        Root<Empresa> fromEmpresa = query.from(Empresa.class);
        Join<Empresa, Endereco> fromEndereco = fromEmpresa.join(Empresa_.endereco, JoinType.LEFT);
        Predicate[] conditions = createFiltersCondition(builder, fromEmpresa, queryParams);

        TypedQuery<SearchEmpresaUnidadeTO> typedQuery = getEm().createQuery(
                query.multiselect(fromEmpresa.get(Empresa_.id),
                        fromEmpresa.get(Empresa_.nomePrincipal),
                        fromEndereco,
                        fromEmpresa.get(Empresa_.cnpj),
                        fromEmpresa.get(Empresa_.situacao)
                ).where(conditions)
                .orderBy(builder.asc(fromEmpresa.get(Empresa_.id)))
                .distinct(true)
        );

        if (queryParams.hasPagination()) {
            typedQuery.setMaxResults(queryParams.getPagination().getPaginationSize());
            typedQuery.setFirstResult(queryParams.getPagination().getFirstResult());
        }

        List<SearchEmpresaUnidadeTO> empresasEncontradas = typedQuery.getResultList();
        for (SearchEmpresaUnidadeTO empresaEncontrada : empresasEncontradas) {
            empresaEncontrada.setUnidades(unidadeEmpresaService.findWithEnderecoByEmpresaId(empresaEncontrada.getId()));
        }

        return empresasEncontradas;
    }

    public Empresa findByIdForEdition(Long id) {
        StringBuilder builder = new StringBuilder("SELECT DISTINCT e FROM Empresa e ");
        builder.append("\n left join fetch e.logo l");
        builder.append("\n left join fetch e.unidades u");
        builder.append("\n left join fetch e.nomesEmpresa nm");
        builder.append("\n left join fetch e.divisoesCnae dc");
        builder.append("\n left join fetch e.gruposCnae gc");
        builder.append("\n left join fetch e.classesCnae cc");
        builder.append("\n left join fetch e.subclassesCnae sc");
        builder.append("\n left join fetch e.composicaoSocietarias cs");
        builder.append("\n WHERE e.id = ?1");

        TypedQuery<Empresa> query = getEm().createQuery(builder.toString(), Empresa.class);

        query.setParameter(1, id);
        Empresa empresa = query.getSingleResult();
        empresa.loadForEdition();
        return empresa;

    }

    public List<EmpresaUnidadeDTO> findEmpresaUnidadeDTOAtivas(Long idEmpresa, String searchName) {
        if (searchName == null || searchName.length() < 3) {
            return Collections.EMPTY_LIST;
        }

        CriteriaBuilder builder = getEm().getCriteriaBuilder();
        CriteriaQuery<EmpresaUnidadeDTO> query = builder.createQuery(EmpresaUnidadeDTO.class);
        Root<Empresa> fromEmpresa = query.from(Empresa.class);

        Join<Empresa, NomeEmpresa> fromNomesEmpresa = fromEmpresa.join(Empresa_.nomesEmpresa, JoinType.LEFT);
        Join<Empresa, UnidadeEmpresa> fromUnidade = fromEmpresa.join(Empresa_.unidades, JoinType.LEFT);
        Join<Empresa, Endereco> fromEnderecoEmpresa = fromEmpresa.join(Empresa_.endereco, JoinType.LEFT);
        Join<UnidadeEmpresa, Endereco> fromEnderecoUnidade = fromUnidade.join(UnidadeEmpresa_.endereco, JoinType.LEFT);

        List<Predicate> conditions = new ArrayList();

        conditions.add(builder.or(
                builder.like(builder.lower(fromEmpresa.get(Empresa_.razaoSocial)), "%" + searchName + "%"),
                builder.like(builder.lower(fromEmpresa.get(Empresa_.nomePrincipal)), "%" + searchName + "%"),
                builder.like(builder.lower(fromNomesEmpresa.get(NomeEmpresa_.nome)), "%" + searchName + "%")));

        conditions.add(builder.equal(fromEmpresa.get(Empresa_.situacao), SituacaoEmpresa.ATIVA));

        if (idEmpresa != null) {
            conditions.add(builder.equal(fromEmpresa.get(Empresa_.id), idEmpresa));
        }

        TypedQuery<EmpresaUnidadeDTO> typedQuery = getEm().createQuery(query.multiselect(
                fromEmpresa.get(Empresa_.id),
                fromUnidade.get(UnidadeEmpresa_.id),
                fromEmpresa.get(Empresa_.nomePrincipal),
                fromUnidade.get(UnidadeEmpresa_.nome),
                fromEnderecoEmpresa,
                fromEnderecoUnidade)
                .distinct(true)
                .where(conditions.toArray(new Predicate[]{}))
                .orderBy(builder.asc(fromEmpresa.get(Empresa_.id)))
        );
        List<EmpresaUnidadeDTO> resultList = typedQuery.getResultList();
        Map<EmpresaDTO, List<EmpresaUnidadeDTO>> groupByEmpresa = resultList.stream().collect(Collectors.groupingBy(EmpresaUnidadeDTO::mapEmpresaDTO));
        return groupByEmpresa.entrySet()
                .stream()
                .map(dto -> {
                    LinkedList<EmpresaUnidadeDTO> list = new LinkedList(dto.getValue());
                    EmpresaUnidadeDTO empresaUnidadeDTO = new EmpresaUnidadeDTO(dto.getKey());
                    if (!list.contains(empresaUnidadeDTO)) {
                        list.addFirst(empresaUnidadeDTO);
                    }

                    return list;
                }).flatMap(list -> list.stream())
                .collect(Collectors.toList());

    }

    private <T> void setBackReference(Collection<T> collection, Consumer<T> action) {
        if (collection != null) {
            collection.forEach(action);
        }
    }

    public SituacaoEmpresa getSituacaoByCNPJ(String cnpj) {
        StringBuilder builder = new StringBuilder("SELECT e.situacao FROM Empresa e ");
        builder.append("\n WHERE e.cnpj= :cnpj");
        TypedQuery<SituacaoEmpresa> query = getEm().createQuery(builder.toString(), SituacaoEmpresa.class);
        query.setParameter("cnpj", cnpj);
        List<SituacaoEmpresa> situacaoEmpresas = query.getResultList();

        if (situacaoEmpresas != null && !situacaoEmpresas.isEmpty()) {
            return query.getResultList().get(0);
        }
        return null;
    }

    public Long savePrimeiroContato(Empresa empresa, SecurityUser loggedUser) throws SystemException {
        try {
            transaction.begin();
            this.getEm().joinTransaction();

            UsuarioExterno responsavel = empresa.getResponsavel();
            SituacaoEmpresa situacaoEmpresa = null;

            if (StringUtils.isNotEmpty(empresa.getAssuntoFaleConosco()) && StringUtils.isNotEmpty(empresa.getDescricaoFaleConosco())) {
                Contato contato = new Contato();
                contato.setTipoContato(TipoContato.OUTROS);
                contato.setAssunto(empresa.getAssuntoFaleConosco());
                contato.setDescricao(empresa.getDescricaoFaleConosco());
                contato.setDataEfetivado(new Date());
                contato.setEmpresa(empresa);

                empresa.setContatos(new HashSet());
                empresa.getContatos().add(contato);
            }

            if (empresa.getCnpj() != null && !empresa.getCnpj().isEmpty()) {
                situacaoEmpresa = getSituacaoByCNPJ(empresa.getCnpj());
                if (situacaoEmpresa != null && (situacaoEmpresa.equals(SituacaoEmpresa.ATIVA) || situacaoEmpresa.equals(SituacaoEmpresa.PEDENTE_APROVACAO))) {
                    throw new EmpresaAlreadyExistsException();
                } else if (SituacaoEmpresa.CADASTRO_ARQUIVADO.equals(situacaoEmpresa)) {
                    this.createQuery(Empresa.Query.updateSituacaoEmpresa, SituacaoEmpresa.PEDENTE_VALIDACAO, empresa.getId());
                }
            }

            User existentUser = userService.findByLogin(responsavel.getEmail(), TipoUsuario.EXTERNO);
            if (situacaoEmpresa != null && situacaoEmpresa.equals(SituacaoEmpresa.CADASTRO_ARQUIVADO)) {
                //TODO Mudar Usuário Ativo para inativo
            } else {
                if (existentUser != null) {
                    throw new ExternalUserDuplicatedException();
                }
            }
            
            preSaveEmpresa(empresa, null);

            empresa.setOrigemEmpresa(decodeOrigem(empresa));
            empresa.setResponsavel(null);
            empresa.setEmpresaNovaMG(Boolean.FALSE);
            empresa = (Empresa) this.merge(empresa);

            responsavel.setEmpresaId(empresa.getId());
            responsavel.setRole(new GrupoUsuario(GrupoUsuario.USUARIO_EXTERNO_RESPONSAVEL, null));
            responsavel.setPassword("-----");
            responsavel.setAtivo(false);
            userService.createExternalUserTransactional((UsuarioExterno) responsavel, loggedUser);
            User locateResponsavel = (User) this.createQuery(CadeiaProdutiva.Query.locateResponsavelById, empresa.getCadeiaProdutiva().getId()).getSingleResult();

            if (locateResponsavel != null && locateResponsavel.getEmail() != null) {
                emailService.sendContactRequest(locateResponsavel.getEmail(), empresa.getNomePrincipal(), empresa.getId());
//                emailService.sendSuccessfullyFirstContactToManagerEmail(locateResponsavel.getEmail());
            }

            if (responsavel.getEmail() != null) {
                emailService.sendSuccessfullyFirstContactEmail(responsavel.getEmail());
            }

            transaction.commit();
            return empresa.getId();
        } catch (Exception ex) {
            transaction.rollback();
            throw new RuntimeException(ex);
        }
    }
    
    private OrigemEmpresaEnum decodeOrigem(Empresa emp){
        return emp.getPaisOrigem() != null && emp.getPaisOrigem().getId() != 2L ? OrigemEmpresaEnum.INTERNACIONAL :
                (emp.getEstadoOrigem() != null && emp.getEstadoOrigem().getId() != 35  ? OrigemEmpresaEnum.OUTRO_ESTADO:
                OrigemEmpresaEnum.MINEIRA);
    }

    public Boolean validarCadastro(Long empresaId) throws SystemException {
        try {
            transaction.begin();
            this.getEm().joinTransaction();

            this.createQuery(Empresa.Query.updateSituacaoEmpresa, SituacaoEmpresa.ATIVA, empresaId).executeUpdate();
            TypedQuery<UsuarioExterno> query = getEm().createNamedQuery(UsuarioExterno.Query.locateByEmpresa, UsuarioExterno.class);
            query.setParameter(1, empresaId);
            List<UsuarioExterno> usuarioResponsavelList = query.getResultList();

            for (UsuarioExterno u : usuarioResponsavelList) {
                if (u.getEmail() != null) {
                    emailService.sendValidateCompanyEmail(u.getEmail());
                    if (u.isAtivo() != null && !u.isAtivo()) {
                        verificationTokenService.sendEmailVerificationToken(u);
                    }                    
                }
            }
            transaction.commit();
        } catch (Exception error) {
            transaction.rollback();
            logger.log(Level.SEVERE, String.format("Erro ao validar cadastro da empresa id %s", empresaId), error);
            throw new RuntimeException(error);
        }

        return true;
    }

    public Object findByFilters(Boolean countBoolean, QueryParameter queryParam) {
        if (countBoolean != null && countBoolean) {
            return countFindByFilters(queryParam);
        } else {
            return findByFilters(queryParam);
        }
    }

    public String getNomeById(Long empresaId) {
        StringBuilder builder = new StringBuilder("SELECT e.nomePrincipal FROM Empresa e ");
        builder.append("\n WHERE e.id = :empresaId");
        TypedQuery<String> query = getEm().createQuery(builder.toString(), String.class);
        query.setParameter("empresaId", empresaId);
        List<String> nomes = query.getResultList();

        if (nomes != null && !nomes.isEmpty()) {
            return nomes.get(0);
        }
        return null;
    }

    public void removeEmpresa(List<Long> ids) throws SystemException {
        try {
            transaction.begin();
            this.getEm().joinTransaction();

            List<Empresa> empresasToDelete = this.findByNamedQuery(Empresa.Query.findAllByIds, ids);
            empresasToDelete.forEach(u -> {
                List<UsuarioExterno> usuarios = this.createQuery(UsuarioExterno.Query.locateByEmpresa, u.getId()).getResultList();
                for (UnidadeEmpresa und : u.getUnidades()) {
                    usuarios.addAll(this.createQuery(UsuarioExterno.Query.locateByUnidade, und.getId()).getResultList());
                }
                for (User usuario : usuarios) {
                    this.remove(usuario);
                }
                this.remove(u);
            });

            transaction.commit();
        } catch (Exception ex) {
            transaction.rollback();
            logger.log(Level.SEVERE, null, ex);
            throw new RuntimeException(ex);
        }
    }
}