package br.com.gpi.server.domain.service;

import br.com.gpi.server.domain.common.BaseEntity;
import br.com.gpi.server.domain.common.BaseService;
import br.com.gpi.server.domain.entity.CadeiaProdutiva;
import br.com.gpi.server.domain.entity.Empresa;
import br.com.gpi.server.domain.entity.GrupoUsuario;
import br.com.gpi.server.domain.entity.InstrumentoFormalizacao;
import br.com.gpi.server.domain.entity.Projeto;
import br.com.gpi.server.domain.entity.RelatorioAcompanhamento;
import br.com.gpi.server.domain.entity.SituacaoEmpresa;
import br.com.gpi.server.domain.entity.SituacaoProjetoEnum;
import br.com.gpi.server.domain.entity.TipoUsuario;
import br.com.gpi.server.domain.entity.User;
import br.com.gpi.server.domain.entity.UsuarioInterno;
import br.com.gpi.server.helper.AlertaHelper;
import br.com.gpi.server.to.AlertaTO;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.inject.Inject;
import javax.persistence.EntityManager;

public class AlertaService extends BaseService<BaseEntity> {

    @Inject
    public AlertaService(EntityManager em) {
        super(BaseEntity.class, em);
    }

    public List<AlertaTO> findAllAlertasByLoggedUser(User user) {
        List<AlertaTO> alertas = new ArrayList<>();
        findEmpresasAguardandoValidacaoAlertas(alertas, user);
        findProjetosPendentesAceiteAlertas(alertas, user);
        findProjetosPendentesValidacaoAlertas(alertas, user);
        findProjetosInicioProjetoMais120DiasAlertas(alertas, user);
        findProjetosInicioImplantacaoVencidosAlertas(alertas, user);
        findProjetosInicioOperacaoVencidoAlertas(alertas, user);
        findInstrumentosEmNegociacaoVencidosAlertas(alertas, user);
        findAcompanhamentoAnualPendentePreenchimento(alertas, user);
        return alertas;
    }

    public void findEmpresasAguardandoValidacaoAlertas(List<AlertaTO> alertas, User user) {
        if (Objects.equals(user.getTipo(), TipoUsuario.INTERNO.getId()) && GrupoUsuario.GERENTE.equals(user.getRole().getId())) {
            AlertaTO alerta = new AlertaTO();
            alerta.setTitulo(AlertaHelper.AlertaTypes.EMPRESA_AGUARDANDO_VALIDACAO.getTitulo());
            UsuarioInterno gerente = (UsuarioInterno) user;
            List<CadeiaProdutiva> cadeiasGerente = this.createQuery(CadeiaProdutiva.Query.locateByDepartamento, gerente.getDepartamento().getId()).getResultList();
            List<Object[]> descricoes = this.createQuery(Empresa.Query.findNomeByCadeiaProdutivaGerencia, cadeiasGerente, SituacaoEmpresa.PEDENTE_VALIDACAO).getResultList();
            
            if(descricoes.isEmpty()) {
                return;
            }
            
            alerta.setAlertas(descricoes, AlertaHelper.AlertaTypes.EMPRESA_AGUARDANDO_VALIDACAO.getLink());
            alertas.addAll(AlertaHelper.buildAlertasByTitle(alerta, AlertaHelper.AlertaTypes.EMPRESA_AGUARDANDO_VALIDACAO));
        }
    }

    public void findProjetosPendentesAceiteAlertas(List<AlertaTO> alertas, User user) {
        findGenericAlerts(false, alertas, user, TipoUsuario.INTERNO, AlertaHelper.AlertaTypes.PROJETO_PENDENTE_ACEITE, Projeto.Query.findBySituacaoEUsuarioInvestimento, SituacaoProjetoEnum.PENDENTE_ACEITE, user.getId());
    }

    public void findProjetosPendentesValidacaoAlertas(List<AlertaTO> alertas, User user) {
        findGenericAlerts(false, alertas, user, TipoUsuario.INTERNO, AlertaHelper.AlertaTypes.PROJETO_PENDENTE_VALIDACAO, Projeto.Query.findBySituacaoEUsuarioInvestimento, SituacaoProjetoEnum.PEDENTE_VALIDACAO, user.getId());
    }

    public void findProjetosInicioProjetoMais120DiasAlertas(List<AlertaTO> alertas, User user) {
        findGenericAlerts(false, alertas, user, TipoUsuario.INTERNO, AlertaHelper.AlertaTypes.PROJETO_ESTAGIO_INICIAL_MAIOR_120_DIAS, Projeto.Query.findByUsuarioInvestimentoProjEmEstagioInicioProjetoMais120Dias, user.getId());
    }

    public void findProjetosInicioImplantacaoVencidosAlertas(List<AlertaTO> alertas, User user) {
        findGenericAlerts(false, alertas, user, TipoUsuario.INTERNO, AlertaHelper.AlertaTypes.PROJETO_INICIO_IMPLANTACAO_VENCIDO, Projeto.Query.findByUsuarioInvestimentoProjInicioImplantacaoVencido, user.getId());
        findGenericAlerts(false, alertas, user, TipoUsuario.INTERNO, AlertaHelper.AlertaTypes.PROJETO_INICIO_IMPLANTACAO_VENCIDO, Projeto.Query.findByUsuarioInvestimentoProjInicioImplantacaoVencidoSucroEnergetico, user.getId());
    }
    
    public void findProjetosInicioOperacaoVencidoAlertas(List<AlertaTO> alertas, User user) {
        findGenericAlerts(false, alertas, user, TipoUsuario.INTERNO, AlertaHelper.AlertaTypes.PROJETO_INICIO_OPERACAO_VENCIDO, Projeto.Query.findByUsuarioInvestimentoProjInicioOperacaoVencido, user.getId());
        findGenericAlerts(false, alertas, user, TipoUsuario.INTERNO, AlertaHelper.AlertaTypes.PROJETO_INICIO_OPERACAO_VENCIDO, Projeto.Query.findByUsuarioInvestimentoProjInicioOperacaoVencidoSucroEnergetico, user.getId());
    }
    
    public void findInstrumentosEmNegociacaoVencidosAlertas(List<AlertaTO> alertas, User user) {
        findGenericAlerts(false, alertas, user, TipoUsuario.INTERNO, AlertaHelper.AlertaTypes.INSTRUMENTO_EM_NEGOCIACAO_VENCIDO, InstrumentoFormalizacao.Query.findByResponsavelProjetoProtocoloAssinaturaPrevistaVencida, user.getId());
    }

    public void findAcompanhamentoAnualPendentePreenchimento(List<AlertaTO> alertas, User user) {
        findGenericAlerts(false, alertas, user, TipoUsuario.INTERNO, AlertaHelper.AlertaTypes.RELATORIO_ANUAL_PREENCHIMENTO_PENDENTE, RelatorioAcompanhamento.Query.findPendenteValidacaoByResponsavelInvestimento, user);        
    }
    
    private void findGenericAlerts(boolean replaceByDescription, List<AlertaTO> alertas, User user, TipoUsuario tipoUsuario, AlertaHelper.AlertaTypes tipoAlerta, String query, Object... params) {
        if (Objects.equals(user.getTipo(), tipoUsuario.getId())) {
            List<Object[]> descricoes = this.createQuery(query, params).getResultList();
            if (descricoes != null && !descricoes.isEmpty()) {
                AlertaTO alerta = new AlertaTO();
                alerta.setTitulo(tipoAlerta.getTitulo());
                alerta.setAlertas(descricoes, tipoAlerta.getLink());
                if (replaceByDescription) {
                    AlertaHelper.buildAlertas(alerta, tipoAlerta);
                    alertas.add(alerta);
                } else {
                    alertas.addAll(AlertaHelper.buildAlertasByTitle(alerta, tipoAlerta));
                }
            }
        }
    }

}
