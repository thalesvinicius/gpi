package br.com.gpi.server.domain.entity;

import br.com.gpi.server.core.exception.EnumException;
import br.com.gpi.server.core.jpa.typesafe.EnumerationEntityType;
import static br.com.gpi.server.domain.entity.OrigemEmpresaEnum.values;

public enum OrigemEmpresaEnum implements EnumerationEntityType<Integer> {

    INVALID(0, "invalid"),  MINEIRA(1, "Mineira"), OUTRO_ESTADO(2, "Outro Estado"), INTERNACIONAL(3, "Internacional");

    private OrigemEmpresaEnum(int id, String descricao) {
        this.id = id;
        this.descricao = descricao;
    }

    private final int id;
    private final String descricao;

    @Override
    public Integer getId() {
        return this.id;
    }

    @Override
    public String getDescription() {
        return this.descricao;
    }
    
    public static OrigemEmpresaEnum getOrigemEmpresa(Integer id) {
        for (OrigemEmpresaEnum situacaoProjeto : values()) {
            if (situacaoProjeto.id == id) {
                return situacaoProjeto;
            }
        }
        throw new EnumException("OrigemEmpresa couldn't load.");
    }
}
