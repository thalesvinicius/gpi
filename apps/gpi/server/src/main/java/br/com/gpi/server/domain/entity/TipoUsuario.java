package br.com.gpi.server.domain.entity;

import br.com.gpi.server.core.exception.EnumException;
import br.com.gpi.server.core.jpa.typesafe.EnumerationEntityType;
import java.util.Objects;

public enum TipoUsuario implements EnumerationEntityType<Integer>{

    INTERNO(1, "Interno"), EXTERNO(2, "Externo");
    private Integer id;
    private String descricao;
    
    TipoUsuario(Integer id, String descricao) {
        this.id = id;
        this.descricao = descricao;
    }
    
    @Override
    public Integer getId() {
        return this.id;
    }

    @Override
    public String getDescription() {
        return descricao;
    }
    
    public static TipoUsuario getTipoUsuario(Integer id) {
        for (TipoUsuario tipoUsuario : values()) {
            if (Objects.equals(tipoUsuario.id, id)) {
                return tipoUsuario;
            }
        }
        throw new EnumException(String.format("TipoUsuario %s is not recognized", id));
    }

}
