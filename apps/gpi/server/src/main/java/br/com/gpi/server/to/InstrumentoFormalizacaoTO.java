package br.com.gpi.server.to;

import br.com.gpi.server.domain.entity.Empresa;
import br.com.gpi.server.domain.entity.HistoricoSituacaoInstrumentoFormalizacao;
import br.com.gpi.server.domain.entity.TipoInstrFormalizacao;
import br.com.gpi.server.domain.entity.UnidadeEmpresa;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Set;

public class InstrumentoFormalizacaoTO {

    private Long id;
    private String protocolo;
    private TipoInstrFormalizacao tipoInstrFormalizacao;
    private HistoricoSituacaoInstrumentoFormalizacao situacaoAtual;
    private Date dataAssinatura;
    private Date dataAssinaturaPrevista;
    private Integer versao;
    private Empresa auxEmpresa;
    private UnidadeEmpresa auxUnidade;
    private Set<String> empresas;
    private Set<String> projetos;
    private List<InstrumentoFormalizacaoTO> vinculados;
    private InstrumentoFormalizacaoTO pai;
    private String usuarioResponsavelPelaInformacao;
    private Date dataUltimaAlteracao;
    private String localDocumento;
    private String titulo;
    private List<HistoricoSituacaoInstrumentoFormalizacao> situacoes;

    public InstrumentoFormalizacaoTO() {
    }

    public InstrumentoFormalizacaoTO(Date dataAssinaturaPrevista) {
        this.dataAssinaturaPrevista = dataAssinaturaPrevista;
    }
    
    public InstrumentoFormalizacaoTO(Long id, String protocolo, TipoInstrFormalizacao tipoInstrFormalizacao, HistoricoSituacaoInstrumentoFormalizacao situacao, Date dataAssinatura, Integer versao) {
        this.id = id;
        this.protocolo = protocolo;
        this.tipoInstrFormalizacao = tipoInstrFormalizacao;
        this.situacaoAtual = situacao;
        this.dataAssinatura = dataAssinatura;
        this.versao = versao;
    }
    
    public InstrumentoFormalizacaoTO(Long id, String protocolo, TipoInstrFormalizacao tipoInstrFormalizacao, HistoricoSituacaoInstrumentoFormalizacao situacao, Date dataAssinatura, Integer versao, String localDocumento) {
        this(id, protocolo, tipoInstrFormalizacao, situacao, dataAssinatura, versao);
        this.localDocumento = localDocumento;
    }

    public InstrumentoFormalizacaoTO(Long id, String protocolo, TipoInstrFormalizacao tipoInstrFormalizacao, HistoricoSituacaoInstrumentoFormalizacao situacao, Date dataAssinatura, Integer versao, Empresa empresa, UnidadeEmpresa unidade) {
        this.id = id;
        this.protocolo = protocolo;
        this.tipoInstrFormalizacao = tipoInstrFormalizacao;
        this.situacaoAtual = situacao;
        this.dataAssinatura = dataAssinatura;
        this.versao = versao;
        this.auxEmpresa = empresa;
        this.auxUnidade = unidade;
    }
    
    public InstrumentoFormalizacaoTO(Long id, String protocolo, TipoInstrFormalizacao tipoInstrFormalizacao, HistoricoSituacaoInstrumentoFormalizacao situacao, Date dataAssinatura, Integer versao, Empresa empresa, UnidadeEmpresa unidade, String localDocumento, String titulo) {
        this(id, protocolo, tipoInstrFormalizacao, situacao, dataAssinatura, versao, empresa, unidade);
        this.localDocumento = localDocumento;
        this.titulo = titulo;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getProtocolo() {
        return protocolo;
    }

    public void setProtocolo(String protocolo) {
        this.protocolo = protocolo;
    }

    public String getTipoInstrFormalizacao() {
        if (this.tipoInstrFormalizacao != null) {
            return tipoInstrFormalizacao.getDescricao();
        } else {
            return null;
        }
    }

    public void setTipoInstrFormalizacao(TipoInstrFormalizacao tipoInstrFormalizacao) {
        this.tipoInstrFormalizacao = tipoInstrFormalizacao;
    }

    public String getSituacao() {
        if (this.situacaoAtual != null) {
            return this.situacaoAtual.getSituacao().getDescricao();
        } else {
            return null;
        }
    }

    public void setSituacao(HistoricoSituacaoInstrumentoFormalizacao situacao) {
        this.situacaoAtual = situacao;
    }

    public Date getDataAssinatura() {
        return dataAssinatura;
    }

    public void setDataAssinatura(Date dataAssinatura) {
        this.dataAssinatura = dataAssinatura;
    }

    public Integer getVersao() {
        return versao;
    }

    public void setVersao(Integer versao) {
        this.versao = versao;
    }

    public Empresa getAuxEmpresa() {
        return auxEmpresa;
    }

    public void setAuxEmpresa(Empresa auxEmpresa) {
        this.auxEmpresa = auxEmpresa;
    }

    public UnidadeEmpresa getAuxUnidade() {
        return auxUnidade;
    }

    public void setAuxUnidade(UnidadeEmpresa auxUnidade) {
        this.auxUnidade = auxUnidade;
    }

    public Set<String> getEmpresas() {
        return empresas;
    }

    public Set<String> getProjetos() {
        return projetos;
    }

    public void setProjetos(Set<String> projetos) {
        this.projetos = projetos;
    }
        
    public void setEmpresas(Set<String> empresas) {
        this.empresas = empresas;
    }

    public List<InstrumentoFormalizacaoTO> getVinculados() {
        return vinculados;
    }

    public void setVinculados(List<InstrumentoFormalizacaoTO> vinculados) {
        this.vinculados = vinculados;
    }

    public InstrumentoFormalizacaoTO getPai() {
        return pai;
    }

    public void setPai(InstrumentoFormalizacaoTO pai) {
        this.pai = pai;
    }

    public Date getDataAssinaturaPrevista() {
        return dataAssinaturaPrevista;
    }

    public void setDataAssinaturaPrevista(Date dataAssinaturaPrevista) {
        this.dataAssinaturaPrevista = dataAssinaturaPrevista;
    }

    public String getUsuarioResponsavelPelaInformacao() {
        return usuarioResponsavelPelaInformacao;
    }

    public void setUsuarioResponsavelPelaInformacao(String usuarioResponsavelPelaInformacao) {
        this.usuarioResponsavelPelaInformacao = usuarioResponsavelPelaInformacao;
    }

    public Date getDataUltimaAlteracao() {
        return dataUltimaAlteracao;
    }

    public void setDataUltimaAlteracao(Date dataUltimaAlteracao) {
        this.dataUltimaAlteracao = dataUltimaAlteracao;
    }
    
    public String getJoinedEmpresas() {
        if (empresas != null) {
            return String.join(",", empresas);
        } else {
            return null;
        }
    }
    
    public void setLocalDocumento(String localDocumento) {
        this.localDocumento = localDocumento;
    }

    public String getLocalDocumento() {
        return localDocumento;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final InstrumentoFormalizacaoTO other = (InstrumentoFormalizacaoTO) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

    public String getEmpresaOrUnidade() {
        if (auxEmpresa == null) {
            return null;
        }
        return (auxUnidade == null) ? auxEmpresa.getNomePrincipal() : auxUnidade.getNome();
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public List<HistoricoSituacaoInstrumentoFormalizacao> getSituacoes() {
        return situacoes;
    }

    public void setSituacoes(List<HistoricoSituacaoInstrumentoFormalizacao> situacoes) {
        this.situacoes = situacoes;
    }
}
