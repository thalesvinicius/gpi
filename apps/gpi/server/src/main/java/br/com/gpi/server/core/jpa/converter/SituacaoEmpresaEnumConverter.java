package br.com.gpi.server.core.jpa.converter;

import br.com.gpi.server.domain.entity.SituacaoEmpresa;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = false)
public class SituacaoEmpresaEnumConverter implements AttributeConverter<SituacaoEmpresa, Integer> {

    @Override
    public Integer convertToDatabaseColumn(SituacaoEmpresa situacao) {
        return situacao.getId();
    }

    @Override
    public SituacaoEmpresa convertToEntityAttribute(Integer dbData) {
        return SituacaoEmpresa.getSituacaoEmpresa(dbData);
    }

}
