package br.com.gpi.server.domain.entity;

import br.com.gpi.server.core.jpa.converter.TipoFaturamentoEnumConverter;
import br.com.gpi.server.domain.common.BaseEntity;
import br.com.gpi.server.domain.entity.enumerated.EnumTipoFaturamento;
import com.fasterxml.jackson.annotation.JsonIgnore;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.envers.Audited;

@Audited
@Entity
@Table(name = "FaturamentoAnterior")
public class FaturamentoAnterior extends BaseEntity<Long> {
    
    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "financeiro_id")
    private Financeiro financeiro;
    
    @Enumerated
    @Column(name = "tipo", insertable = false, updatable = false)
    private EnumTipoFaturamento tipoFaturamento;
    
    @JsonIgnore
    @Column(name = "tipo")
    private Integer tipoFaturamentoInt;
    
    private int ano;
    
    private Double valor;

    public Financeiro getFinanceiro() {
        return financeiro;
    }

    public void setFinanceiro(Financeiro financeiro) {
        this.financeiro = financeiro;
    }

    public EnumTipoFaturamento getTipoFaturamento() {
        return tipoFaturamento;
    }

    public void setTipoFaturamento(EnumTipoFaturamento tipoFaturamento) {
        if(tipoFaturamento == null) {
            return;
        }
        this.tipoFaturamento = tipoFaturamento;
        this.tipoFaturamentoInt = (new TipoFaturamentoEnumConverter()).convertToDatabaseColumn(tipoFaturamento);
    }

    public Integer getTipoFaturamentoInt() {
        return tipoFaturamentoInt;
    }

    public void setTipoFaturamentoInt(Integer tipoFaturamentoInt) {
        if(tipoFaturamentoInt == null) {
            return;
        }
        this.tipoFaturamentoInt = tipoFaturamentoInt;
        this.tipoFaturamento = (new TipoFaturamentoEnumConverter()).convertToEntityAttribute(tipoFaturamentoInt);
    }
    
    

    public int getAno() {
        return ano;
    }

    public void setAno(int ano) {
        this.ano = ano;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }
    
}
