package br.com.gpi.server.domain.service;

import br.com.gpi.server.domain.common.BaseService;
import br.com.gpi.server.domain.entity.CNAE;
import br.com.gpi.server.domain.entity.Empresa;
import br.com.gpi.server.domain.repository.CNAERepository;
import br.com.gpi.server.to.CnaeWrapper;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import javax.inject.Inject;
import javax.persistence.EntityManager;

public class CNAEService extends BaseService<CNAE> {

    @Inject
    private CNAERepository CNAERepository;

    @Inject
    public CNAEService(EntityManager em) {
        super(CNAE.class, em);
    }

    public CnaeWrapper getCNAEWrapper(Empresa empresa) {
        CnaeWrapper wrapper = new CnaeWrapper();

        wrapper.setDivisoesSelecionadas(new ArrayList<>());
        if (empresa.getDivisoesCnae() != null) {
            wrapper.getDivisoesSelecionadas().addAll(empresa.getDivisoesCnae().stream().map(u -> u.getCnae()).collect(Collectors.toSet()));
        }
        wrapper.setGruposSelecionados(new ArrayList<>());
        if (empresa.getGruposCnae() != null) {
            wrapper.getGruposSelecionados().addAll(empresa.getGruposCnae().stream().map(u -> u.getCnae()).collect(Collectors.toSet()));
        }
        wrapper.setClassesSelecionadas(new ArrayList<>());
        if (empresa.getClassesCnae() != null) {
            wrapper.getClassesSelecionadas().addAll(empresa.getClassesCnae().stream().map(u -> u.getCnae()).collect(Collectors.toSet()));
        }
        
        List<String> classesSelecionadasIds = empresa.getClassesCnae().stream().map(u -> u.getCnaeId()).collect(Collectors.toList());
        List<String> gruposSelecionadasIds = empresa.getGruposCnae().stream().map(u -> u.getCnaeId()).collect(Collectors.toList());
        List<String> divisoesSelecionadasIds = empresa.getDivisoesCnae().stream().map(u -> u.getCnaeId()).collect(Collectors.toList());
        
        if(!classesSelecionadasIds.isEmpty()) {
            wrapper.setSubclassesCarregadas(CNAERepository.findByParentId(classesSelecionadasIds));
        } else {
            wrapper.setSubclassesCarregadas(new ArrayList<>());
        }
        if(!gruposSelecionadasIds.isEmpty()) {
            wrapper.setClassesCarregadas(CNAERepository.findByParentId(gruposSelecionadasIds));
        } else {
            wrapper.setClassesCarregadas(new ArrayList<>());
        }
        if(!divisoesSelecionadasIds.isEmpty()) {
            wrapper.setGruposCarregados(CNAERepository.findByParentId(divisoesSelecionadasIds));
        } else {
            wrapper.setGruposCarregados(new ArrayList<>());
        }
        return wrapper;
    }

   }
