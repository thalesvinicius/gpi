package br.com.gpi.server.to;


public class FaturamentoDoGrupoERealizadoEmMinasGeraisDTO {
    private int ano;
    private Double valorGrupo;
    private Long idGrupo;
    private Double valorMinasGerais;
    private Long idMinasGerais;
    
    public FaturamentoDoGrupoERealizadoEmMinasGeraisDTO() {
        
    }
    
    public FaturamentoDoGrupoERealizadoEmMinasGeraisDTO(int ano, Double valorGrupo, 
            Double valorMinasGerais) {
        this.ano = ano;
        this.valorGrupo = valorGrupo;
        this.valorMinasGerais = valorMinasGerais;                
    }

    public int getAno() {
        return ano;
    }

    public void setAno(int ano) {
        this.ano = ano;
    }

    public Double getValorGrupo() {
        return valorGrupo;
    }

    public void setValorGrupo(Double valorGrupo) {
        this.valorGrupo = valorGrupo;
    }

    public Double getValorMinasGerais() {
        return valorMinasGerais;
    }

    public void setValorMinasGerais(Double valorMinasGerais) {
        this.valorMinasGerais = valorMinasGerais;
    }

    public Long getIdGrupo() {
        return idGrupo;
    }

    public void setIdGrupo(Long idGrupo) {
        this.idGrupo = idGrupo;
    }

    public Long getIdMinasGerais() {
        return idMinasGerais;
    }

    public void setIdMinasGerais(Long idMinasGerais) {
        this.idMinasGerais = idMinasGerais;
    }

}
