package br.com.gpi.server.domain.entity;

import br.com.gpi.server.domain.common.AuditedEntity;
import com.fasterxml.jackson.annotation.JsonIgnore;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.hibernate.envers.Audited;
import org.hibernate.validator.constraints.Email;

@Audited
@Entity
@Table(name = "UnidadeEmpresa")
@NamedQueries({
    @NamedQuery(name = UnidadeEmpresa.Query.findAllByEmpresaId, query = "SELECT u FROM UnidadeEmpresa u WHERE u.empresa.id = ?"),
    @NamedQuery(name = UnidadeEmpresa.Query.getUnidadeComNome, query = "SELECT u FROM UnidadeEmpresa u WHERE lower(u.nome) = lower(?) and u.empresa.id = ?"),
    @NamedQuery(name = UnidadeEmpresa.Query.findUnidadeEmpresaWithEndereco, query = "SELECT new br.com.gpi.server.to.SearchEmpresaUnidadeTO(u.id, u.nome, e) FROM UnidadeEmpresa u LEFT JOIN u.endereco e WHERE u.empresa.id = :empresaId"),
    @NamedQuery(name = UnidadeEmpresa.Query.deleteByEmpresaId, query = "delete FROM UnidadeEmpresa u WHERE u.empresa.id in ?1"),
    @NamedQuery(name = UnidadeEmpresa.Query.findAllByIds, query = "SELECT u FROM UnidadeEmpresa u WHERE u.id in ?1") 
        
        
})
public class UnidadeEmpresa extends AuditedEntity<Long> {

    @NotNull
    @Column(name = "nome", length = 150, nullable = false)
    private String nome;

    @Email
    @Size(max = 50)
    @Column(name = "email", length = 50)
    private String email;

    @Column(name = "telefone", length = 18)
    private String telefone;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "endereco_id", unique = true, nullable = true)
    private Endereco endereco;

    @JsonIgnore
    @NotNull
    @ManyToOne(optional = false)
    @JoinColumn(name = "empresa_id", nullable = false)
    private Empresa empresa;
    
    @Transient
    private transient String nomePrincipal;

//    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)   
//    @JoinTable(name = "ContatoPessoaUnidadeEmpresa", joinColumns = {
//        @JoinColumn(name = "UnidadeEmpresa_id")}, inverseJoinColumns = {
//        @JoinColumn(name = "contatoPessoa_id")})
//    private Set<ContatoPessoa> contatosPessoa;
        
    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public Endereco getEndereco() {
        return endereco;
    }

    public void setEndereco(Endereco endereco) {
        this.endereco = endereco;
    }
    public Empresa getEmpresa() {
        return empresa;
    }

    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }

    public String getNomePrincipal() {
        return nome;
    }
    
//    public Set <ContatoPessoa> getContatosPessoa() {
//        return contatosPessoa;
//    }
//
//    public void setContatosPessoa(Set<ContatoPessoa> contatosPessoa) {
//        this.contatosPessoa = contatosPessoa;
//    }

    public static class Query {        
        public static final String findAllByEmpresaId = "UnidadeEmpresa.findAllByEmpresaId";
        public static final String getUnidadeComNome = "UnidadeEmpresa.isUnidadeValida";
        public static final String findUnidadeEmpresaWithEndereco = "UnidadeEmpresa.findWithEnderecoByEmpresaId";
        public static final String deleteByEmpresaId = "UnidadeEmpresa.deleteUnidadeEmpresaByEmpresaId";
        public static final String findAllByIds = "UnidadeEmpresa.findAllByIds";
    }
        
}
