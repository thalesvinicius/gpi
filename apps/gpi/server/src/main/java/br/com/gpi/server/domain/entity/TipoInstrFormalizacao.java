package br.com.gpi.server.domain.entity;

import br.com.gpi.server.core.exception.EnumException;

public enum TipoInstrFormalizacao {

    INVALID(0, "invalid"),  
    APROV_FINANCIAMENTO(1, "Aprovação Financiamento BDMG"),
    AQUIS_TERRENO(2, "Aquisição de Terreno"),
    AUTORIZACAO_DIRETORIA(3, "Autorização Diretoria"),
    PROTOCOLO_INTENCAO(4, "Protocolo de Intenções"),
    TERMO_ADITIVO(5, "Termo Aditivo");

    private final Integer id;
    private final String descricao;

    private TipoInstrFormalizacao(Integer id, String descricao) {
        this.id = id;
        this.descricao = descricao;
    }

    public Integer getId() {
        return id;
    }

    public String getDescricao() {
        return descricao;
    }

    public static TipoInstrFormalizacao getTipoInstrFormalizacao(Integer id) {
        for (TipoInstrFormalizacao tipo : values()) {
            if (tipo.getId().equals(id)) {
                return tipo;
            }
        }
        throw new EnumException(String.format("Not recognized value %d for TipoInstrFormalizacao enum", id));
    }
}
