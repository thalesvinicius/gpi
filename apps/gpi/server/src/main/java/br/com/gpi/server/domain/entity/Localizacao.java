package br.com.gpi.server.domain.entity;

import br.com.gpi.server.domain.common.AuditedEntity;
import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.Set;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import org.hibernate.envers.Audited;

/**
 * @since 01/07/2014
 * @author <a href="mailto:thalesvd@algartech.com">Thales Dias</a>
 */
@Audited(withModifiedFlag=true)
@Entity
@Table(name = "Localizacao")
@NamedQueries({
    @NamedQuery(name = Localizacao.Query.locateByProjetoId, query = "SELECT l FROM Localizacao l WHERE l.projeto.id = ?")
})
public class Localizacao extends AuditedEntity<Long> {

    @JsonIgnore
    @OneToOne
    @JoinColumn(name = "projeto_id")
    private Projeto projeto;

    @Column(name = "latitude", length = 6, nullable = false)
    private String latitude;

    @Column(name = "longetude", length = 6, nullable = false)
    private String longetude;

    @Column(name = "poligono", nullable = false)
    private String poligono;

    @ManyToOne
    @JoinColumn(name = "municipio_id")
    private Municipio municipio;

    @ManyToOne
    @JoinColumn(name = "microRegiao_id")
    private MicroRegiao microRegiao;

    @ManyToOne
    @JoinColumn(name = "regiaoPlanejamento_id")
    private RegiaoPlanejamento regiaoPlanejamento;

    @Column(name = "localizacaoSecundaria", length = 255, nullable = true)
    private String localizacaoSecundaria;

    @NotNull
    @Basic(optional = false)
    private Boolean aDefinir;

    @OneToMany(mappedBy = "localizacao", cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true)
    private Set<AnexoLocalizacao> anexosLocalizacao;

    public Projeto getProjeto() {
        return projeto;
    }

    public void setProjeto(Projeto projeto) {
        this.projeto = projeto;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongetude() {
        return longetude;
    }

    public void setLongetude(String longetude) {
        this.longetude = longetude;
    }

    public String getPoligono() {
        return poligono;
    }

    public void setPoligono(String poligono) {
        this.poligono = poligono;
    }

    public Municipio getMunicipio() {
        return municipio;
    }

    public void setMunicipio(Municipio municipio) {
        this.municipio = municipio;
    }

    public MicroRegiao getMicroRegiao() {
        return microRegiao;
    }

    public void setMicroRegiao(MicroRegiao microRegiao) {
        this.microRegiao = microRegiao;
    }

    public RegiaoPlanejamento getRegiaoPlanejamento() {
        return regiaoPlanejamento;
    }

    public void setRegiaoPlanejamento(RegiaoPlanejamento regiaoPlanejamento) {
        this.regiaoPlanejamento = regiaoPlanejamento;
    }

    public String getLocalizacaoSecundaria() {
        return localizacaoSecundaria;
    }

    public void setLocalizacaoSecundaria(String localizacaoSecundaria) {
        this.localizacaoSecundaria = localizacaoSecundaria;
    }

    public Boolean getaDefinir() {
        return aDefinir;
    }

    public void setaDefinir(Boolean aDefinir) {
        this.aDefinir = aDefinir;
    }

    public Set<AnexoLocalizacao> getAnexosLocalizacao() {
        return anexosLocalizacao;
    }

    public void setAnexosLocalizacao(Set<AnexoLocalizacao> anexosLocalizacao) {
        this.anexosLocalizacao = anexosLocalizacao;
    }

    public static class Query {

        public static final String locateByProjetoId = "Localizacao.findAllByProjetoId";
    }

}
