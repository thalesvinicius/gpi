package br.com.gpi.server.domain.user.exception;

import br.com.gpi.server.core.exception.BaseWebApplicationException;
import br.com.gpi.server.domain.entity.VerificationToken;

/**
 *
 * @version 1.0
 * @author <a href="mailto:pviniciusfm@gmail.com">Paulo Vinicius F. Machado</a>
 */
public class AlreadyUsedTokenException extends BaseWebApplicationException {

    private static final long serialVersionUID = 1L;

    public AlreadyUsedTokenException(VerificationToken.VerificationTokenType type) {
        super(409, "40905", "user.invalid.alreadyActivated", "The token has already been verified");
        String message = "";
        switch (type) {
            case emailVerification:
                message = "Seu usuario ja foi ativado.";
                break;
            case lostPassword:
                message = "Sua senha ja foi recuperada.";
                break;
        }
        setApplicationMessage(message);        
    }
}
