package br.com.gpi.server.endpoint;

import br.com.gpi.server.domain.entity.User;
import br.com.gpi.server.domain.service.AlertaService;
import br.com.gpi.server.domain.service.UserService;
import br.com.gpi.server.to.AlertaTO;
import java.util.List;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/alerta/{id}")
@Stateless
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
@TransactionManagement(TransactionManagementType.BEAN)
public class AlertaEndpoint {

    @Inject
    private AlertaService alertaService;
    
    @Inject
    private UserService userService;
    
    @PathParam("id")
    private Long id;

    @GET
    public Response findAllAlertasByLoggedUser() {
        User user = userService.findById(id);
        List<AlertaTO> alertas = alertaService.findAllAlertasByLoggedUser(user);
        return Response.ok(alertas).build();
    }

}
