package br.com.gpi.server.domain.entity;

import br.com.gpi.server.domain.common.AuditedEntity;
import java.util.Date;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.hibernate.envers.Audited;

@Audited
@Entity
@Table(name = "Publicacao")
@NamedQueries({
    @NamedQuery(name = Publicacao.Query.findAnexosByPublicacao, query = "SELECT a FROM Publicacao p JOIN p.anexos AS a  WHERE p.id = ?")
})
public class Publicacao extends AuditedEntity<Long> {
    
    @OneToOne(cascade = CascadeType.ALL,fetch = FetchType.LAZY)
    @JoinColumn(name = "instrumentoFormalizacao_id")
    private InstrumentoFormalizacao instrumentoFormalizacao;
    @Temporal(TemporalType.DATE)
    private Date dataPublicacao;

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(name = "PublicacaoAnexo", joinColumns = {
        @JoinColumn(name = "publicacao_id")}, inverseJoinColumns = {
        @JoinColumn(name = "anexo_id")})
    private Set<Anexo> anexos;

    public InstrumentoFormalizacao getInstrumentoFormalizacao() {
        return instrumentoFormalizacao;
    }

    public void setInstrumentoFormalizacao(InstrumentoFormalizacao instrumentoFormalizacao) {
        this.instrumentoFormalizacao = instrumentoFormalizacao;
    }

    public Date getDataPublicacao() {
        return dataPublicacao;
    }

    public void setDataPublicacao(Date dataPublicacao) {
        this.dataPublicacao = dataPublicacao;
    }

    public Set<Anexo> getAnexos() {
        return anexos;
    }

    public void setAnexos(Set<Anexo> anexos) {
        this.anexos = anexos;
    }

    public static class Query {

        public static final String findAnexosByPublicacao = "Publicacao.findAnexosByPublicacao";
    }

}
