package br.com.gpi.server.domain.service;

import br.com.gpi.server.domain.entity.GrupoUsuario;
import br.com.gpi.server.domain.entity.UsuarioInterno;
import br.com.gpi.server.querybuilder.Condition;
import br.com.gpi.server.querybuilder.ConditionFactory;
import br.com.gpi.server.querybuilder.DynamicQuery;
import br.com.gpi.server.querybuilder.Locator;
import br.com.gpi.server.to.AdvancedSearchRequest;
import br.com.gpi.server.to.ConditionTO;
import br.com.gpi.server.to.FieldTO;
import java.util.List;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;

public class BuscaAvancadaService {

    @Inject
    private EntityManager em;

    public List buscaAvancada(AdvancedSearchRequest advRequest, Long idUsuario) {
        UsuarioInterno usuarioLogado = this.em.find(UsuarioInterno.class, idUsuario);
        DynamicQuery dynamicQuery = new DynamicQuery();
        
        if(usuarioLogado.getRole().getId() == GrupoUsuario.GERENTE.longValue()) {
            advRequest.getSelectedConditions().add(new ConditionTO(new String[]{idUsuario.toString()}, "projetoResponsavelCadeiaProdutiva"));
            
        } else if(usuarioLogado.getRole().getId() == GrupoUsuario.ANALISTA_DE_PROMOCAO.longValue()) {
            advRequest.getSelectedConditions().add(new ConditionTO(new String[] {idUsuario.toString()}, "projetoResponsavelInvestimento"));
        }
        
        for (int i = 0; i < advRequest.getSelectedFields().size(); i++) {
            FieldTO f = advRequest.getSelectedFields().get(i);
            if (f == null) {
                continue;
            }
            if (!Locator.CAMPOS_MAP.containsKey(f.getIdentifier())) {
                throw new RuntimeException(String.format("O campo %s da busca avançada não existe.", f.getIdentifier()));
            } else {
                dynamicQuery.addSelect(Locator.CAMPOS_MAP.get(f.getIdentifier()));
            }
        }
        
        ConditionFactory factory = new ConditionFactory();
        advRequest.getSelectedConditions().stream().forEachOrdered(to -> {
            Condition condition = factory.getInstance(to);
            if (condition != null) {
                dynamicQuery.addCondition(condition);
            }
        });
        
        dynamicQuery.addOrderBy(Locator.campoEmpresaNomePrincipal, Locator.campoProjetoNome);
        
        Query nativeQuery = em.createNativeQuery(dynamicQuery.getSQL());
        List list = nativeQuery.getResultList();
        
        return list;
    }
}
