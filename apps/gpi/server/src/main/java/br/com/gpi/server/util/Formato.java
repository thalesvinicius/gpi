/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.gpi.server.util;

/**
 *
 * @author marcoabc
 */
public enum Formato {

    PDF(".pdf"), XLS(".xls");

    private final String extensao;

    private Formato(String extensao) {
        this.extensao = extensao;
    }

    public String getExtensao() {
        return extensao;
    }

    public static Formato getFormatoComExtensao(String extensao) {
        if (extensao != null && !extensao.isEmpty()) {
            for (Formato f : values()) {
                if (f.name().equalsIgnoreCase(extensao)) {
                    return f;
                }
            }
        }
        return null;
    }
}
