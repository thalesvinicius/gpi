package br.com.gpi.server.domain.service;

import br.com.gpi.server.core.exception.BaseWebApplicationException;

class ExternalUserDuplicatedException extends BaseWebApplicationException {

    public ExternalUserDuplicatedException() {
        super(409, "409", "MSG80_LOGIN_DUPLICADO", "");
    }

}
