package br.com.gpi.server.domain.entity;

import br.com.gpi.server.domain.common.AuditedEntity;
import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import org.hibernate.annotations.Where;
import org.hibernate.envers.Audited;

@Audited
@Entity
@Table(name = "InsumoProduto")
@NamedQueries({
    @NamedQuery(name = InsumoProduto.Query.findByProjetoId, query = "Select ip FROM InsumoProduto ip WHERE ip.projeto.id = ?1")
})
public class InsumoProduto extends AuditedEntity<Long> {

    @JsonIgnore
    @OneToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "projeto_id")
    private Projeto projeto;

    @NotNull
    @Column(nullable = false)
    private Boolean aquisicaoOutroEstado;
    private Boolean interesseParceria;
    private Boolean importacaoProduto;
    private Boolean contribuinteSubstituto;
    private Double percentualOrigemMinas;
    private Double percentualOrigemOutros;
    private Double percentualOrigemImportado;
    private Double percentualFabricaMinas;
    private Double percentualAdquiridoOutro;
    private Double percentualImportado;
    private Double percentualValorAgregado;
    private Double percentualICMS;
    private Double percentualClienteIndustria;
    private Double percentualClienteComercial;
    private Double percentualClienteConsumidor;
    private Double percentualMercadoMinas;
    private Double percentualMercadoNacional;
    private Double percentualMercadoExterior;
    private Double percentualTributoEfetivo;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "insumoProduto", fetch = FetchType.EAGER, orphanRemoval = true)
    private Set<Insumo> insumos;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "insumoProduto", fetch = FetchType.LAZY)
    private Set<Produto> produtos;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "insumoProduto", targetEntity = Produto.class, fetch = FetchType.EAGER, orphanRemoval = true)
    @Where(clause = "tipo = " + Produto.PRODUTO_ADQ_E_COM_POR_MINAS)
    private Set<ProdutoAdqEComPorMinas> produtosAdqEComPorMinas;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "insumoProduto", targetEntity = Produto.class, fetch = FetchType.EAGER, orphanRemoval = true)
    @Where(clause = "tipo = " + Produto.PRODUTO_ADQ_OUTROS_ESTADOS_PARA_COM)
    private Set<ProdutoAdqOutrosEstadosParaCom> produtosAdqOutrosEstadosParaCom;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "insumoProduto", targetEntity = Produto.class, fetch = FetchType.EAGER, orphanRemoval = true)
    @Where(clause = "tipo = " + Produto.PRODUTO_FAB_E_COM_POR_MINAS)
    private Set<ProdutoFabEComPorMinas> produtosFabEComPorMinas;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "insumoProduto", targetEntity = Produto.class, fetch = FetchType.EAGER, orphanRemoval = true)
    @Where(clause = "tipo = " + Produto.PRODUTO_IMP_PARA_COM)
    private Set<ProdutoImpParaCom> produtosImpParaCom;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "insumoProduto", fetch = FetchType.EAGER, orphanRemoval = true)
    private Set<Parceria> parcerias;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "insumoProduto", fetch = FetchType.EAGER, orphanRemoval = true)
    private Set<Servico> servicos;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "insumoProduto", fetch = FetchType.EAGER, orphanRemoval = true)
    private Set<Producao> producoes;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "insumoProduto", fetch = FetchType.EAGER, orphanRemoval = true)
    private Set<Concorrente> concorrentes;

    public Projeto getProjeto() {
        return projeto;
    }

    public void setProjeto(Projeto projeto) {
        this.projeto = projeto;
    }
    
    public Boolean getAquisicaoOutroEstado(){
        return this.isAquisicaoOutroEstado();
    }
    
    public Boolean isAquisicaoOutroEstado() {
        return aquisicaoOutroEstado;
    }

    public void setAquisicaoOutroEstado(Boolean aquisicaoOutroEstado) {
        this.aquisicaoOutroEstado = aquisicaoOutroEstado;
    }

    public Boolean isInteresseParceria() {
        return interesseParceria;
    }

    public Boolean getInteresseParceria() {
        return interesseParceria;
    }

    public void setInteresseParceria(Boolean interesseParceria) {
        this.interesseParceria = interesseParceria;
    }

    public Boolean isImportacaoProduto() {
        return importacaoProduto;
    }
    
    public Boolean getImportacaoProduto() {
        return isImportacaoProduto();
    }

    public void setImportacaoProduto(Boolean importacaoProduto) {
        this.importacaoProduto = importacaoProduto;
    }

    public Boolean isContribuinteSubstituto() {
        return contribuinteSubstituto;
    }
    
    public Boolean getContribuinteSubstituto() {
        return isContribuinteSubstituto();
    }

    public void setContribuinteSubstituto(Boolean contribuinteSubstituto) {
        this.contribuinteSubstituto = contribuinteSubstituto;
    }

    public Double getPercentualOrigemMinas() {
        return percentualOrigemMinas;
    }

    public void setPercentualOrigemMinas(Double percentualOrigemMinas) {
        this.percentualOrigemMinas = percentualOrigemMinas;
    }

    public Double getPercentualOrigemOutros() {
        return percentualOrigemOutros;
    }

    public void setPercentualOrigemOutros(Double percentualOrigemOutros) {
        this.percentualOrigemOutros = percentualOrigemOutros;
    }

    public Double getPercentualOrigemImportado() {
        return percentualOrigemImportado;
    }

    public void setPercentualOrigemImportado(Double percentualOrigemImportado) {
        this.percentualOrigemImportado = percentualOrigemImportado;
    }

    public Double getPercentualFabricaMinas() {
        return percentualFabricaMinas;
    }

    public void setPercentualFabricaMinas(Double percentualFabricaMinas) {
        this.percentualFabricaMinas = percentualFabricaMinas;
    }

    public Double getPercentualAdquiridoOutro() {
        return percentualAdquiridoOutro;
    }

    public void setPercentualAdquiridoOutro(Double percentualAdquiridoOutro) {
        this.percentualAdquiridoOutro = percentualAdquiridoOutro;
    }

    public Double getPercentualImportado() {
        return percentualImportado;
    }

    public void setPercentualImportado(Double percentualImportado) {
        this.percentualImportado = percentualImportado;
    }

    public Double getPercentualValorAgregado() {
        return percentualValorAgregado;
    }

    public void setPercentualValorAgregado(Double percentualValorAgregado) {
        this.percentualValorAgregado = percentualValorAgregado;
    }

    public Double getPercentualICMS() {
        return percentualICMS;
    }

    public void setPercentualICMS(Double percentualICMS) {
        this.percentualICMS = percentualICMS;
    }

    public Double getPercentualClienteIndustria() {
        return percentualClienteIndustria;
    }

    public void setPercentualClienteIndustria(Double percentualClienteIndustria) {
        this.percentualClienteIndustria = percentualClienteIndustria;
    }

    public Double getPercentualClienteComercial() {
        return percentualClienteComercial;
    }

    public void setPercentualClienteComercial(Double percentualClienteComercial) {
        this.percentualClienteComercial = percentualClienteComercial;
    }

    public Double getPercentualClienteConsumidor() {
        return percentualClienteConsumidor;
    }

    public void setPercentualClienteConsumidor(Double percentualClienteConsumidor) {
        this.percentualClienteConsumidor = percentualClienteConsumidor;
    }

    public Double getPercentualMercadoMinas() {
        return percentualMercadoMinas;
    }

    public void setPercentualMercadoMinas(Double percentualMercadoMinas) {
        this.percentualMercadoMinas = percentualMercadoMinas;
    }

    public Double getPercentualMercadoNacional() {
        return percentualMercadoNacional;
    }

    public void setPercentualMercadoNacional(Double percentualMercadoNacional) {
        this.percentualMercadoNacional = percentualMercadoNacional;
    }

    public Double getPercentualMercadoExterior() {
        return percentualMercadoExterior;
    }

    public void setPercentualMercadoExterior(Double percentualMercadoExterior) {
        this.percentualMercadoExterior = percentualMercadoExterior;
    }

    public Double getPercentualTributoEfetivo() {
        return percentualTributoEfetivo;
    }

    public void setPercentualTributoEfetivo(Double percentualTributoEfetivo) {
        this.percentualTributoEfetivo = percentualTributoEfetivo;
    }

    public Set<Insumo> getInsumos() {
        return insumos;
    }

    public void setInsumos(Set<Insumo> insumos) {
        this.insumos = insumos;
    }

    public Set<ProdutoAdqEComPorMinas> getProdutosAdqEComPorMinas() {
        return produtosAdqEComPorMinas;
    }

    public void setProdutosAdqEComPorMinas(Set<ProdutoAdqEComPorMinas> produtosAdqEComPorMinas) {
        this.produtosAdqEComPorMinas = produtosAdqEComPorMinas;
    }

    public Set<ProdutoAdqOutrosEstadosParaCom> getProdutosAdqOutrosEstadosParaCom() {
        return produtosAdqOutrosEstadosParaCom;
    }

    public void setProdutosAdqOutrosEstadosParaCom(Set<ProdutoAdqOutrosEstadosParaCom> produtosAdqOutrosEstadosParaCom) {
        this.produtosAdqOutrosEstadosParaCom = produtosAdqOutrosEstadosParaCom;
    }

    public Set<ProdutoFabEComPorMinas> getProdutosFabEComPorMinas() {
        return produtosFabEComPorMinas;
    }

    public void setProdutosFabEComPorMinas(Set<ProdutoFabEComPorMinas> produtosFabEComPorMinas) {
        this.produtosFabEComPorMinas = produtosFabEComPorMinas;
    }

    public Set<ProdutoImpParaCom> getProdutosImpParaCom() {
        return produtosImpParaCom;
    }

    public void setProdutosImpParaCom(Set<ProdutoImpParaCom> produtosImpParaCom) {
        this.produtosImpParaCom = produtosImpParaCom;
    }

    public Set<Parceria> getParcerias() {
        return parcerias;
    }

    public void setParcerias(Set<Parceria> parcerias) {
        this.parcerias = parcerias;
    }

    public Set<Servico> getServicos() {
        return servicos;
    }

    public void setServicos(Set<Servico> servicos) {
        this.servicos = servicos;
    }

    public Set<Producao> getProducoes() {
        return producoes;
    }

    public void setProducoes(Set<Producao> producoes) {
        this.producoes = producoes;
    }

    public Set<Concorrente> getConcorrentes() {
        return concorrentes;
    }

    public void setConcorrentes(Set<Concorrente> concorrentes) {
        this.concorrentes = concorrentes;
    }

    public Set<Produto> getProdutos() {
        return produtos;
    }

    public void setProdutos(Set<Produto> produtos) {
        this.produtos = produtos;
    }

    public static class Query {

        public static final String findByProjetoId = "InsumoProduto.findByProjetoId";
    }

}
