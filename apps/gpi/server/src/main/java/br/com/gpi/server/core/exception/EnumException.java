package br.com.gpi.server.core.exception;

public class EnumException extends RuntimeException {

    public EnumException(String message) {
        super(message);
    }

}
