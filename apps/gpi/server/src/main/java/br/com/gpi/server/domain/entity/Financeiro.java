package br.com.gpi.server.domain.entity;

import br.com.gpi.server.domain.common.AuditedEntity;
import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import org.hibernate.envers.Audited;

@Audited
@Entity
@Table(name = "Financeiro")
@NamedQueries({
    @NamedQuery(name = Financeiro.Query.locateById, query = "SELECT f FROM Financeiro f WHERE f.id = :id"),
    @NamedQuery(name = Financeiro.Query.locateByProjetoId, query = "SELECT f FROM Financeiro f WHERE f.projeto.id = ?")
})
public class Financeiro extends AuditedEntity<Long> {
    
    @JsonIgnore
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "projeto_id")
    private Projeto projeto;
    
    private String observacao;
    
    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(name = "FinanceiroAnexo", joinColumns = {
        @JoinColumn(name = "financeiro_id")}, inverseJoinColumns = {
        @JoinColumn(name = "anexo_id")})
    private Set<Anexo> anexos;   
        
    @OneToMany(mappedBy = "financeiro", fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<InvestimentoPrevisto> investimentoPrevistoList;

    @OneToMany(mappedBy = "financeiro", fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<OrigemRecurso> origemRecursoList;
    
    @OneToMany(mappedBy = "financeiro", fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<UsoFonte> usoFonteList;
    
    @OneToMany(mappedBy = "financeiro", fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<FaturamentoAnterior> faturamentoAnteriorList;
    
    @OneToMany(mappedBy = "financeiro", fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<FaturamentoPrevisto> faturamentoPrevistoList;

    
    
    public Projeto getProjeto() {
        return projeto;
    }

    public void setProjeto(Projeto projeto) {
        this.projeto = projeto;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public Set<Anexo> getAnexos() {
        return anexos;
    }

    public void setAnexos(Set<Anexo> anexos) {
        this.anexos = anexos;
    }

    public Set<InvestimentoPrevisto> getInvestimentoPrevistoList() {
        return investimentoPrevistoList;
    }

    public void setInvestimentoPrevistoList(Set<InvestimentoPrevisto> investimentoPrevistoList) {
        this.investimentoPrevistoList = investimentoPrevistoList;
    }

    public Set<OrigemRecurso> getOrigemRecursoList() {
        return origemRecursoList;
    }

    public void setOrigemRecursoList(Set<OrigemRecurso> origemRecursoList) {
        this.origemRecursoList = origemRecursoList;
    }

    public Set<UsoFonte> getUsoFonteList() {
        return usoFonteList;
    }

    public void setUsoFonteList(Set<UsoFonte> usoFonteList) {
        this.usoFonteList = usoFonteList;
    }

    public Set<FaturamentoAnterior> getFaturamentoAnteriorList() {
        return faturamentoAnteriorList;
    }

    public void setFaturamentoAnteriorList(Set<FaturamentoAnterior> faturamentoAnteriorList) {
        this.faturamentoAnteriorList = faturamentoAnteriorList;
    }

    public Set<FaturamentoPrevisto> getFaturamentoPrevistoList() {
        return faturamentoPrevistoList;
    }

    public void setFaturamentoPrevistoList(Set<FaturamentoPrevisto> faturamentoPrevistoList) {
        this.faturamentoPrevistoList = faturamentoPrevistoList;
    }
        
    public static class Query {
        public static final String locateById = "Financeiro.locateById";
        public static final String locateByProjetoId = "Financeiro.locateByProjetoId";
    }
    
}
