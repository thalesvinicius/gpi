set identity_insert Usuario ON GO

Insert Into Usuario(ID, tipo, Cargo_id, Nome, Senha, email, Telefone, Ativo, usuarioResponsavel_id, dataUltimaAlteracao, grupoUsuario_id)
Values(9, 1,1, 'Administrador', 'Y6RIuZfzPJMxHkqB3vTs8kvcg6hPvKHTmp9XXZ+zCQM=', 'gpi@teste.com.br', '(31)9999-9999', 'TRUE', null, '2014-07-17 10:57:52.638', 1) GO
Insert Into UsuarioInterno(id, departamento_id, matricula)
Values(9, 1,'1234') GO

INSERT INTO USUARIO(ID, tipo, CARGO_ID, NOME, SENHA, EMAIL, TELEFONE, ATIVO, USUARIORESPONSAVEL_ID, DATAULTIMAALTERACAO, grupoUsuario_id)
VALUES(10, 1,1, 'infra', 'aTbXc5rV/qYEJE1xqa0YqhamLi2dlcIIPv6wA/r30nA=', NULL, '(31)9999-9999', 'TRUE', 9, '2014-08-04 10:37:45.685', 2) GO
Insert Into UsuarioInterno(id, departamento_id, matricula)
Values(10,2, 'infra') GO

INSERT INTO USUARIO(ID, tipo, CARGO_ID, NOME, SENHA, EMAIL, TELEFONE, ATIVO, USUARIORESPONSAVEL_ID, DATAULTIMAALTERACAO, grupoUsuario_id)
VALUES(11, 1,1, 'info', 'tPppjheCayiASoW6qST/jMm+ddB8kgl5Y3hbiulvS+M=', NULL, '(31)9999-9999', 'TRUE', 9, '2014-08-04 10:38:15.494', 3) GO
Insert Into UsuarioInterno(id, departamento_id, matricula)
Values(11,3, 'info') GO

INSERT INTO USUARIO(ID, tipo, CARGO_ID, NOME, SENHA, EMAIL, TELEFONE, ATIVO, USUARIORESPONSAVEL_ID, dataUltimaAlteracao, grupoUsuario_id)
VALUES(12, 1,1, 'portfolio', '25kVFmB8Kl3mm3HuAJqenQQkpVYYfZoFRAsCsjFQptw=', NULL, '(31)9999-9999', 'TRUE', 9, '2014-08-04 10:38:34.569', 4) GO
Insert Into UsuarioInterno(id, departamento_id, matricula)
Values(12,4, 'portfolio') GO

INSERT INTO USUARIO(ID, tipo, CARGO_ID, NOME, SENHA, EMAIL, TELEFONE, ATIVO, USUARIORESPONSAVEL_ID, DATAULTIMAALTERACAO, grupoUsuario_id)
VALUES(13, 1,1, 'promocao', 'LDBtZJTO2UJAIcsA00Ax7UlBFawfDmgFHjKCN2NBPSg=', NULL, '(31)9999-9999', 'TRUE', 9, '2014-08-04 10:38:52.372', 5) GO
Insert Into UsuarioInterno(id, departamento_id, matricula)
Values(13,5, 'promocao') GO

INSERT INTO USUARIO(ID, tipo, CARGO_ID, NOME, SENHA, EMAIL, TELEFONE, ATIVO, USUARIORESPONSAVEL_ID, DATAULTIMAALTERACAO, grupoUsuario_id)
VALUES(14, 1,1, 'diretor', '00HHwkGfhTkYIkIyZc7lviZ+dROq5MEyDnAQxFpJYIs=', NULL, '(31)9999-9999', 'TRUE', 9, '2014-08-04 10:39:11.43', 6) GO
Insert Into UsuarioInterno(id, departamento_id, matricula)
Values(14, 6, 'diretor') GO

INSERT INTO USUARIO(ID, tipo, CARGO_ID, NOME, SENHA, EMAIL, TELEFONE, ATIVO, USUARIORESPONSAVEL_ID, DATAULTIMAALTERACAO, grupoUsuario_id)
VALUES(15, 1,1, 'gerente', 'x0qvxP0T3wHNmHh27Gx571m74EqNcCoVawxeimg9FVY=', NULL, '(31)9999-9999', 'TRUE', 9, '2014-08-04 10:39:26.952', 7) GO
Insert Into UsuarioInterno(id, departamento_id, matricula)
Values(15, 7, 'gerente') GO

INSERT INTO USUARIO(ID, tipo, CARGO_ID, NOME, SENHA, EMAIL, TELEFONE, ATIVO, USUARIORESPONSAVEL_ID, DATAULTIMAALTERACAO, grupoUsuario_id)
VALUES(16, 1,1, 'geral', '5anSlQJf689zF97bn1yX5ef/hkOjg9iDMw2+UPR4txs=', NULL, '(31)9999-9999', 'TRUE', 9, '2014-08-04 10:39:40.352', 8) GO
Insert Into UsuarioInterno(id, departamento_id, matricula)
Values(16, 8, 'geral') GO

-- INSERT INTO USUARIO(ID, grupousuario_id, CARGO_ID, NOME, SENHA, EMAIL, TELEFONE, TELEFONE2, ATIVO, USUARIORESPONSAVEL_ID, DATAULTIMAALTERACAO, TIPO) 
-- VALUES (17, 9, 1, 'Usuário Externo Empresa 1', 'coGobraWZhxcCT+R08IUZeRdWM4aG+PtF6CktwaHX98=', 'teste@teste.com', '(31)9999-9999', '1111-1111', 'true', NULL, '2014-07-03 00:00:00.0', 2) GO
-- INSERT INTO USUARIOEXTERNO (ID, EMPRESA_ID, UNIDADE_ID, SENHA, DATABLOQUEIOTEMPORARIO, cargoExterno) 
-- VALUES (17, 1, null, 'coGobraWZhxcCT+R08IUZeRdWM4aG+PtF6CktwaHX98=', '2014-08-22 14:20:52.003', 'Nenhum')
-- GO

set identity_insert Usuario OFF;