alter table logInvestimentoPrevisto add dateCreated datetime  default current_timestamp;
alter table logOrigemRecurso add dateCreated datetime  default current_timestamp;
alter table logUsoFonte add dateCreated datetime  default current_timestamp;
alter table logFaturamentoAnterior add dateCreated datetime  default current_timestamp;
alter table logFaturamentoPrevisto add dateCreated datetime  default current_timestamp;

alter table LogParceria add dateCreated datetime  default current_timestamp;
alter table LogInsumo add dateCreated datetime  default current_timestamp;
alter table LogConcorrente add dateCreated datetime  default current_timestamp;
alter table LogServico add dateCreated datetime  default current_timestamp;
alter table LogProducao add dateCreated datetime  default current_timestamp;
alter table LogProduto add dateCreated datetime  default current_timestamp;
