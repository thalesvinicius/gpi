--  Create Tables 
CREATE TABLE Indicador ( 
	id int NOT NULL,    --  Código do Indicador 
	nome varchar(255) NOT NULL    --  Nome do Indicador 
)
;

CREATE TABLE Meta ( 
	id int identity(1,1)  NOT NULL,    --  Identificador da meta 
	indicador_id int NOT NULL,    --  Código do Indicador do INDI 
	ano int NOT NULL,    --  Ano referente a meta 
	mes int NOT NULL,    --  Mês referente a meta 
	meta decimal(10,2) NOT NULL,    --  Valor da meta 
        estagio_id int NULL
)
;

--  Create Primary Key Constraints 
ALTER TABLE Indicador ADD CONSTRAINT PK_Indicador 
	PRIMARY KEY CLUSTERED (id)
;

ALTER TABLE Meta ADD CONSTRAINT PK_Meta 
	PRIMARY KEY CLUSTERED (id)
;

--  Create Foreign Key Constraints 
ALTER TABLE Meta ADD CONSTRAINT FK_Meta_Indicador 
	FOREIGN KEY (indicador_id) REFERENCES Indicador (id)
;

CREATE INDEX IX_Meta_Indicador ON Meta (indicador_id);

--  Create Foreign Key Constraints 
ALTER TABLE Meta ADD CONSTRAINT FK_Meta_Estagio 
	FOREIGN KEY (estagio_id) REFERENCES Estagio (id)
;

CREATE INDEX IX_Meta_Estagio ON Meta (estagio_id);


EXEC sp_addextendedproperty 'MS_Description', 'Indicadores gerenciais do INDI', 'Schema', dbo, 'table', Indicador
;
EXEC sp_addextendedproperty 'MS_Description', 'Código do Indicador', 'Schema', dbo, 'table', Indicador, 'column', id
;

EXEC sp_addextendedproperty 'MS_Description', 'Nome do Indicador', 'Schema', dbo, 'table', Indicador, 'column', nome
;

EXEC sp_addextendedproperty 'MS_Description', 'Configuração das Metas dos Indicadores do INDI', 'Schema', dbo, 'table', Meta
;
EXEC sp_addextendedproperty 'MS_Description', 'Identificador da meta', 'Schema', dbo, 'table', Meta, 'column', id
;

EXEC sp_addextendedproperty 'MS_Description', 'Código do Indicador do INDI', 'Schema', dbo, 'table', Meta, 'column', indicador_id
;

EXEC sp_addextendedproperty 'MS_Description', 'Ano referente a meta', 'Schema', dbo, 'table', Meta, 'column', ano
;

EXEC sp_addextendedproperty 'MS_Description', 'Mês referente a meta', 'Schema', dbo, 'table', Meta, 'column', mes
;

EXEC sp_addextendedproperty 'MS_Description', 'Valor da meta', 'Schema', dbo, 'table', Meta, 'column', meta
;
