IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.VIEWS
        WHERE TABLE_NAME = 'VIEW_FATURAMENTO_BUSCA_AVANCADA')
    DROP VIEW VIEW_FATURAMENTO_BUSCA_AVANCADA
GO

Create view VIEW_FATURAMENTO_BUSCA_AVANCADA AS
--Faturamento
select
f.projeto_id,
tb.financeiro_id, 
ROUND(max(tb.faturamentoInicialPrevisto),2) as faturamentoInicialPrevisto,
ROUND(max(tb.faturamentoInicialReal),2) as faturamentoInicialReal,
ROUND(max(tb.faturamentoPlenoPrevisto),2) as faturamentoPlenoPrevisto,
ROUND(max(tb.faturamentoPlenoReal),2) as faturamentoPlenoReal
from
(
--faturamento inicial
select 
fp.financeiro_id,
sum(fp.valor)/1000 as faturamentoInicialPrevisto,
sum(fp.valorRealizado)/1000 as faturamentoInicialReal,
0 as faturamentoPlenoPrevisto,
0 as faturamentoPlenoReal
from FaturamentoPrevisto fp
group by fp.financeiro_id, fp.ano
having fp.ano = (
select min(fp1.ano) 
from FaturamentoPrevisto fp1
where fp1.financeiro_id = fp.financeiro_id)

union all

--faturamento real
select 
fp.financeiro_id,
0 as faturamentoInicialPrevisto,
0 as faturamentoInicialReal,
sum(fp.valor)/1000 as faturamentoPlenoPrevisto,
sum(fp.valorRealizado)/1000 as faturamentoPlenoReal
from FaturamentoPrevisto fp
group by fp.financeiro_id, fp.ano
having fp.ano = (
select max(fp1.ano) 
from FaturamentoPrevisto fp1
where fp1.financeiro_id = fp.financeiro_id)
) tb
inner join Financeiro f on tb.financeiro_id = f.id
group by tb.financeiro_id, f.projeto_id;

-- --Faturamento
-- select
-- f.projeto_id,
-- tb.financeiro_id, 
-- replace(replace(replace(convert(varchar,cast(floor(ROUND(max(tb.faturamentoInicialPrevisto),2)) as money),1), ',', 'x'),'.00',
--     ','+right(ROUND(max(tb.faturamentoInicialPrevisto),2),2)), 'x', '.') as faturamentoInicialPrevisto,
-- replace(replace(replace(convert(varchar,cast(floor(ROUND(max(tb.faturamentoInicialReal),2)) as money),1), ',', 'x'),'.00',
--     ','+right(ROUND(max(tb.faturamentoInicialReal),2),2)), 'x', '.') as faturamentoInicialReal,
-- replace(replace(replace(convert(varchar,cast(floor(ROUND(max(tb.faturamentoPlenoPrevisto),2)) as money),1), ',', 'x'),'.00',
--     ','+right(ROUND(max(tb.faturamentoPlenoPrevisto),2),2)), 'x', '.') as faturamentoPlenoPrevisto,
-- replace(replace(replace(convert(varchar,cast(floor(ROUND(max(tb.faturamentoPlenoReal),2)) as money),1), ',', 'x'),'.00',
--     ','+right(ROUND(max(tb.faturamentoPlenoReal),2),2)), 'x', '.') as faturamentoPlenoReal
-- from
-- (
-- --faturamento inicial
-- select 
-- fp.financeiro_id,
-- sum(fp.valor)/1000 as faturamentoInicialPrevisto,
-- sum(fp.valorRealizado)/1000 as faturamentoInicialReal,
-- 0 as faturamentoPlenoPrevisto,
-- 0 as faturamentoPlenoReal
-- from FaturamentoPrevisto fp
-- group by fp.financeiro_id, fp.ano
-- having fp.ano = (
-- select min(fp1.ano) 
-- from FaturamentoPrevisto fp1
-- where fp1.financeiro_id = fp.financeiro_id)
-- 
-- union all
-- 
-- --faturamento real
-- select 
-- fp.financeiro_id,
-- replace(replace(replace(convert(varchar,cast(floor(0) as money),1), ',', 'x'),'.00',
--     ','+right(0,2)), 'x', '.') as faturamentoInicialPrevisto,
-- 0 as faturamentoInicialReal,
-- replace(replace(replace(convert(varchar,cast(floor(sum(fp.valor)/1000) as money),1), ',', 'x'),'.00',
--     ','+right(sum(fp.valor)/1000,2)), 'x', '.') as faturamentoPlenoPrevisto,
-- replace(replace(replace(convert(varchar,cast(floor(sum(fp.valorRealizado)/1000) as money),1), ',', 'x'),'.00',
--     ','+right(sum(fp.valorRealizado)/1000,2)), 'x', '.') as faturamentoPlenoReal
-- from FaturamentoPrevisto fp
-- group by fp.financeiro_id, fp.ano
-- having fp.ano = (
-- select max(fp1.ano) 
-- from FaturamentoPrevisto fp1
-- where fp1.financeiro_id = fp.financeiro_id)
-- ) tb
-- inner join Financeiro f on tb.financeiro_id = f.id
-- group by tb.financeiro_id, f.projeto_id;*/