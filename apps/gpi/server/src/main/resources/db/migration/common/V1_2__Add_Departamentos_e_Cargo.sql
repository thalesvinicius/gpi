-- Departamento
insert into Departamento (id, sigla, descricao, departamentoSuperior_id) values (1, 'DPG', 'DPG - Diretoria de Planejamento e Gestão', null);
insert into Departamento (id, sigla, descricao, departamentoSuperior_id) values (2, 'DPG/AP', 'DPG/AP - Gerência de Administração e Pessoas', 1);
insert into Departamento (id, sigla, descricao, departamentoSuperior_id) values (3, 'DPG/IM', 'DPG/IM - Gerência de Infraestrutura e Meio Ambiente', 1);
insert into Departamento (id, sigla, descricao, departamentoSuperior_id) values (4, 'DPG/TG', 'DPG/TG - Gerência de Tecnologia e Gestão de Projetos', 1);
insert into Departamento (id, sigla, descricao, departamentoSuperior_id) values (5, 'DPI1', 'DPI1 - Diretoria de Promoção de Investimentos 1', null);
insert into Departamento (id, sigla, descricao, departamentoSuperior_id) values (6, 'DPI1/IN1', 'DPI1/IN1 - Gerência de Promoção de Investimentos 1', 5);
insert into Departamento (id, sigla, descricao, departamentoSuperior_id) values (7, 'DPI1/IN2', 'DPI1/IN2 - Gerência de Promoção de Investimentos 2', 5);
insert into Departamento (id, sigla, descricao, departamentoSuperior_id) values (8, 'DPI1/IN7', 'DPI1/IN7 - Gerência de Promoção de Investimentos 7', 5);
insert into Departamento (id, sigla, descricao, departamentoSuperior_id) values (9, 'DPI2', 'DPI2 - Diretoria de Promoção de Investimentos 2', null);
insert into Departamento (id, sigla, descricao, departamentoSuperior_id) values (10, 'DPI2/IN3', 'DPI2/IN3 - Gerência de Promoção de Investimentos 3', 9);
insert into Departamento (id, sigla, descricao, departamentoSuperior_id) values (11, 'DPI2/IN4', 'DPI2/IN4 - Gerência de Promoção de Investimentos 4', 9);
insert into Departamento (id, sigla, descricao, departamentoSuperior_id) values (12, 'DPI2/IN5', 'DPI2/IN5 - Gerência de Promoção de Investimentos 5', 9);
insert into Departamento (id, sigla, descricao, departamentoSuperior_id) values (13, 'DPN', 'DPN - Diretoria de Prospecção de Negócios', null);
insert into Departamento (id, sigla, descricao, departamentoSuperior_id) values (14, 'DPN/PN1', 'DPN/PN1 - Gerência de Prospecção de Negócios 1', 13);
insert into Departamento (id, sigla, descricao, departamentoSuperior_id) values (15, 'DPN/PN2', 'DPN/PN2 - Gerência de Prospecção de Negócios 2', 13);
insert into Departamento (id, sigla, descricao, departamentoSuperior_id) values (16, 'DPR', 'DPR - Presidência', null);
insert into Departamento (id, sigla, descricao, departamentoSuperior_id) values (17, 'DPR/IC', 'DPR/IC - Gerência de Informação e Conhecimento', 16);
insert into Departamento (id, sigla, descricao, departamentoSuperior_id) values (18, 'DPR/JR', 'DPR/JR - Gerência Jurídica', 16);
insert into Departamento (id, sigla, descricao, departamentoSuperior_id) values (19, 'DVP', 'DVP - Vice-presidência', null);

-- Cargo
insert into Cargo (id, descricao) values (1,'Advogado');
insert into Cargo (id, descricao) values (2,'Analista de Administração e Finanças');
insert into Cargo (id, descricao) values (3,'Analista Contábil');
insert into Cargo (id, descricao) values (4,'Analista de Comunicação Social');
insert into Cargo (id, descricao) values (5,'Analista de Promoção de Investimentos');
insert into Cargo (id, descricao) values (6,'Analista de Recursos Humanos');
insert into Cargo (id, descricao) values (7,'Analista de Sistemas');
insert into Cargo (id, descricao) values (8,'Diretor');
insert into Cargo (id, descricao) values (9,'Diretor-Presidente');
insert into Cargo (id, descricao) values (10,'Diretor Vice-Presidente');
insert into Cargo (id, descricao) values (11,'Gerente');
insert into Cargo (id, descricao) values (12,'Secretária de Diretoria');
insert into Cargo (id, descricao) values (13,'Secretária de Gerência');
insert into Cargo (id, descricao) values (14,'Técnico Administrativo');
insert into Cargo (id, descricao) values (15,'Técnico de Informática');