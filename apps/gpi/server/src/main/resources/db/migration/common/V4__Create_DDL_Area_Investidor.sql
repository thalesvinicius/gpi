CREATE TABLE AnexoPesquisa ( 
	pesquisa_id int NOT NULL,    --  Identificador da pesquisa 
	anexo_id int NOT NULL    --  Identificador do anexo 
)
;

CREATE TABLE PerguntaPesquisa ( 
	id int NOT NULL,    --  número da pergunta 
	pergunta varchar(1000) NOT NULL,    --  Texto da pergunta 
	tipo int NOT NULL,    --  Tipo da resposta, 0 - Comum 1 - Sim/Não 
)
;

CREATE TABLE Pesquisa ( 
	id int identity(1,1)  NOT NULL,    --  Identificador da Pesquisa 
	instrumentoFormalizacao_id int NOT NULL,    --  Identificador do Instrumento de Formalização 
	dataSolicitacao datetime NOT NULL,    --  Data em que a pesquisa foi solicitada 
	dataResposta datetime,    --  Data em que a pesquisa foi respondida 
	comentario text,    --  Comentário 
	contatoExternoResponsavel_id int,    --  Contato Externo responsável pela resposa 
	dataUltimaAlteracao datetime NOT NULL    --  Data da última alteração 
)
;

CREATE TABLE PesquisaPerguntaPesquisa ( 
	pesquisa_id int NOT NULL,    --  Identificador da pesquisa 
	perguntaPesquisa_id int NOT NULL,    --  Identificador da Pergunta 
	resposta int,    --  1 - Excelente, 2 - Bom, 3 - Regular, 4 - Ruim, 5 - Peso da resposta, 6 - Sim, 7 - Não 
	justificativa varchar(256)    --  Justificativa 
)
;

/*
EXEC sp_addextendedproperty 'MS_Description', 'Anexos da pesquisa', 'Schema', dbo, 'table', AnexoPesquisa
;
EXEC sp_addextendedproperty 'MS_Description', 'Identificador da pesquisa', 'Schema', dbo, 'table', AnexoPesquisa, 'column', pesquisa_id
;

EXEC sp_addextendedproperty 'MS_Description', 'Identificador do anexo', 'Schema', dbo, 'table', AnexoPesquisa, 'column', anexo_id
;

EXEC sp_addextendedproperty 'MS_Description', 'Perguntas da Pesquisa de Satisfacao', 'Schema', dbo, 'table', PerguntaPesquisa
;
EXEC sp_addextendedproperty 'MS_Description', 'número da pergunta', 'Schema', dbo, 'table', PerguntaPesquisa, 'column', id
;

EXEC sp_addextendedproperty 'MS_Description', 'Texto da pergunta', 'Schema', dbo, 'table', PerguntaPesquisa, 'column', pergunta
;

EXEC sp_addextendedproperty 'MS_Description', 'Tipo da resposta, 0 - Comum, 1 - Sim/Não', 'Schema', dbo, 'table', PerguntaPesquisa, 'column', tipo
;

EXEC sp_addextendedproperty 'MS_Description', 'Pesquisa de Satisfação', 'Schema', dbo, 'table', Pesquisa
;
EXEC sp_addextendedproperty 'MS_Description', 'Identificador da Pesquisa', 'Schema', dbo, 'table', Pesquisa, 'column', id
;

EXEC sp_addextendedproperty 'MS_Description', 'Identificador do Instrumento de Formalização', 'Schema', dbo, 'table', Pesquisa, 'column', intrumentoFormalizacao_id
;

EXEC sp_addextendedproperty 'MS_Description', 'Data em que a pesquisa foi solicitada', 'Schema', dbo, 'table', Pesquisa, 'column', dataSolicitacao
;

EXEC sp_addextendedproperty 'MS_Description', 'Data em que a pesquisa foi respondida', 'Schema', dbo, 'table', Pesquisa, 'column', dataResposta
;

EXEC sp_addextendedproperty 'MS_Description', 'Comentário', 'Schema', dbo, 'table', Pesquisa, 'column', comentario
;

EXEC sp_addextendedproperty 'MS_Description', 'Contato Externo responsável pela resposa', 'Schema', dbo, 'table', Pesquisa, 'column', contatoExternoResponsavel_id
;

EXEC sp_addextendedproperty 'MS_Description', 'Data da última alteração', 'Schema', dbo, 'table', Pesquisa, 'column', dataUltimaAlteracao
;

EXEC sp_addextendedproperty 'MS_Description', 'Respostas da pesquisa', 'Schema', dbo, 'table', PesquisaPerguntaPesquisa
;
EXEC sp_addextendedproperty 'MS_Description', 'Identificador da pesquisa', 'Schema', dbo, 'table', PesquisaPerguntaPesquisa, 'column', pesquisa_id
;

EXEC sp_addextendedproperty 'MS_Description', 'Identificador da Pergunta', 'Schema', dbo, 'table', PesquisaPerguntaPesquisa, 'column', perguntaPesquisa_id
;

EXEC sp_addextendedproperty 'MS_Description', '1 - Excelente, 2 - Bom, 3 - Regular, 4 - Ruim, 5 - Peso da resposta, 6 - Sim, 7 - Não', 'Schema', dbo, 'table', PesquisaPerguntaPesquisa, 'column', resposta
;

EXEC sp_addextendedproperty 'MS_Description', 'Justificativa', 'Schema', dbo, 'table', PesquisaPerguntaPesquisa, 'column', justificativa
;
*/