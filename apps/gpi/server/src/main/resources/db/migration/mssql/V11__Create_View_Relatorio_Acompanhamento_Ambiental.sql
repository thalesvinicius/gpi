IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.VIEWS
        WHERE TABLE_NAME = 'VIEW_RELATORIO_ACOMPANHAMENTO_AMBIENTAL')
    DROP VIEW VIEW_RELATORIO_ACOMPANHAMENTO_AMBIENTAL
GO

Create view VIEW_RELATORIO_ACOMPANHAMENTO_AMBIENTAL AS 

SELECT *,
(CASE WHEN expectativaOI > expectativaEmissaoLicenca THEN 1
	WHEN expectativaOI < expectativaEmissaoLicenca THEN 0
	ELSE NULL END) as situacaoProcessoId, 
	(CASE WHEN DATEDIFF(day,expectativaOI,expectativaEmissaoLicenca ) < 0 THEN 0 ELSE DATEDIFF(day,expectativaOI,expectativaEmissaoLicenca) END )  as diasForaDoPrazo
FROM (SELECT DISTINCT (proj.id) as id, 

e.nomePrincipal,
proj.nome as projeto,
cid.nome as cidade,
reg.nome as regiao,
reg.id as regiaoId,
cadeia.descricao as cadeiaProdutiva,
cadeia.id as cadeiaProdutivaId,
meio.classeEmpreendimento,

(SELECT STUFF((SELECT ', ' + (CASE etapa2.tipo
         WHEN 1 THEN 'LP'
         WHEN 2 THEN 'LI'
         WHEN 3 THEN 'LO'
         WHEN 4 THEN 'AAF'
         WHEN 5 THEN 'Outros'
      END) FROM EtapaMeioAmbiente etapa2 WHERE etapa2.meioAmbiente_id=meio.id FOR XML PATH('')), 1, 2, '')) as etapas,

meio.dataEntregaEstudos,

-- Classe 1 ou 2 = +30 dias
-- Classe 3 ou 4 = +360 dias
-- Classe 5 ou 6 = +540 dias
(case WHEN (CASE WHEN proj.cadeiaProdutiva_id = 42 THEN cron.inicioOperacaoSucroenergetico ELSE cron.inicioOperacao END) is not null and meio.dataEntregaEstudos is null THEN
	(case meio.classeEmpreendimento 
		WHEN 1 THEN DATEADD(day,30,CAST(CURRENT_TIMESTAMP AS DATE)) 
		WHEN 2 THEN DATEADD(day,30,CAST(CURRENT_TIMESTAMP AS DATE)) 
		WHEN 3 THEN DATEADD(day,360,CAST(CURRENT_TIMESTAMP AS DATE)) 
		WHEN 4 THEN DATEADD(day,360,CAST(CURRENT_TIMESTAMP AS DATE)) 
		WHEN 5 THEN DATEADD(day,540,CAST(CURRENT_TIMESTAMP AS DATE)) 
		WHEN 6 THEN DATEADD(day,540,CAST(CURRENT_TIMESTAMP AS DATE)) END) 
     ELSE 
	(case meio.classeEmpreendimento 
		WHEN 1 THEN DATEADD(day,30,meio.dataEntregaEstudos) 
		WHEN 2 THEN DATEADD(day,30,meio.dataEntregaEstudos) 
		WHEN 3 THEN DATEADD(day,360,meio.dataEntregaEstudos) 
		WHEN 4 THEN DATEADD(day,360,meio.dataEntregaEstudos) 
		WHEN 5 THEN DATEADD(day,540,meio.dataEntregaEstudos) 
		WHEN 6 THEN DATEADD(day,540,meio.dataEntregaEstudos) END) 
	END) as expectativaEmissaoLicenca,

(CASE WHEN proj.cadeiaProdutiva_id = 42 THEN cron.inicioOperacaoSucroenergetico ELSE cron.inicioOperacao END) as expectativaOI,

estagio.descricao as estagioAtual,
estagio.id as estagioId,
situacaoProj.descricao as situacaoAtual,
situacaoProj.id as situacaoId,
usuarioInterno.matricula as analista,
usuarioInterno.id as analistaId,
dep.sigla as departamento,
dep.id as departamentoId, 
etapaMeio.tipo as etapaMeioAmbienteId

FROM PROJETO proj

LEFT JOIN EMPRESA e ON e.id=proj.empresa_id
INNER JOIN CadeiaProdutiva cadeia ON cadeia.id= proj.cadeiaProdutiva_id
LEFT JOIN Localizacao loc ON proj.id=loc.projeto_id
LEFT JOIN Municipio cid ON cid.id=loc.municipio_id
LEFT JOIN RegiaoPlanejamento reg ON reg.id=loc.regiaoPlanejamento_id 
LEFT JOIN Cronograma cron ON cron.projeto_id=proj.id 
LEFT JOIN EstagioProjeto estagioProjeto ON proj.estagioAtual_id=estagioProjeto.estagio_id
LEFT JOIN Estagio estagio ON estagioProjeto.estagio_id=estagio.id
LEFT JOIN SituacaoProjeto situacaoProj ON situacaoProj.id=proj.situacaoAtual_id
LEFT JOIN UsuarioInterno usuarioInterno ON usuarioInterno.id=proj.usuarioResponsavel_id
LEFT JOIN Departamento dep ON dep.id=usuarioInterno.departamento_id
INNER JOIN MeioAmbiente meio ON meio.projeto_id=proj.id
LEFT JOIN EtapaMeioAmbiente etapaMeio ON etapaMeio.meioAmbiente_id=meio.id) as ACOMP;