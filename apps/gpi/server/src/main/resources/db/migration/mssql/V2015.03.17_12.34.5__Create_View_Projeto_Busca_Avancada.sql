IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.VIEWS
        WHERE TABLE_NAME = 'VIEW_PROJETO_BUSCA_AVANCADA')
    DROP VIEW VIEW_PROJETO_BUSCA_AVANCADA
GO

Create view VIEW_PROJETO_BUSCA_AVANCADA AS

select distinct 
p.ultimaVersao,
p.id,
p.nome,
p.descricao,

CASE p.prospeccaoAtiva
WHEN 1 THEN 'Sim'
WHEN 0 THEN 'Não'
ELSE ''
END as prospeccaoAtiva,
p.prospeccaoAtiva as prospeccaoAtiva_id,

CASE p.tipo
WHEN 1 THEN 'Expansão'
WHEN 2 THEN 'Novo'
ELSE ''
END as tipo,
p.tipo as tipo_id,
cp.descricao as cadeiaProdutiva,
p.usuarioInvestimento_id,
p.cadeiaProdutiva_id,
cp.usuarioResponsavel_id as responsavelCadeiaProdutiva_id,
p.empresa_id,
p.unidade_id,
p.familia_id,
Convert(varchar(10),CONVERT(date,p.dataCadastro,106),103) as dataCadastro,
p.informacaoPrimeiroContato,
est.descricao as estagioAtual,
p.estagioAtual_id,
p.situacaoAtual_id,
Convert(varchar(10),CONVERT(date,ep.dataInicioEstagio,106),103) as dataEstagioAtual,
sp.descricao as situacaoAtual,
Convert(varchar(10),CONVERT(date,hsp.data,106),103) as dataSituacaoAtual,
resp.nome as responsavel,
Convert(varchar(10),CONVERT(date,ip.dataInicioEstagio,106),103) as dataRealIP,
Convert(varchar(10),CONVERT(date,pp.dataInicioEstagio,106),103) as dataRealPP,
vifba.dataPrevistaAssinatura as dataPrevistaDF,
Convert(varchar(10),CONVERT(date,df.dataInicioEstagio,106),103) as dataRealDF,
CASE cp.id
WHEN null THEN null
WHEN 42 THEN Convert(varchar(10),CONVERT(date,cron.inicioImplantacaoAgricola,106),103) ELSE Convert(varchar(10),CONVERT(date,cron.inicioImplantacao,106),103)
END as dataPrevistaII,
Convert(varchar(10),CONVERT(date,ii.dataInicioEstagio,106),103) as dataRealII,
CASE cp.id
WHEN null THEN null
WHEN 42 THEN Convert(varchar(10),CONVERT(date,cron.inicioOperacaoSucroenergetico,106),103) ELSE Convert(varchar(10),CONVERT(date,cron.inicioOperacao,106),103)
END as dataPrevistaOI,
Convert(varchar(10),CONVERT(date,oi.dataInicioEstagio,106),103) as dataRealOI,
Convert(varchar(10),CONVERT(date,cc.dataInicioEstagio,106),103) as dataRealCC,
cp.departamento_id as gerencia_id,
Convert(varchar(10), p.familia_id) + '.' + Convert(varchar(10), p.versao) as versaocompleta,
CASE 
WHEN tab.projeto_id is not null THEN 1
WHEN tab.projeto_id is null THEN 0
END as necessidadeFinanciamentoBDMG,
ma.classeEmpreendimento,
ma.id as meioAmbiente_id

from projeto p 
inner join CadeiaProdutiva cp on p.cadeiaProdutiva_id = cp.id
inner join EstagioProjeto ep on p.id = ep.projeto_id and ep.ativo = 1
inner join Estagio est on p.estagioAtual_id = est.id
inner join HistoricoSituacaoProjeto hsp on p.historicoSituacaoProjeto_id = hsp.id
inner join SituacaoProjeto sp on hsp.situacaoProjeto_id = sp.id
left join Usuario resp on p.usuarioInvestimento_id = resp.id
left join Cronograma cron on p.id = cron.projeto_id 
left join MeioAmbiente ma on p.id = ma.projeto_id
left join EstagioProjeto ip on p.id = ip.projeto_id and ip.estagio_id = 3
left join EstagioProjeto pp on p.id = pp.projeto_id and pp.estagio_id = 5
left join EstagioProjeto df on p.id = df.projeto_id and df.estagio_id = 1
left join EstagioProjeto ii on p.id = ii.projeto_id and ii.estagio_id = 2
left join EstagioProjeto oi on p.id = oi.projeto_id and oi.estagio_id = 4
left join EstagioProjeto cc on p.id = cc.projeto_id and cc.estagio_id = 6
left join VIEW_INSTRUMENTO_FORMALIZACAO_BUSCA_AVANCADA vifba on p.id = vifba.projeto_id
left join (select distinct fin.projeto_id from Financeiro fin inner join UsoFonte uf on fin.id = uf.financeiro_id and uf.tipo = 16 /*BDMG*/) as tab on tab.projeto_id = p.id;