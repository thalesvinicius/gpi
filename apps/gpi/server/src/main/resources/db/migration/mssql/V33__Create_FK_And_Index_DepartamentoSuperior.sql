ALTER TABLE Departamento ADD CONSTRAINT FK_Departamento_Departamento
	FOREIGN KEY (departamentoSuperior_id) REFERENCES Departamento(id);

create index IX_Departamento_DepartamentoSuperior_id on Departamento (departamentoSuperior_id);