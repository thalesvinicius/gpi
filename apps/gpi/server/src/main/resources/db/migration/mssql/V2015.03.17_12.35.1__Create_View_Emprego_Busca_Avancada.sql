IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.VIEWS
        WHERE TABLE_NAME = 'VIEW_EMPREGO_BUSCA_AVANCADA')
    DROP VIEW VIEW_EMPREGO_BUSCA_AVANCADA
GO

Create view VIEW_EMPREGO_BUSCA_AVANCADA AS
-- Empregos diretos permanentes  

SELECT
projeto_id,
sum(direto) AS empregoDiretoPermanenteTotalPrevisto,
sum(diretoRealizado) AS empregoDiretoPermanenteTotalReal
From Emprego
where Emprego.tipo <> 1
  and Emprego.tipo <> 3
  and Emprego.tipo <> 5
  and Emprego.tipo <> 6
Group by projeto_id;