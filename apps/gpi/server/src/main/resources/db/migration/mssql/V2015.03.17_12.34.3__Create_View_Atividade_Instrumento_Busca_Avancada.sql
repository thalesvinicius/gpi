IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.VIEWS
        WHERE TABLE_NAME = 'VIEW_ATIVIDADE_INSTRUMENTO_BUSCA_AVANCADA')
    DROP VIEW VIEW_ATIVIDADE_INSTRUMENTO_BUSCA_AVANCADA
GO

Create view VIEW_ATIVIDADE_INSTRUMENTO_BUSCA_AVANCADA AS

select 
ai.id,
ai.atividadeLocal_id,
al.local_id,
ai.instrumentoFormalizacao_id
from 
AtividadeInstrumento ai
inner join AtividadeLocal al on ai.atividadeLocal_id = al.id;