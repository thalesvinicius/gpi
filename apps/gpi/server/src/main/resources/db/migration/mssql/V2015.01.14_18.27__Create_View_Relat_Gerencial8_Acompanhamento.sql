IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.VIEWS
        WHERE TABLE_NAME = 'VIEW_RELATORIO_ACOPANHAMENTO_GERAL')
    DROP VIEW VIEW_RELATORIO_ACOPANHAMENTO_GERAL
GO 


CREATE VIEW VIEW_RELATORIO_ACOPANHAMENTO_GERAL AS 

SELECT DISTINCT E.nomePrincipal AS NOME_EMPRESA, 
E.id AS ID_EMPRESA, 
P.id AS ID_projeto, 
p.nome AS NOME_PROJETO,
en.cidade as CIDADE, 
cp.id as ID_CADEIA_PRODUTIVA, 
rp.nome as regiao_planejamento,
CP.descricao AS CADEIA_PRODUTIVA,
IP.ano AS ANO, 
es.id AS estagioId,
es.descricao as ESTAGIO_PROJETO, 
P.situacaoAtual_id as statusId, 
st.descricao as Status,
u.nome as ANALISTA, 
u.id as analistaId,
d.sigla as GERENCIA,
d.id as gerenciaId,
rp.id as regiaoPlanejamentoId,
SUM(ISNULL(IP.valor,0))/1000 AS INVESTIMENTO_PREVISTO, 
SUM(ISNULL(IP.valorRealizado,0))/1000 AS INVESTIMENTO_REALIZADO , 
SUM(ISNULL(FP.valor,0))/1000 AS FATURAMENTO_PREVISTO , 
SUM(ISNULL(FP.valorRealizado,0))/1000 AS FATURAMENTO_REALIZADO,
SUM(ISNULL(EP.DIRETOS,0)) AS PERMANENTES_DIRETOS, 
SUM(ISNULL(EP.REALIZADOS,0)) AS PERMANENTES_DIRETOS_REALIZADOS

FROM Empresa E inner join Projeto P on P.empresa_id = E.id
inner join Endereco en on en.id = e.endereco_id
INNER JOIN Localizacao l on l.projeto_id = p.id
inner join RegiaoPlanejamento rp on rp.id  = l.regiaoPlanejamento_id
inner join CadeiaProdutiva cp on P.cadeiaProdutiva_id = cp.id
inner join EstagioProjeto esp on esp.projeto_id = p.id
INNER JOIN Estagio es on es.id = esp.estagio_id
inner join Financeiro f on p.id = f.projeto_id
inner join UsuarioInterno a on a.id = p.usuarioResponsavel_id
INNER JOIN Usuario u on u.id = a.id
inner join Departamento d on d.id = A.departamento_id
inner join SituacaoProjeto st on st.id = p.situacaoAtual_id
inner join InvestimentoPrevisto ip on ip.financeiro_id = f.id
inner join FaturamentoPrevisto fp on fp.financeiro_id = f.id
inner join (SELECT em.projeto_id, em.ano, SUM(em.direto) AS DIRETOS, SUM(em.diretoRealizado) AS REALIZADOS from Emprego em
			WHERE em.tipo in (2,4,7,8) group by em.projeto_id, em.ano) ep on ep.projeto_id = p.id
group by st.descricao, e.nomePrincipal,e.id,p.id,p.nome,cp.id,rp.id,cp.descricao,ip.ano,p.situacaoAtual_id,u.nome,d.id,en.cidade,
rp.nome,es.descricao,d.descricao,d.sigla,u.id,es.id ;