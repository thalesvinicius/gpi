IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.VIEWS
        WHERE TABLE_NAME = 'VIEW_INFORMACAO_ADICIONAL_BUSCA_AVANCADA')
    DROP VIEW VIEW_INFORMACAO_ADICIONAL_BUSCA_AVANCADA
GO

Create view VIEW_INFORMACAO_ADICIONAL_BUSCA_AVANCADA AS

select 
ia.id,
CASE ia.tipoContato
WHEN 1 THEN 'E-mail'
WHEN 2 THEN 'Reunião'
WHEN 3 THEN 'Telefone'
WHEN 4 THEN 'Outros'
ELSE ''
END as tipoContato,
ia.tipoContato as tipoContato_id,
ia.empresa_id,
ia.assunto,
ia.descricao,
Convert(varchar(10),CONVERT(date,ia.data,106),103) as data,
ia.data as data_full
from InformacaoAdicional ia

