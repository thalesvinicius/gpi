IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.VIEWS
        WHERE TABLE_NAME = 'VIEW_RELATORIO_ACOPANHAMENTO_ANUAL')
    DROP VIEW VIEW_RELATORIO_ACOPANHAMENTO_ANUAL
GO 

CREATE VIEW VIEW_RELATORIO_ACOPANHAMENTO_ANUAL AS 
select distinct projeto.familia_id,
RelatorioAcompanhamento.ano,
projeto.nome nomeProjeto,
empresa.nomePrincipal,
usuarioExterno1_.email,
RelatorioAcompanhamento.situacaoAtual_id,
projeto.estagioAtual_id,
RelatorioAcompanhamento.dataValidacao,
RelatorioAcompanhamento.dataUltimaAlteracao,
RelatorioAcompanhamento.dataInicioPrimeiraCampanha,
RelatorioAcompanhamento.dataFimPrimeiraCampanha,
RelatorioAcompanhamento.dataInicioSegundaCampanha,
RelatorioAcompanhamento.dataFimSegundaCampanha,
usuarioint4_1_.nome nomeUsuario
from Projeto projeto 
left outer join Empresa empresa on projeto.empresa_id=empresa.id 
left outer join UsuarioExterno usuarioExterno on empresa.usuarioExternoResponsavel_id=usuarioExterno.id 
left outer join Usuario usuarioExterno1_ on usuarioExterno.id=usuarioExterno1_.id 
left outer join RelatorioAcompanhamento RelatorioAcompanhamento on projeto.familia_id=RelatorioAcompanhamento.familia_id 
left outer join UsuarioInterno usuarioint4_ on projeto.usuarioInvestimento_id=usuarioint4_.id 
left outer join Usuario usuarioint4_1_ on usuarioint4_.id=usuarioint4_1_.id 
left outer join Departamento departamen5_ on usuarioint4_.departamento_id=departamen5_.id
inner join PROJETOINSTRUMENTOFORMALIZACAO instrument6_ on projeto.id=instrument6_.projeto_id
inner join InstrumentoFormalizacao instrument7_ on instrument6_.instrumentoFormalizacao_id=instrument7_.id
where projeto.familia_id=
(
   select
   viewprojet8_.familia_id
   from VIEW_PROJETO_ULT_VERSAO_ATIVA_SUSPENSA viewprojet8_
   where viewprojet8_.familia_id=projeto.familia_id
)
and instrument7_.tipo=4
and instrument7_.situacaoAtual_id=1