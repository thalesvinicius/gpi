CREATE TABLE AnexoRelatorioAcompanhamento ( 
	relatorioAcompanhamento_id int NOT NULL,    --  Identificador do Relatório de Acompanhamento 
	anexo_id int NOT NULL    --  Identificador do Anexo 
);

CREATE TABLE RelatorioAcompanhamento ( 
	id int identity(1,1)  NOT NULL,    --  Identificador do Relatório 
	projeto_id int NOT NULL,    --  Identificador do Projeto 
	ano int NOT NULL,    --  Ano referência do relatório 
	dataInicioPrimeiraCampanha datetime,    --  Data de Início da Primeira Campanha 
	dataFimPrimeiraCampanha datetime,    --  Data Final da Segunda Campanha 
	dataInicioSegundaCampanha datetime,    --  Data Início da Segunda Campanha 
	dataFimSegundaCampanha datetime,    --  Data final da segunda campanha 
	termoConfiabilidade varchar(255) NOT NULL,    --  Termo de Confiabilidade ao qual o usuário aceitou 
	aceiteTermo bit NOT NULL,    --  Aceite do termo de confiabilidade 
	observacao text,    --  Observação sobre o preenchimento do relatório 
	nomeResponsavelRelatorio varchar(100),    --  Nome do Responsável pelo preenchimento 
	telefoneResponsavelRelatorio varchar(18),    --  Telefone do Responsável pelo Relatório 
	emailResponsavelRelatorio varchar(50),    --  E-mail do Responsável pelo Relatório 
	usuarioResponsavel_id int NOT NULL,    --  Usuário logado responsável pelas informações incluídas ou alteradas 
	dataUltimaAlteracao datetime NOT NULL    --  Data da Última Alteração 
);

CREATE TABLE RelatorioAcompanhamentoSituacao ( 
	relatorioAcompanhamento_id int NOT NULL,    --  Identificador do relatório de acompanhamento 
	situacaoRelatorio_id int NOT NULL    --  Identificador da Situação 
);

CREATE TABLE SituacaoRelatorio ( 
	id int NOT NULL,    --  Código ENUM da Situação 
	descricao varchar(100) NOT NULL    --  Descrição da Situação 
);

--  Create Primary Key Constraints 
ALTER TABLE RelatorioAcompanhamento ADD CONSTRAINT PK_RelatorioAcompanhamento 
	PRIMARY KEY CLUSTERED (id);

ALTER TABLE SituacaoRelatorio ADD CONSTRAINT PK_SituacaoRelatorio 
	PRIMARY KEY CLUSTERED (id);

--  Create Foreign Key Constraints 
ALTER TABLE AnexoRelatorioAcompanhamento ADD CONSTRAINT FK_AnexoRelatorioAcompanhamento_Anexo 
	FOREIGN KEY (anexo_id) REFERENCES Anexo (id);

ALTER TABLE AnexoRelatorioAcompanhamento ADD CONSTRAINT FK_AnexoRelatorioAcompanhamento_RelatorioAcompanhamento 
	FOREIGN KEY (relatorioAcompanhamento_id) REFERENCES RelatorioAcompanhamento (id);

ALTER TABLE RelatorioAcompanhamento ADD CONSTRAINT FK_RelatorioAcompanhamento_Projeto 
	FOREIGN KEY (projeto_id) REFERENCES Projeto (id);

ALTER TABLE RelatorioAcompanhamentoSituacao ADD CONSTRAINT FK_RelatorioAcompanhamentoSituacao_RelatorioAcompanhamento 
	FOREIGN KEY (relatorioAcompanhamento_id) REFERENCES RelatorioAcompanhamento (id);

ALTER TABLE RelatorioAcompanhamentoSituacao ADD CONSTRAINT FK_RelatorioAcompanhamentoSituacao_SituacaoRelatorio 
	FOREIGN KEY (situacaoRelatorio_id) REFERENCES SituacaoRelatorio (id);


ALTER TABLE Cronograma add inicioTratamentoTributarioPrevisto datetime;    --  Data de Início do Tratamento Tributário Previsto 
ALTER TABLE Cronograma add inicioTratamentoTributarioRealizado datetime;    --  Data de Início do Tratamento Tributário Realizado 

ALTER TABLE Emprego add diretoRealizado int;    --  Empregos Diretos Realizados, preenchido pelo relatório anual de acompanhamento 

ALTER TABLE FaturamentoPrevisto add valorRealizado decimal(16,2);    --  Valor Realizado informado pelo relatório anual de acompanhamento 

ALTER TABLE InvestimentoPrevisto add valorRealizado decimal(16,2);    --  Valor Realizado informado pelo relatório anual de acompanhamento 