CREATE TABLE SituacaoProjeto ( 
	id int NOT NULL,
	descricao varchar(100) NOT NULL
);

alter table projeto add historicoSituacaoProjeto_id int null;

alter table HistoricoSituacaoProjeto add situacaoProjeto_id int null;

update HistoricoSituacaoProjeto set situacaoProjeto_id = situacao;

alter table HistoricoSituacaoProjeto drop column situacao;

alter table HistoricoSituacaoProjeto alter column situacaoProjeto_id int not null;