
INSERT INTO perguntaPesquisa(id, pergunta, tipo) VALUES
(1, '1 - A qualidade do atendimento prestado pelo INDI foi:', 1),
(2, '2 - Como você julga o apoio do INDI em sua decisão de investir em Minas Gerais?', 1),
(3, '3 - O tempo para atendimento pelo INDI foi:', 1),
(4, '4 - Como você julga a atuação do INDI junto ao Estado para o alcance de seus objetivos?', 1),
(5, '5 - Você indicaria os serviços do INDI a outro Investidor?', 0);
