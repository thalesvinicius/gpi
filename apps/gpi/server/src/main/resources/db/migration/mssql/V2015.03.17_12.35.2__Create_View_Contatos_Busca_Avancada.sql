IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.VIEWS
        WHERE TABLE_NAME = 'VIEW_CONTATOS_BUSCA_AVANCADA')
    DROP VIEW VIEW_CONTATOS_BUSCA_AVANCADA
GO

Create view VIEW_CONTATOS_BUSCA_AVANCADA AS

select 
ue.id,
u.nome,
ue.cargoExterno,
ue.empresa_id,
ue.unidade_id,
Convert(varchar(10),CONVERT(date,ue.dataBloqueioTemporario,106),103) as dataBloqueioTemporario,
u.email,
u.telefone,
u.telefone2,
CASE u.mailing
WHEN 1 THEN 'Sim'
WHEN 0 THEN 'Não'
ELSE ''
END as mailing,
isnull(und.nome, e.nomePrincipal) as empresaunidade,

CASE u.ativo
WHEN 1 THEN 'Ativo'
WHEN 0 THEN 'Inativo'
ELSE ''
END as status

from usuarioexterno ue
inner join Usuario u on ue.id = u.id
left join empresa e on ue.empresa_id = e.id
left join UnidadeEmpresa und on ue.unidade_id = und.id
left join Cargo c on u.cargo_id = c.id;