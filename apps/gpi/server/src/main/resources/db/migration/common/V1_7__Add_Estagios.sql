--Estagio
insert into ESTAGIO (id, descricao) values(1, 'Decisão Formalizada');
insert into ESTAGIO (id, descricao) values(2, 'Implantação Iniciada');
insert into ESTAGIO (id, descricao) values(3, 'Início de Projeto');
insert into ESTAGIO (id, descricao) values(4, 'Operação Iniciada');
insert into ESTAGIO (id, descricao) values(5, 'Projeto Promissor');
insert into ESTAGIO (id, descricao) values(6, 'Compromisso Cumprido');