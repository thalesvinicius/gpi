--CNAEEmpresa
IF EXISTS (select cons.CONSTRAINT_NAME FROM information_schema.TABLE_CONSTRAINTS cons
where cons.CONSTRAINT_NAME = 'FK_CNAEEmpresa_Empresa')
	ALTER TABLE CNAEEmpresa DROP CONSTRAINT FK_CNAEEmpresa_Empresa;

ALTER TABLE CNAEEmpresa ADD CONSTRAINT FK_CNAEEmpresa_Empresa 
	FOREIGN KEY (empresa_id) REFERENCES Empresa (id)
	ON DELETE CASCADE;

--InformacaoAdicional
IF EXISTS (select cons.CONSTRAINT_NAME FROM information_schema.TABLE_CONSTRAINTS cons
where cons.CONSTRAINT_NAME = 'FK_InformacaoAdicional_Empresa')
	ALTER TABLE InformacaoAdicional DROP CONSTRAINT FK_InformacaoAdicional_Empresa;

ALTER TABLE InformacaoAdicional ADD CONSTRAINT FK_InformacaoAdicional_Empresa 
	FOREIGN KEY (empresa_id) REFERENCES Empresa (id)
	ON DELETE CASCADE;

--AnexoInformacaoAdicional
IF EXISTS (select cons.CONSTRAINT_NAME FROM information_schema.TABLE_CONSTRAINTS cons
where cons.CONSTRAINT_NAME = 'FK_AnexoInformacaoAdicional_InformacaoAdicional')
ALTER TABLE AnexoInformacaoAdicional DROP CONSTRAINT FK_AnexoInformacaoAdicional_InformacaoAdicional;

ALTER TABLE AnexoInformacaoAdicional ADD CONSTRAINT FK_AnexoInformacaoAdicional_InformacaoAdicional 
	FOREIGN KEY (InformacaoAdicional_id) REFERENCES InformacaoAdicional (id)
	ON DELETE CASCADE;

--UnidadeEmpresa
IF EXISTS (select cons.CONSTRAINT_NAME FROM information_schema.TABLE_CONSTRAINTS cons
where cons.CONSTRAINT_NAME = 'FK_UnidadeEmpresa_Empresa')
ALTER TABLE UnidadeEmpresa DROP CONSTRAINT FK_UnidadeEmpresa_Empresa;

ALTER TABLE UnidadeEmpresa ADD CONSTRAINT FK_UnidadeEmpresa_Empresa 
	FOREIGN KEY (empresa_id) REFERENCES Empresa (id)
	ON DELETE CASCADE;

--UsuarioExterno
IF EXISTS (select cons.CONSTRAINT_NAME FROM information_schema.TABLE_CONSTRAINTS cons
where cons.CONSTRAINT_NAME = 'FK_ContatoExterno_Empresa')
ALTER TABLE UsuarioExterno DROP CONSTRAINT FK_ContatoExterno_Empresa;

ALTER TABLE UsuarioExterno ADD CONSTRAINT FK_ContatoExterno_Empresa 
	FOREIGN KEY (empresa_id) REFERENCES Empresa (id)
	ON DELETE CASCADE;

	