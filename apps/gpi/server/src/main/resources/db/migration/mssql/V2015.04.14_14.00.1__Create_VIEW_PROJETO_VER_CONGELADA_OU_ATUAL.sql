IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.VIEWS
        WHERE TABLE_NAME = 'VIEW_PROJETO_VER_CONGELADA_OU_ATUAL')
    DROP VIEW VIEW_PROJETO_VER_CONGELADA_OU_ATUAL
GO

Create View VIEW_PROJETO_VER_CONGELADA_OU_ATUAL AS
 SELECT
   tab.isLastVersion, p.*
   FROM
   (
      SELECT
      0 as isLastVersion, p.id, p.familia_id
      FROM projeto p
      WHERE p.ultimaversao = 1
      AND p.versao = 0
      AND situacaoatual_id <> 4
      UNION
      SELECT
      0 as isLastVersion, p.id, p.familia_id
      FROM projeto p
      WHERE p.ultimaversao = 0
      AND situacaoatual_id <> 4
      AND p.id in
      (
         SELECT
         top(1) p2.id
         FROM projeto p2
         WHERE p2.ultimaversao = 0
         AND p2.situacaoatual_id <> 4
         and p2.familia_id = p.familia_id
         order by p2.versao desc
      )
      UNION
      SELECT
      1 as isLastVersion, p.id, p.familia_id
      FROM projeto p
      WHERE p.ultimaversao = 1
      AND situacaoatual_id <> 4
   )
   as tab
   INNER JOIN projeto p ON tab.id = p.id;