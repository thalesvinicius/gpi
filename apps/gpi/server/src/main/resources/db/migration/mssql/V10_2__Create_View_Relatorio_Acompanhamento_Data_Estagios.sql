IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.VIEWS
        WHERE TABLE_NAME = 'VIEW_RELATORIO_ACOMPANHAMENTO_DATAS_ESTAGIOS')
    DROP VIEW VIEW_RELATORIO_ACOMPANHAMENTO_DATAS_ESTAGIOS
GO

Create view VIEW_RELATORIO_ACOMPANHAMENTO_DATAS_ESTAGIOS AS

select distinct
ROW_NUMBER() over (order by emp.id asc) as id,
emp.id as 'empresa_id',
pro.id as 'projeto_id',
cadpro.id as 'cadeiaProdutiva_id',
usuInv.id as 'usuarioResponsavel_id',
repla.id as 'regiaoPlanejamento_id',
dep.id as 'departamento_id',
pro.ultimaVersao as ultimaVersao,
isnull(emp.nomePrincipal, '') as 'nomeEmpresa',
pro.nome as nomeProjeto,
isnull(mun.nome, isnull(loc.localizacaoSecundaria, '')) as cidade,
isnull(repla.nome, '') as 'regiaoPlanejamento',
(CAST ( sum(invPrev.valor) AS decimal (16,2))) as valorInvestimento,
(CAST ( sum(fatIni.valor) AS decimal (16,2))) as faturamentoInicial,
(CAST ( sum(fatFin.valor) AS decimal (16,2))) as faturamentoFinal,
sum(empgo.direto) as empregosDiretos,
sum(empgo.indireto) as empregosIndiretos,
cadpro.descricao as 'cadeiaProdutiva',
pro.situacaoAtual_id as 'situacaoAtual',
pro.estagioAtual_id as 'estagioAtual',
usuInv.nome as 'analista',
dep.descricao as 'departamento',
min(instForm.dataAssinaturaPrevista) as dataPrevistaDF,
isnull(cron.inicioImplantacaoIndustrial, isnull(cron.inicioImplantacaoAgricola, cron.inicioImplantacao)) as dataPrevistaII,
isnull(cron.inicioOperacaoSucroenergetico, cron.inicioOperacao) as dataPrevistaOI,
max(estproIP.dataInicioEstagio) as dataRealIP,
max(estproPP.dataInicioEstagio) as dataRealPP,
max(estproDF.dataInicioEstagio) as dataRealDF,
max(estproII.dataInicioEstagio) as dataRealII,
max(estproOI.dataInicioEstagio) as dataRealOI,
max(estproCC.dataInicioEstagio) as dataRealCC

from projeto pro
inner join empresa emp on emp.id = pro.empresa_id
inner join CadeiaProdutiva cadpro on pro.cadeiaProdutiva_id = cadpro.id
left join localizacao loc on pro.id = loc.id
left join municipio mun on loc.municipio_id = mun.id
left join RegiaoPlanejamento repla on loc.regiaoPlanejamento_id = repla.id
left join Cronograma cron on pro.id= cron.projeto_id
left join Financeiro fin on fin.projeto_id = pro.id
left join InvestimentoPrevisto invPrev on fin.id = invPrev.financeiro_id
left join FaturamentoPrevisto fatIni on fin.id = fatIni.financeiro_id and fatIni.id = (select top(1) id from FaturamentoPrevisto where financeiro_id = fin.id order by ano asc)
left join FaturamentoPrevisto fatFin on fin.id = fatFin.financeiro_id and fatFin.id = (select top(1) id from FaturamentoPrevisto where financeiro_id = fin.id order by ano desc)
left join Emprego empgo on empgo.projeto_id = pro.id
left join EstagioProjeto estproIP on pro.id = estproIP.projeto_id and estproIP.estagio_id = 3 
left join EstagioProjeto estproPP on pro.id = estproPP.projeto_id and estproPP.estagio_id = 5 
left join EstagioProjeto estproDF on pro.id = estproDF.projeto_id and estproDF.estagio_id = 1 
left join EstagioProjeto estproII on pro.id = estproII.projeto_id and estproII.estagio_id = 2 
left join EstagioProjeto estproOI on pro.id = estproOI.projeto_id and estproOI.estagio_id = 4 
left join EstagioProjeto estproCC on pro.id = estproCC.projeto_id and estproCC.estagio_id = 6 
left join Usuario usuInv on pro.usuarioInvestimento_id = usuInv.id
left join UsuarioInterno ui on usuInv.id = ui.id
left join Departamento dep on ui.departamento_id = dep.id
left join ProjetoInstrumentoFormalizacao proInstForm on pro.id = proInstForm.projeto_id
left join InstrumentoFormalizacao instForm on proInstForm.instrumentoFormalizacao_id = instForm.id and instForm.id = (select min(pif.InstrumentoFormalizacao_id) from ProjetoInstrumentoFormalizacao pif where pif.projeto_id = pro.id)
group by emp.id, pro.id, cadpro.id, usuInv.id, repla.id, dep.id, pro.ultimaVersao, emp.nomePrincipal, pro.nome, mun.nome, loc.localizacaoSecundaria, repla.nome, fatIni.valor, fatFin.valor, cadpro.descricao, pro.situacaoAtual_id, pro.estagioAtual_id, usuInv.nome, dep.descricao, cron.inicioImplantacaoIndustrial, cron.inicioImplantacaoAgricola, cron.inicioImplantacao, cron.inicioOperacaoSucroenergetico, cron.inicioOperacao;