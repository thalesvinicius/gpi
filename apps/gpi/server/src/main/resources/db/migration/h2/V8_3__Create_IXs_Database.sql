-- Create Indexes Constraints
create index IX_AccessToken_user_id on AccessToken (user_id);
create index IX_AnexoInformacaoAdicional_anexo_id on AnexoInformacaoAdicional (anexo_id);
create index IX_AnexoInformacaoAdicional_informacaoAdicional_id on AnexoInformacaoAdicional (informacaoAdicional_id);
create index IX_AnexoLocalizacao_anexo_id on AnexoLocalizacao (anexo_id);
create index IX_AnexoLocalizacao_localizacao_id on AnexoLocalizacao (localizacao_id);
create index IX_AnexoPesquisa_anexo_id on AnexoPesquisa (anexo_id);
create index IX_AnexoPesquisa_pesquisa_id on AnexoPesquisa (pesquisa_id);
create index IX_AnexoProjeto_anexo_id on AnexoProjeto (anexo_id);
create index IX_AnexoProjeto_projeto_id on AnexoProjeto (projeto_id);
create index IX_AtividadeInstrumento_anexo_id on AtividadeInstrumento (anexo_id);
create index IX_AtividadeInstrumento_atividadeLocal_id on AtividadeInstrumento (atividadeLocal_id);
create index IX_AtividadeInstrumento_instrumentoFormalizacao_id on AtividadeInstrumento (instrumentoFormalizacao_id);
create index IX_AtividadeLocal_local_id on AtividadeLocal (local_id);
create index IX_AtividadeProjeto_anexo_id on AtividadeProjeto (anexo_id);
create index IX_AtividadeProjeto_atividadeLocal_id on AtividadeProjeto (atividadeLocal_id);
create index IX_AtividadeProjeto_projeto_id on AtividadeProjeto (projeto_id);
create index IX_AtividadeTemplate_atividadeLocal_id on AtividadeTemplate (atividadeLocal_id);
create index IX_AtividadeTemplate_template_id on AtividadeTemplate (template_id);
create index IX_CadeiaProdutiva_departamento_id on CadeiaProdutiva (departamento_id);
create index IX_CNAE_CNAESuperior_id on CNAE (CNAESuperior_id);
create index IX_CNAEEmpresa_CNAE_id on CNAEEmpresa (CNAE_id);
create index IX_CNAEEmpresa_empresa_id on CNAEEmpresa (empresa_id);
create index IX_Concorrente_insumoProduto_id on Concorrente (insumoProduto_id);
create index IX_Cronograma_projeto_id on Cronograma (projeto_id);
create index IX_Emprego_projeto_id on Emprego (projeto_id);
create index IX_Empresa_anexo_id on Empresa (anexo_id);
create index IX_Empresa_cadeiaProdutiva_id on Empresa (cadeiaProdutiva_id);
create index IX_Empresa_naturezaJuridica_id on Empresa (naturezaJuridica_id);
create index IX_Endereco_municipio_id on Endereco (municipio_id);
create index IX_Endereco_pais_id on Endereco (pais_id);
create index IX_Endereco_uf_id on Endereco (uf_id);
create index IX_EnergiaContratada_infraestrutura_id on EnergiaContratada (infraestrutura_id);
create index IX_EstagioProjeto_estagio_id on EstagioProjeto (estagio_id);
create index IX_EstagioProjeto_projeto_id on EstagioProjeto (projeto_id);
create index IX_EtapaMeioAmbiente_meioAmbiente_id on EtapaMeioAmbiente (meioAmbiente_id);
create index IX_FaturamentoAnterior_financeiro_id on FaturamentoAnterior (financeiro_id);
create index IX_FaturamentoPrevisto_financeiro_id on FaturamentoPrevisto (financeiro_id);
create index IX_Financeiro_projeto_id on Financeiro (projeto_id);
create index IX_FinanceiroAnexo_anexo_id on FinanceiroAnexo (anexo_id);
create index IX_FinanceiroAnexo_financeiro_id on FinanceiroAnexo (financeiro_id);
create index IX_HistoricoSituacaoInstrumentoFormalizacao_instrumentoFormalizacao_id on HistoricoSituacaoInstrumentoFormalizacao (instrumentoFormalizacao_id);
create index IX_HistoricoSituacaoInstrumentoFormalizacao_situacaoInstrumentoFormalizacao_id on HistoricoSituacaoInstrumentoFormalizacao (situacaoInstrumentoFormalizacao_id);
create index IX_HistoricoSituacaoProjeto_projeto_id on HistoricoSituacaoProjeto (projeto_id);
create index IX_HistoricoSituacaoProjeto_situacaoProjeto_id on HistoricoSituacaoProjeto (situacaoProjeto_id);
create index IX_InformacaoAdicional_empresa_id on InformacaoAdicional (empresa_id);
create index IX_Infraestrutura_projeto_id on Infraestrutura (projeto_id);
create index IX_Insumo_insumoProduto_id on Insumo (insumoProduto_id);
create index IX_Insumo_ncm_id on Insumo (ncm_id);
create index IX_InsumoProduto_projeto_id on InsumoProduto (projeto_id);
create index IX_InvestimentoPrevisto_financeiro_id on InvestimentoPrevisto (financeiro_id);
create index IX_Localizacao_microRegiao_id on Localizacao (microRegiao_id);
create index IX_Localizacao_municipio_id on Localizacao (municipio_id);
create index IX_Localizacao_projeto_id on Localizacao (projeto_id);
create index IX_Localizacao_regiaoPlanejamento_id on Localizacao (regiaoPlanejamento_id);
create index IX_MeioAmbiente_projeto_id on MeioAmbiente (projeto_id);
create index IX_MeioAmbienteAnexo_anexo_id on MeioAmbienteAnexo (anexo_id);
create index IX_MeioAmbienteAnexo_meioAmbiente_id on MeioAmbienteAnexo (meioAmbiente_id);
create index IX_MicroRegiao_regiaoPlanejamento_id on MicroRegiao (regiaoPlanejamento_id);
create index IX_Municipio_microRegiao_id on Municipio (microRegiao_id);
create index IX_Municipio_regiaoPlanejamento_id on Municipio (regiaoPlanejamento_id);
create index IX_Municipio_UF_id on Municipio (UF_id);
create index IX_NomeEmpresa_empresa_id on NomeEmpresa (empresa_id);
create index IX_OrigemRecurso_financeiro_id on OrigemRecurso (financeiro_id);
create index IX_OrigemRecurso_pais_id on OrigemRecurso (pais_id);
create index IX_OrigemRecurso_uf_id on OrigemRecurso (uf_id);
create index IX_Parceria_insumoProduto_id on Parceria (insumoProduto_id);
create index IX_Pesquisa_instrumentoFormalizacao_id on Pesquisa (instrumentoFormalizacao_id);
create index IX_PesquisaPerguntaPesquisa_perguntaPesquisa_id on PesquisaPerguntaPesquisa (perguntaPesquisa_id);
create index IX_PesquisaPerguntaPesquisa_pesquisa_id on PesquisaPerguntaPesquisa (pesquisa_id);
create index IX_Pleito_projeto_id on Pleito (projeto_id);
create index IX_Producao_insumoProduto_id on Producao (insumoProduto_id);
create index IX_Produto_insumoProduto_id on Produto (insumoProduto_id);
create index IX_Produto_ncm_id on Produto (ncm_id);
create index IX_Projeto_cadeiaProdutiva_id on Projeto (cadeiaProdutiva_id);
create index IX_Projeto_empresa_id on Projeto (empresa_id);
create index IX_Projeto_unidade_id on Projeto (unidade_id);
create index IX_Projeto_usuarioInvestimento_id on Projeto (usuarioInvestimento_id);
create index IX_ProjetoInstrumentoFormalizacao_instrumentoFormalizacao_id on ProjetoInstrumentoFormalizacao (instrumentoFormalizacao_id);
create index IX_ProjetoInstrumentoFormalizacao_projeto_id on ProjetoInstrumentoFormalizacao (projeto_id);
create index IX_Publicacao_instrumentoFormalizacao_id on Publicacao (instrumentoFormalizacao_id);
create index IX_PublicacaoAnexo_anexo_id on PublicacaoAnexo (anexo_id);
create index IX_PublicacaoAnexo_publicacao_id on PublicacaoAnexo (publicacao_id);
create index IX_Servico_insumoProduto_id on Servico (insumoProduto_id);
create index IX_Signatario_instrumentoFormalizacao_id on Signatario (instrumentoFormalizacao_id);
create index IX_UF_pais_id on UF (pais_id);
create index IX_UnidadeEmpresa_empresa_id on UnidadeEmpresa (empresa_id);
create index IX_UsoFonte_financeiro_id on UsoFonte (financeiro_id);
create index IX_Usuario_grupoUsuario_id on Usuario (grupoUsuario_id);
create index IX_UsuarioExterno_empresa_id on UsuarioExterno (empresa_id);
create index IX_UsuarioExterno_id on UsuarioExterno (id);
create index IX_UsuarioExterno_unidade_id on UsuarioExterno (unidade_id);
create index IX_UsuarioInterno_departamento_id on UsuarioInterno (departamento_id);
create index IX_UsuarioInterno_id on UsuarioInterno (id);
create index IX_VerificationToken_user_id on VerificationToken (user_id);

-- Indexes that not come from FKs

--Empresa
create index IX_Empresa_cnpj on Empresa (cnpj);
create index IX_Empresa_razaosocial on Empresa (razaosocial);
create index IX_Empresa_nomeprincipal on Empresa (nomeprincipal);
create index IX_Empresa_situacao on Empresa (situacao);

--NomeEmpresa
create index IX_NomeEmpresa_nome on NomeEmpresa (nome);

--Projeto
create index IX_Projeto_nome on Projeto (nome);

--InstrumentoFormalizacao
create index IX_InstrumentoFormalizacao_situacaoatual_id on InstrumentoFormalizacao (situacaoatual_id);
create index IX_InstrumentoFormalizacao_historicoSituacaoInstrumentoFormalizacao_id on InstrumentoFormalizacao (historicoSituacaoInstrumentoFormalizacao_id);
create index IX_InstrumentoFormalizacao_tipo on InstrumentoFormalizacao (tipo);
create index IX_InstrumentoFormalizacao_protocolo on InstrumentoFormalizacao (protocolo);

--produto
create index IX_Produto_tipo on Produto (tipo);