IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.VIEWS
        WHERE TABLE_NAME = 'VIEW_INFRAESTRUTURA_BUSCA_AVANCADA')
    DROP VIEW VIEW_INFRAESTRUTURA_BUSCA_AVANCADA
GO

Create view VIEW_INFRAESTRUTURA_BUSCA_AVANCADA AS

Select distinct
inf.id,
inf.projeto_id,
inf.areaNecessaria,
inf.demandaEnergiaEstimada

from Infraestrutura inf;