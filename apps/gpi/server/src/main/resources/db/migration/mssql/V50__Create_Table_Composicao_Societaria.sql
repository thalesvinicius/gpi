CREATE TABLE ComposicaoSocietaria
(
   id int identity(1,1) NOT NULL,
   empresa_id int NOT NULL,
   socio varchar(255),
   empresa_grupo varchar(255),
   participacao_societaria decimal(6,2),
   cpf_cnpj varchar(255)
);

ALTER TABLE ComposicaoSocietaria
ADD CONSTRAINT FK_ComposicaoSocietaria_Empresa
FOREIGN KEY (empresa_id)
REFERENCES "dbo"."Empresa"(id);

CREATE INDEX IX_ComposicaoSocietaria_empresa_id ON ComposicaoSocietaria(empresa_id);