IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.VIEWS
        WHERE TABLE_NAME = 'VIEW_LOCALIZACAO_BUSCA_AVANCADA')
    DROP VIEW VIEW_LOCALIZACAO_BUSCA_AVANCADA
GO

Create view VIEW_LOCALIZACAO_BUSCA_AVANCADA AS

Select distinct
l.id,
l.projeto_id,
l.latitude,
l.longetude as longitude,
l.poligono,
l.localizacaoSecundaria,
Case l.aDefinir
WHEN 1 then 'Sim'
When 0 then 'Não'
ELSE ''
END as localizacaoADefinir,
rp.nome as RegiaoPlanejamento,
m.nome as Municipio,
mr.nome as MicroRegiao
from Localizacao l
inner join projeto p on l.projeto_id = p.id
left join RegiaoPlanejamento rp on l.regiaoPlanejamento_id = rp.id
left join Municipio m on l.municipio_id = m.id
left join MicroRegiao mr on l.microRegiao_id = mr.id;