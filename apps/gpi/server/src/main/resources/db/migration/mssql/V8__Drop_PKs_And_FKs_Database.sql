-- Delete all PKs and FKs
declare commands cursor for
SELECT 'alter table ' + cons.table_name + ' drop constraint ' + cons.constraint_name + ';' as query FROM information_schema.TABLE_CONSTRAINTS cons
where cons.TABLE_NAME in ('Anexo', 'AccessToken', 'AnexoInformacaoAdicional', 'AnexoLocalizacao', 'AnexoPesquisa', 'ProjetoRecente', 'AnexoProjeto', 'AtividadeInstrumento', 'AtividadeLocal', 'AtividadeProjeto', 'AtividadeTemplate', 'CadeiaProdutiva', 'Cargo', 'CNAE', 'CNAEEmpresa', 'Concorrente', 'Cronograma', 'Departamento', 'Emprego', 'Empresa', 'Endereco', 'EnergiaContratada', 'Estagio', 'EstagioProjeto', 'EtapaMeioAmbiente', 'FaturamentoAnterior', 'FaturamentoPrevisto', 'Financeiro', 'FinanceiroAnexo', 'GrupoUsuario', 'HistoricoSituacaoInstrumentoFormalizacao', 'HistoricoSituacaoProjeto', 'InformacaoAdicional', 'Infraestrutura', 'InstrumentoFormalizacao', 'Insumo', 'InsumoProduto', 'InvestimentoPrevisto', 'Local', 'Localizacao', 'MeioAmbiente', 'MeioAmbienteAnexo', 'MicroRegiao', 'Municipio', 'NaturezaJuridica', 'NCM', 'NomeEmpresa', 'OrigemRecurso', 'Pais', 'Parceria', 'PerguntaPesquisa', 'Pesquisa', 'PesquisaPerguntaPesquisa', 'Pleito', 'Producao', 'Produto', 'Projeto', 'ProjetoInstrumentoFormalizacao', 'Publicacao', 'PublicacaoAnexo', 'RegiaoPlanejamento', 'Servico', 'Signatario', 'SituacaoInstrumentoFormalizacao', 'SituacaoProjeto', 'Template', 'UF', 'UnidadeEmpresa', 'UsoFonte', 'Usuario', 'UsuarioExterno', 'VerificationToken', 'UsuarioInterno')
order by cons.constraint_type;

declare @cmd varchar(max)

open commands
fetch next from commands into @cmd
while @@FETCH_STATUS=0
begin
  exec(@cmd)
  fetch next from commands into @cmd
end

close commands
deallocate commands;