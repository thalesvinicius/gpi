-- origemEmpresa
alter table empresa add origemEmpresa int
GO

update empresa set origemEmpresa = 1 where empresaMineira = 1;
update empresa set origemEmpresa = 2 where empresaMineira = 0 and empresaInternacional = 0;
update empresa set origemEmpresa = 3 where empresaMineira = 0 and empresaInternacional = 1;
GO
alter table empresa alter column origemEmpresa int not null;
GO
-- paisOrigem
alter table empresa add paisOrigem int;
GO
-- estadoOrigem
alter table empresa add estadoOrigem int;
GO

--FKs
IF EXISTS (select cons.CONSTRAINT_NAME FROM information_schema.TABLE_CONSTRAINTS cons
where cons.CONSTRAINT_NAME = 'FK_Empresa_Pais')
        ALTER TABLE Empresa DROP CONSTRAINT FK_Empresa_Pais;

ALTER TABLE Empresa ADD CONSTRAINT FK_Empresa_Pais 
	FOREIGN KEY (paisOrigem) REFERENCES Pais(id);

IF EXISTS (select cons.CONSTRAINT_NAME FROM information_schema.TABLE_CONSTRAINTS cons
where cons.CONSTRAINT_NAME = 'FK_Empresa_UF')
        ALTER TABLE Empresa DROP CONSTRAINT FK_Empresa_UF;

ALTER TABLE Empresa ADD CONSTRAINT FK_Empresa_UF
	FOREIGN KEY (estadoOrigem) REFERENCES UF(id);

--Indexes
IF EXISTS (select ix.name FROM sysindexes ix
where ix.name = 'IX_Empresa_EstadoOrigem')

DROP INDEX IX_Empresa_EstadoOrigem ON Empresa;

create index IX_Empresa_EstadoOrigem on Empresa (estadoOrigem);

IF EXISTS (select ix.name FROM sysindexes ix
where ix.name = 'IX_Empresa_PaisOrigem')

DROP INDEX IX_Empresa_PaisOrigem ON Empresa;

create index IX_Empresa_PaisOrigem on Empresa (paisOrigem);