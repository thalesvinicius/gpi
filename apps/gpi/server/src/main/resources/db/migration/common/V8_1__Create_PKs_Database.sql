/* If you came here because migrations fail go to 
'V8__Drop_PKs_And_FKs_Database.sql' and uncomment your 
corresponding database section;
*/

--  Create Primary Key Constraints 
ALTER TABLE Anexo ADD CONSTRAINT PK_Anexo 
	PRIMARY KEY  (id)
;

ALTER TABLE AnexoInformacaoAdicional ADD CONSTRAINT PK_AnexoContato 
	PRIMARY KEY  (anexo_id, informacaoAdicional_id)
;

ALTER TABLE AnexoLocalizacao ADD CONSTRAINT PK_AnexoLocalizacao 
	PRIMARY KEY  (anexo_id, localizacao_id)
;

ALTER TABLE AnexoProjeto ADD CONSTRAINT PK_AnexoProjeto 
	PRIMARY KEY  (anexo_id, projeto_id)
;

ALTER TABLE AtividadeInstrumento ADD CONSTRAINT PK_AtividadeInstrumento 
	PRIMARY KEY  (id)
;

ALTER TABLE AtividadeLocal ADD CONSTRAINT PK_AtividadeLocal 
	PRIMARY KEY  (id)
;

ALTER TABLE AtividadeProjeto ADD CONSTRAINT PK_AtividadeProjeto 
	PRIMARY KEY  (id)
;

ALTER TABLE AtividadeTemplate ADD CONSTRAINT PK_AtividadeTemplate 
	PRIMARY KEY  (id)
;

ALTER TABLE CadeiaProdutiva ADD CONSTRAINT PK_CadeiaProdutiva 
	PRIMARY KEY  (id)
;

ALTER TABLE Cargo ADD CONSTRAINT PK_Cargo 
	PRIMARY KEY  (id)
;

ALTER TABLE CNAE ADD CONSTRAINT PK_CNAE 
	PRIMARY KEY  (id)
;

ALTER TABLE CNAEEmpresa ADD CONSTRAINT PK_CNAEEmpresa 
	PRIMARY KEY  (CNAE_id, empresa_id)
;

ALTER TABLE Concorrente ADD CONSTRAINT PK_Concorrente 
	PRIMARY KEY  (id)
;

ALTER TABLE Cronograma ADD CONSTRAINT PK_Cronograma 
	PRIMARY KEY  (id)
;

ALTER TABLE Departamento ADD CONSTRAINT PK_Departamento 
	PRIMARY KEY  (id)
;

ALTER TABLE Emprego ADD CONSTRAINT PK_Emprego 
	PRIMARY KEY  (id)
;

ALTER TABLE Empresa ADD CONSTRAINT PK_Empresa 
	PRIMARY KEY  (id)
;

ALTER TABLE Endereco ADD CONSTRAINT PK_Endereco 
	PRIMARY KEY  (id)
;

ALTER TABLE EnergiaContratada ADD CONSTRAINT PK_EnergiaContratada 
	PRIMARY KEY  (id)
;

ALTER TABLE Estagio ADD CONSTRAINT PK_Estagio 
	PRIMARY KEY  (id)
;

ALTER TABLE EstagioProjeto ADD CONSTRAINT PK_EstagioProjeto 
	PRIMARY KEY  (projeto_id, estagio_id)
;

ALTER TABLE EtapaMeioAmbiente ADD CONSTRAINT PK_EtapaMeioAmbiente 
	PRIMARY KEY  (id)
;

ALTER TABLE FaturamentoAnterior ADD CONSTRAINT PK_FaturamentoAnterior 
	PRIMARY KEY  (id)
;

ALTER TABLE FaturamentoPrevisto ADD CONSTRAINT PK_FaturamentoPrevisto 
	PRIMARY KEY  (id)
;

ALTER TABLE Financeiro ADD CONSTRAINT PK_Financeiro 
	PRIMARY KEY  (id)
;

ALTER TABLE GrupoUsuario ADD CONSTRAINT PK_GrupoUsuario 
	PRIMARY KEY  (id)
;

ALTER TABLE HistoricoSituacaoInstrumentoFormalizacao ADD CONSTRAINT PK_HistoricoSituacaoInstrumentoFormalizacao 
	PRIMARY KEY  (id)
;

ALTER TABLE HistoricoSituacaoProjeto ADD CONSTRAINT PK_Situacao 
	PRIMARY KEY  (id)
;

ALTER TABLE InformacaoAdicional ADD CONSTRAINT PK_InformacaoAdicional 
	PRIMARY KEY  (id)
;

ALTER TABLE Infraestrutura ADD CONSTRAINT PK_Infraestrutura 
	PRIMARY KEY  (id)
;

ALTER TABLE InstrumentoFormalizacao ADD CONSTRAINT PK_InstrumentoFormalizacao 
	PRIMARY KEY  (id)
;

ALTER TABLE Insumo ADD CONSTRAINT PK_Insumo 
	PRIMARY KEY  (id)
;

ALTER TABLE InsumoProduto ADD CONSTRAINT PK_InsumoProduto 
	PRIMARY KEY  (id)
;

ALTER TABLE InvestimentoPrevisto ADD CONSTRAINT PK_InvestimentoPrevisto 
	PRIMARY KEY  (id)
;

ALTER TABLE Local ADD CONSTRAINT PK_Local 
	PRIMARY KEY  (id)
;

ALTER TABLE Localizacao ADD CONSTRAINT PK_Localizacao 
	PRIMARY KEY  (id)
;

ALTER TABLE MeioAmbiente ADD CONSTRAINT PK_MeioAmbiente 
	PRIMARY KEY  (id)
;

ALTER TABLE MicroRegiao ADD CONSTRAINT PK_MicroRegiao 
	PRIMARY KEY  (id)
;

ALTER TABLE Municipio ADD CONSTRAINT PK_Municipio 
	PRIMARY KEY  (id)
;

ALTER TABLE NaturezaJuridica ADD CONSTRAINT PK_NaturezaJuridica 
	PRIMARY KEY  (id)
;

ALTER TABLE NCM ADD CONSTRAINT PK_NCM 
	PRIMARY KEY  (id)
;

ALTER TABLE NomeEmpresa ADD CONSTRAINT PK_NomeEmpresa 
	PRIMARY KEY  (id)
;

ALTER TABLE OrigemRecurso ADD CONSTRAINT PK_OrigemRecurso 
	PRIMARY KEY  (id)
;

ALTER TABLE Pais ADD CONSTRAINT PK_Pais 
	PRIMARY KEY  (id)
;

ALTER TABLE Parceria ADD CONSTRAINT PK_Parceria 
	PRIMARY KEY  (id)
;

ALTER TABLE PerguntaPesquisa ADD CONSTRAINT PK_PerguntaPesquisa 
	PRIMARY KEY  (id)
;

ALTER TABLE Pesquisa ADD CONSTRAINT PK_Pesquisa 
	PRIMARY KEY  (id)
;

ALTER TABLE Pleito ADD CONSTRAINT PK_Pleito 
	PRIMARY KEY  (id)
;

ALTER TABLE Producao ADD CONSTRAINT PK_Producao 
	PRIMARY KEY  (id)
;

ALTER TABLE Produto ADD CONSTRAINT PK_Produto 
	PRIMARY KEY  (id)
;

ALTER TABLE Projeto ADD CONSTRAINT PK_Projeto 
	PRIMARY KEY  (id)
;

ALTER TABLE Publicacao ADD CONSTRAINT PK_Publicacao 
	PRIMARY KEY  (id)
;

ALTER TABLE RegiaoPlanejamento ADD CONSTRAINT PK_RegiaoPlanejamento 
	PRIMARY KEY  (id)
;

ALTER TABLE Servico ADD CONSTRAINT PK_Servico 
	PRIMARY KEY  (id)
;

ALTER TABLE Signatario ADD CONSTRAINT PK_Signatario 
	PRIMARY KEY  (id)
;

ALTER TABLE SituacaoInstrumentoFormalizacao ADD CONSTRAINT PK_SituacaoInstrumentoFormalizacao 
	PRIMARY KEY  (id)
;

ALTER TABLE SituacaoProjeto ADD CONSTRAINT PK_SituacaoProjeto 
	PRIMARY KEY  (id)
;

ALTER TABLE Template ADD CONSTRAINT PK_Template 
	PRIMARY KEY  (id)
;

ALTER TABLE UF ADD CONSTRAINT PK_UF 
	PRIMARY KEY  (id)
;

ALTER TABLE UnidadeEmpresa ADD CONSTRAINT PK_UnidadeEmpresa 
	PRIMARY KEY  (id)
;

ALTER TABLE UsoFonte ADD CONSTRAINT PK_UsoFonte 
	PRIMARY KEY  (id)
;

ALTER TABLE Usuario ADD CONSTRAINT PK_Usuario 
	PRIMARY KEY  (id)
;

ALTER TABLE UsuarioExterno ADD CONSTRAINT PK_ContatoExterno 
	PRIMARY KEY  (id)
;

ALTER TABLE UsuarioInterno ADD CONSTRAINT PK_UsuarioInterno 
	PRIMARY KEY  (id)
;

ALTER TABLE AccessToken ADD CONSTRAINT PK_AccessToken
	PRIMARY KEY  (id)
;

ALTER TABLE VerificationToken ADD CONSTRAINT PK_VerificationToken 
	PRIMARY KEY  (id)
;