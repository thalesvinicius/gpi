--AtividadeTemplate
IF EXISTS (select cons.CONSTRAINT_NAME FROM information_schema.TABLE_CONSTRAINTS cons
where cons.CONSTRAINT_NAME = 'FK_AtividadeTemplate_Template')
        ALTER TABLE AtividadeTemplate DROP CONSTRAINT FK_AtividadeTemplate_Template;

ALTER TABLE AtividadeTemplate ADD CONSTRAINT FK_AtividadeTemplate_Template 
        FOREIGN KEY (template_id) REFERENCES Template (id)
        ON DELETE CASCADE;