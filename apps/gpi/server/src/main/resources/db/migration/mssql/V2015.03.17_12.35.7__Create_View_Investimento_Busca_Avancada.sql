IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.VIEWS
        WHERE TABLE_NAME = 'VIEW_INVESTIMENTO_BUSCA_AVANCADA')
    DROP VIEW VIEW_INVESTIMENTO_BUSCA_AVANCADA
GO

Create view VIEW_INVESTIMENTO_BUSCA_AVANCADA AS
-- Investimento
select
f.projeto_id,
invPrev.financeiro_id,
ROUND(SUM(invPrev.valor)/1000, 2) as investimentoTotalPrevisto,
ROUND(SUM(invPrev.valorRealizado)/1000, 2) as investimentoTotalRealizado
from InvestimentoPrevisto invPrev
inner join Financeiro f on invPrev.financeiro_id = f.id
group by invPrev.financeiro_id, f.projeto_id;


-- select
-- f.projeto_id,
-- invPrev.financeiro_id,
-- replace(replace(replace(convert(varchar,cast(floor(ROUND(SUM(invPrev.valor)/1000, 2)) as money),1), ',', 'x'),'.00',
--     ','+right(ROUND(SUM(invPrev.valor)/1000, 2),2)), 'x', '.') as investimentoTotalPrevisto,
-- replace(replace(replace(convert(varchar,cast(floor(ROUND(SUM(invPrev.valorRealizado)/1000, 2)) as money),1), ',', 'x'),'.00',
--     ','+right(ROUND(SUM(invPrev.valorRealizado)/1000, 2),2)), 'x', '.') as investimentoTotalRealizado
-- from InvestimentoPrevisto invPrev
-- inner join Financeiro f on invPrev.financeiro_id = f.id
-- group by invPrev.financeiro_id, f.projeto_id;