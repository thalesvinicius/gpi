IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.VIEWS
        WHERE TABLE_NAME = 'VIEW_RELATORIO_PAINEL_METAS')
    DROP VIEW VIEW_RELATORIO_PAINEL_METAS
GO

CREATE VIEW VIEW_RELATORIO_PAINEL_METAS
AS
WITH projeto_ver_congelada_ou_atual AS (
    SELECT p.* 
    FROM (
            SELECT p.id, p.familia_id 
            FROM projeto p
            WHERE p.ultimaversao = 1 AND p.versao = 0 AND situacaoatual_id <> 4 
            UNION   
            SELECT
            p.id, p.familia_id
            FROM projeto p
            WHERE p.ultimaversao = 0
            AND situacaoatual_id <> 4
            AND p.id in
            (
               SELECT
               top(1) p2.id
               FROM projeto p2
               WHERE p2.ultimaversao = 0
               AND p2.situacaoatual_id <> 4
               and p2.familia_id = p.familia_id
               order by p2.versao desc
            )       
    ) as tab
    INNER JOIN projeto p ON tab.id = p.id
)
SELECT
    DPT.id AS gerenciaId,
    DPT.descricao AS gerencia,
    DPS.id AS diretoriaId,
    DPS.descricao AS diretoria,
    PRJ.prospeccaoativa AS prospeccaoAtiva,    
    EST.estagio_id AS estagioId,     
    STC.id AS situacaoProjetoId,
    DATEPART(mm, EST.dataInicioEstagio) AS mesCadastro,
    DATEPART(yyyy, EST.dataInicioEstagio) AS anoCadastro,
    (select SUM(EMP.direto) FROM emprego EMP WHERE EMP.projeto_id = PRJAOC.id
AND tipo NOT IN(1,3,5,6)) AS empregosDiretos,
    (select ISNULL(SUM(valor) / 1000000, 0) FROM FaturamentoPrevisto FAT WHERE
FAT.financeiro_id = FIN.id AND FAT.ano = ( SELECT MAX(ano) FROM
FaturamentoPrevisto WHERE financeiro_id = FIN.id )) as faturamentoPlenoMilhoes,
    (select ISNULL(SUM(valor) / 1000000, 0) FROM InvestimentoPrevisto INV WHERE
INV.financeiro_id = FIN.id) AS investimentoPrevistoMilhoes
FROM projeto PRJ  
    INNER JOIN projeto_ver_congelada_ou_atual PRJAOC ON PRJAOC.familia_id =
PRJ.familia_id
    LEFT JOIN Financeiro FIN ON FIN.projeto_id = PRJAOC.id        
    LEFT JOIN EstagioProjeto EST ON EST.projeto_id = PRJ.id
    LEFT JOIN Usuario USR ON USR.id = PRJ.usuarioinvestimento_id
    LEFT JOIN UsuarioInterno UIN ON UIN.id = USR.id
    LEFT JOIN Departamento DPT ON DPT.id = UIN.departamento_id
    LEFT JOIN Departamento DPS ON DPS.id = DPT.departamentosuperior_id
    LEFT JOIN HistoricoSituacaoProjeto HST ON HST.id =
PRJ.historicosituacaoprojeto_id
    LEFT JOIN SituacaoProjeto STC ON STC.id = HST.situacaoprojeto_id    
WHERE PRJ.ultimaVersao = 1
