DROP TABLE ProjetoRecente;

CREATE TABLE UsuarioProjetoRecente (
            usuario_id int NOT NULL,
            projeto_id int NOT NULL,
            dataAcesso datetime,
            primary key (usuario_id,projeto_id)
);

ALTER TABLE UsuarioProjetoRecente ADD CONSTRAINT FK_Usuario_ProjetoRecente
	FOREIGN KEY (projeto_id) REFERENCES Projeto (id);
