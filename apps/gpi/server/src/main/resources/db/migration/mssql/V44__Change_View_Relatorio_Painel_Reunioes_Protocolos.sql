IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.VIEWS
        WHERE TABLE_NAME = 'VIEW_RELATORIO_PAINEL_REUNIOES_PROTOCOLOS')
    DROP VIEW VIEW_RELATORIO_PAINEL_REUNIOES_PROTOCOLOS
GO

CREATE VIEW VIEW_RELATORIO_PAINEL_REUNIOES_PROTOCOLOS
AS
WITH projeto_ver_congelada_ou_atual AS (
    SELECT p.* 
    FROM (
            SELECT p.id, p.familia_id 
            FROM projeto p
            WHERE p.ultimaversao = 1 AND p.versao = 0 AND situacaoatual_id <> 4	
            UNION	
            SELECT
            p.id, p.familia_id
            FROM projeto p
            WHERE p.ultimaversao = 0
            AND situacaoatual_id <> 4
            AND p.id in
            (
               SELECT
               top(1) p2.id
               FROM projeto p2
               WHERE p2.ultimaversao = 0
               AND p2.situacaoatual_id <> 4
               and p2.familia_id = p.familia_id
               order by p2.versao desc
            )		
    ) as tab
    INNER JOIN projeto p ON tab.id = p.id
)
SELECT
    INS.id AS instrumentoId,
    EMP.nomeprincipal AS empresa,
    PRJ.nome AS projeto,
    MUN.nome AS cidade,
    REG.nome AS regiaoPlanejamento,    
    CAD.descricao AS cadeiaProdutiva,
    EST.descricao AS estagioAtual,
    (select max(datainicioestagio) from EstagioProjeto EPR where EPR.projeto_id = PRJ.ID AND EPR.estagio_id = PRJ.estagioatual_id) AS dataEstagioAtual,	
    (select datainicioestagio from EstagioProjeto EPR where EPR.projeto_id = PRJ.ID AND EPR.estagio_id = 1) AS dataEstagioDecisaoFormalizada,
    USR.nome AS analista,
    DPT.id AS gerenciaId,
    DPT.descricao AS gerencia,
    DPS.id AS diretoriaId,
    DPS.descricao AS diretoria,
    CAD.id AS cadeiaProdutivaId,                    
    PRJ.prospeccaoativa AS prospeccaoAtiva,    
    LOC.regiaoplanejamento_id AS regiaoPlanejamentoId,
    PRJ.estagioatual_id AS projetoEstagioatualId,     
    INS.tipo AS tipoInstrumentoId,
    CASE INS.tipo
        WHEN 1 THEN 'Aprovação Financiamento BDMG'
        WHEN 2 THEN 'Aquisição de Terreno'
        WHEN 3 THEN 'Autorização Diretoria'
        WHEN 4 THEN 'Protocolo de Intenções'
        WHEN 5 THEN 'Termo Aditivo'
    END AS tipoInstrumento,
    INS.dataassinatura AS dataAssinaturaInstrumento,
    INS.dataassinaturaprevista AS dataAssinaturaPrevistaInstrumento,
    SIT.id AS situacaoInstrumentoId,		
    SIT.descricao AS situacaoInstrumento,
    HSI.justificativa AS justificativaInstrumento,
    CASE WHEN SIT.id = 2 THEN HSI.dataInicioSituacao END as dataCancelamentoInstrumento,		
    STC.descricao AS situacaoProjeto,
    STC.id AS situacaoProjetoId,
    HST.justificativa as justificativaProjeto,
    CASE WHEN STC.id = 2 THEN HST.data END as dataCancelamentoProjeto,
    PRJ.dataCadastro AS dataCadastroProjeto,
    (select sum(direto) from emprego where projeto_id = PRJAOC.id) AS empregosDiretos,
    (select ISNULL(SUM(valor) / 1000, 0) from investimentoprevisto where financeiro_id = fin.id) AS investimentoPrevisto,	
    (select ISNULL(SUM(valor) / 1000, 0) from faturamentoprevisto where ano = (select min(ano) from faturamentoprevisto where financeiro_id = fin.id) and financeiro_id = fin.id) as faturamentoInicial,
    (select ISNULL(SUM(valor) / 1000, 0) from faturamentoprevisto where ano = (select max(ano) from faturamentoprevisto where financeiro_id = fin.id) and financeiro_id = fin.id) as faturamentoPleno
FROM projeto PRJ  
    INNER JOIN projeto_ver_congelada_ou_atual PRJAOC ON PRJAOC.familia_id = PRJ.familia_id
    INNER JOIN Empresa EMP ON EMP.id = PRJ.empresa_id
    LEFT JOIN Localizacao LOC ON LOC.projeto_id = PRJ.id
    LEFT JOIN Municipio MUN ON MUN.id = LOC.municipio_id
    LEFT JOIN RegiaoPlanejamento REG ON REG.id = LOC.regiaoplanejamento_id
    LEFT JOIN Financeiro FIN ON FIN.projeto_id = PRJAOC.id    
    LEFT JOIN CadeiaProdutiva CAD ON CAD.id = PRJ.cadeiaprodutiva_id
    LEFT JOIN Estagio EST ON EST.id = PRJ.estagioatual_id
    LEFT JOIN Usuario USR ON USR.id = PRJ.usuarioinvestimento_id
    LEFT JOIN UsuarioInterno UIN ON UIN.id = USR.id
    LEFT JOIN Departamento DPT ON DPT.id = UIN.departamento_id
    LEFT JOIN ProjetoInstrumentoFormalizacao PEI ON PEI.projeto_id = PRJ.id
    LEFT JOIN InstrumentoFormalizacao INS ON INS.id = PEI.instrumentoformalizacao_id	
    LEFT JOIN HistoricoSituacaoInstrumentoFormalizacao HSI ON HSI.id = INS.historicoSituacaoInstrumentoFormalizacao_id
    LEFT JOIN situacaoInstrumentoFormalizacao SIT ON SIT.id = HSI.situacaoInstrumentoFormalizacao_id     
    LEFT JOIN Departamento DPS ON DPS.id = DPT.departamentosuperior_id
    LEFT JOIN HistoricoSituacaoProjeto HST on HST.id = PRJ.historicosituacaoprojeto_id
    LEFT JOIN situacaoProjeto STC on STC.id = HST.situacaoprojeto_id
WHERE 
    INS.ultimaversao = 1 
    AND (INS.ultimaversao = 1 OR INS.ultimaversao IS NULL)