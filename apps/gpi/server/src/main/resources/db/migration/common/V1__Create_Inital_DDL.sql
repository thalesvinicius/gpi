CREATE TABLE Anexo ( 
	id int identity(1,1) NOT NULL,
	nome varchar(255),
	local varchar(255),
        storage varBinary(MAX),
	extencao varchar(5),
	data datetime
);

CREATE TABLE AnexoInformacaoAdicional ( 
	anexo_id int NOT NULL,
	informacaoAdicional_id int NOT NULL
);

CREATE TABLE AnexoLocalizacao ( 
        id int identity(1,1) NOT NULL,
	anexo_id int NOT NULL,
	localizacao_id int NOT NULL,
	anexoAreaLocal bit NOT NULL,
	tipoLocalizacao int
);

CREATE TABLE AnexoProjeto ( 
	anexo_id int NOT NULL,
	projeto_id int NOT NULL
)
;

CREATE TABLE CadeiaProdutiva ( 
	id int NOT NULL,
	descricao varchar(255),
        usuarioResponsavel_id int, 
	departamento_id int
)
;

CREATE TABLE Cargo ( 
	id int NOT NULL,
	descricao varchar(255)
)
;

CREATE TABLE CNAE ( 
	id varchar(12) NOT NULL,
	denominacao varchar(255),
	CNAESuperior_id varchar(12),
        nivel int NOT NULL
)
;

CREATE TABLE CNAEEmpresa ( 
	CNAE_id varchar(12) NOT NULL,
	empresa_id int NOT NULL,
        nivel int NOT NULL
)
;

CREATE TABLE InformacaoAdicional ( 
	id int identity(1,1)  NOT NULL,
	empresa_id int,
	tipoContato int NOT NULL,
	assunto varchar(255),
	data datetime,
	descricao varchar(1500),
	usuarioResponsavel_id int NOT NULL,
	dataUltimaAlteracao datetime NOT NULL
)
;

CREATE TABLE Cronograma ( 
	id int identity(1,1) NOT NULL,
	projeto_id int NOT NULL,
	inicioProjeto datetime,
	inicioProjetoExecutivo datetime,
	inicioContratacaoEquipamentos datetime,
	inicioImplantacao datetime,
	inicioImplantacaoAgricola datetime,
	inicioImplantacaoIndustrial datetime,
	terminoImplantacaoAgricola datetime,
	terminoImplantacaoIndustrial datetime,
	inicioOperacao datetime,
    inicioOperacaoSucroenergetico datetime,
	terminoProjetoExecutivo datetime,
	terminoProjeto datetime,
	usuarioResponsavel_id int NOT NULL,
	dataUltimaAlteracao datetime NOT NULL
)
;

CREATE TABLE Departamento ( 
	id int NOT NULL,
        sigla varchar(255),
	descricao varchar(255),
	departamentoSuperior_id int
)
;

CREATE TABLE Empresa ( 
	id int identity(1,1)  NOT NULL,
	naturezaJuridica_id int,
	cadeiaProdutiva_id int,
	endereco_id int,
	anexo_id int,
	cnpj varchar(14),
	razaoSocial varchar(255),
	inscricaoEstadual varchar(255),
	nomePrincipal varchar(255),
    usuarioExternoResponsavel_id int,
	composicaoSocietaria varchar(1000),
	historico varchar(1000),
	situacao int NOT NULL,
	justificativaArquivamento varchar(1000),
	empresaMineira bit,
	empresaInternacional bit,
	usuarioResponsavel_id int,
	dataUltimaAlteracao datetime NOT NULL
)
;

CREATE TABLE Endereco ( 
	id int identity(1,1) NOT NULL,
	municipio_id int,
        uf_id int, 
	logradouro varchar(255),
	numero varchar(15),
	complemento varchar(255),
	CEP varchar(8),
	bairro varchar(255),
	cidade varchar(255),
	pais_id int
)
;

CREATE TABLE EnderecoInternacional ( 
	id int identity(1,1) NOT NULL
)
;

CREATE TABLE Estagio ( 
	id int NOT NULL,
	descricao varchar(100) NOT NULL
        
)
;

CREATE TABLE EstagioProjeto ( 
	projeto_id int NOT NULL,
	estagio_id int NOT NULL,
	dataInicioEstagio datetime NOT NULL,
	ativo bit NOT NULL
)
;

CREATE TABLE GrupoUsuario ( 
	id int NOT NULL,
	descricao varchar(255),
        tipo int NOT NULL    --  Tipo do Usu�rio, 1 = Interno 2 = Externo 
)
;

CREATE TABLE Localizacao ( 
	id int identity(1,1)  NOT NULL,    --  Identificador da Localização do Projeto 
	projeto_id int NOT NULL,    --  Identificação do projeto 
	latitude varchar(6),    --  Latitude em graus XX XX XX 
	longetude varchar(6),    --  Longetude em graus XX XX XX 
	poligono varchar(1000),
	municipio_id int,    --  Identificador do Município 
	microRegiao_id int,    --  Identificador da Microregiao de planejamento 
	regiaoPlanejamento_id int,    --  Identificador da Região de Planejamento 
	localizacaoSecundaria varchar(255),    --  Descritivo da localização secundária, Município, Micro ou Região de Planejamento 
	aDefinir bit DEFAULT 0 NOT NULL,    --  A definir? 
	usuarioResponsavel_id int NOT NULL,    --  Identificador do usuário responsável pelas informações 
	dataUltimaAlteracao datetime NOT NULL    --  Data da última alteração 
);

CREATE TABLE MicroRegiao ( 
	id int NOT NULL,
        regiaoPlanejamento_id int,    --  Identificador da região de planejamento 
	nome varchar(255)
)
;

CREATE TABLE Municipio ( 
	id int NOT NULL,
	UF_id int,
	nome varchar(255),
	microRegiao_id int,
	regiaoPlanejamento_id int
)
;

CREATE TABLE NaturezaJuridica ( 
	id int NOT NULL,
	descricao varchar(50)
)
;

CREATE TABLE NomeEmpresa ( 
	id int identity(1,1)  NOT NULL,
	empresa_id int NOT NULL,
	nome varchar(255)
)
;

CREATE TABLE Pais ( 
	id int NOT NULL,
	nome varchar(255)
)
;

CREATE TABLE Pleito ( 
	id int identity(1,1)  NOT NULL,
	projeto_id int NOT NULL,
	pleitos text,
	observacoes text,
	usuarioResponsavel_id int NOT NULL,
	dataUltimaAlteracao datetime NOT NULL
)
;

CREATE TABLE Projeto ( 
	id int identity(1,1)  NOT NULL,
	usuarioInvestimento_id int NOT NULL,
	empresa_id int NOT NULL,
	unidade_id int,
	nome varchar(255),
	descricao varchar(1000),
	situacaoAtual_id int,
	prospeccaoAtiva bit,
	tipo int,
	cadeiaProdutiva_id int,
	dataCadastro datetime NOT NULL,
	informacaoPrimeiroContato varchar(1000),
	usuarioResponsavel_id int NOT NULL,
	dataUltimaAlteracao datetime NOT NULL,
        versao int,
        familia_id int,
        estagioAtual_id int,
        ultimaVersao bit NOT NULL,
        ultimaAtividade_id int
)
;

CREATE TABLE Emprego ( 
	id int identity(1,1)  NOT NULL,
	projeto_id int NOT NULL, 
	ano int NOT NULL, 
	direto int NOT NULL,
	indireto int NOT NULL,
	tipo int NOT NULL,
	usuarioResponsavel_id int NOT NULL,
	dataUltimaAlteracao datetime NOT NULL
)
;

CREATE TABLE Infraestrutura ( 
	id int identity(1,1)  NOT NULL,
	projeto_id int NOT NULL,  
	areaNecessaria decimal(10,2), 
	areaConstruida decimal(10,2),
	potenciaEnergeticaEstimada decimal(10,2),
	consumoAguaEstimado decimal(10,2),    
	consumoCombustivelEstimado decimal(10,2),
	consumoGasEstimado decimal(10,2),    
	geracaoEnergiaPropria decimal(10,2),  
	geracaoEnergiaVenda decimal(10,2),    
	demandaEnergiaEstimada decimal(10,2),    
	ofertaEnergiaVendaEstimada decimal(10,2),
	possuiTerreno bit DEFAULT 0,
	contatoCemig bit DEFAULT 0, 
	areaAlagada bit DEFAULT 0, 
	energiaJaContratada bit,  
	usuarioResponsavel_id int NOT NULL,
	dataUltimaAlteracao datetime NOT NULL
)
;

CREATE TABLE EnergiaContratada ( 
	id int identity(1,1)  NOT NULL,  
	infraestrutura_id int NOT NULL,    
	contrato int,    
	nome varchar(255),
	localizacao varchar(255)
)
;

CREATE TABLE RegiaoPlanejamento ( 
	id int NOT NULL,
	nome varchar(255)
)
;

CREATE TABLE UF ( 
	id int NOT NULL,
	pais_id int,
	sigla varchar(5),
	nome varchar(255)
)
;

CREATE TABLE UnidadeEmpresa ( 
	id int identity(1,1)  NOT NULL,
	empresa_id int NOT NULL,
	endereco_id int,
	nome varchar(255),
	email varchar(255),
	telefone varchar(18),
	usuarioResponsavel_id int NOT NULL,
	dataUltimaAlteracao datetime NOT NULL
)
;

  CREATE TABLE Usuario ( 
            id int identity(1,1)  NOT NULL,
            cargo_id int NOT NULL,
            grupoUsuario_id int NOT NULL,            
            nome varchar(50),
            senha varchar(128) not null,
            email varchar(255),
            telefone varchar(18),            
            telefone2 varchar(18),
            ativo bit not null,
            usuarioResponsavel_id int,
            dataUltimaAlteracao datetime NOT NULL,
            tipo int NOT NULL,
            mailing bit
        );

CREATE TABLE UsuarioInterno ( 
            id int NOT NULL,
            departamento_id int NOT NULL,
            matricula varchar(15) NOT NULL
        );

CREATE TABLE UsuarioExterno ( 
            id int NOT NULL,
            cargoExterno varchar(100) NOT NULL,
            empresa_id int,
            unidade_id int,
            senha varchar(128),
            dataBloqueioTemporario datetime
        );

create table AccessToken (
        id int identity(1,1) not null,
        lastUpdated datetime,
        timeCreated datetime,
        token varchar(36),
        user_id int
    );

create table VerificationToken (
        id int identity(1,1) not null,
        expiryDate datetime,
        token varchar(36),
        tokenType varchar(255),
        verified bit not null,
        user_id int NOT NULL
    );

create table ProjetoRecente(
    projeto_id int not null,
    usuario_id int not null,        
    dataUltimaAlteracao datetime NOT NULL

);

CREATE TABLE HistoricoSituacaoProjeto ( 
	id int identity(1,1)  NOT NULL,    --  Identificador da Situa��o 
	projeto_id int NOT NULL,    --  Identificador do Projeto 
	situacao int NOT NULL,    --  Situa��o do projeto, Ativo, Cancelado, Suspenso, PendenteValidacao 
	data datetime NOT NULL,    --  Data da Situa��o 
	justificativa varchar(1000)    --  Justificativa da Situa��o
);