IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.VIEWS
        WHERE TABLE_NAME = 'VIEW_INSTRUMENTO_FORMALIZACAO_BUSCA_AVANCADA')
    DROP VIEW VIEW_INSTRUMENTO_FORMALIZACAO_BUSCA_AVANCADA
GO

Create view VIEW_INSTRUMENTO_FORMALIZACAO_BUSCA_AVANCADA AS

select distinct 
ie.id,
ie.protocolo,
ie.tipo,
ie.versao,
ie.titulo,
CASE ie.possuiContrapartidaFincGoverno 
     WHEN 1 THEN 'Sim'
     WHEN 0 THEN 'Não'
     ELSE ''
END as possuiContrapartidaFincGoverno,
CASE ie.tipo 
     WHEN 1 THEN 'Aprovação Financiamento BDMG'
     WHEN 2 THEN 'Aquisição de Terreno'
     WHEN 3 THEN 'Autorização Diretoria'
     WHEN 4 THEN 'Protocolo de Intenções'
     WHEN 5 THEN 'Termo Aditivo'
     ELSE ''
END  as tipoDescricao,
p.empresa_id,
p.id as projeto_id,
Convert(varchar(10),CONVERT(date,hsif.dataInicioSituacao,106),103) as dataInicioSituacao,
hsif.dataInicioSituacao as dataInicioSituacao_full,
Convert(varchar(10),CONVERT(date,ie.dataAssinaturaPrevista,106),103) as dataPrevistaAssinatura,
ie.dataAssinaturaPrevista as dataPrevistaAssinatura_full,
Convert(varchar(10),CONVERT(date,ie.dataAssinatura,106),103) as dataRealAssinatura,
ie.dataAssinatura as dataRealAssinatura_full,
hsif.justificativa,
sif.descricao as situacao,
ie.situacaoAtual_id,
ie.localdocumento,
Convert(varchar(10),CONVERT(date,pub.dataPublicacao,106),103) as datapublicacao,
pub.dataPublicacao as datapublicacao_full,
CASE 
WHEN pes.dataResposta is not null THEN 1
WHEN pes.dataResposta is null THEN 0
END as pesquisaSatisfacaoRespondida

from InstrumentoFormalizacao ie 
inner join ProjetoInstrumentoFormalizacao pif on ie.id = pif.instrumentoFormalizacao_id
inner join Projeto p on pif.projeto_id = p.id
inner join HistoricoSituacaoInstrumentoFormalizacao hsif on ie.historicoSituacaoInstrumentoFormalizacao_id = hsif.id
inner join SituacaoInstrumentoFormalizacao sif on hsif.situacaoInstrumentoFormalizacao_id = sif.id
left join Publicacao pub on ie.id = pub.instrumentoFormalizacao_id
left join Pesquisa pes on pes.instrumentoFormalizacao_id = ie.id;