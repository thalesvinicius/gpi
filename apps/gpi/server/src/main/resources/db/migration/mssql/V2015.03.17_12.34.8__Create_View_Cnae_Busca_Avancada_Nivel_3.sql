IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.VIEWS
        WHERE TABLE_NAME = 'VIEW_CNAE_BUSCA_AVANCADA_NIVEL_3')
    DROP VIEW VIEW_CNAE_BUSCA_AVANCADA_NIVEL_3
GO

Create view VIEW_CNAE_BUSCA_AVANCADA_NIVEL_3 AS

Select 
c.id,
c.denominacao as cnae, 
c.CNAESuperior_id, 
ce.empresa_id
from CNAEEmpresa ce 
left join CNAE c on ce.CNAE_id = c.id
where ce.nivel = 3;