IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.VIEWS
        WHERE TABLE_NAME = 'VIEW_PAINEL_INDICE_SATISFACAO')
    DROP VIEW VIEW_PAINEL_INDICE_SATISFACAO
GO

Create View VIEW_PAINEL_INDICE_SATISFACAO AS
select ROW_NUMBER() over (order by tab.mesAno asc) as id, tab.* from (
select v.diretoria,
v.diretoriaId,
right(convert(varchar, v.dataRecebimento, 103), 7) as mesAno,
max(DATEPART(mm, v.dataRecebimento)) as mes, 
max(DATEPART(yyyy, v.dataRecebimento)) AS ano,
CONVERT(decimal(16,2), sum(v.porcentagemPesquisa)/count(v.diretoria)) as percentual
from VIEW_RELATORIO_PESQUISAS_RECEBIDAS v
where v.porcentagemPesquisa is not null
group by v.diretoria, v.diretoriaId, right(convert(varchar, v.dataRecebimento, 103), 7)) as tab;
