DROP TABLE LogAccessToken
GO

DROP TABLE LogAnexo
GO

DROP TABLE LogAnexoInformacaoAdicional
GO

DROP TABLE LogAnexoLocalizacao
GO

DROP TABLE LogAnexoPesquisa
GO

DROP TABLE LogAnexoProjeto
GO

DROP TABLE LogAnexoRelatorioAcompanhamento
GO

DROP TABLE LogAtividadeInstrumento
GO

DROP TABLE LogAtividadeLocal
GO

DROP TABLE LogAtividadeProjeto
GO

DROP TABLE LogAtividadeTemplate
GO

DROP TABLE LogCadeiaProdutiva
GO

DROP TABLE LogCargo
GO

DROP TABLE LogCNAE
GO

DROP TABLE LogCNAEEmpresa
GO

DROP TABLE LogComposicaoSocietaria
GO

DROP TABLE LogConcorrente
GO

DROP TABLE LogCronograma
GO

DROP TABLE LogDepartamento
GO

DROP TABLE LogEmprego
GO

DROP TABLE LogEmpresa
GO

DROP TABLE LogEndereco
GO

DROP TABLE LogEnergiaContratada
GO

DROP TABLE LogEstagioProjeto
GO

DROP TABLE LogEtapaMeioAmbiente
GO

DROP TABLE LogFaturamentoAnterior
GO

DROP TABLE LogFaturamentoPrevisto
GO

DROP TABLE LogFinanceiro
GO

DROP TABLE LogFinanceiroAnexo
GO

DROP TABLE LogGrupoUsuario
GO

DROP TABLE LogHistoricoSituacaoInstrumentoFormalizacao
GO

DROP TABLE LogHistoricoSituacaoProjeto
GO

DROP TABLE LogInformacaoAdicional
GO

DROP TABLE LogInfraestrutura
GO

DROP TABLE LogInstrumentoFormalizacao
GO

DROP TABLE LogInsumo
GO

DROP TABLE LogInsumoProduto
GO

DROP TABLE LogInvestimentoPrevisto
GO

DROP TABLE LogLocal
GO

DROP TABLE LogLocalizacao
GO

DROP TABLE LogMeioAmbiente
GO

DROP TABLE LogMeioAmbienteAnexo
GO

DROP TABLE LogMeta
GO

DROP TABLE LogMicroRegiao
GO

DROP TABLE LogMunicipio
GO

DROP TABLE LogNaturezaJuridica
GO

DROP TABLE LogNCM
GO

DROP TABLE LogNomeEmpresa
GO

DROP TABLE LogOrigemRecurso
GO

DROP TABLE LogPais
GO

DROP TABLE LogParceria
GO

DROP TABLE LogPerguntaPesquisa
GO

DROP TABLE LogPesquisa
GO

DROP TABLE LogPesquisaPerguntaPesquisa
GO

DROP TABLE LogPleito
GO

DROP TABLE LogProducao
GO

DROP TABLE LogProduto
GO

DROP TABLE LogProjeto
GO

DROP TABLE LogPROJETOINSTRUMENTOFORMALIZACAO
GO

DROP TABLE LogPublicacao
GO

DROP TABLE LogPublicacaoAnexo
GO

DROP TABLE LogRegiaoPlanejamento
GO

DROP TABLE LogRelatorioAcompanhamento
GO

DROP TABLE LogRelatorioAcompanhamentoSituacao
GO

DROP TABLE LogServico
GO

DROP TABLE LogSignatario
GO

DROP TABLE LogSituacaoInstrumentoFormalizacao
GO

DROP TABLE LogTemplate
GO

DROP TABLE LogUF
GO

DROP TABLE LogUnidadeEmpresa
GO

DROP TABLE LogUsoFonte
GO


DROP TABLE LogUsuarioExterno
GO

DROP TABLE LogUsuarioInterno
GO

DROP TABLE LogUsuario
GO

DROP TABLE LogUsuarioProjetoRecente
GO

DROP TABLE LogVerificationToken
GO

DROP TABLE LogRevisao
GO

-------------CREATE LOG TABLES

CREATE TABLE LogAccessToken
(
   id int,
   logVersao bigint NOT NULL,
   logAcao smallint,
   lastUpdated datetime,
   timeCreated datetime,
   token varchar(36),
   user_id int
)
GO
CREATE TABLE LogAnexo
(
   id int,
   logVersao bigint NOT NULL,
   logAcao smallint,
   data datetime,
   extencao varchar(5),
   nome varchar(255),
   local varchar(255)
)
GO
CREATE TABLE LogAnexoInformacaoAdicional
(
   logVersao bigint NOT NULL,
   informacaoAdicional_id int,
   anexo_id int,
   logAcao smallint
)
GO
CREATE TABLE LogAnexoLocalizacao
(
   id int,
   logVersao bigint NOT NULL,
   logAcao smallint,
   anexoAreaLocal tinyint,
   tipoLocalizacao int,
   anexo_id int,
   localizacao_id int
)
GO
CREATE TABLE LogAnexoPesquisa
(
   logVersao bigint NOT NULL,
   pesquisa_id int,
   anexo_id int,
   logAcao smallint
)
GO
CREATE TABLE LogAnexoProjeto
(
   logVersao bigint NOT NULL,
   projeto_id int,
   anexo_id int,
   logAcao smallint
)
GO
CREATE TABLE LogAnexoRelatorioAcompanhamento
(
   logVersao bigint NOT NULL,
   relatorioAcompanhamento_id int,
   anexo_id int,
   logAcao smallint
)
GO
CREATE TABLE LogAtividadeInstrumento
(
   id int,
   logVersao bigint NOT NULL,
   logAcao smallint,
   dataUltimaAlteracao datetime,
   bloqueiaICE tinyint,
   cc tinyint,
   dataConclusao datetime,
   dataInicio datetime,
   df tinyint,
   ii tinyint,
   obrigatorio tinyint,
   observacao varchar(4000),
   oi tinyint,
   paralela tinyint,
   posicao int,
   usuarioResponsavel_id int,
   anexo_id int,
   atividadeLocal_id int,
   instrumentoFormalizacao_id int
)
GO
CREATE TABLE LogAtividadeLocal
(
   id int,
   logVersao bigint NOT NULL,
   logAcao smallint,
   ativo tinyint,
   descricao varchar(255),
   local_id int
)
GO
CREATE TABLE LogAtividadeProjeto
(
   id int,
   logVersao bigint NOT NULL,
   logAcao smallint,
   dataUltimaAlteracao datetime,
   bloqueiaICE tinyint,
   cc tinyint,
   dataConclusao datetime,
   df tinyint,
   ii tinyint,
   obrigatorio tinyint,
   observacao varchar(4000),
   oi tinyint,
   posicao int,
   usuarioResponsavel_id int,
   anexo_id int,
   atividadeLocal_id int,
   projeto_id int
)
GO
CREATE TABLE LogAtividadeTemplate
(
   id int,
   logVersao bigint NOT NULL,
   logAcao smallint,
   bloqueiaICE tinyint,
   cc tinyint,
   df tinyint,
   ii tinyint,
   obrigatorio tinyint,
   oi tinyint,
   posicao int,
   atividadeLocal_id int,
   template_id int
)
GO
CREATE TABLE LogCadeiaProdutiva
(
   id int,
   logVersao bigint NOT NULL,
   logAcao smallint,
   descricao varchar(255),
   departamento_id int,
   usuarioResponsavel_id int
)
GO
CREATE TABLE LogCargo
(
   id int,
   logVersao bigint NOT NULL,
   logAcao smallint,
   descricao varchar(255)
)
GO
CREATE TABLE LogCNAE
(
   nivel int NOT NULL,
   id varchar(12),
   logVersao bigint NOT NULL,
   logAcao smallint,
   denominacao varchar(255),
   CNAESuperior_id varchar(12)
)
GO
CREATE TABLE LogCNAEEmpresa
(
   CNAE_id varchar(12),
   empresa_id int,
   logVersao bigint NOT NULL,
   logAcao smallint,
   nivel int
)
GO
CREATE TABLE LogComposicaoSocietaria
(
   id int,
   logVersao bigint NOT NULL,
   logAcao smallint,
   cpf_cnpj varchar(255),
   empresa_grupo varchar(255),
   participacao_societaria decimal(18,0),
   socio varchar(255),
   empresa_id int
)
GO
CREATE TABLE LogConcorrente
(
   id int,
   logVersao bigint NOT NULL,
   logAcao smallint,
   descricaoProduto varchar(200),
   nome varchar(200),
   insumoProduto_id int
)
GO
CREATE TABLE LogCronograma
(
   id int,
   logVersao bigint NOT NULL,
   logAcao smallint,
   dataUltimaAlteracao datetime,
   inicioContratacaoEquipamentos datetime,
   inicioImplantacao datetime,
   inicioImplantacaoAgricola datetime,
   inicioImplantacaoIndustrial datetime,
   inicioOperacao datetime,
   inicioOperacaoSucroenergetico datetime,
   inicioProjeto datetime,
   inicioProjetoExecutivo datetime,
   inicioTratamentoTributarioPrevisto datetime,
   inicioTratamentoTributarioRealizado datetime,
   terminoImplantacaoAgricola datetime,
   terminoImplantacaoIndustrial datetime,
   terminoProjeto datetime,
   terminoProjetoExecutivo datetime,
   usuarioResponsavel_id int,
   projeto_id int
)
GO
CREATE TABLE LogDepartamento
(
   id int,
   logVersao bigint NOT NULL,
   logAcao smallint,
   descricao varchar(255),
   sigla varchar(255),
   departamentoSuperior_id int
)
GO
CREATE TABLE LogEmprego
(
   id int,
   logVersao bigint NOT NULL,
   logAcao smallint,
   dataUltimaAlteracao datetime,
   ano int,
   direto int,
   diretoRealizado int,
   indireto int,
   tipo int,
   usuarioResponsavel_id int,
   projeto_id int
)
GO
CREATE TABLE LogEmpresa
(
   id int,
   logVersao bigint NOT NULL,
   logAcao smallint,
   dataUltimaAlteracao datetime,
   cnpj varchar(14),
   composicaoSocietaria varchar(1000),
   empresaNovaMG tinyint,
   historico varchar(4000),
   inscricaoEstadual varchar(255),
   justificativaArquivamento varchar(1000),
   nomePrincipal varchar(255),
   origemEmpresa int,
   razaoSocial varchar(255),
   situacao int,
   usuarioResponsavel_id int,
   cadeiaProdutiva_id int,
   endereco_id int,
   estadoOrigem int,
   anexo_id int,
   naturezaJuridica_id int,
   paisOrigem int,
   usuarioExternoResponsavel_id int
)
GO
CREATE TABLE LogEndereco
(
   id int,
   logVersao bigint NOT NULL,
   logAcao smallint,
   bairro varchar(255),
   cep varchar(8),
   cidade varchar(255),
   complemento varchar(255),
   logradouro varchar(255),
   numero varchar(15),
   municipio_id int,
   pais_id int,
   uf_id int
)
GO
CREATE TABLE LogEnergiaContratada
(
   id int,
   logVersao bigint NOT NULL,
   logAcao smallint,
   contrato int,
   localizacao varchar(255),
   nome varchar(255),
   infraestrutura_id int
)
GO
CREATE TABLE LogEstagioProjeto
(
   dataInicioEstagio datetime,
   estagio_id int,
   projeto_id int,
   logVersao bigint NOT NULL,
   logAcao smallint,
   ativo tinyint
)
GO
CREATE TABLE LogEtapaMeioAmbiente
(
   id int,
   logVersao bigint NOT NULL,
   logAcao smallint,
   tipo int,
   meioAmbiente_id int
)
GO
CREATE TABLE LogFaturamentoAnterior
(
   id int,
   logVersao bigint NOT NULL,
   logAcao smallint,
   ano int,
   tipo int,
   valor decimal(18,0),
   financeiro_id int
)
GO
CREATE TABLE LogFaturamentoPrevisto
(
   id int,
   logVersao bigint NOT NULL,
   logAcao smallint,
   ano int,
   tipo int,
   valor decimal(18,0),
   valorRealizado decimal(18,0),
   financeiro_id int
)
GO
CREATE TABLE LogFinanceiro
(
   id int,
   logVersao bigint NOT NULL,
   logAcao smallint,
   dataUltimaAlteracao datetime,
   observacao text,
   usuarioResponsavel_id int,
   projeto_id int
)
GO
CREATE TABLE LogFinanceiroAnexo
(
   logVersao bigint NOT NULL,
   financeiro_id int,
   anexo_id int,
   logAcao smallint
)
GO
CREATE TABLE LogGrupoUsuario
(
   id int,
   logVersao bigint NOT NULL,
   logAcao smallint,
   descricao varchar(255)
)
GO
CREATE TABLE LogHistoricoSituacaoInstrumentoFormalizacao
(
   id int,
   logVersao bigint NOT NULL,
   logAcao smallint,
   dataInicioSituacao datetime,
   justificativa varchar(4000),
   instrumentoFormalizacao_id int,
   situacaoInstrumentoFormalizacao_id int
)
GO
CREATE TABLE LogHistoricoSituacaoProjeto
(
   id int,
   logVersao bigint NOT NULL,
   logAcao smallint,
   data datetime,
   justificativa varchar(4000),
   projeto_id int,
   situacaoProjeto_id int
)
GO
CREATE TABLE LogInformacaoAdicional
(
   id int,
   logVersao bigint NOT NULL,
   logAcao smallint,
   dataUltimaAlteracao datetime,
   assunto varchar(255),
   data datetime,
   descricao varchar(4000),
   tipoContato int,
   usuarioResponsavel_id int,
   empresa_id int
)
GO
CREATE TABLE LogInfraestrutura
(
   id int,
   logVersao bigint NOT NULL,
   logAcao smallint,
   dataUltimaAlteracao datetime,
   areaAlagada tinyint,
   areaConstruida decimal(18,0),
   areaNecessaria decimal(18,0),
   consumoAguaEstimado decimal(18,0),
   consumoCombustivelEstimado decimal(18,0),
   consumoGasEstimado decimal(18,0),
   contatoCemig tinyint,
   demandaEnergiaEstimada decimal(18,0),
   energiaJaContratada tinyint,
   geracaoEnergiaPropria decimal(18,0),
   geracaoEnergiaVenda decimal(18,0),
   ofertaEnergiaVendaEstimada decimal(18,0),
   possuiTerreno tinyint,
   potenciaEnergeticaEstimada decimal(18,0),
   usuarioResponsavel_id int,
   projeto_id int
)
GO
CREATE TABLE LogInstrumentoFormalizacao
(
   id int,
   logVersao bigint NOT NULL,
   logAcao smallint,
   dataUltimaAlteracao datetime,
   dataAssinatura datetime,
   dataAssinaturaPrevista datetime,
   localDocumento varchar(255),
   possuiContrapartidaFincGoverno tinyint,
   protocolo varchar(10),
   situacaoAtual_id int,
   tipo int,
   titulo varchar(255),
   ultimaVersao tinyint,
   versao int,
   usuarioResponsavel_id int,
   historicoSituacaoInstrumentoFormalizacao_id int
)
GO
CREATE TABLE LogInsumo
(
   id int,
   logVersao bigint NOT NULL,
   logAcao smallint,
   Nome varchar(200),
   empresaFornecedor varchar(255),
   origem int,
   quantidadeAno int,
   unidadeMedida varchar(50),
   valorEstimadoAno decimal(18,0),
   insumoProduto_id int,
   ncm_id int
)
GO
CREATE TABLE LogInsumoProduto
(
   id int,
   logVersao bigint NOT NULL,
   logAcao smallint,
   dataUltimaAlteracao datetime,
   aquisicaoOutroEstado tinyint,
   contribuinteSubstituto tinyint,
   importacaoProduto tinyint,
   interesseParceria tinyint,
   percentualAdquiridoOutro decimal(18,0),
   percentualClienteComercial decimal(18,0),
   percentualClienteConsumidor decimal(18,0),
   percentualClienteIndustria decimal(18,0),
   percentualFabricaMinas decimal(18,0),
   percentualICMS decimal(18,0),
   percentualImportado decimal(18,0),
   percentualMercadoExterior decimal(18,0),
   percentualMercadoMinas decimal(18,0),
   percentualMercadoNacional decimal(18,0),
   percentualOrigemImportado decimal(18,0),
   percentualOrigemMinas decimal(18,0),
   percentualOrigemOutros decimal(18,0),
   percentualTributoEfetivo decimal(18,0),
   percentualValorAgregado decimal(18,0),
   usuarioResponsavel_id int,
   projeto_id int
)
GO
CREATE TABLE LogInvestimentoPrevisto
(
   id int,
   logVersao bigint NOT NULL,
   logAcao smallint,
   ano int,
   tipo int,
   valor decimal(18,0),
   financeiro_id int,
   valorRealizado decimal(18,0)
)
GO
CREATE TABLE LogLocal
(
   id int,
   logVersao bigint NOT NULL,
   logAcao smallint,
   ativo tinyint,
   descricao varchar(255)
)
GO
CREATE TABLE LogLocalizacao
(
   id int,
   logVersao bigint NOT NULL,
   logAcao smallint,
   dataUltimaAlteracao datetime,
   aDefinir tinyint,
   latitude varchar(6),
   localizacaoSecundaria varchar(255),
   longetude varchar(6),
   poligono varchar(1000),
   usuarioResponsavel_id int,
   microRegiao_id int,
   municipio_id int,
   projeto_id int,
   regiaoPlanejamento_id int
)
GO
CREATE TABLE LogMeioAmbiente
(
   id int,
   logVersao bigint NOT NULL,
   logAcao smallint,
   dataUltimaAlteracao datetime,
   classeEmpreendimento int,
   dataEntregaEstudos datetime,
   dataEntregaInformacao datetime,
   dataPedidoInformacao datetime,
   dataRealizadaVistoria datetime,
   licenciamentoIniciado tinyint,
   necessidadeEIARIMA tinyint,
   observacao text,
   pedidoInformacoesComplementar tinyint,
   realizadaVistoria tinyint,
   usuarioResponsavel_id int,
   projeto_id int
)
GO
CREATE TABLE LogMeioAmbienteAnexo
(
   logVersao bigint NOT NULL,
   MeioAmbiente_id int,
   anexo_id int,
   logAcao smallint
)
GO
CREATE TABLE LogMeta
(
   id int,
   logVersao bigint NOT NULL,
   logAcao smallint,
   ano int,
   estagio_id int,
   indicador_id int,
   mes int,
   meta decimal(18,0)
)
GO
CREATE TABLE LogMicroRegiao
(
   id int,
   logVersao bigint NOT NULL,
   logAcao smallint,
   nome varchar(255),
   regiaoPlanejamento_id int
)
GO
CREATE TABLE LogMunicipio
(
   id int,
   logVersao bigint NOT NULL,
   logAcao smallint,
   nome varchar(255),
   microRegiao_id int,
   regiaoPlanejamento_id int,
   uf_id int
)
GO
CREATE TABLE LogNaturezaJuridica
(
   id int,
   logVersao bigint NOT NULL,
   logAcao smallint,
   descricao varchar(50)
)
GO
CREATE TABLE LogNCM
(
   id int,
   logVersao bigint NOT NULL,
   logAcao smallint,
   descricao varchar(255)
)
GO
CREATE TABLE LogNomeEmpresa
(
   id int,
   logVersao bigint NOT NULL,
   logAcao smallint,
   nome varchar(255),
   empresa_id int
)
GO
CREATE TABLE LogOrigemRecurso
(
   id int,
   logVersao bigint NOT NULL,
   logAcao smallint,
   origemNacional tinyint,
   percentualInvestimento decimal(18,0),
   valorInvestimento decimal(18,0),
   financeiro_id int,
   pais_id int,
   uf_id int
)
GO
CREATE TABLE LogPais
(
   id int,
   logVersao bigint NOT NULL,
   logAcao smallint,
   nome varchar(255)
)
GO
CREATE TABLE LogParceria
(
   id int,
   logVersao bigint NOT NULL,
   logAcao smallint,
   descricao varchar(200),
   insumoProduto_id int
)
GO
CREATE TABLE LogPerguntaPesquisa
(
   id int,
   logVersao bigint NOT NULL,
   logAcao smallint,
   pergunta varchar(1000),
   tipo int
)
GO
CREATE TABLE LogPesquisa
(
   id int,
   logVersao bigint NOT NULL,
   logAcao smallint,
   comentario text,
   contatoExternoResponsavel_id int,
   dataResposta datetime,
   dataSolicitacao datetime,
   dataUltimaAlteracao datetime,
   indiceSatisfacao decimal(18,0),
   instrumentoFormalizacao_id int
)
GO
CREATE TABLE LogPesquisaPerguntaPesquisa
(
   perguntaPesquisa_id int,
   pesquisa_id int,
   logVersao bigint NOT NULL,
   logAcao smallint,
   justificativa varchar(256),
   resposta int
)
GO
CREATE TABLE LogPleito
(
   id int,
   logVersao bigint NOT NULL,
   logAcao smallint,
   dataUltimaAlteracao datetime,
   observacoes text,
   pleitos text,
   usuarioResponsavel_id int,
   projeto_id int
)
GO
CREATE TABLE LogProducao
(
   id int,
   logVersao bigint NOT NULL,
   logAcao smallint,
   ano int,
   areaPlantada decimal(18,0),
   geracaoEnergia decimal(18,0),
   moagemCana decimal(18,0),
   outro decimal(18,0),
   producaoAcucar decimal(18,0),
   producaoEtanol decimal(18,0),
   insumoProduto_id int
)
GO
CREATE TABLE LogProduto
(
   tipo int NOT NULL,
   id int,
   logVersao bigint NOT NULL,
   logAcao smallint,
   nome varchar(200),
   quantidade1 int,
   quantidade2 int,
   quantidade3 int,
   sujeitoST tinyint,
   unidadeMedida varchar(50),
   insumoProduto_id int,
   ncm_id int
)
GO
CREATE TABLE LogProjeto
(
   id int,
   logVersao bigint NOT NULL,
   logAcao smallint,
   dataUltimaAlteracao datetime,
   dataCadastro datetime,
   descricao varchar(4000),
   estagioAtual_id int,
   informacaoPrimeiroContato varchar(4000),
   nome varchar(255),
   prospeccaoAtiva tinyint,
   situacaoAtual_id int,
   tipo int,
   ultimaVersao tinyint,
   versao int,
   familia_id int,
   usuarioResponsavel_id int,
   cadeiaProdutiva_id int,
   empresa_id int,
   historicosituacaoprojeto_id int,
   ultimaAtividade_id int,
   unidade_id int,
   usuarioInvestimento_id int
)
GO
CREATE TABLE LogPROJETOINSTRUMENTOFORMALIZACAO
(
   logVersao bigint NOT NULL,
   instrumentoFormalizacao_id int,
   projeto_id int,
   logAcao smallint
)
GO
CREATE TABLE LogPublicacao
(
   id int,
   logVersao bigint NOT NULL,
   logAcao smallint,
   dataUltimaAlteracao datetime,
   dataPublicacao datetime,
   usuarioResponsavel_id int,
   instrumentoFormalizacao_id int
)
GO
CREATE TABLE LogPublicacaoAnexo
(
   logVersao bigint NOT NULL,
   publicacao_id int,
   anexo_id int,
   logAcao smallint
)
GO
CREATE TABLE LogRegiaoPlanejamento
(
   id int,
   logVersao bigint NOT NULL,
   logAcao smallint,
   nome varchar(255)
)
GO
CREATE TABLE LogRelatorioAcompanhamento
(
   id int,
   logVersao bigint NOT NULL,
   logAcao smallint,
   dataUltimaAlteracao datetime,
   aceiteTermo tinyint,
   ano int,
   dataFimPrimeiraCampanha datetime,
   dataFimSegundaCampanha datetime,
   dataInicioPrimeiraCampanha datetime,
   dataInicioSegundaCampanha datetime,
   dataValidacao datetime,
   emailResponsavelRelatorio varchar(50),
   nomeResponsavelRelatorio varchar(100),
   observacao text,
   situacaoAtual_id int,
   telefoneResponsavelRelatorio varchar(18),
   termoConfiabilidade varchar(255),
   usuarioResponsavel_id int,
   projeto_id int,
   familia_id int
)
GO
CREATE TABLE LogRelatorioAcompanhamentoSituacao
(
   relatorioAcompanhamento_id int,
   situacaoRelatorio_id int,
   logVersao bigint NOT NULL,
   logAcao smallint
)
GO
CREATE TABLE LogRevisao
(
   id bigint PRIMARY KEY NOT NULL Identity(1,1),
   logDataUltimaAlteracao datetime NOT NULL,
   logUsuarioResponsavel varchar(100) NOT NULL
)
GO
CREATE TABLE LogServico
(
   id int,
   logVersao bigint NOT NULL,
   logAcao smallint,
   descricao varchar(200),
   insumoProduto_id int
)
GO
CREATE TABLE LogSignatario
(
   id int,
   logVersao bigint NOT NULL,
   logAcao smallint,
   cargo varchar(255),
   email varchar(255),
   nome varchar(255),
   telefone1 varchar(255),
   telefone2 varchar(255),
   instrumentoFormalizacao_id int
)
GO
CREATE TABLE LogSituacaoInstrumentoFormalizacao
(
   id int,
   logVersao bigint NOT NULL,
   logAcao smallint,
   descricao varchar(100)
)
GO
CREATE TABLE LogTemplate
(
   id int,
   logVersao bigint NOT NULL,
   logAcao smallint,
   dataUltimaAlteracao datetime,
   nome varchar(100),
   status tinyint,
   tipo int,
   usuarioResponsavel_id int
)
GO
CREATE TABLE LogUF
(
   id int,
   logVersao bigint NOT NULL,
   logAcao smallint,
   nome varchar(255),
   sigla varchar(5),
   pais_id int
)
GO
CREATE TABLE LogUnidadeEmpresa
(
   id int,
   logVersao bigint NOT NULL,
   logAcao smallint,
   dataUltimaAlteracao datetime,
   email varchar(255),
   nome varchar(255),
   telefone varchar(18),
   usuarioResponsavel_id int,
   empresa_id int,
   endereco_id int
)
GO
CREATE TABLE LogUsoFonte
(
   id int,
   logVersao bigint NOT NULL,
   logAcao smallint,
   tipo int,
   valorARealizar decimal(18,0),
   valorRealizado decimal(18,0),
   financeiro_id int
)
GO
CREATE TABLE LogUsuario
(
   tipo int NOT NULL,
   id int,
   logVersao bigint NOT NULL,
   logAcao smallint,
   dataUltimaAlteracao datetime,
   ativo tinyint,
   email varchar(255),
   mailing tinyint,
   nome varchar(50),
   senha varchar(128),
   telefone varchar(18),
   telefone2 varchar(18),
   usuarioResponsavel_id int,
   cargo_id int,
   grupousuario_id int
)
GO
CREATE TABLE LogUsuarioExterno
(
   id int,
   logVersao bigint NOT NULL,
   cargoExterno varchar(100),
   dataBloqueioTemporario datetime,
   empresa_id int,
   unidade_id int
)
GO
CREATE TABLE LogUsuarioInterno
(
   id int,
   logVersao bigint NOT NULL,
   matricula varchar(15),
   departamento_id int
)
GO
CREATE TABLE LogUsuarioProjetoRecente
(
   projeto_id int,
   usuario_id int,
   logVersao bigint NOT NULL,
   logAcao smallint,
   dataAcesso datetime
)
GO
CREATE TABLE LogVerificationToken
(
   id int,
   logVersao bigint NOT NULL,
   logAcao smallint,
   expiryDate datetime,
   token varchar(36),
   tokenType varchar(255),
   verified tinyint,
   user_id int
)
GO
ALTER TABLE LogAccessToken
ADD CONSTRAINT FK_c801nrf4efbg8gcitb51smn4o
FOREIGN KEY (logVersao)
REFERENCES LogRevisao(id)
GO
ALTER TABLE LogAnexo
ADD CONSTRAINT FK_p7hbejn8svbunj7h9n801tsx2
FOREIGN KEY (logVersao)
REFERENCES LogRevisao(id)
GO
ALTER TABLE LogAnexoInformacaoAdicional
ADD CONSTRAINT FK_e8imd8s2ewe9kpxm2j2e1129l
FOREIGN KEY (logVersao)
REFERENCES LogRevisao(id)
GO
ALTER TABLE LogAnexoLocalizacao
ADD CONSTRAINT FK_neprwcj56ufj8y6e1n4gsps4l
FOREIGN KEY (logVersao)
REFERENCES LogRevisao(id)
GO
ALTER TABLE LogAnexoPesquisa
ADD CONSTRAINT FK_khd45lvq779aa0mo161pvsc8m
FOREIGN KEY (logVersao)
REFERENCES LogRevisao(id)
GO
ALTER TABLE LogAnexoProjeto
ADD CONSTRAINT FK_8rjqs5npvxfqm4r6alywvnsf5
FOREIGN KEY (logVersao)
REFERENCES LogRevisao(id)
GO
ALTER TABLE LogAnexoRelatorioAcompanhamento
ADD CONSTRAINT FK_et3dvjiqvpki69ba89o45gk8a
FOREIGN KEY (logVersao)
REFERENCES LogRevisao(id)
GO
ALTER TABLE LogAtividadeInstrumento
ADD CONSTRAINT FK_djxlot76187w8a7qfeoui5i3m
FOREIGN KEY (logVersao)
REFERENCES LogRevisao(id)
GO
ALTER TABLE LogAtividadeLocal
ADD CONSTRAINT FK_2x5ln0q8sxblte9ac24cv8frg
FOREIGN KEY (logVersao)
REFERENCES LogRevisao(id)
GO
ALTER TABLE LogAtividadeProjeto
ADD CONSTRAINT FK_k3n209s45oik6qvogkrwtp37p
FOREIGN KEY (logVersao)
REFERENCES LogRevisao(id)
GO
ALTER TABLE LogAtividadeTemplate
ADD CONSTRAINT FK_4t9mvnc64rhtlvpnw8vwhqjxw
FOREIGN KEY (logVersao)
REFERENCES LogRevisao(id)
GO
ALTER TABLE LogCadeiaProdutiva
ADD CONSTRAINT FK_183lc3asxyjvlvp6f4ep9xa1b
FOREIGN KEY (logVersao)
REFERENCES LogRevisao(id)
GO
ALTER TABLE LogCargo
ADD CONSTRAINT FK_rhb7g4javccs9tyjw6sh24mht
FOREIGN KEY (logVersao)
REFERENCES LogRevisao(id)
GO
ALTER TABLE LogCNAE
ADD CONSTRAINT FK_q4adhmh8vam0w7xyves09ilv5
FOREIGN KEY (logVersao)
REFERENCES LogRevisao(id)
GO
ALTER TABLE LogCNAEEmpresa
ADD CONSTRAINT FK_lt3qnu7qt14h3bwycb25q21vf
FOREIGN KEY (logVersao)
REFERENCES LogRevisao(id)
GO
ALTER TABLE LogComposicaoSocietaria
ADD CONSTRAINT FK_49gg2jbxkf892p2jgo29m3g16
FOREIGN KEY (logVersao)
REFERENCES LogRevisao(id)
GO
ALTER TABLE LogConcorrente
ADD CONSTRAINT FK_p93ajw2wxrx8n87etjeowf9jo
FOREIGN KEY (logVersao)
REFERENCES LogRevisao(id)
GO
ALTER TABLE LogCronograma
ADD CONSTRAINT FK_hpffcg78twlhht6532gnb21ns
FOREIGN KEY (logVersao)
REFERENCES LogRevisao(id)
GO
ALTER TABLE LogDepartamento
ADD CONSTRAINT FK_i01ltjjk0ud7817rwx06xhah
FOREIGN KEY (logVersao)
REFERENCES LogRevisao(id)
GO
ALTER TABLE LogEmprego
ADD CONSTRAINT FK_svfrudtrn0kwwlp6os3f0atad
FOREIGN KEY (logVersao)
REFERENCES LogRevisao(id)
GO
ALTER TABLE LogEmpresa
ADD CONSTRAINT FK_pdtpktq05husp5krnadel5jd1
FOREIGN KEY (logVersao)
REFERENCES LogRevisao(id)
GO
ALTER TABLE LogEndereco
ADD CONSTRAINT FK_gt3otgosy50il7c0v9npm1om1
FOREIGN KEY (logVersao)
REFERENCES LogRevisao(id)
GO
ALTER TABLE LogEnergiaContratada
ADD CONSTRAINT FK_d5jqf8686fr0eo13lhchy5vls
FOREIGN KEY (logVersao)
REFERENCES LogRevisao(id)
GO
ALTER TABLE LogEstagioProjeto
ADD CONSTRAINT FK_lecvwcsaivk3xomro6fhru6u5
FOREIGN KEY (logVersao)
REFERENCES LogRevisao(id)
GO
ALTER TABLE LogEtapaMeioAmbiente
ADD CONSTRAINT FK_o44kaofkd45j8p5gr1mqa39xb
FOREIGN KEY (logVersao)
REFERENCES LogRevisao(id)
GO
ALTER TABLE LogFaturamentoAnterior
ADD CONSTRAINT FK_k1cqn0pjeapls0hbuxht6l400
FOREIGN KEY (logVersao)
REFERENCES LogRevisao(id)
GO
ALTER TABLE LogFaturamentoPrevisto
ADD CONSTRAINT FK_syhq56p24xqyhma6egpmhk42r
FOREIGN KEY (logVersao)
REFERENCES LogRevisao(id)
GO
ALTER TABLE LogFinanceiro
ADD CONSTRAINT FK_ihjqsj8bdyy08kigq0qco9vhc
FOREIGN KEY (logVersao)
REFERENCES LogRevisao(id)
GO
ALTER TABLE LogFinanceiroAnexo
ADD CONSTRAINT FK_rg300nvqe7lb5q3iwx2k90xnm
FOREIGN KEY (logVersao)
REFERENCES LogRevisao(id)
GO
ALTER TABLE LogGrupoUsuario
ADD CONSTRAINT FK_9d6uosabrtmap6lmnxtjbxbdt
FOREIGN KEY (logVersao)
REFERENCES LogRevisao(id)
GO
ALTER TABLE LogHistoricoSituacaoInstrumentoFormalizacao
ADD CONSTRAINT FK_ijj9a84gya3j89d5ijqfvoapw
FOREIGN KEY (logVersao)
REFERENCES LogRevisao(id)
GO
ALTER TABLE LogHistoricoSituacaoProjeto
ADD CONSTRAINT FK_d3im8u70q7mxpehjejddc92dk
FOREIGN KEY (logVersao)
REFERENCES LogRevisao(id)
GO
ALTER TABLE LogInformacaoAdicional
ADD CONSTRAINT FK_qw6g7uy5e6u9er7ii3vccpb8l
FOREIGN KEY (logVersao)
REFERENCES LogRevisao(id)
GO
ALTER TABLE LogInfraestrutura
ADD CONSTRAINT FK_i7dr9ograw4bkuw2t1uxxdtd1
FOREIGN KEY (logVersao)
REFERENCES LogRevisao(id)
GO
ALTER TABLE LogInstrumentoFormalizacao
ADD CONSTRAINT FK_3kdf5b24eewlxgrgl4fr9ifm
FOREIGN KEY (logVersao)
REFERENCES LogRevisao(id)
GO
ALTER TABLE LogInsumo
ADD CONSTRAINT FK_mngqik62qg79pic2x69yaiej1
FOREIGN KEY (logVersao)
REFERENCES LogRevisao(id)
GO
ALTER TABLE LogInsumoProduto
ADD CONSTRAINT FK_qvbc4pcjvp05u7094v22nqerx
FOREIGN KEY (logVersao)
REFERENCES LogRevisao(id)
GO
ALTER TABLE LogInvestimentoPrevisto
ADD CONSTRAINT FK_pre1sifis5srbrc652kctag0x
FOREIGN KEY (logVersao)
REFERENCES LogRevisao(id)
GO
ALTER TABLE LogLocal
ADD CONSTRAINT FK_hla1nw3ecl8h7pruihbkxjx76
FOREIGN KEY (logVersao)
REFERENCES LogRevisao(id)
GO
ALTER TABLE LogLocalizacao
ADD CONSTRAINT FK_6bq6bt95553d5k6qe2pfw4xr
FOREIGN KEY (logVersao)
REFERENCES LogRevisao(id)
GO
ALTER TABLE LogMeioAmbiente
ADD CONSTRAINT FK_n5iskqa9d9h9tijxj1jifo51p
FOREIGN KEY (logVersao)
REFERENCES LogRevisao(id)
GO
ALTER TABLE LogMeioAmbienteAnexo
ADD CONSTRAINT FK_7dypfufarw3yo8q64rl5xdadg
FOREIGN KEY (logVersao)
REFERENCES LogRevisao(id)
GO
ALTER TABLE LogMeta
ADD CONSTRAINT FK_1iwpk1ot64tmbvaxc5j5muynt
FOREIGN KEY (logVersao)
REFERENCES LogRevisao(id)
GO
ALTER TABLE LogMicroRegiao
ADD CONSTRAINT FK_qt1u4vxa8peaj6nylrhe5x91i
FOREIGN KEY (logVersao)
REFERENCES LogRevisao(id)
GO
ALTER TABLE LogMunicipio
ADD CONSTRAINT FK_hckkmta64be3nog6xp32m4uku
FOREIGN KEY (logVersao)
REFERENCES LogRevisao(id)
GO
ALTER TABLE LogNaturezaJuridica
ADD CONSTRAINT FK_3qbmg8vhsldpxbj5ukt4gylc4
FOREIGN KEY (logVersao)
REFERENCES LogRevisao(id)
GO
ALTER TABLE LogNCM
ADD CONSTRAINT FK_rfwfxxl4ba4k6ra3nb5cy5q8x
FOREIGN KEY (logVersao)
REFERENCES LogRevisao(id)
GO
ALTER TABLE LogNomeEmpresa
ADD CONSTRAINT FK_aygjxofqgjwm2pr6fvh3xmv65
FOREIGN KEY (logVersao)
REFERENCES LogRevisao(id)
GO
ALTER TABLE LogOrigemRecurso
ADD CONSTRAINT FK_2qanyui9yocm4hd01d99lvne6
FOREIGN KEY (logVersao)
REFERENCES LogRevisao(id)
GO
ALTER TABLE LogPais
ADD CONSTRAINT FK_41n5ui1o9799c5qgpf3qs5xn4
FOREIGN KEY (logVersao)
REFERENCES LogRevisao(id)
GO
ALTER TABLE LogParceria
ADD CONSTRAINT FK_ao74t6oa14rsgax4w52mwi5s2
FOREIGN KEY (logVersao)
REFERENCES LogRevisao(id)
GO
ALTER TABLE LogPerguntaPesquisa
ADD CONSTRAINT FK_j5vja7nxxtjkxsmkb5x1kbcec
FOREIGN KEY (logVersao)
REFERENCES LogRevisao(id)
GO
ALTER TABLE LogPesquisa
ADD CONSTRAINT FK_54hsfbgh9w2j880ab0h8sg5ke
FOREIGN KEY (logVersao)
REFERENCES LogRevisao(id)
GO
ALTER TABLE LogPesquisaPerguntaPesquisa
ADD CONSTRAINT FK_mwgprwr9waj9m4to5egu1becm
FOREIGN KEY (logVersao)
REFERENCES LogRevisao(id)
GO
ALTER TABLE LogPleito
ADD CONSTRAINT FK_gqvw4yp3mt0c95mv8qo78gye2
FOREIGN KEY (logVersao)
REFERENCES LogRevisao(id)
GO
ALTER TABLE LogProducao
ADD CONSTRAINT FK_ngavhprsku73wun4uu956wh6v
FOREIGN KEY (logVersao)
REFERENCES LogRevisao(id)
GO
ALTER TABLE LogProduto
ADD CONSTRAINT FK_ed88t9yfypdl5m2l8n9uih43x
FOREIGN KEY (logVersao)
REFERENCES LogRevisao(id)
GO
ALTER TABLE LogProjeto
ADD CONSTRAINT FK_bd8xa0pithg2f61gysv8j37p4
FOREIGN KEY (logVersao)
REFERENCES LogRevisao(id)
GO
ALTER TABLE LogPROJETOINSTRUMENTOFORMALIZACAO
ADD CONSTRAINT FK_32fdx63kql7qbusah8oxjcr8c
FOREIGN KEY (logVersao)
REFERENCES LogRevisao(id)
GO
ALTER TABLE LogPublicacao
ADD CONSTRAINT FK_54hh1x96p3uay6dvxagyp4eus
FOREIGN KEY (logVersao)
REFERENCES LogRevisao(id)
GO
ALTER TABLE LogPublicacaoAnexo
ADD CONSTRAINT FK_i58nx51vj92jw5msr9k47e51j
FOREIGN KEY (logVersao)
REFERENCES LogRevisao(id)
GO
ALTER TABLE LogRegiaoPlanejamento
ADD CONSTRAINT FK_k9ydnex5dvitfcytnmfift7af
FOREIGN KEY (logVersao)
REFERENCES LogRevisao(id)
GO
ALTER TABLE LogRelatorioAcompanhamento
ADD CONSTRAINT FK_4say5q48ja95hei3egrtvx0u2
FOREIGN KEY (logVersao)
REFERENCES LogRevisao(id)
GO
ALTER TABLE LogRelatorioAcompanhamentoSituacao
ADD CONSTRAINT FK_naay0igaofy1cj9ggk028mk9
FOREIGN KEY (logVersao)
REFERENCES LogRevisao(id)
GO
CREATE UNIQUE INDEX PK__LogRevis__3213E83F10EC3765 ON LogRevisao(id)
GO
ALTER TABLE LogServico
ADD CONSTRAINT FK_4k6ujmqp8bx7dke21u6tshrm6
FOREIGN KEY (logVersao)
REFERENCES LogRevisao(id)
GO
ALTER TABLE LogSignatario
ADD CONSTRAINT FK_jodcrdojho4l6t0vrguinn239
FOREIGN KEY (logVersao)
REFERENCES LogRevisao(id)
GO
ALTER TABLE LogSituacaoInstrumentoFormalizacao
ADD CONSTRAINT FK_gecahvigedpkibh4x0dho5nsb
FOREIGN KEY (logVersao)
REFERENCES LogRevisao(id)
GO
ALTER TABLE LogTemplate
ADD CONSTRAINT FK_c2ufk06eys540rrgf1i9vpto1
FOREIGN KEY (logVersao)
REFERENCES LogRevisao(id)
GO
ALTER TABLE LogUF
ADD CONSTRAINT FK_i2lano5new0qsuyhwvw7vx0it
FOREIGN KEY (logVersao)
REFERENCES LogRevisao(id)
GO
ALTER TABLE LogUnidadeEmpresa
ADD CONSTRAINT FK_dav99fra93gw47s3cgjdg0nc6
FOREIGN KEY (logVersao)
REFERENCES LogRevisao(id)
GO
ALTER TABLE LogUsoFonte
ADD CONSTRAINT FK_gsaq7yae953rk5x5via5bm4i2
FOREIGN KEY (logVersao)
REFERENCES LogRevisao(id)
GO
ALTER TABLE LogUsuario
ADD CONSTRAINT FK_c4uidqh6e2mwy2xt66uamh884
FOREIGN KEY (logVersao)
REFERENCES LogRevisao(id)
GO
ALTER TABLE LogUsuarioProjetoRecente
ADD CONSTRAINT FK_7kxm7tn2c83rfafmei5auggsl
FOREIGN KEY (logVersao)
REFERENCES LogRevisao(id)
GO
ALTER TABLE LogVerificationToken
ADD CONSTRAINT FK_hh86cnf3ryw7f8iphks6cllry
FOREIGN KEY (logVersao)
REFERENCES LogRevisao(id)
GO
