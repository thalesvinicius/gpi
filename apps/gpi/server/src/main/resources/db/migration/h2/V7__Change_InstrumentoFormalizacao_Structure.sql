alter table INSTRUMENTOFORMALIZACAO add situacaoAtual_id int;  -- codigo do enum da situacao

alter table INSTRUMENTOFORMALIZACAO add historicoSituacaoInstrumentoFormalizacao_id int;  --foreign key para a situacao atual no historico


update INSTRUMENTOFORMALIZACAO set situacaoAtual_id = situacao;

alter table INSTRUMENTOFORMALIZACAO alter column situacaoAtual_id int not null;

alter table INSTRUMENTOFORMALIZACAO drop column situacao;

--  Create Tables 
CREATE TABLE SituacaoInstrumentoFormalizacao ( 
	id int NOT NULL,    --  Identificador da Situação 
	descricao varchar(100) NOT NULL    --  Descrição da Situação do intrumento. 
)
;

CREATE TABLE HistoricoSituacaoInstrumentoFormalizacao ( 
	id int identity(1,1)  NOT NULL,    -- Identificador incremental do relacionamento 
	situacaoInstrumentoFormalizacao_id int NOT NULL,    -- Código da Situação 
	instrumentoFormalizacao_id int NOT NULL,    -- Identificador do Instrumento de Formalização 
	dataInicioSituacao datetime NOT NULL,    -- Data de início da situação 
	justificativa varchar(1000)    -- Justificativa 
)
;