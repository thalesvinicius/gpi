--Endereço
insert into Endereco (municipio_id,uf_id,logradouro,numero,complemento,CEP,bairro,cidade,pais_id) values (198, 35, 'Rua Desembargador Jorge Fontana 1', '600', '5º Andar', '30320670', 'Belvedere', 'Belo Horizonte', 2);
insert into Endereco (municipio_id,uf_id,logradouro,numero,complemento,CEP,bairro,cidade,pais_id) values (198, 35, 'Rua Desembargador Jorge Fontana 2', '600', '5º Andar', '30320670', 'Belvedere', 'Belo Horizonte', 2);
insert into Endereco (municipio_id,uf_id,logradouro,numero,complemento,CEP,bairro,cidade,pais_id) values (198, 35, 'Rua Desembargador Jorge Fontana 3', '600', '5º Andar', '30320670', 'Belvedere', 'Belo Horizonte', 2);
insert into Endereco (municipio_id,uf_id,logradouro,numero,complemento,CEP,bairro,cidade,pais_id) values (198, 35, 'Rua Desembargador Jorge Fontana 4', '600', '5º Andar', '30320670', 'Belvedere', 'Belo Horizonte', 2);
insert into Endereco (municipio_id,uf_id,logradouro,numero,complemento,CEP,bairro,cidade,pais_id) values (198, 35, 'Rua Desembargador Jorge Fontana 5', '600', '5º Andar', '30320670', 'Belvedere', 'Belo Horizonte', 2);
insert into Endereco (municipio_id,uf_id,logradouro,numero,complemento,CEP,bairro,cidade,pais_id) values (198, 35, 'Rua Desembargador Jorge Fontana 6', '600', '5º Andar', '30320670', 'Belvedere', 'Belo Horizonte', 2);
insert into Endereco (municipio_id,uf_id,logradouro,numero,complemento,CEP,bairro,cidade,pais_id) values (198, 35, 'Rua Desembargador Jorge Fontana 7', '600', '5º Andar', '30320670', 'Belvedere', 'Belo Horizonte', 2);
insert into Endereco (municipio_id,uf_id,logradouro,numero,complemento,CEP,bairro,cidade,pais_id) values (198, 35, 'Rua Desembargador Jorge Fontana 8', '600', '5º Andar', '30320670', 'Belvedere', 'Belo Horizonte', 2);
insert into Endereco (municipio_id,uf_id,logradouro,numero,complemento,CEP,bairro,cidade,pais_id) values (198, 35, 'Rua Desembargador Jorge Fontana 9', '600', '5º Andar', '30320670', 'Belvedere', 'Belo Horizonte', 2);
insert into Endereco (municipio_id,uf_id,logradouro,numero,complemento,CEP,bairro,cidade,pais_id) values (198, 35, 'Rua Desembargador Jorge Fontana 10', '600', '5º Andar', '30320670', 'Belvedere', 'Belo Horizonte', 2);
insert into Endereco (municipio_id,uf_id,logradouro,numero,complemento,CEP,bairro,cidade,pais_id) values (198, 35, 'Rua Desembargador Jorge Fontana 11', '600', '5º Andar', '30320670', 'Belvedere', 'Belo Horizonte', 2);
insert into Endereco (municipio_id,uf_id,logradouro,numero,complemento,CEP,bairro,cidade,pais_id) values (198, 35, 'Rua Desembargador Jorge Fontana 12', '600', '5º Andar', '30320670', 'Belvedere', 'Belo Horizonte', 2);
insert into Endereco (municipio_id,uf_id,logradouro,numero,complemento,CEP,bairro,cidade,pais_id) values (198, 35, 'Rua Desembargador Jorge Fontana 13', '600', '5º Andar', '30320670', 'Belvedere', 'Belo Horizonte', 2);
insert into Endereco (municipio_id,uf_id,logradouro,numero,complemento,CEP,bairro,cidade,pais_id) values (198, 35, 'Rua Desembargador Jorge Fontana 14', '600', '5º Andar', '30320670', 'Belvedere', 'Belo Horizonte', 2);
insert into Endereco (municipio_id,uf_id,logradouro,numero,complemento,CEP,bairro,cidade,pais_id) values (198, 35, 'Rua Desembargador Jorge Fontana 15', '600', '5º Andar', '30320670', 'Belvedere', 'Belo Horizonte', 2);
insert into Endereco (municipio_id,uf_id,logradouro,numero,complemento,CEP,bairro,cidade,pais_id) values (198, 35, 'Rua Desembargador Jorge Fontana 16', '600', '5º Andar', '30320670', 'Belvedere', 'Belo Horizonte', 2);
insert into Endereco (municipio_id,uf_id,logradouro,numero,complemento,CEP,bairro,cidade,pais_id) values (198, 35, 'Rua Desembargador Jorge Fontana 17', '600', '5º Andar', '30320670', 'Belvedere', 'Belo Horizonte', 2);

--Empresa
insert into Empresa (naturezaJuridica_id,cadeiaProdutiva_id,endereco_id,anexo_id,
                    cnpj, razaoSocial, inscricaoEstadual, nomePrincipal, 
                    composicaoSocietaria, historico, situacao, justificativaArquivamento,
                    empresaMineira, empresaInternacional, usuarioResponsavel_id, dataUltimaAlteracao) 
values (1, 1, 1, null, '78425986003615', 'Empresa Teste', 'ISENTO', 'Empresa Teste',
        'teste',
        'historico teste', 1, 'Justificativa de Arquivamento', 0, 1, 1, '2014-07-03');
insert into Empresa (naturezaJuridica_id,cadeiaProdutiva_id,endereco_id,anexo_id,
                    cnpj, razaoSocial, inscricaoEstadual, nomePrincipal, 
                    composicaoSocietaria, historico, situacao, justificativaArquivamento,
                    empresaMineira, empresaInternacional, usuarioResponsavel_id, dataUltimaAlteracao) 
values (1, 1, 2, null, '78425986003615', 'Empresa Teste 2', 'ISENTO', 'Empresa Teste 2',
        'teste',
        'historico teste', 1, 'Justificativa de Arquivamento', 0, 0, 1, '2014-07-03');
insert into Empresa (naturezaJuridica_id,cadeiaProdutiva_id,endereco_id,anexo_id,
                    cnpj, razaoSocial, inscricaoEstadual, nomePrincipal, 
                    composicaoSocietaria, historico, situacao, justificativaArquivamento,
                    empresaMineira, empresaInternacional, usuarioResponsavel_id, dataUltimaAlteracao) 
values (1, 1, 3, null, '78425986003615', 'Empresa Teste 3', 'ISENTO', 'Empresa Teste 3',
        'teste',
        'historico teste', 1, 'Justificativa de Arquivamento', 0, 0, 1, '2014-07-03');
insert into Empresa (naturezaJuridica_id,cadeiaProdutiva_id,endereco_id,anexo_id,
                    cnpj, razaoSocial, inscricaoEstadual, nomePrincipal,
                    composicaoSocietaria, historico, situacao, justificativaArquivamento,
                    empresaMineira, empresaInternacional, usuarioResponsavel_id, dataUltimaAlteracao) 
values (1, 1, 4, null, '78425986003615', 'Empresa Teste 4', 'ISENTO', 'Empresa Teste 4',
        'teste 4',
        'historico teste4', 1, 'Justificativa de Arquivamento', 0, 0, 1, '2014-07-03');
insert into Empresa (naturezaJuridica_id,cadeiaProdutiva_id,endereco_id,anexo_id,
                    cnpj, razaoSocial, inscricaoEstadual, nomePrincipal,
                    composicaoSocietaria, historico, situacao, justificativaArquivamento,
                    empresaMineira, empresaInternacional, usuarioResponsavel_id, dataUltimaAlteracao) 
values (1, 1, 5, null, '78425986003615', 'Empresa Teste 5', 'ISENTO', 'Empresa Teste 5',
        'teste 5',
        'historico teste 5', 1, 'Justificativa de Arquivamento', 0, 0, 1, '2014-07-03');

-- --ContatoPessoa
-- insert into ContatoPessoa (nome, cargo, telefone, telefoneadicional, email)
-- values('Contato 1', 'Cargo 1', '(123)4564-5688', '(123)4564-5688', 'exemplo@teste.com');
-- insert into ContatoPessoa (nome, cargo, telefone, telefoneadicional, email)
-- values('Contato 2', 'Cargo 1', '(123)4564-5688', '(123)4564-5688', 'exemplo@teste.com');
-- insert into ContatoPessoa (nome, cargo, telefone, telefoneadicional, email)
-- values('Contato 3', 'Cargo 1', '(123)4564-5688', '(123)4564-5688', 'exemplo@teste.com');
-- insert into ContatoPessoa (nome, cargo, telefone, telefoneadicional, email)
-- values('Contato 4', 'Cargo 1', '(123)4564-5688', '(123)4564-5688', 'exemplo@teste.com');
-- insert into ContatoPessoa (nome, cargo, telefone, telefoneadicional, email)
-- values('Contato 5', 'Cargo 1', '(123)4564-5688', '(123)4564-5688', 'exemplo@teste.com');
-- insert into ContatoPessoa (nome, cargo, telefone, telefoneadicional, email)
-- values('Contato 6', 'Cargo 1', '(123)4564-5688', '(123)4564-5688', 'exemplo@teste.com');
-- insert into ContatoPessoa (nome, cargo, telefone, telefoneadicional, email)
-- values('Contato 7', 'Cargo 1', '(123)4564-5688', '(123)4564-5688', 'exemplo@teste.com');
-- insert into ContatoPessoa (nome, cargo, telefone, telefoneadicional, email)
-- values('Contato 8', 'Cargo 1', '(123)4564-5688', '(123)4564-5688', 'exemplo@teste.com');
-- insert into ContatoPessoa (nome, cargo, telefone, telefoneadicional, email)
-- values('Contato 9', 'Cargo 1', '(123)4564-5688', '(123)4564-5688', 'exemplo@teste.com');
-- insert into ContatoPessoa (nome, cargo, telefone, telefoneadicional, email)
-- values('Contato 10', 'Cargo 1', '(123)4564-5688', '(123)4564-5688', 'exemplo@teste.com');
-- 
-- insert into ContatoPessoaEmpresa (contatoPessoa_id, empresa_id) values(1, 1);
-- insert into ContatoPessoaEmpresa (contatoPessoa_id, empresa_id) values(2, 1);
-- insert into ContatoPessoaEmpresa (contatoPessoa_id, empresa_id) values(3, 1);
-- insert into ContatoPessoaEmpresa (contatoPessoa_id, empresa_id) values(4, 2);
-- insert into ContatoPessoaEmpresa (contatoPessoa_id, empresa_id) values(5, 2);
-- insert into ContatoPessoaEmpresa (contatoPessoa_id, empresa_id) values(6, 2);
-- 
-- insert into ContatoPessoaUnidadeEmpresa (contatoPessoa_id, unidadeEmpresa_id) values(7, 3);
-- insert into ContatoPessoaUnidadeEmpresa (contatoPessoa_id, unidadeEmpresa_id) values(8, 3);
-- insert into ContatoPessoaUnidadeEmpresa (contatoPessoa_id, unidadeEmpresa_id) values(9, 3);
-- insert into ContatoPessoaUnidadeEmpresa (contatoPessoa_id, unidadeEmpresa_id) values(10, 3);

--InformacaoAdicional
insert into InformacaoAdicional (empresa_id,tipoContato,assunto,data,descricao,usuarioResponsavel_id,dataUltimaAlteracao) 
values (1, 1, 'Assunto do tipo contato 1111', '2014-07-04', 'Descrição do contato 1', 1, '2014-07-03');
insert into InformacaoAdicional (empresa_id,tipoContato,assunto,data,descricao,usuarioResponsavel_id,dataUltimaAlteracao) 
values (1, 1, 'Assunto do tipo contato 2222', '2014-07-04', 'Descrição do contato 2', 1, '2014-07-03');
insert into InformacaoAdicional (empresa_id,tipoContato,assunto,data,descricao,usuarioResponsavel_id,dataUltimaAlteracao) 
values (1, 1, 'Assunto do tipo contato 3333', '2014-07-04', 'Descrição do contato 3', 1, '2014-07-03');
insert into InformacaoAdicional (empresa_id,tipoContato,assunto,data,descricao,usuarioResponsavel_id,dataUltimaAlteracao) 
values (1, 1, 'Assunto do tipo contato 4444', '2014-07-04', 'Descrição do contato 4', 1, '2014-07-03');
insert into InformacaoAdicional (empresa_id,tipoContato,assunto,data,descricao,usuarioResponsavel_id,dataUltimaAlteracao) 
values (1, 1, 'Assunto do tipo contato 5555', '2014-07-04', 'Descrição do contato 5', 1, '2014-07-03');

--NomeEmpresa
insert into NomeEmpresa (empresa_id, nome) 
values (1, 'Nome empresa 1');
insert into NomeEmpresa (empresa_id, nome) 
values (1, 'Nome empresa 2');
insert into NomeEmpresa (empresa_id, nome) 
values (1, 'Nome empresa 3');
insert into NomeEmpresa (empresa_id, nome) 
values (1, 'Nome empresa 4');
insert into NomeEmpresa (empresa_id, nome) 
values (2, 'Nome empresa 5');
insert into NomeEmpresa (empresa_id, nome) 
values (2, 'Nome empresa 6');
insert into NomeEmpresa (empresa_id, nome) 
values (2, 'Nome empresa 1');
insert into NomeEmpresa (empresa_id, nome) 
values (2, 'Nome empresa 2');
insert into NomeEmpresa (empresa_id, nome) 
values (2, 'Nome empresa 3');
insert into NomeEmpresa (empresa_id, nome) 
values (3, 'Nome empresa 4');
insert into NomeEmpresa (empresa_id, nome) 
values (3, 'Nome empresa 5');
insert into NomeEmpresa (empresa_id, nome) 
values (3, 'Nome empresa 6');

--UnidadeEmpresa
insert into UnidadeEmpresa (id,empresa_id,endereco_id,nome,email,telefone,usuarioResponsavel_id,dataUltimaAlteracao) 
values (1, 1, 6, 'Empresa de teste 1', 'empresa@teste.com.br', '553155555555', 1, '2014-07-03');
insert into UnidadeEmpresa (id,empresa_id,endereco_id,nome,email,telefone,usuarioResponsavel_id,dataUltimaAlteracao) 
values (2, 1, 7, 'Empresa de teste 2', 'empresa@teste.com.br', '553155555555', 1, '2014-07-03');
insert into UnidadeEmpresa (id,empresa_id,endereco_id,nome,email,telefone,usuarioResponsavel_id,dataUltimaAlteracao) 
values (3, 2, 8, 'Empresa de teste 3', 'empresa@teste.com.br', '553155555555', 1, '2014-07-03');
insert into UnidadeEmpresa (id,empresa_id,endereco_id,nome,email,telefone,usuarioResponsavel_id,dataUltimaAlteracao) 
values (4, 2, 9, 'Empresa de teste 4', 'empresa@teste.com.br', '553155555555', 1, '2014-07-03');
insert into UnidadeEmpresa (id,empresa_id,endereco_id,nome,email,telefone,usuarioResponsavel_id,dataUltimaAlteracao) 
values (5, 3, 10, 'Empresa de teste 5', 'empresa@teste.com.br', '553155555555', 1, '2014-07-03');

--Projeto
insert into Projeto (usuarioInvestimento_id,empresa_id,unidade_id,nome,
                    descricao,situacaoAtual_id,prospeccaoAtiva,tipo,cadeiaProdutiva_id,
                    dataCadastro,informacaoPrimeiroContato,usuarioResponsavel_id,
                    dataUltimaAlteracao,versao,familia_id,ultimaVersao,estagioAtual_id)
values (2, 1, 1, 'Projeto 1.1', 'Descrição do projeto 1', 1, 0, 1, 20, 
        '2014-07-03', 'Informacao do primeiro contato', 1, '2014-07-03',1,1,0,1);

insert into Projeto (usuarioInvestimento_id,empresa_id,unidade_id,nome,
                    descricao,situacaoAtual_id,prospeccaoAtiva,tipo,cadeiaProdutiva_id,
                    dataCadastro,informacaoPrimeiroContato,usuarioResponsavel_id,
                    dataUltimaAlteracao,versao,familia_id,ultimaVersao,estagioAtual_id)
values (2, 1, 1, 'Projeto 1.2', 'Descrição do projeto 1', 2, 0, 1, 42, 
        '2014-07-03', 'Informacao do primeiro contato', 1, '2014-07-03',2,1,1,1);

insert into Projeto (usuarioInvestimento_id,empresa_id,unidade_id,nome,
                    descricao,situacaoAtual_id,prospeccaoAtiva,tipo,cadeiaProdutiva_id,
                    dataCadastro,informacaoPrimeiroContato,usuarioResponsavel_id,
                    dataUltimaAlteracao,versao,familia_id,ultimaVersao,estagioAtual_id)
values (2, 1, 1, 'Projeto 2.1', 'Descrição do projeto 2', 3, 0, 1, 20, 
        '2014-07-03', 'Informacao do primeiro contato', 1, '2014-07-03',1,2,0,1);
insert into Projeto (usuarioInvestimento_id,empresa_id,unidade_id,nome,
                    descricao,situacaoAtual_id,prospeccaoAtiva,tipo,cadeiaProdutiva_id,
                    dataCadastro,informacaoPrimeiroContato,usuarioResponsavel_id,
                    dataUltimaAlteracao,versao,familia_id,ultimaVersao,estagioAtual_id)
values (2, 1, 1, 'Projeto 2.2', 'Descrição do projeto 2', 4, 0, 1, 20, 
        '2014-07-03', 'Informacao do primeiro contato', 1, '2014-07-03',2,2,1,1);
insert into Projeto (usuarioInvestimento_id,empresa_id,unidade_id,nome,
                    descricao,situacaoAtual_id,prospeccaoAtiva,tipo,cadeiaProdutiva_id,
                    dataCadastro,informacaoPrimeiroContato,usuarioResponsavel_id,
                    dataUltimaAlteracao,versao,familia_id,ultimaVersao,estagioAtual_id)
values (2, 1, 1, 'Projeto 3.1', 'Descrição do projeto 3', 5, 0, 1, 20, 
        '2014-07-03', 'Informacao do primeiro contato', 1, '2014-07-03',1,3,0,1);
insert into Projeto (usuarioInvestimento_id,empresa_id,unidade_id,nome,
                    descricao,situacaoAtual_id,prospeccaoAtiva,tipo,cadeiaProdutiva_id,
                    dataCadastro,informacaoPrimeiroContato,usuarioResponsavel_id,
                    dataUltimaAlteracao,versao,familia_id,ultimaVersao,estagioAtual_id)
values (2, 1, 1, 'Projeto 3.2', 'Descrição do projeto 2', 6, 0, 1, 20, 
        '2014-07-03', 'Informacao do primeiro contato', 1, '2014-07-03',2,3,1,1);
-- insert into Projeto (usuarioInvestimento_id,empresa_id,unidade_id,nome,
--                     descricao,situacaoAtual_id,prospeccaoAtiva,tipo,cadeiaProdutiva_id,
--                     dataCadastro,informacaoPrimeiroContato,usuarioResponsavel_id,
--                     dataUltimaAlteracao)
-- values (1, 1, 1, 'Projeto 2', 'Descrição do projeto 2', 7, 0, 1, 20, 
--         '2014-07-03', 'Informacao do primeiro contato', 1, '2014-07-03');
-- insert into Projeto (usuarioInvestimento_id,empresa_id,unidade_id,nome,
--                     descricao,situacaoAtual_id,prospeccaoAtiva,tipo,cadeiaProdutiva_id,
--                     dataCadastro,informacaoPrimeiroContato,usuarioResponsavel_id,
--                     dataUltimaAlteracao)
-- values (1, 1, 1, 'Projeto 3', 'Descrição do projeto 3', 8, 0, 1, 20, 
--         '2014-07-03', 'Informacao do primeiro contato', 1, '2014-07-03');
-- insert into Projeto (usuarioInvestimento_id,empresa_id,unidade_id,nome,
--                     descricao,situacaoAtual_id,prospeccaoAtiva,tipo,cadeiaProdutiva_id,
--                     dataCadastro,informacaoPrimeiroContato,usuarioResponsavel_id,
--                     dataUltimaAlteracao)
-- values (1, 1, 1, 'Projeto 4', 'Descrição do projeto 4', 9, 0, 1, 20, 
--         '2014-07-03', 'Informacao do primeiro contato', 1, '2014-07-03');
-- insert into Projeto (usuarioInvestimento_id,empresa_id,unidade_id,nome,
--                     descricao,situacaoAtual_id,prospeccaoAtiva,tipo,cadeiaProdutiva_id,
--                     dataCadastro,informacaoPrimeiroContato,usuarioResponsavel_id,
--                     dataUltimaAlteracao)
-- values (1, 1, 1, 'Projeto 5', 'Descrição do projeto 5', 10, 0, 1, 20, 
--         '2014-07-03', 'Informacao do primeiro contato', 1, '2014-07-03');

Insert into HistoricoSituacaoProjeto (id, projeto_id, situacaoProjeto_id, data, justificativa) values (1 ,1, 1, '2014-07-03', null);
Insert into HistoricoSituacaoProjeto (id, projeto_id, situacaoProjeto_id, data, justificativa) values (2 ,2, 1, '2014-07-03', null);
Insert into HistoricoSituacaoProjeto (id, projeto_id, situacaoProjeto_id, data, justificativa) values (3 ,3, 1, '2014-07-03', null);
Insert into HistoricoSituacaoProjeto (id, projeto_id, situacaoProjeto_id, data, justificativa) values (4 ,4, 1, '2014-07-03', null);
Insert into HistoricoSituacaoProjeto (id, projeto_id, situacaoProjeto_id, data, justificativa) values (5 ,5, 1, '2014-07-03', null);
Insert into HistoricoSituacaoProjeto (id, projeto_id, situacaoProjeto_id, data, justificativa) values (6 ,6, 1, '2014-07-03', null);

-- EMPREGO TEMPORARIO
insert into Emprego (projeto_id,ano,direto,indireto,tipo, usuarioResponsavel_id,dataUltimaAlteracao)
values (1, 1998, 70,80,1,1, '2014-07-03');
insert into Emprego (projeto_id,ano,direto,indireto,tipo, usuarioResponsavel_id,dataUltimaAlteracao)
values (1, 1997, 1000,1500,1,1, '2013-07-06');

-- EMPREGO PERMANENTE
insert into Emprego (projeto_id,ano,direto,indireto,tipo, usuarioResponsavel_id,dataUltimaAlteracao)
values (1, 2013, 15,20,2,1, '2014-07-03');
insert into Emprego (projeto_id,ano,direto,indireto,tipo, usuarioResponsavel_id,dataUltimaAlteracao)
values (1, 2004, 50,15,2,1, '2013-07-06');

-- EMPREGO TEMPORARIO_OBRA
insert into Emprego (projeto_id,ano,direto,indireto,tipo, usuarioResponsavel_id,dataUltimaAlteracao)
values (1, 2008, 70,90,3,1, '2014-07-03');
insert into Emprego (projeto_id,ano,direto,indireto,tipo, usuarioResponsavel_id,dataUltimaAlteracao)
values (1, 2005, 150,100,3,1, '2013-07-06');

-- EMPREGO PERMANENTE OPERACAO
insert into Emprego (projeto_id,ano,direto,indireto,tipo, usuarioResponsavel_id,dataUltimaAlteracao)
values (1, 2013, 700,57,4,1, '2014-07-03');

-- EMPREGO TEMPORARIO IMPLANTAÇÃO
insert into Emprego (projeto_id,ano,direto,indireto,tipo, usuarioResponsavel_id,dataUltimaAlteracao)
values (1, 2013, 1000,100,5,1, '2013-07-06');
insert into Emprego (projeto_id,ano,direto,indireto,tipo, usuarioResponsavel_id,dataUltimaAlteracao)
values (1, 2013, 300,200,6,1, '2014-07-03');
insert into Emprego (projeto_id,ano,direto,indireto,tipo, usuarioResponsavel_id,dataUltimaAlteracao)
values (1, 2004, 150,50,5,1, '2013-07-06');
insert into Emprego (projeto_id,ano,direto,indireto,tipo, usuarioResponsavel_id,dataUltimaAlteracao)
values (1, 2004, 500,600,6,1, '2014-07-03');

-- EMPREGO PERMANENTE MATURAÇÃO
insert into Emprego (projeto_id,ano,direto,indireto,tipo, usuarioResponsavel_id,dataUltimaAlteracao)
values (1, 2002, 200,300,7,1, '2013-07-06');
insert into Emprego (projeto_id,ano,direto,indireto,tipo, usuarioResponsavel_id,dataUltimaAlteracao)
values (1, 2002, 150,500,8,1, '2014-07-03');
insert into Emprego (projeto_id,ano,direto,indireto,tipo, usuarioResponsavel_id,dataUltimaAlteracao)
values (1, 2012, 100,100,7,1, '2013-07-06');
insert into Emprego (projeto_id,ano,direto,indireto,tipo, usuarioResponsavel_id,dataUltimaAlteracao)
values (1, 2012, 30,40,8,1, '2014-07-03');

--Cronograma
insert into Cronograma (projeto_id, inicioProjeto, inicioProjetoExecutivo,
                        inicioContratacaoEquipamentos, inicioImplantacao,
                        inicioImplantacaoAgricola, inicioImplantacaoIndustrial,
                        terminoImplantacaoAgricola, terminoImplantacaoIndustrial ,
                        inicioOperacao, terminoProjetoExecutivo , terminoProjeto ,
                        usuarioResponsavel_id, dataUltimaAlteracao) 
values (1, '2014-07-10', '2014-07-10', '2014-07-10', '2014-07-10', 
        '2014-07-10', '2014-07-10', '2014-07-10', '2014-07-10', '2014-07-10', 
        '2014-07-10', '2014-07-10', 1, '2014-07-10');
insert into Cronograma (projeto_id, inicioProjeto, inicioProjetoExecutivo,
                        inicioContratacaoEquipamentos, inicioImplantacao,
                        inicioImplantacaoAgricola, inicioImplantacaoIndustrial,
                        terminoImplantacaoAgricola, terminoImplantacaoIndustrial ,
                        inicioOperacao, terminoProjetoExecutivo , terminoProjeto ,
                        usuarioResponsavel_id, dataUltimaAlteracao) 
values (2, '2014-07-10', '2014-07-10', '2014-07-10', '2014-07-10', 
        '2014-07-10', '2014-07-10', '2014-07-10', '2014-07-10', '2014-07-10', 
        '2014-07-10', '2014-07-10', 1, '2014-07-10');
insert into Cronograma (projeto_id, inicioProjeto, inicioProjetoExecutivo,
                        inicioContratacaoEquipamentos, inicioImplantacao,
                        inicioImplantacaoAgricola, inicioImplantacaoIndustrial,
                        terminoImplantacaoAgricola, terminoImplantacaoIndustrial ,
                        inicioOperacao, terminoProjetoExecutivo , terminoProjeto ,
                        usuarioResponsavel_id, dataUltimaAlteracao) 
values (3, '2014-07-10', '2014-07-10', '2014-07-10', '2014-07-10', 
        '2014-07-10', '2014-07-10', '2014-07-10', '2014-07-10', '2014-07-10', 
        '2014-07-10', '2014-07-10', 1, '2014-07-10');
insert into Cronograma (projeto_id, inicioProjeto, inicioProjetoExecutivo,
                        inicioContratacaoEquipamentos, inicioImplantacao,
                        inicioImplantacaoAgricola, inicioImplantacaoIndustrial,
                        terminoImplantacaoAgricola, terminoImplantacaoIndustrial ,
                        inicioOperacao, terminoProjetoExecutivo , terminoProjeto ,
                        usuarioResponsavel_id, dataUltimaAlteracao) 
values (4, '2014-07-10', '2014-07-10', '2014-07-10', '2014-07-10', 
        '2014-07-10', '2014-07-10', '2014-07-10', '2014-07-10', '2014-07-10', 
        '2014-07-10', '2014-07-10', 1, '2014-07-10');
insert into Cronograma (projeto_id, inicioProjeto, inicioProjetoExecutivo,
                        inicioContratacaoEquipamentos, inicioImplantacao,
                        inicioImplantacaoAgricola, inicioImplantacaoIndustrial,
                        terminoImplantacaoAgricola, terminoImplantacaoIndustrial ,
                        inicioOperacao, terminoProjetoExecutivo , terminoProjeto ,
                        usuarioResponsavel_id, dataUltimaAlteracao) 
values (5, '2014-07-10', '2014-07-10', '2014-07-10', '2014-07-10', 
        '2014-07-10', '2014-07-10', '2014-07-10', '2014-07-10', '2014-07-10', 
        '2014-07-10', '2014-07-10', 1, '2014-07-10');

--Infraestrutura
insert into Infraestrutura (id,projeto_id,areaNecessaria,areaConstruida,potenciaEnergeticaEstimada,
        consumoAguaEstimado,consumoCombustivelEstimado,consumoGasEstimado,geracaoEnergiaPropria,
        geracaoEnergiaVenda,demandaEnergiaEstimada,ofertaEnergiaVendaEstimada,possuiTerreno,contatoCemig,
        areaAlagada,energiaJaContratada,usuarioResponsavel_id,dataUltimaAlteracao)
values (1,1,120,35,435,932,210,304,1230,1291,120,129,1,0,1,0,1,CURRENT_TIMESTAMP);
insert into Infraestrutura (id,projeto_id,areaNecessaria,areaConstruida,potenciaEnergeticaEstimada,
        consumoAguaEstimado,consumoCombustivelEstimado,consumoGasEstimado,geracaoEnergiaPropria,
        geracaoEnergiaVenda,demandaEnergiaEstimada,ofertaEnergiaVendaEstimada,possuiTerreno,contatoCemig,
        areaAlagada,energiaJaContratada,usuarioResponsavel_id,dataUltimaAlteracao)
values (2,2,178,456,285,2015,110,808,650,871,208,116,0,1,0,1,1,CURRENT_TIMESTAMP);

--Pleito
insert into Pleito(id, projeto_id, pleitos, observacoes, usuarioresponsavel_id, dataultimaalteracao)
values(1, 1, 'Pleito do projeto 1', 'Observacoes do pleito do projeto 1', 1, CURRENT_TIMESTAMP);

insert into Pleito(id, projeto_id, pleitos, observacoes, usuarioresponsavel_id, dataultimaalteracao)
values(2, 2, 'Pleito do projeto 2', 'Observacoes do pleito do projeto 2', 1, CURRENT_TIMESTAMP);

--Usuário
-- Insert Into Usuario(ID, grupousuario_id, tipo, Cargo_id, Nome, Senha, email, Telefone, Ativo, usuarioResponsavel_id, dataUltimaAlteracao)
-- Values(2, 1, 1, 'Analista de Infraestrutura', 'MOkhtSnOLM/Jc+EZb1lfZ3AO86X8TSkWV01Ewf4RMFk=', 'gpi1@teste.com.br', '(31)9999-9999', 'TRUE', null, '2014-07-17 10:57:52.638');
-- Insert Into UsuarioExterno(usuario_id, departamento_id, matricula)
-- Values(2, 2, '1');

Insert Into Usuario(ID, grupousuario_id, tipo, Cargo_id, Nome, Senha, email, Telefone, Ativo, usuarioResponsavel_id, dataUltimaAlteracao)
Values(2, 2, 1, 1, 'Analista de Infraestrutura', 'MOkhtSnOLM/Jc+EZb1lfZ3AO86X8TSkWV01Ewf4RMFk=', 'gpi1@teste.com.br', '(31)9999-9999', 'TRUE', null, '2014-07-17 10:57:52.638');
Insert Into UsuarioInterno(id, departamento_id, matricula)
Values(2, 2, '1');

Insert Into Usuario(ID, grupousuario_id, tipo, Cargo_id, Nome, Senha, email, Telefone, Ativo, usuarioResponsavel_id, dataUltimaAlteracao)
Values(3,3, 1, 1, 'Analista de Informação', 'qCkcL4khnKTHX42TEn1qcwb/JPNXN7P95picdrj8Rh8=', 'gpi2@teste.com.br', '(31)9999-9999', 'TRUE', null, '2014-07-17 10:57:52.638');
Insert Into UsuarioInterno(id, departamento_id, matricula)
Values(3, 3, '12');

Insert Into Usuario(ID, grupousuario_id, tipo, Cargo_id, Nome, Senha, email, Telefone, Ativo, usuarioResponsavel_id, dataUltimaAlteracao)
Values(4, 4,1,1, 'Analista de Portfólio', 'shkOnN/GRQw7s261jmUzYkBveBYx0bUel+1eE9xBgVk=', 'gpi2@teste.com.br', '(31)9999-9999', 'TRUE', null, '2014-07-17 10:57:52.638');
Insert Into UsuarioInterno(id, departamento_id, matricula)
Values(4, 4, '123');

Insert Into Usuario(ID, grupousuario_id, tipo, Cargo_id, Nome, Senha, email, Telefone, Ativo, usuarioResponsavel_id, dataUltimaAlteracao)
Values(1, 5, 1,1, 'Analista de Promoção', 'w3tqa4rfA7IGsOjsmRA2iWGHu7muc5EDtJlf+WytseA=', 'gpi3@teste.com.br', '(31)9999-9999', 'TRUE', null, '2014-07-17 10:57:52.638');
Insert Into UsuarioInterno(id, departamento_id, matricula)
Values(1, 5, '12345');

Insert Into Usuario(ID, grupousuario_id, tipo, Cargo_id, Nome, Senha, email, Telefone, Ativo, usuarioResponsavel_id, dataUltimaAlteracao)
Values(6, 6, 1,1, 'Diretor', 'rqK4X3Z6AZSDFoFhTMZtYOVsCmtEfwnSZfP+ouBamtI=', 'gpi4@teste.com.br', '(31)9999-9999', 'TRUE', null, '2014-07-17 10:57:52.638');
Insert Into UsuarioInterno(id, departamento_id, matricula)
Values(6, 6, '123456');

Insert Into Usuario(ID, grupousuario_id, tipo, Cargo_id, Nome, Senha, email, Telefone, Ativo, usuarioResponsavel_id, dataUltimaAlteracao)
Values(7,7, 1,1, 'Gerente', 'RZiOYzacz12zkMHWJ4jYtB6Vqy/f1fsvEHF9xMr8gBw=', 'gpi4@teste.com.br', '(31)9999-9999', 'TRUE', null, '2014-07-17 10:57:52.638');
Insert Into UsuarioInterno(id, departamento_id, matricula)
Values(7, 7, '1234567');

Insert Into Usuario(ID, grupousuario_id, tipo, Cargo_id, Nome, Senha, email, Telefone, Ativo, usuarioResponsavel_id, dataUltimaAlteracao)
Values(8,8, 1,1, 'Secretaria Geral', 'SeR6T/9kI7zgMVaRgMBs9WihITDGvwvkA9NU23sUT5M=', 'gpi4@teste.com.br', '(31)9999-9999', 'TRUE', null, '2014-07-17 10:57:52.638');
Insert Into UsuarioInterno(id, departamento_id, matricula)
Values(8, 8, '12345678');

Insert Into Usuario(ID, grupousuario_id, tipo, Cargo_id, Nome, Senha, email, Telefone, Ativo, usuarioResponsavel_id, dataUltimaAlteracao)
Values(9,1, 1,1, 'Administrador', 'Y6RIuZfzPJMxHkqB3vTs8kvcg6hPvKHTmp9XXZ+zCQM=', 'gpi@teste.com.br', '(31)9999-9999', 'TRUE', null, '2014-07-17 10:57:52.638');
Insert Into UsuarioInterno(id, departamento_id, matricula)
Values(9, 1,'1234');

INSERT INTO USUARIO(ID, grupousuario_id, tipo, CARGO_ID, NOME, SENHA, EMAIL, TELEFONE, ATIVO, USUARIORESPONSAVEL_ID, DATAULTIMAALTERACAO)
VALUES(10,2, 1,1, 'infra', 'aTbXc5rV/qYEJE1xqa0YqhamLi2dlcIIPv6wA/r30nA=', NULL, '(31)9999-9999', 1, 9, '2014-08-04 10:37:45.685');
Insert Into UsuarioInterno(id, departamento_id, matricula)
Values(10,2, 'infra');

INSERT INTO USUARIO(ID, grupousuario_id, tipo, CARGO_ID, NOME, SENHA, EMAIL, TELEFONE, ATIVO, USUARIORESPONSAVEL_ID, DATAULTIMAALTERACAO)
VALUES(11,3, 1,1, 'info', 'tPppjheCayiASoW6qST/jMm+ddB8kgl5Y3hbiulvS+M=', NULL, '(31)9999-9999', 1, 9, '2014-08-04 10:38:15.494');
Insert Into UsuarioInterno(id, departamento_id, matricula)
Values(11,3, 'info');

INSERT INTO USUARIO(ID, grupousuario_id, tipo, CARGO_ID, NOME, SENHA, EMAIL, TELEFONE, ATIVO, USUARIORESPONSAVEL_ID, DATAULTIMAALTERACAO)
VALUES(12,4, 1,1, 'portfolio', '25kVFmB8Kl3mm3HuAJqenQQkpVYYfZoFRAsCsjFQptw=', NULL, '(31)9999-9999', 1, 9, '2014-08-04 10:38:34.569');
Insert Into UsuarioInterno(id, departamento_id, matricula)
Values(12,4, 'portfolio');

INSERT INTO USUARIO(ID, grupousuario_id, tipo, CARGO_ID, NOME, SENHA, EMAIL, TELEFONE, ATIVO, USUARIORESPONSAVEL_ID, DATAULTIMAALTERACAO)
VALUES(13,5, 1,1, 'promocao', 'LDBtZJTO2UJAIcsA00Ax7UlBFawfDmgFHjKCN2NBPSg=', NULL, '(31)9999-9999', 1, 9, '2014-08-04 10:38:52.372');
Insert Into UsuarioInterno(id, departamento_id, matricula)
Values(13,5, 'promocao');

INSERT INTO USUARIO(ID, grupousuario_id, tipo, CARGO_ID, NOME, SENHA, EMAIL, TELEFONE, ATIVO, USUARIORESPONSAVEL_ID, DATAULTIMAALTERACAO)
VALUES(14,6, 1,1, 'diretor', '00HHwkGfhTkYIkIyZc7lviZ+dROq5MEyDnAQxFpJYIs=', NULL, '(31)9999-9999', 1, 9, '2014-08-04 10:39:11.43');
Insert Into UsuarioInterno(id, departamento_id, matricula)
Values(14, 6, 'diretor');

INSERT INTO USUARIO(ID, grupousuario_id, tipo, CARGO_ID, NOME, SENHA, EMAIL, TELEFONE, ATIVO, USUARIORESPONSAVEL_ID, DATAULTIMAALTERACAO)
VALUES(15,7, 1,1, 'gerente', 'x0qvxP0T3wHNmHh27Gx571m74EqNcCoVawxeimg9FVY=', NULL, '(31)9999-9999', 1, 9, '2014-08-04 10:39:26.952');
Insert Into UsuarioInterno(id, departamento_id, matricula)
Values(15, 7, 'gerente');

INSERT INTO USUARIO(ID, grupousuario_id, tipo, CARGO_ID, NOME, SENHA, EMAIL, TELEFONE, ATIVO, USUARIORESPONSAVEL_ID, DATAULTIMAALTERACAO)
VALUES(16, 8,1,1, 'geral', '5anSlQJf689zF97bn1yX5ef/hkOjg9iDMw2+UPR4txs=', NULL, '(31)9999-9999', 1, 9, '2014-08-04 10:39:40.352');
Insert Into UsuarioInterno(id, departamento_id, matricula)
Values(16, 8, 'geral');

INSERT INTO USUARIO(ID, grupousuario_id, CARGO_ID, NOME, SENHA, EMAIL, TELEFONE, TELEFONE2, ATIVO, USUARIORESPONSAVEL_ID, DATAULTIMAALTERACAO, TIPO) 
VALUES (17, 9, 1, 'Usuário Externo Empresa 1', 'coGobraWZhxcCT+R08IUZeRdWM4aG+PtF6CktwaHX98=', 'teste@teste.com', '(31)9999-9999', '1111-1111', true, NULL, '2014-07-03 00:00:00.0', 2);
INSERT INTO USUARIOEXTERNO (ID, EMPRESA_ID, UNIDADE_ID, SENHA, DATABLOQUEIOTEMPORARIO, CARGOEXTERNO,) 
VALUES (17, 1, null, 'coGobraWZhxcCT+R08IUZeRdWM4aG+PtF6CktwaHX98=', '2014-08-22 14:20:52.003', 'CEO');

/*INSERT INTO GRUPOUSUARIOUSUARIO (GRUPOUSUARIO_ID, USUARIO_ID) 
VALUES (9, 17);

Insert Into GRUPOUSUARIOUSUARIO (GRUPOUSUARIO_ID, USUARIO_ID)
Values(5,1);
Insert Into GRUPOUSUARIOUSUARIO (GRUPOUSUARIO_ID, USUARIO_ID)
Values(2,2);
Insert Into GRUPOUSUARIOUSUARIO (GRUPOUSUARIO_ID, USUARIO_ID)
Values(3,3);
Insert Into GRUPOUSUARIOUSUARIO (GRUPOUSUARIO_ID, USUARIO_ID)
Values(4,4);
Insert Into GRUPOUSUARIOUSUARIO (GRUPOUSUARIO_ID, USUARIO_ID)
Values(6,6);
Insert Into GRUPOUSUARIOUSUARIO (GRUPOUSUARIO_ID, USUARIO_ID)
Values(7,7);
Insert Into GRUPOUSUARIOUSUARIO (GRUPOUSUARIO_ID, USUARIO_ID)
Values(8,8);
Insert Into GRUPOUSUARIOUSUARIO (GRUPOUSUARIO_ID, USUARIO_ID)
Values(1,9);
INSERT INTO GRUPOUSUARIOUSUARIO (GRUPOUSUARIO_ID, USUARIO_ID) 
	VALUES (2, 10);
INSERT INTO GRUPOUSUARIOUSUARIO (GRUPOUSUARIO_ID, USUARIO_ID) 
	VALUES (3, 11);
INSERT INTO GRUPOUSUARIOUSUARIO (GRUPOUSUARIO_ID, USUARIO_ID) 
	VALUES (4, 12);
INSERT INTO GRUPOUSUARIOUSUARIO (GRUPOUSUARIO_ID, USUARIO_ID) 
	VALUES (5, 13);
INSERT INTO GRUPOUSUARIOUSUARIO (GRUPOUSUARIO_ID, USUARIO_ID) 
	VALUES (6, 14);
INSERT INTO GRUPOUSUARIOUSUARIO (GRUPOUSUARIO_ID, USUARIO_ID) 
	VALUES (7, 15);
INSERT INTO GRUPOUSUARIOUSUARIO (GRUPOUSUARIO_ID, USUARIO_ID) 
	VALUES (8, 16);

Insert Into DEPARTAMENTOUSUARIO (DEPARTAMENTO_ID, USUARIO_ID)
Values(5,1);
Insert Into DEPARTAMENTOUSUARIO (DEPARTAMENTO_ID, USUARIO_ID)
Values(2,2);
Insert Into DEPARTAMENTOUSUARIO (DEPARTAMENTO_ID, USUARIO_ID)
Values(3,3);
Insert Into DEPARTAMENTOUSUARIO (DEPARTAMENTO_ID, USUARIO_ID)
Values(4,4);
Insert Into DEPARTAMENTOUSUARIO (DEPARTAMENTO_ID, USUARIO_ID)
Values(6,6);
Insert Into DEPARTAMENTOUSUARIO (DEPARTAMENTO_ID, USUARIO_ID)
Values(7,7);
Insert Into DEPARTAMENTOUSUARIO (DEPARTAMENTO_ID, USUARIO_ID)
Values(8,8);
Insert Into DEPARTAMENTOUSUARIO (DEPARTAMENTO_ID, USUARIO_ID)
Values(1,9);
INSERT INTO DEPARTAMENTOUSUARIO (DEPARTAMENTO_ID, USUARIO_ID) 
	VALUES (2, 10);
INSERT INTO DEPARTAMENTOUSUARIO (DEPARTAMENTO_ID, USUARIO_ID) 
	VALUES (3, 11);
INSERT INTO DEPARTAMENTOUSUARIO (DEPARTAMENTO_ID, USUARIO_ID) 
	VALUES (4, 12);
INSERT INTO DEPARTAMENTOUSUARIO (DEPARTAMENTO_ID, USUARIO_ID) 
	VALUES (5, 13);
INSERT INTO DEPARTAMENTOUSUARIO (DEPARTAMENTO_ID, USUARIO_ID) 
	VALUES (6, 14);
INSERT INTO DEPARTAMENTOUSUARIO (DEPARTAMENTO_ID, USUARIO_ID) 
	VALUES (7, 15);
INSERT INTO DEPARTAMENTOUSUARIO (DEPARTAMENTO_ID, USUARIO_ID) 
	VALUES (8, 16);*/

Insert into InstrumentoFormalizacao 
    (protocolo,situacaoAtual_id,tipo,versao,titulo,dataAssinatura,dataAssinaturaPrevista,
        possuiContrapartidaFincGoverno,usuarioResponsavel_id,dataUltimaAlteracao, ultimaVersao) 
values ('9999999',1,1,1,'Instrumento 1','2014-01-01','2014-01-01',1,1,'2014-01-01',0);

Insert into InstrumentoFormalizacao 
    (protocolo,situacaoAtual_id,tipo,versao,titulo,dataAssinatura,dataAssinaturaPrevista,
        possuiContrapartidaFincGoverno,usuarioResponsavel_id,dataUltimaAlteracao, ultimaVersao) 
values ('9999999',1,1,2,'Aditivo Instr 1','2014-01-01','2014-01-01',1,1,'2014-01-01',0);

insert into historicosituacaoinstrumentoformalizacao(instrumentoformalizacao_id, situacaoinstrumentoformalizacao_id,
datainiciosituacao) 
values (1,1,CURRENT_TIMESTAMP);

update instrumentoFormalizacao set historicoSituacaoInstrumentoFormalizacao_id = 1 where id = 1;

Insert into Signatario  
    (instrumentoFormalizacao_id,nome,cargo,telefone1,telefone2,email) 
values (1,'Signatario 1','Cargo1','319999999','3199999999','signatario@email.com');

Insert into ProjetoInstrumentoFormalizacao  
    (instrumentoFormalizacao_id,projeto_id) 
values (1,1);
Insert into ProjetoInstrumentoFormalizacao  
    (instrumentoFormalizacao_id,projeto_id) 
values (1,2);

INSERT INTO INSUMOPRODUTO(ID, PROJETO_ID, AQUISICAOOUTROESTADO, INTERESSEPARCERIA, IMPORTACAOPRODUTO, CONTRIBUINTESUBSTITUTO, PERCENTUALORIGEMMINAS, PERCENTUALORIGEMOUTROS, PERCENTUALORIGEMIMPORTADO, PERCENTUALFABRICAMINAS, PERCENTUALADQUIRIDOOUTRO, PERCENTUALIMPORTADO, PERCENTUALVALORAGREGADO, PERCENTUALICMS, PERCENTUALCLIENTEINDUSTRIA, PERCENTUALCLIENTECOMERCIAL, PERCENTUALCLIENTECONSUMIDOR, PERCENTUALMERCADOMINAS, PERCENTUALMERCADONACIONAL, PERCENTUALMERCADOEXTERIOR, PERCENTUALTRIBUTOEFETIVO, USUARIORESPONSAVEL_ID, DATAULTIMAALTERACAO) VALUES
(1, 1, TRUE, TRUE, TRUE, FALSE, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 32.00, 48.00, 20.00, 51.00, 49.00, 0.00, 1.00, 7, TIMESTAMP '2014-07-30 13:53:34.92');

INSERT INTO INSUMO(ID, INSUMOPRODUTO_ID, NOME, NCM_ID, ORIGEM, QUANTIDADEANO, UNIDADEMEDIDA, VALORESTIMADOANO, EMPRESAFORNECEDOR) VALUES
(1, 1, 'product 1', 29049011, 3, 1, 1.00, 1.00, 'Disney alterado'),
(2, 1, 'paulo ', 29181929, 3, 64, 54.00, 654.00, '54'),
(3, 1, 'product 3', 90101010, 1, 56, 45.00, 456.00, '456'),
(4, 1, 'kiljhasdf', 29215111, 3, 56, 42.00, 465.00, '132453123');

INSERT INTO PRODUTO(ID, INSUMOPRODUTO_ID, NOME, NCM_ID, SUJEITOST, QUANTIDADE1, QUANTIDADE2, QUANTIDADE3, UNIDADEMEDIDA, TIPO) VALUES
(1, 1, 'produto tipo 1', 1011100, TRUE, 1, 1, 1, 1.00, 1),
(2, 1, 'produto tipo 2', 1011100, TRUE, 1, 1, 1, 1.00, 2),
(3, 1, 'produto tipo 3', 1011100, TRUE, 1, 1, 1, 1.00, 3),
(4, 1, 'produto tipo 4', 1011100, TRUE, 1, 1, 1, 1.00, 4),
(5, 1, 'askdfj', 29362720, NULL, 56, 564, 54, 564.00, 2),
(6, 1, 'asdf', 29379221, NULL, 56, 214, 45, 654.00, 3),
(7, 1, 'adsf', 34012010, NULL, 54, 354, 654, 654.00, 3),
(8, 1, 'asdfasdf', 22083010, TRUE, 3254, 564, 654, 65.00, 3),
(9, 1, 'aisouf 2', 3037200, TRUE, 5, 46, 46, 455.00, 1),
(10, 1, 'asdf', 29332995, NULL, 654, 465, 354, 465.00, 4),
(11, 1, '12', 68051000, TRUE, 465645645, 645456, 645456, 645.00, 4),
(12, 1, 'asdfa', 90183929, NULL, 54, 2, 1, 45.00, 4);

INSERT INTO PRODUCAO(ID, INSUMOPRODUTO_ID, ANO, AREAPLANTADA, MOAGEMCANA, PRODUCAOETANOL, PRODUCAOACUCAR, GERACAOENERGIA, OUTRO) VALUES
(1, 1, 2014, 2.00, 1.00, 4.00, 1.00, 1.00, 1.00),
(2, 1, 2014, 1.00, 1.00, 1.00, 1.00, 6.00, 1.00),
(3, 1, 2014, 1.00, 1.00, 1.00, 1.00, 7.00, 1.00),
(4, 1, 2014, 1.00, 1.00, 1.00, 8.00, 1.00, 1.00);

insert into parceria values (1, 1, 'pareceria 1');
insert into parceria values (2, 1, 'pareceria 2');
insert into parceria values (3, 1, 'pareceria 3');
insert into parceria values (4, 1, 'pareceria 4');

INSERT INTO SERVICO(ID, INSUMOPRODUTO_ID, DESCRICAO) VALUES
(1, 1, 'servico 1'),
(2, 1, 'servico 2'),
(3, 1, 'servico 3'),
(4, 1, 'servico 4'),
(5, 1, 'asdfasdf'),
(6, 1, 'asdf');

INSERT INTO CONCORRENTE(ID, INSUMOPRODUTO_ID, NOME, DESCRICAOPRODUTO) VALUES
(1, 1, 'concorrente 1', 'p1'),
(4, 1, 'concorrente 4', 'p4'),
(5, 1, 'fghjfghjfghj', 'qw'),
(6, 1, 'fdasdfasdfasfgfjfghj', 'fghjfghjfghj'),
(7, 1, 'asdfas', 'fghj'),
(8, 1, 'fghjfgfghjfghj', 'qwerqwreqwerqr'),
(9, 1, 'fas', 'asdfasf'),
(10, 1, 'asd', 'asdfasdfasdf'),
(11, 1, 'dfasdf', 'asdfaasdfasdfasdf'),
(12, 1, 'sdfasdfasdfasdfasdf', 'asdfasdf'),
(13, 1, 'asdfasdf', 'sadf'),
(14, 1, 'asdfasdfasdfa', 'sdfasdfasdf'),
(15, 1, 'asdfasdfasfd', 'sadfasdfasdfasdf'),
(16, 1, 'asdf', 'asdf');

Insert into Financeiro (id, projeto_id, observacao, usuarioResponsavel_id, dataUltimaAlteracao)
values(1, 2, 'observacao do financeiro 1',  2, CURRENT_TIMESTAMP);

Insert into InvestimentoPrevisto(id, financeiro_id, tipo, ano, valor)
values(1, 1, 1, 2013, 240000);
Insert into InvestimentoPrevisto(id, financeiro_id, tipo, ano, valor)
values(2, 1, 1, 2012, 25700);
Insert into InvestimentoPrevisto(id, financeiro_id, tipo, ano, valor)
values(3, 1, 2, 2013, 1450200);
Insert into InvestimentoPrevisto(id, financeiro_id, tipo, ano, valor)
values(4, 1, 2, 2012, 102000);
Insert into InvestimentoPrevisto(id, financeiro_id, tipo, ano, valor)
values(5, 1, 3, 2013, 150000);
Insert into InvestimentoPrevisto(id, financeiro_id, tipo, ano, valor)
values(6, 1, 4, 2012, 50000);
Insert into InvestimentoPrevisto(id, financeiro_id, tipo, ano, valor)
values(7, 1, 5, 2013, 2000000);
Insert into InvestimentoPrevisto(id, financeiro_id, tipo, ano, valor)
values(8, 1, 6, 2012, 505200);

Insert into ORIGEMRECURSO (id, financeiro_id, origemnacional, uf_id, pais_id, percentualinvestimento, valorinvestimento)
values(1, 1, 1, 27, 2, 60, 1000000);
Insert into ORIGEMRECURSO (id, financeiro_id, origemnacional, uf_id, pais_id, percentualinvestimento, valorinvestimento)
values(2, 1, 0, 35, 3, 60, 2520750);

--  Publicacao
INSERT INTO PUBLICACAO(ID, INSTRUMENTOFORMALIZACAO_ID, DATAPUBLICACAO, USUARIORESPONSAVEL_ID, DATAULTIMAALTERACAO) VALUES
(1, 1, TIMESTAMP '2014-01-30 00:00:00.0', 2, TIMESTAMP '2014-05-20 00:00:00.0');

insert into FATURAMENTOANTERIOR (FINANCEIRO_ID, TIPO, ANO, VALOR)
values (1, 1, 2013, 120000);
insert into FATURAMENTOANTERIOR (FINANCEIRO_ID, TIPO, ANO, VALOR)
values (1, 2, 2013, 120000);

insert into FATURAMENTOPREVISTO(FINANCEIRO_ID, TIPO, ANO, VALOR)
values(1, 4, 2014, 450000);
insert into FATURAMENTOPREVISTO(FINANCEIRO_ID, TIPO, ANO, VALOR)
values(1, 5, 2014, 1250000);

insert into FATURAMENTOPREVISTO(FINANCEIRO_ID, TIPO, ANO, VALOR)
values(1, 1, 2014, 120000);
insert into FATURAMENTOPREVISTO(FINANCEIRO_ID, TIPO, ANO, VALOR)
values(1, 2, 2013, 168000);

insert into FATURAMENTOPREVISTO(FINANCEIRO_ID, TIPO, ANO, VALOR)
values(1, 3, 2014, 856000);
insert into FATURAMENTOPREVISTO(FINANCEIRO_ID, TIPO, ANO, VALOR)
values(1, 3, 2013, 765000);

--  Meio Ambiente
INSERT INTO MEIOAMBIENTE (PROJETO_ID, LICENCIAMENTOINICIADO, NECESSIDADEEIARIMA, REALIZADAVISTORIA, PEDIDOINFORMACOESCOMPLEMENTAR, DATAENTREGAESTUDOS, DATAREALIZADAVISTORIA, DATAPEDIDOINFORMACAO, DATAENTREGAINFORMACAO, CLASSEEMPREENDIMENTO, OBSERVACAO, USUARIORESPONSAVEL_ID, DATAULTIMAALTERACAO) 
	VALUES (1, 0, 0, 0, 0, '2014-07-29 10:50:07.56', '2014-07-29 10:50:07.56', '2014-07-29 10:50:07.56', '2014-07-29 10:50:07.56', 1, 'Observação Meio Ambiente 1', 2, '2014-07-29 10:50:07.56');

-- Etapa Meio Ambiente
INSERT INTO ETAPAMEIOAMBIENTE (MEIOAMBIENTE_ID, TIPO) VALUES (1, 1);
INSERT INTO ETAPAMEIOAMBIENTE (MEIOAMBIENTE_ID, TIPO) VALUES (1, 2);
INSERT INTO ETAPAMEIOAMBIENTE (MEIOAMBIENTE_ID, TIPO) VALUES (1, 3);
INSERT INTO ETAPAMEIOAMBIENTE (MEIOAMBIENTE_ID, TIPO) VALUES (1, 4);
INSERT INTO ETAPAMEIOAMBIENTE (MEIOAMBIENTE_ID, TIPO) VALUES (1, 5);

-- Template
insert into template(nome, tipo, status, usuarioresponsavel_id, dataultimaalteracao)
values ('template teste', 0, 1, 1, CURRENT_TIMESTAMP);

insert into template(nome, tipo, status, usuarioresponsavel_id, dataultimaalteracao)
values ('template padrão', 0, 1, 1, CURRENT_TIMESTAMP);

insert into template(nome, tipo, status, usuarioresponsavel_id, dataultimaalteracao)
values ('template ocasional', 1, 0, 1, CURRENT_TIMESTAMP);

insert into template(nome, tipo, status, usuarioresponsavel_id, dataultimaalteracao)
values ('template ', 1, 1, 1, CURRENT_TIMESTAMP);

-- AtividadeTemplate
insert into atividadetemplate(id, atividadelocal_id, template_id, posicao, obrigatorio, bloqueiaice, df, ii, oi, cc)
values(1, 10162, 1, 1, 1, 0, 1, 0, 0, 0);
insert into atividadetemplate(id, atividadelocal_id, template_id, posicao, obrigatorio, bloqueiaice, df, ii, oi, cc)
values(2, 10183, 2, 1, 0, 1, 0, 1, 1, 0);

-- pesquisa
insert into pesquisa(instrumentoformalizacao_id,datasolicitacao,comentario,dataultimaalteracao,CONTATOEXTERNORESPONSAVEL_ID)
values(1,current_timestamp, 'comentario normal',current_timestamp, 1);

-- pesquisaPerguntaPesquisa
insert into pesquisaPerguntaPesquisa(pesquisa_id,perguntaPesquisa_id,resposta,justificativa)
values(1,1,3,'é porque é');
insert into pesquisaPerguntaPesquisa(pesquisa_id,perguntaPesquisa_id,resposta,justificativa)
values(1,2,4, 'porque sim');
insert into pesquisaPerguntaPesquisa(pesquisa_id,perguntaPesquisa_id,resposta,justificativa)
values(1,3,5, 'é logico');
insert into pesquisaPerguntaPesquisa(pesquisa_id,perguntaPesquisa_id,resposta,justificativa)
values(1,4,6, 'está claro');
insert into pesquisaPerguntaPesquisa(pesquisa_id,perguntaPesquisa_id,resposta,justificativa)
values(1,5,1, 'por dedução');

--indicador
INSERT INTO indicador (id,nome) VALUES (1,'Formalização de Investimento') GO
INSERT INTO indicador (id,nome) VALUES (2,'Índice de Satisfação') GO
INSERT INTO indicador (id,nome) VALUES (3,'Quantidade de Projetos') GO
INSERT INTO indicador (id,nome) VALUES (4,'Investimento') GO
INSERT INTO indicador (id,nome) VALUES (5,'Empregos Diretos') GO
INSERT INTO indicador (id,nome) VALUES (6,'Faturamento Pleno') GO

--meta
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (1,2014,1,7.00,1) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (1,2014,2,12.00,1) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (1,2014,3,7.00,1) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (1,2014,4,3.00,1) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (1,2014,5,9.00,1) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (1,2014,6,17.00,1) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (1,2014,7,21.00,1) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (1,2014,8,19.00,1) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (1,2014,9,10.00,1) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (1,2014,10,10.00,1) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (1,2014,11,3.00,1) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (1,2014,12,5.00,1) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (2,2014,1,70,1) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (2,2014,2,70,1) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (2,2014,3,70,1) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (2,2014,4,70,1) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (2,2014,5,70,1) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (2,2014,6,70,1) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (2,2014,7,70,1) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (2,2014,8,70,1) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (2,2014,9,70,1) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (2,2014,10,70,1) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (2,2014,11,70,1) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (2,2014,12,70,1) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (3,2014,1,7.00,1) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (3,2014,2,12.00,1) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (3,2014,3,7.00,1) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (3,2014,4,3.00,1) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (3,2014,5,9.00,1) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (3,2014,6,17.00,1) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (3,2014,7,21.00,1) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (3,2014,8,19.00,1) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (3,2014,9,10.00,1) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (3,2014,10,10.00,1) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (3,2014,11,3.00,1) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (3,2014,12,5.00,1) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (4,2014,1,218.00,1) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (4,2014,2,1651.00,1) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (4,2014,3,219.00,1) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (4,2014,4,422.00,1) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (4,2014,5,1751.00,1) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (4,2014,6,947.00,1) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (4,2014,7,1491.00,1) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (4,2014,8,4307.00,1) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (4,2014,9,702.00,1) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (4,2014,10,151.00,1) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (4,2014,11,1910.00,1) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (4,2014,12,231.00,1) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (5,2014,1,17732.00,1) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (5,2014,2,17732.00,1) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (5,2014,3,17732.00,1) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (5,2014,4,17732.00,1) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (5,2014,5,17732.00,1) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (5,2014,6,17732.00,1) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (5,2014,7,17732.00,1) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (5,2014,8,17732.00,1) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (5,2014,9,17732.00,1) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (5,2014,10,17732.00,1) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (5,2014,11,17732.00,1) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (5,2014,12,17732.00,1) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (3,2014,1,61.00,2) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (3,2014,2,61.00,2) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (3,2014,3,61.00,2) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (3,2014,4,61.00,2) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (3,2014,5,61.00,2) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (3,2014,6,61.00,2) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (3,2014,7,61.00,2) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (3,2014,8,61.00,2) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (3,2014,9,61.00,2) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (3,2014,10,61.00,2) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (3,2014,11,61.00,2) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (3,2014,12,61.00,2) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (4,2014,1,10000.00,2) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (4,2014,2,10000.00,2) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (4,2014,3,10000.00,2) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (4,2014,4,10000.00,2) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (4,2014,5,10000.00,2) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (4,2014,6,10000.00,2) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (4,2014,7,10000.00,2) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (4,2014,8,10000.00,2) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (4,2014,9,10000.00,2) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (4,2014,10,10000.00,2) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (4,2014,11,10000.00,2) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (4,2014,12,10000.00,2) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (5,2014,1,9908.00,2) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (5,2014,2,9908.00,2) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (5,2014,3,9908.00,2) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (5,2014,4,9908.00,2) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (5,2014,5,9908.00,2) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (5,2014,6,9908.00,2) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (5,2014,7,9908.00,2) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (5,2014,8,9908.00,2) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (5,2014,9,9908.00,2) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (5,2014,10,9908.00,2) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (5,2014,11,9908.00,2) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (5,2014,12,9908.00,2) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (3,2014,1,83.00,4) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (3,2014,2,83.00,4) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (3,2014,3,83.00,4) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (3,2014,4,83.00,4) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (3,2014,5,83.00,4) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (3,2014,6,83.00,4) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (3,2014,7,83.00,4) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (3,2014,8,83.00,4) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (3,2014,9,83.00,4) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (3,2014,10,83.00,4) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (3,2014,11,83.00,4) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (3,2014,12,83.00,4) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (4,2014,1,8000.00,4) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (4,2014,2,8000.00,4) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (4,2014,3,8000.00,4) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (4,2014,4,8000.00,4) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (4,2014,5,8000.00,4) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (4,2014,6,8000.00,4) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (4,2014,7,8000.00,4) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (4,2014,8,8000.00,4) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (4,2014,9,8000.00,4) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (4,2014,10,8000.00,4) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (4,2014,11,8000.00,4) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (4,2014,12,8000.00,4) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (5,2014,1,13772.00,4) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (5,2014,2,13772.00,4) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (5,2014,3,13772.00,4) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (5,2014,4,13772.00,4) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (5,2014,5,13772.00,4) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (5,2014,6,13772.00,4) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (5,2014,7,13772.00,4) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (5,2014,8,13772.00,4) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (5,2014,9,13772.00,4) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (5,2014,10,13772.00,4) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (5,2014,11,13772.00,4) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (5,2014,12,13772.00,4) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (1,2015,1,10.00,1) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (1,2015,2,20.00,1) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (1,2015,3,30.00,1) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (1,2015,4,40.00,1) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (1,2015,5,50.00,1) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (1,2015,6,60.00,1) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (1,2015,7,70.00,1) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (1,2015,8,80.00,1) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (1,2015,9,90.00,1) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (1,2015,10,100.00,1) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (1,2015,11,110.00,1) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (1,2015,12,120.00,1) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (2,2015,1,0.90,1) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (2,2015,2,0.90,1) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (2,2015,3,0.90,1) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (2,2015,4,0.90,1) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (2,2015,5,0.90,1) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (2,2015,6,0.90,1) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (2,2015,7,0.90,1) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (2,2015,8,0.90,1) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (2,2015,9,0.90,1) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (2,2015,10,0.90,1) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (2,2015,11,0.90,1) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (2,2015,12,0.90,1) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (3,2015,1,10.00,1) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (3,2015,2,20.00,1) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (3,2015,3,30.00,1) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (3,2015,4,40.00,1) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (3,2015,5,50.00,1) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (3,2015,6,60.00,1) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (3,2015,7,70.00,1) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (3,2015,8,80.00,1) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (3,2015,9,90.00,1) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (3,2015,10,100.00,1) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (3,2015,11,110.00,1) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (3,2015,12,120.00,1) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (4,2015,1,15000.00,1) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (4,2015,2,15000.00,1) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (4,2015,3,15000.00,1) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (4,2015,4,15000.00,1) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (4,2015,5,15000.00,1) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (4,2015,6,15000.00,1) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (4,2015,7,15000.00,1) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (4,2015,8,15000.00,1) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (4,2015,9,15000.00,1) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (4,2015,10,15000.00,1) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (4,2015,11,15000.00,1) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (4,2015,12,15000.00,1) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (3,2015,1,0.00,2) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (3,2015,2,0.00,2) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (3,2015,3,0.00,2) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (3,2015,4,0.00,2) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (3,2015,5,0.00,2) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (3,2015,6,0.00,2) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (3,2015,7,0.00,2) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (3,2015,8,0.00,2) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (3,2015,9,0.00,2) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (3,2015,10,0.00,2) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (3,2015,11,0.00,2) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (3,2015,12,0.00,2) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (4,2015,1,0.00,2) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (4,2015,2,0.00,2) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (4,2015,3,0.00,2) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (4,2015,4,0.00,2) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (4,2015,5,0.00,2) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (4,2015,6,0.00,2) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (4,2015,7,0.00,2) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (4,2015,8,0.00,2) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (4,2015,9,0.00,2) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (4,2015,10,0.00,2) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (4,2015,11,0.00,2) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (4,2015,12,0.00,2) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (3,2015,1,0.00,4) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (3,2015,2,0.00,4) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (3,2015,3,0.00,4) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (3,2015,4,0.00,4) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (3,2015,5,0.00,4) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (3,2015,6,0.00,4) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (3,2015,7,0.00,4) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (3,2015,8,0.00,4) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (3,2015,9,0.00,4) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (3,2015,10,0.00,4) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (3,2015,11,0.00,4) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (3,2015,12,0.00,4) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (5,2015,1,10000.00,4) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (5,2015,2,10000.00,4) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (5,2015,3,10000.00,4) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (5,2015,4,10000.00,4) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (5,2015,5,10000.00,4) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (5,2015,6,10000.00,4) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (5,2015,7,10000.00,4) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (5,2015,8,10000.00,4) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (5,2015,9,10000.00,4) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (5,2015,10,10000.00,4) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (5,2015,11,10000.00,4) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (5,2015,12,10000.00,4) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (6,2015,1,10000.00,4) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (6,2015,2,10000.00,4) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (6,2015,3,10000.00,4) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (6,2015,4,10000.00,4) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (6,2015,5,10000.00,4) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (6,2015,6,10000.00,4) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (6,2015,7,10000.00,4) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (6,2015,8,10000.00,4) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (6,2015,9,10000.00,4) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (6,2015,10,10000.00,4) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (6,2015,11,10000.00,4) GO
INSERT INTO meta (indicador_id,ano,mes,meta,estagio_id) VALUES (6,2015,12,10000.00,4) GO
