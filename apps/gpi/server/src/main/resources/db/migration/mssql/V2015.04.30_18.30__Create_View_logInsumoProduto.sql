IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.VIEWS
        WHERE TABLE_NAME = 'view_logInsumoProduto')
    DROP VIEW view_logInsumoProduto
GO

Create View view_logInsumoProduto AS
Select
   Distinct
   Case
      When (LOGPAR.descricao_MOD = 1) then 1 else 0
   End as parcerias_MOD,
   percentualAdquiridoOutro_MOD, percentualClienteComercial_MOD,
   percentualClienteConsumidor_MOD, percentualClienteIndustria_MOD,
   percentualFabricaMinas_MOD, percentualICMS_MOD, percentualImportado_MOD,
   Case 
      When (LOGINS.valorEstimadoAno_MOD = 1 OR LOGINS.unidadeMedida_MOD = 1 
         OR LOGINS.quantidadeAno_MOD = 1 OR LOGINS.empresaFornecedor_MOD = 1 
         OR LOGINS.origem_MOD = 1 OR LOGINS.Nome_MOD = 1 OR LOGINS.ncm_MOD = 1) then 1 else 0
   End as insumos_MOD,
   interesseParceria_MOD, importacaoProduto_MOD, contribuinteSubstituto_MOD, 
   Case
      When (LOGCON.descricaoProduto_MOD = 1) then 1 else 0
   End as concorrentes_MOD,
   aquisicaoOutroEstado_MOD,
   Case
      When (LOGSER.descricao_MOD = 1) then 1 else 0
   End as servicos_MOD,
   Case
      When (LOGPRC.ano_MOD = 1 OR LOGPRC.areaPlantada_MOD = 1 OR LOGPRC.outro_MOD = 1 
         OR LOGPRC.moagemCana_MOD = 1 OR LOGPRC.geracaoEnergia_MOD = 1 
         OR LOGPRC.producaoAcucar_MOD = 1 OR LOGPRC.producaoEtanol_MOD = 1) then 1 else 0
   End as producoes_MOD,
   produtos_MOD,
   Case
      When (LOGPRD.tipo = 2 and (LOGPRD.quantidade1_MOD = 1 OR LOGPRD.quantidade2_MOD = 1 
         OR LOGPRD.quantidade3_MOD = 1 OR LOGPRD.sujeitoST_MOD = 1
         OR LOGPRD.unidadeMedida_MOD = 1 OR LOGPRD.tipo_MOD = 1 
         OR LOGPRD.ncm_MOD = 1 OR LOGPRD.nome_MOD = 1)) then 1 else 0
   End as produtosAdqEComPorMinas_MOD, 
   Case
      When (LOGPRD.tipo = 3 and (LOGPRD.quantidade1_MOD = 1 OR LOGPRD.quantidade2_MOD = 1 
         OR LOGPRD.quantidade3_MOD = 1 OR LOGPRD.sujeitoST_MOD = 1
         OR LOGPRD.unidadeMedida_MOD = 1 OR LOGPRD.tipo_MOD = 1 
         OR LOGPRD.ncm_MOD = 1 OR LOGPRD.nome_MOD = 1)) then 1 else 0
   End as produtosAdqOutrosEstadosParaCom_MOD, 
   Case
      When (LOGPRD.tipo = 1 and (LOGPRD.quantidade1_MOD = 1 OR LOGPRD.quantidade2_MOD = 1 
         OR LOGPRD.quantidade3_MOD = 1 OR LOGPRD.sujeitoST_MOD = 1
         OR LOGPRD.unidadeMedida_MOD = 1 OR LOGPRD.tipo_MOD = 1 
         OR LOGPRD.ncm_MOD = 1 OR LOGPRD.nome_MOD = 1)) then 1 else 0
   End as produtosFabEComPorMinas_MOD, 
   Case
      When (LOGPRD.tipo = 4 and (LOGPRD.quantidade1_MOD = 1 OR LOGPRD.quantidade2_MOD = 1 
         OR LOGPRD.quantidade3_MOD = 1 OR LOGPRD.sujeitoST_MOD = 1
         OR LOGPRD.unidadeMedida_MOD = 1 OR LOGPRD.tipo_MOD = 1 
         OR LOGPRD.ncm_MOD = 1 OR LOGPRD.nome_MOD = 1)) then 1 else 0
   End as produtosImpParaCom_MOD, 
   percentualMercadoExterior_MOD, percentualMercadoMinas_MOD,
   percentualMercadoNacional_MOD, percentualOrigemImportado_MOD, percentualOrigemMinas_MOD,
   percentualOrigemOutros_MOD, percentualTributoEfetivo_MOD, percentualValorAgregado_MOD,
   LOGINP.projeto_id as projetoId,  
   LOGINP.dataUltimaAlteracao as dataUltimaAlteracao
From LogInsumoProduto LOGINP
   Left Join logParceria LOGPAR 
   	on LOGPAR.logVersao = LOGINP.logVersao
   Left Join logInsumo LOGINS 
   	on LOGINS.logVersao = LOGINP.logVersao
   Left Join logConcorrente LOGCON 
   	on LOGCON.logVersao = LOGINP.logVersao
   Left Join logServico LOGSER 
   	on LOGSER.logVersao = LOGINP.logVersao
   Left Join logProducao LOGPRC 
   	on LOGPRC.logVersao = LOGINP.logVersao
   Left Join logProduto LOGPRD 
   	on LOGPRD.logVersao = LOGINP.logVersao
Where
   LOGINP.logVersao = (
   	Select max(logVersao) 
   	From LogInsumoProduto LOGINP2 
   	where LOGINP.projeto_id = LOGINP2.projeto_id
   )