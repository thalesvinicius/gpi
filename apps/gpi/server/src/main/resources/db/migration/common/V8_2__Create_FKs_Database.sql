-- Create Foreign Key Constraints 

ALTER TABLE AnexoInformacaoAdicional ADD CONSTRAINT FK_AnexoInformacaoAdicional_InformacaoAdicional 
	FOREIGN KEY (informacaoAdicional_id) REFERENCES InformacaoAdicional (id);

ALTER TABLE AnexoInformacaoAdicional ADD CONSTRAINT FK_AnexoInformacaoAdicional_Anexo 
	FOREIGN KEY (anexo_id) REFERENCES Anexo (id);

ALTER TABLE AnexoLocalizacao ADD CONSTRAINT FK_AnexoLocalizacao_Anexo 
	FOREIGN KEY (anexo_id) REFERENCES Anexo (id);

ALTER TABLE AnexoLocalizacao ADD CONSTRAINT FK_AnexoLocalizacao_Localizacao 
	FOREIGN KEY (localizacao_id) REFERENCES Localizacao (id);

ALTER TABLE AnexoPesquisa ADD CONSTRAINT FK_PesquisaAnexo_Anexo 
	FOREIGN KEY (anexo_id) REFERENCES Anexo (id);

ALTER TABLE AnexoPesquisa ADD CONSTRAINT FK_PesquisaAnexo_Pesquisa 
	FOREIGN KEY (pesquisa_id) REFERENCES Pesquisa (id);

ALTER TABLE AnexoProjeto ADD CONSTRAINT FK_AnexoProjeto_Anexo 
	FOREIGN KEY (anexo_id) REFERENCES Anexo (id);

ALTER TABLE AnexoProjeto ADD CONSTRAINT FK_AnexoProjeto_Projeto 
	FOREIGN KEY (projeto_id) REFERENCES Projeto (id);

ALTER TABLE AtividadeInstrumento ADD CONSTRAINT FK_AtividadeInstrumento_Anexo 
	FOREIGN KEY (anexo_id) REFERENCES Anexo (id);

ALTER TABLE AtividadeInstrumento ADD CONSTRAINT FK_AtividadeInstrumento_AtividadeLocal 
	FOREIGN KEY (atividadeLocal_id) REFERENCES AtividadeLocal (id);

ALTER TABLE AtividadeInstrumento ADD CONSTRAINT FK_AtividadeInstrumento_InstrumentoFormalizacao 
	FOREIGN KEY (instrumentoFormalizacao_id) REFERENCES InstrumentoFormalizacao (id);

ALTER TABLE AtividadeLocal ADD CONSTRAINT FK_AtividadeLocal_Local 
	FOREIGN KEY (local_id) REFERENCES Local (id);

ALTER TABLE AtividadeProjeto ADD CONSTRAINT FK_AtividadeProjeto_Anexo 
	FOREIGN KEY (anexo_id) REFERENCES Anexo (id);

ALTER TABLE AtividadeProjeto ADD CONSTRAINT FK_AtividadeProjeto_AtividadeLocal 
	FOREIGN KEY (atividadeLocal_id) REFERENCES AtividadeLocal (id);

ALTER TABLE AtividadeProjeto ADD CONSTRAINT FK_AtividadeProjeto_Projeto 
	FOREIGN KEY (projeto_id) REFERENCES Projeto (id);

ALTER TABLE AtividadeTemplate ADD CONSTRAINT FK_AtividadeTemplate_AtividadeLocal 
	FOREIGN KEY (atividadeLocal_id) REFERENCES AtividadeLocal (id);

ALTER TABLE AtividadeTemplate ADD CONSTRAINT FK_AtividadeTemplate_Template 
	FOREIGN KEY (template_id) REFERENCES Template (id);

ALTER TABLE CadeiaProdutiva ADD CONSTRAINT FK_CadeiaProdutiva_Departamento 
	FOREIGN KEY (departamento_id) REFERENCES Departamento (id);

ALTER TABLE CNAE ADD CONSTRAINT FK_CNAE_CNAE 
	FOREIGN KEY (CNAESuperior_id) REFERENCES CNAE (id);

ALTER TABLE CNAEEmpresa ADD CONSTRAINT FK_CNAEEmpresa_CNAE 
	FOREIGN KEY (CNAE_id) REFERENCES CNAE (id);

ALTER TABLE CNAEEmpresa ADD CONSTRAINT FK_CNAEEmpresa_Empresa 
	FOREIGN KEY (empresa_id) REFERENCES Empresa (id);

ALTER TABLE Concorrente ADD CONSTRAINT FK_Concorrente_InsumoProduto 
	FOREIGN KEY (insumoProduto_id) REFERENCES InsumoProduto (id);

ALTER TABLE Cronograma ADD CONSTRAINT FK_Cronograma_Projeto 
	FOREIGN KEY (projeto_id) REFERENCES Projeto (id);

ALTER TABLE Emprego ADD CONSTRAINT FK_Emprego_Projeto 
	FOREIGN KEY (projeto_id) REFERENCES Projeto (id);

ALTER TABLE Empresa ADD CONSTRAINT FK_Empresa_Anexo 
	FOREIGN KEY (anexo_id) REFERENCES Anexo (id);

ALTER TABLE Empresa ADD CONSTRAINT FK_Empresa_CadeiaProdutiva 
	FOREIGN KEY (cadeiaProdutiva_id) REFERENCES CadeiaProdutiva (id);

ALTER TABLE Empresa ADD CONSTRAINT FK_Empresa_NaturezaJuridica 
	FOREIGN KEY (naturezaJuridica_id) REFERENCES NaturezaJuridica (id);

ALTER TABLE Endereco ADD CONSTRAINT FK_Endereco_Municipio 
	FOREIGN KEY (municipio_id) REFERENCES Municipio (id);

ALTER TABLE Endereco ADD CONSTRAINT FK_Endereco_Pais 
	FOREIGN KEY (pais_id) REFERENCES Pais (id);

ALTER TABLE Endereco ADD CONSTRAINT FK_Endereco_UF 
	FOREIGN KEY (uf_id) REFERENCES UF (id);

ALTER TABLE EnergiaContratada ADD CONSTRAINT FK_EnergiaContratada_Infraestrutura 
	FOREIGN KEY (infraestrutura_id) REFERENCES Infraestrutura (id);

ALTER TABLE EstagioProjeto ADD CONSTRAINT FK_EstagioProjeto_Estagio 
	FOREIGN KEY (estagio_id) REFERENCES Estagio (id);

ALTER TABLE EstagioProjeto ADD CONSTRAINT FK_EstagioProjeto_Projeto 
	FOREIGN KEY (projeto_id) REFERENCES Projeto (id);

ALTER TABLE EtapaMeioAmbiente ADD CONSTRAINT FK_EtapaMeioAmbiente_MeioAmbiente 
	FOREIGN KEY (meioAmbiente_id) REFERENCES MeioAmbiente (id);

ALTER TABLE FaturamentoAnterior ADD CONSTRAINT FK_FaturamentoAnterior_Financeiro 
	FOREIGN KEY (financeiro_id) REFERENCES Financeiro (id);

ALTER TABLE FaturamentoPrevisto ADD CONSTRAINT FK_FaturamentoPrevisto_Financeiro 
	FOREIGN KEY (financeiro_id) REFERENCES Financeiro (id);

ALTER TABLE Financeiro ADD CONSTRAINT FK_Financeiro_Projeto 
	FOREIGN KEY (projeto_id) REFERENCES Projeto (id);

ALTER TABLE FinanceiroAnexo ADD CONSTRAINT FK_FinanceiroAnexo_Anexo 
	FOREIGN KEY (anexo_id) REFERENCES Anexo (id);

ALTER TABLE FinanceiroAnexo ADD CONSTRAINT FK_FinanceiroAnexo_Financeiro 
	FOREIGN KEY (financeiro_id) REFERENCES Financeiro (id);

ALTER TABLE HistoricoSituacaoInstrumentoFormalizacao ADD CONSTRAINT FK_HistoricoSituacaoInstrumentoFormalizacao_InstrumentoFormalizacao 
	FOREIGN KEY (instrumentoFormalizacao_id) REFERENCES InstrumentoFormalizacao (id);

ALTER TABLE HistoricoSituacaoInstrumentoFormalizacao ADD CONSTRAINT FK_HistoricoSituacaoInstrumentoFormalizacao_SituacaoInstrumentoFormalizacao 
	FOREIGN KEY (situacaoInstrumentoFormalizacao_id) REFERENCES SituacaoInstrumentoFormalizacao (id);

ALTER TABLE HistoricoSituacaoProjeto ADD CONSTRAINT FK_HistoricoSituacaoProjeto_SituacaoProjeto 
	FOREIGN KEY (situacaoProjeto_id) REFERENCES SituacaoProjeto (id);

ALTER TABLE HistoricoSituacaoProjeto ADD CONSTRAINT FK_HistoricoSituacaoProjeto_Projeto
        FOREIGN KEY (projeto_id) REFERENCES Projeto (id);

ALTER TABLE InformacaoAdicional ADD CONSTRAINT FK_InformacaoAdicional_Empresa 
	FOREIGN KEY (empresa_id) REFERENCES Empresa (id);

ALTER TABLE Infraestrutura ADD CONSTRAINT FK_Infraestrutura_Projeto 
	FOREIGN KEY (projeto_id) REFERENCES Projeto (id);

ALTER TABLE Insumo ADD CONSTRAINT FK_Insumo_InsumoProduto 
	FOREIGN KEY (insumoProduto_id) REFERENCES InsumoProduto (id);

ALTER TABLE Insumo ADD CONSTRAINT FK_Insumo_NCM 
	FOREIGN KEY (ncm_id) REFERENCES NCM (id);

ALTER TABLE InsumoProduto ADD CONSTRAINT FK_InsumoProduto_Projeto 
	FOREIGN KEY (projeto_id) REFERENCES Projeto (id);

ALTER TABLE InvestimentoPrevisto ADD CONSTRAINT FK_InvestimentoPrevisto_Financeiro 
	FOREIGN KEY (financeiro_id) REFERENCES Financeiro (id);

ALTER TABLE Localizacao ADD CONSTRAINT FK_Localizacao_MicroRegiao 
	FOREIGN KEY (microRegiao_id) REFERENCES MicroRegiao (id);

ALTER TABLE Localizacao ADD CONSTRAINT FK_Localizacao_Municipio 
	FOREIGN KEY (municipio_id) REFERENCES Municipio (id);

ALTER TABLE Localizacao ADD CONSTRAINT FK_Localizacao_Projeto 
	FOREIGN KEY (projeto_id) REFERENCES Projeto (id);

ALTER TABLE Localizacao ADD CONSTRAINT FK_Localizacao_RegiaoPlanejamento 
	FOREIGN KEY (regiaoPlanejamento_id) REFERENCES RegiaoPlanejamento (id);

ALTER TABLE MeioAmbiente ADD CONSTRAINT FK_MeioAmbiente_Projeto 
	FOREIGN KEY (projeto_id) REFERENCES Projeto (id);

ALTER TABLE MeioAmbienteAnexo ADD CONSTRAINT FK_MeioAmbienteAnexo_Anexo 
	FOREIGN KEY (anexo_id) REFERENCES Anexo (id);

ALTER TABLE MeioAmbienteAnexo ADD CONSTRAINT FK_MeioAmbienteAnexo_MeioAmbiente 
	FOREIGN KEY (meioAmbiente_id) REFERENCES MeioAmbiente (id);

ALTER TABLE MicroRegiao ADD CONSTRAINT FK_MicroRegiao_RegiaoPlanejamento 
	FOREIGN KEY (regiaoPlanejamento_id) REFERENCES RegiaoPlanejamento (id);

ALTER TABLE Municipio ADD CONSTRAINT FK_Municipio_MicroRegiao 
	FOREIGN KEY (microRegiao_id) REFERENCES MicroRegiao (id);

ALTER TABLE Municipio ADD CONSTRAINT FK_Municipio_RegiaoPlanejamento 
	FOREIGN KEY (regiaoPlanejamento_id) REFERENCES RegiaoPlanejamento (id);

ALTER TABLE Municipio ADD CONSTRAINT FK_Municipio_UF 
	FOREIGN KEY (UF_id) REFERENCES UF (id);

ALTER TABLE NomeEmpresa ADD CONSTRAINT FK_NomeEmpresa_Empresa 
	FOREIGN KEY (empresa_id) REFERENCES Empresa (id);

ALTER TABLE OrigemRecurso ADD CONSTRAINT FK_OrigemRecurso_Financeiro 
	FOREIGN KEY (financeiro_id) REFERENCES Financeiro (id);

ALTER TABLE OrigemRecurso ADD CONSTRAINT FK_OrigemRecurso_Pais 
	FOREIGN KEY (pais_id) REFERENCES Pais (id);

ALTER TABLE OrigemRecurso ADD CONSTRAINT FK_OrigemRecurso_UF 
	FOREIGN KEY (uf_id) REFERENCES UF (id);

ALTER TABLE Parceria ADD CONSTRAINT FK_Parceria_InsumoProduto 
	FOREIGN KEY (insumoProduto_id) REFERENCES InsumoProduto (id);

ALTER TABLE Pesquisa ADD CONSTRAINT FK_Pesquisa_InstrumentoFormalizacao 
	FOREIGN KEY (instrumentoFormalizacao_id) REFERENCES InstrumentoFormalizacao (id);

ALTER TABLE PesquisaPerguntaPesquisa ADD CONSTRAINT FK_PesquisaPerguntaPesquisa_PerguntaPesquisa 
	FOREIGN KEY (perguntaPesquisa_id) REFERENCES PerguntaPesquisa (id);

ALTER TABLE PesquisaPerguntaPesquisa ADD CONSTRAINT FK_PesquisaPerguntaPesquisa_Pesquisa 
	FOREIGN KEY (pesquisa_id) REFERENCES Pesquisa (id);

ALTER TABLE Pleito ADD CONSTRAINT FK_Pleito_Projeto 
	FOREIGN KEY (projeto_id) REFERENCES Projeto (id);

ALTER TABLE Producao ADD CONSTRAINT FK_Producao_InsumoProduto 
	FOREIGN KEY (insumoProduto_id) REFERENCES InsumoProduto (id);

ALTER TABLE Produto ADD CONSTRAINT FK_Produto_InsumoProduto 
	FOREIGN KEY (insumoProduto_id) REFERENCES InsumoProduto (id);

ALTER TABLE Produto ADD CONSTRAINT FK_Produto_NCM 
	FOREIGN KEY (ncm_id) REFERENCES NCM (id);

ALTER TABLE Projeto ADD CONSTRAINT FK_Projeto_CadeiaProdutiva 
	FOREIGN KEY (cadeiaProdutiva_id) REFERENCES CadeiaProdutiva (id);

ALTER TABLE Projeto ADD CONSTRAINT FK_Projeto_Empresa 
	FOREIGN KEY (empresa_id) REFERENCES Empresa (id);

ALTER TABLE Projeto ADD CONSTRAINT FK_Projeto_UnidadeEmpresa 
	FOREIGN KEY (unidade_id) REFERENCES UnidadeEmpresa (id);

ALTER TABLE ProjetoInstrumentoFormalizacao ADD CONSTRAINT FK_ProjetoInstrumentoFormalizacao_InstrumentoFormalizacao 
	FOREIGN KEY (instrumentoFormalizacao_id) REFERENCES InstrumentoFormalizacao (id);

ALTER TABLE ProjetoInstrumentoFormalizacao ADD CONSTRAINT FK_ProjetoInstrumentoFormalizacao_Projeto 
	FOREIGN KEY (projeto_id) REFERENCES Projeto (id);

ALTER TABLE Publicacao ADD CONSTRAINT FK_Publicacao_InstrumentoFormalizacao 
	FOREIGN KEY (instrumentoFormalizacao_id) REFERENCES InstrumentoFormalizacao (id);

ALTER TABLE PublicacaoAnexo ADD CONSTRAINT FK_PublicacaoAnexo_Anexo 
	FOREIGN KEY (anexo_id) REFERENCES Anexo (id);

ALTER TABLE PublicacaoAnexo ADD CONSTRAINT FK_PublicacaoAnexo_Publicacao 
	FOREIGN KEY (publicacao_id) REFERENCES Publicacao (id);

ALTER TABLE Servico ADD CONSTRAINT FK_Servico_InsumoProduto 
	FOREIGN KEY (insumoProduto_id) REFERENCES InsumoProduto (id);

ALTER TABLE Signatario ADD CONSTRAINT FK_Signatario_InstrumentoFormalizacao 
	FOREIGN KEY (instrumentoFormalizacao_id) REFERENCES InstrumentoFormalizacao (id);

ALTER TABLE UF ADD CONSTRAINT FK_UF_Pais 
	FOREIGN KEY (pais_id) REFERENCES Pais (id);

ALTER TABLE UnidadeEmpresa ADD CONSTRAINT FK_UnidadeEmpresa_Empresa 
	FOREIGN KEY (empresa_id) REFERENCES Empresa (id);

ALTER TABLE UsoFonte ADD CONSTRAINT FK_UsoFonte_Financeiro 
	FOREIGN KEY (financeiro_id) REFERENCES Financeiro (id);

ALTER TABLE Usuario ADD CONSTRAINT FK_Usuario_GrupoUsuario 
	FOREIGN KEY (grupoUsuario_id) REFERENCES GrupoUsuario (id);

ALTER TABLE UsuarioExterno ADD CONSTRAINT FK_ContatoExterno_Empresa 
	FOREIGN KEY (empresa_id) REFERENCES Empresa (id);

ALTER TABLE UsuarioExterno ADD CONSTRAINT FK_ContatoExterno_Usuario 
	FOREIGN KEY (id) REFERENCES Usuario (id);

ALTER TABLE UsuarioExterno ADD CONSTRAINT FK_UsuarioExterno_UnidadeEmpresa 
	FOREIGN KEY (unidade_id) REFERENCES UnidadeEmpresa (id);

ALTER TABLE UsuarioInterno ADD CONSTRAINT FK_UsuarioInterno_Departamento 
	FOREIGN KEY (departamento_id) REFERENCES Departamento (id);

ALTER TABLE UsuarioInterno ADD CONSTRAINT FK_UsuarioInterno_Usuario 
	FOREIGN KEY (id) REFERENCES Usuario (id);

ALTER TABLE AccessToken ADD CONSTRAINT FK_AccessToken_Usuario 
	FOREIGN KEY (user_id) REFERENCES Usuario (id);

ALTER TABLE VerificationToken ADD CONSTRAINT FK_VerificationToken_Usuario 
	FOREIGN KEY (user_id) REFERENCES Usuario (id);
