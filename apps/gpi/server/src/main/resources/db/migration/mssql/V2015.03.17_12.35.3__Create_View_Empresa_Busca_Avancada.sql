IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.VIEWS
        WHERE TABLE_NAME = 'VIEW_EMPRESA_BUSCA_AVANCADA')
    DROP VIEW VIEW_EMPRESA_BUSCA_AVANCADA
GO

Create view VIEW_EMPRESA_BUSCA_AVANCADA AS

select distinct
e.*, 
vend.municipio_id,
vend.municipio,
vend.municipioCidade,
vend.uf_id,
vend.siglaEstado,
vend.estado,
vend.logradouro,
vend.numero,
vend.complemento,
vend.CEP,
vend.bairro,
vend.cidade,
vend.pais_id,
vend.pais,
vend.microregiao_id,
vend.microregiao,
vend.regiaoPlanejamento_id,
vend.RegiaoPlanejamento,
vend.enderecoCompleto,
cp.descricao as cadeiaProdutiva,
tab.possuiProjetos,
nj.descricao as naturezaJuridica,
CASE e.origemEmpresa
WHEN 1 THEN 'Mineira'
WHEN 2 THEN 'Outros Estados'
WHEN 3 THEN 'Internacional'
ELSE ''
END as origemEmpresaDescricao
from empresa e 
left join NaturezaJuridica nj on nj.id = e.naturezaJuridica_id
left join VIEW_ENDERECO_BUSCA_AVANCADA vend on e.endereco_id = vend.id
left join cadeiaProdutiva cp on e.cadeiaProdutiva_id = cp.id
left join (
	select
	e1.id,
	Case p1.id 
	WHEN null THEN 0
	ELSE 1
	END as possuiProjetos
	from empresa e1 
	left join projeto p1 on p1.empresa_id = e1.id
) tab on e.id = tab.id;