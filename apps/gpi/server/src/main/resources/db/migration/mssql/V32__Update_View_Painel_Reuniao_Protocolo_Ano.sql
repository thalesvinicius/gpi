IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.VIEWS
        WHERE TABLE_NAME = 'VIEW_RELATORIO_PAINEL_REUNIOES_PROTOCOLOS_ASSINADOS_MES_ANO')
    DROP VIEW VIEW_RELATORIO_PAINEL_REUNIOES_PROTOCOLOS_ASSINADOS_MES_ANO
GO 
CREATE VIEW VIEW_RELATORIO_PAINEL_REUNIOES_PROTOCOLOS_ASSINADOS_MES_ANO
AS 
WITH projeto_ver_congelada_ou_atual AS (
    SELECT p.* 
    FROM (
            SELECT p.id, p.familia_id 
            FROM projeto p
            WHERE p.ultimaversao = 1 AND p.versao = 0 AND situacaoatual_id <> 4 
            UNION   
            SELECT
            p.id, p.familia_id
            FROM projeto p
            WHERE p.ultimaversao = 0
            AND situacaoatual_id <> 4
            AND p.id in
            (
               SELECT
               top(1) p2.id
               FROM projeto p2
               WHERE p2.ultimaversao = 0
               AND p2.situacaoatual_id <> 4
               and p2.familia_id = p.familia_id
               order by p2.versao desc
            )       
    ) as tab
    INNER JOIN projeto p ON tab.id = p.id
)
SELECT
    DPS.id AS diretoriaId,
    DPS.descricao AS diretoria,
    DPT.id AS gerenciaId,
    DPT.descricao AS gerencia,    
    INS.tipo AS tipoInstrumentoId,
    DATEPART(mm, INS.dataassinatura) as mesAssinaturaInstrumento, 	
    DATEPART(yyyy, INS.dataassinatura) AS anoAssinaturaInstrumento,
    COUNT(INS.id) AS numProtocolos
FROM projeto_ver_congelada_ou_atual PRJ      
    LEFT JOIN Usuario USR ON USR.id = PRJ.usuarioinvestimento_id
    LEFT JOIN UsuarioInterno UIN ON UIN.id = USR.id
    LEFT JOIN Departamento DPT ON DPT.id = UIN.departamento_id
    LEFT JOIN ProjetoInstrumentoFormalizacao PEI ON PEI.projeto_id = PRJ.id
    LEFT JOIN InstrumentoFormalizacao INS ON INS.id = PEI.instrumentoformalizacao_id	
    LEFT JOIN HistoricoSituacaoInstrumentoFormalizacao HSI ON HSI.id = INS.historicoSituacaoInstrumentoFormalizacao_id
    LEFT JOIN situacaoInstrumentoFormalizacao SIT ON SIT.id = HSI.situacaoInstrumentoFormalizacao_id     
    LEFT JOIN Departamento DPS ON DPS.id = DPT.departamentosuperior_id
WHERE 
INS.dataassinatura IS NOT NULL    
GROUP BY
    DPS.id,
    DPS.descricao,    
    DPT.id,
    DPT.descricao,    
    INS.tipo,
    DATEPART(mm, INS.dataassinatura), 	
    DATEPART(yyyy, INS.dataassinatura);
