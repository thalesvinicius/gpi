IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.VIEWS
        WHERE TABLE_NAME = 'VIEW_RELATORIO_PESQUISAS_RECEBIDAS')
    DROP VIEW VIEW_RELATORIO_PESQUISAS_RECEBIDAS;
GO

CREATE VIEW VIEW_RELATORIO_PESQUISAS_RECEBIDAS
AS
SELECT 
    PES.id AS pesquisaId,
    EMP.nomePrincipal AS empresa,
    DPR.sigla AS gerencia,
    DPR.id AS gerenciaId,
    DPS.sigla AS diretoria,
    DPS.id AS diretoriaId,
    USR.nome AS responsavel,
    PES.indiceSatisfacao AS porcentagemPesquisa,
    MTA.meta AS meta,
    PES.dataResposta AS dataRecebimento,
    PES.comentario AS comentario	
FROM Pesquisa PES
    INNER JOIN InstrumentoFormalizacao INS ON INS.id = PES.instrumentoformalizacao_id
    INNER JOIN ProjetoInstrumentoFormalizacao PIN ON PIN.instrumentoformalizacao_id = INS.id
    INNER JOIN Projeto PRJ ON PRJ.id = PIN.projeto_id
    INNER JOIN Empresa EMP ON EMP.id = PRJ.empresa_id
    INNER JOIN Usuario USR ON USR.id = PRJ.usuarioInvestimento_id 
    INNER JOIN UsuarioInterno UIN ON UIN.id = USR.id
    INNER JOIN Departamento DPR ON DPR.id = UIN.departamento_id
    INNER JOIN Departamento DPS ON DPS.id = DPR.departamentosuperior_id
    INNER JOIN Meta MTA ON MTA.indicador_id = 2
WHERE
    MTA.mes = datepart(mm, PES.dataResposta)
    AND MTA.ano = datepart(yyyy, PES.dataResposta)   
