IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.VIEWS
        WHERE TABLE_NAME = 'VIEW_RELATORIO_AGENDA_POSITIVA_DF')
    DROP VIEW VIEW_RELATORIO_AGENDA_POSITIVA_DF
GO 

Create View VIEW_RELATORIO_AGENDA_POSITIVA_DF 
AS
Select
proj.id,
proj.nome Projeto,
proj.situacaoAtual_id,
proj.versao,
proj.familia_id,
proj.estagioAtual_id,
'DF' AS EstagioPrevisto,
1 AS ESTAGIO_ID ,
proVersao.ultimaVersao,
(CASE WHEN proj.unidade_id IS NOT NULL THEN uni.nome ELSE empa.nomePrincipal END) as NomeEmpresa,
usua.Nome UsuarioResponsavel,
usua.id UsuarioResponsavel_id,
depa.sigla Departamento,
depa.id departamento_id,
rpla.nome RegiaoPlanejamento,
Municipio.nome Municipio,
rpla.id RegiaoPlanejamento_id,
cpro.descricao CadeiaProdutiva,
cpro.id cadeiaprodutiva_id,
inst.dataAssinaturaPrevista as Dt_Previsao,
inst.dataAssinatura as Dt_Real,
depa.departamentosuperior_id as departamentoSuperior_id,
proVersao.isLastVersion as isLastVersion
    ,
(select sum(valor) from faturamentoAnterior a where a.financeiro_id = finc.id group by a.financeiro_id ) /1000 totalFaturamentoAnterior
    ,
(select sum(valor) from faturamentoprevisto where ano = (select min(ano) from faturamentoprevisto where financeiro_id = finc.id) and financeiro_id = finc.id) /1000 totalFaturamentoPrevisto
    ,
(select sum(valor) from investimentoPrevisto c where c.financeiro_id = finc.id group by c.financeiro_id ) /1000 totalInvestimentoPrevisto
    ,
(select sum(direto) from emprego d where d.projeto_id = proj.id  AND d.tipo NOT IN(1,3,5,6) group by d.projeto_id ) totalEmpregoDireto
    ,
(
   select
   sum(indireto)
   from emprego e
   where e.projeto_id = proj.id
   group by e.projeto_id
)
totalEmpregoIndireto
From  view_projeto_ver_congelada_ou_atual proVersao
Inner Join projeto proj on proVersao.familia_id = proj.familia_id
left join financeiro finc on proVersao.id = finc.projeto_id
Left join usuario usua on proj.usuarioinvestimento_id = usua.id
Left join usuarioInterno uint on usua.id = uint.id
Left join departamento depa on uint.departamento_id = depa.id
Left join empresa empa on proj.empresa_id = empa.id
Left join localizacao loca on proj.id = loca.projeto_id
Left join Municipio on Municipio.id = loca.municipio_id
Left join regiaoPlanejamento rpla on loca.regiaoPlanejamento_id = rpla.id
Left join cronograma cron on proj.id = cron.projeto_id
Left join projeto projInst on projInst.familia_id = proj.familia_id
Left join
(
   Select
   dataAssinaturaPrevista, projeto_id, dataAssinatura
   from projetoInstrumentoFormalizacao pIns
   inner join InstrumentoFormalizacao ifor on pIns.instrumentoFormalizacao_id = iFor.id
   and ifor.tipo = 4
   and ifor.situacaoAtual_id <> 2
   and ifor.situacaoAtual_id <> 4
)
inst on projInst.id = inst.projeto_id
Left join cadeiaProdutiva cpro on proj.cadeiaProdutiva_id = cpro.id
LEFT JOIN UnidadeEmpresa uni ON uni.id=proj.unidade_id
where inst.dataAssinaturaPrevista IS NOT NULL
AND proj.situacaoAtual_id = 1 -- Ativo
AND (proj.estagioAtual_id in(2, 4, 5) OR inst.dataAssinatura IS NOT NULL)
AND proj.ultimaVersao = 1;