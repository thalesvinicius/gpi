CREATE TABLE LogAccessToken(
	[id] [bigint] NOT NULL,
	[logVersao] [bigint] NOT NULL,
	[logAcao] [smallint] NULL,
	[lastUpdated] [datetime2](7) NULL,
	[timeCreated] [datetime2](7) NULL,
	[token] [varchar](36) NULL,
	[user_id] [bigint] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC,
	[logVersao] ASC
))
;

CREATE TABLE LogAnexo(
	[id] [bigint] NOT NULL,
	[logVersao] [bigint] NOT NULL,
	[logAcao] [smallint] NULL,
	[data] [date] NULL,
	[extencao] [varchar](5) NULL,
	[nome] [varchar](255) NULL,
	[local] [varchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC,
	[logVersao] ASC
))
;

CREATE TABLE LogAnexoInformacaoAdicional(
	[logVersao] [bigint] NOT NULL,
	[informacaoAdicional_id] [bigint] NOT NULL,
	[anexo_id] [bigint] NOT NULL,
	[logAcao] [smallint] NULL,
PRIMARY KEY CLUSTERED 
(
	[logVersao] ASC,
	[informacaoAdicional_id] ASC,
	[anexo_id] ASC
))
;

CREATE TABLE LogAnexoLocalizacao(
	[id] [bigint] NOT NULL,
	[logVersao] [bigint] NOT NULL,
	[logAcao] [smallint] NULL,
	[anexoAreaLocal] [bit] NULL,
	[tipoLocalizacao] [int] NULL,
	[anexo_id] [bigint] NULL,
	[localizacao_id] [bigint] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC,
	[logVersao] ASC
))
;

CREATE TABLE LogAnexoPesquisa(
	[logVersao] [bigint] NOT NULL,
	[pesquisa_id] [bigint] NOT NULL,
	[anexo_id] [bigint] NOT NULL,
	[logAcao] [smallint] NULL,
PRIMARY KEY CLUSTERED 
(
	[logVersao] ASC,
	[pesquisa_id] ASC,
	[anexo_id] ASC
))
;

CREATE TABLE LogAnexoProjeto(
	[logVersao] [bigint] NOT NULL,
	[projeto_id] [bigint] NOT NULL,
	[anexo_id] [bigint] NOT NULL,
	[logAcao] [smallint] NULL,
PRIMARY KEY CLUSTERED 
(
	[logVersao] ASC,
	[projeto_id] ASC,
	[anexo_id] ASC
))
;

CREATE TABLE LogAnexoRelatorioAcompanhamento(
	[logVersao] [bigint] NOT NULL,
	[relatorioAcompanhamento_id] [bigint] NOT NULL,
	[anexo_id] [bigint] NOT NULL,
	[logAcao] [smallint] NULL,
PRIMARY KEY CLUSTERED 
(
	[logVersao] ASC,
	[relatorioAcompanhamento_id] ASC,
	[anexo_id] ASC
))
;

CREATE TABLE LogAtividadeInstrumento(
	[id] [bigint] NOT NULL,
	[logVersao] [bigint] NOT NULL,
	[logAcao] [smallint] NULL,
	[dataUltimaAlteracao] [datetime2](7) NULL,
	[bloqueiaICE] [bit] NULL,
	[cc] [bit] NULL,
	[dataConclusao] [datetime2](7) NULL,
	[dataInicio] [datetime2](7) NULL,
	[df] [bit] NULL,
	[ii] [bit] NULL,
	[obrigatorio] [bit] NULL,
	[observacao] [varchar](255) NULL,
	[oi] [bit] NULL,
	[paralela] [bit] NULL,
	[posicao] [int] NULL,
	[usuarioResponsavel_id] [bigint] NULL,
	[anexo_id] [bigint] NULL,
	[atividadeLocal_id] [bigint] NULL,
	[instrumentoFormalizacao_id] [bigint] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC,
	[logVersao] ASC
))
;


CREATE TABLE LogAtividadeLocal(
	[id] [bigint] NOT NULL,
	[logVersao] [bigint] NOT NULL,
	[logAcao] [smallint] NULL,
	[ativo] [bit] NULL,
	[descricao] [varchar](255) NULL,
	[local_id] [bigint] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC,
	[logVersao] ASC
))
;

CREATE TABLE LogAtividadeProjeto(
	[id] [bigint] NOT NULL,
	[logVersao] [bigint] NOT NULL,
	[logAcao] [smallint] NULL,
	[dataUltimaAlteracao] [datetime2](7) NULL,
	[bloqueiaICE] [bit] NULL,
	[cc] [bit] NULL,
	[dataConclusao] [datetime2](7) NULL,
	[df] [bit] NULL,
	[ii] [bit] NULL,
	[obrigatorio] [bit] NULL,
	[observacao] [varchar](255) NULL,
	[oi] [bit] NULL,
	[posicao] [int] NULL,
	[usuarioResponsavel_id] [bigint] NULL,
	[anexo_id] [bigint] NULL,
	[atividadeLocal_id] [bigint] NULL,
	[projeto_id] [bigint] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC,
	[logVersao] ASC
))
;

CREATE TABLE LogAtividadeTemplate(
	[id] [bigint] NOT NULL,
	[logVersao] [bigint] NOT NULL,
	[logAcao] [smallint] NULL,
	[bloqueiaICE] [bit] NULL,
	[cc] [bit] NULL,
	[df] [bit] NULL,
	[ii] [bit] NULL,
	[obrigatorio] [bit] NULL,
	[oi] [bit] NULL,
	[posicao] [int] NULL,
	[atividadeLocal_id] [bigint] NULL,
	[template_id] [bigint] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC,
	[logVersao] ASC
))
;

CREATE TABLE LogCadeiaProdutiva(
	[id] [bigint] NOT NULL,
	[logVersao] [bigint] NOT NULL,
	[logAcao] [smallint] NULL,
	[descricao] [varchar](255) NULL,
	[departamento_id] [bigint] NULL,
	[usuarioResponsavel_id] [bigint] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC,
	[logVersao] ASC
))
;

CREATE TABLE LogCargo(
	[id] [bigint] NOT NULL,
	[logVersao] [bigint] NOT NULL,
	[logAcao] [smallint] NULL,
	[descricao] [varchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC,
	[logVersao] ASC
))
;

CREATE TABLE LogCNAE(
	[nivel] [int] NOT NULL,
	[id] [varchar](255) NOT NULL,
	[logVersao] [bigint] NOT NULL,
	[logAcao] [smallint] NULL,
	[denominacao] [varchar](255) NULL,
	[CNAESuperior_id] [varchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC,
	[logVersao] ASC
))
;

CREATE TABLE LogCNAEEmpresa(
	[CNAE_id] [varchar](255) NOT NULL,
	[empresa_id] [bigint] NOT NULL,
	[logVersao] [bigint] NOT NULL,
	[logAcao] [smallint] NULL,
	[nivel] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[CNAE_id] ASC,
	[empresa_id] ASC,
	[logVersao] ASC
))
;

CREATE TABLE LogComposicaoSocietaria(
	[id] [bigint] NOT NULL,
	[logVersao] [bigint] NOT NULL,
	[logAcao] [smallint] NULL,
	[cpf_cnpj] [varchar](20) NULL,
	[empresa_grupo] [varchar](100) NULL,
	[participacao_societaria] [float] NULL,
	[socio] [varchar](50) NULL,
	[empresa_id] [bigint] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC,
	[logVersao] ASC
))
;

CREATE TABLE LogConcorrente(
	[id] [bigint] NOT NULL,
	[logVersao] [bigint] NOT NULL,
	[logAcao] [smallint] NULL,
	[descricaoProduto] [varchar](255) NULL,
	[nome] [varchar](255) NULL,
	[insumoProduto_id] [bigint] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC,
	[logVersao] ASC
))
;

CREATE TABLE LogCronograma(
	[id] [bigint] NOT NULL,
	[logVersao] [bigint] NOT NULL,
	[logAcao] [smallint] NULL,
	[dataUltimaAlteracao] [datetime2](7) NULL,
	[inicioContratacaoEquipamentos] [date] NULL,
	[inicioImplantacao] [date] NULL,
	[inicioImplantacaoAgricola] [date] NULL,
	[inicioImplantacaoIndustrial] [date] NULL,
	[inicioOperacao] [date] NULL,
	[inicioOperacaoSucroenergetico] [date] NULL,
	[inicioProjeto] [date] NULL,
	[inicioProjetoExecutivo] [date] NULL,
	[inicioTratamentoTributarioPrevisto] [date] NULL,
	[inicioTratamentoTributarioRealizado] [date] NULL,
	[terminoImplantacaoAgricola] [date] NULL,
	[terminoImplantacaoIndustrial] [date] NULL,
	[terminoProjeto] [date] NULL,
	[terminoProjetoExecutivo] [date] NULL,
	[usuarioResponsavel_id] [bigint] NULL,
	[projeto_id] [bigint] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC,
	[logVersao] ASC
))
;

CREATE TABLE LogDepartamento(
	[id] [bigint] NOT NULL,
	[logVersao] [bigint] NOT NULL,
	[logAcao] [smallint] NULL,
	[descricao] [varchar](255) NULL,
	[sigla] [varchar](50) NULL,
	[departamentoSuperior_id] [bigint] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC,
	[logVersao] ASC
))
;

CREATE TABLE LogEmprego(
	[id] [bigint] NOT NULL,
	[logVersao] [bigint] NOT NULL,
	[logAcao] [smallint] NULL,
	[dataUltimaAlteracao] [datetime2](7) NULL,
	[ano] [int] NULL,
	[direto] [int] NULL,
	[diretoRealizado] [int] NULL,
	[indireto] [int] NULL,
	[tipo] [int] NULL,
	[usuarioResponsavel_id] [bigint] NULL,
	[projeto_id] [bigint] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC,
	[logVersao] ASC
))
;

CREATE TABLE LogEmpresa(
	[id] [bigint] NOT NULL,
	[logVersao] [bigint] NOT NULL,
	[logAcao] [smallint] NULL,
	[dataUltimaAlteracao] [datetime2](7) NULL,
	[cnpj] [varchar](14) NULL,
	[composicaoSocietaria] [varchar](1000) NULL,
	[empresaNovaMG] [bit] NULL,
	[historico] [varchar](1000) NULL,
	[inscricaoEstadual] [varchar](8) NULL,
	[justificativaArquivamento] [varchar](255) NULL,
	[nomePrincipal] [varchar](100) NULL,
	[origemEmpresa] [int] NULL,
	[razaoSocial] [varchar](200) NULL,
	[situacao] [int] NULL,
	[usuarioResponsavel_id] [bigint] NULL,
	[cadeiaProdutiva_id] [bigint] NULL,
	[endereco_id] [bigint] NULL,
	[estadoOrigem] [bigint] NULL,
	[anexo_id] [bigint] NULL,
	[naturezaJuridica_id] [bigint] NULL,
	[paisOrigem] [bigint] NULL,
	[usuarioExternoResponsavel_id] [bigint] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC,
	[logVersao] ASC
))
;

CREATE TABLE LogEndereco(
	[id] [bigint] NOT NULL,
	[logVersao] [bigint] NOT NULL,
	[logAcao] [smallint] NULL,
	[bairro] [varchar](255) NULL,
	[cep] [varchar](8) NULL,
	[cidade] [varchar](255) NULL,
	[complemento] [varchar](50) NULL,
	[logradouro] [varchar](250) NULL,
	[numero] [varchar](15) NULL,
	[municipio_id] [bigint] NULL,
	[pais_id] [bigint] NULL,
	[uf_id] [bigint] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC,
	[logVersao] ASC
))
;

CREATE TABLE LogEnergiaContratada(
	[id] [bigint] NOT NULL,
	[logVersao] [bigint] NOT NULL,
	[logAcao] [smallint] NULL,
	[contrato] [int] NULL,
	[localizacao] [varchar](255) NULL,
	[nome] [varchar](255) NULL,
	[infraestrutura_id] [bigint] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC,
	[logVersao] ASC
))
;

CREATE TABLE LogEstagioProjeto(
	[dataInicioEstagio] [datetime2](7) NOT NULL,
	[estagio_id] [bigint] NOT NULL,
	[projeto_id] [bigint] NOT NULL,
	[logVersao] [bigint] NOT NULL,
	[logAcao] [smallint] NULL,
	[ativo] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[dataInicioEstagio] ASC,
	[estagio_id] ASC,
	[projeto_id] ASC,
	[logVersao] ASC
))
;

CREATE TABLE LogEtapaMeioAmbiente(
	[id] [bigint] NOT NULL,
	[logVersao] [bigint] NOT NULL,
	[logAcao] [smallint] NULL,
	[tipo] [int] NULL,
	[meioAmbiente_id] [bigint] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC,
	[logVersao] ASC
))
;

CREATE TABLE LogFaturamentoAnterior(
	[id] [bigint] NOT NULL,
	[logVersao] [bigint] NOT NULL,
	[logAcao] [smallint] NULL,
	[ano] [int] NULL,
	[tipo] [int] NULL,
	[valor] [float] NULL,
	[financeiro_id] [bigint] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC,
	[logVersao] ASC
))
;

CREATE TABLE LogFaturamentoPrevisto(
	[id] [bigint] NOT NULL,
	[logVersao] [bigint] NOT NULL,
	[logAcao] [smallint] NULL,
	[ano] [int] NULL,
	[tipo] [int] NULL,
	[valor] [float] NULL,
	[valorRealizado] [float] NULL,
	[financeiro_id] [bigint] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC,
	[logVersao] ASC
))
;

CREATE TABLE LogFinanceiro(
	[id] [bigint] NOT NULL,
	[logVersao] [bigint] NOT NULL,
	[logAcao] [smallint] NULL,
	[dataUltimaAlteracao] [datetime2](7) NULL,
	[observacao] [varchar](255) NULL,
	[usuarioResponsavel_id] [bigint] NULL,
	[projeto_id] [bigint] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC,
	[logVersao] ASC
))
;

CREATE TABLE LogFinanceiroAnexo(
	[logVersao] [bigint] NOT NULL,
	[financeiro_id] [bigint] NOT NULL,
	[anexo_id] [bigint] NOT NULL,
	[logAcao] [smallint] NULL,
PRIMARY KEY CLUSTERED 
(
	[logVersao] ASC,
	[financeiro_id] ASC,
	[anexo_id] ASC
))
;

CREATE TABLE LogGrupoUsuario(
	[id] [bigint] NOT NULL,
	[logVersao] [bigint] NOT NULL,
	[logAcao] [smallint] NULL,
	[descricao] [varchar](60) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC,
	[logVersao] ASC
))
;

CREATE TABLE LogHistoricoSituacaoInstrumentoFormalizacao(
	[id] [bigint] NOT NULL,
	[logVersao] [bigint] NOT NULL,
	[logAcao] [smallint] NULL,
	[dataInicioSituacao] [datetime2](7) NULL,
	[justificativa] [varchar](255) NULL,
	[instrumentoFormalizacao_id] [bigint] NULL,
	[situacaoInstrumentoFormalizacao_id] [bigint] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC,
	[logVersao] ASC
))
;

CREATE TABLE LogHistoricoSituacaoProjeto(
	[id] [bigint] NOT NULL,
	[logVersao] [bigint] NOT NULL,
	[logAcao] [smallint] NULL,
	[data] [datetime2](7) NULL,
	[justificativa] [varchar](255) NULL,
	[projeto_id] [bigint] NULL,
	[situacaoProjeto_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC,
	[logVersao] ASC
))
;

CREATE TABLE LogInformacaoAdicional(
	[id] [bigint] NOT NULL,
	[logVersao] [bigint] NOT NULL,
	[logAcao] [smallint] NULL,
	[dataUltimaAlteracao] [datetime2](7) NULL,
	[assunto] [varchar](150) NULL,
	[data] [date] NULL,
	[descricao] [varchar](1500) NULL,
	[tipoContato] [int] NULL,
	[usuarioResponsavel_id] [bigint] NULL,
	[empresa_id] [bigint] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC,
	[logVersao] ASC
))
;

CREATE TABLE LogInfraestrutura(
	[id] [bigint] NOT NULL,
	[logVersao] [bigint] NOT NULL,
	[logAcao] [smallint] NULL,
	[dataUltimaAlteracao] [datetime2](7) NULL,
	[areaAlagada] [bit] NULL,
	[areaConstruida] [numeric](19, 2) NULL,
	[areaNecessaria] [numeric](19, 2) NULL,
	[consumoAguaEstimado] [numeric](19, 2) NULL,
	[consumoCombustivelEstimado] [numeric](19, 2) NULL,
	[consumoGasEstimado] [numeric](19, 2) NULL,
	[contatoCemig] [bit] NULL,
	[demandaEnergiaEstimada] [numeric](19, 2) NULL,
	[energiaJaContratada] [bit] NULL,
	[geracaoEnergiaPropria] [numeric](19, 2) NULL,
	[geracaoEnergiaVenda] [numeric](19, 2) NULL,
	[ofertaEnergiaVendaEstimada] [numeric](19, 2) NULL,
	[possuiTerreno] [bit] NULL,
	[potenciaEnergeticaEstimada] [numeric](19, 2) NULL,
	[usuarioResponsavel_id] [bigint] NULL,
	[projeto_id] [bigint] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC,
	[logVersao] ASC
))
;

CREATE TABLE LogInstrumentoFormalizacao(
	[id] [bigint] NOT NULL,
	[logVersao] [bigint] NOT NULL,
	[logAcao] [smallint] NULL,
	[dataUltimaAlteracao] [datetime2](7) NULL,
	[dataAssinatura] [date] NULL,
	[dataAssinaturaPrevista] [date] NULL,
	[localDocumento] [varchar](255) NULL,
	[possuiContrapartidaFincGoverno] [bit] NULL,
	[protocolo] [varchar](255) NULL,
	[situacaoAtual_id] [int] NULL,
	[tipo] [int] NULL,
	[titulo] [varchar](255) NULL,
	[ultimaVersao] [bit] NULL,
	[versao] [int] NULL,
	[usuarioResponsavel_id] [bigint] NULL,
	[historicoSituacaoInstrumentoFormalizacao_id] [bigint] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC,
	[logVersao] ASC
))
;

CREATE TABLE LogInsumo(
	[id] [bigint] NOT NULL,
	[logVersao] [bigint] NOT NULL,
	[logAcao] [smallint] NULL,
	[Nome] [varchar](255) NULL,
	[empresaFornecedor] [varchar](255) NULL,
	[origem] [int] NULL,
	[quantidadeAno] [int] NULL,
	[unidadeMedida] [varchar](255) NULL,
	[valorEstimadoAno] [float] NULL,
	[insumoProduto_id] [bigint] NULL,
	[ncm_id] [bigint] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC,
	[logVersao] ASC
))
;

CREATE TABLE LogInsumoProduto(
	[id] [bigint] NOT NULL,
	[logVersao] [bigint] NOT NULL,
	[logAcao] [smallint] NULL,
	[dataUltimaAlteracao] [datetime2](7) NULL,
	[aquisicaoOutroEstado] [bit] NULL,
	[contribuinteSubstituto] [bit] NULL,
	[importacaoProduto] [bit] NULL,
	[interesseParceria] [bit] NULL,
	[percentualAdquiridoOutro] [float] NULL,
	[percentualClienteComercial] [float] NULL,
	[percentualClienteConsumidor] [float] NULL,
	[percentualClienteIndustria] [float] NULL,
	[percentualFabricaMinas] [float] NULL,
	[percentualICMS] [float] NULL,
	[percentualImportado] [float] NULL,
	[percentualMercadoExterior] [float] NULL,
	[percentualMercadoMinas] [float] NULL,
	[percentualMercadoNacional] [float] NULL,
	[percentualOrigemImportado] [float] NULL,
	[percentualOrigemMinas] [float] NULL,
	[percentualOrigemOutros] [float] NULL,
	[percentualTributoEfetivo] [float] NULL,
	[percentualValorAgregado] [float] NULL,
	[usuarioResponsavel_id] [bigint] NULL,
	[projeto_id] [bigint] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC,
	[logVersao] ASC
))
;

CREATE TABLE LogInvestimentoPrevisto(
	[id] [bigint] NOT NULL,
	[logVersao] [bigint] NOT NULL,
	[logAcao] [smallint] NULL,
	[ano] [int] NULL,
	[tipo] [int] NULL,
	[valor] [float] NULL,
	[financeiro_id] [bigint] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC,
	[logVersao] ASC
))
;

CREATE TABLE LogLocal(
	[id] [bigint] NOT NULL,
	[logVersao] [bigint] NOT NULL,
	[logAcao] [smallint] NULL,
	[ativo] [bit] NULL,
	[descricao] [varchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC,
	[logVersao] ASC
))
;

CREATE TABLE LogLocalizacao(
	[id] [bigint] NOT NULL,
	[logVersao] [bigint] NOT NULL,
	[logAcao] [smallint] NULL,
	[dataUltimaAlteracao] [datetime2](7) NULL,
	[aDefinir] [bit] NULL,
	[latitude] [varchar](6) NULL,
	[localizacaoSecundaria] [varchar](255) NULL,
	[longetude] [varchar](6) NULL,
	[poligono] [varchar](255) NULL,
	[usuarioResponsavel_id] [bigint] NULL,
	[microRegiao_id] [bigint] NULL,
	[municipio_id] [bigint] NULL,
	[projeto_id] [bigint] NULL,
	[regiaoPlanejamento_id] [bigint] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC,
	[logVersao] ASC
))
;

CREATE TABLE LogMeioAmbiente(
	[id] [bigint] NOT NULL,
	[logVersao] [bigint] NOT NULL,
	[logAcao] [smallint] NULL,
	[dataUltimaAlteracao] [datetime2](7) NULL,
	[classeEmpreendimento] [int] NULL,
	[dataEntregaEstudos] [datetime2](7) NULL,
	[dataEntregaInformacao] [datetime2](7) NULL,
	[dataPedidoInformacao] [datetime2](7) NULL,
	[dataRealizadaVistoria] [datetime2](7) NULL,
	[licenciamentoIniciado] [bit] NULL,
	[necessidadeEIARIMA] [bit] NULL,
	[observacao] [varchar](max) NULL,
	[pedidoInformacoesComplementar] [bit] NULL,
	[realizadaVistoria] [bit] NULL,
	[usuarioResponsavel_id] [bigint] NULL,
	[projeto_id] [bigint] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC,
	[logVersao] ASC
))
; 

CREATE TABLE LogMeioAmbienteAnexo(
	[logVersao] [bigint] NOT NULL,
	[MeioAmbiente_id] [bigint] NOT NULL,
	[anexo_id] [bigint] NOT NULL,
	[logAcao] [smallint] NULL,
PRIMARY KEY CLUSTERED 
(
	[logVersao] ASC,
	[MeioAmbiente_id] ASC,
	[anexo_id] ASC
))
;

CREATE TABLE LogMeta(
	[id] [bigint] NOT NULL,
	[logVersao] [bigint] NOT NULL,
	[logAcao] [smallint] NULL,
	[ano] [int] NULL,
	[estagio_id] [int] NULL,
	[indicador_id] [int] NULL,
	[mes] [int] NULL,
	[meta] [float] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC,
	[logVersao] ASC
))
;

CREATE TABLE LogMicroRegiao(
	[id] [bigint] NOT NULL,
	[logVersao] [bigint] NOT NULL,
	[logAcao] [smallint] NULL,
	[nome] [varchar](255) NULL,
	[regiaoPlanejamento_id] [bigint] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC,
	[logVersao] ASC
))
;

CREATE TABLE LogMunicipio(
	[id] [bigint] NOT NULL,
	[logVersao] [bigint] NOT NULL,
	[logAcao] [smallint] NULL,
	[nome] [varchar](255) NULL,
	[microRegiao_id] [bigint] NULL,
	[regiaoPlanejamento_id] [bigint] NULL,
	[uf_id] [bigint] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC,
	[logVersao] ASC
))
;

CREATE TABLE LogNaturezaJuridica(
	[id] [bigint] NOT NULL,
	[logVersao] [bigint] NOT NULL,
	[logAcao] [smallint] NULL,
	[descricao] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC,
	[logVersao] ASC
))
;

CREATE TABLE LogNCM(
	[id] [bigint] NOT NULL,
	[logVersao] [bigint] NOT NULL,
	[logAcao] [smallint] NULL,
	[descricao] [varchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC,
	[logVersao] ASC
))
;

CREATE TABLE LogNomeEmpresa(
	[id] [bigint] NOT NULL,
	[logVersao] [bigint] NOT NULL,
	[logAcao] [smallint] NULL,
	[nome] [varchar](100) NULL,
	[empresa_id] [bigint] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC,
	[logVersao] ASC
))
;

CREATE TABLE LogOrigemRecurso(
	[id] [bigint] NOT NULL,
	[logVersao] [bigint] NOT NULL,
	[logAcao] [smallint] NULL,
	[origemNacional] [bit] NULL,
	[percentualInvestimento] [float] NULL,
	[valorInvestimento] [float] NULL,
	[financeiro_id] [bigint] NULL,
	[pais_id] [bigint] NULL,
	[uf_id] [bigint] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC,
	[logVersao] ASC
))
;

CREATE TABLE LogPais(
	[id] [bigint] NOT NULL,
	[logVersao] [bigint] NOT NULL,
	[logAcao] [smallint] NULL,
	[nome] [varchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC,
	[logVersao] ASC
))
;

CREATE TABLE LogParceria(
	[id] [bigint] NOT NULL,
	[logVersao] [bigint] NOT NULL,
	[logAcao] [smallint] NULL,
	[descricao] [varchar](255) NULL,
	[insumoProduto_id] [bigint] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC,
	[logVersao] ASC
))
;

CREATE TABLE LogPerguntaPesquisa(
	[id] [bigint] NOT NULL,
	[logVersao] [bigint] NOT NULL,
	[logAcao] [smallint] NULL,
	[pergunta] [varchar](255) NULL,
	[tipo] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC,
	[logVersao] ASC
))
;

CREATE TABLE LogPesquisa(
	[id] [bigint] NOT NULL,
	[logVersao] [bigint] NOT NULL,
	[logAcao] [smallint] NULL,
	[comentario] [varchar](255) NULL,
	[contatoExternoResponsavel_id] [bigint] NULL,
	[dataResposta] [datetime2](7) NULL,
	[dataSolicitacao] [datetime2](7) NULL,
	[dataUltimaAlteracao] [datetime2](7) NULL,
	[indiceSatisfacao] [float] NULL,
	[instrumentoFormalizacao_id] [bigint] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC,
	[logVersao] ASC
))
;

CREATE TABLE LogPesquisaPerguntaPesquisa(
	[perguntaPesquisa_id] [bigint] NOT NULL,
	[pesquisa_id] [bigint] NOT NULL,
	[logVersao] [bigint] NOT NULL,
	[logAcao] [smallint] NULL,
	[justificativa] [varchar](255) NULL,
	[resposta] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[perguntaPesquisa_id] ASC,
	[pesquisa_id] ASC,
	[logVersao] ASC
))
;

CREATE TABLE LogPleito(
	[id] [bigint] NOT NULL,
	[logVersao] [bigint] NOT NULL,
	[logAcao] [smallint] NULL,
	[dataUltimaAlteracao] [datetime2](7) NULL,
	[observacoes] [varchar](255) NULL,
	[pleitos] [varchar](255) NULL,
	[usuarioResponsavel_id] [bigint] NULL,
	[projeto_id] [bigint] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC,
	[logVersao] ASC
))
;

CREATE TABLE LogProducao(
	[id] [bigint] NOT NULL,
	[logVersao] [bigint] NOT NULL,
	[logAcao] [smallint] NULL,
	[ano] [int] NULL,
	[areaPlantada] [float] NULL,
	[geracaoEnergia] [float] NULL,
	[moagemCana] [float] NULL,
	[outro] [float] NULL,
	[producaoAcucar] [float] NULL,
	[producaoEtanol] [float] NULL,
	[insumoProduto_id] [bigint] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC,
	[logVersao] ASC
))
;

CREATE TABLE LogProduto(
	[tipo] [int] NOT NULL,
	[id] [bigint] NOT NULL,
	[logVersao] [bigint] NOT NULL,
	[logAcao] [smallint] NULL,
	[nome] [varchar](255) NULL,
	[quantidade1] [int] NULL,
	[quantidade2] [int] NULL,
	[quantidade3] [int] NULL,
	[sujeitoST] [bit] NULL,
	[unidadeMedida] [varchar](255) NULL,
	[insumoProduto_id] [bigint] NULL,
	[ncm_id] [bigint] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC,
	[logVersao] ASC
))
;

CREATE TABLE LogProjeto(
	[id] [bigint] NOT NULL,
	[logVersao] [bigint] NOT NULL,
	[logAcao] [smallint] NULL,
	[dataUltimaAlteracao] [datetime2](7) NULL,
	[dataCadastro] [datetime2](7) NULL,
	[descricao] [varchar](255) NULL,
	[estagioAtual_id] [int] NULL,
	[informacaoPrimeiroContato] [varchar](255) NULL,
	[nome] [varchar](255) NULL,
	[prospeccaoAtiva] [bit] NULL,
	[situacaoAtual_id] [int] NULL,
	[tipo] [int] NULL,
	[ultimaVersao] [bit] NULL,
	[versao] [int] NULL,
	[familia_id] [bigint] NULL,
	[usuarioResponsavel_id] [bigint] NULL,
	[cadeiaProdutiva_id] [bigint] NULL,
	[empresa_id] [bigint] NULL,
	[historicosituacaoprojeto_id] [bigint] NULL,
	[ultimaAtividade_id] [bigint] NULL,
	[unidade_id] [bigint] NULL,
	[usuarioInvestimento_id] [bigint] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC,
	[logVersao] ASC
))
;

CREATE TABLE LogPROJETOINSTRUMENTOFORMALIZACAO(
	[logVersao] [bigint] NOT NULL,
	[instrumentoFormalizacao_id] [bigint] NOT NULL,
	[projeto_id] [bigint] NOT NULL,
	[logAcao] [smallint] NULL,
PRIMARY KEY CLUSTERED 
(
	[logVersao] ASC,
	[instrumentoFormalizacao_id] ASC,
	[projeto_id] ASC
))
;

CREATE TABLE LogPublicacao(
	[id] [bigint] NOT NULL,
	[logVersao] [bigint] NOT NULL,
	[logAcao] [smallint] NULL,
	[dataUltimaAlteracao] [datetime2](7) NULL,
	[dataPublicacao] [date] NULL,
	[usuarioResponsavel_id] [bigint] NULL,
	[instrumentoFormalizacao_id] [bigint] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC,
	[logVersao] ASC
))
;

CREATE TABLE LogPublicacaoAnexo(
	[logVersao] [bigint] NOT NULL,
	[publicacao_id] [bigint] NOT NULL,
	[anexo_id] [bigint] NOT NULL,
	[logAcao] [smallint] NULL,
PRIMARY KEY CLUSTERED 
(
	[logVersao] ASC,
	[publicacao_id] ASC,
	[anexo_id] ASC
))
;

CREATE TABLE LogRegiaoPlanejamento(
	[id] [bigint] NOT NULL,
	[logVersao] [bigint] NOT NULL,
	[logAcao] [smallint] NULL,
	[nome] [varchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC,
	[logVersao] ASC
))
;

CREATE TABLE LogRelatorioAcompanhamento(
	[id] [bigint] NOT NULL,
	[logVersao] [bigint] NOT NULL,
	[logAcao] [smallint] NULL,
	[dataUltimaAlteracao] [datetime2](7) NULL,
	[aceiteTermo] [bit] NULL,
	[ano] [int] NULL,
	[dataFimPrimeiraCampanha] [datetime2](7) NULL,
	[dataFimSegundaCampanha] [datetime2](7) NULL,
	[dataInicioPrimeiraCampanha] [datetime2](7) NULL,
	[dataInicioSegundaCampanha] [datetime2](7) NULL,
	[dataValidacao] [datetime2](7) NULL,
	[emailResponsavelRelatorio] [varchar](50) NULL,
	[nomeResponsavelRelatorio] [varchar](100) NULL,
	[observacao] [varchar](255) NULL,
	[situacaoAtual_id] [int] NULL,
	[telefoneResponsavelRelatorio] [varchar](18) NULL,
	[termoConfiabilidade] [varchar](255) NULL,
	[usuarioResponsavel_id] [bigint] NULL,
	[projeto_id] [bigint] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC,
	[logVersao] ASC
))
;

CREATE TABLE LogRelatorioAcompanhamentoSituacao(
	[relatorioAcompanhamento_id] [bigint] NOT NULL,
	[situacaoRelatorio_id] [bigint] NOT NULL,
	[logVersao] [bigint] NOT NULL,
	[logAcao] [smallint] NULL,
PRIMARY KEY CLUSTERED 
(
	[relatorioAcompanhamento_id] ASC,
	[situacaoRelatorio_id] ASC,
	[logVersao] ASC
))
;

CREATE TABLE LogRevisao(
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[logDataUltimaAlteracao] [datetime2](7) NOT NULL,
	[logUsuarioResponsavel] [varchar](100) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
))
;

CREATE TABLE LogServico(
	[id] [bigint] NOT NULL,
	[logVersao] [bigint] NOT NULL,
	[logAcao] [smallint] NULL,
	[descricao] [varchar](255) NULL,
	[insumoProduto_id] [bigint] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC,
	[logVersao] ASC
))
;

CREATE TABLE LogSignatario(
	[id] [bigint] NOT NULL,
	[logVersao] [bigint] NOT NULL,
	[logAcao] [smallint] NULL,
	[cargo] [varchar](255) NULL,
	[email] [varchar](255) NULL,
	[nome] [varchar](255) NULL,
	[telefone1] [varchar](255) NULL,
	[telefone2] [varchar](255) NULL,
	[instrumentoFormalizacao_id] [bigint] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC,
	[logVersao] ASC
))
;

CREATE TABLE LogSituacaoInstrumentoFormalizacao(
	[id] [bigint] NOT NULL,
	[logVersao] [bigint] NOT NULL,
	[logAcao] [smallint] NULL,
	[descricao] [varchar](100) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC,
	[logVersao] ASC
))
;

CREATE TABLE LogTemplate(
	[id] [bigint] NOT NULL,
	[logVersao] [bigint] NOT NULL,
	[logAcao] [smallint] NULL,
	[dataUltimaAlteracao] [datetime2](7) NULL,
	[nome] [varchar](100) NULL,
	[status] [bit] NULL,
	[tipo] [int] NULL,
	[usuarioResponsavel_id] [bigint] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC,
	[logVersao] ASC
))
;

CREATE TABLE LogUF(
	[id] [bigint] NOT NULL,
	[logVersao] [bigint] NOT NULL,
	[logAcao] [smallint] NULL,
	[nome] [varchar](255) NULL,
	[sigla] [varchar](255) NULL,
	[pais_id] [bigint] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC,
	[logVersao] ASC
))
;

CREATE TABLE LogUnidadeEmpresa(
	[id] [bigint] NOT NULL,
	[logVersao] [bigint] NOT NULL,
	[logAcao] [smallint] NULL,
	[dataUltimaAlteracao] [datetime2](7) NULL,
	[email] [varchar](50) NULL,
	[nome] [varchar](150) NULL,
	[telefone] [varchar](18) NULL,
	[usuarioResponsavel_id] [bigint] NULL,
	[empresa_id] [bigint] NULL,
	[endereco_id] [bigint] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC,
	[logVersao] ASC
))
;

CREATE TABLE LogUsoFonte(
	[id] [bigint] NOT NULL,
	[logVersao] [bigint] NOT NULL,
	[logAcao] [smallint] NULL,
	[tipo] [int] NULL,
	[valorARealizar] [float] NULL,
	[valorRealizado] [float] NULL,
	[financeiro_id] [bigint] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC,
	[logVersao] ASC
))
;

CREATE TABLE LogUsuario(
	[tipo] [int] NOT NULL,
	[id] [bigint] NOT NULL,
	[logVersao] [bigint] NOT NULL,
	[logAcao] [smallint] NULL,
	[dataUltimaAlteracao] [datetime2](7) NULL,
	[ativo] [bit] NULL,
	[email] [varchar](50) NULL,
	[mailing] [bit] NULL,
	[nome] [varchar](100) NULL,
	[senha] [varchar](128) NULL,
	[telefone] [varchar](18) NULL,
	[telefone2] [varchar](18) NULL,
	[usuarioResponsavel_id] [bigint] NULL,
	[cargo_id] [bigint] NULL,
	[grupousuario_id] [bigint] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC,
	[logVersao] ASC
))
;

CREATE TABLE LogUsuarioExterno(
	[id] [bigint] NOT NULL,
	[logVersao] [bigint] NOT NULL,
	[cargoExterno] [varchar](255) NULL,
	[dataBloqueioTemporario] [datetime2](7) NULL,
	[empresa_id] [bigint] NULL,
	[unidade_id] [bigint] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC,
	[logVersao] ASC
))
;

CREATE TABLE LogUsuarioInterno(
	[id] [bigint] NOT NULL,
	[logVersao] [bigint] NOT NULL,
	[matricula] [varchar](15) NULL,
	[departamento_id] [bigint] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC,
	[logVersao] ASC
))
;

CREATE TABLE LogUsuarioProjetoRecente(
	[projeto_id] [bigint] NOT NULL,
	[usuario_id] [bigint] NOT NULL,
	[logVersao] [bigint] NOT NULL,
	[logAcao] [smallint] NULL,
	[dataAcesso] [datetime2](7) NULL,
PRIMARY KEY CLUSTERED 
(
	[projeto_id] ASC,
	[usuario_id] ASC,
	[logVersao] ASC
))
;

CREATE TABLE LogVerificationToken(
	[id] [bigint] NOT NULL,
	[logVersao] [bigint] NOT NULL,
	[logAcao] [smallint] NULL,
	[expiryDate] [datetime2](7) NULL,
	[token] [varchar](36) NULL,
	[tokenType] [varchar](255) NULL,
	[verified] [bit] NULL,
	[user_id] [bigint] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC,
	[logVersao] ASC
))
;

ALTER TABLE LogAccessToken WITH CHECK ADD  CONSTRAINT [FK_c801nrf4efbg8gcitb51smn4o] FOREIGN KEY([logVersao])
REFERENCES LogRevisao ([id])
;

ALTER TABLE LogAccessToken CHECK CONSTRAINT [FK_c801nrf4efbg8gcitb51smn4o]
;

ALTER TABLE LogAnexo WITH CHECK ADD  CONSTRAINT [FK_p7hbejn8svbunj7h9n801tsx2] FOREIGN KEY([logVersao])
REFERENCES LogRevisao ([id])
;

ALTER TABLE LogAnexo CHECK CONSTRAINT [FK_p7hbejn8svbunj7h9n801tsx2]
;

ALTER TABLE LogAnexoInformacaoAdicional WITH CHECK ADD  CONSTRAINT [FK_e8imd8s2ewe9kpxm2j2e1129l] FOREIGN KEY([logVersao])
REFERENCES LogRevisao ([id])
;

ALTER TABLE LogAnexoInformacaoAdicional CHECK CONSTRAINT [FK_e8imd8s2ewe9kpxm2j2e1129l]
;

ALTER TABLE LogAnexoLocalizacao WITH CHECK ADD  CONSTRAINT [FK_neprwcj56ufj8y6e1n4gsps4l] FOREIGN KEY([logVersao])
REFERENCES LogRevisao ([id])
;

ALTER TABLE LogAnexoLocalizacao CHECK CONSTRAINT [FK_neprwcj56ufj8y6e1n4gsps4l]
;

ALTER TABLE LogAnexoPesquisa WITH CHECK ADD  CONSTRAINT [FK_khd45lvq779aa0mo161pvsc8m] FOREIGN KEY([logVersao])
REFERENCES LogRevisao ([id])
;

ALTER TABLE LogAnexoPesquisa CHECK CONSTRAINT [FK_khd45lvq779aa0mo161pvsc8m]
;

ALTER TABLE LogAnexoProjeto WITH CHECK ADD  CONSTRAINT [FK_8rjqs5npvxfqm4r6alywvnsf5] FOREIGN KEY([logVersao])
REFERENCES LogRevisao ([id])
;

ALTER TABLE LogAnexoProjeto CHECK CONSTRAINT [FK_8rjqs5npvxfqm4r6alywvnsf5]
;

ALTER TABLE LogAnexoRelatorioAcompanhamento WITH CHECK ADD  CONSTRAINT [FK_et3dvjiqvpki69ba89o45gk8a] FOREIGN KEY([logVersao])
REFERENCES LogRevisao ([id])
;

ALTER TABLE LogAnexoRelatorioAcompanhamento CHECK CONSTRAINT [FK_et3dvjiqvpki69ba89o45gk8a]
;

ALTER TABLE LogAtividadeInstrumento WITH CHECK ADD  CONSTRAINT [FK_djxlot76187w8a7qfeoui5i3m] FOREIGN KEY([logVersao])
REFERENCES LogRevisao ([id])
;

ALTER TABLE LogAtividadeInstrumento CHECK CONSTRAINT [FK_djxlot76187w8a7qfeoui5i3m]
;

ALTER TABLE LogAtividadeLocal WITH CHECK ADD  CONSTRAINT [FK_2x5ln0q8sxblte9ac24cv8frg] FOREIGN KEY([logVersao])
REFERENCES LogRevisao ([id])
;

ALTER TABLE LogAtividadeLocal CHECK CONSTRAINT [FK_2x5ln0q8sxblte9ac24cv8frg]
;

ALTER TABLE LogAtividadeProjeto WITH CHECK ADD  CONSTRAINT [FK_k3n209s45oik6qvogkrwtp37p] FOREIGN KEY([logVersao])
REFERENCES LogRevisao ([id])
;

ALTER TABLE LogAtividadeProjeto CHECK CONSTRAINT [FK_k3n209s45oik6qvogkrwtp37p]
;

ALTER TABLE LogAtividadeTemplate WITH CHECK ADD  CONSTRAINT [FK_4t9mvnc64rhtlvpnw8vwhqjxw] FOREIGN KEY([logVersao])
REFERENCES LogRevisao ([id])
;

ALTER TABLE LogAtividadeTemplate CHECK CONSTRAINT [FK_4t9mvnc64rhtlvpnw8vwhqjxw]
;

ALTER TABLE LogCadeiaProdutiva WITH CHECK ADD  CONSTRAINT [FK_183lc3asxyjvlvp6f4ep9xa1b] FOREIGN KEY([logVersao])
REFERENCES LogRevisao ([id])
;

ALTER TABLE LogCadeiaProdutiva CHECK CONSTRAINT [FK_183lc3asxyjvlvp6f4ep9xa1b]
;

ALTER TABLE LogCargo WITH CHECK ADD  CONSTRAINT [FK_rhb7g4javccs9tyjw6sh24mht] FOREIGN KEY([logVersao])
REFERENCES LogRevisao ([id])
;

ALTER TABLE LogCargo CHECK CONSTRAINT [FK_rhb7g4javccs9tyjw6sh24mht]
;

ALTER TABLE LogCNAE WITH CHECK ADD  CONSTRAINT [FK_q4adhmh8vam0w7xyves09ilv5] FOREIGN KEY([logVersao])
REFERENCES LogRevisao ([id])
;

ALTER TABLE LogCNAE CHECK CONSTRAINT [FK_q4adhmh8vam0w7xyves09ilv5]
;

ALTER TABLE LogCNAEEmpresa WITH CHECK ADD  CONSTRAINT [FK_lt3qnu7qt14h3bwycb25q21vf] FOREIGN KEY([logVersao])
REFERENCES LogRevisao ([id])
;

ALTER TABLE LogCNAEEmpresa CHECK CONSTRAINT [FK_lt3qnu7qt14h3bwycb25q21vf]
;

ALTER TABLE LogComposicaoSocietaria WITH CHECK ADD  CONSTRAINT [FK_49gg2jbxkf892p2jgo29m3g16] FOREIGN KEY([logVersao])
REFERENCES LogRevisao ([id])
;

ALTER TABLE LogComposicaoSocietaria CHECK CONSTRAINT [FK_49gg2jbxkf892p2jgo29m3g16]
;

ALTER TABLE LogConcorrente WITH CHECK ADD  CONSTRAINT [FK_p93ajw2wxrx8n87etjeowf9jo] FOREIGN KEY([logVersao])
REFERENCES LogRevisao ([id])
;

ALTER TABLE LogConcorrente CHECK CONSTRAINT [FK_p93ajw2wxrx8n87etjeowf9jo]
;

ALTER TABLE LogCronograma WITH CHECK ADD  CONSTRAINT [FK_hpffcg78twlhht6532gnb21ns] FOREIGN KEY([logVersao])
REFERENCES LogRevisao ([id])
;

ALTER TABLE LogCronograma CHECK CONSTRAINT [FK_hpffcg78twlhht6532gnb21ns]
;

ALTER TABLE LogDepartamento WITH CHECK ADD  CONSTRAINT [FK_i01ltjjk0ud7817rwx06xhah] FOREIGN KEY([logVersao])
REFERENCES LogRevisao ([id])
;

ALTER TABLE LogDepartamento CHECK CONSTRAINT [FK_i01ltjjk0ud7817rwx06xhah]
;

ALTER TABLE LogEmprego WITH CHECK ADD  CONSTRAINT [FK_svfrudtrn0kwwlp6os3f0atad] FOREIGN KEY([logVersao])
REFERENCES LogRevisao ([id])
;

ALTER TABLE LogEmprego CHECK CONSTRAINT [FK_svfrudtrn0kwwlp6os3f0atad]
;

ALTER TABLE LogEmpresa WITH CHECK ADD  CONSTRAINT [FK_pdtpktq05husp5krnadel5jd1] FOREIGN KEY([logVersao])
REFERENCES LogRevisao ([id])
;

ALTER TABLE LogEmpresa CHECK CONSTRAINT [FK_pdtpktq05husp5krnadel5jd1]
;

ALTER TABLE LogEndereco WITH CHECK ADD  CONSTRAINT [FK_gt3otgosy50il7c0v9npm1om1] FOREIGN KEY([logVersao])
REFERENCES LogRevisao ([id])
;

ALTER TABLE LogEndereco CHECK CONSTRAINT [FK_gt3otgosy50il7c0v9npm1om1]
;

ALTER TABLE LogEnergiaContratada WITH CHECK ADD  CONSTRAINT [FK_d5jqf8686fr0eo13lhchy5vls] FOREIGN KEY([logVersao])
REFERENCES LogRevisao ([id])
;

ALTER TABLE LogEnergiaContratada CHECK CONSTRAINT [FK_d5jqf8686fr0eo13lhchy5vls]
;

ALTER TABLE LogEstagioProjeto WITH CHECK ADD  CONSTRAINT [FK_lecvwcsaivk3xomro6fhru6u5] FOREIGN KEY([logVersao])
REFERENCES LogRevisao ([id])
;

ALTER TABLE LogEstagioProjeto CHECK CONSTRAINT [FK_lecvwcsaivk3xomro6fhru6u5]
;

ALTER TABLE LogEtapaMeioAmbiente WITH CHECK ADD  CONSTRAINT [FK_o44kaofkd45j8p5gr1mqa39xb] FOREIGN KEY([logVersao])
REFERENCES LogRevisao ([id])
;

ALTER TABLE LogEtapaMeioAmbiente CHECK CONSTRAINT [FK_o44kaofkd45j8p5gr1mqa39xb]
;

ALTER TABLE LogFaturamentoAnterior WITH CHECK ADD  CONSTRAINT [FK_k1cqn0pjeapls0hbuxht6l400] FOREIGN KEY([logVersao])
REFERENCES LogRevisao ([id])
;

ALTER TABLE LogFaturamentoAnterior CHECK CONSTRAINT [FK_k1cqn0pjeapls0hbuxht6l400]
;

ALTER TABLE LogFaturamentoPrevisto WITH CHECK ADD  CONSTRAINT [FK_syhq56p24xqyhma6egpmhk42r] FOREIGN KEY([logVersao])
REFERENCES LogRevisao ([id])
;

ALTER TABLE LogFaturamentoPrevisto CHECK CONSTRAINT [FK_syhq56p24xqyhma6egpmhk42r]
;

ALTER TABLE LogFinanceiro WITH CHECK ADD  CONSTRAINT [FK_ihjqsj8bdyy08kigq0qco9vhc] FOREIGN KEY([logVersao])
REFERENCES LogRevisao ([id])
;

ALTER TABLE LogFinanceiro CHECK CONSTRAINT [FK_ihjqsj8bdyy08kigq0qco9vhc]
;

ALTER TABLE LogFinanceiroAnexo WITH CHECK ADD  CONSTRAINT [FK_rg300nvqe7lb5q3iwx2k90xnm] FOREIGN KEY([logVersao])
REFERENCES LogRevisao ([id])
;

ALTER TABLE LogFinanceiroAnexo CHECK CONSTRAINT [FK_rg300nvqe7lb5q3iwx2k90xnm]
;

ALTER TABLE LogGrupoUsuario WITH CHECK ADD  CONSTRAINT [FK_9d6uosabrtmap6lmnxtjbxbdt] FOREIGN KEY([logVersao])
REFERENCES LogRevisao ([id])
;

ALTER TABLE LogGrupoUsuario CHECK CONSTRAINT [FK_9d6uosabrtmap6lmnxtjbxbdt]
;

ALTER TABLE LogHistoricoSituacaoInstrumentoFormalizacao WITH CHECK ADD  CONSTRAINT [FK_ijj9a84gya3j89d5ijqfvoapw] FOREIGN KEY([logVersao])
REFERENCES LogRevisao ([id])
;

ALTER TABLE LogHistoricoSituacaoInstrumentoFormalizacao CHECK CONSTRAINT [FK_ijj9a84gya3j89d5ijqfvoapw]
;

ALTER TABLE LogHistoricoSituacaoProjeto WITH CHECK ADD  CONSTRAINT [FK_d3im8u70q7mxpehjejddc92dk] FOREIGN KEY([logVersao])
REFERENCES LogRevisao ([id])
;

ALTER TABLE LogHistoricoSituacaoProjeto CHECK CONSTRAINT [FK_d3im8u70q7mxpehjejddc92dk]
;

ALTER TABLE LogInformacaoAdicional WITH CHECK ADD  CONSTRAINT [FK_qw6g7uy5e6u9er7ii3vccpb8l] FOREIGN KEY([logVersao])
REFERENCES LogRevisao ([id])
;

ALTER TABLE LogInformacaoAdicional CHECK CONSTRAINT [FK_qw6g7uy5e6u9er7ii3vccpb8l]
;

ALTER TABLE LogInfraestrutura WITH CHECK ADD  CONSTRAINT [FK_i7dr9ograw4bkuw2t1uxxdtd1] FOREIGN KEY([logVersao])
REFERENCES LogRevisao ([id])
;

ALTER TABLE LogInfraestrutura CHECK CONSTRAINT [FK_i7dr9ograw4bkuw2t1uxxdtd1]
;

ALTER TABLE LogInstrumentoFormalizacao WITH CHECK ADD  CONSTRAINT [FK_3kdf5b24eewlxgrgl4fr9ifm] FOREIGN KEY([logVersao])
REFERENCES LogRevisao ([id])
;

ALTER TABLE LogInstrumentoFormalizacao CHECK CONSTRAINT [FK_3kdf5b24eewlxgrgl4fr9ifm]
;

ALTER TABLE LogInsumo WITH CHECK ADD  CONSTRAINT [FK_mngqik62qg79pic2x69yaiej1] FOREIGN KEY([logVersao])
REFERENCES LogRevisao ([id])
;

ALTER TABLE LogInsumo CHECK CONSTRAINT [FK_mngqik62qg79pic2x69yaiej1]
;

ALTER TABLE LogInsumoProduto WITH CHECK ADD  CONSTRAINT [FK_qvbc4pcjvp05u7094v22nqerx] FOREIGN KEY([logVersao])
REFERENCES LogRevisao ([id])
;

ALTER TABLE LogInsumoProduto CHECK CONSTRAINT [FK_qvbc4pcjvp05u7094v22nqerx]
;

ALTER TABLE LogInvestimentoPrevisto WITH CHECK ADD  CONSTRAINT [FK_pre1sifis5srbrc652kctag0x] FOREIGN KEY([logVersao])
REFERENCES LogRevisao ([id])
;

ALTER TABLE LogInvestimentoPrevisto CHECK CONSTRAINT [FK_pre1sifis5srbrc652kctag0x]
;

ALTER TABLE LogLocal WITH CHECK ADD  CONSTRAINT [FK_hla1nw3ecl8h7pruihbkxjx76] FOREIGN KEY([logVersao])
REFERENCES LogRevisao ([id])
;

ALTER TABLE LogLocal CHECK CONSTRAINT [FK_hla1nw3ecl8h7pruihbkxjx76]
;

ALTER TABLE LogLocalizacao WITH CHECK ADD  CONSTRAINT [FK_6bq6bt95553d5k6qe2pfw4xr] FOREIGN KEY([logVersao])
REFERENCES LogRevisao ([id])
;

ALTER TABLE LogLocalizacao CHECK CONSTRAINT [FK_6bq6bt95553d5k6qe2pfw4xr]
;

ALTER TABLE LogMeioAmbiente WITH CHECK ADD  CONSTRAINT [FK_n5iskqa9d9h9tijxj1jifo51p] FOREIGN KEY([logVersao])
REFERENCES LogRevisao ([id])
;

ALTER TABLE LogMeioAmbiente CHECK CONSTRAINT [FK_n5iskqa9d9h9tijxj1jifo51p]
;

ALTER TABLE LogMeioAmbienteAnexo WITH CHECK ADD  CONSTRAINT [FK_7dypfufarw3yo8q64rl5xdadg] FOREIGN KEY([logVersao])
REFERENCES LogRevisao ([id])
;

ALTER TABLE LogMeioAmbienteAnexo CHECK CONSTRAINT [FK_7dypfufarw3yo8q64rl5xdadg]
;

ALTER TABLE LogMeta WITH CHECK ADD  CONSTRAINT [FK_1iwpk1ot64tmbvaxc5j5muynt] FOREIGN KEY([logVersao])
REFERENCES LogRevisao ([id])
;

ALTER TABLE LogMeta CHECK CONSTRAINT [FK_1iwpk1ot64tmbvaxc5j5muynt]
;

ALTER TABLE LogMicroRegiao WITH CHECK ADD  CONSTRAINT [FK_qt1u4vxa8peaj6nylrhe5x91i] FOREIGN KEY([logVersao])
REFERENCES LogRevisao ([id])
;

ALTER TABLE LogMicroRegiao CHECK CONSTRAINT [FK_qt1u4vxa8peaj6nylrhe5x91i]
;

ALTER TABLE LogMunicipio WITH CHECK ADD  CONSTRAINT [FK_hckkmta64be3nog6xp32m4uku] FOREIGN KEY([logVersao])
REFERENCES LogRevisao ([id])
;

ALTER TABLE LogMunicipio CHECK CONSTRAINT [FK_hckkmta64be3nog6xp32m4uku]
;

ALTER TABLE LogNaturezaJuridica WITH CHECK ADD  CONSTRAINT [FK_3qbmg8vhsldpxbj5ukt4gylc4] FOREIGN KEY([logVersao])
REFERENCES LogRevisao ([id])
;

ALTER TABLE LogNaturezaJuridica CHECK CONSTRAINT [FK_3qbmg8vhsldpxbj5ukt4gylc4]
;

ALTER TABLE LogNCM WITH CHECK ADD  CONSTRAINT [FK_rfwfxxl4ba4k6ra3nb5cy5q8x] FOREIGN KEY([logVersao])
REFERENCES LogRevisao ([id])
;

ALTER TABLE LogNCM CHECK CONSTRAINT [FK_rfwfxxl4ba4k6ra3nb5cy5q8x]
;

ALTER TABLE LogNomeEmpresa WITH CHECK ADD  CONSTRAINT [FK_aygjxofqgjwm2pr6fvh3xmv65] FOREIGN KEY([logVersao])
REFERENCES LogRevisao ([id])
;

ALTER TABLE LogNomeEmpresa CHECK CONSTRAINT [FK_aygjxofqgjwm2pr6fvh3xmv65]
;

ALTER TABLE LogOrigemRecurso WITH CHECK ADD  CONSTRAINT [FK_2qanyui9yocm4hd01d99lvne6] FOREIGN KEY([logVersao])
REFERENCES LogRevisao ([id])
;

ALTER TABLE LogOrigemRecurso CHECK CONSTRAINT [FK_2qanyui9yocm4hd01d99lvne6]
;

ALTER TABLE LogPais WITH CHECK ADD  CONSTRAINT [FK_41n5ui1o9799c5qgpf3qs5xn4] FOREIGN KEY([logVersao])
REFERENCES LogRevisao ([id])
;

ALTER TABLE LogPais CHECK CONSTRAINT [FK_41n5ui1o9799c5qgpf3qs5xn4]
;

ALTER TABLE LogParceria WITH CHECK ADD  CONSTRAINT [FK_ao74t6oa14rsgax4w52mwi5s2] FOREIGN KEY([logVersao])
REFERENCES LogRevisao ([id])
;

ALTER TABLE LogParceria CHECK CONSTRAINT [FK_ao74t6oa14rsgax4w52mwi5s2]
;

ALTER TABLE LogPerguntaPesquisa WITH CHECK ADD  CONSTRAINT [FK_j5vja7nxxtjkxsmkb5x1kbcec] FOREIGN KEY([logVersao])
REFERENCES LogRevisao ([id])
;

ALTER TABLE LogPerguntaPesquisa CHECK CONSTRAINT [FK_j5vja7nxxtjkxsmkb5x1kbcec]
;

ALTER TABLE LogPesquisa WITH CHECK ADD  CONSTRAINT [FK_54hsfbgh9w2j880ab0h8sg5ke] FOREIGN KEY([logVersao])
REFERENCES LogRevisao ([id])
;

ALTER TABLE LogPesquisa CHECK CONSTRAINT [FK_54hsfbgh9w2j880ab0h8sg5ke]
;

ALTER TABLE LogPesquisaPerguntaPesquisa WITH CHECK ADD  CONSTRAINT [FK_mwgprwr9waj9m4to5egu1becm] FOREIGN KEY([logVersao])
REFERENCES LogRevisao ([id])
;

ALTER TABLE LogPesquisaPerguntaPesquisa CHECK CONSTRAINT [FK_mwgprwr9waj9m4to5egu1becm]
;

ALTER TABLE LogPleito WITH CHECK ADD  CONSTRAINT [FK_gqvw4yp3mt0c95mv8qo78gye2] FOREIGN KEY([logVersao])
REFERENCES LogRevisao ([id])
;

ALTER TABLE LogPleito CHECK CONSTRAINT [FK_gqvw4yp3mt0c95mv8qo78gye2]
;

ALTER TABLE LogProducao WITH CHECK ADD  CONSTRAINT [FK_ngavhprsku73wun4uu956wh6v] FOREIGN KEY([logVersao])
REFERENCES LogRevisao ([id])
;

ALTER TABLE LogProducao CHECK CONSTRAINT [FK_ngavhprsku73wun4uu956wh6v]
;

ALTER TABLE LogProduto WITH CHECK ADD  CONSTRAINT [FK_ed88t9yfypdl5m2l8n9uih43x] FOREIGN KEY([logVersao])
REFERENCES LogRevisao ([id])
;

ALTER TABLE LogProduto CHECK CONSTRAINT [FK_ed88t9yfypdl5m2l8n9uih43x]
;

ALTER TABLE LogProjeto WITH CHECK ADD  CONSTRAINT [FK_bd8xa0pithg2f61gysv8j37p4] FOREIGN KEY([logVersao])
REFERENCES LogRevisao ([id])
;

ALTER TABLE LogProjeto CHECK CONSTRAINT [FK_bd8xa0pithg2f61gysv8j37p4]
;

ALTER TABLE LogPROJETOINSTRUMENTOFORMALIZACAO WITH CHECK ADD  CONSTRAINT [FK_32fdx63kql7qbusah8oxjcr8c] FOREIGN KEY([logVersao])
REFERENCES LogRevisao ([id])
;

ALTER TABLE LogPROJETOINSTRUMENTOFORMALIZACAO CHECK CONSTRAINT [FK_32fdx63kql7qbusah8oxjcr8c]
;

ALTER TABLE LogPublicacao WITH CHECK ADD  CONSTRAINT [FK_54hh1x96p3uay6dvxagyp4eus] FOREIGN KEY([logVersao])
REFERENCES LogRevisao ([id])
;

ALTER TABLE LogPublicacao CHECK CONSTRAINT [FK_54hh1x96p3uay6dvxagyp4eus]
;

ALTER TABLE LogPublicacaoAnexo WITH CHECK ADD  CONSTRAINT [FK_i58nx51vj92jw5msr9k47e51j] FOREIGN KEY([logVersao])
REFERENCES LogRevisao ([id])
;

ALTER TABLE LogPublicacaoAnexo CHECK CONSTRAINT [FK_i58nx51vj92jw5msr9k47e51j]
;

ALTER TABLE LogRegiaoPlanejamento WITH CHECK ADD  CONSTRAINT [FK_k9ydnex5dvitfcytnmfift7af] FOREIGN KEY([logVersao])
REFERENCES LogRevisao ([id])
;

ALTER TABLE LogRegiaoPlanejamento CHECK CONSTRAINT [FK_k9ydnex5dvitfcytnmfift7af]
;

ALTER TABLE LogRelatorioAcompanhamento WITH CHECK ADD  CONSTRAINT [FK_4say5q48ja95hei3egrtvx0u2] FOREIGN KEY([logVersao])
REFERENCES LogRevisao ([id])
;

ALTER TABLE LogRelatorioAcompanhamento CHECK CONSTRAINT [FK_4say5q48ja95hei3egrtvx0u2]
;

ALTER TABLE LogRelatorioAcompanhamentoSituacao WITH CHECK ADD  CONSTRAINT [FK_naay0igaofy1cj9ggk028mk9] FOREIGN KEY([logVersao])
REFERENCES LogRevisao ([id])
;

ALTER TABLE LogRelatorioAcompanhamentoSituacao CHECK CONSTRAINT [FK_naay0igaofy1cj9ggk028mk9]
;

ALTER TABLE LogServico WITH CHECK ADD  CONSTRAINT [FK_4k6ujmqp8bx7dke21u6tshrm6] FOREIGN KEY([logVersao])
REFERENCES LogRevisao ([id])
;

ALTER TABLE LogServico CHECK CONSTRAINT [FK_4k6ujmqp8bx7dke21u6tshrm6]
;

ALTER TABLE LogSignatario WITH CHECK ADD  CONSTRAINT [FK_jodcrdojho4l6t0vrguinn239] FOREIGN KEY([logVersao])
REFERENCES LogRevisao ([id])
;

ALTER TABLE LogSignatario CHECK CONSTRAINT [FK_jodcrdojho4l6t0vrguinn239]
;

ALTER TABLE LogSituacaoInstrumentoFormalizacao WITH CHECK ADD  CONSTRAINT [FK_gecahvigedpkibh4x0dho5nsb] FOREIGN KEY([logVersao])
REFERENCES LogRevisao ([id])
;

ALTER TABLE LogSituacaoInstrumentoFormalizacao CHECK CONSTRAINT [FK_gecahvigedpkibh4x0dho5nsb]
;

ALTER TABLE LogTemplate WITH CHECK ADD  CONSTRAINT [FK_c2ufk06eys540rrgf1i9vpto1] FOREIGN KEY([logVersao])
REFERENCES LogRevisao ([id])
;

ALTER TABLE LogTemplate CHECK CONSTRAINT [FK_c2ufk06eys540rrgf1i9vpto1]
;

ALTER TABLE LogUF WITH CHECK ADD  CONSTRAINT [FK_i2lano5new0qsuyhwvw7vx0it] FOREIGN KEY([logVersao])
REFERENCES LogRevisao ([id])
;

ALTER TABLE LogUF CHECK CONSTRAINT [FK_i2lano5new0qsuyhwvw7vx0it]
;

ALTER TABLE LogUnidadeEmpresa WITH CHECK ADD  CONSTRAINT [FK_dav99fra93gw47s3cgjdg0nc6] FOREIGN KEY([logVersao])
REFERENCES LogRevisao ([id])
;

ALTER TABLE LogUnidadeEmpresa CHECK CONSTRAINT [FK_dav99fra93gw47s3cgjdg0nc6]
;

ALTER TABLE LogUsoFonte WITH CHECK ADD  CONSTRAINT [FK_gsaq7yae953rk5x5via5bm4i2] FOREIGN KEY([logVersao])
REFERENCES LogRevisao ([id])
;

ALTER TABLE LogUsoFonte CHECK CONSTRAINT [FK_gsaq7yae953rk5x5via5bm4i2]
;

ALTER TABLE LogUsuario WITH CHECK ADD  CONSTRAINT [FK_c4uidqh6e2mwy2xt66uamh884] FOREIGN KEY([logVersao])
REFERENCES LogRevisao ([id])
;

ALTER TABLE LogUsuario CHECK CONSTRAINT [FK_c4uidqh6e2mwy2xt66uamh884]
;

ALTER TABLE LogUsuarioExterno WITH CHECK ADD  CONSTRAINT [FK_rop85lgfpilodjq065r1vohg6] FOREIGN KEY([id], [logVersao])
REFERENCES LogUsuario ([id], [logVersao])
;

ALTER TABLE LogUsuarioExterno CHECK CONSTRAINT [FK_rop85lgfpilodjq065r1vohg6]
;

ALTER TABLE LogUsuarioInterno WITH CHECK ADD  CONSTRAINT [FK_8udmifc4jw083r8vx0l2mj21f] FOREIGN KEY([id], [logVersao])
REFERENCES LogUsuario ([id], [logVersao])
;

ALTER TABLE LogUsuarioInterno CHECK CONSTRAINT [FK_8udmifc4jw083r8vx0l2mj21f]
;

ALTER TABLE LogUsuarioProjetoRecente WITH CHECK ADD  CONSTRAINT [FK_7kxm7tn2c83rfafmei5auggsl] FOREIGN KEY([logVersao])
REFERENCES LogRevisao ([id])
;

ALTER TABLE LogUsuarioProjetoRecente CHECK CONSTRAINT [FK_7kxm7tn2c83rfafmei5auggsl]
;

ALTER TABLE LogVerificationToken WITH CHECK ADD  CONSTRAINT [FK_hh86cnf3ryw7f8iphks6cllry] FOREIGN KEY([logVersao])
REFERENCES LogRevisao ([id])
;

ALTER TABLE LogVerificationToken CHECK CONSTRAINT [FK_hh86cnf3ryw7f8iphks6cllry]
;
