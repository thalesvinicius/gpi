IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.VIEWS
        WHERE TABLE_NAME = 'VIEW_RELATORIO_AGENDA_POSITIVA')
    DROP VIEW VIEW_RELATORIO_AGENDA_POSITIVA
GO 

Create View VIEW_RELATORIO_AGENDA_POSITIVA AS

WITH projeto_ver_congelada_ou_atual AS
(
   SELECT
   p.*
   FROM
   (
      SELECT
      p.id, p.familia_id
      FROM projeto p
      WHERE p.ultimaversao = 1
      AND p.versao = 0
      AND situacaoatual_id <> 4
      UNION
      SELECT
      p.id, p.familia_id
      FROM projeto p
      WHERE p.ultimaversao = 0
      AND situacaoatual_id <> 4
      AND p.id in
      (
         SELECT
         top(1) p2.id
         FROM projeto p2
         WHERE p2.ultimaversao = 0
         AND p2.situacaoatual_id <> 4
         and p2.familia_id = p.familia_id
         order by p2.versao desc
      )
      UNION
      SELECT
      p.id, p.familia_id
      FROM projeto p
      WHERE p.ultimaversao = 1
      and p.versao > 0
      AND situacaoatual_id <> 4
   )
   as tab
   INNER JOIN projeto p ON tab.id = p.id
)

Select proj.id, proj.nome Projeto, proj.situacaoAtual_id,
    proj.versao, proj.familia_id, proj.estagioAtual_id, 
    'DF' EstagioPrevisto,1 AS ESTAGIO_ID ,proj.ultimaVersao,
    (CASE WHEN proj.unidade_id IS NOT NULL THEN uni.nome ELSE empa.nomePrincipal END) as NomeEmpresa,
    usua.Nome UsuarioResponsavel, usua.id UsuarioResponsavel_id, depa.sigla Departamento, depa.id departamento_id, rpla.nome RegiaoPlanejamento,
    Municipio.nome Municipio, rpla.id RegiaoPlanejamento_id, cpro.descricao CadeiaProdutiva,
    cpro.id cadeiaprodutiva_id, inst.dataAssinaturaPrevista as Dt_Previsao, 
    inst.dataAssinatura as Dt_Real, depa.departamentosuperior_id as departamentoSuperior_id
    , (select sum(valor) from faturamentoAnterior a where a.financeiro_id = finc.id group by a.financeiro_id ) /1000 totalFaturamentoAnterior
    , (select sum(valor) from faturamentoprevisto where ano = (select min(ano) from faturamentoprevisto where financeiro_id = finc.id) and financeiro_id = finc.id) /1000 totalFaturamentoPrevisto
    , (select sum(valor) from investimentoPrevisto c where c.financeiro_id = finc.id group by c.financeiro_id ) /1000 totalInvestimentoPrevisto
    , (select sum(direto) from emprego d where d.projeto_id = proj.id group by d.projeto_id ) totalEmpregoDireto
    , (select sum(indireto) from emprego e where e.projeto_id = proj.id group by e.projeto_id ) totalEmpregoIndireto
From  projeto_ver_congelada_ou_atual proVersao
Inner Join projeto proj on proVersao.id = proj.id
left join financeiro finc on proj.id = finc.projeto_id
Left join usuario usua on proj.usuarioResponsavel_id = usua.id
Left join usuarioInterno uint on usua.id = uint.id
Left join departamento depa on uint.departamento_id = depa.id
Left join empresa empa on proj.empresa_id = empa.id
Left join localizacao loca on proj.id = loca.projeto_id
Left join Municipio on Municipio.id = loca.municipio_id
Left join regiaoPlanejamento rpla on loca.regiaoPlanejamento_id = rpla.id
Left join cronograma cron on proj.id = cron.projeto_id
Left join (Select dataAssinaturaPrevista, projeto_id, dataAssinatura from projetoInstrumentoFormalizacao pIns    
            inner join InstrumentoFormalizacao ifor on pIns.instrumentoFormalizacao_id = iFor.id
            and ifor.tipo = 4) inst on proj.id = inst.projeto_id
Left join cadeiaProdutiva cpro on proj.cadeiaProdutiva_id = cpro.id
LEFT JOIN UnidadeEmpresa uni ON uni.id=proj.unidade_id
where  inst.dataAssinatura is null
AND inst.dataAssinaturaPrevista is not null
AND (proj.estagioAtual_id = 5 or proj.estagioAtual_id = 2 or proj.estagioAtual_id = 4) -- PP, II ou OI

UNION


Select proj.id, proj.nome Projeto, proj.situacaoAtual_id, proj.versao,
    proj.familia_id, proj.estagioAtual_id, 'II' EstagioPrevisto, 2 AS ESTAGIO_ID ,proj.ultimaVersao,
    (CASE WHEN proj.unidade_id IS NOT NULL THEN uni.nome ELSE empa.nomePrincipal END) as NomeEmpresa,
    usua.Nome UsuarioResponsavel, usua.id UsuarioResponsavel_id, depa.sigla Departamento, depa.id departamento_id,
    rpla.nome RegiaoPlanejamento, Municipio.nome Municipio, rpla.id RegiaoPlanejamento_id,
    cpro.descricao CadeiaProdutiva, cpro.id cadeiaprodutiva_id, 
    Case when cron.inicioOperacao is not null then cron.inicioOperacao 
        when cron.inicioOperacaoSucroenergetico is not null then cron.inicioOperacaoSucroenergetico
        else null end Dt_Previsao, eproOI.dataInicioEstagio Dt_Real, depa.departamentosuperior_id as departamentoSuperior_id
    , (select sum(valor) from faturamentoAnterior a where a.financeiro_id = finc.id group by a.financeiro_id ) /1000 totalFaturamentoAnterior
    , (select sum(valor) from faturamentoprevisto where ano = (select min(ano) from faturamentoprevisto where financeiro_id = finc.id) and financeiro_id = finc.id) /1000 totalFaturamentoPrevisto
    , (select sum(valor) from investimentoPrevisto c where c.financeiro_id = finc.id group by c.financeiro_id ) /1000 totalInvestimentoPrevisto
    , (select sum(direto) from emprego d where d.projeto_id = proj.id group by d.projeto_id ) totalEmpregoDireto
    , (select sum(indireto) from emprego e where e.projeto_id = proj.id group by e.projeto_id ) totalEmpregoIndireto
From  projeto_ver_congelada_ou_atual proVersao
Inner Join projeto proj on proVersao.id = proj.id
inner join (Select familia_id, max(versao) versaoAtual from projeto group by familia_id) proj2 on proj.familia_id = proj2.familia_id left join financeiro finc                       on proj.id = finc.projeto_id
Left join usuario usua                          on proj.usuarioResponsavel_id = usua.id
Left join usuarioInterno uint                   on usua.id = uint.id
Left join departamento depa                     on uint.departamento_id = depa.id
Left join empresa empa                          on proj.empresa_id = empa.id
Left join localizacao loca                      on proj.id = loca.projeto_id
Left join Municipio on Municipio.id = loca.municipio_id
Left join regiaoPlanejamento rpla               on loca.regiaoPlanejamento_id = rpla.id
Left join cronograma cron                       on proj.id = cron.projeto_id
Left join (Select dataAssinaturaPrevista, projeto_id from projetoInstrumentoFormalizacao pIns    
            Inner join InstrumentoFormalizacao ifor         
            on pIns.instrumentoFormalizacao_id = iFor.id
            And ifor.tipo = 4) inst on proj.id = inst.projeto_id
Left join estagioProjeto eproOI                 on proj.id = eproOI.projeto_id
                                                and eproOI.estagio_id = 4 Left join cadeiaProdutiva cpro                  on proj.cadeiaProdutiva_id = cpro.id
LEFT JOIN UnidadeEmpresa uni ON uni.id=proj.unidade_id
Where ((proj.ultimaVersao = 'true' and versaoAtual = 0) or (proj.ultimaVersao = 'false' and proj.versao = versaoAtual -1 )) And eproOI.dataInicioEstagio is null
AND proj.estagioAtual_id = 1 -- DF
And Case when cron.inicioOperacao is not null then cron.inicioOperacao 
When cron.inicioOperacaoSucroenergetico is not null then cron.inicioOperacaoSucroenergetico
Else null end is not null

UNION


Select proj.id, proj.nome Projeto, proj.situacaoAtual_id, proj.versao, proj.familia_id,
    proj.estagioAtual_id, 'OI' EstagioPrevisto, 4 AS ESTAGIO_ID, proj.ultimaVersao, 
    (CASE WHEN proj.unidade_id IS NOT NULL THEN uni.nome ELSE empa.nomePrincipal END) as NomeEmpresa,
    usua.Nome UsuarioResponsavel, usua.id UsuarioResponsavel_id, depa.sigla Departamento, depa.id departamento_id, rpla.nome RegiaoPlanejamento,
    Municipio.nome Municipio, rpla.id RegiaoPlanejamento_id, cpro.descricao CadeiaProdutiva,
    cpro.id cadeiaprodutiva_id, 
    Case when cron.inicioImplantacao is not null then cron.inicioImplantacao 
    when cron.inicioImplantacaoAgricola is not null then cron.inicioImplantacaoAgricola
    when cron.inicioImplantacaoIndustrial is not null then cron.inicioImplantacaoIndustrial
    else null end Dt_Previsao, eproII.dataInicioEstagio Dt_Real, depa.departamentosuperior_id as departamentoSuperior_id
    , (select sum(valor) from faturamentoAnterior a where a.financeiro_id = finc.id group by a.financeiro_id ) /1000 totalFaturamentoAnterior
    , (select sum(valor) from faturamentoprevisto where ano = (select min(ano) from faturamentoprevisto where financeiro_id = finc.id) and financeiro_id = finc.id) /1000 totalFaturamentoPrevisto
    , (select sum(valor) from investimentoPrevisto c where c.financeiro_id = finc.id group by c.financeiro_id ) /1000 totalInvestimentoPrevisto
    , (select sum(direto) from emprego d where d.projeto_id = proj.id group by d.projeto_id ) totalEmpregoDireto
    , (select sum(indireto) from emprego e where e.projeto_id = proj.id group by e.projeto_id ) totalEmpregoIndireto
From  projeto_ver_congelada_ou_atual proVersao
Inner Join projeto proj on proVersao.id = proj.id
inner join (Select familia_id, max(versao) versaoAtual from projeto group by familia_id) proj2 on proj.familia_id = proj2.familia_id left join financeiro finc                       on proj.id = finc.projeto_id
Left join usuario usua                          on proj.usuarioResponsavel_id = usua.id
Left join usuarioInterno uint                   on usua.id = uint.id
Left join departamento depa                     on uint.departamento_id = depa.id
Left join empresa empa                          on proj.empresa_id = empa.id
Left join localizacao loca                      on proj.id = loca.projeto_id
Left join Municipio on Municipio.id = loca.municipio_id
Left join regiaoPlanejamento rpla               on loca.regiaoPlanejamento_id = rpla.id
Left join cronograma cron                       on proj.id = cron.projeto_id
Left join (Select dataAssinaturaPrevista, projeto_id from projetoInstrumentoFormalizacao pIns    
    inner join InstrumentoFormalizacao ifor         
    on pIns.instrumentoFormalizacao_id = iFor.id
    and ifor.tipo = 4) inst on proj.id = inst.projeto_id
Left join estagioProjeto eproII on proj.id = eproII.projeto_id
and eproII.estagio_id = 2 Left join cadeiaProdutiva cpro                  on proj.cadeiaProdutiva_id = cpro.id
LEFT JOIN UnidadeEmpresa uni ON uni.id=proj.unidade_id
where ((proj.ultimaVersao = 'true' and versaoAtual = 0) or (proj.ultimaVersao = 'false' and proj.versao = versaoAtual -1 )) and eproII.dataInicioEstagio is null
AND (proj.estagioAtual_id = 1 or proj.estagioAtual_id = 2) -- DF ou II
AND Case When cron.inicioImplantacao is not null then cron.inicioImplantacao 
 When cron.inicioImplantacaoAgricola is not null then cron.inicioImplantacaoAgricola
 When cron.inicioImplantacaoIndustrial is not null then cron.inicioImplantacaoIndustrial
Else null end is not null
