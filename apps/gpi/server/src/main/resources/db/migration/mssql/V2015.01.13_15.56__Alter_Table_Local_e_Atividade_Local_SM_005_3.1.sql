alter table Local add ativo bit;
GO
alter table AtividadeLocal add ativo bit;
GO

update Local set ativo = 1;
update AtividadeLocal set ativo = 1;

alter table Local alter column ativo bit not null;
GO
alter table AtividadeLocal alter column ativo bit not null;
GO