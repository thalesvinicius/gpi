IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.VIEWS
        WHERE TABLE_NAME = 'VIEW_RELATORIO_ACOPANHAMENTO_ANUAL')
    DROP VIEW VIEW_RELATORIO_ACOPANHAMENTO_ANUAL
GO

CREATE VIEW VIEW_RELATORIO_ACOPANHAMENTO_ANUAL AS 
SELECT ultver.id as idUltimaVersao,
RelatorioAcompanhamento.ano,
projeto.nome nomeProjeto,
empresa.nomePrincipal,
usuarioExterno1_.email,
RelatorioAcompanhamento.situacaoAtual_id,
projeto.estagioAtual_id,
RelatorioAcompanhamento.dataValidacao,
RelatorioAcompanhamento.dataUltimaAlteracao,
RelatorioAcompanhamento.dataInicioPrimeiraCampanha,
RelatorioAcompanhamento.dataFimPrimeiraCampanha,
RelatorioAcompanhamento.dataInicioSegundaCampanha,
RelatorioAcompanhamento.dataFimSegundaCampanha,
CASE 
	WHEN RelatorioAcompanhamento.ano < IsNull(Year(RelatorioAcompanhamento.dataValidacao),0) THEN Year(RelatorioAcompanhamento.dataValidacao)
	WHEN RelatorioAcompanhamento.ano < IsNull(Year(RelatorioAcompanhamento.dataInicioSegundaCampanha),0) THEN Year(RelatorioAcompanhamento.dataInicioSegundaCampanha)
	WHEN RelatorioAcompanhamento.ano  <  IsNull (Year(RelatorioAcompanhamento.dataInicioPrimeiraCampanha),0) THEN  Year(RelatorioAcompanhamento.dataInicioPrimeiraCampanha)
	ELSE RelatorioAcompanhamento.ano
END AS anoEnviado,
usuarioint4_1_.nome nomeUsuario,
projeto.cadeiaProdutiva_id,
departamento.id as departamento_id,
usuarioint4_.id usuario_id
from Projeto projeto 
INNER join Empresa empresa on projeto.empresa_id=empresa.id 
left outer join UsuarioExterno usuarioExterno on empresa.usuarioExternoResponsavel_id=usuarioExterno.id 
left outer join Usuario usuarioExterno1_ on usuarioExterno.id=usuarioExterno1_.id 
left outer join RelatorioAcompanhamento RelatorioAcompanhamento on projeto.familia_id=RelatorioAcompanhamento.familia_id 
left outer join UsuarioInterno usuarioint4_ on projeto.usuarioInvestimento_id=usuarioint4_.id 
left outer join Usuario usuarioint4_1_ on usuarioint4_.id=usuarioint4_1_.id 
left outer join Departamento departamento on usuarioint4_.departamento_id=departamento.id
inner join PROJETOINSTRUMENTOFORMALIZACAO instrument6_ on projeto.id=instrument6_.projeto_id
inner join InstrumentoFormalizacao instrument7_ on instrument6_.instrumentoFormalizacao_id=instrument7_.id
inner join VIEW_PROJETO_ULT_VERSAO_ATIVA_SUSPENSA ultver on projeto.familia_id = ultver.familia_id
and instrument7_.tipo=4
and instrument7_.situacaoAtual_id=1;

