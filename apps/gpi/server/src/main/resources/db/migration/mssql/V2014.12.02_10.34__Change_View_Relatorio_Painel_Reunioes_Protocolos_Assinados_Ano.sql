IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.VIEWS
        WHERE TABLE_NAME = 'VIEW_RELATORIO_PAINEL_REUNIOES_PROTOCOLOS_ASSINADOS_ANO')
    DROP VIEW VIEW_RELATORIO_PAINEL_REUNIOES_PROTOCOLOS_ASSINADOS_ANO
GO

CREATE VIEW VIEW_RELATORIO_PAINEL_REUNIOES_PROTOCOLOS_ASSINADOS_ANO
AS
SELECT
    DPS.id AS diretoriaId,
    DPS.descricao AS diretoria,
    DPT.id AS gerenciaId,
    DPT.descricao AS gerencia,    
    INS.tipo AS tipoInstrumentoId, 	
    DATEPART(yyyy, INS.dataassinatura) AS anoAssinaturaInstrumento,
    COUNT(distinct INS.id) AS numProtocolos
FROM InstrumentoFormalizacao INS 
    LEFT JOIN ProjetoInstrumentoFormalizacao PEI ON INS.id = PEI.instrumentoformalizacao_id
    LEFT join projeto PRJ ON PEI.projeto_id = PRJ.id
    LEFT JOIN Usuario USR ON USR.id = PRJ.usuarioinvestimento_id
    LEFT JOIN UsuarioInterno UIN ON UIN.id = USR.id
    LEFT JOIN Departamento DPT ON DPT.id = UIN.departamento_id
    LEFT JOIN HistoricoSituacaoInstrumentoFormalizacao HSI ON HSI.id = INS.historicoSituacaoInstrumentoFormalizacao_id
    LEFT JOIN situacaoInstrumentoFormalizacao SIT ON SIT.id = HSI.situacaoInstrumentoFormalizacao_id
    LEFT JOIN Departamento DPS ON DPS.id = DPT.departamentosuperior_id
WHERE 
    INS.dataassinatura IS NOT NULL
GROUP BY
    DPS.id,
    DPS.descricao,    
    DPT.id,
    DPT.descricao,    
    INS.tipo,	
    DATEPART(yyyy, INS.dataassinatura);