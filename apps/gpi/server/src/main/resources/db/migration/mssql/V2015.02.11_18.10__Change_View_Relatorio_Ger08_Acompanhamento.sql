IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.VIEWS
        WHERE TABLE_NAME = 'VIEW_RELATORIO_ACOPANHAMENTO_GERAL')
    DROP VIEW VIEW_RELATORIO_ACOPANHAMENTO_GERAL
GO 

CREATE VIEW VIEW_RELATORIO_ACOPANHAMENTO_GERAL AS 

WITH projeto_ver_congelada_ou_atual AS
(
   SELECT
   tab.isLastVersion, p.*
   FROM
   (
      SELECT
      1 as isLastVersion, p.id, p.familia_id
      FROM projeto p
      WHERE p.ultimaversao = 1
      AND p.versao = 0
      AND situacaoatual_id <> 4
      UNION
      SELECT
      0 as isLastVersion, p.id, p.familia_id
      FROM projeto p
      WHERE p.ultimaversao = 0
      AND situacaoatual_id <> 4
      AND p.id in
      (
         SELECT
         top(1) p2.id
         FROM projeto p2
         WHERE p2.ultimaversao = 0
         AND p2.situacaoatual_id <> 4
         and p2.familia_id = p.familia_id
         order by p2.versao desc
      )      
   )
   as tab
   INNER JOIN projeto p ON tab.id = p.id
)

SELECT DISTINCT E.nomePrincipal AS NOME_EMPRESA, vals.ano,
E.id AS ID_EMPRESA, 
P.id AS ID_projeto, 

p.nome AS NOME_PROJETO,
m.nome as CIDADE, 
cp.id as ID_CADEIA_PRODUTIVA, 
rp.nome as regiao_planejamento,
CP.descricao AS CADEIA_PRODUTIVA,
es.id AS estagioId,
es.descricao as ESTAGIO_PROJETO, 
P.situacaoAtual_id as statusId, 
u.nome as ANALISTA, 
u.id as analistaId,
d.sigla as GERENCIA,
d.id as gerenciaId,
rp.id as regiaoPlanejamentoId,
vals.investPrevisto AS INVESTIMENTO_PREVISTO,
vals.fatPrevisto AS FATURAMENTO_PREVISTO ,
vals.investRealizado as INVESTIMENTO_REALIZADO,
vals.fatRealizado AS FATURAMENTO_REALIZADO,

SUM(ISNULL(EP.DIRETOS,0)) AS PERMANENTES_DIRETOS, 
SUM(ISNULL(EP.REALIZADOS,0)) AS PERMANENTES_DIRETOS_REALIZADOS

FROM Empresa E inner join projeto_ver_congelada_ou_atual P on P.empresa_id = E.id
INNER JOIN Localizacao l on l.projeto_id = p.id
inner join RegiaoPlanejamento rp on rp.id  = l.regiaoPlanejamento_id
inner join CadeiaProdutiva cp on P.cadeiaProdutiva_id = cp.id
INNER JOIN Estagio es on es.id = P.estagioAtual_id
inner join Financeiro f on p.id = f.projeto_id
inner join UsuarioInterno a on a.id = p.usuarioResponsavel_id
INNER JOIN Usuario u on u.id = a.id
INNER JOIN Municipio m on m.id = l.municipio_id
inner join Departamento d on d.id = A.departamento_id
inner join (SELECT  SUM ( CASE when IPFP.invfat = 1 then ipfp.previsto else 0 end ) / 1000 AS investPrevisto,    
	  			SUM ( CASE when IPFP.invfat = 1 then ipfp.realizado else 0 end )/ 1000 AS investRealizado,	
	  			SUM ( CASE when IPFP.invfat = 2 then ipfp.previsto else 0 end ) / 1000 AS fatPrevisto,    
	  			SUM ( CASE when IPFP.invfat = 2 then ipfp.realizado else 0 end ) / 1000 AS fatRealizado,	
	  ipfp.ano,
	  ipfp.financeiro_id
	FROM (
		SELECT ip.ano, ip.financeiro_id , SUM(ISNULL(IP.valor,0)) as previsto, SUM(ISNULL(ip.valorRealizado,0)) as realizado, 1 as invfat
      	FROM InvestimentoPrevisto ip WHERE IP.ano > 0 GROUP BY IP.ano,IP.financeiro_id
  		UNION
     	SELECT fp.ano,fp.financeiro_id, SUM(ISNULL(fp.valor,0)) as previsto, SUM(ISNULL(fp.valorRealizado,0)) as realizado, 2 as invfat  
     	FROM FaturamentoPrevisto fp WHERE fP.ano > 0 GROUP BY FP.ano, FP.financeiro_id
         ) ipfp
GROUP BY ipFP.ano, ipFP.financeiro_id ) vals on vals.financeiro_id = f.id
left join (SELECT em.projeto_id, em.ano, SUM(em.direto) AS DIRETOS, SUM(em.diretoRealizado) AS REALIZADOS from Emprego em
			WHERE em.tipo in (2,4,7,8) group by em.projeto_id, em.ano) ep on ep.projeto_id = p.id and ep.ano = vals.ano

WHERE P.ultimaVersao = 1 AND ES.id = P.estagioAtual_id 

group by vals.ano, e.nomePrincipal,e.id,p.id,p.nome,cp.id,rp.id,cp.descricao,p.situacaoAtual_id,u.nome,d.id,m.nome,
rp.nome,es.descricao,d.descricao,d.sigla,u.id,es.id ,vals.investPrevisto, vals.fatPrevisto, vals.investRealizado, vals.fatRealizado;