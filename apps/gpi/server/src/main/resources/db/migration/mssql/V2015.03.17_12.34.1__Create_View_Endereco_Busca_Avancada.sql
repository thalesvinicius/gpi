IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.VIEWS
        WHERE TABLE_NAME = 'VIEW_ENDERECO_BUSCA_AVANCADA')
    DROP VIEW VIEW_ENDERECO_BUSCA_AVANCADA
GO

Create view VIEW_ENDERECO_BUSCA_AVANCADA AS

select distinct 
e.id, 
m.id as municipio_id, 
isnull(isnull(m.nome, e.cidade), '') as municipioCidade,
m.nome as municipio, 
uf.id as uf_id, 
uf.sigla as siglaEstado, 
uf.nome as estado, 
e.logradouro, 
e.numero, 
e.complemento, 
e.CEP, 
e.bairro, 
e.cidade, 
p.id as pais_id, 
p.nome as pais, 
mr.id as microregiao_id, 
mr.nome as microregiao, 
rp.id as regiaoPlanejamento_id, 
rp.nome as RegiaoPlanejamento,
CAST(ISNULL(m.nome, ISNULL(e.cidade, ' - ')) AS VARCHAR(MAX)) + ', ' + 
ISNULL(e.logradouro, ' - ') + ', ' +
CAST(ISNULL(e.numero, ' - ') AS VARCHAR(MAX)) +
ISNULL(e.complemento, '') + ' - CEP: ' + 
ISNULL(LEFT(e.cep, 5) + '-' + RIGHT(e.cep, 3), ' - ') as enderecoCompleto

from endereco e
left join UF uf on e.uf_id = uf.id
left join Municipio m on e.municipio_id = m.id
left join pais p on e.pais_id = p.id
left join MicroRegiao mr on m.microRegiao_id = mr.id
left join RegiaoPlanejamento rp on m.regiaoPlanejamento_id = rp.id;