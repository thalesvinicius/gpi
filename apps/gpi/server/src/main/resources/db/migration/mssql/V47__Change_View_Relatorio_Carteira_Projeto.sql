IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.VIEWS
        WHERE TABLE_NAME = 'VIEW_RELATORIO_CARTEIRA_PROJETO')
    DROP VIEW VIEW_RELATORIO_CARTEIRA_PROJETO
GO 

Create View VIEW_RELATORIO_CARTEIRA_PROJETO AS

WITH projeto_ver_congelada_ou_atual AS (
	SELECT p.* 
	FROM (
		SELECT p.id, p.familia_id 
		FROM projeto p
		WHERE p.ultimaversao = 1 AND p.versao = 0 AND situacaoatual_id <> 4	
		UNION	
		SELECT max(p.id), p.familia_id 
		FROM projeto p 
		WHERE p.ultimaversao = 0 AND situacaoatual_id <> 4	
		GROUP BY p.familia_id
	) as tab
	INNER JOIN projeto p ON tab.id = p.id
)
SELECT
        PRJ.id AS id,
        EMP.nomeprincipal AS empresa,
        PRJ_ATUAL.nome AS projeto,	
        MUN.nome AS cidade,
        REG.nome AS regiaoPlanejamento,
        REG.id AS regiaoPlanejamentoId,

        (select sum(valor) from investimentoprevisto where financeiro_id = fin.id) /1000 AS investimentoPrevisto,	
        (select sum(valor) from faturamentoprevisto where ano = (select min(ano) from faturamentoprevisto where financeiro_id = fin.id) and financeiro_id = fin.id) /1000 as faturamentoInicial,
        (select sum(valor) from faturamentoprevisto where ano = (select max(ano) from faturamentoprevisto where financeiro_id = fin.id) and financeiro_id = fin.id) /1000 as faturamentoFinal,
        (select sum(direto) from emprego where projeto_id = prj.id) AS empregosDiretos,
        (select sum(indireto) from emprego where projeto_id = prj.id) AS empregosIndiretos,

        CAD.id AS cadeiaProdutivaId,
        CAD.descricao AS cadeiaProdutiva,
        STC.id AS situacaoProjeto,
        PRJ_ATUAL.estagioatual_id AS estagioAtual,
        USR.nome AS analista,
        USR.id AS analistaId,	
        DPT.id AS gerenciaId,
        DPT.sigla AS gerencia,
        DPS.id AS diretoriaId

FROM projeto_ver_congelada_ou_atual PRJ
        LEFT JOIN Projeto PRJ_ATUAL on PRJ_ATUAL.familia_id = PRJ.familia_id
        INNER JOIN Empresa EMP ON EMP.id = PRJ_ATUAL.empresa_id
        LEFT JOIN Localizacao LOC ON LOC.projeto_id = PRJ_ATUAL.id
        LEFT JOIN Municipio MUN ON MUN.id = LOC.municipio_id
        LEFT JOIN RegiaoPlanejamento REG ON REG.id = LOC.regiaoplanejamento_id
        LEFT JOIN Financeiro FIN ON FIN.projeto_id = PRJ.id    
        LEFT JOIN CadeiaProdutiva CAD ON CAD.id = PRJ_ATUAL.cadeiaprodutiva_id
        LEFT JOIN Usuario USR ON USR.id = PRJ_ATUAL.usuarioinvestimento_id
        LEFT JOIN UsuarioInterno UIN ON UIN.id = USR.id
        LEFT JOIN Departamento DPT ON DPT.id = UIN.departamento_id
        LEFT JOIN Departamento DPS ON DPS.id = DPT.departamentosuperior_id
        LEFT JOIN HistoricoSituacaoProjeto HST on HST.id = PRJ_ATUAL.historicosituacaoprojeto_id
        LEFT JOIN situacaoProjeto STC on STC.id = HST.situacaoprojeto_id
WHERE PRJ_ATUAL.ultimaVersao = 1