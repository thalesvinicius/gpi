-- FKs
IF EXISTS (select cons.CONSTRAINT_NAME FROM information_schema.TABLE_CONSTRAINTS cons
where cons.CONSTRAINT_NAME = 'FK_Empresa_Endereco')
        ALTER TABLE Empresa DROP CONSTRAINT FK_Empresa_Endereco;

ALTER TABLE Empresa ADD CONSTRAINT FK_Empresa_Endereco 
	FOREIGN KEY (endereco_id) REFERENCES Endereco(id);

IF EXISTS (select cons.CONSTRAINT_NAME FROM information_schema.TABLE_CONSTRAINTS cons
where cons.CONSTRAINT_NAME = 'FK_Empresa_Anexo')
        ALTER TABLE Empresa DROP CONSTRAINT FK_Empresa_Anexo;

ALTER TABLE Empresa ADD CONSTRAINT FK_Empresa_Anexo 
	FOREIGN KEY (anexo_id) REFERENCES Anexo(id);

-- UnidadeEmpresa
IF EXISTS (select cons.CONSTRAINT_NAME FROM information_schema.TABLE_CONSTRAINTS cons
where cons.CONSTRAINT_NAME = 'FK_UnidadeEmpresa_Empresa')
ALTER TABLE UnidadeEmpresa DROP CONSTRAINT FK_UnidadeEmpresa_Empresa;

ALTER TABLE UnidadeEmpresa ADD CONSTRAINT FK_UnidadeEmpresa_Empresa 
	FOREIGN KEY (empresa_id) REFERENCES Empresa (id);

IF EXISTS (select cons.CONSTRAINT_NAME FROM information_schema.TABLE_CONSTRAINTS cons
where cons.CONSTRAINT_NAME = 'FK_Unidade_Empresa_Endereco')
        ALTER TABLE UnidadeEmpresa DROP CONSTRAINT FK_Unidade_Empresa_Endereco;

ALTER TABLE UnidadeEmpresa ADD CONSTRAINT FK_Unidade_Empresa_Endereco 
	FOREIGN KEY (endereco_id) REFERENCES Endereco(id);

--Indexes
IF EXISTS (select ix.name FROM sysindexes ix
where ix.name = 'IX_Empresa_Endereco_id')

DROP INDEX IX_Empresa_Endereco_id ON Empresa;

IF EXISTS (select ix.name FROM sysindexes ix
where ix.name = 'IX_UnidadeEmpresa_Endereco_id')

DROP INDEX IX_UnidadeEmpresa_Endereco_id ON UnidadeEmpresa;

create index IX_Empresa_Endereco_id on Empresa (endereco_id);
create index IX_UnidadeEmpresa_Endereco_id on UnidadeEmpresa (endereco_id);