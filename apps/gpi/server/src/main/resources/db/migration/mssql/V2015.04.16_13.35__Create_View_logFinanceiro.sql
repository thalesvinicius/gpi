IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.VIEWS
        WHERE TABLE_NAME = 'view_logFinanceiro')
    DROP VIEW view_logFinanceiro
GO

Create View view_logFinanceiro AS
Select
   LOGFIN.dataUltimaAlteracao,
   Case
       When (LOGINV.ano_MOD = 1 OR LOGINV.valor_MOD = 1) then 1 else 0
   End as investimentoPrevistoList_MOD,
   Case
       When (LOGORG.origemNacional_MOD = 1 OR LOGORG.pais_MOD = 1 OR LOGORG.percentualInvestimento_MOD = 1 OR LOGORG.uf_MOD = 1 OR LOGORG.valorInvestimento_MOD = 1) then 1 else 0
   End as origemRecursoList_MOD,
   Case
   	  When (LOGUSO.valorRealizado_MOD = 1 OR LOGUSO.valorARealizar_MOD = 1) then 1 else 0
   End as usoFonteList_MOD,
   Case
       When (LOGFAN.ano_MOD = 1 OR LOGFAN.valor_MOD = 1) then 1 else 0
   End as faturamentoAnteriorList_MOD,
   Case
       When (LOGFAP.ano_MOD = 1 OR LOGFAP.valor_MOD = 1) then 1 else 0
   End as faturamentoPrevistoList_MOD,
   LOGFIN.projeto_id as projetoId
From
   logFinanceiro LOGFIN
   Left Join logInvestimentoPrevisto LOGINV 
   	on LOGINV.financeiro_id = LOGFIN.id and LOGINV.dateCreated >= LOGFIN.dataUltimaAlteracao
   Left Join logOrigemRecurso LOGORG 
   	on LOGORG.financeiro_id = LOGFIN.id and LOGORG.dateCreated >= LOGFIN.dataUltimaAlteracao
   Left Join logUsoFonte LOGUSO
   	on LOGUSO.financeiro_id = LOGFIN.id and LOGUSO.dateCreated >= LOGFIN.dataUltimaAlteracao
   Left Join logFaturamentoAnterior LOGFAN
   	on LOGFAN.financeiro_id = LOGFIN.id and LOGFAN.dateCreated >= LOGFIN.dataUltimaAlteracao   
   Left Join logFaturamentoPrevisto LOGFAP
   	on LOGFAP.financeiro_id = LOGFIN.id and LOGFAP.dateCreated >= LOGFIN.dataUltimaAlteracao   
Where
   LOGFIN.logVersao = (
   	Select max(logVersao) 
   	From logFinanceiro LOGFIN2 
   	where LOGFIN.projeto_id = LOGFIN2.projeto_id
   )   