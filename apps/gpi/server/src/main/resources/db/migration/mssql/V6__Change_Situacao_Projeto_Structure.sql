/*
Comment GO statements to work on H2 database. 
Keep uncommented GO statements to work on Sql Server database.
*/
CREATE TABLE SituacaoProjeto ( 
	id int NOT NULL,
	descricao varchar(100) NOT NULL
);

alter table projeto add historicoSituacaoProjeto_id int null;
GO
alter table HistoricoSituacaoProjeto add situacaoProjeto_id int null;
GO
update HistoricoSituacaoProjeto set situacaoProjeto_id = situacao;

alter table HistoricoSituacaoProjeto drop column situacao;
GO
alter table HistoricoSituacaoProjeto alter column situacaoProjeto_id int not null;
GO

