IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.VIEWS
        WHERE TABLE_NAME = 'VIEW_RELATORIO_ACOMPANHAMENTO_INSTRUMENTO')
    DROP VIEW VIEW_RELATORIO_ACOMPANHAMENTO_INSTRUMENTO
GO

Create view VIEW_RELATORIO_ACOMPANHAMENTO_INSTRUMENTO AS 

SELECT DISTINCT 
inst.id as id,
e.nomePrincipal as empresa,
inst.titulo as titulo,
inst.dataAssinaturaPrevista as previsaoAssinatura,
inst.tipo as tipo,
loc.descricao as localAtividade,
loc.id as localAtividadeId,
al.descricao as atividade,
ai.dataInicio as dataInicioAtividade,
usuario.matricula as analista,
usuario.id as analistaId,
dep.sigla as gerencia,
dep.id as gerenciaId

FROM INSTRUMENTOFORMALIZACAO inst

LEFT JOIN PROJETOINSTRUMENTOFORMALIZACAO projIf ON projIF.instrumentoFormalizacao_id = inst.id
LEFT JOIN PROJETO proj ON proj.id=projIf.projeto_id
LEFT JOIN EMPRESA e ON e.id=proj.empresa_id
LEFT JOIN USUARIOINTERNO usuario ON usuario.id=proj.usuarioResponsavel_id
LEFT JOIN ATIVIDADEINSTRUMENTO ai ON ai.instrumentoFormalizacao_id = inst.id
LEFT JOIN ATIVIDADELOCAL al ON al.id = ai.atividadeLocal_id
LEFT JOIN LOCAL loc ON loc.id = al.local_id
LEFT JOIN DEPARTAMENTO dep ON dep.id=usuario.departamento_id

WHERE 
-- Atividade Atual
(ai.paralela = 0 or ai.paralela IS NULL)
AND ai.dataInicio IS NOT NULL
AND ai.dataConclusao IS NULL
 -- Em negociação
AND inst.situacaoAtual_id = 3