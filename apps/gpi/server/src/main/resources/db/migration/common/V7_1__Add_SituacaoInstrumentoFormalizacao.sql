-- SituacaoInstrumentoFormalizacao
insert into SITUACAOINSTRUMENTOFORMALIZACAO(ID, DESCRICAO) values (1, 'Assinado');
insert into SITUACAOINSTRUMENTOFORMALIZACAO(ID, DESCRICAO) values (2, 'Cancelado');
insert into SITUACAOINSTRUMENTOFORMALIZACAO(ID, DESCRICAO) values (3, 'Em Negociação');
insert into SITUACAOINSTRUMENTOFORMALIZACAO(ID, DESCRICAO) values (4, 'Extinto');