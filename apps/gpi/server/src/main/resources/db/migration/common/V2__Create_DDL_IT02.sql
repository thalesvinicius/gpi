CREATE TABLE InstrumentoFormalizacao (
    id int identity(1,1) NOT NULL,
    protocolo varchar(10),
    situacao int,
    tipo int,
    versao int,
    titulo varchar(255),
    dataAssinaturaPrevista datetime,
    dataAssinatura datetime,
    possuiContrapartidaFincGoverno bit,
    usuarioResponsavel_id int,
    dataUltimaAlteracao datetime
);

CREATE TABLE Signatario (
    id int identity(1,1) NOT NULL,
    instrumentoFormalizacao_id int,
    nome varchar(255),
    cargo varchar(255),
    telefone1 varchar(255),
    telefone2 varchar(255),
    email varchar(255)
);

CREATE TABLE ProjetoInstrumentoFormalizacao (
    projeto_id int,
    instrumentoFormalizacao_id int
);

CREATE TABLE AtividadeInstrumento (
	id int identity(1,1)  NOT NULL,    --  Identificador da atividade
	instrumentoFormalizacao_id int NOT NULL,    --  Identificador do instrumento de formaliza��o
	atividadeLocal_id int NOT NULL,    --  C�digo da Atividade referente ao local
	posicao int NOT NULL,    --  Posicionamento da atividade, ordena��o
	anexo_id int,    --  Identificador do anexo de evidencia da atividade
	dataInicio datetime,    --  Data de in�cio da atividade
	dataConclusao datetime,    --  Data de Conclus�o da atividade
        obrigatorio bit,    --  � obrigat�rio?
        observacao varchar(200),    --  Observa��o da atividade
	paralela bit,    --  � uma atividade paralela?
	bloqueiaICE bit,    --  Bloqueia o cadastro do ICE?
	df bit,    --  Inicia o est�gio de DF?
	ii bit,    --  Inicia o est�gio de II?
	oi bit,    --  Inicia o est�gio de OI?
	cc bit,    --  Inicia o est�gio de CC?
	usuarioResponsavel_id int NOT NULL,    --  Usu�rio respons�vel pelas informa��es
	dataUltimaAlteracao datetime NOT NULL    --  Data da �ltima altera��o
);

CREATE TABLE AtividadeLocal (
	id int NOT NULL,    --  C�digo da Atividade de Local
	local_id int NOT NULL,    --  C�digo do local
	descricao varchar(255) NOT NULL,    --  Descri��o da atividade
);

CREATE TABLE AtividadeProjeto (
	id int identity(1,1)  NOT NULL,    --  Identificador da atividade de projeto
	projeto_id int NOT NULL,    --  Identificador do projeto
	atividadeLocal_id int NOT NULL,    --  Identificador da atividade referente ao local
	posicao int NOT NULL,    --  Posi��o da atividade, ordena��o
	anexo_id int,    --  Arquivo de evid�ncia da atividade
	dataConclusao datetime,    --  Data de conclus�o da atividade
	observacao varchar(200),    --  Observa��o da atividade
	obrigatorio bit,    --  � obrigat�rio?
	bloqueiaICE bit,    --  Bloqueia o ICE?
	df bit,    --  Inicia o est�gio DF?
	ii bit,    --  Inicia o est�gio II?
	oi bit,    --  Inicia o est�gio OI?
	cc bit,    --  Inicia o est�gio CC?
	usuarioResponsavel_id int NOT NULL,    --  Usu�rio responsavel pelas informa��es
	dataUltimaAlteracao datetime NOT NULL    --  Data da �ltima altera��o
);

CREATE TABLE AtividadeTemplate (
	id int identity(1,1)  NOT NULL,    --  Identificador da atividade
	atividadeLocal_id int NOT NULL,    --  C�digo da atividade referente ao local
	template_id int NOT NULL,    --  Identificador do template
	posicao int NOT NULL,    --  Posi��o da atividade, referente a ordena��o
	obrigatorio bit,    --  � obrigat�rio?
	bloqueiaICE bit,    --  Bloqueia o cadastro do ICE?
	df bit,    --  Inicia o est�gio DF?
	ii bit,    --  Inicia o est�gio II?
	oi bit,    --  Inicia o est�gio OI?
	cc bit    --  Inicia o est�gio CC?
);

CREATE TABLE Concorrente (
	id int identity(1,1)  NOT NULL,    --  identificador do concorrente
	insumoProduto_id int NOT NULL,    --  Identificador do insumo e produto
	nome varchar(200),    --  Nome do concorrente
	descricaoProduto varchar(200)    --  Nome do produto
);

CREATE TABLE EtapaMeioAmbiente (
	id int identity(1,1)  NOT NULL,    --  Identificador das Etapas
	meioAmbiente_id int NOT NULL,    --  Identificador do Meio Ambiente
	tipo int,    --  Tipo: LP - Licen�a Pr�via, LI - Licen�a de Instala��o, LO - Licen�a de Opera��o, AAF - Autoriza��o Ambiental de Funcionamento Outros
);

CREATE TABLE FaturamentoAnterior (
	id int identity(1,1)  NOT NULL,    --  Identificador do faturamento anterior
	financeiro_id int NOT NULL,    --  Identificador do financeiro
	tipo int NOT NULL,    --  Tipo do faturamento
	ano int NOT NULL,    --  Ano
	valor decimal(16,2)    --  Valor Faturamento
);

CREATE TABLE FaturamentoPrevisto (
	id int identity(1,1)  NOT NULL,    --  Identificador do faturamento previsto
	financeiro_id int NOT NULL,    --  Identificador do Financeiro
	tipo int NOT NULL,    --  Tipo do faturamento
	ano int NOT NULL,    --  Ano
	valor decimal(16,2)    --  Valor do faturamento previsto
);

CREATE TABLE Financeiro (
	id int identity(1,1)  NOT NULL,    --  Identificador do Financeiro
	projeto_id int NOT NULL,    --  Identificador do projeto
	observacao text,    --  Observa��o
	usuarioResponsavel_id int NOT NULL,    --  Usu�rio respons�vel pelas informa��es
	dataUltimaAlteracao datetime NOT NULL    --  Data da �ltima altera��o
);

CREATE TABLE FinanceiroAnexo (
	financeiro_id int NOT NULL,    --  Identificador do financeiro
	anexo_id int NOT NULL    --  Identificador do anexo
);

CREATE TABLE Insumo (
	id int identity(1,1)  NOT NULL,    --  Identificador do insumo
	insumoProduto_id int NOT NULL,    --  Identificador do insumo e produto
	nome varchar(200),    --  Nome do insumo
	ncm_id int,    --  C�digo do NCM
	origem int,    --  Origem:
	quantidadeAno int,    --  Quantidade Ano
	unidadeMedida varchar(50),    --  Unidade de Medida
	valorEstimadoAno decimal(10,2),    --  Valor Estimado Ano
	empresaFornecedor varchar(255)    --  Quantidade de empresas fornecedoras
);

CREATE TABLE InsumoProduto (
	id int identity(1,1)  NOT NULL,    --  Identificador dos insumos e produtos
	projeto_id int NOT NULL,    --  Identificador do projeto
	aquisicaoOutroEstado bit NOT NULL,    --  Produto adquirido em outro estado?
	interesseParceria bit,    --  Interesse em parceria?
	importacaoProduto bit,    --  Haver� importa��o de produto?
	contribuinteSubstituto bit,    --  Inscri��o informada de contribuinte substituto?
	percentualOrigemMinas decimal(5,2),    --  Porcentagem de Origem de Todos os Insumos em Minas Gerais
	percentualOrigemOutros decimal(5,2),    --  Porcentagem de Origem de Todos os Insumos em outros estados
	percentualOrigemImportado decimal(5,2),    --  Porcentagem de Origem de Todos os Insumos em Importados
	percentualFabricaMinas decimal(5,2),    --  Percentual de produto fabricado em Minas
	percentualAdquiridoOutro decimal(5,2),    --  Percentual de produto adquirido em outro estado
	percentualImportado decimal(5,2),    --  Percentual de produto importado
	percentualValorAgregado decimal(5,2),    --  Percentual Valor Agregado
	percentualICMS decimal(5,2),    --  Percentual Carga Tribut�ria de ICMS
	percentualClienteIndustria decimal(5,2),    --  Percentual CLiente Industria
	percentualClienteComercial decimal(5,2),    --  Percentual de Cliente Comercial
	percentualClienteConsumidor decimal(5,2),    --  Percentual de Cliente Consumidor
	percentualMercadoMinas decimal(5,2),    --  Percentual mercado Minas
	percentualMercadoNacional decimal(5,2),    --  Percentual Mercado Nacional
	percentualMercadoExterior decimal(5,2),    --  Percentual Mercado Exterior
	percentualTributoEfetivo decimal(5,2),    --  Percentual Carga Tribut�ria Efetiva
	usuarioResponsavel_id int NOT NULL,    --  Usu�rio respons�vel pelas informa��es
	dataUltimaAlteracao datetime NOT NULL    --  Data da �ltima altera��o
);

CREATE TABLE InvestimentoPrevisto (
	id int identity(1,1)  NOT NULL,    --  Identificador do investimento
	financeiro_id int NOT NULL,    --  Identificador do financeiro
	tipo int NOT NULL,    --  Tipo do investimento
	ano int NOT NULL,    --  Ano
	valor decimal(16,2)    --  Valor
);

CREATE TABLE Local (
	id int NOT NULL,    --  C�digo do Local
	descricao varchar(255) NOT NULL    --  Descri��o do local

);

CREATE TABLE MeioAmbiente (
	id int identity(1,1)  NOT NULL,    --  Identificador do meio ambiente
	projeto_id int NOT NULL,    --  Identificador do projeto
	licenciamentoIniciado bit NOT NULL,    --  Licenciamento j� foi iniciado?
	necessidadeEIARIMA bit,    --  Necessidade EIA RIMA?
	realizadaVistoria bit,    --  Foi realizada a vistoria?
	pedidoInformacoesComplementar bit,    --  Foi feito pedido de informa��es complementares?
	dataEntregaEstudos datetime,    --  Data da entrega dos estudos
	dataRealizadaVistoria datetime,    --  Data Realizada Vistoria
	dataPedidoInformacao datetime,    --  Data Pedido de Informa��o
	dataEntregaInformacao datetime,    --  Data da entrega das informa��es complementares
	classeEmpreendimento int,    --  Classe do empreendimento
	observacao text,    --  Observa��o
	usuarioResponsavel_id int NOT NULL,    --  Identificador do usu�rio respons�vel pelas informa��es
	dataUltimaAlteracao datetime NOT NULL    --  Data da �ltima altera��o
);

CREATE TABLE MeioAmbienteAnexo (
	meioAmbiente_id int NOT NULL,    --  Identificador do meio ambiente
	anexo_id int NOT NULL    --  Identificador do Anexo
);

CREATE TABLE NCM (
	id int NOT NULL,    --  Identificador do NCM
	descricao varchar(255) NOT NULL    --  Descri��o do NCM
);

CREATE TABLE OrigemRecurso (
	id int identity(1,1)  NOT NULL,    --  Identificador da origem
	financeiro_id int NOT NULL,    --  Identificador da origem
	origemNacional bit NOT NULL,    --  Origem Nacional?
	uf_id int,    --  Identificador da unidade federativa
	pais_id int,    --  Identificador do pa�s
	percentualInvestimento decimal(5,2),    --  Percentual do investimento por origem
	valorInvestimento decimal(16,2)    --  Valor do Investimento por origem
);

CREATE TABLE Parceria (
	id int identity(1,1)  NOT NULL,    --  Identificador da parceria
	insumoProduto_id int NOT NULL,    --  Identificador do insumo e produto
	descricao varchar(200)    --  Descri��o da parceria
);

CREATE TABLE Producao (
	id int identity(1,1)  NOT NULL,    --  Identificador da Produ��o
	insumoProduto_id int NOT NULL,    --  Identificador do insumo e produto
	ano int,    --  Ano
	areaPlantada decimal(10,2),    --  �rea plantada
	moagemCana decimal(10,2),    --  Moagem de Cana
	producaoEtanol decimal(10,2),    --  Produ��o de Etanol
	producaoAcucar decimal(10,2),    --  Produ��o de A�ucar
	geracaoEnergia decimal(10,2),    --  Gera��o de Energia
	outro decimal(10,2)    --  Outros produtos
);

CREATE TABLE Produto (
	id int identity(1,1)  NOT NULL,    --  Identificador do produto
	insumoProduto_id int NOT NULL,    --  Identificador do insumo e produto
	nome varchar(200),    --  Nome do Produto
	ncm_id int,    --  C�digo do NCM
	sujeitoST bit,    --  O produto est� sujeito a ST?
	quantidade1 int,    --  Quantidade Ano1
	quantidade2 int,    --  Quantidade Ano 2
	quantidade3 int,    --  Quantidade Ano 3
	unidadeMedida varchar(50),    --  Unidade de Medida
	tipo int NOT NULL    --  Tipo do registro
);

CREATE TABLE Publicacao (
	id int identity(1,1)  NOT NULL,    --  Identificador da Publicacao
	instrumentoFormalizacao_id int NOT NULL,    --  Identificador do instrumento de fomrmaliza��o
	dataPublicacao datetime NOT NULL,    --  Data da publica��o
	usuarioResponsavel_id int NOT NULL,    --  Usu�rio respons�vel pelas informa��es
	dataUltimaAlteracao datetime NOT NULL    --  Data da �ltima altera��o
);

CREATE TABLE PublicacaoAnexo (
	publicacao_id int NOT NULL,    --  Identificador da publicacao
	anexo_id int NOT NULL   --  Identificador do anexo
);

CREATE TABLE Servico (
	id int identity(1,1)  NOT NULL,    --  Identificador do Servi�o
	insumoProduto_id int NOT NULL,    --  Identificador do insumo e produto
	descricao varchar(200)    --  Descri��o do Servi�o
);

CREATE TABLE Template (
	id int identity(1,1)  NOT NULL,    --  Identificador do template
	nome varchar(100),    --  Nome
	tipo int NOT NULL,    --  Tipo do template: Projeto, Protocolo de inten��es e termo aditivo
	status bit NOT NULL,    --  Status: Ativo ou Inativo
	usuarioResponsavel_id int NOT NULL,    --  Identificador do usu�rio respons�vel pelas altera��es
	dataUltimaAlteracao datetime NOT NULL    --  Data da �ltima altera��o
);

CREATE TABLE UsoFonte (
	id int identity(1,1)  NOT NULL,    --  Identificador do uso e fontes
	financeiro_id int NOT NULL,    --  Identificador do financeiro
	tipo int NOT NULL,    --  Tipo
	valorRealizado decimal(16,2),    --  Valor realizado
	valorARealizar decimal(16,2)    --  Valor a realizar
);

/*
EXEC sp_addextendedproperty 'MS_Description', 'Atividades de acompanhamneto do instrumento', 'Schema', dbo, 'table', AtividadeInstrumento;
EXEC sp_addextendedproperty 'MS_Description', 'Identificador da atividade', 'Schema', dbo, 'table', AtividadeInstrumento, 'column', id;
EXEC sp_addextendedproperty 'MS_Description', 'Identificador do instrumento de formaliza��o', 'Schema', dbo, 'table', AtividadeInstrumento, 'column', instrumentoFormalizacao_id;
EXEC sp_addextendedproperty 'MS_Description', 'C�digo da Atividade referente ao local', 'Schema', dbo, 'table', AtividadeInstrumento, 'column', atividadeLocal_id;
EXEC sp_addextendedproperty 'MS_Description', 'Posicionamento da atividade, ordena��o', 'Schema', dbo, 'table', AtividadeInstrumento, 'column', posicao;
EXEC sp_addextendedproperty 'MS_Description', 'Identificador do anexo de evidencia da atividade', 'Schema', dbo, 'table', AtividadeInstrumento, 'column', anexo_id;
EXEC sp_addextendedproperty 'MS_Description', 'Data de in�cio da atividade', 'Schema', dbo, 'table', AtividadeInstrumento, 'column', dataInicio;
EXEC sp_addextendedproperty 'MS_Description', 'Data de Conclus�o da atividade', 'Schema', dbo, 'table', AtividadeInstrumento, 'column', dataConclusao;
EXEC sp_addextendedproperty 'MS_Description', 'Observa��o da atividade', 'Schema', dbo, 'table', AtividadeInstrumento, 'column', observacao;
EXEC sp_addextendedproperty 'MS_Description', '� uma atividade paralela?', 'Schema', dbo, 'table', AtividadeInstrumento, 'column', paralela;
EXEC sp_addextendedproperty 'MS_Description', 'Bloqueia o cadastro do ICE?', 'Schema', dbo, 'table', AtividadeInstrumento, 'column', bloqueiaICE;
EXEC sp_addextendedproperty 'MS_Description', 'Inicia o est�gio de DF?', 'Schema', dbo, 'table', AtividadeInstrumento, 'column', df;
EXEC sp_addextendedproperty 'MS_Description', 'Inicia o est�gio de II?', 'Schema', dbo, 'table', AtividadeInstrumento, 'column', ii;
EXEC sp_addextendedproperty 'MS_Description', 'Inicia o est�gio de OI?', 'Schema', dbo, 'table', AtividadeInstrumento, 'column', oi;
EXEC sp_addextendedproperty 'MS_Description', 'Inicia o est�gio de CC?', 'Schema', dbo, 'table', AtividadeInstrumento, 'column', cc;
EXEC sp_addextendedproperty 'MS_Description', 'Usu�rio respons�vel pelas informa��es', 'Schema', dbo, 'table', AtividadeInstrumento, 'column', usuarioResponsavel_id;
EXEC sp_addextendedproperty 'MS_Description', 'Data da �ltima altera��o', 'Schema', dbo, 'table', AtividadeInstrumento, 'column', dataUltimaAlteracao;
EXEC sp_addextendedproperty 'MS_Description', 'Conjunto de atividades pr�-definidas para uso em atividades e templates.', 'Schema', dbo, 'table', AtividadeLocal;
EXEC sp_addextendedproperty 'MS_Description', 'C�digo da Atividade de Local', 'Schema', dbo, 'table', AtividadeLocal, 'column', id;
EXEC sp_addextendedproperty 'MS_Description', 'C�digo do local', 'Schema', dbo, 'table', AtividadeLocal, 'column', local_id;
EXEC sp_addextendedproperty 'MS_Description', 'Descri��o da atividade', 'Schema', dbo, 'table', AtividadeLocal, 'column', descricao;
EXEC sp_addextendedproperty 'MS_Description', 'Atividades de acompanhamento do projeto', 'Schema', dbo, 'table', AtividadeProjeto;
EXEC sp_addextendedproperty 'MS_Description', 'Identificador da atividade de projeto', 'Schema', dbo, 'table', AtividadeProjeto, 'column', id;
EXEC sp_addextendedproperty 'MS_Description', 'Identificador do projeto', 'Schema', dbo, 'table', AtividadeProjeto, 'column', projeto_id;
EXEC sp_addextendedproperty 'MS_Description', 'Identificador da atividade referente ao local', 'Schema', dbo, 'table', AtividadeProjeto, 'column', atividadeLocal_id;
EXEC sp_addextendedproperty 'MS_Description', 'Posi��o da atividade, ordena��o', 'Schema', dbo, 'table', AtividadeProjeto, 'column', posicao;
EXEC sp_addextendedproperty 'MS_Description', 'Arquivo de evid�ncia da atividade', 'Schema', dbo, 'table', AtividadeProjeto, 'column', anexo_id;
EXEC sp_addextendedproperty 'MS_Description', 'Data de conclus�o da atividade', 'Schema', dbo, 'table', AtividadeProjeto, 'column', dataConclusao;
EXEC sp_addextendedproperty 'MS_Description', 'Observa��o da atividade', 'Schema', dbo, 'table', AtividadeProjeto, 'column', observacao;
EXEC sp_addextendedproperty 'MS_Description', '� obrigat�rio?', 'Schema', dbo, 'table', AtividadeProjeto, 'column', obrigatorio;
EXEC sp_addextendedproperty 'MS_Description', 'Bloqueia o ICE?', 'Schema', dbo, 'table', AtividadeProjeto, 'column', bloqueiaICE;
EXEC sp_addextendedproperty 'MS_Description', 'Inicia o est�gio DF?', 'Schema', dbo, 'table', AtividadeProjeto, 'column', df;
EXEC sp_addextendedproperty 'MS_Description', 'Inicia o est�gio II?', 'Schema', dbo, 'table', AtividadeProjeto, 'column', ii;
EXEC sp_addextendedproperty 'MS_Description', 'Inicia o est�gio OI?', 'Schema', dbo, 'table', AtividadeProjeto, 'column', oi;
EXEC sp_addextendedproperty 'MS_Description', 'Inicia o est�gio CC?', 'Schema', dbo, 'table', AtividadeProjeto, 'column', cc;
EXEC sp_addextendedproperty 'MS_Description', 'Usu�rio responsavel pelas informa��es', 'Schema', dbo, 'table', AtividadeProjeto, 'column', usuarioResponsavel_id;
EXEC sp_addextendedproperty 'MS_Description', 'Data da �ltima altera��o', 'Schema', dbo, 'table', AtividadeProjeto, 'column', dataUltimaAlteracao;
EXEC sp_addextendedproperty 'MS_Description', 'Atividades do template', 'Schema', dbo, 'table', AtividadeTemplate;
EXEC sp_addextendedproperty 'MS_Description', 'Identificador da atividade', 'Schema', dbo, 'table', AtividadeTemplate, 'column', id;
EXEC sp_addextendedproperty 'MS_Description', 'C�digo da atividade referente ao local', 'Schema', dbo, 'table', AtividadeTemplate, 'column', atividadeLocal_id;
EXEC sp_addextendedproperty 'MS_Description', 'Identificador do template', 'Schema', dbo, 'table', AtividadeTemplate, 'column', template_id;
EXEC sp_addextendedproperty 'MS_Description', 'Posi��o da atividade, referente a ordena��o', 'Schema', dbo, 'table', AtividadeTemplate, 'column', posicao;
EXEC sp_addextendedproperty 'MS_Description', '� obrigat�rio?', 'Schema', dbo, 'table', AtividadeTemplate, 'column', obrigatorio;
EXEC sp_addextendedproperty 'MS_Description', 'Bloqueia o cadastro do ICE?', 'Schema', dbo, 'table', AtividadeTemplate, 'column', bloqueiaICE;
EXEC sp_addextendedproperty 'MS_Description', 'Inicia o est�gio DF?', 'Schema', dbo, 'table', AtividadeTemplate, 'column', df;
EXEC sp_addextendedproperty 'MS_Description', 'Inicia o est�gio II?', 'Schema', dbo, 'table', AtividadeTemplate, 'column', ii;
EXEC sp_addextendedproperty 'MS_Description', 'Inicia o est�gio OI?', 'Schema', dbo, 'table', AtividadeTemplate, 'column', oi;
EXEC sp_addextendedproperty 'MS_Description', 'Inicia o est�gio CC?', 'Schema', dbo, 'table', AtividadeTemplate, 'column', cc;
EXEC sp_addextendedproperty 'MS_Description', 'Concorrentes', 'Schema', dbo, 'table', Concorrente;
EXEC sp_addextendedproperty 'MS_Description', 'identificador do concorrente', 'Schema', dbo, 'table', Concorrente, 'column', id;
EXEC sp_addextendedproperty 'MS_Description', 'Identificador do insumo e produto', 'Schema', dbo, 'table', Concorrente, 'column', insumoProduto_id;
EXEC sp_addextendedproperty 'MS_Description', 'Nome do concorrente', 'Schema', dbo, 'table', Concorrente, 'column', nome;
EXEC sp_addextendedproperty 'MS_Description', 'Nome do produto', 'Schema', dbo, 'table', Concorrente, 'column', descricaoProduto;
EXEC sp_addextendedproperty 'MS_Description', 'Etapas referentes ao meio ambiente', 'Schema', dbo, 'table', EtapaMeioAmbiente;
EXEC sp_addextendedproperty 'MS_Description', 'Identificador das Etapas', 'Schema', dbo, 'table', EtapaMeioAmbiente, 'column', id;
EXEC sp_addextendedproperty 'MS_Description', 'Identificador do Meio Ambiente', 'Schema', dbo, 'table', EtapaMeioAmbiente, 'column', meioAmbiente_id;
EXEC sp_addextendedproperty 'MS_Description', 'Tipo: LP - Licen�a Pr�via, LI - Licen�a de Instala��o, LO - Licen�a de Opera��o, AAF - Autoriza��o Ambiental de Funcionamento, Outros', 'Schema', dbo, 'table', EtapaMeioAmbiente, 'column', tipo;
EXEC sp_addextendedproperty 'MS_Description', 'Faturamento Anterior', 'Schema', dbo, 'table', FaturamentoAnterior;
EXEC sp_addextendedproperty 'MS_Description', 'Identificador do faturamento anterior', 'Schema', dbo, 'table', FaturamentoAnterior, 'column', id;
EXEC sp_addextendedproperty 'MS_Description', 'Identificador do financeiro', 'Schema', dbo, 'table', FaturamentoAnterior, 'column', financeiro_id;
EXEC sp_addextendedproperty 'MS_Description', 'Tipo do faturamento', 'Schema', dbo, 'table', FaturamentoAnterior, 'column', tipo;
EXEC sp_addextendedproperty 'MS_Description', 'Ano', 'Schema', dbo, 'table', FaturamentoAnterior, 'column', ano;
EXEC sp_addextendedproperty 'MS_Description', 'Valor Faturamento', 'Schema', dbo, 'table', FaturamentoAnterior, 'column', valor;
EXEC sp_addextendedproperty 'MS_Description', 'Faturamento Previsto', 'Schema', dbo, 'table', FaturamentoPrevisto;
EXEC sp_addextendedproperty 'MS_Description', 'Identificador do faturamento previsto', 'Schema', dbo, 'table', FaturamentoPrevisto, 'column', id;
EXEC sp_addextendedproperty 'MS_Description', 'Identificador do Financeiro', 'Schema', dbo, 'table', FaturamentoPrevisto, 'column', financeiro_id;
EXEC sp_addextendedproperty 'MS_Description', 'Tipo do faturamento', 'Schema', dbo, 'table', FaturamentoPrevisto, 'column', tipo;
EXEC sp_addextendedproperty 'MS_Description', 'Ano', 'Schema', dbo, 'table', FaturamentoPrevisto, 'column', ano;
EXEC sp_addextendedproperty 'MS_Description', 'Valor do faturamento previsto', 'Schema', dbo, 'table', FaturamentoPrevisto, 'column', valor;
EXEC sp_addextendedproperty 'MS_Description', 'Informa��es financeiras do projeto', 'Schema', dbo, 'table', Financeiro;
EXEC sp_addextendedproperty 'MS_Description', 'Identificador do Financeiro', 'Schema', dbo, 'table', Financeiro, 'column', id;
EXEC sp_addextendedproperty 'MS_Description', 'Identificador do projeto', 'Schema', dbo, 'table', Financeiro, 'column', projeto_id;
EXEC sp_addextendedproperty 'MS_Description', 'Observa��o', 'Schema', dbo, 'table', Financeiro, 'column', observacao;
EXEC sp_addextendedproperty 'MS_Description', 'Usu�rio respons�vel pelas informa��es', 'Schema', dbo, 'table', Financeiro, 'column', usuarioResponsavel_id;
EXEC sp_addextendedproperty 'MS_Description', 'Data da �ltima altera��o', 'Schema', dbo, 'table', Financeiro, 'column', dataUltimaAlteracao;
EXEC sp_addextendedproperty 'MS_Description', 'Anexos relacionados ao financeiro', 'Schema', dbo, 'table', FinanceiroAnexo;
EXEC sp_addextendedproperty 'MS_Description', 'Identificador do financeiro', 'Schema', dbo, 'table', FinanceiroAnexo, 'column', financeiro_id;
EXEC sp_addextendedproperty 'MS_Description', 'Identificador do anexo', 'Schema', dbo, 'table', FinanceiroAnexo, 'column', anexo_id;
EXEC sp_addextendedproperty 'MS_Description', 'Insumos', 'Schema', dbo, 'table', Insumo;
EXEC sp_addextendedproperty 'MS_Description', 'Identificador do insumo', 'Schema', dbo, 'table', Insumo, 'column', id;
EXEC sp_addextendedproperty 'MS_Description', 'Identificador do insumo e produto', 'Schema', dbo, 'table', Insumo, 'column', insumoProduto_id;
EXEC sp_addextendedproperty 'MS_Description', 'Nome do insumo', 'Schema', dbo, 'table', Insumo, 'column', nome;
EXEC sp_addextendedproperty 'MS_Description', 'C�digo do NCM', 'Schema', dbo, 'table', Insumo, 'column', ncm_id;
EXEC sp_addextendedproperty 'MS_Description', 'Origem:', 'Schema', dbo, 'table', Insumo, 'column', origem;
EXEC sp_addextendedproperty 'MS_Description', 'Quantidade Ano', 'Schema', dbo, 'table', Insumo, 'column', quantidadeAno;
EXEC sp_addextendedproperty 'MS_Description', 'Unidade de Medida', 'Schema', dbo, 'table', Insumo, 'column', unidadeMedida;
EXEC sp_addextendedproperty 'MS_Description', 'Valor Estimado Ano', 'Schema', dbo, 'table', Insumo, 'column', valorEstimadoAno;
EXEC sp_addextendedproperty 'MS_Description', 'Quantidade de empresas fornecedoras', 'Schema', dbo, 'table', Insumo, 'column', quantidadeEmpresaFornecedora;
EXEC sp_addextendedproperty 'MS_Description', 'Insumos e Produtos do projeto', 'Schema', dbo, 'table', InsumoProduto;
EXEC sp_addextendedproperty 'MS_Description', 'Identificador dos insumos e produtos', 'Schema', dbo, 'table', InsumoProduto, 'column', id;
EXEC sp_addextendedproperty 'MS_Description', 'Identificador do projeto', 'Schema', dbo, 'table', InsumoProduto, 'column', projeto_id;
EXEC sp_addextendedproperty 'MS_Description', 'Produto adquirido em outro estado?', 'Schema', dbo, 'table', InsumoProduto, 'column', aquisicaoOutroEstado;
EXEC sp_addextendedproperty 'MS_Description', 'Interesse em parceria?', 'Schema', dbo, 'table', InsumoProduto, 'column', interesseParceria;
EXEC sp_addextendedproperty 'MS_Description', 'Haver� importa��o de produto?', 'Schema', dbo, 'table', InsumoProduto, 'column', importacaoProduto;
EXEC sp_addextendedproperty 'MS_Description', 'Inscri��o informada de contribuinte substituto?', 'Schema', dbo, 'table', InsumoProduto, 'column', contribuinteSubstituto;
EXEC sp_addextendedproperty 'MS_Description', 'Porcentagem de Origem de Todos os Insumos em Minas Gerais', 'Schema', dbo, 'table', InsumoProduto, 'column', percentualOrigemMinas;
EXEC sp_addextendedproperty 'MS_Description', 'Porcentagem de Origem de Todos os Insumos em outros estados', 'Schema', dbo, 'table', InsumoProduto, 'column', percentualOrigemOutros;
EXEC sp_addextendedproperty 'MS_Description', 'Porcentagem de Origem de Todos os Insumos em Importados', 'Schema', dbo, 'table', InsumoProduto, 'column', percentualOrigermImportado;
EXEC sp_addextendedproperty 'MS_Description', 'Percentual de produto fabricado em Minas', 'Schema', dbo, 'table', InsumoProduto, 'column', percentualFabricaMinas;
EXEC sp_addextendedproperty 'MS_Description', 'Percentual de produto adquirido em outro estado', 'Schema', dbo, 'table', InsumoProduto, 'column', percentualAdquiridoOutro;
EXEC sp_addextendedproperty 'MS_Description', 'Percentual de produto importado', 'Schema', dbo, 'table', InsumoProduto, 'column', percentualImportado;
EXEC sp_addextendedproperty 'MS_Description', 'Percentual Valor Agregado', 'Schema', dbo, 'table', InsumoProduto, 'column', percentualValorAgregado;
EXEC sp_addextendedproperty 'MS_Description', 'Percentual Carga Tribut�ria de ICMS', 'Schema', dbo, 'table', InsumoProduto, 'column', percentualICMS;
EXEC sp_addextendedproperty 'MS_Description', 'Percentual CLiente Industria', 'Schema', dbo, 'table', InsumoProduto, 'column', percentualClienteIndustria;
EXEC sp_addextendedproperty 'MS_Description', 'Percentual de Cliente Comercial', 'Schema', dbo, 'table', InsumoProduto, 'column', percentualClienteComercial;
EXEC sp_addextendedproperty 'MS_Description', 'Percentual de Cliente Consumidor', 'Schema', dbo, 'table', InsumoProduto, 'column', percentualClienteConsumidor;
EXEC sp_addextendedproperty 'MS_Description', 'Percentual mercado Minas', 'Schema', dbo, 'table', InsumoProduto, 'column', percentualMercadoMinas;
EXEC sp_addextendedproperty 'MS_Description', 'Percentual Mercado Nacional', 'Schema', dbo, 'table', InsumoProduto, 'column', percentualMercadoNacional;
EXEC sp_addextendedproperty 'MS_Description', 'Percentual Mercado Exterior', 'Schema', dbo, 'table', InsumoProduto, 'column', percentualMercadoExterior;
EXEC sp_addextendedproperty 'MS_Description', 'Percentual Carga Tribut�ria Efetiva', 'Schema', dbo, 'table', InsumoProduto, 'column', percentualTributoEfetivo;
EXEC sp_addextendedproperty 'MS_Description', 'Usu�rio respons�vel pelas informa��es', 'Schema', dbo, 'table', InsumoProduto, 'column', usuarioResponsavel_id;
EXEC sp_addextendedproperty 'MS_Description', 'Data da �ltima altera��o', 'Schema', dbo, 'table', InsumoProduto, 'column', dataUltimaAlteracao;
EXEC sp_addextendedproperty 'MS_Description', 'Investimento Previsto', 'Schema', dbo, 'table', InvestimentoPrevisto;
EXEC sp_addextendedproperty 'MS_Description', 'Identificador do investimento', 'Schema', dbo, 'table', InvestimentoPrevisto, 'column', id;
EXEC sp_addextendedproperty 'MS_Description', 'Identificador do financeiro', 'Schema', dbo, 'table', InvestimentoPrevisto, 'column', financeiro_id;
EXEC sp_addextendedproperty 'MS_Description', 'Tipo do investimento', 'Schema', dbo, 'table', InvestimentoPrevisto, 'column', tipo;
EXEC sp_addextendedproperty 'MS_Description', 'Ano', 'Schema', dbo, 'table', InvestimentoPrevisto, 'column', ano;
EXEC sp_addextendedproperty 'MS_Description', 'Valor', 'Schema', dbo, 'table', InvestimentoPrevisto, 'column', valor;
EXEC sp_addextendedproperty 'MS_Description', 'Conjunto de locais pr�-definidos para uso em atividades e template', 'Schema', dbo, 'table', Local;
EXEC sp_addextendedproperty 'MS_Description', 'C�digo do Local', 'Schema', dbo, 'table', Local, 'column', id;
EXEC sp_addextendedproperty 'MS_Description', 'Descri��o do local', 'Schema', dbo, 'table', Local, 'column', descricao;
EXEC sp_addextendedproperty 'MS_Description', 'Informa��es sobre o meio ambiente e licenciamento', 'Schema', dbo, 'table', MeioAmbiente;
EXEC sp_addextendedproperty 'MS_Description', 'Identificador do meio ambiente', 'Schema', dbo, 'table', MeioAmbiente, 'column', id;
EXEC sp_addextendedproperty 'MS_Description', 'Identificador do projeto', 'Schema', dbo, 'table', MeioAmbiente, 'column', projeto_id;
EXEC sp_addextendedproperty 'MS_Description', 'Licenciamento j� foi iniciado?', 'Schema', dbo, 'table', MeioAmbiente, 'column', licenciamentoIniciado;
EXEC sp_addextendedproperty 'MS_Description', 'Necessidade EIA RIMA?', 'Schema', dbo, 'table', MeioAmbiente, 'column', necessidadeEIARIMA;
EXEC sp_addextendedproperty 'MS_Description', 'Foi realizada a vistoria?', 'Schema', dbo, 'table', MeioAmbiente, 'column', realizadaVistoria;
EXEC sp_addextendedproperty 'MS_Description', 'Foi feito pedido de informa��es complementares?', 'Schema', dbo, 'table', MeioAmbiente, 'column', pedidoInformacoesComplementar;
EXEC sp_addextendedproperty 'MS_Description', 'Data da entrega dos estudos', 'Schema', dbo, 'table', MeioAmbiente, 'column', dataEntregaEstudos;
EXEC sp_addextendedproperty 'MS_Description', 'Data em que entrou fora do prazo', 'Schema', dbo, 'table', MeioAmbiente, 'column', dataEntradaForaPrazo;
EXEC sp_addextendedproperty 'MS_Description', 'Data Realizada Vistoria', 'Schema', dbo, 'table', MeioAmbiente, 'column', dataRealizadaVistoria;
EXEC sp_addextendedproperty 'MS_Description', 'Data Pedido de Informa��o', 'Schema', dbo, 'table', MeioAmbiente, 'column', dataPedidoInformacao;
EXEC sp_addextendedproperty 'MS_Description', 'Data da entrega das informa��es complementares', 'Schema', dbo, 'table', MeioAmbiente, 'column', dataEntregaInformacao;
EXEC sp_addextendedproperty 'MS_Description', 'Classe do empreendimento', 'Schema', dbo, 'table', MeioAmbiente, 'column', classeEmpreendimento;
EXEC sp_addextendedproperty 'MS_Description', 'Observa��o', 'Schema', dbo, 'table', MeioAmbiente, 'column', observacao;
EXEC sp_addextendedproperty 'MS_Description', 'Identificador do usu�rio respons�vel pelas informa��es', 'Schema', dbo, 'table', MeioAmbiente, 'column', usuarioResponsavel_id;
EXEC sp_addextendedproperty 'MS_Description', 'Data da �ltima altera��o', 'Schema', dbo, 'table', MeioAmbiente, 'column', dataUltimaAlteracao;
EXEC sp_addextendedproperty 'MS_Description', 'Relacionamento entre Meio Ambiente e Anexos', 'Schema', dbo, 'table', MeioAmbienteAnexo;
EXEC sp_addextendedproperty 'MS_Description', 'Identificador do meio ambiente', 'Schema', dbo, 'table', MeioAmbienteAnexo, 'column', meioAmbiente_id;
EXEC sp_addextendedproperty 'MS_Description', 'Identificador do Anexo', 'Schema', dbo, 'table', MeioAmbienteAnexo, 'column', anexo_id;
EXEC sp_addextendedproperty 'MS_Description', 'Tabela de classifica��o NCM', 'Schema', dbo, 'table', NCM;
EXEC sp_addextendedproperty 'MS_Description', 'Identificador do NCM', 'Schema', dbo, 'table', NCM, 'column', id;
EXEC sp_addextendedproperty 'MS_Description', 'Descri��o do NCM', 'Schema', dbo, 'table', NCM, 'column', descricao;
EXEC sp_addextendedproperty 'MS_Description', 'Origem dos recursos', 'Schema', dbo, 'table', OrigemRecurso;
EXEC sp_addextendedproperty 'MS_Description', 'Identificador da origem', 'Schema', dbo, 'table', OrigemRecurso, 'column', id;
EXEC sp_addextendedproperty 'MS_Description', 'Identificador da origem', 'Schema', dbo, 'table', OrigemRecurso, 'column', financeiro_id;
EXEC sp_addextendedproperty 'MS_Description', 'Origem Nacional?', 'Schema', dbo, 'table', OrigemRecurso, 'column', origemNacional;
EXEC sp_addextendedproperty 'MS_Description', 'Identificador da unidade federativa', 'Schema', dbo, 'table', OrigemRecurso, 'column', uf_id;
EXEC sp_addextendedproperty 'MS_Description', 'Identificador do pa�s', 'Schema', dbo, 'table', OrigemRecurso, 'column', pais_id;
EXEC sp_addextendedproperty 'MS_Description', 'Percentual do investimento por origem', 'Schema', dbo, 'table', OrigemRecurso, 'column', percentualInvestimento;
EXEC sp_addextendedproperty 'MS_Description', 'Valor do Investimento por origem', 'Schema', dbo, 'table', OrigemRecurso, 'column', valorInvestimento;
EXEC sp_addextendedproperty 'MS_Description', 'Parceria', 'Schema', dbo, 'table', Parceria;
EXEC sp_addextendedproperty 'MS_Description', 'Identificador da parceria', 'Schema', dbo, 'table', Parceria, 'column', id;
EXEC sp_addextendedproperty 'MS_Description', 'Identificador do insumo e produto', 'Schema', dbo, 'table', Parceria, 'column', insumoProduto_id;
EXEC sp_addextendedproperty 'MS_Description', 'Descri��o da parceria', 'Schema', dbo, 'table', Parceria, 'column', descricao;
EXEC sp_addextendedproperty 'MS_Description', 'Produ��o', 'Schema', dbo, 'table', Producao;
EXEC sp_addextendedproperty 'MS_Description', 'Identificador da Produ��o', 'Schema', dbo, 'table', Producao, 'column', id;
EXEC sp_addextendedproperty 'MS_Description', 'Identificador do insumo e produto', 'Schema', dbo, 'table', Producao, 'column', insumoProduto;
EXEC sp_addextendedproperty 'MS_Description', 'Ano', 'Schema', dbo, 'table', Producao, 'column', ano;
EXEC sp_addextendedproperty 'MS_Description', '�rea plantada', 'Schema', dbo, 'table', Producao, 'column', areaPlantada;
EXEC sp_addextendedproperty 'MS_Description', 'Moagem de Cana', 'Schema', dbo, 'table', Producao, 'column', moagemCana;
EXEC sp_addextendedproperty 'MS_Description', 'Produ��o de Etanol', 'Schema', dbo, 'table', Producao, 'column', producaoEtanol;
EXEC sp_addextendedproperty 'MS_Description', 'Produ��o de A�ucar', 'Schema', dbo, 'table', Producao, 'column', producaoAcucar;
EXEC sp_addextendedproperty 'MS_Description', 'Gera��o de Energia', 'Schema', dbo, 'table', Producao, 'column', geracaoEnergia;
EXEC sp_addextendedproperty 'MS_Description', 'Outros produtos', 'Schema', dbo, 'table', Producao, 'column', outro;
EXEC sp_addextendedproperty 'MS_Description', 'Produtos', 'Schema', dbo, 'table', Produto;
EXEC sp_addextendedproperty 'MS_Description', 'Identificador do produto', 'Schema', dbo, 'table', Produto, 'column', id;
EXEC sp_addextendedproperty 'MS_Description', 'Identificador do insumo e produto', 'Schema', dbo, 'table', Produto, 'column', insumoProduto_id;
EXEC sp_addextendedproperty 'MS_Description', 'Nome do Produto', 'Schema', dbo, 'table', Produto, 'column', nome;
EXEC sp_addextendedproperty 'MS_Description', 'C�digo do NCM', 'Schema', dbo, 'table', Produto, 'column', ncm_id;
EXEC sp_addextendedproperty 'MS_Description', 'O produto est� sujeito a ST?', 'Schema', dbo, 'table', Produto, 'column', sujeitoST;
EXEC sp_addextendedproperty 'MS_Description', 'Quantidade Ano1', 'Schema', dbo, 'table', Produto, 'column', quantidade1;
EXEC sp_addextendedproperty 'MS_Description', 'Quantidade Ano 2', 'Schema', dbo, 'table', Produto, 'column', quantidade2;
EXEC sp_addextendedproperty 'MS_Description', 'Quantidade Ano 3', 'Schema', dbo, 'table', Produto, 'column', quantidade3;
EXEC sp_addextendedproperty 'MS_Description', 'Unidade de Medida', 'Schema', dbo, 'table', Produto, 'column', unidadeMedida;
EXEC sp_addextendedproperty 'MS_Description', 'Tipo do registro', 'Schema', dbo, 'table', Produto, 'column', tipo;
EXEC sp_addextendedproperty 'MS_Description', 'C�digo da familia de projetos para o versionamento', 'Schema', dbo, 'table', Projeto, 'column', familia_id;
EXEC sp_addextendedproperty 'MS_Description', 'Publica��o dos instrumentos de formaliza��o', 'Schema', dbo, 'table', Publicacao;
EXEC sp_addextendedproperty 'MS_Description', 'Identificador da Publicacao', 'Schema', dbo, 'table', Publicacao, 'column', id;
EXEC sp_addextendedproperty 'MS_Description', 'Identificador do instrumento de fomrmaliza��o', 'Schema', dbo, 'table', Publicacao, 'column', instrumentoFormalizacao_id;
EXEC sp_addextendedproperty 'MS_Description', 'Data da publica��o', 'Schema', dbo, 'table', Publicacao, 'column', dataPublicacao;
EXEC sp_addextendedproperty 'MS_Description', 'Usu�rio respons�vel pelas informa��es', 'Schema', dbo, 'table', Publicacao, 'column', usuarioResponsavel_id;
EXEC sp_addextendedproperty 'MS_Description', 'Data da �ltima altera��o', 'Schema', dbo, 'table', Publicacao, 'column', dataUltimaAlteracao;
EXEC sp_addextendedproperty 'MS_Description', 'Relacionamento entre a publica��o e seus anexos.', 'Schema', dbo, 'table', PublicacaoAnexo;
EXEC sp_addextendedproperty 'MS_Description', 'Identificador da publicacao', 'Schema', dbo, 'table', PublicacaoAnexo, 'column', publicacao_id;
EXEC sp_addextendedproperty 'MS_Description', 'Identificador do anexo', 'Schema', dbo, 'table', PublicacaoAnexo, 'column', anexo_id;
EXEC sp_addextendedproperty 'MS_Description', 'Servi�os', 'Schema', dbo, 'table', Servico;
EXEC sp_addextendedproperty 'MS_Description', 'Identificador do Servi�o', 'Schema', dbo, 'table', Servico, 'column', id;
EXEC sp_addextendedproperty 'MS_Description', 'Identificador do insumo e produto', 'Schema', dbo, 'table', Servico, 'column', insumoProduto_id;
EXEC sp_addextendedproperty 'MS_Description', 'Descri��o do Servi�o', 'Schema', dbo, 'table', Servico, 'column', descricao;
EXEC sp_addextendedproperty 'MS_Description', 'Template de Atividades de acompanhamento de projeto e instrumento de formaliza��o', 'Schema', dbo, 'table', Template;
EXEC sp_addextendedproperty 'MS_Description', 'Identificador do template', 'Schema', dbo, 'table', Template, 'column', id;
EXEC sp_addextendedproperty 'MS_Description', 'Nome', 'Schema', dbo, 'table', Template, 'column', nome;
EXEC sp_addextendedproperty 'MS_Description', 'Tipo do template: Projeto, Protocolo de inten��es e termo aditivo', 'Schema', dbo, 'table', Template, 'column', tipo;
EXEC sp_addextendedproperty 'MS_Description', 'Status: Ativo ou Inativo', 'Schema', dbo, 'table', Template, 'column', status;
EXEC sp_addextendedproperty 'MS_Description', 'Identificador do usu�rio respons�vel pelas altera��es', 'Schema', dbo, 'table', Template, 'column', usuarioResponsavel_id;
EXEC sp_addextendedproperty 'MS_Description', 'Data da �ltima altera��o', 'Schema', dbo, 'table', Template, 'column', dataUltimaAlteracao;
EXEC sp_addextendedproperty 'MS_Description', 'Quadro de uso e fontes financeiras', 'Schema', dbo, 'table', UsoFonte;
EXEC sp_addextendedproperty 'MS_Description', 'Identificador do uso e fontes', 'Schema', dbo, 'table', UsoFonte, 'column', id;
EXEC sp_addextendedproperty 'MS_Description', 'Identificador do financeiro', 'Schema', dbo, 'table', UsoFonte, 'column', financeiro_id;
EXEC sp_addextendedproperty 'MS_Description', 'Tipo', 'Schema', dbo, 'table', UsoFonte, 'column', tipo;
EXEC sp_addextendedproperty 'MS_Description', 'Valor realizado', 'Schema', dbo, 'table', UsoFonte, 'column', valorRealizado;
EXEC sp_addextendedproperty 'MS_Description', 'Valor a realizar', 'Schema', dbo, 'table', UsoFonte, 'column', valorARealizar;
*/
