-- Delete all PKs and FKs
-- Comment this section below to SQLServer and uncomment to H2

alter table Anexo  drop primary key;
alter table AnexoLocalizacao  drop primary key;
alter table InformacaoAdicional  drop primary key;
alter table Cronograma  drop primary key;
alter table Empresa  drop primary key;
alter table Endereco  drop primary key;
alter table EnderecoInternacional  drop primary key;
alter table Localizacao  drop primary key;
alter table NomeEmpresa  drop primary key;
alter table Pleito  drop primary key;
alter table Projeto  drop primary key;
alter table Emprego  drop primary key;
alter table Infraestrutura  drop primary key;
alter table EnergiaContratada  drop primary key;
alter table UnidadeEmpresa  drop primary key;
alter table Usuario  drop primary key;
alter table AccessToken  drop primary key;
alter table VerificationToken  drop primary key;
alter table HistoricoSituacaoProjeto  drop primary key;
alter table InstrumentoFormalizacao  drop primary key;
alter table Signatario  drop primary key;
alter table AtividadeInstrumento  drop primary key;
alter table AtividadeProjeto  drop primary key;
alter table AtividadeTemplate  drop primary key;
alter table Concorrente  drop primary key;
alter table EtapaMeioAmbiente  drop primary key;
alter table FaturamentoAnterior  drop primary key;
alter table FaturamentoPrevisto  drop primary key;
alter table Financeiro  drop primary key;
alter table Insumo  drop primary key;
alter table InsumoProduto  drop primary key;
alter table InvestimentoPrevisto  drop primary key;
alter table MeioAmbiente  drop primary key;
alter table OrigemRecurso  drop primary key;
alter table Parceria  drop primary key;
alter table Producao  drop primary key;
alter table Produto  drop primary key;
alter table Publicacao  drop primary key;
alter table Servico  drop primary key;
alter table Template  drop primary key;
alter table UsoFonte  drop primary key;
alter table Pesquisa drop primary key;
alter table HistoricoSituacaoInstrumentoFormalizacao drop primary key;
