IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.VIEWS
        WHERE TABLE_NAME = 'VIEW_UNIDADEEMPRESA_BUSCA_AVANCADA')
    DROP VIEW VIEW_UNIDADEEMPRESA_BUSCA_AVANCADA
GO

Create view VIEW_UNIDADEEMPRESA_BUSCA_AVANCADA AS

select 
ue.*, 
vend.municipio_id,
vend.municipio,
vend.municipioCidade,
vend.uf_id,
vend.siglaEstado,
vend.estado,
vend.logradouro,
vend.numero,
vend.complemento,
vend.CEP,
vend.bairro,
vend.cidade,
vend.pais_id,
vend.pais,
vend.microregiao_id,
vend.microregiao,
vend.regiaoPlanejamento_id,
vend.RegiaoPlanejamento,
vend.enderecoCompleto

from unidadeEmpresa ue 
left join VIEW_ENDERECO_BUSCA_AVANCADA vend on ue.endereco_id = vend.id;