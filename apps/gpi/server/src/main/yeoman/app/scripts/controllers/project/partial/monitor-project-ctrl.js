'use strict';
angular.module('gpifrontApp')
    .controller('MonitorProjectCtrl', function($scope, Restangular, $translate, $filter, $rootScope, growl, configuration, ScreenService, $state, $stateParams, dialogs, ESTAGIO_PROJETO, SITUACAO_PROJETO) {

        // Usado para validar o form
        $scope.formSubmitted = false;
        $scope.screenService = ScreenService;
        $scope.gridType = "PROJETO";

        /**
         * Reseta modelo para receber dados do servidor
         */
        $scope.resetModels = function() {
            $scope.acompanhamento = {
                projeto: {}
            };

            $scope.acompanhamento.atividades = [];

            $scope.currentActivity = [{}];
        }

        $scope.isSituacaoAtual = function() {
            return $scope.acompanhamento.projeto.situacaoDescricao;
        }

        $scope.isCI = function() {
            return $scope.acompanhamento.projeto.estagio === ESTAGIO_PROJETO.INICIO_PROJETO.id;
        }

        $scope.isPP = function() {
            return $scope.acompanhamento.projeto.estagio === ESTAGIO_PROJETO.PROJETO_PROMISSOR.id;
        }

        $scope.isDF = function() {
            return $scope.acompanhamento.projeto.estagio === ESTAGIO_PROJETO.FORMALIZADA.id;
        }

        $scope.isII = function() {
            return $scope.acompanhamento.projeto.estagio === ESTAGIO_PROJETO.IMPLANTACAO_INICIADO.id;
        }

        $scope.isOI = function() {
            return $scope.acompanhamento.projeto.estagio === ESTAGIO_PROJETO.OPERACAO_INICIADA.id;
        }

        $scope.isCC = function() {
            return $scope.acompanhamento.projeto.estagio === ESTAGIO_PROJETO.COMPROMISSO_CUMPRIDO.id;
        }

        /**
         *  Definição das colunas de atividades
         */
        $scope.columnDefs = [{
            field: 'localAtividade',
            enableCellEdit: false,
            width: "*",
            translationKey: 'common.local',
            cellTemplate: "<div ng-show=\"!isEditing(row.entity) || row.entity.obrigatorio\" class=\"ngCellText\" ng-class=\"col.colIndex()\"><span ng-cell-text title=\"{{row.entity.localAtividade.descricao}}\">{{row.entity.localAtividade.descricao}}</span></div>" +
                '<select ng-show=\"isEditing(row.entity) && !row.entity.obrigatorio\" ng-change=\"onChangeLocal(row.entity.localAtividade, row.entity.atividadeLocal)\" ng-class="\'colt\' + col.index\" ng-input=\"COL_FIELD\" ng-model=\"row.entity.localAtividade\">' +
                '<option title=\"{{local.descricao}}\" ng-value=\"local.id\" ng-repeat=\"local in locais\" ng-selected=\"row.entity.localAtividade.id === local.id\">{{local.descricao}}</option>' +
                '</select>'

        }, {
            field: 'atividadeLocal',
            enableCellEdit: false,
            width: "***",
            translationKey: 'common.activity',
            cellTemplate: "<div ng-show=\"!isEditing(row.entity) || row.entity.obrigatorio\" class=\"ngCellText\" ng-class=\"col.colIndex()\"><span ng-cell-text title=\"{{row.entity.atividadeLocal.descricao}}\">{{row.entity.atividadeLocal.descricao}}</span></div>" +
                '<select ng-show=\"isEditing(row.entity) && !row.entity.obrigatorio\" ng-class="\'colt\' + col.index\" ng-input=\"COL_FIELD\" ng-model=\"row.entity.atividadeLocal\">' +
                '<option title=\"{{atividade.descricao}}\" ng-value=\"atividade.id\" ng-repeat=\"atividade in getAtividades(row.entity.localAtividade)\" ng-selected=\"row.entity.atividadeLocal.id === atividade.id\">{{atividade.descricao}}</option>' +
                '</select>'
        }, {
            field: 'dataConclusao',
            enableCellEdit: false,
            width: '90',
            translationKey: 'common.done',
            cellTemplate: "<div ng-show=\"!isEditing(row.entity)\" class=\"ngCellText\" ng-class=\"col.colIndex()\"><span ng-cell-text>{{row.entity.dataConclusao | dateTransformFilter}}</span></div>" +
                "<datepicker-custom ng-show=\"isEditing(row.entity) && (row.entity.ativo === true)\" id=\"datepicker\" date=\"COL_FIELD\" configs=\"configsDatepicker\"></datepicker-custom>"
        }];

        /**
         * Função de inicialização responsável por executar a primeira consulta e cópia das atividades
         * definidas no template pela UC17
         */
        $scope.initialize = function() {
            $scope.resetModels();
            Restangular.one("projeto", $stateParams.id).one("atividade").get().then(function(atividadeWrapper) {
                $scope.acompanhamento.projeto = atividadeWrapper.cabecalho;
                $scope.acompanhamento.cronograma = atividadeWrapper.cronograma;

                if (!atividadeWrapper.atividades || _.isEmpty(atividadeWrapper.atividades)) {
                    var confirmCopyTemplate = dialogs.confirm($filter('translate')('DIALOGS_CONFIRMATION'),
                        "As atividades serão carregadas de acordo com o template ativo para Projetos. Deseja continuar?");
                    confirmCopyTemplate.result.then(function(btn) {
                        Restangular.one("projeto", $stateParams.id).one("atividade/copyFromTemplate").post().then(function(atividades) {
                            $scope.acompanhamento.atividades = !atividades ? [] : atividades;
                        });
                    });
                } else {
                    $scope.acompanhamento.atividades = atividadeWrapper.atividades;
                }

            });
            $scope.locais = [];

            Restangular.all("local").getList().then(function(result) {
                setTimeout(function() {
                    $scope.locais = result;
                    $scope.gridScope.locais = $scope.locais;
                    $scope.gridScope.getAtividades = $scope.getAtividades;
                }, true);
            });

            $scope.atividades = [];

            $scope.getAtividades = function(local) {
                if (local && local.atividades) {
                    return local.atividades;
                }
                return $scope.atividades;
            }

            $scope.onChangeLocal = function(local, atividadeLocal) {
                // setTimeout(function() {
                // $scope.$apply(function(local, atividadeLocal) {
                // atividadeLocal = null;
                for (var i = 0; i < $scope.locais.length; i++) {
                    if ($scope.locais[i].id === parseInt(local)) {
                        local = $scope.locais[i];
                        $scope.atividades = local.atividades;
                        break;
                    }
                };
                // }(local, atividadeLocal));

                // }, true);
            }

            $scope.gridScope = {
                locais: $scope.locais,
                atividades: $scope.atividades,
                onChangeLocal: $scope.onChangeLocal,
                getAtividades: $scope.getAtividades,
                currentLocation: {},
                disableActions: $scope.disableGrid
            }
        }

        $scope.deleteServicePath = "projeto/" + $stateParams.id + "/atividade/removeOne";
        $scope.saveServicePath = "projeto/" + $stateParams.id + "/atividade";

        $scope.getForm = function() {
            return $scope.formMonitorProject;
        }

        $scope.disableGrid = function() {
            return ScreenService.isShowScreen() ||($scope.acompanhamento.projeto.situacao !== SITUACAO_PROJETO.ATIVO.id && $scope.acompanhamento.projeto.situacao !== SITUACAO_PROJETO.PENDENTE_ACEITE.id && $scope.acompanhamento.projeto.situacao !== SITUACAO_PROJETO.PEDENTE_VALIDACAO.id);
        }


        $scope.checkUserAuthorization();
        //Chamando método de inicialização da tela
        $scope.initialize();
    });
