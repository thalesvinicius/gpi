'use strict';

angular.module('gpifrontApp')
    .controller('LoginCtrl', function($scope, $rootScope, $translate, AUTH_EVENTS, AuthService,
        $state, $stateParams, Restangular,growl, $http, configuration, $filter, ValidationService) {
        $scope.credentials = {
            username: '',
            password: ''
        };

        $scope.showResetArea = false;
        $scope.challengeValid = {
            isValid: false
        };

        $scope.showCaptcha = function() {
            return $scope.repeatedLogin >= 2;
        };

        $scope.maxAttemptsExceeded = function() {
            return $scope.repeatedLogin >= 5;
        };

        $http({
            method: 'GET',
            url: configuration.localPath + 'api/monitor/version'
        }).then(function(response) {
            if (response.status === 200) {
                $scope.versionNumber = response.data;
            }
        });

        var newPassword = false;
        var submitted = false;
        if ($rootScope.authError) {
            if ($rootScope.authError === AUTH_EVENTS.notAuthenticated) {
                $scope.errorMessage = "Usuário não autenticado";
            } else if ($rootScope.authError === AUTH_EVENTS.sessionTimeout) {
                $scope.errorMessage = "Sessão expirada, favor entrar novamente";
            }
        }

        $scope.clearFunctions = {
            mainScreen: function() {
                $scope.credentials = {
                    username: '',
                    password: '',
                    confirmPassword: ''
                };
            },
            captchaScreen: function() {

            }
        };

        $scope.clearScreen = function() {
            $scope.clearFunctions.mainScreen();
            $scope.clearFunctions.captchaScreen();
        }

        $scope.confirmPasswordVisible = function() {
            var token = $stateParams.token;
            if (!token) {
                return false;
            } else {
                return true;
            }
        }

        $scope.resetPasswordVisible = function() {
            var tipo = $stateParams.environment;
            if (!tipo) {
                return false;
            } else if (tipo === 'externo') {
                return true;
            } else {
                return false;
            }
        }

        var cleanMessages = function() {
            $scope.errorMessage = null;
            $scope.resetMessage = null;
        }

        $scope.forceShowResetArea = function() {
            cleanMessages();
            $scope.showResetArea = true;
        }

        $scope.hideResetArea = function() {
            cleanMessages();
            $scope.showResetArea = false;
        }

        $scope.requestNewPassword = function(form) {
            if (!ValidationService.validarEmail(form.userEmailInput.$modelValue)) {
                growl.error($filter('translate')('message.error.MSG74_FORMATO_INVALIDO', {value: "E-mail"}));
                return;
            }

            var request = {
                emailAddress: form.userEmailInput.$modelValue
            };

            cleanMessages();

            Restangular.all('oauth/reset/request').post(request).then(function(result) {
                $scope.resetMessage = 'Um e-mail foi enviado contendo instruções para o procedimento de mudança de senha.';
                $scope.showResetArea = false;
            }, function errorCallback(response) {
                if (response.data && response.data.consumerMessage) {
                    $scope.errorMessage = $filter('translate')(response.data.consumerMessage);
                    $scope.resetMessage = '';
                }else{
                    $scope.errorMessage = 'Houve um erro interno ao enviar sua solicitação. Tente novamente e se o erro persistir contate o INDI.';
                }
            });
        }

        $scope.blockExternalUser = function(login) {            
            Restangular.all('oauth/blockExternalUser').post(login).then(function(response){
                console.log("user " + login + " blocked for many attempts with wrong password.");
            }, function(){
                console.log("Error on sending request to block user " + login + " for many attempts with wrong password.");
            });                                            
        }

        function getCookie(cname) {
            var name = cname + "=";
            var ca = document.cookie.split(';');
            for(var i=0; i<ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0)==' ') c = c.substring(1);
                if (c.indexOf(name) == 0) return c.substring(name.length,c.length);
            }
            return "";
        }

        function setCookie(cname, cvalue, exdays) {
            var d = new Date();
            d.setTime(d.getTime() + (exdays*24*60*60*1000));
            var expires = "expires="+d.toUTCString();
            document.cookie = cname + "=" + cvalue + "; " + expires;
        }

        $scope.init = function() {            
            var attempts = getCookie("GPILoginAttempts");
            if (attempts) {
                $scope.repeatedLogin = attempts;
            } 

            $scope.resetPasswordVisible();
            if ($stateParams.message){
                $scope.resetMessage = $stateParams.message;
            }
        }

        $scope.checkValidation = function(input) {
            return input.$invalid && (input.$dirty || submitted);
        }

        $scope.login = function(valid, credentials) {            
            $rootScope.authError = null;
            if (!valid) {
                submitted = true;
                return;
            }

            if ($stateParams.environment && $scope.maxAttemptsExceeded()) {                
             /* RNE202 - O número máximo de tentativas de autenticação fracassadas não deve ser superior a cinco. 
                Caso essa regra seja violada a conta do usuário deve ter seu status alterado para bloqueado por 
                um período de 20 minutos contatos a partir da última tentativa. 
                A mensagem de erro MSG75_FALHA_AUTENTICACAO deve ser exibida. */
                $scope.clearScreen();
                $rootScope.$broadcast(AUTH_EVENTS.loginFailed);                
                $scope.blockExternalUser(credentials.username);
                growl.error($filter('translate')("message.error.MSG75_FALHA_AUTENTICACAO", {value: credentials.username}));
            } else if ($scope.showCaptcha() && !$scope.challengeValid.isValid) {
                $scope.clearScreen();
                $scope.errorMessage = $filter('translate')("message.error.MSG003_VALOR_IMAGEM").replace('<Nome do campo>', 'Captcha');
                $scope.resetMessage = null;            
            } else {
                var previousState = $rootScope.previousState;
                if (!previousState)
                    previousState = 'base.home';
                AuthService.login(credentials, previousState, $stateParams.environment).then(function() {
                    $scope.repeatedLogin = 0;                    
                    setCookie("GPILoginAttempts", $scope.repeatedLogin, 1);
                    $rootScope.previousState = null;
                    $rootScope.$broadcast(AUTH_EVENTS.loginSuccess);
                    $scope.errorMessage = null;
                }, function(response) {
                    $scope.clearScreen();
                    if (!$scope.repeatedLogin)
                        $scope.repeatedLogin = 0;
                    ++$scope.repeatedLogin;
                    setCookie("GPILoginAttempts", $scope.repeatedLogin, 1);
                    $rootScope.$broadcast(AUTH_EVENTS.loginFailed);
                    $scope.errorMessage = $filter('translate')(response.data.error_description);                        
                });                                
            }            
        }

        $scope.init();
    });
