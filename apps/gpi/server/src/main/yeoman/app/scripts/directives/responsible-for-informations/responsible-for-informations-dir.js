
/*  NG-RESPONSIBLE-FOR-INFORMATIONS

    @author: marcoabc

    Example of usage:
    
    ***********HTML**************
    <ng-responsible-for-informations usuario="projeto.usuarioResponsavel.nome" 
    data="projeto.usuarioResponsavel.dataUltimaAlteracao"></ng-responsible-for-informations>    

    **********Javascript*********
    // You can pass a Date(), a String or an undefined value as Date. 
    // As default, if you don't pass the date, it will be "--/--/---- --:--"

    $scope.projeto.usuarioResponsavel.nome = "Marco Aurélio";
    $scope.projeto.usuarioResponsavel.dataUltimaAlteracao = new Date();
    $scope.projeto.usuarioResponsavel.dataUltimaAlteracao = '09/10/1990 12:30';
    $scope.projeto.usuarioResponsavel.dataUltimaAlteracao = '';

*/
angular.module('gpifrontApp')
    .directive('ngResponsibleForInformations', function($compile, $rootScope, configuration) {
        return {
            restrict: 'E',
            templateUrl: configuration.rootDir + 'scripts/directives/responsible-for-informations/responsible-for-informations.html',
            scope: {
                usuario: "=",
                data: "="
            },
            link: function(scope, iElement, iAttrs) {

                //Watch criado devido a alteracoes a partir do restangular
                scope.$watch('data', function(newVal, oldVal) {
                    if (oldVal != newVal) {
                        dateChanged(newVal);
                    }

                    if (scope.usuario === undefined || scope.usuario === "") {
                        scope.usuario = "--";
                    }
                }, true);

                var dateChanged = function(data){
                    if (data === undefined) {
                        scope.dataUltimaAlteracao = "--/--/---- --:--";
                    }else if (data instanceof Date) {
                        var day = data.getDate();
                        var month = data.getMonth() + 1; //Months are zero based
                        var year = data.getFullYear();

                        var hours = data.getHours();
                        var minutes = data.getMinutes();

                        var data = ('0' + day).slice(-2) + "/" + ('0' + month).slice(-2) + "/" + year;
                        var hora = ('0' + hours).slice(-2) + ":" + ('0' + minutes).slice(-2);

                        scope.dataUltimaAlteracao = data + " " + hora;
                    }else if (data.toString().length == 13){ // Se for Milliseconds
                        dateChanged(new Date(data));
                    }
                }

                dateChanged(scope.data);
            }
        };
    });

