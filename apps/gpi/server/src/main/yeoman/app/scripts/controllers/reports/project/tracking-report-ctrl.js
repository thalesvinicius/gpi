'use strict'
angular.module('gpifrontApp')
    .controller('TrackingReportCtrl', function($scope, growl, Restangular, $filter, 
        ESTAGIO_PROJETO, STATUS_PROJETO, AuthService, AUTH_ROLES, UserSession, 
        $http, configuration, ScreenService) {

        $scope.disableHeader = true;
        $scope.filtro = {
            estagios: [],
            cadeiasProdutivas: [],
            regioesPlanejamento: [],
            departamentos: [],
            empresa: "",
            usuariosResponsaveis: []
        };
        $scope.selectedItems = [];
        $scope.disableEdit = true;
        $scope.gridScope = {
            options : {
             enableCellSelection : false,  
             selectWithCheckboxOnly: false,
             showSelectionCheckbox: false
            }
        };


        // RNE163 se for gerente não exibe o campo Gerência e se for analista de portfolio
        //  não exibe os campos Gerência e Analista
        $scope.isGerente = function() {
            return AuthService.isUserAuthorized([AUTH_ROLES.manager]);
        }

        // RNE163 se for gerente não exibe o campo Gerência e se for analista de portfolio
        //  não exibe os campos Gerência e Analista
        $scope.isPromocao = function() {
            return AuthService.isUserAuthorized([AUTH_ROLES.promotionAnalist]);
        }

        /*
         *   Recupera todos os ids da lista passada
         */
        var getIdList = function(list) {
            var idList = [];
            if (list !== undefined) {
                if (_.isArray(list)) {
                    _.each(list, function(val, key) {
                        idList.push(val.id);
                    });
                } else {
                    idList.push(list.id);
                }
            }
            return idList;
        }

        $scope.disabledExport = function() {
            return $scope.data.length === 0;
        }

        /*
         *   Evento chamado ao selecionar gerência
         */
        $scope.changeManageMultiSelect = function() {
            //  Busca os usuários com base no departamento
            if ($scope.filtro.departamentos && $scope.filtro.departamentos.length > 0) {
                Restangular.all('user/byDepartamentos').getList({
                    "departamentos": $scope.filtro.departamentos
                }).then(function(analistas) {
                    $scope.analistas = analistas;
                    //  Por default todos os analistas vem selecionados
                    $scope.filtro.usuariosResponsavel = $scope.isPromocao() ? [UserSession.getUser().id] : [];


                });

                Restangular.all('cadeiaProdutiva/byDepartamentos').getList({
                    "departamentos": $scope.filtro.departamentos
                }).then(function(cadeiasProdutivas) {
                    $scope.cadeiasProdutivas = cadeiasProdutivas;
                    //  Por default todas as cadeiasProdutivas vem selecionadas
                    // $scope.filtro.cadeiasProdutivas = getIdList(cadeiasProdutivas);
                });
            } else {
                $scope.analistas = [];
                $scope.cadeiasProdutivas = [];
                $scope.filtro.usuariosResponsavel = [];
                $scope.filtro.cadeiasProdutivas = [];
            }
        }

        /*
         *   Utilizado pelos usuarios GERENTE e PROMOCAO para recuperar as gerencias
         */
        var getDepartamentosUsuarioLogado = function() {
            Restangular.one('user/' + UserSession.getUser().id).get().then(function(user) {
                $scope.filtro.departamentos = getIdList(user.departamento);
                //  Aciona o evento onChange do select de departamentos
                //  para preencher os analistas e as cadeias produtivas
                $scope.changeManageMultiSelect();
            });
        }

        /*  Iniciando defaults do relatório */
        $scope.reset = function() {
            $scope.data = [];
            

            /*  Inicializando as listas com seus respectivos registros selecionados. */
            // $scope.filtro.estagios = getIdList($scope.estagios);
            $scope.filtro.status = [STATUS_PROJETO.ATIVO.id];

            Restangular.all('domain/regiaoPlanejamento').getList().then(function(regioesPlanejamento) {
                $scope.regioesPlanejamento = regioesPlanejamento;
                //  Por default todas as regioesPlanejamento vem selecionadas
                // $scope.filtro.regioesPlanejamento = getIdList(regioesPlanejamento);
            });

            // RNE163 se for gerente não exibe o campo Gerência e se for analista de portfolio
            //  não exibe os campos Gerência e Analista
            if ($scope.isGerente() || $scope.isPromocao()) {
                getDepartamentosUsuarioLogado();
            } else {
                Restangular.all('domain/departamento').getList().then(function(departamentos) {
                    $scope.gerencias = departamentos;
                    //  Por default todas as gerencias vem selecionadas
                    // $scope.filtro.departamentos = getIdList(departamentos);
                    $scope.changeManageMultiSelect();

                    
                });
            }
        }

        var initialize = function() {
            $scope.estagios = ESTAGIO_PROJETO.getAll();
            $scope.status = STATUS_PROJETO.getAll();

            Restangular.all('domain/regiaoPlanejamento').getList().then(function(regioesPlanejamento) {
                $scope.regioesPlanejamento = regioesPlanejamento;
                //  Por default todas as regioesPlanejamento vem selecionadas
                // $scope.filtro.regioesPlanejamento = getIdList(regioesPlanejamento);
            });

            $scope.reset();
        }

        initialize();

        //  Recupera o caminho baseado no relatório
        var getPath = function(format) {
            return 'relatorio/projeto/acompanhamento/export/' + format;
        }

        var getHeader = function(format) {
            var header = '';
            if (format === 'pdf') {
                header = 'application/pdf';
            } else if (format === 'xlsx') {
                header = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
            }
            return header;
        }

        var getFileName = function() {
            return 'RelatorioAcompanhamento';
        }

        $scope.clean = function() {
            $scope.reset();
            $scope.estagios = ESTAGIO_PROJETO.getAll();
            $scope.filtro.estagios = _.map($scope.estagios,function(est){return est.id})
        }

        $scope.export = function(format) {
            $http({
                method: 'GET',
                url: configuration.localPath + "api/" + getPath(format),
                params: {
                    "estagios": $scope.filtro.estagios,
                    "analistas": $scope.filtro.analistas,
                    "empresa": $scope.filtro.empresa,
                    "cadeiasProdutivas": $scope.filtro.cadeiasProdutivas,
                    "status": $scope.filtro.status,
                    "departamentos": $scope.filtro.departamentos,
                    "regioesPlanejamento": $scope.filtro.regioesPlanejamento,
                    "usuariosResponsavel": $scope.filtro.usuariosResponsavel,
                    "todosEstagios": _.map($scope.estagios, function(estagio) {
                        return estagio.id
                    })
                },
                headers: {
                    Accept: getHeader(format),
                    Authorization: 'Bearer ' + UserSession.getUser().accessToken,                    
                    'todasGerencias': getIdList($scope.gerencias),
                    'todasCadeiasProdutivas': getIdList($scope.cadeiasProdutivas),
                    'todasRegioesPlanejamento': getIdList($scope.regioesPlanejamento),
                    'todosAnalistas': getIdList($scope.analistas)
                },
                responseType: 'arraybuffer'
            }).
            success(function(response) {
                ScreenService.browserAwareSave(response,'Acompanhamento.' + format , getHeader(format));
            });
        }

        $scope.empregosTemplate = '<div class=\"ngCellText\" ng-class=\"col.colIndex()\"><input type="numeric" ng-class="\'colt\' + col.index" ng-input="COL_FIELD" ng-model="COL_FIELD"/></div>';

        /*Início configs grid*/
        $scope.columnDefs = [{
            field: 'empresa',
            width: "200",
            translationKey: 'common.company',
            enableCellEdit: false
        }, {
            field: 'nomeProjeto',
            width: "200",
            translationKey: 'common.project',
            enableCellEdit: false
        }, {
            field: 'cidade',
            width: "200",
            translationKey: 'address.city',
            enableCellEdit: false
        }, {
            field: 'regiaoPlanejamento',
            width: "180",
            translationKey: 'location.planing.region',
            enableCellEdit: false
        },{
            field: 'cadeiaProdutiva',
            width: "200",
            translationKey: 'common.productive.chain',
            enableCellEdit: false
        }, {
            field: 'descStatus',
            width: "100",
            translationKey: 'common.currentStatus',
            enableCellEdit: false
        },{
            field: 'descEstagio',
            width: "100",
            translationKey: 'common.currentStage',
            enableCellEdit: false
        },{
            field: 'analista',
            width: "100",
            translationKey: 'common.analyst',
            enableCellEdit: false
        }, {
            field: 'gerencia',
            width: "100",
            translationKey: 'common.manage',
            enableCellEdit: false
        },  {
            field: 'ano',
            width: "50",
            translationKey: 'common.year',
            enableCellEdit: false
        },{
            field: 'investimentoPrevisto',
            width: "100",
            translationKey: 'reportProjects.investimento.previsto',
            cellFilter: "currency:''",
            enableCellEdit: false
        }, {
            field: 'investimentoRealizado',
            width: "100",
            translationKey: 'reportProjects.investimento.realizado',
            cellFilter: "currency:''",
            enableCellEdit: false
        }, {
            field: 'faturamentoPrevisto',
            width: "100",
            translationKey: 'reportProjects.faturamento.previsto',
            cellFilter: "currency:''",
            enableCellEdit: false
        }, {
            field: 'faturamentoRealizado',
            width: "100",
            translationKey: 'reportProjects.faturamento.realizado',
            cellFilter: "currency:''",
            enableCellEdit: false
        }, {
            field: 'empregosPermanentesDiretos',
            width: "50",
            translationKey: 'reportProjects.empregos.diretos',
            enableCellEdit: false,
            cellFilter: 'number:0'
        }, {
            field: 'empregoPermanentesDiretosRealizados',
            width: "50",
            translationKey: 'reportProjects.empregos.indiretos',
            enableCellEdit: false,
            cellFilter: 'number:0'
        }];
        /*Fim configs grid*/

        function translate(translationKey) {
            var text = "";
            if (typeof translationKey !== 'undefined' && translationKey) {
                text = $filter('translate')(translationKey);
            }
            return text;
        };

        $scope.search = function() {
            Restangular.all('relatorio/projeto/acompanhamento/pesquisar').getList($scope.filtro).then(function(result) {
                
                $scope.data = result;

                if ($scope.data.length === 0) {
                    growl.error(translate('message.error.MSG004_NENHUM_REGISTRO_ENCONTRADO'));
                }

                for (var i = $scope.data.length - 1; i >= 0; i--) {
                    var current = $scope.data[i];
                    if(current.ano == 9999){
                        current.ano = 'Total';
                        current.empresa = null;
                    }
                };


            }, function errorCallback(response) {
                if (response.status !== 404) {
                    growl.error("Error: The server returned status " + response.status);
                }
            });
        };
    });