'use strict'

angular
    .module("gpifrontApp")
    .controller("AdvancedSearchCtrl", function($scope, AuthService, AUTH_ROLES, Restangular, growl, ngTableParams, ESTAGIO_PROJETO_PREVISTO, TIPO_CONTATO, SITUACAO_INSTRUMENTO_FORMALIZACAO, TIPO_INSTRUMENTO_FORMALIZACAO, TIPO_MEIO_AMBIENTE, CLASSE_EMPREENDIMENTO, SITUACAO_EMPRESA, ORIGEM_EMPRESA, SITUACAO_PROJETO, ESTAGIO_PROJETO, $filter) {
        $scope.query = {
            selectedFields: [],
            selectedConditions: []
        };

        $scope.columnDefs = [];
        var filterInit = function() {
            $scope.filtro = {
                empresaUnidade: {
                    conditionKey: "empresaUnidade",
                    value: null,
                    values: [],
                    onChange: function() {
                        $scope.filtro.empresaUnidade.values.splice(0, 1);
                        $scope.filtro.empresaUnidade.values.push($scope.filtro.empresaUnidade.value ? $scope.filtro.empresaUnidade.value.toString() : null);
                        // console.log($scope.filtro.empresaUnidade.values)
                    }
                },
                temProjetosEmpresa: {
                    conditionKey: "empresaPossuiProjetos",
                    value: null,
                    values: [],
                    onChange: function() {
                        $scope.filtro.temProjetosEmpresa.values.splice(0, 1);
                        $scope.filtro.temProjetosEmpresa.values.push($scope.filtro.temProjetosEmpresa.value ? $scope.filtro.temProjetosEmpresa.value.toString() : null);
                        // console.log($scope.filtro.temProjetosEmpresa.values)
                    }
                },
                empresaNovaEmMG: {
                    conditionKey: "empresaNovaMG",
                    value: null,
                    values: [],
                    onChange: function() {
                        $scope.filtro.empresaNovaEmMG.values.splice(0, 1);
                        $scope.filtro.empresaNovaEmMG.values.push($scope.filtro.empresaNovaEmMG.value ? $scope.filtro.empresaNovaEmMG.value.toString() : null);
                        // console.log($scope.filtro.empresaNovaEmMG.values)
                    }
                },

                versaoCorrenteProjeto: {
                    conditionKey: "projetoUltimaVersao",
                    value: 1,
                    values: [1],
                    onChange: function() {
                        $scope.filtro.versaoCorrenteProjeto.values.splice(0, 1);
                        $scope.filtro.versaoCorrenteProjeto.values.push($scope.filtro.versaoCorrenteProjeto.value ? $scope.filtro.versaoCorrenteProjeto.value.toString() : null);
                        // console.log($scope.filtro.versaoCorrenteProjeto.values)
                    }
                },
                prospeccaoAtivaProjeto: {
                    conditionKey: "projetoProspeccaoAtiva",
                    value: null,
                    values: [],
                    onChange: function() {
                        $scope.filtro.prospeccaoAtivaProjeto.values.splice(0, 1);
                        $scope.filtro.prospeccaoAtivaProjeto.values.push($scope.filtro.prospeccaoAtivaProjeto.value ? $scope.filtro.prospeccaoAtivaProjeto.value.toString() : null);
                        // console.log($scope.filtro.prospeccaoAtivaProjeto.values)
                    }
                },
                codigoNCMInsumo: {
                    conditionKey: "insumoNCM",
                    value: null,
                    values: [],
                    onChange: function() {
                        $scope.filtro.codigoNCMInsumo.values.splice(0, 1);
                        $scope.filtro.codigoNCMInsumo.values.push($scope.filtro.codigoNCMInsumo.value ? $scope.filtro.codigoNCMInsumo.value.toString() : null);
                        // console.log($scope.filtro.codigoNCMInsumo.values)
                    }
                },

                codigoNCMProduto: {
                    conditionKey: "produtoNCM",
                    value: null,
                    values: [],
                    onChange: function() {
                        $scope.filtro.codigoNCMProduto.values.splice(0, 1);
                        $scope.filtro.codigoNCMProduto.values.push($scope.filtro.codigoNCMProduto.value ? $scope.filtro.codigoNCMProduto.value.toString() : null);
                        // console.log($scope.filtro.codigoNCMProduto.values)
                    }
                },

                localizacaoProjeto: {
                    conditionKey: "projetoLocalizacao",
                    value: null,
                    values: [],
                    onChange: function() {
                        $scope.filtro.localizacaoProjeto.values.splice(0, 1);
                        $scope.filtro.localizacaoProjeto.values.push($scope.filtro.localizacaoProjeto.value ? $scope.filtro.localizacaoProjeto.value.toString() : null);
                        // console.log($scope.filtro.localizacaoProjeto.values)
                    }
                },

                localizacaoEmpresa: {
                    conditionKey: "empresaLocalizacao",
                    value: null,
                    values: [],
                    onChange: function() {
                        $scope.filtro.localizacaoEmpresa.values.splice(0, 1);
                        $scope.filtro.localizacaoEmpresa.values.push($scope.filtro.localizacaoEmpresa.value ? $scope.filtro.localizacaoEmpresa.value.toString() : null);
                        // console.log($scope.filtro.localizacaoEmpresa.values)
                    }
                },

                responsaveisInvestimento: {
                    conditionKey: "projetoResponsavelInvestimento",
                    values: []
                },
                gerencias: {
                    conditionKey: "projetoGerencia",
                    values: []
                },
                tiposProjeto: {
                    conditionKey: "projetoTipo",
                    values: []
                },
                naturezasJuridicas: {
                    conditionKey: "empresaNaturezaJuridica",
                    values: []
                },
                origens: {
                    conditionKey: "empresaOrigem",
                    values: []
                },
                situacoesEmpresa: {
                    conditionKey: "empresaSituacaoAtual",
                    values: []
                },
                situacoesProjeto: {
                    conditionKey: "projetoSituacaoAtual",
                    values: []
                },
                situacoesProjetoAlteracao: {
                    conditionKey: "projetoSituacaoAlteracao",
                    values: []
                },
                tiposRelacionamentosEmpresa: {
                    conditionKey: "infoAdTipoRelacionamento",
                    values: []
                },
                cadeiasProdutivasEmpresa: {
                    conditionKey: "empresaCadeiaProdutiva",
                    values: []
                },
                cadeiasProdutivasProjeto: {
                    conditionKey: "projetoCadeiaProdutiva",
                    values: []
                },
                estagios: {
                    conditionKey: "projetoEstagioAtual",
                    values: []
                },
                dataRelacionamentoEmpresa: {
                    conditionKey: "infoAdTipoRelacionamentoDatas",
                    dataInicio: null,
                    dataFim: null,
                    values: [],
                    onChange: function() {
                        $scope.filtro.dataRelacionamentoEmpresa.values = formatDateRangeSql($scope.filtro.dataRelacionamentoEmpresa.dataInicio, $scope.filtro.dataRelacionamentoEmpresa.dataFim);
                        // console.log($scope.filtro.dataRelacionamentoEmpresa.values)
                    }
                },
                previsaoEstagioProjeto: {
                    conditionKey: "projetoEstagiosPrevisao",
                    dataInicio: null,
                    dataFim: null,
                    values: [],
                    onChange: function() {
                        $scope.filtro.previsaoEstagioProjeto.values = formatDateRangeSql($scope.filtro.previsaoEstagioProjeto.dataInicio, $scope.filtro.previsaoEstagioProjeto.dataFim);
                        // console.log($scope.filtro.previsaoEstagioProjeto.dataPrevisaoEstagioProjeto.values);
                    },
                    estagiosPrevistos: {
                        conditionKey: "",
                        values: [],
                        onChange: function() {
                            //made this way because of the ConditionTO on the serverSide. This filter is a ComposedCondition
                            $scope.filtro.previsaoEstagioProjeto.dependency = $scope.filtro.previsaoEstagioProjeto.estagiosPrevistos;
                        }
                    }
                },
                estagiosAlteracao: {
                    conditionKey: "projetoEstagiosAlteracao",
                    values: []
                },
                dataAlteracaoSituacaoProjeto: {
                    conditionKey: "projetoSituacaoAlteracaoDatas",
                    dataInicio: null,
                    dataFim: null,
                    values: [],
                    onChange: function() {
                        $scope.filtro.dataAlteracaoSituacaoProjeto.values = formatDateRangeSql($scope.filtro.dataAlteracaoSituacaoProjeto.dataInicio, $scope.filtro.dataAlteracaoSituacaoProjeto.dataFim);
                        // console.log($scope.filtro.dataAlteracaoSituacaoProjeto.values)
                    }
                },
                dataAlteracaoEstagioProjeto: {
                    conditionKey: "projetoEstagioAlteracaoDatas",
                    dataInicio: null,
                    dataFim: null,
                    values: [],
                    onChange: function() {
                        $scope.filtro.dataAlteracaoEstagioProjeto.values = formatDateRangeSql($scope.filtro.dataAlteracaoEstagioProjeto.dataInicio, $scope.filtro.dataAlteracaoEstagioProjeto.dataFim);
                        // console.log($scope.filtro.dataAlteracaoEstagioProjeto.values)
                    }
                },
                dataPrevistaAssinaturaInstrumento: {
                    conditionKey: "instrumentoDataPrevistaAssinatura",
                    dataInicio: null,
                    dataFim: null,
                    values: [],
                    onChange: function() {
                        $scope.filtro.dataPrevistaAssinaturaInstrumento.values = formatDateRangeSql($scope.filtro.dataPrevistaAssinaturaInstrumento.dataInicio, $scope.filtro.dataPrevistaAssinaturaInstrumento.dataFim);
                        // console.log($scope.filtro.dataPrevistaAssinaturaInstrumento.values)
                    }
                },
                dataAssinaturaInstrumento: {
                    conditionKey: "instrumentoDataAssinatura",
                    dataInicio: null,
                    dataFim: null,
                    values: [],
                    onChange: function() {
                        $scope.filtro.dataAssinaturaInstrumento.values = formatDateRangeSql($scope.filtro.dataAssinaturaInstrumento.dataInicio, $scope.filtro.dataAssinaturaInstrumento.dataFim);
                        // console.log($scope.filtro.dataAssinaturaInstrumento.values)
                    }
                },
                dataPublicacaoInstrumento: {
                    conditionKey: "instrumentoDataPublicacao",
                    dataInicio: null,
                    dataFim: null,
                    values: [],
                    onChange: function() {
                        $scope.filtro.dataPublicacaoInstrumento.values = formatDateRangeSql($scope.filtro.dataPublicacaoInstrumento.dataInicio, $scope.filtro.dataPublicacaoInstrumento.dataFim);
                        // console.log($scope.filtro.dataPublicacaoInstrumento.values)
                    }
                },
                origemRecurso: {
                    conditionKey: "financeiroOrigemRecurso",
                    values: []
                },
                necessidadeFinanciamento: {
                    conditionKey: "projetoNecessidadeFinanciamentoBDMG",
                    values: []
                },
                classeMeioAmbiente: {
                    conditionKey: "projetoClasseEmpreendimento",
                    values: []
                },
                etapaLicenciamentoAmbiental: {
                    conditionKey: "projetoEtapaLicensiamentoAmbiental",
                    values: []
                },

                localDocumentoInstrumento: {
                    conditionKey: "instrumentoLocalDocumento",
                    values: []
                },
                tipoInstrumento: {
                    conditionKey: "instrumentoTipo",
                    values: []
                },
                tipoProjeto: {
                    conditionKey: "projetoTipo",
                    values: []
                },
                situacaoInstrumento: {
                    conditionKey: "instrumentoSituacaoAtual",
                    values: []
                },

                pesquisaSatisfacaoRespondida: {
                    conditionKey: "pesquisaSatisfacaoRespondida",
                    value: null,
                    values: [],
                    onChange: function() {
                        $scope.filtro.pesquisaSatisfacaoRespondida.values.splice(0, 1);
                        $scope.filtro.pesquisaSatisfacaoRespondida.values.push($scope.filtro.pesquisaSatisfacaoRespondida.value ? $scope.filtro.pesquisaSatisfacaoRespondida.value.toString() : null);
                        // console.log($scope.filtro.pesquisaSatisfacaoRespondida.values)
                    }
                },

                rangeEmpregoDiretoPermanentePrevisto: {
                    conditionKey: "empregoDirPermanenteTotalPrevisto",
                    valorInicial: null,
                    valorFinal: null,
                    values: [],
                    onChange: function() {
                        $scope.filtro.rangeEmpregoDiretoPermanentePrevisto.values = [$scope.filtro.rangeEmpregoDiretoPermanentePrevisto.valorInicial, $scope.filtro.rangeEmpregoDiretoPermanentePrevisto.valorFinal];
                        // console.log($scope.filtro.rangeEmpregoDiretoPermanentePrevisto.values)
                    }
                },

                rangeInvestimentoTotalPrevisto: {
                    conditionKey: "investimentoTotalPrevisto",
                    valorInicial: null,
                    valorFinal: null,
                    values: [],
                    onChange: function() {
                        $scope.filtro.rangeInvestimentoTotalPrevisto.values = [$scope.filtro.rangeInvestimentoTotalPrevisto.valorInicial, $scope.filtro.rangeInvestimentoTotalPrevisto.valorFinal];
                        // console.log($scope.filtro.rangeInvestimentoTotalPrevisto.values)
                    }
                },

                rangeFaturamentoPlenoPrevisto: {
                    conditionKey: "faturamentoPlenoPrevisto",
                    valorInicial: null,
                    valorFinal: null,
                    values: [],
                    onChange: function() {
                        $scope.filtro.rangeFaturamentoPlenoPrevisto.values = [$scope.filtro.rangeFaturamentoPlenoPrevisto.valorInicial, $scope.filtro.rangeFaturamentoPlenoPrevisto.valorFinal];
                        // console.log($scope.filtro.rangeFaturamentoPlenoPrevisto.values)
                    }
                },

                rangePrimeiroFaturamentoPrevisto: {
                    conditionKey: "faturamentoInicialPrevisto",
                    valorInicial: null,
                    valorFinal: null,
                    values: [],
                    onChange: function() {
                        $scope.filtro.rangePrimeiroFaturamentoPrevisto.values = [$scope.filtro.rangePrimeiroFaturamentoPrevisto.valorInicial, $scope.filtro.rangePrimeiroFaturamentoPrevisto.valorFinal];
                        // console.log($scope.filtro.rangePrimeiroFaturamentoPrevisto.values)
                    }
                },



                rangeEmpregoDiretoPermanenteRealizado: {
                    conditionKey: "empregoDirPermanenteTotalReal",
                    valorInicial: null,
                    valorFinal: null,
                    values: [],
                    onChange: function() {
                        $scope.filtro.rangeEmpregoDiretoPermanenteRealizado.values = [$scope.filtro.rangeEmpregoDiretoPermanenteRealizado.valorInicial, $scope.filtro.rangeEmpregoDiretoPermanenteRealizado.valorFinal];
                        // console.log($scope.filtro.rangeEmpregoDiretoPermanenteRealizado.values)
                    }
                },

                rangeInvestimentoTotalRealizado: {
                    conditionKey: "investimentoTotalReal",
                    valorInicial: null,
                    valorFinal: null,
                    values: [],
                    onChange: function() {
                        $scope.filtro.rangeInvestimentoTotalRealizado.values = [$scope.filtro.rangeInvestimentoTotalRealizado.valorInicial, $scope.filtro.rangeInvestimentoTotalRealizado.valorFinal];
                        // console.log($scope.filtro.rangeInvestimentoTotalRealizado.values)
                    }
                },

                rangeFaturamentoPlenoRealizado: {
                    conditionKey: "faturamentoPlenoReal",
                    valorInicial: null,
                    valorFinal: null,
                    values: [],
                    onChange: function() {
                        $scope.filtro.rangeFaturamentoPlenoRealizado.values = [$scope.filtro.rangeFaturamentoPlenoRealizado.valorInicial, $scope.filtro.rangeFaturamentoPlenoRealizado.valorFinal];
                        // console.log($scope.filtro.rangeFaturamentoPlenoRealizado.values)
                    }
                },

                rangePrimeiroFaturamentoRealizado: {
                    conditionKey: "faturamentoInicialReal",
                    valorInicial: null,
                    valorFinal: null,
                    values: [],
                    onChange: function() {
                        $scope.filtro.rangePrimeiroFaturamentoRealizado.values = [$scope.filtro.rangePrimeiroFaturamentoRealizado.valorInicial, $scope.filtro.rangePrimeiroFaturamentoRealizado.valorFinal];
                        // console.log($scope.filtro.rangePrimeiroFaturamentoRealizado.values)
                    }
                },
                rangeDemandaContratadaEnergia: {
                    conditionKey: "infraEstruturaEnergiaContratada",
                    valorInicial: null,
                    valorFinal: null,
                    values: [],
                    onChange: function() {
                        $scope.filtro.rangeDemandaContratadaEnergia.values = [$scope.filtro.rangeDemandaContratadaEnergia.valorInicial, $scope.filtro.rangeDemandaContratadaEnergia.valorFinal];
                        // console.log($scope.filtro.rangeDemandaContratadaEnergia.values)
                    }
                }
            };
        }

        var formatDateRangeSql = function(dataInicio, dataFim) {
            dataInicio = dataInicio ? new Date(dataInicio) : null;
            dataFim = dataFim ? new Date(dataFim) : null;
            var formatDateSql = function(date, time) {
                var returnValue = null;
                if (date) {
                    var day = date.getDate();
                    var month = date.getMonth() + 1; //Months are zero based
                    var year = date.getFullYear();
                    returnValue = year + "-" + ('0' + month).slice(-2) + "-" + ('0' + day).slice(-2) + " " + time;
                }
                return returnValue;
            }

            var array = [];
            array.push(formatDateSql(dataInicio, "00:00:00.0"));
            array.push(formatDateSql(dataFim, "23:59:59.999"));
            return array;
        }

        $scope.fields = {
            empresas: [],
            projetos: [],
            instrumentos: []
        };

        $scope.configsDatepicker = {
            disabled: false,
            restangularUpdated: false
        }

        $scope.result = [];

        $scope.tableParams = new ngTableParams({
            page: 1, // show first page
            count: $scope.result.length
        }, {
            total: $scope.result.length, // length of data
            getData: function($defer, params) {
                // use build-in angular filter
                $defer.resolve($scope.result);
                // var orderedData = params.sorting() ?
                //     $filter('orderBy')($scope.result, params.orderBy()) :
                //     $scope.result;
                // $defer.resolve(orderedData);
            }
        });

        var updateResultTableParams = function(result) {
            $scope.tableParams = new ngTableParams({
                page: 1, // show first page
                count: result.length
            }, {
                total: result.length, // length of data
                getData: function($defer, params) {
                    // use build-in angular filter
                    $defer.resolve($scope.result);
                    // var orderedData = params.sorting() ?
                    //     $filter('orderBy')(result, params.orderBy()) :
                    //     result;
                    // $defer.resolve(orderedData);
                }
            });
        }

        var buildConditionsList = function(filtro) {
            var conditions = [];
            for (var property in filtro) {
                if (filtro[property].values && filtro[property].values.length > 0 && filtro[property].values[0] !== null && filtro[property].values[0].toString().trim() != "") {
                    conditions.push(filtro[property]);

                    // console.log(property);
                    // console.log(filtro[property].conditionKey);
                }
            }
            console.log("condition size" + conditions.length)
            return conditions;
        }

        $scope.clean = function(filtro) {
            $scope.fields.empresas = [];
            $scope.fields.projetos = [];
            $scope.fields.instrumentos = [];

            Restangular.all("buscaAvancada/getCampos").getList().then(function(result) {
                if (result !== null) {
                    result = $filter('orderBy')(result, 'description', false);

                    $scope.fieldsEmpresa = result.filter(function(field) {
                        return field.category === 'EMPRESA';
                    });
                    $scope.fieldsProjeto = result.filter(function(field) {
                        return field.category === 'PROJETO';
                    });
                    $scope.fieldsInstrFormalizacao = result.filter(function(field) {
                        return field.category === 'INSTRUMENTO_FORMALIZACAO';
                    });
                    $scope.campos = result;
                }
            });

            filterInit();
            cnaeInit();

            $scope.gridOptions.data.splice(0, $scope.gridOptions.data.length);
            $scope.gridOptions.columnDefs.splice(0, $scope.gridOptions.columnDefs.length);
            $scope.result.splice(0, $scope.result.length);
        }

        $scope.showResponsavelInvestimentoFilter = function() {
            return !AuthService.isUserAuthorized([AUTH_ROLES.promotionAnalist]);
        }

        $scope.showGerenciaFilter = function() {
            return !AuthService.isUserAuthorized([AUTH_ROLES.manager]);
        }


        $scope.getCsvData = function() {
            var csvData = [];
            csvData.unshift($scope.columnsName);
            if ($scope.columnsName.length === 1) {
                for (var i = 0; i < $scope.result.length; i++) {
                    csvData.push([$scope.result[i]]);
                };
            } else {
                csvData = csvData.concat($scope.result);
            }
            return csvData;
        }


        $scope.advancedSearch = function() {
            buildCNAEListsForServer();
            $scope.query.selectedConditions = buildConditionsList($scope.filtro);

            updateSelectFields();
            $scope.result = [];
            $scope.columnsName = [];
            if ($scope.query.selectedFields.length === 0) {
                growl.error('É necessário selecionar no mínimo um campo para poder efetuar a pesquisa.');
                return;
            }
            Restangular.all("buscaAvancada").post($scope.query).then(function(result) {
                setTimeout(function() {
                    if (result.length > 0) {
                        updateResultTableParams(result);
                        $scope.result = result;
                        $scope.columnsName = [];

                        $scope.query.selectedFields;
                        $scope.columnDefs.splice(0, $scope.columnDefs.length);
                        for (var i = 0; i < $scope.query.selectedFields.length; i++) {
                            $scope.columnDefs.push({
                                displayName: $scope.query.selectedFields[i].description.toString(),
                                field: $scope.query.selectedFields[i].identifier,
                                enableColumnResizing: true,
                                minWidth: 150
                            })
                            $scope.columnsName.push($scope.query.selectedFields[i].description.toString());
                        }

                        $scope.gridOptions.data.splice(0, $scope.gridOptions.data.length);

                        for (var i = 0; i < result.length; i++) {
                            var item = {};
                            if (_.isArray(result[0])) {
                                for (var y = 0; y < result[i].length; y++) {
                                    item[$scope.query.selectedFields[y].identifier] = result[i][y];
                                }
                            } else {
                                item[$scope.query.selectedFields[0].identifier] = result[i];
                            }
                            $scope.gridOptions.data.push(item);
                        };
                    } else {
                        growl.error(translate('message.error.MSG004_NENHUM_REGISTRO_ENCONTRADO'));
                    }
                    translateFooterText();
                }, true);

            });
        };

        var updateSelectFields = function() {
            $scope.query.selectedFields = $scope.fields.empresas.concat($scope.fields.instrumentos).concat($scope.fields.projetos);
        }

        var cnaeInit = function() {
            $scope.selectedDivisoesCnaeLoaded = false;
            $scope.selectedGruposCnaeLoaded = false;
            $scope.selectedClassesCnaeLoaded = false;

            $scope.divisoesCnae = []

            Restangular.all("cnae").getList().then(function(result) {
                $scope.divisoesCnae = result;
            });

            $scope.selectedDivisoesCnae = [];

            $scope.gruposCnae = [];
            $scope.selectedGruposCnae = [];

            $scope.classesCnae = [];
            $scope.selectedClassesCnae = [];

            $scope.subclassesCnae = [];
            $scope.selectedSubclassesCnae = [];

            $scope.subclassesCnae = [];

        }


        var adjustSelectedList = function(selectedList, loadedList, childSelectedList, childList) {
            var entitiesToRemoveFromSelectedList = [];
            for (var i = 0; i < selectedList.length; i++) {
                var containsSelected = false;
                for (var y = 0; y < loadedList.length; y++) {
                    if (selectedList[i] === loadedList[y].id) {
                        containsSelected = true;
                        break;
                    }
                };
                if (!containsSelected) {
                    entitiesToRemoveFromSelectedList.push(selectedList[i]);
                }
            };

            for (var i = 0; i < entitiesToRemoveFromSelectedList.length; i++) {
                selectedList.splice(selectedList.indexOf(entitiesToRemoveFromSelectedList[i]), 1);
            };

            if (selectedList.length <= 0) {
                childSelectedList.splice(0, childSelectedList.length - 1);
                childList.splice(0, childList.length - 1);;
            }

        }

        var buildDivisoesCnae = function() {
            if ($scope.selectedDivisoesCnae.length > 0) {
                Restangular.all('cnae/findByParentId').post($scope.selectedDivisoesCnae).then(function(result) {
                    $scope.gruposCnae = result;
                    adjustSelectedList($scope.selectedGruposCnae, $scope.gruposCnae, $scope.selectedClassesCnae, $scope.classesCnae);
                    buildGruposCnae();
                });
            } else {
                $scope.gruposCnae = [];
                //limpando os grupos selecionados
                $scope.selectedGruposCnae = [];

                //limpando as listas de classes e classes selecionadas
                $scope.classesCnae = [];
                $scope.selectedClassesCnae = [];

                //limpando as listas de subclasses e subclasses selecionadas
                $scope.subclassesCnae = [];
                $scope.selectedSubclassesCnae = [];
            }

        }

        var buildGruposCnae = function() {
            if ($scope.selectedGruposCnae.length > 0) {
                Restangular.all('cnae/findByParentId').post($scope.selectedGruposCnae).then(function(result) {
                    $scope.classesCnae = result;
                    adjustSelectedList($scope.selectedClassesCnae, $scope.classesCnae, $scope.selectedSubclassesCnae, $scope.subclassesCnae);
                    buildClassesCnae();
                });
            } else {
                $scope.classesCnae = [];
                // limpando a lista de classes selecionadas
                $scope.selectedClassesCnae = [];

                //limpando as listas de subclasses e subclasses selecionadas
                $scope.subclassesCnae = [];
                $scope.selectedSubclassesCnae = [];
            }
        }

        var buildClassesCnae = function() {
            if ($scope.selectedClassesCnae.length > 0) {
                Restangular.all('cnae/findByParentId').post($scope.selectedClassesCnae).then(function(result) {
                    $scope.subclassesCnae = result;
                    adjustSelectedList($scope.selectedSubclassesCnae, $scope.subclassesCnae, [], []);
                });
            } else {
                $scope.subclassesCnae = [];
                $scope.selectedSubclassesCnae = [];
            }
        }

        $scope.$watch("selectedDivisoesCnae", function(newVal, oldVal) {
            if ($scope.selectedDivisoesCnaeLoaded === true) {
                $scope.selectedDivisoesCnaeLoaded = false;
            } else if (newVal !== oldVal) {
                buildDivisoesCnae();
            }
        });

        $scope.$watch("selectedGruposCnae", function(newVal, oldVal) {
            if ($scope.selectedGruposCnaeLoaded === true) {
                $scope.selectedGruposCnaeLoaded = false;
            } else if (newVal !== oldVal) {
                buildGruposCnae();
            }
        });

        $scope.$watch("selectedClassesCnae", function(newVal, oldVal) {
            if ($scope.selectedClassesCnaeLoaded === true) {
                $scope.selectedClassesCnaeLoaded = false;
            } else if (newVal !== oldVal) {
                buildClassesCnae();
            }
        });

        var buildCNAEListsForServer = function() {

            //preenchendo o objeto de divisoes do cnae
            $scope.filtro.divisoesCnae = {
                conditionKey: "empresaDivisoesCnae",
                values: []
            };
            if ($scope.selectedDivisoesCnae.length > 0) {
                for (var i = 0; i < $scope.selectedDivisoesCnae.length; i++) {
                    $scope.filtro.divisoesCnae.values.push(
                        $scope.selectedDivisoesCnae[i]
                    );
                };
            }

            //preenchendo o objeto de grupos do cnae
            $scope.filtro.gruposCnae = {
                conditionKey: "empresaGruposCnae",
                values: []
            };
            if ($scope.selectedGruposCnae.length > 0) {
                for (var i = 0; i < $scope.selectedGruposCnae.length; i++) {
                    $scope.filtro.gruposCnae.values.push(
                        $scope.selectedGruposCnae[i]
                    );
                };
            }

            //preenchendo o objeto de classes do cnae
            $scope.filtro.classesCnae = {
                conditionKey: "empresaClassesCnae",
                values: []
            };
            if ($scope.selectedClassesCnae.length > 0) {
                for (var i = 0; i < $scope.selectedClassesCnae.length; i++) {
                    $scope.filtro.classesCnae.values.push(
                        $scope.selectedClassesCnae[i]
                    );
                };
            }

            //preenchendo o objeto de subclasses do cnae
            $scope.filtro.subclassesCnae = {
                conditionKey: "empresaSubclassesCnae",
                values: []
            };
            if ($scope.selectedSubclassesCnae.length > 0) {
                for (var i = 0; i < $scope.selectedSubclassesCnae.length; i++) {
                    $scope.filtro.subclassesCnae.values.push(
                        $scope.selectedSubclassesCnae[i]
                    );
                };
            }
        }

        var init = function() {
            filterInit();
            $scope.fieldsEmpresa = [];
            $scope.fieldsProjeto = [];
            $scope.fieldsInstrFormalizacao = [];
            $scope.SITUACOES_EMPRESA = SITUACAO_EMPRESA.todas;
            $scope.ORIGEM_EMPRESA_ENUM = ORIGEM_EMPRESA.getAll()

            $scope.cadeiasProdutivas = [];
            $scope.situacoes = SITUACAO_PROJETO.todas;
            $scope.estagios = ESTAGIO_PROJETO.getAll();
            $scope.estagiosPrevistos = ESTAGIO_PROJETO_PREVISTO.getAll();

            $scope.situacoesInstrumentoFormalizacao = SITUACAO_INSTRUMENTO_FORMALIZACAO.getAll();

            $scope.classesEmpreendimentos = CLASSE_EMPREENDIMENTO.getAll();
            $scope.etapasMeioAmbiente = TIPO_MEIO_AMBIENTE.getAll();

            $scope.tiposInstrumentoFormalizacao = TIPO_INSTRUMENTO_FORMALIZACAO.getAll();

            $scope.tiposContato = TIPO_CONTATO.getAll();

            Restangular.all('domain/cadeiaProdutiva').getList().then(function(result) {
                $scope.cadeiasProdutivas = result;
            });

            $scope.naturezasJuridicas = [];
            Restangular.all('domain/naturezaJuridica').getList().then(function(result) {
                $scope.naturezasJuridicas = result;
            });

            $scope.locais = [];
            Restangular.all('domain/local').getList().then(function(result) {
                $scope.locais = result;
            });

            Restangular.all("buscaAvancada/getCampos").getList().then(function(result) {
                if (result !== null) {
                    result = $filter('orderBy')(result, 'description', false);

                    $scope.fieldsEmpresa = result.filter(function(field) {
                        return field.category === 'EMPRESA';
                    });
                    $scope.fieldsProjeto = result.filter(function(field) {
                        return field.category === 'PROJETO';
                    });
                    $scope.fieldsInstrFormalizacao = result.filter(function(field) {
                        return field.category === 'INSTRUMENTO_FORMALIZACAO';
                    });
                    $scope.campos = result;
                }
            });

            $scope.gerencias = [];
            Restangular.all('domain/departamento').getList().then(function(result) {
                $scope.gerencias = result;
            });

            $scope.responsaveis = [];
            Restangular.all('user').getList().then(function(result) {
                $scope.responsaveis = result;
            });

            cnaeInit();
        }
        init();



        $scope.gridOptions = {
            enablePaginationControls: false,
            enableColumnResizing: true,
            enableColumnMenus: false,
            paginationPageSizes: [250, 500, 1000, 2500],
            paginationPageSize: 500,
            columnDefs: $scope.columnDefs,
            paginationCurrentPage: 1,
            enableSorting: false,
            totalItems: 0,
            maxSize: 5,
            showGridFooter: false,
            data: [],
            exporterCsvColumnSeparator: ';',
            exporterCsvFilename: 'buscaAvancada.csv',
            onRegisterApi: function(gridApi) {
                $scope.gridApi = gridApi;
            }
        };

        function translateFooterText() {
            setTimeout(function() {
                $scope.totalItems = $scope.gridOptions.totalItems;
                $scope.initialPageRow = $scope.totalItems > 0 ? ($scope.gridOptions.paginationCurrentPage - 1) * $scope.gridOptions.paginationPageSize + 1 : 0;
                $scope.finalPageRow = $scope.totalItems <= 0 ? 0 : $scope.gridOptions.paginationCurrentPage * $scope.gridOptions.paginationPageSize;
                if ($scope.totalItems < $scope.finalPageRow) {
                    $scope.finalPageRow = $scope.totalItems;
                }
            }, true);
        }

        $scope.$watch("gridOptions.totalItems", function(newVal, oldVal) {
            // setTimeout(function() {
            if (newVal != oldVal) {
                translateFooterText();
            }
            // }, true);
        });

        $scope.$watch("gridOptions.paginationCurrentPage", function(newVal, oldVal) {
            // setTimeout(function() {
            if (newVal != oldVal) {
                translateFooterText();
            }
            // }, true);
        });

        $scope.$watch("gridOptions.paginationPageSize", function(newVal, oldVal) {
            // setTimeout(function() {
            if (newVal != oldVal) {
                translateFooterText();
            }
            // }, true);
        });

        $scope.export = function() {
            var myElement = angular.element(document.querySelectorAll(".custom-csv-link-location"));
            $scope.gridApi.exporter.csvExport('all', 'all', myElement);
        };

        $scope.disabledExport = function() {
            return !$scope.gridOptions || !$scope.gridOptions.data || $scope.gridOptions.data.length <= 0;
        }

        translateFooterText();

        function translate(translationKey) {
            var text = "";
            if (typeof translationKey !== 'undefined' && translationKey) {
                text = $filter('translate')(translationKey);
            }
            return text;
        };

        // updateResultTableParams([]);
    })
