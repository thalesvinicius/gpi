'use strict';
angular.module('gpifrontApp')
    .controller('ProjectSearchCtrl', function($scope, Restangular, $translate, $filter, $rootScope, growl, configuration, GridButton, SITUACAO_PROJETO, ESTAGIO_PROJETO, UserSession, TIPO_USUARIO) {

        $scope.getDepartments = function() {
            Restangular.all('domain/departamento/sigla/asc').getList().then(function(result) {
                $scope.departamentos = result;
            });
        };

        $scope.getUsers = function() {
            Restangular.all('user').getList().then(function(result) {
                $scope.usuarios = result;
            });
        };

        $scope.getProductiveChains = function() {
            Restangular.all('domain/cadeiaProdutiva').getList().then(function(result) {
                $scope.cadeiasProdutivas = result;
            });
        };

        $scope.getSituations = function() {
            $scope.situacoes = SITUACAO_PROJETO.todas;
        };

        $scope.getStages = function() {
            $scope.estagios = ESTAGIO_PROJETO.getAll();
        };
        // Instanciando as listas
        $scope.getDepartments();
        $scope.getUsers();
        $scope.getProductiveChains();
        $scope.getSituations();
        $scope.getStages();

        $scope.isExternalUser = UserSession.getUser().tipo == TIPO_USUARIO.EXTERNO;

    });
