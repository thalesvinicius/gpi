'use strict'

angular.module('gpifrontApp')
    .controller('SurveyReceivedCtrl', function($scope) {

    	// Define os filtros que serão utilizados na busca.
    	$scope.filterOptions = {
    		estagio: false,
    		anual: true,
    		regiaoPlanejamento: false,
    		gerencia: true,
    		cadeiaProdutiva: false,
    		prospeccao: false,
            intervaloDatas: false,
            mesAno: true                        
    	};

        $scope.dataColumns = [{ 
            title: 'Empresa', 
            field: 'empresa', 
            visible: true, 
            filter: { 'name': 'text' } 
        }, { 
            title: 'Gerência', 
            field: 'gerencia', 
            visible: true 
        }, { 
            title: 'Diretoria', 
            field: 'diretoria', 
            visible: true 
        }, { 
            title: 'Responsável', 
            field: 'responsavel', 
            visible: true 
        }, { 
            title: 'Porcentagem da Pesquisa', 
            field: 'porcentagemPesquisa', 
            visible: true 
        }, { 
            title: 'Meta', 
            field: 'meta', 
            visible: true 
        }, { 
            title: 'Data de Recebimento', 
            field: 'dataRecebimento', 
            visible: true,             
            format: 'date'             
        }, { 
            title: 'Comentário', 
            field: 'comentario', 
            visible: true 
        }];

	});    	

