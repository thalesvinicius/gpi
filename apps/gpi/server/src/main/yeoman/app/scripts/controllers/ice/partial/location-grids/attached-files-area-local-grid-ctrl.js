'use strict'

angular
	.module('gpifrontApp')
	.controller('AttachedFilesAreaLocalGridCtrl', function($scope, GridButton, dialogs, $filter, growl){

        $scope.gridScope = {};

        // initialize its data-model attribute
        $scope.anexosAreaLocalAbrangencia = $scope.$parent.anexosAreaLocalAbrangencia;

        // initialize its selected-items attribute
        $scope.anexosAreaLocalSelectedFiles = [];
        
        //Define its column-defs attribute
        $scope.anexosAreaLocalColumnDefs = [{        
            field: 'nome',
            enableCellEdit: false,
            width: "**",
            translationKey: 'common.file.name'
        }, {
            field: 'dataAnexo',
            enableCellEdit: false,
            width: "**",
            translationKey: 'common.date',
            cellFilter: 'dateTransformFilter'
        }];

        //define its buttons-config attribute on a persistence screen
        $scope.anexosAreaLocalButtonsConfig = {    
            newButton: new GridButton(true, 'common.attach.file', false, null, null, ''),
            deleteButton: new GridButton(true, 'common.delete', false, null, null, '')
        }

        //define its sort-info attribute if you desire a default-sort
        $scope.sortInfo = {
            fields: ['nome'],
            directions: ['asc']
        };


        function translate(translationKey) {
            var text = "";
            if (typeof translationKey !== 'undefined' && translationKey) {
                text = $filter('translate')(translationKey);
            }
            return text;
        };

        $scope.preDeleteFile = function() {
            var confirmDialog = dialogs.confirm(translate('DIALOGS_CONFIRMATION'), translate('message.confirm.MSG009_CONFIRMAR_EXCLUSAO'));
            confirmDialog.result.then(function(btn) {
                // Caso confirme executa a exclusão
                $scope.deleteFile();
            }, function(btn) {
                $scope.gridScope.checkSelectAllCheckBox(true, true);
                $scope.gridScope.checkSelectAllCheckBox(false, true);
                // Caso não confirme deseleciona os registros selecionados
                $scope.anexosAreaLocalSelectedFiles.splice(0, $scope.anexosAreaLocalSelectedFiles.length);
            });

        }

        $scope.deleteFile = function() {
            if ($scope.$parent.anexosAreaLocalAbrangencia.length > 0 && $scope.anexosAreaLocalSelectedFiles.length > 0) {
                $scope.anexosAreaLocalSelectedFiles.forEach(function(selected) {
                    var model = $scope.$parent.anexosAreaLocalAbrangencia;
                    var indexOnMainList = model.indexOf(selected);
                    // deleting each unit from main list
                    $scope.$parent.anexosAreaLocalAbrangencia.splice(indexOnMainList, 1);
                    for (var i = 0; i < $scope.$flow.files.length; i++) {
                        var file = $scope.$flow.files[i];
                        file.cancel();
                    };
                });
                // deleting all 'selectedItems' starting at index 0
                $scope.anexosAreaLocalSelectedFiles.splice(0, $scope.anexosAreaLocalSelectedFiles.length);

                growl.info($filter('translate')('message.error.MSG38_SECAO_ATUALIZADA', {value: "Localização"}));                                        
            }
        }  

        $scope.disableFn = function () {
            return $scope.anexosAreaLocalSelectedFiles.length < 1;
        }              

	});