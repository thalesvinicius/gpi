'use strict';

/*  NG-GRID-CUSTOM

    @authors: lucasgom, pedrofh

    Example of usage:

    ***********HTML**************
    //Refer your directive controller
    <div class="panel" ng-controller="ContactCtrl">
        <div class="panel-heading">
            <span class="panel-title" translate>contact.data</span>
        </div>
        <ng-grid-custom data-model="contacts" disable-header="disableHeader" disable-footer="disableFooter" column-defs="columnDefs" sort-info="sortInfo" buttons-config="buttonsConfig" selected-items="selectedItems"></ng-grid-custom>
    </div>

    //Using with a custom header
    <div class="panel" ng-controller="ContactCtrl">
        <div class="panel-heading">
            <span class="panel-title" translate>contact.data</span>
        </div>
        <div class="grid-header">
            //put your custom header content here
        </div>
        <ng-grid-custom data-model="contacts" disable-header="disableHeader" disable-footer="disableFooter" column-defs="columnDefs" sort-info="sortInfo" buttons-config="buttonsConfig" selected-items="selectedItems"></ng-grid-custom>
    </div>


    //Using with a custom footer
    <div class="panel" ng-controller="ContactCtrl">
        <div class="panel-heading">
            <span class="panel-title" translate>contact.data</span>
        </div>
        <ng-grid-custom data-model="contacts" disable-header="disableHeader" disable-footer="disableFooter" column-defs="columnDefs" sort-info="sortInfo" buttons-config="buttonsConfig" selected-items="selectedItems"></ng-grid-custom>
        <div class="grid-footer clearfix">
            //put your footer content here
        </div>
    </div>


    **********Javascript*********
    We recommend to use one controller for each grid you use. But if you use more
    than one in the same controller DON'T use the same variable as attribute for both.

    app.controller('ContactCtrl', function($scope, Restangular, $filter, $rootScope, GridButton) {

        // initialize its data-model attribute
        $scope.contacts = [];
        // initialize its selected-items attribute
        $scope.selectedItems = [];

        //Define its column-defs attribute
        $scope.columnDefs = [{
            field: 'nome',
            enableCellEdit: true,
            width: "**",
            translationKey: 'common.name',
            editableCellTemplate: '<input type="text" placeholder="{{\'common.name\' | translate}}" ng-class="\'colt\' + col.index"  ng-input="COL_FIELD" ng-model="COL_FIELD" maxlength="100"/>'

        }, {
            field: 'cargo',
            enableCellEdit: true,
            width: "**",
            translationKey: 'common.job.title',
            editableCellTemplate: '<input type="text" placeholder="{{\'common.job.title\' | translate}}" ng-class="\'colt\' + col.index"  ng-input="COL_FIELD" ng-model="COL_FIELD" maxlength="100"/>'
        }, {
            field: 'tel1',
            enableCellEdit: true,
            width: "*",
            translationKey: 'common.phone1',
            editableCellTemplate: '<input type="phone" placeholder="{{\'common.phone1\' | translate}}" ng-class="\'colt\' + col.index" maxlength="18" ng-input="COL_FIELD" ng-model="COL_FIELD"/>'
        }, {
            field: 'tel2',
            enableCellEdit: true,
            width: "*",
            translationKey: 'common.phone2',
            editableCellTemplate: '<input type="phone" placeholder="{{\'common.phone2\' | translate}}" ng-class="\'colt\' + col.index" maxlength="18" ng-input="COL_FIELD" ng-model="COL_FIELD"/>'

        }, {
            field: 'email',
            enableCellEdit: true,
            width: "**",
            translationKey: 'common.email',
            editableCellTemplate: '<input type="email" placeholder="example@email.com" ng-class="\'colt\' + col.index" maxlength="50" ng-input="COL_FIELD" ng-model="COL_FIELD" />'
        }];

        //define some delete function if you want
        function deleteFn () {
            console.log("do delete()");
        }

        //define some disable function if you need
        function disableFn() {
            return $scope.selectedItems.length !== 1;
        }

        //define its buttons-config attribute on a persistence screen
        $scope.buttonsConfig = {
            //function GridButton(show, translationKey, searchMode, disable, action, screenLink)
            newButton: new GridButton(true, 'contact.new', false, null, null, ''),
            deleteButton: new GridButton(true, 'common.delete', false, null, null, '')
        }

        //or on a search screen
        $scope.buttonsConfig = {
            newButton: new GridButton(true, 'contact.new', true, null, null, '/empresa/manter'),
            editButton: new GridButton(true, 'common.edit', true, disableFn, null, '/empresa/editar'),
            deleteButton: new GridButton(true, 'common.delete', false, disableFn, deleteFn, '')
        }

        //define its sort-info attribute if you desire a default-sort
        $scope.sortInfo = {
            fields: ['nome'],
            directions: ['asc']
        };

        //define its disable-header or disable-footer attributes if you need them. They can be a function or a boolean

        //$scope.disableHeader = true;
        //$scope.disableFooter = true;

        $scope.disableHeader = function () {
            return true;
        }

        $scope.disableFooter = function () {
            return true;
        }
    });
*/

angular
    .module('gpifrontApp')
    .factory('GridButton', function() {
        return function button(show, translationKey, searchMode, disableFn, action, screenLink) {
            // Bollean used to show the button
            this.show = show;
            // String key from translation of the button name
            this.translationKey = translationKey;

            /* Boolean representing the source screen from the button
                set true if it will be called from search screen*/
            this.searchMode = searchMode;
            // Function used for disable the button
            if (typeof disableFn === 'function') {
                this.disableFn = disableFn;
            } else {
                this.disableFn = function() {
                    return null;
                }
            }
            /*Function used to define its button action,
                will executes before directive default action execute*/
            if (typeof action === 'function') {
                this.action = action;
            }

            /*Set the path of the screen that will be called.
                Needs from 'searchMode' be set to true*/
            this.screenLink = screenLink;

        };
    })
    .directive('ngGridCustom', function($compile, $rootScope, Restangular, dialogs, $filter, $location, GridButton, configuration, INSUMO_PRODUTO_ORIGEM, NCM) {
        return {
            restrict: 'E',
            scope: {
                small: "=small"
            },
            templateUrl: configuration.rootDir + 'views/common/ng-grid-custom-dir-tpl.html',
            // template: '<div class="grid-style" ng-grid="options"></div>',
            compile: function(cElem, cAttrs) {
                return {
                    pre: function(scope, iElement, iAttrs) {
                        scope.selectedChildrenItems = {};

                        var injectionFunctions = scope.$parent[iAttrs.injectionFunctions];
                        if (!angular.isArray(injectionFunctions))
                            injectionFunctions = [injectionFunctions];
                        for (var i = injectionFunctions.length - 1; i >= 0; i--) {
                            var injFunction = injectionFunctions[i];
                            scope[injFunction] = scope.$parent[injFunction];
                        };

                        scope.selectChildrenItem = function(child) {
                            var selectedItems = scope.$parent[iAttrs.selectedItems];
                            if (scope.selectedChildrenItems[child.id]) {
                                selectedItems.push(child);
                            } else {
                                for (var i = 0; i < selectedItems.length; i++) {
                                    if (selectedItems[i].id === child.id) {
                                        selectedItems.splice(i, 1);
                                        break;
                                    }
                                };
                            }
                        }

                        //deprecated-- use disable-header attribute
                        scope.disableHeader = function() {
                            if (typeof scope.$parent[iAttrs.disableHeader] === 'function') {
                                return scope.$parent[iAttrs.disableHeader]();
                            } else if (typeof scope.$parent[iAttrs.disableHeader] === 'boolean') {
                                return scope.$parent[iAttrs.disableHeader];
                            } else {
                                return scope.newButton.show === false && scope.searchButton.show === false && scope.editButton.show === false && scope.deleteButton.show === false;
                            }
                        }

                        //deprecated-- use disable-header attribute
                        scope.disableFooter = function() {
                            if (typeof scope.$parent[iAttrs.disableFooter] === 'function') {
                                return scope.$parent[iAttrs.disableFooter]();
                            } else if (typeof scope.$parent[iAttrs.disableFooter] === 'boolean') {
                                return scope.$parent[iAttrs.disableFooter];
                            } else {
                                return false;
                            }
                        }

                        function disableEdit() {
                            return scope.$parent[iAttrs.selectedItems].length !== 1;
                        }

                        function disableDelete() {
                            return scope.$parent[iAttrs.selectedItems].length < 1;
                        }

                        function buttonsConfigInit() {
                            scope.newButton = new GridButton(true, 'common.new', false, false, null, '');

                            scope.searchButton = new GridButton(false, 'common.search', false, false, null, '');

                            scope.editButton = new GridButton(false, 'common.edit', false, disableEdit, null, '');

                            scope.deleteButton = new GridButton(true, 'common.delete', false, disableDelete, null, '');


                            if (typeof scope.$parent.$eval(iAttrs.buttonsConfig) !== 'undefined' && scope.$parent.$eval(iAttrs.buttonsConfig)) {
                                if (typeof scope.$parent.$eval(iAttrs.buttonsConfig).newButton !== 'undefined' && scope.$parent.$eval(iAttrs.buttonsConfig).newButton) {
                                    scope.newButton = scope.$parent.$eval(iAttrs.buttonsConfig).newButton;
                                }
                                if (scope.$parent.$eval(iAttrs.buttonsConfig).searchButton && typeof scope.$parent.$eval(iAttrs.buttonsConfig).searchButton !== 'undefined') {
                                    scope.searchButton = scope.$parent.$eval(iAttrs.buttonsConfig).searchButton;
                                }
                                if (scope.$parent.$eval(iAttrs.buttonsConfig).editButton && typeof scope.$parent.$eval(iAttrs.buttonsConfig).editButton !== 'undefined') {
                                    scope.editButton = scope.$parent.$eval(iAttrs.buttonsConfig).editButton;
                                    if (scope.editButton.disableFn() === null) {
                                        scope.editButton.disableFn = disableDelete;
                                    } else {
                                        var parentEditFunction = angular.copy(scope.editButton.disableFn);
                                        var disableEditFull = function() {
                                                return disableEdit() || parentEditFunction();
                                            }
                                            //chamando a função passada junto com a função padrão para não precisar verificar há algum registro selecionado na grid.
                                        scope.editButton.disableFn = disableEditFull;
                                    }
                                }
                                if (scope.$parent.$eval(iAttrs.buttonsConfig).deleteButton && typeof scope.$parent.$eval(iAttrs.buttonsConfig).deleteButton !== 'undefined') {
                                    scope.deleteButton = scope.$parent.$eval(iAttrs.buttonsConfig).deleteButton;
                                    if (scope.deleteButton.disableFn() === null) {
                                        scope.deleteButton.disableFn = disableDelete;
                                    } else {
                                        var parentDeleteFunction = angular.copy(scope.deleteButton.disableFn);
                                        var disableDeleteFull = function() {
                                                return disableDelete() || parentDeleteFunction();
                                            }
                                            //chamando a função passada junto com a função padrão para não precisar verificar há algum registro selecionado na grid.
                                        scope.deleteButton.disableFn = disableDeleteFull;
                                    }
                                }
                            }
                        };

                        function verifyDirectiveAttributes() {
                            if (typeof scope.$parent.$eval(iAttrs.selectedItems) === 'undefined') {
                                throw 'selected-items attribute is not defined!';
                            }

                            if (typeof scope.$parent.$eval(iAttrs.columnDefs) === 'undefined') {
                                throw 'column-defs attribute is not defined!';
                            }

                            if (typeof scope.$parent.$eval(iAttrs.model) === 'undefined') {
                                throw 'data-model attribute is not defined!';
                            }
                        };

                        function buildLinkCellTemplate() {
                            for (var i = 0; i < scope.columnDefs.length; i++) {
                                if (typeof scope.columnDefs[i].showScreenRedirect !== 'undefined' && scope.columnDefs[i].showScreenRedirect) {
                                    if (typeof scope.columnDefs[i].showLink !== 'undefined' && scope.columnDefs[i].showLink) {
                                        var linkCellTemplate = '<div class="ngCellText text-center" ng-class="col.colIndex()">' +
                                            '  <a href=#' + scope.columnDefs[i].showLink + '?id={{row.entity.id}}><i class="fa fa-eye page-header-icon"></i></a>' +
                                            '</div>';
                                        scope.columnDefs[i].enableCellEdit = false;
                                        scope.columnDefs[i].cellTemplate = linkCellTemplate;

                                    } else {
                                        throw 'showLink is undefined or is null for columnDefs[' + i + ']';
                                    }

                                }
                            };
                        };

                        function translate(translationKey) {
                            var text = "";
                            if (typeof translationKey !== 'undefined' && translationKey) {
                                text = $filter('translate')(translationKey);
                            }
                            return text;
                        };

                        function translateColumnsDisplayName() {
                            for (var i = 0; i < scope.columnDefs.length; i++) {
                                if (typeof scope.columnDefs[i].translationKey !== 'undefined' && scope.columnDefs[i].translationKey != null) {
                                    scope.columnDefs[i].displayName = translate(scope.columnDefs[i].translationKey);
                                } else {
                                    throw 'translationKey undefined or is null for columnDefs[' + i + ']';
                                }

                            };
                        };

                        function translateFooterText() {
                            scope.totalItems = scope.totalServerItems;
                            scope.initialPageRow = scope.totalItems > 0 ? (scope.pagingOptions.currentPage - 1) * scope.pagingOptions.pageSize + 1 : 0;
                            scope.finalPageRow = scope.pagingOptions.currentPage * scope.pagingOptions.pageSize;
                            if (scope.totalItems < scope.finalPageRow) {
                                scope.finalPageRow = scope.totalItems;
                            }
                        }

                        function translateButtonsDisplayName() {
                            scope.newButton.displayName = translate(scope.newButton.translationKey);
                            scope.searchButton.displayName = translate(scope.searchButton.translationKey)
                            scope.editButton.displayName = translate(scope.editButton.translationKey)
                            scope.deleteButton.displayName = translate(scope.deleteButton.translationKey)
                        }

                        function doTranslation() {
                            translateColumnsDisplayName();
                            translateFooterText();
                        }

                        function gridOptionsInit() {
                            scope.options.selectedItems = scope.$parent[iAttrs.selectedItems];
                            scope.options.totalServerItems = 'totalServerItems';
                            scope.options.enableCellSelection = true;
                            scope.options.enableCellEdit = true;
                            scope.options.enableRowSelection = true;
                            scope.options.showSelectionCheckbox = true;
                            scope.options.selectWithCheckboxOnly = true;
                            scope.options.enableSorting = true;
                            scope.options.multiSelect = true;
                            scope.options.enablePaging = true;
                            scope.options.showFooter = false;
                            scope.options.footerRowHeight = 0;
                            scope.options.footerTemplate = '<div ng-show="false"></div>';

                            scope.options.sortInfo = scope.$parent[iAttrs.sortInfo];
                            scope.options.useExternalSorting = true;
                            scope.options.columnDefs = 'columnDefs';
                            scope.options.pagingOptions = scope.pagingOptions;
                            scope.options.enableColumnResize = true;
                            if (scope.$parent[iAttrs.aggregateTemplate]) {
                                scope.options.aggregateTemplate = scope.$parent[iAttrs.aggregateTemplate];
                            }
                            if (scope.$parent[iAttrs.groups]) {
                                scope.options.groups = scope.$parent[iAttrs.groups];
                            }
                            if (scope.$parent[iAttrs.rowTemplate]) {
                                scope.options.rowTemplate = scope.$parent[iAttrs.rowTemplate];
                            }

                            if (scope.$parent[iAttrs.rowStyle]) {
                                scope.$on('ngGridEventData', function() {
                                    if (scope.options.$gridScope) {
                                        scope.options.$gridScope.rowStyle = scope.$parent.$eval(iAttrs.rowStyle);
                                    }
                                });
                            }
                            scope.options.afterSelectionChange = function(rowItem) {
                                if (rowItem instanceof Array) {

                                    if (typeof scope.options.$gridScope !== 'undefined') {
                                        if (scope.options.$gridScope.$$childHead) {
                                            if (scope.options.$gridScope.$$childHead.allSelected == true) {
                                                if (scope.$parent[iAttrs.model].length == 0) {
                                                    scope.checkSelectAllCheckBox(false, true);
                                                }
                                            }
                                        }
                                    }
                                } else {
                                    if (rowItem.selected) {
                                        //verifying if all content visible on the page are checked
                                        var allPageVisibleContent = scope.$parent[iAttrs.model].length < scope.pagingOptions.pageSize ? scope.$parent[iAttrs.model].length : scope.pagingOptions.pageSize;
                                        // verifying if all lines are selected
                                        if (allPageVisibleContent === scope.options.selectedItems.length) {
                                            scope.checkSelectAllCheckBox(true, true);
                                        }
                                    } else {
                                        scope.checkSelectAllCheckBox(false, false);
                                    }
                                }
                            }
                            //sobreescrevendo os atributos defaults caso seja definido no escopo pai
                            if (iAttrs.gridScope) {
                                for (var property in scope.$parent[iAttrs.gridScope]) {
                                    if (property === 'options') {
                                        for (var optionsProperty in scope.$parent[iAttrs.gridScope].options) {
                                            scope.options[optionsProperty] = scope.$parent[iAttrs.gridScope].options[optionsProperty];
                                        }
                                    } else {
                                        scope[property] = scope.$parent[iAttrs.gridScope][property];
                                    }
                                }
                            }
                        }

                        function init() {
                            buttonsConfigInit();
                            verifyDirectiveAttributes();
                            scope.columnDefs = scope.$parent.$eval(iAttrs.columnDefs);
                            scope.options = {};

                            scope.options.data = 'pagingData';

                            scope.filterOptions = {
                                filterText: "",
                                useExternalFilter: true
                            };

                            scope.totalServerItems = 0;

                            var pageSize = 10;

                            //WORKAROUND arrumar para desabilitar a paginação quando o foooter for hidden
                            if (typeof scope.$parent[iAttrs.disableFooter] === 'function') {
                                scope.$parent[iAttrs.disableFooter]() === true ? pageSize = 10000 : pageSize = 10;
                            } else if (typeof scope.$parent[iAttrs.disableFooter] === 'boolean') {
                                scope.$parent[iAttrs.disableFooter] === true ? pageSize = 10000 : pageSize = 10;
                            }

                            scope.pagingOptions = {
                                pageSizes: [10, 20, 50, 100],
                                pageSize: pageSize,
                                currentPage: 1,
                                maxSize: 5
                            };

                            buildLinkCellTemplate();

                            scope.footerShowingText = "";
                            scope.footerPerPageText = "";


                            scope.disableFooter = scope[iAttrs.disableFooter];
                            doTranslation();
                            gridOptionsInit();

                            //exposing this scope to parent
                            if (!angular.isUndefined(scope.$parent.gridScope)) {
                                scope.$parent.gridScope = scope;
                            }

                            //exposing this scope to parent
                            if (!angular.isUndefined(scope.$parent.$parent.gridScope)) {
                                scope.$parent.$parent.gridScope = scope;
                            }


                            scope.ncmList = NCM;
                            scope.insumoProdutoOrigem = INSUMO_PRODUTO_ORIGEM;

                            // Restangular.oneUrl('ncmProperties', configuration.dataDir + 'project/ncmProperties.json').get().then(function(result) {
                            //     setTimeout(function() {
                            //         scope.ncmProperties = result;
                            //     }, true);
                            // });

                        };

                        init();

                        scope.search = function() {
                            scope.$parent[iAttrs.model] = scope.searchButton.search();
                        }

                        scope.addNew = function() {
                            if (typeof scope.newButton.searchMode != 'undefined' && scope.newButton.searchMode) {
                                if (typeof scope.newButton.screenLink != 'undefined' && scope.newButton.screenLink) {
                                    $location.path(scope.newButton.screenLink);
                                } else {
                                    throw 'screenLink property is undefined or is null in newButton object!';
                                }
                            } else {
                                if (typeof scope.newButton.action === 'function') {
                                    if (scope.newButton.action() === false) {
                                        return;
                                    }
                                }
                                scope.$parent[iAttrs.model].unshift({});
                            }
                        };

                        scope.edit = function() {
                            if (typeof scope.editButton.searchMode != 'undefined' && scope.editButton.searchMode) {
                                if (typeof scope.editButton.screenLink != 'undefined' && scope.editButton.screenLink) {
                                    if (scope.$parent[iAttrs.selectedItems].length == 1) {
                                        $location.path(scope.editButton.screenLink).search({
                                            "id": scope.$parent[iAttrs.selectedItems][0].id.toString()
                                        });
                                    } else {
                                        throw 'more than one row selected to edit!';
                                    }
                                } else {
                                    throw 'screenLink property is undefined or is null in newButton object!';
                                }
                            }
                        }

                        scope.preDelete = function() {
                            var confirmDialog = dialogs.confirm(translate('DIALOGS_CONFIRMATION'), translate('message.confirm.MSG009_CONFIRMAR_EXCLUSAO'));
                            confirmDialog.result.then(function(btn) {
                                // Caso confirme executa a exclusão
                                scope.deleteFn();
                            }, function(btn) {
                                scope.checkSelectAllCheckBox(true, true);
                                scope.checkSelectAllCheckBox(false, true);
                                // Caso não confirme deseleciona os registros selecionados
                                scope.$parent[iAttrs.selectedItems].splice(0, scope.$parent[iAttrs.selectedItems].length);
                            });

                        }
                        //TODO deixar parametrizável a função de deletar passando o serviço a ser chamado
                        scope.deleteFn = function(ignoreParentFunction) {
                            if (typeof scope.deleteButton.action === 'function' && (angular.isUndefined(ignoreParentFunction) || ignoreParentFunction !== true)) {
                                if (scope.deleteButton.action() === false) {
                                    return;
                                }
                            }

                            if (scope.$parent.$eval(iAttrs.model).length > 0 && scope.$parent[iAttrs.selectedItems].length > 0) {
                                scope.$parent[iAttrs.selectedItems].forEach(function(selected) {
                                    var model = scope.$parent.$eval(iAttrs.model);
                                    var indexOnMainList = model.indexOf(selected);
                                    if (indexOnMainList === -1 && iAttrs.childAttribute) {
                                        for (var i = 0; i < model.length; i++) {
                                            var children = model[i][iAttrs.childAttribute];
                                            indexOnMainList = children.indexOf(selected);
                                            children.splice(indexOnMainList, 1);
                                        };
                                    }
                                    // deleting each unit from main list
                                    else {
                                        scope.$parent.$eval(iAttrs.model).splice(indexOnMainList, 1);
                                    }
                                });
                                // deleting all 'selectedItems' starting at index 0
                                scope.$parent[iAttrs.selectedItems].splice(0, scope.$parent[iAttrs.selectedItems].length);
                            }


                        };

                        scope.sortData = function(field, direction) {
                            if (typeof field !== "undefined") {
                                if (!scope.$parent[iAttrs.model]) {
                                    return;
                                }
                                scope.$parent[iAttrs.model].sort(function(a, b) {

                                    var aFieldValue = scope.getChildProperty(a, field);
                                    var bFieldValue = scope.getChildProperty(b, field);

                                    if (typeof aFieldValue === "number" && typeof bFieldValue === "number") {
                                        if (direction === "desc") {
                                            return bFieldValue - aFieldValue;
                                        }
                                        return aFieldValue - bFieldValue;
                                    } else {
                                        aFieldValue = typeof aFieldValue === "String" ? aFieldValue.toLowerCase() : (!angular.isUndefined(aFieldValue) && aFieldValue ? aFieldValue : "");
                                        bFieldValue = typeof bFieldValue === "String" ? bFieldValue.toLowerCase() : (!angular.isUndefined(bFieldValue) && bFieldValue ? bFieldValue : "");
                                        if (direction === "desc") {
                                            return aFieldValue > bFieldValue ? -1 : 1;
                                        }
                                        return aFieldValue > bFieldValue ? 1 : -1;
                                    }
                                    return 0;
                                })
                            }

                        }

                        scope.setPagingData = function(data, page, pageSize) {
                            if (!scope.$parent.$$phase && !scope.$$phase) {
                                scope.$apply(function(internalData) {
                                    if (internalData) {
                                        var pagedData = scope.options.isServerSide ? internalData : internalData.slice((page - 1) * pageSize, page * pageSize);
                                        scope.pagingData = pagedData;
                                        scope.totalServerItems = scope.options.isServerSide ? scope.options.maxResults : internalData.length;
                                        translateFooterText();
                                    }
                                }(data));
                            }
                        };

                        scope.checkSelectAllCheckBox = function(checked, considerToogleSelectAllFunction) {
                            if (typeof scope.options.$gridScope !== 'undefined' && scope.options.$gridScope !== null) {
                                if (scope.options.$gridScope.$$childHead) {
                                    scope.options.$gridScope.$$childHead.allSelected = checked;
                                }
                                if (considerToogleSelectAllFunction) {
                                    scope.options.$gridScope.toggleSelectAll(checked, true);
                                    scope.selectedChildrenItems = {};
                                }
                            }
                        };

                        scope.getPagedDataAsync = function(pageSize, page, searchText) {
                            setTimeout(function() {
                                scope.setPagingData(scope.$parent[iAttrs.model], page, pageSize);
                            });
                            scope.checkSelectAllCheckBox(false, true);
                            // angular.element('.ngSelectionHeader').attr('checked', false);
                        };

                        $rootScope.$on('$translateChangeEnd', function() {
                            doTranslation();
                        });

                        scope.$parent.$watch(iAttrs.sortInfo, function(newVal, oldVal) {
                            if (newVal !== oldVal) {
                                scope.sortData(newVal.fields[0], newVal.directions[0]);
                            }
                        }, true);

                        scope.$watch('pagingOptions', function(newVal, oldVal) {
                            if (newVal !== oldVal) {
                                if (scope.options.isServerSide) {
                                    setTimeout(function() {
                                        var newPage = newVal.currentPage;
                                        var oldPage = oldVal.currentPage;

                                        if (newVal.pageSize !== oldVal.pageSize) {
                                            scope.pageOnChange(0, 1, newVal.pageSize);
                                        } else {
                                            scope.pageOnChange(oldVal.currentPage, newVal.currentPage, newVal.pageSize);
                                        }
                                    });
                                } else {
                                    scope.getPagedDataAsync(newVal.pageSize, newVal.currentPage, scope.filterOptions.filterText);
                                }
                            }
                        }, true);

                        scope.$watch('filterOptions', function(newVal, oldVal) {
                            if (newVal !== oldVal) {
                                scope.getPagedDataAsync(scope.pagingOptions.pageSize, scope.pagingOptions.currentPage, scope.filterOptions.filterText);
                            }
                        }, true);

                        scope.isDataModelEmpty = true;

                        scope.$parent.$watch(iAttrs.model, function(newVal, oldVal) {
                            if (newVal != oldVal) {
                                //update paging composition always 'iAttrs.model' update
                                if (typeof scope.$parent[iAttrs.sortInfo] !== 'undefined' && scope.$parent[iAttrs.sortInfo] !== null) {
                                    if (scope.isDataModelEmpty === true) {
                                        scope.isDataModelEmpty = false
                                        scope.sortData(scope.$parent[iAttrs.sortInfo].fields[0], scope.$parent[iAttrs.sortInfo].directions[0]);
                                    }
                                }
                                scope.getPagedDataAsync(scope.pagingOptions.pageSize, scope.pagingOptions.currentPage);
                            }
                        }, true);

                        scope.expandedNodes = {};
                        scope.nodeExpanded = function(nodeKey) {
                            return scope.expandedNodes[nodeKey];
                        };

                        scope.selectNodeHead = function(rowKey) {
                            scope.expandedNodes[rowKey] = !scope.expandedNodes[rowKey];
                        };

                        scope.logar = function(c) {
                            console.log(c);
                        };

                        scope.headClass = function(entity, rowKey, children) {
                            if (children && children.length && !scope.expandedNodes[rowKey])
                                return "fa fa-chevron-right";
                            else if (children && children.length && scope.expandedNodes[rowKey])
                                return "fa fa-chevron-down";
                            else
                                return "";
                        };

                        scope.getChildProperty = function(obj, property) {
                            if (typeof property === 'undefined' || property === null)
                                return '';
                            var properties = property.split('.');
                            var returnObject = obj;
                            for (var i = 0; i < properties.length; i++) {
                                returnObject = returnObject[properties[i]];
                                //if the object is null return itself
                                if (!returnObject) {
                                    return returnObject;
                                }
                            };
                            return returnObject;
                        }
                    }
                }
            }
        };
    })
    .directive('fileUploader', function($rootScope, $filter, configuration, Restangular, growl) {
        return {
            restrict: 'E',
            scope: {
                attachedFiles: '=model',
                attachedFilesColumnDefs: '=columnDefs',
                attachedSelectedFiles: '=selectedItems',
                sortInfo: '=sortInfo'
            },
            template: '<ng-grid-custom data-model="attachedFiles" sort-info="sortInfo" small="true" column-defs="attachedFilesColumnDefs" selected-items="attachedSelectedFiles" injection-functions="injectionFunctions" grid-scope="gridScope" disable-header="disableHeader"></ng-grid-custom>',
            compile: function(cElem, cAttrs) {
                return {
                    pre: function(scope, iElement, iAttrs) {
                        scope.attachedFilesColumnDefs.push({
                            field: "path",
                            translationKey: 'common.show',
                            cellTemplate: '<div class="text-center" ng-class="col.colIndex()">' +
                                '<a target="_self" href="{{downloadLink(row.entity.path, row.entity.nome)}}" ng-if="!row.entity.internalFlowFile"><i class="fa fa-eye page-header-icon"></i></a>' +
                                '<div ng-if="row.entity.internalFlowFile" class="progress progress-striped" ng-class="{active: row.entity.internalFlowFile.isUploading()}">' +
                                '<div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" ng-style="{width: (row.entity.internalFlowFile.progress() * 100) + \'%\'}" style="width: 100%;">' +
                                '<span class="sr-only ng-binding">1% Complete</span>' +
                                '</div>' +
                                '</div>' +
                                '</div>',
                            enableCellEdit: false
                        });
                        scope.internalFlowFiles = {};
                        scope.gridScope = {};
                        scope.injectionFunctions = 'downloadLink';

                        //Watcher para o evento de arquivo: Inicio do upload
                        scope.$on('flow::fileAdded', function(event, $flow, flowFile) {
                            console.log(flowFile);
                            //Valida RN 024 - Formatos permitidos
                            if (!scope.validateFileExtensions(flowFile.name)) {
                                growl.error($filter('translate')("message.error.MSG27_FORMATO_INVÁLIDO"));
                                event.preventDefault();
                                return false;
                            }

                            //Valida Tamanho Máximo
                            if (!scope.validateFileSize(flowFile.size)) {
                                growl.error($filter('translate')("message.error.MSG19_TAMANHO_EXCEDIDO"));
                                event.preventDefault();
                                return false;
                            }

                            scope.internalFlowFiles[flowFile.uniqueIdentifier] = flowFile;

                            if (!scope.attachedFiles || angular.isUndefined(scope.attachedFiles)) {
                                scope.attachedFiles = [];
                            }

                            scope.attachedFiles.unshift({
                                internalFlowFile: {
                                    uniqueIdentifier: flowFile.uniqueIdentifier,
                                    isUploading: function() {
                                        return scope.internalFlowFiles[this.uniqueIdentifier].isUploading();
                                    },
                                    progress: function() {
                                        return scope.internalFlowFiles[this.uniqueIdentifier].progress();
                                    }
                                }
                            });
                            setTimeout(function() {
                                if (!flowFile.isUploading() && !flowFile.isComplete()) {
                                    flowFile.resume();
                                }
                            }, true);
                        });

                        scope.$on('flow::fileSuccess', function(event, $flow, flowFile, response) {
                            var responseObj = JSON.parse(response);
                            responseObj.nome = flowFile.name;
                            for (var i = 0; i < scope.attachedFiles.length; i++) {
                                var internalFlowFile = scope.attachedFiles[i].internalFlowFile;
                                if (internalFlowFile && typeof internalFlowFile !== 'undefined' && internalFlowFile.uniqueIdentifier === flowFile.uniqueIdentifier) {
                                    var uniqueIdentifier = scope.attachedFiles[i].internalFlowFile.uniqueIdentifier;
                                    scope.attachedFiles[i] = responseObj;
                                    scope.attachedFiles.uniqueIdentifier = uniqueIdentifier;
                                }
                            };
                        });

                        /**
                         * Valida Tamanho Máximo
                         */
                        scope.validateFileSize = function(fileSize) {
                            if (fileSize > 52428800) {
                                return false;
                            } else {
                                return true;
                            }
                        }
                        /**
                         * Valida RN 024 - Formatos permitidos
                         */
                        scope.validateFileExtensions = function(fileName) {
                            var fileExtensioRegExt = /(?:\.([^.]+))?$/;
                            var ext = fileExtensioRegExt.exec(fileName)[1];
                            var acceptedExtensions = ['doc', 'docx', 'pdf', 'xls', 'xlsx', 'txt', 'jpg', 'gif', 'png', 'bmp', 'html', 'swf', 'xml', 'htmlx', 'tif', 'odt', 'pptx', 'ppt', 'xlf', 'msg', 'eml', 'dbx', 'zip', 'rar', 'gtm', 'kml', 'kmz', 'jpeg'];

                            if (ext && _.contains(acceptedExtensions, ext)) {
                                return true;
                            } else {
                                return false;
                            }
                        };

                        scope.disableHeader = true;
                        scope.downloadLink = function(path, fileName) {
                            return configuration.localPath + 'api/anexo/' +encodeURIComponent(fileName) + '?id=' + encodeURIComponent(path);
                        };
                    }
                }
            }
        }
    });
