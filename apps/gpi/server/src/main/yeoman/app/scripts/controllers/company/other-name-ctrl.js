'use strict';

angular.module('gpifrontApp')
    .controller('OtherNameCtrl', function($scope, Restangular, $filter, $rootScope, GridButton,growl) {


        $scope.otherNames = [];

        // $scope.$parent.empresa.nomesEmpresa;

        $scope.selectedItems = [];

        $scope.deletedItems = [];

        $scope.columnDefs = [{
            field: 'nome',
            enableCellEdit: true,
            width: "*",
            translationKey: 'common.name',
            editableCellTemplate: "<input maxlength='200' ng-class=\"'colt' + col.index\" ng-input=\"COL_FIELD\" ng-model=\"COL_FIELD\" />"
        }];

        function deleteFn() {
            growl.info($filter('translate')('message.error.MSG38_SECAO_ATUALIZADA',{value:'&quot;Outros Nomes&quot;'}));
        }

        // function newFn() {
        //     $scope.$parent.empresa.nomesEmpresa.unshift({"empresa_id": $scope.$parent.empresa.id});

        //     return false;
        // }

        $scope.buttonsConfig = {
            //function GridButton(show, translationKey, searchMode, disable, action, screenLink)
            newButton: new GridButton(true, 'company.new.name', false, null, null, ''),
            deleteButton: new GridButton(true, 'common.delete', false, null, deleteFn, '')

        }

        var changeColumnDisplayNames = function() {
            for (var i = 0; i < $scope.columnDefs.length; i++) {
                $scope.columnDefs[i].displayName = $scope.columnDefs[i].translationFn();
            };
        };


        $scope.$parent.$watch('empresa', function(newVal, oldVal) {
            setTimeout(function() {
                $scope.otherNames = newVal.nomesEmpresa;
            })
        }, true);

        $scope.sortInfo = {
            fields: ['nome'],
            directions: ['asc']
        };

    });
