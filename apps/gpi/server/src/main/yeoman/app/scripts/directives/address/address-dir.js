'use strict';

angular.module('gpifrontApp')
    .controller('AddressCtrl', function($scope, Restangular) {
        
        var baseDomain = "domain/";
        var ENDERECO_BRASIL = 'BRASIL';

        $scope.paises = [];
        $scope.estados = [];
        $scope.municipios = [];
        $scope.municipiosEncontrados = [];        

        var setPaises = function() {
            var basePais = Restangular.all(baseDomain + "pais/nome/asc");                 
            basePais.getList().then(function(pais) {
                $scope.paises = pais;
            });            
        }

        var setUfs = function() {        
            var baseUf = Restangular.all(baseDomain + "uf");                 
            baseUf.getList().then(function(ufs) {
                $scope.ufs = ufs;
            });
        }

        var setMunicipios = function() {
            var baseMunicipios = Restangular.all(baseDomain + "municipio/nome/asc");                 
            baseMunicipios.getList().then(function(municipios) {
                $scope.municipios = municipios;                
                if ($scope.municipiosEncontrados.length == 0) {
                    $scope.municipiosEncontrados = municipios;
                }

            });                    
        }            
            
        $scope.loadMunicipios = function() {            
            var selectedEstado = angular.element("select[name='address-state-select'] option:selected").val();

            $scope.endereco.municipio = null;            
            $scope.municipiosEncontrados = [];        

            for (var i =0; i < $scope.municipios.length; i++) {
                if ($scope.municipios[i].uf && $scope.municipios[i].uf.id == selectedEstado) {
                    $scope.municipiosEncontrados.push($scope.municipios[i]);
                } 
            }            
        }

        $scope.isEnderecoBrasil = function() {
            return $scope.endereco && $scope.endereco.pais && $scope.endereco.pais.nome === ENDERECO_BRASIL;
        }

        setPaises();
        setUfs();
        setMunicipios();

    })
    .directive('address', function(configuration) {
    	return {
            restrict: 'E',
            templateUrl: configuration.rootDir + 'scripts/directives/address/address-dir-tpl.html',
            controller: 'AddressCtrl',
            scope: {
                endereco: '=', 
                readonly: '='
            }
    	}

    })
