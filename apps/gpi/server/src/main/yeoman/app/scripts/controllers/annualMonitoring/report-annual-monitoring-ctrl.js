'use strict'
angular.module('gpifrontApp')
    .controller('ReportAnnualMonitoringCtrl', function($scope, growl, ESTAGIO_PROJETO, AuthService, AUTH_ROLES, Restangular, $translate, $http,
        $filter, configuration, $window, ngTableParams, $rootScope, UserSession) {

        $scope.loggedUser = UserSession.getUser();

        var formSubmitted = false;
        $scope.relatorioAnualAcompanhamentoList = [];
        $scope.relatorioAnualAcompanhamentoSelectedItems = [];

        /*
         *   Recupera todos os ids da lista passada
         */
        var getIdList = function(list) {
            var idList = [];
            if (_.isArray(list)) {
                _.each(list, function(val, key) {
                    idList.push(val.id);
                });
            } else {
                idList.push(list.id);
            }
            return idList;
        }

        $scope.checkValidation = function(formField) {
            if (!angular.isUndefined(formField)) {
                return formField.$invalid && (formField.$dirty || formSubmitted);
            }
        }        

        // RNE163 se for gerente não exibe o campo Gerência e se for analista de portfolio
        //  não exibe os campos Gerência e Analista
        $scope.isGerente = function() {
            return AuthService.isUserAuthorized([AUTH_ROLES.manager]);
        }

        var initialize = function() {
            $scope.resetFilters();
        }

        $scope.resetFilters = function() {
            $scope.filtro = {
                situacao: 'NAO_SOLICITADO',
                departamentos: [],
                regioesPlanejamento: [],
                cadeiasProdutivas: [],
                usuarioResponsavel: [],
                estagios: [],
                versao: true,
                notShowRealized: true
            };

            $scope.estagios = [ESTAGIO_PROJETO.FORMALIZADA, ESTAGIO_PROJETO.IMPLANTACAO_INICIADO, ESTAGIO_PROJETO.OPERACAO_INICIADA];

            Restangular.all('domain/cadeiaProdutiva').getList().then(function(result) {
                $scope.cadeiasProdutivas = result;
            });

            Restangular.all('domain/departamento').getList().then(function(departamentos) {
                $scope.gerencias = departamentos;
                //  Por default todas as gerencias vem selecionadas
                // $scope.filtro.departamentos = getIdList(departamentos);
                $scope.changeManageMultiSelect();
            });

            if (AuthService.isUserAuthorized([AUTH_ROLES.manager])) {
                Restangular.one('user/' + $scope.loggedUser.id).get().then(function(user) {
                    $scope.gerencias = user.departamento;
                    //  Aciona o evento onChange do select de departamentos
                    //  para preencher os analistas e as cadeias produtivas
                    //$scope.changeManageMultiSelect();
                });
            }
        }

        var validate = function() {
            var valid = true;
            var invalidAno = typeof $scope.filtro.ano === 'undefined' || !$scope.filtro.ano || $scope.filtro.ano.trim() === '';

            if (invalidAno === true) {
                valid = false;
            }

            return valid;
        }

        function translate(translationKey) {
            var text = " ";
            if (typeof translationKey !== 'undefined' && translationKey) {
                text = $filter('translate')(translationKey);
            }
            return text;
        }

        $scope.clean = function() {
            $scope.resetFilters();
            $scope.relatorioAnualAcompanhamentoList = [];
        }

        $scope.search = function(isFormValid) {
            if (validate() === true) {
                searchWithFilters($scope.filtro);
            } else {
                growl.error(translate("message.error.MSGXX_DADOS_INVALIDOS"));
                formSubmitted = true;
                return false;
            }            
        };

        var searchWithFilters = function(filtro) {
            if ($scope.filtro.estagios.length === 0) {
                $scope.filtro.estagios = [ESTAGIO_PROJETO.FORMALIZADA.id, ESTAGIO_PROJETO.IMPLANTACAO_INICIADO.id, ESTAGIO_PROJETO.OPERACAO_INICIADA.id];
            }

            Restangular.all('projeto/acompanhamentoAnual/gerar').getList(filtro).then(function(result) {
                $scope.relatorioAnualAcompanhamentoList = result;

                if (!$scope.relatorioAnualAcompanhamentoList || 
                    $scope.relatorioAnualAcompanhamentoList.length <= 0) {
                    growl.error(translate('message.error.MSG004_NENHUM_REGISTRO_ENCONTRADO'));
                }
            }, function errorCallback(response) {
                if (response.status !== 404) {
                    growl.error("Error: The server returned status " + response.status);
                }
            });            
        }

        initialize();

        /*
         *   Evento chamado ao selecionar gerência
         */
        $scope.changeManageMultiSelect = function() {
            //  Busca os usuários com base no departamento
            if ($scope.filtro.departamentos && $scope.filtro.departamentos.length > 0) {
                Restangular.all('user/byDepartamentos').getList({
                    "departamentos": $scope.filtro.departamentos
                }).then(function(analistas) {
                    $scope.analistas = analistas;
                    //  Por default todos os analistas vem selecionados
                    //$scope.filtro.usuariosResponsavel = [UserSession.getUser().id];
                });

                // Restangular.all('cadeiaProdutiva/byDepartamentos').getList({
                //     "departamentos": $scope.filtro.departamentos
                // }).then(function(cadeiasProdutivas) {
                //     $scope.cadeiasProdutivas = cadeiasProdutivas;
                //     //  Por default todas as cadeiasProdutivas vem selecionadas
                //     // $scope.filtro.cadeiasProdutivas = getIdList(cadeiasProdutivas);
                // });
            }

            $scope.analistas = [];
            $scope.filtro.usuarioResponsavel = [];            
        }


        /*
        Envia um e-mail para o contato responsável pela área do 
        investidor e cria a primeira campanha de preenchimento
        */
        $scope.sendFirstCampaign = function() {
            var existeEmailInvalido = false;
            $scope.projetos = [];
            $scope.projetos.push($scope.filtro.ano);
            if ($scope.relatorioAnualAcompanhamentoSelectedItems.length > 0) {
                $scope.relatorioAnualAcompanhamentoSelectedItems.forEach(function(selected) {
                    if (selected.emailResponsavelAreaInvestidor === null || selected.emailResponsavelAreaInvestidor.trim() === '') {
                        existeEmailInvalido = true;
                    }
                    $scope.projetos.push(selected.id);
                    //$scope.projetos.push(selected.protocolo);
                });
            }

            if (existeEmailInvalido === true) {
                growl.error(translate("message.error.MSG98_EMAIL_NAO_ENCONTRADO"));
            } else {
                var operation = Restangular.all('projeto/acompanhamentoAnual/enviarPrimeiraCampanha');
                    operation.post($scope.projetos).then(function(result) {
                    if (result == "1") {
                        growl.success(translate("message.success.MSG006_REGISTRO_GRAVADO_SUCESSO"));    
                    }
                    growl.success(translate("message.success.MSG97_EMAIL_ENVIADO"));
                }, function() {
                    growl.error("Ocorreu um erro ao tentar gravar o registro!");
                });
            }
            $scope.search();
        }

        /*Envia um e-mail para o contato responsável pela área do 
        investidor e cria a segunda campanha de preenchimento*/
        $scope.sendSecondCampaign = function() {
            var existeEmailInvalido = false;
            $scope.projetos = [];
            $scope.projetos.push($scope.filtro.ano);
            if ($scope.relatorioAnualAcompanhamentoSelectedItems.length > 0) {
                $scope.relatorioAnualAcompanhamentoSelectedItems.forEach(function(selected) {
                    if (selected.emailResponsavelAreaInvestidor === null || selected.emailResponsavelAreaInvestidor.trim() === '') {
                        existeEmailInvalido = true;
                    }
                    $scope.projetos.push(selected.id);
                });
            }

            if (existeEmailInvalido === true) {
                growl.error(translate("message.error.MSG98_EMAIL_NAO_ENCONTRADO"));
            } else {
                var operation = Restangular.all('projeto/acompanhamentoAnual/enviarSegundaCampanha');
                    operation.post($scope.projetos).then(function(result) {
                    if (result == "1") {
                        growl.success(translate("message.success.MSG006_REGISTRO_GRAVADO_SUCESSO"));
                    }
                    growl.success(translate("message.success.MSG97_EMAIL_ENVIADO"));
                }, function() {
                    growl.error("Ocorreu um erro ao tentar gravar o registro!");
                });
            }
            $scope.search();
        }

    });
