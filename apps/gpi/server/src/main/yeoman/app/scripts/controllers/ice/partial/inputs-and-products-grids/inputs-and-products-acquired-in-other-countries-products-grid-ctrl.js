'use strict';

angular.module('gpifrontApp')
    .controller('InputsAndProductsAcquiredInOtherCountriesProductsGridCtrl', function($scope, Restangular, $filter, $rootScope, GridButton, growl) {

        $scope.produtosImpParaCom = $scope.$parent.insumoProduto.produtosImpParaCom;

        $scope.selectedItems = [];

        // $scope.gridScope = {
        //     ncmList: [],
        //     ncmProperties: {}
        // };

        // $scope.$parent.$watch('ncmList', function(newVal, oldVal) {
        //     if (newVal !== oldVal) {
        //         $scope.gridScope.ncmList = newVal;
        //     }
        // }, true);
        // // $scope.gridScope.ncmProperties = NCM_CONSTANT;
        // $scope.$parent.$watch('ncmProperties', function(newVal, oldVal) {
        //     if (newVal !== oldVal) {
        //         // $scope.gridScope.ncmProperties = newVal;
        //         // atribuindo aqui porque neste momento o gridScope já foi preenchido
        //     }
        // }, true);

        $scope.ncmCellTemplate = '<div class=\"ngCellText\" ng-class=\"col.colIndex()\"><span ng-cell-text>{{row.entity.ncm.id}}</span></div>';
        $scope.sujetoSTCellTemplate = '<div class=\"ngCellText\" ng-class=\"col.colIndex()\"><span ng-cell-text>{{row.entity.sujeitoST === true || row.entity.sujeitoST === \'true\' ? \'Sim\' : (row.entity.sujeitoST === false || row.entity.sujeitoST === \'false\' ? \'Não\' : \'\')}}</span></div>';
        $scope.cellNCMSelectEditableTemplate = '<select placeholder="{{\'common.ncm\' | translate}}" ng-class="\'colt\' + col.index" ng-input="COL_FIELD" ng-model="row.entity.ncm" ng-options="ncm as ncm.id for ncm in ncmList" ng-change="row.entity.nome = row.entity.ncm.descricao"><option value="" translate>common.select</option></select>';
        $scope.cellSujeitoStSelectEditableTemplate = '<select placeholder="{{\'inputsandproducts.st.subject\' | translate}}" ng-class="\'colt\' + col.index" ng-input="COL_FIELD" ng-model="row.entity.sujeitoST">'+
            '<option value="false" ng-selected=\"row.entity.sujeitoST === false || row.entity.sujeitoST === \'false\' \"  translate>common.no</option>' +
            '<option value="true" ng-selected=\"row.entity.sujeitoST === true || row.entity.sujeitoST === \'true\' \" translate>common.yes</option></select>';

        $scope.columnDefs = [{
            field: 'ncm',
            enableCellEdit: true,
            width: "**",
            translationKey: 'common.ncm',
            editableCellTemplate: $scope.cellNCMSelectEditableTemplate,
            cellTemplate: $scope.ncmCellTemplate
            // cellFilter: "mapStatus:ncmList"

        }, {
            field: 'nome',
            enableCellEdit: true,
            width: "**",
            translationKey: 'common.product',
            editableCellTemplate: '<input type="text" placeholder="{{\'common.inputs\' | translate}}" ng-class="\'colt\' + col.index"  ng-input="COL_FIELD" ng-model="COL_FIELD" maxlength="200"/>'

        }, {
            field: 'sujeitoST',
            enableCellEdit: true,
            width: "*",
            translationKey: 'inputsandproducts.st.subject',
            editableCellTemplate: $scope.cellSujeitoStSelectEditableTemplate,
            cellTemplate: $scope.sujetoSTCellTemplate
            // cellFilter: "mapStatus"
        }, {
            field: 'quantidade1',
            enableCellEdit: true,
            width: "*",
            translationKey: 'Ano 1 (Quantidade)',
            editableCellTemplate: '<input type="numeric" placeholder="{{\'Ano 1 (Quantidade)\' | translate}}" ng-class="\'colt\' + col.index" maxlength="18" ng-input="COL_FIELD" ng-model="COL_FIELD"/>'

        }, {
            field: 'quantidade2',
            enableCellEdit: true,
            width: "*",
            translationKey: 'Ano 2 (Quantidade)',
            editableCellTemplate: '<input type="numeric" placeholder="{{\'Ano 2 (Quantidade)\' | translate}}" ng-class="\'colt\' + col.index" maxlength="18" ng-input="COL_FIELD" ng-model="COL_FIELD"/>'

        }, {
            field: 'quantidade3',
            enableCellEdit: true,
            width: "*",
            translationKey: 'Ano 3 (Quantidade)',
            editableCellTemplate: '<input type="numeric" placeholder="{{\'Ano 3 (Quantidade)\' | translate}}" ng-class="\'colt\' + col.index" maxlength="18" ng-input="COL_FIELD" ng-model="COL_FIELD"/>'

        }, {
            field: 'unidadeMedida',
            enableCellEdit: true,
            width: "**",
            translationKey: 'inputsandproducts.measurement.unit2',
            editableCellTemplate: '<input type="text" placeholder="{{\'inputsandproducts.measurement.unit2\' | translate}}" ng-class="\'colt\' + col.index" maxlength="50" ng-input="COL_FIELD" ng-model="COL_FIELD" />'
        }];


        $scope.buttonsConfig = {
            //function GridButton(show, translationKey, searchMode, disable, action, screenLink)
            newButton: new GridButton(true, 'common.new', false, null, null, ''),
            deleteButton: new GridButton(true, 'common.delete', false, null, deleteFn, '')
        }

        function deleteFn() {
            growl.info($filter('translate')('message.error.MSG38_SECAO_ATUALIZADA', {
                value: "Produtos Adquiridos em Outros Paises"
            }));
        }

        $scope.sortInfo = {
            fields: ['nome'],
            directions: ['asc']
        };

        $scope.$parent.$watch('insumoProduto', function(newVal, oldVal) {
            if (newVal.produtosImpParaCom !== oldVal.produtosImpParaCom) {
                $scope.produtosImpParaCom = newVal.produtosImpParaCom;
            }
        }, true);
    });