'use strict';
angular.module('gpifrontApp')
    .controller('ProjectAttachedFilesGridCtrl', function($scope, GridButton, dialogs, $filter, growl) {

        $scope.gridScope = {};

        $scope.sortInfo = {
            fields: ['fileName'],
            directions: ['asc']
        };

        $scope.buttonsConfig = {
            //function GridButton(show, translationKey, searchMode, disable, action, screenLink)
            newButton: new GridButton(true, 'common.attach.file', false, null, null, ''),
            deleteButton: new GridButton(true, 'common.delete', false, null, null, '')
        }

        //Grid de Arquivos
        $scope.projeto.anexos = $scope.$parent.projeto.anexos;
        $scope.attachedSelectedFiles = [];

        $scope.attachedFilesColumnDefs = [{
            field: "nome",
            translationKey: 'common.file.name',
            enableCellEdit: false
        }, {
            field: "dataAnexo",
            translationKey: 'common.date',
            enableCellEdit: false,
            cellFilter: 'dateTransformFilter'
        }];

        $scope.disableFn = function () {
            return $scope.attachedSelectedFiles.length < 1;
        }

        function translate(translationKey) {
            var text = "";
            if (typeof translationKey !== 'undefined' && translationKey) {
                text = $filter('translate')(translationKey);
            }
            return text;
        };

        $scope.preDeleteFile = function() {
            var confirmDialog = dialogs.confirm(translate('DIALOGS_CONFIRMATION'), translate('message.confirm.MSG009_CONFIRMAR_EXCLUSAO'));
            confirmDialog.result.then(function(btn) {
                // Caso confirme executa a exclusão
                $scope.deleteFile();

                growl.info($filter('translate')('message.error.MSG38_SECAO_ATUALIZADA', {value: translate("common.attached.files")}));
            }, function(btn) {
                $scope.gridScope.checkSelectAllCheckBox(true, true);
                $scope.gridScope.checkSelectAllCheckBox(false, true);
                // Caso não confirme deseleciona os registros selecionados
                $scope.attachedSelectedFiles.splice(0, $scope.attachedSelectedFiles.length);
            });
        }

        $scope.deleteFile = function() {
            if ($scope.projeto.anexos.length > 0 && $scope.attachedSelectedFiles.length > 0) {
                $scope.attachedSelectedFiles.forEach(function(selected) {
                    var model = $scope.projeto.anexos;
                    var indexOnMainList = model.indexOf(selected);
                    // deleting each unit from main list
                    $scope.projeto.anexos.splice(indexOnMainList, 1);
                    for (var i = 0; i < $scope.$flow.files.length; i++) {
                        var file = $scope.$flow.files[i];
                        file.cancel();
                    };
                });
                // deleting all 'selectedItems' starting at index 0
                $scope.attachedSelectedFiles.splice(0, $scope.attachedSelectedFiles.length);
            }
        }
    });
