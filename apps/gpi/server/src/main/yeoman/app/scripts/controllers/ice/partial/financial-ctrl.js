'use strict';
angular.module('gpifrontApp')
    .controller('IceFinancialCtrl', function($scope, $translate, $filter, growl, $rootScope, AuthService, $stateParams, AUTH_ROLES, CHANGE_FINANCEIRO, ScreenService, Restangular, CADEIA_PRODUTIVA, ngTableParams, TIPO_INVESTIMENTO, TIPO_FATURAMENTO, dialogs, SITUACAO_PROJETO, $state) {

        var baseProjeto = Restangular.all("projeto")

        var tipoInvestimento1Rows = [
            TIPO_INVESTIMENTO["INVESTIMENTO_TERRENO"],
            TIPO_INVESTIMENTO["INVESTIMENTO_ESTUDO_PROJETO_DESP_PRE_OPERACIONAIS"],
            TIPO_INVESTIMENTO["INVESTIMENTO_OBRAS_CIVIS_INSTALACOES"],
            TIPO_INVESTIMENTO["INVESTIMENTO_MAQ_EQUIP_NACIONAIS"],
            TIPO_INVESTIMENTO["INVESTIMENTO_MAQ_EQUIP_IMPORTADOS"],
            TIPO_INVESTIMENTO["INVESTIMENTO_MAQ_EQUIP_USADOS"],
            TIPO_INVESTIMENTO["INVESTIMENTO_CAPITAL_GIRO"],
            TIPO_INVESTIMENTO["OUTROS_INVESTIMENTOS"],
        ];

        var tipoInvestimento2Rows = [
            TIPO_INVESTIMENTO["RECURSOS_PROPRIOS"]
        ];

        var tipoInvestimento3Rows = [
            TIPO_INVESTIMENTO["BDMG"],
            TIPO_INVESTIMENTO["OUTRAS_FONTES"]
        ];

        $scope.changes = [];
        $scope.change_financeiro = CHANGE_FINANCEIRO;

        $scope.setInvestimentosPrevistos = function() {
            var investList = [];
            if ($scope.isCadeiaProdutivaSucroEnergetico()) {
                //Somente a cadeia produtiva Sucroenergetico
                angular.forEach($scope.investimentoPrevisto2, function(investimento) {

                    investList.push({
                        id: investimento.idIndustrial,
                        valor: investimento.valorIndustrial,
                        tipoInvestimento: TIPO_INVESTIMENTO["INVESTIMENTO_INDUSTRIAL"].id,
                        ano: investimento.ano
                    });

                    investList.push({
                        id: investimento.idAgricola,
                        valor: investimento.valorAgricola,
                        tipoInvestimento: TIPO_INVESTIMENTO["INVESTIMENTO_AGRICOLA"].id,
                        ano: investimento.ano
                    });

                    investList.push({
                        id: investimento.idCapacitacao,
                        valor: investimento.valorCapacitacao,
                        tipoInvestimento: TIPO_INVESTIMENTO["INVESTIMENTO_CAPACITACAO_PROFISSIONAL"].id,
                        ano: investimento.ano
                    });

                    investList.push({
                        id: investimento.idProprios,
                        valor: investimento.valorProprios,
                        tipoInvestimento: TIPO_INVESTIMENTO["INVESTIMENTO_PROPRIOS"].id,
                        ano: investimento.ano
                    });

                });
            } else {
                //Todas as cadeias produtivas, exceto sucroenergetico
                angular.forEach($scope.investimentoPrevisto, function(investimento) {

                    investList.push({
                        id: investimento.idMaquinasEquip,
                        valor: investimento.valorMaquinasEquip,
                        tipoInvestimento: TIPO_INVESTIMENTO["INVESTIMENTO_MAQ_EQUIP_TERRENO_OBRAS"].id,
                        ano: investimento.ano
                    });

                    investList.push({
                        id: investimento.idCapitalGiroEOutros,
                        valor: investimento.valorCapitalGiroEOutros,
                        tipoInvestimento: TIPO_INVESTIMENTO["INVESTIMENTO_CAPITAL_GIRO_E_OUTROS"].id,
                        ano: investimento.ano
                    });

                })
            }

            $scope.financeiro.investimentoPrevistoList = investList;
        }

        $scope.externalUser = function() {
            return AuthService.isUserAuthorized([AUTH_ROLES.externalResponsible, AUTH_ROLES.externalMeling, AUTH_ROLES.externalOther]);
        }

        $scope.setFaturamentos = function() {
            var fatAntList = [];
            angular.forEach($scope.faturamentoAnterior, function(faturamentoAnterior) {

                fatAntList.push({
                    id: faturamentoAnterior.idGrupo,
                    valor: faturamentoAnterior.valorGrupo,
                    tipoFaturamento: TIPO_FATURAMENTO["FATURAMENTO_DO_GRUPO"].id,
                    ano: faturamentoAnterior.ano
                });

                fatAntList.push({
                    id: faturamentoAnterior.idMinasGerais,
                    valor: faturamentoAnterior.valorMinasGerais,
                    tipoFaturamento: TIPO_FATURAMENTO["FATURAMENTO_REALIZADO_EM_MG"].id,
                    ano: faturamentoAnterior.ano
                });

            })

            $scope.financeiro.faturamentoAnteriorList = fatAntList;

            var fatPrevList = [];
            if ($scope.isCadeiaProdutivaEletricoEletroeletronico()) {
                //Somente a cadeia produtiva eletrico e eletroeletrônicos
                angular.forEach($scope.faturamentoPrevisto1, function(faturamento1) {

                    fatPrevList.push({
                        id: faturamento1.idIndustrializados,
                        valor: faturamento1.valorIndustrializados,
                        tipoFaturamento: TIPO_FATURAMENTO["FATURAMENTO_PRODUTOS_INDUSTRIALIZADOS"].id,
                        ano: faturamento1.ano
                    });

                    fatPrevList.push({
                        id: faturamento1.idAdquiridosComercializacao,
                        valor: faturamento1.valorAdquiridosComercializacao,
                        tipoFaturamento: TIPO_FATURAMENTO["FATURAMENTO_PRODUTOS_ADQ_PARA_COMERCIALIZACAO"].id,
                        ano: faturamento1.ano
                    });

                })
            } else {
                //Todas as cadeias produtivas, exceto eletrico e eletroeletrônicos.
                angular.forEach($scope.faturamentoPrevisto2, function(faturamento2) {

                    fatPrevList.push({
                        id: faturamento2.id,
                        valor: faturamento2.valor,
                        tipoFaturamento: TIPO_FATURAMENTO["FATURAMENTO_PREVISTO"].id,
                        ano: faturamento2.ano
                    });
                });

            }

            $scope.financeiro.faturamentoPrevistoList = fatPrevList;
        }

        $scope.setOrigemRecurso = function() {
            if ($scope.financeiro !== undefined && $scope.origemRecurso !== undefined && $scope.origemIdentificada) {
                $scope.financeiro.origemRecursoList = $scope.origemRecurso;
            } else if (!$scope.origemIdentificada) {
                $scope.financeiro.origemRecursoList = [];
            }
        }

        $scope.getUsoFonteDescricaoAndTipoListByTipoInvestimentoRows = function(tipoInvestimentoRows) {
            var descricaoAndTipoList = [];
            for (var i = tipoInvestimentoRows.length - 1; i >= 0; i--) {
                descricaoAndTipoList[i] = new Object();
                descricaoAndTipoList[i].descricao = tipoInvestimentoRows[i].descricao;
                descricaoAndTipoList[i].tipo = tipoInvestimentoRows[i].id;
            }
            return descricaoAndTipoList;
        }

        $scope.putUsoFonteRowsTab = function() {
            $scope.tabUsoFonte1 = $scope.getUsoFonteDescricaoAndTipoListByTipoInvestimentoRows(tipoInvestimento1Rows);
            $scope.tabUsoFonte2 = $scope.getUsoFonteDescricaoAndTipoListByTipoInvestimentoRows(tipoInvestimento2Rows);
            $scope.tabUsoFonte3 = $scope.getUsoFonteDescricaoAndTipoListByTipoInvestimentoRows(tipoInvestimento3Rows);
        }

        $scope.putUsoFonteData = function(arrTipoInvestimento) {
            var data = [];

            for (var i = arrTipoInvestimento.length - 1; i >= 0; i--) {
                if (typeof $scope.financeiro.usoFonteList !== 'undefined') {
                    data[i] = new Object();
                    angular.forEach($scope.financeiro.usoFonteList, function(usoFonte) {
                        if (arrTipoInvestimento[i].id === usoFonte.tipo) {
                            data[i] = usoFonte;
                        }
                    })
                    data[i].descricao = arrTipoInvestimento[i].descricao;
                    data[i].tipo = arrTipoInvestimento[i].id;
                }
            };

            return data;
        }

        $scope.getUsoFonte = function() {

            $scope.tabUsoFonte1 = $scope.putUsoFonteData(tipoInvestimento1Rows);

            if (typeof $scope.tabUsoFonte1 !== 'undefined') {
                $scope.tableParams = new ngTableParams({
                    page: 1,
                    // count: 0,
                    sorting: {
                        time: 'asc'
                    }
                }, {
                    counts: []
                }, {
                    total: $scope.tabUsoFonte1.length,
                    getData: function($defer, params) {
                        var orderedData = params.sorting() ? $filter('orderBy')($scope.tabUsoFonte1, params.orderBy()) : $scope.tabUsoFonte1;
                        $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                    }
                })
            }

            $scope.tabUsoFonte2 = $scope.putUsoFonteData(tipoInvestimento2Rows);

            if (typeof $scope.tabUsoFonte2 !== 'undefined') {
                $scope.tableParams = new ngTableParams({
                    page: 1,
                    // count: 0,
                    sorting: {
                        time: 'asc'
                    }
                }, {
                    counts: []
                }, {
                    total: $scope.tabUsoFonte2.length,
                    getData: function($defer, params) {
                        var orderedData = params.sorting() ? $filter('orderBy')($scope.tabUsoFonte2, params.orderBy()) : $scope.tabUsoFonte2;
                        $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                    }
                })
            }

            $scope.tabUsoFonte3 = $scope.putUsoFonteData(tipoInvestimento3Rows);

            if (typeof $scope.tabUsoFonte3 !== 'undefined') {
                $scope.tableParams = new ngTableParams({
                    page: 1,
                    // count: 0,
                    sorting: {
                        time: 'asc'
                    }
                }, {
                    counts: []
                }, {
                    total: $scope.tabUsoFonte3.length,
                    getData: function($defer, params) {
                        var orderedData = params.sorting() ? $filter('orderBy')($scope.tabUsoFonte3, params.orderBy()) : $scope.tabUsoFonte3;
                        $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                    }
                })
            }

        }

        $scope.setUsoFonte = function() {
            $scope.financeiro.usoFonteList = [];

            var data = [];

            if (typeof $scope.tabUsoFonte1 !== 'undefined') {
                data = data.concat($scope.tabUsoFonte1);
            }
            if (typeof $scope.tabUsoFonte2 !== 'undefined') {
                data = data.concat($scope.tabUsoFonte2);
            }
            if (typeof $scope.tabUsoFonte3 !== 'undefined') {
                data = data.concat($scope.tabUsoFonte3);
            }

            for (var i = 0; i <= data.length - 1; i++) {
                if (data[i].valorRealizado !== null || data[i].valorARealizar !== null) {
                    $scope.financeiro.usoFonteList[i] = {
                        tipo: data[i].tipo
                    };
                    if (data[i].id !== undefined) {
                        $scope.financeiro.usoFonteList[i].id = data[i].id;
                    }    
                    if (data[i].valorRealizado !== undefined) {
                        $scope.financeiro.usoFonteList[i].valorRealizado = data[i].valorRealizado;
                    }
                    if (data[i].valorARealizar !== undefined) {
                        $scope.financeiro.usoFonteList[i].valorARealizar = data[i].valorARealizar;
                    }
                }

            };
        }

        $scope.updateOrigemRecurso = function() {
            $scope.somatorioTotalPercentual = 0;
            $scope.somatorioTotalValor = 0;
            $.each($scope.origemRecurso, function(index, origemRecurso) {
                var percentualInvestimento = origemRecurso.percentualInvestimento ? parseFloat(origemRecurso.percentualInvestimento) : 0;
                var percentual = percentualInvestimento ? (percentualInvestimento / 100) : 0;

                if ($scope.isCadeiaProdutivaSucroEnergetico()) {
                    origemRecurso.valorInvestimento = $scope.somatorioInvestimento2Total ? $scope.somatorioInvestimento2Total * percentual : 0;
                } else {
                    origemRecurso.valorInvestimento = $scope.somatorioInvestimentoTotal ? $scope.somatorioInvestimentoTotal * percentual : 0;
                }

                $scope.somatorioTotalValor += origemRecurso.valorInvestimento;
                $scope.somatorioTotalPercentual += percentualInvestimento;
                if($scope.somatorioTotalPercentual > 100){
                    growl.error($filter('translate')('message.error.MSG70_PERCENTUAL_ACIMA_PERMITIDO'));
                }
            });
        }

        $scope.initialize = function() {
            $scope.cadeiaProdutiva = CADEIA_PRODUTIVA;

            $scope.data = [];
            $scope.financeiro = {};
            $scope.screenService = ScreenService;
            $scope.disableFooter = true;
            $scope.origemIdentificada = true;
            $scope.anexosFinanceiro = [];
            $scope.projeto = {
                cadeiaProdutiva: {
                    id: -1
                }
            };

            $scope.investimentoPrevisto = [];
            $scope.investimentoPrevistoSelectedItems = [];

            $scope.investimentoPrevisto2 = [];
            $scope.investimentoPrevisto2SelectedItems = [];

            $scope.origemRecurso = [];
            $scope.origemRecursoSelectedItems = [];

            $scope.faturamentoAnterior = [];
            $scope.faturamentoAnteriorSelectedItems = [];

            $scope.faturamentoPrevisto1 = [];
            $scope.faturamentoPrevisto1SelectedItems = [];

            $scope.faturamentoPrevisto2 = [];
            $scope.faturamentoPrevisto2SelectedItems = [];

            baseProjeto.one($stateParams.id).get().then(function(result) {
                $scope.projeto = result;

                if (!$scope.externalUser() && $scope.projeto.situacaoAtual.situacao === SITUACAO_PROJETO.PENDENTE_ACEITE.id) {
                    baseProjeto.one($stateParams.id).getList("financeiro/findChange").then(function(result) {
                        $scope.changes = (result[0] === undefined ? 0 : result[0]);
                    }, function() {
                        growl.error("Ocorreu um erro ao tentar buscar o registro!");
                    });
                }

                if ($stateParams.screen != null) {
                    if ($stateParams.screen != $scope.type.INCLUIR && $stateParams.id) {
                        baseProjeto.one($stateParams.id).one("financeiro").getList().then(function(financeiro) {
                            if (financeiro.length > 0) {
                                $scope.financeiro = financeiro[0];
                                $scope.anexosFinanceiro = financeiro[0].anexos;

                                if (financeiro[0].origemRecursoList.length > 0) {
                                    $scope.origemRecurso = financeiro[0].origemRecursoList;
                                    $scope.origemIdentificada = true;
                                } else {
                                    $scope.origemIdentificada = false;
                                }

                                if ($scope.isCadeiaProdutivaSucroEnergetico()) {
                                    baseProjeto.one($stateParams.id).one("financeiro", financeiro[0].id).one("investimentoPrevisto2").getList().then(function(investimento) {
                                        if (investimento.length > 0) {
                                            $scope.investimentoPrevisto2 = investimento;
                                        }
                                    });
                                } else {
                                    baseProjeto.one($stateParams.id).one("financeiro", financeiro[0].id).one("investimentoPrevisto1").getList().then(function(investimento) {
                                        if (investimento.length > 0) {
                                            $scope.investimentoPrevisto = investimento;
                                        }
                                    });
                                }

                                if ($scope.isCadeiaProdutivaEletricoEletroeletronico()) {
                                    baseProjeto.one($stateParams.id).one("financeiro", financeiro[0].id).one("faturamentoPrevisto1").getList().then(function(faturamento) {
                                        if (faturamento.length > 0) {
                                            $scope.faturamentoPrevisto1 = faturamento;
                                        }
                                    });
                                } else {
                                    baseProjeto.one($stateParams.id).one("financeiro", financeiro[0].id).one("faturamentoPrevisto2").getList().then(function(faturamento) {
                                        if (faturamento.length > 0) {
                                            $scope.faturamentoPrevisto2 = faturamento;
                                        }
                                    });
                                }

                                baseProjeto.one($stateParams.id).one("financeiro", financeiro[0].id).one("faturamentoAnterior").getList().then(function(faturamento) {
                                    if (faturamento.length > 0) {
                                        $scope.faturamentoAnterior = faturamento;
                                    }
                                });

                                $scope.getUsoFonte();
                            }

                        })
                    }

                    $scope.putUsoFonteRowsTab();
                }

            });

        };       

        $scope.$watch('investimentoPrevisto', function(newVal, oldVal) {
            if ($scope.investimentoPrevisto) {
                setTimeout(function() {
                    $scope.somatorioMaquinasEquip = 0;
                    $scope.somatorioCapitalGiroEOutros = 0;
                    $scope.somatorioInvestimentoTotal = 0;
                    $.each($scope.investimentoPrevisto, function(index, investimento) {
                        investimento.valorMaquinasEquip = investimento.valorMaquinasEquip ? parseFloat(investimento.valorMaquinasEquip) : 0;
                        investimento.valorCapitalGiroEOutros = investimento.valorCapitalGiroEOutros ? parseFloat(investimento.valorCapitalGiroEOutros) : 0;
                        $scope.somatorioMaquinasEquip += investimento.valorMaquinasEquip;
                        $scope.somatorioCapitalGiroEOutros += investimento.valorCapitalGiroEOutros;
                        investimento.investimentoTotal = investimento.valorMaquinasEquip + investimento.valorCapitalGiroEOutros;
                        $scope.somatorioInvestimentoTotal = $scope.somatorioMaquinasEquip + $scope.somatorioCapitalGiroEOutros;
                    });
                    $scope.updateOrigemRecurso();
                }, true);
            }
        }, true);

        $scope.$watch('investimentoPrevisto2', function(newVal, oldVal) {
            if ($scope.investimentoPrevisto2) {
                setTimeout(function() {
                    $scope.somatorioIndustrial = 0;
                    $scope.somatorioAgricola = 0;
                    $scope.somatorioCapacitacao = 0;
                    $scope.somatorioProprios = 0;
                    $scope.somatorioInvestimento2Total = 0;
                    $.each($scope.investimentoPrevisto2, function(index, investimento) {
                        investimento.valorIndustrial = investimento.valorIndustrial ? parseFloat(investimento.valorIndustrial) : 0;
                        investimento.valorAgricola = investimento.valorAgricola ? parseFloat(investimento.valorAgricola) : 0;
                        investimento.valorCapacitacao = investimento.valorCapacitacao ? parseFloat(investimento.valorCapacitacao) : 0;
                        investimento.valorProprios = investimento.valorProprios ? parseFloat(investimento.valorProprios) : 0;
                        $scope.somatorioIndustrial += investimento.valorIndustrial;
                        $scope.somatorioAgricola += investimento.valorAgricola;
                        $scope.somatorioCapacitacao += investimento.valorCapacitacao;
                        $scope.somatorioProprios += investimento.valorProprios;
                    });
                    $scope.somatorioInvestimento2Total = $scope.somatorioIndustrial + $scope.somatorioAgricola + $scope.somatorioCapacitacao + $scope.somatorioProprios;
                    $scope.updateOrigemRecurso();
                }, true);
            }
        }, true);

        $scope.$watch('origemRecurso', function(newVal, oldVal) {
            if ($scope.origemRecurso) {
                setTimeout(function() {
                    $scope.updateOrigemRecurso();
                }, true);
            }
        }, true);

        $scope.$watch('tabUsoFonte1', function(newVal, oldVal) {
            if ($scope.tabUsoFonte1) {
                $scope.somatoriotabUsoFonte1TotalARealizar = 0;
                $scope.somatoriotabUsoFonte1TotalRealizado = 0;
                $scope.somatoriotabUsoFonte1TotalGeral = 0;
                $scope.somatorioInvestimentoCapitalGiro = 0;
                $.each($scope.tabUsoFonte1, function(index, usoFonte) {
                    var valorARealizar = usoFonte.valorARealizar ? usoFonte.valorARealizar : 0;
                    var valorRealizado = usoFonte.valorRealizado ? usoFonte.valorRealizado : 0;
                    $scope.somatoriotabUsoFonte1TotalARealizar += valorARealizar;
                    $scope.somatoriotabUsoFonte1TotalRealizado += valorRealizado;
                    usoFonte.valorTotal = valorARealizar + valorRealizado;
                    usoFonte.valorTotal = usoFonte.valorTotal.toFixed(2);

                    /* RNE167 - O somatório dos recursos (Capital de giro e Outros Investimentos) deve
                       ser igual ao somatório do Capital de giro e outros Investimentos (em R$)
                       do Investimento Previsto. */
                    if (usoFonte.tipo === TIPO_INVESTIMENTO["INVESTIMENTO_CAPITAL_GIRO"].id ||
                        usoFonte.tipo === TIPO_INVESTIMENTO["OUTROS_INVESTIMENTOS"].id) {
                        $scope.somatorioInvestimentoCapitalGiro += usoFonte.valorTotal;
                    }
                });
                $scope.somatoriotabUsoFonte1TotalGeral += $scope.somatoriotabUsoFonte1TotalARealizar + $scope.somatoriotabUsoFonte1TotalRealizado;
            }
        }, true);

        $scope.$watch('tabUsoFonte2', function(newVal, oldVal) {
            if ($scope.tabUsoFonte2) {
                $scope.somatoriotabUsoFonte2TotalGeral = 0;
                $.each($scope.tabUsoFonte2, function(index, usoFonte) {
                    var valorARealizar = usoFonte.valorARealizar ? usoFonte.valorARealizar : 0;
                    var valorRealizado = usoFonte.valorRealizado ? usoFonte.valorRealizado : 0;
                    usoFonte.valorTotal = valorARealizar + valorRealizado;
                    usoFonte.valorTotal = usoFonte.valorTotal.toFixed(2);
                    $scope.somatoriotabUsoFonte2TotalGeral = usoFonte.valorTotal;
                });
                $scope.calcularSomatorioTotais();
            }
        }, true);

        $scope.$watch('tabUsoFonte3', function(newVal, oldVal) {
            if ($scope.tabUsoFonte3) {
                $scope.calcularSomatorioTotais();
            }
        }, true);

        /*
         *   Utilizado para realizar o somatorio dos campos 'Recursos Próprios', 'BDMG' e 'Outras Fontes'
         */
        $scope.calcularSomatorioTotais = function() {
            $scope.somatoriotabUsoFonte3TotalARealizar = 0;
            $scope.somatoriotabUsoFonte3TotalRealizado = 0;
            $scope.somatoriotabUsoFonte3TotalGeral = 0;
            $.each($scope.tabUsoFonte3, function(index, usoFonte) {
                var valorARealizar = usoFonte.valorARealizar ? usoFonte.valorARealizar : 0;
                var valorRealizado = usoFonte.valorRealizado ? usoFonte.valorRealizado : 0;
                $scope.somatoriotabUsoFonte3TotalARealizar += valorARealizar;
                $scope.somatoriotabUsoFonte3TotalRealizado += valorRealizado;
                usoFonte.valorTotal = valorARealizar + valorRealizado;
                usoFonte.valorTotal = usoFonte.valorTotal.toFixed(2);
            });
            $scope.somatoriotabUsoFonte3TotalARealizar += $scope.tabUsoFonte2[0].valorARealizar ? $scope.tabUsoFonte2[0].valorARealizar : 0;
            $scope.somatoriotabUsoFonte3TotalRealizado += $scope.tabUsoFonte2[0].valorRealizado ? $scope.tabUsoFonte2[0].valorRealizado : 0;
            $scope.somatoriotabUsoFonte3TotalGeral += $scope.somatoriotabUsoFonte3TotalARealizar + $scope.somatoriotabUsoFonte3TotalRealizado;
        }

        $scope.isCadeiaProdutivaEnergiasHidricas = function() {
            return $scope.projeto.cadeiaProdutiva.id === $scope.cadeiaProdutiva['ENERGIAS_HIDRICAS'].id;
        }

        $scope.isCadeiaProdutivaSucroEnergetico = function() {
            return $scope.projeto.cadeiaProdutiva.id === $scope.cadeiaProdutiva['SUCROENERGETICO'].id;
        }

        $scope.isCadeiaProdutivaEletricoEletroeletronico = function() {
            return $scope.projeto.cadeiaProdutiva.id === $scope.cadeiaProdutiva['ELETRICO_ELETROELETRONICOS'].id;
        }

        $scope.save = function() {
            if ($stateParams.screen != $scope.type.INCLUIR && $scope.projeto) {
                $scope.setInvestimentosPrevistos();
                $scope.setUsoFonte();
                $scope.setFaturamentos();
                $scope.setOrigemRecurso();

                /* RNE131 - O GPI deverá validar em 100% a coluna Investimento (em %), calcular o percentual
                   informado baseado no somatório do "Investimento Total (em R$)" da seção Investimento
                   Previsto e realizar o somatorio da coluna "Investimento (em R$). Ou seja, o somatório
                   dos Investimentos (em R$) deverá ser igual ao somatório do Investimento Total (em R$). */
                if ($scope.origemRecurso !== undefined && $scope.origemRecurso.length > 0) {
                    if ($scope.somatorioTotalPercentual === undefined || $scope.somatorioTotalPercentual !== 100) {
                        growl.error("A soma dos investimentos (em %) na tabela Origem dos Recursos deve ser igual a 100%.");
                        return;
                    }
                }

                /*  RNE132 - O GPI deverá somar o valor  das colunas "Realizado" e "A Realizar" e informar no Total.
                    Depois validar o valor Total (coluna) com o valor total do "Investimento Total (em R$)" da seção Investimento Previsto.*/
                if ($scope.isCadeiaProdutivaSucroEnergetico()) {
                    if ($scope.somatoriotabUsoFonte1TotalGeral.toFixed(2) !== $scope.somatorioInvestimento2Total.toFixed(2)) {
                        growl.error("O somatório Total do \"Uso ou Aplicação de recursos\" deve ser igual ao somatórios do \"Investimento Industrial (em R$)\", \"Investimento Agrícola (em R$)\", \"Investimento em Capacitação Profissional dos Empregados (em R$)\" e \"Investimentos Próprios (em R$)\" da seção Investimento Previsto.");
                        return;
                    }
                } else {
                    if ($scope.somatoriotabUsoFonte1TotalGeral.toFixed(2) !== $scope.somatorioInvestimentoTotal.toFixed(2)) {
                        growl.error("O somatório Total do \"Uso ou Aplicação de recursos\" deve ser igual ao somatório do \"Investimento Total (em R$)\" da seção Investimento Previsto.");
                        return;
                    }
                }

                /*  RNE135 - O GPI deverá validar o valor Total (coluna) das Fontes de Recursos e Recursos de Terceiros com o
                    valor total do Uso ou Aplicações de recursos.*/
                if ($scope.somatoriotabUsoFonte3TotalGeral.toFixed(2) !== $scope.somatoriotabUsoFonte1TotalGeral.toFixed(2)) {
                    growl.error("O somatório Total \"Fontes de Recursos e Recursos de Terceiros\" deve ser igual ao somatório Total do \"Uso ou Aplicações de recursos\".");
                    return;
                }

                // /* RNE167 - O somatório dos recursos (Capital de giro e Outros Investimentos) deve
                //    ser igual ao somatório do Capital de giro e outros Investimentos (em R$)
                //    do Investimento Previsto. */
                // if ($scope.somatorioCapitalGiroEOutros !== $scope.somatorioInvestimentoCapitalGiro) {
                //     growl.error("O somatório dos recursos (Capital de giro e Outros Investimentos) deve ser igual ao somatório do Capital de giro e outros Investimentos (em R$) do Investimento Previsto.");
                //     return;
                // }

                baseProjeto.one($stateParams.id).post("financeiro", $scope.financeiro).then(function() {
                    growl.success($filter('translate')("message.success.MSG006_REGISTRO_GRAVADO_SUCESSO"));
                    $scope.formFinancial.$setPristine();
                    $state.go("base.project.ice.jobsTpl", {
                        screen: $scope.screenService.getScreenName(),
                        id: $stateParams.id
                    });
                }, function() {
                    growl.error("Ocorreu um erro ao tentar gravar o registro!");
                });

            } else {
                growl.error("Modo de tela invalido!");
            }
        }

        $scope.initialize();

    })
    .constant('CHANGE_FINANCEIRO', {
        INVESTIMENTOPREVISTO: {
            posicao: 0,
            descricao: "investimentoPrevisto"
        },
        ORIGEMIDENTIFICADA: {
            posicao: 1,
            descricao: "origemIdentificada"
        },
        TABUSOFONTE1: {
            posicao: 2,
            descricao: "tabUsoFonte1"
        },
        FATURAMENTOANTERIOR:{
            posicao: 3,
            descricao: "faturamentoAnterior"
        },
        FATURAMENTOPREVISTO: {
            posicao: 4,
            descricao: "faturamentoPrevisto"
        }
    });
