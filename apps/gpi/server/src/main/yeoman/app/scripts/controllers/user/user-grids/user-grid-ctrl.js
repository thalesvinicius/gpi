'use strict'

angular
	.module('gpifrontApp')
	.controller('UserGridCtrl', function($scope, $filter, GridButton, Restangular, growl, AUTH_ROLES, AuthService, $state, $stateParams, dialogs){

        $scope.gridScope = {};

        var baseUsuarios = Restangular.all('user');

        $scope.preDelete = function() {
            var confirmDialog = dialogs.confirm($filter('translate')('DIALOGS_CONFIRMATION'), $filter('translate')('message.confirm.MSG009_CONFIRMAR_EXCLUSAO'));
            confirmDialog.result.then(function(btn) {
                // Caso confirme executa a exclusão
                $scope.deleteFn();
            }, function(btn) {
                $scope.gridScope.checkSelectAllCheckBox(true, true);
                $scope.gridScope.checkSelectAllCheckBox(false, true);
                // Caso não confirme deseleciona os registros selecionados
                $scope.usuariosSelectedItems.splice(0, $scope.usuariosSelectedItems.length);
            });
        }

        //define some delete function if you want
        $scope.deleteFn = function() {
            var ids = [];
            for (var i = $scope.usuariosSelectedItems.length - 1; i >= 0; i--) {
                if ($scope.usuariosSelectedItems[i].ativo) {
                    growl.error("Não é permitido excluir registros com o Status igual a 'Ativo'.");
                    return false;
                } else {
                    ids.push($scope.usuariosSelectedItems[i].id);
                }
            };

            Restangular.all("projeto").customGETLIST("responsavelInvestimento", {"usersId": ids}).then(function(usuarioResponsavelList){
                if (eval(usuarioResponsavelList) > 0) {
                    growl.error('Não é permitido excluir um usuário que seja Responsável pelo investimento.')
                    return false;
                }
                else {
                    Restangular.all("user/removeUser").post(ids).then(function(){
                        $scope.gridScope.deleteFn();
                        $scope.$parent.filtro = {ativo: true};
                        $scope.$parent.search();
                        growl.success("Exclusão efetuada com sucesso.")
                    }, function(){
                        growl.error('Ocorreu um erro ao tentar efetuar a exclusão.')
                        return false;
                    })
                }

            })

            return true;
        }

        $scope.preActiveInactive = function() {
            var confirmDialog = dialogs.confirm($filter('translate')('DIALOGS_CONFIRMATION'), $filter('translate')('message.confirm.MSG011_CONFIRMAR_ATIVAR_INATIVAR'));
            confirmDialog.result.then(function(btn) {
                // Caso confirme executa a ativar/inativar
                $scope.activeInactiveFn();
            }, function(btn) {
                $scope.gridScope.checkSelectAllCheckBox(true, true);
                $scope.gridScope.checkSelectAllCheckBox(false, true);
                // Caso não confirme deseleciona os registros selecionados
                $scope.usuariosSelectedItems.splice(0, $scope.usuariosSelectedItems.length);
            });
        }

        $scope.activeInactiveFn = function() {
            for (var i = $scope.usuariosSelectedItems.length - 1; i >= 0; i--) {
                if ($scope.usuariosSelectedItems[i].ativo) {
                    $scope.usuariosSelectedItems[i].ativo = false;
                } else {
                    $scope.usuariosSelectedItems[i].ativo = true;
                }

                baseUsuarios.post($scope.usuariosSelectedItems[i]).then(function() {
                    $scope.$parent.reset();
                },
                function() {
                    growl.error("Ocorreu um erro ao tentar gravar o registro!");
                });
            };
            growl.success("Ativar/Inativar realizado com sucesso!");
        }

        //define some disable function if you need
        $scope.disableEditFn = function() {
            return $scope.usuariosSelectedItems.length !== 1 || !AuthService.isUserAuthorized(AUTH_ROLES.itAdmin);
        }

        $scope.disableNewFn = function() {
            return !AuthService.isUserAuthorized(AUTH_ROLES.itAdmin);
        }

        $scope.disableDeleteFn = function() {
            return $scope.usuariosSelectedItems.length == 0 || !AuthService.isUserAuthorized(AUTH_ROLES.itAdmin);
        }

        $scope.prepareNewFn = function() {
            $state.go('base.user',{screen:"incluir"});
        }

        $scope.prepareEditFn = function() {
            $state.go('base.user', {screen:'editar', id:$scope.usuariosSelectedItems[0].id});
        }

        // initialize its selected-items attribute
        $scope.usuariosSelectedItems = [];

        //Define its column-defs attribute
        $scope.usuariosColumnDefs = [{
            field: 'id',
            enableCellEdit: false,
            width: "50px",
            translationKey: 'common.id'
        }, {
            field: 'matricula',
            enableCellEdit: false,
            width: "100px",
            translationKey: 'common.registration'
        }, {
            field: 'nome',
            enableCellEdit: false,
            width: "*",
            translationKey: 'common.name'
        }, {
            field: 'cargo.descricao',
            enableCellEdit: false,
            width: "*",
            translationKey: 'common.role'
        }, {
            field: 'departamento.descricao',
            enableCellEdit: false,
            width: "**",
            translationKey: 'common.manage'
        }, {
            field: "ativo",
            enableCellEdit: false,
            width: "90px",
            translationKey: 'common.status',
            cellTemplate: '<div class="ngCellText" ng-switch on="row.entity.ativo"><span ng-class="\'colt\' + col.index" ng-switch-when="true" translate>common.active</span><span ng-class="\'colt\' + col.index" ng-switch-when="false" translate>common.inactive</span>'
        }, {
            field: 'show',
            translationKey: 'common.show',
            showScreenRedirect: true,
            width: "90px",
            showLink: '/usuario/visualizar'
        }];

        //define its buttons-config attribute on a search screen
        $scope.usuariosButtonsConfig = {

            // É necessário um botão de ativar/desativar usuário
            //changeStatusButton: new GridButton(true, 'common.delete', false, disableFn, changeStatusFn, ''),

            newButton: new GridButton(false, 'common.new', true, null, null, '/usuario/incluir'),
            // editButton: new GridButton(false, 'common.edit', true, disableEditFn, null, '/usuario/editar'),
            deleteButton: new GridButton(false, 'common.delete', false, null, null, '')
        }

        //define its sort-info attribute if you desire a default-sort
        $scope.usuariosSortInfo = {
            fields: ['id'],
            directions: ['asc']
        };

	})