'use strict';

angular.module('gpifrontApp')
    .service('UserSession', function($rootScope, AUTH_EVENTS, $state, Restangular, TIPO_USUARIO) {
        var user = {};
        if (localStorage && localStorage.getItem('user')) {
            user = JSON.parse(localStorage.getItem('user'));
        }
        var saveSession = function(user) {
            if (localStorage) {
                localStorage.setItem('user', JSON.stringify(user));
            }
        }
        this.createToken = function(accessToken, expiresIn) {
            Restangular.setDefaultHeaders({
                Authorization: "Bearer " + accessToken
            });
            user.accessToken = accessToken;
            user.expiresIn = expiresIn;
            user.lastActivity = new Date().getTime();
            saveSession(user);
        };
        this.createUser = function(currentUser) {
            user.name = currentUser.name;
            user.email = currentUser.email;
            user.id = currentUser.id;
            user.role = currentUser.role;
            user.tipo = currentUser.tipo;
            user.departamento = currentUser.departamento
            if (this.isExternalUser()) {
                user.empresaId = currentUser.empresaId;
                user.unidadeId = currentUser.unidadeId;
                user.isFromUnidade = currentUser.isFromUnidade;
            }
            saveSession(user);
        };
        this.getUser = function() {
            return user;
        };
        this.isExternalUser = function () {
            return user.tipo == TIPO_USUARIO.EXTERNO;
        }
        this.getStringRoles = function() {
            var returnArray = [];
            if (this.getAccessToken() && user.role) {
                returnArray.push(user.role.descricao);
            }
            return returnArray;
        };
        this.getAccessToken = function() {
            var token = null;
            if (user && user.lastActivity) {
                var now = new Date();
                var expiresInDate = new Date(user.lastActivity + (user.expiresIn * 1000));
                if (now < expiresInDate) {
                    user.lastActivity = new Date().getTime();
                    saveSession(user);
                    return user.accessToken;
                } else {
                    this.destroy();
                    $rootScope.$broadcast(AUTH_EVENTS.sessionTimeout, $state.current.name);
                }
            }
            return token;
        }
        this.destroy = function() {
            user = {};
            localStorage.removeItem('user');
        };
        return this;
    })
