'use strict'

angular
    .module('gpifrontApp')
    .controller('UserCtrl', function($scope, Restangular, $stateParams, growl, $translate, $rootScope, $state, $filter, ScreenService, TIPO_USUARIO) {
        $scope.screenService = ScreenService;

        var baseDomain = "domain/";
        var baseUsuarios = Restangular.all('user');
        var formSubmitted = false;

        $scope.formHasError = function(formField) {
            if (!angular.isUndefined(formField)) {                
                return formField.$invalid && (formField.$dirty || formSubmitted);
            }
        }

        $scope.type = {
            INCLUIR: {
                titlePath: 'user.add',
                icon: "fa-plus",
                disabled: false
            },
            EDITAR: {
                titlePath: 'user.edit',
                icon: "fa-edit",
                disabled: false
            },
            VISUALIZAR: {
                titlePath: 'user.visualize',
                icon: "fa-eye",
                disabled: true
            }
        }
        $scope.screenType = $scope.type[$stateParams.screen.toUpperCase()];

        $scope.usuario = {tipo: TIPO_USUARIO.INTERNO};
        if ($scope.screenType != null) {
            $scope.disabled = $scope.screenType.disabled;
            if ($scope.screenType != $scope.type.INCLUIR && $stateParams.id) {
                Restangular.one('user', $stateParams.id).get().then(function(user) {
                    $scope.usuario = user;
                });
            } else if ($scope.screenType == $scope.type.INCLUIR) {
                $scope.usuario.ativo = "true";
            }
        }

        $scope.cargos = [];
        Restangular.all(baseDomain + 'cargo').getList().then(function(cargos) {
            $scope.cargos = cargos;
        });

        $scope.departamentos = [];
        Restangular.all(baseDomain + 'departamento').getList().then(function(departamentos) {
            $scope.departamentos = departamentos;
        });

        $scope.gruposUsuarios = [];
        Restangular.all(baseDomain + 'grupoUsuario').getList().then(function(gruposUsuarios) {
            $scope.gruposUsuarios = gruposUsuarios;
        });

        $scope.search_LDAP = function() {
            $scope.dados = [];
            Restangular.one('user/pesquisaLDAP/' + $scope.usuario.matricula).getList().then(function(dados) {
                $scope.usuario.nome = dados[0];
                $scope.usuario.email = dados[1];
                $scope.usuario.telefone = dados[2];
                $scope.usuario.cargo = dados[3];
            });
        }

        $scope.save = function(isFormValid) {
            formSubmitted = true;

            if (!isFormValid) {
                growl.error($filter('translate')('message.error.MSGXX_DADOS_INVALIDOS'));
                return;
            }

            baseUsuarios.one("checkMatricula").get({ "matriculaId": $scope.usuario.matricula }).then(function(usuario){
                if (usuario.length > 0 && $scope.screenType == $scope.type.INCLUIR) {
                    growl.error("Já existe um cadastro para a matricula informada.");
                    return;
                }

                baseUsuarios.post($scope.usuario).then(function() {
                        growl.success("Registro gravado com sucesso!");
                        $scope.formUser.$setPristine();
                        $state.go("base.userSearch");
                },
                function() {
                    growl.error("Ocorreu um erro ao tentar gravar o registro!");
                });

            })

        }

    });