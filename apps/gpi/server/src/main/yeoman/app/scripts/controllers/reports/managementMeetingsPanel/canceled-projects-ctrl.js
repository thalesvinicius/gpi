'use strict'

angular.module('gpifrontApp')
    .controller('CanceledProjectsCtrl', function($scope) {

    	// Define os filtros que serão utilizados na busca.
    	$scope.filterOptions = {
    		estagio: false,
    		anual: true,
    		regiaoPlanejamento: true,
    		gerencia: true,
    		cadeiaProdutiva: true,
    		prospeccao: false,
            intervaloDatas: false,
            mesAno: true                        
    	};

        /*Início configs grid*/
        $scope.dataColumns = [{
            field: 'dataCancelamentoProjeto',
            visible: true,
            title: 'reunionPanelReport.cancel.date',
            format: 'date'                
        }, {
            field: 'empresa',
            visible: true,
            title: 'common.company'
        }, {
            field: 'projeto',
            visible: true,
            title: 'common.project'
        }, {
            field: 'cidade',
            visible: true,
            title: 'address.city',
        }, {
            field: 'regiaoPlanejamento',
            visible: true,
            title: 'location.planing.region'
        }, {
            field: 'investimentoPrevisto',
            visible: true,
            title: 'reunionPanelReport.invest',
            format: 'currency'
        }, {
            field: 'empregosDiretos',
            visible: true,
            title: 'reunionPanelReport.direct.jobs'
        }, {
            field: 'cadeiaProdutiva',
            visible: true,
            title: 'reunionPanelReport.productive.chain'
        }, {
            field: 'estagioAtual',
            visible: true,
            title: 'reunionPanelReport.current.stage'
        }, {
            field: 'analista',
            visible: true,
            title: 'reunionPanelReport.analyst'
        }, {
            field: 'gerencia',
            visible: true,
            title: 'reunionPanelReport.management'                    
        }, {
            field: 'dataAssinaturaInstrumento',
            visible: true,
            title: 'reunionPanelReport.protocol.signature.date',    
            format: 'date'                
        }, {
            field: 'justificativaProjeto',
            visible: true,
            title: 'common.justificative'                    
        }];
        /*Fim configs grid*/

	});    	

