'use strict'

angular.module('gpifrontApp')
    .controller('WeeklyStatusReportCtrl', function($scope, ESTAGIO_PROJETO) {
    	// Define os filtros que serão utilizados na busca.
    	$scope.filterOptions = {
    		estagio: true,
    		anual: false,
    		regiaoPlanejamento: true,
    		gerencia: true,
    		cadeiaProdutiva: true,
    		prospeccao: true,
            intervaloDatas: true,
            mesAno: false            
    	};

        $scope.$watch('dados', function(newValue, oldValue) {
            $scope.panelScope.estagios = [
                ESTAGIO_PROJETO.PROJETO_PROMISSOR,
                ESTAGIO_PROJETO.FORMALIZADA,
                ESTAGIO_PROJETO.IMPLANTACAO_INICIADO,
                ESTAGIO_PROJETO.OPERACAO_INICIADA
            ];
        })

        $scope.dataColumns = [{ 
            title: 'Empresa', 
            field: 'empresa', 
            visible: true, 
            filter: { 'name': 'text' } 
        }, { 
            title: 'Projeto', 
            field: 'projeto', 
            visible: true 
        }, { 
            title: 'Cidade', 
            field: 'cidade', 
            visible: true 
        }, { 
            title: 'Região de Planejamento', 
            field: 'regiaoPlanejamento', 
            visible: true 
        }, { 
            title: 'Invest. (R$ mil)', 
            field: 'investimentoPrevisto', 
            visible: true,
            format: 'currency' 
        }, { 
            title: 'Fat. Inicial (R$ mil)', 
            field: 'faturamentoInicial', 
            visible: true,
            format: 'currency' 
        }, { 
            title: 'Fat. Pleno (R$ mil)', 
            field: 'faturamentoPleno', 
            visible: true,
            format: 'currency' 
        }, { 
            title: 'Empregos Diretos', 
            field: 'empregosDiretos', 
            visible: true 
        }, { 
            title: 'Cadeia Produtiva', 
            field: 'cadeiaProdutiva', 
            visible: true 
        }, { 
            title: 'Estágio Atual', 
            field: 'estagioAtual', 
            visible: true 
        }, { 
            title: 'Data Estágio', 
            field: 'dataEstagioAtual', 
            visible: true,
            format: 'date' 
        }, { 
            title: 'Analista', 
            field: 'analista', 
            visible: true 
        }, { 
            title: 'Gerência', 
            field: 'gerencia', 
            visible: true 
        }];
	});    	
