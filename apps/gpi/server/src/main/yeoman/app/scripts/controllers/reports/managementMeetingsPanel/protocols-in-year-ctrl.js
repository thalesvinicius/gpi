'use strict'

angular.module('gpifrontApp')
    .controller('ProtocolsInYearCtrl', function($scope, ScreenService, Restangular, $rootScope, $filter) {
        $scope.screenService = ScreenService;
        $scope.dados = [];

        // Define os filtros que serão utilizados na busca.
        $scope.filterOptions = {
            estagio: false,
            anual: false,
            regiaoPlanejamento: false,
            gerencia: true,
            cadeiaProdutiva: false,
            prospeccao: false,
            intervaloDatas: false,
            mesAno: true,
            tipoInstrumentoFormalizacao: true
        };

        var createCumulLine = function(c3Data, columnReference, excludedColumns, limit) {
            var cumulativeLine = ['Acumulado', null, null]
            var columns = [];
            if (excludedColumns) {
                columns = c3Data.data.columns.filter(function(d) {
                    return excludedColumns.lastIndexOf(d[0]) === -1;
                });
            } else {
                columns = c3Data.data.columns;
            }


            if (columnReference === 'all') {
                for (var i = 3; i < limit; i++) {
                    var newValue = getGroupDataSum(i, columns);
                    if (i === 3) {
                        cumulativeLine.push(newValue);
                    } else {
                        cumulativeLine.push(cumulativeLine[i - 1] + newValue);
                    }
                }
            } else {
                angular.forEach(columns, function(column) {
                    if (column[0] && column[0] === columnReference) {
                        for (var i = 3; i < limit; i++) {
                            if (i == 3) {
                                cumulativeLine.push(column[i]);
                            } else {
                                //sum previous data with the new data
                                cumulativeLine.push(column[i - 1] + column[i]);
                            }
                        }
                    }
                });
            }
            c3Data.data.columns.splice(1,0,cumulativeLine);
            angular.extend(c3Data.data.types, {
                Acumulado: 'line'
            });
        }

        var getGroupDataSum = function(i, dataArray, excludedColumns) {
            var sum = 0;

            angular.forEach(dataArray, function(arrayValues) {
                sum += arrayValues[i];
            });

            return sum;
        }

        var initGraphics = function(month) {
            createCumulLine($scope.chart1, 'all', ['Meta','x'], month+4);
            c3.generate($scope.chart1);
        }

        $scope.initData = function(result) {
            var gerencias = []
            for (var i = 0; i < result.length; i++) {
                gerencias.push(result[i][0]);
            }

            $scope.chart1 = {
                data: {
                    x: 'x',
                    columns: [],
                    type: 'bar',
                    types: {
                        'Meta': 'line'
                    },
                    groups: []
                },
                axis: {
                    x: {
                        type: 'category'
                    }
                }
            };

            if (result) {
                $scope.chart1.data.columns = result;    
                $scope.chart1.data.groups.push(gerencias);
            }            

            result.push(['x', new Date($scope.panelScope.filtro.mesAno).getFullYear()-2, new Date($scope.panelScope.filtro.mesAno).getFullYear()-1, 'JAN', 'FEV', 'MAR', 'ABR', 'MAI', 'JUN', 'JUL', 'AGO', 'SET', 'OUT', 'NOV', 'DEZ']);
            initGraphics(new Date($scope.panelScope.filtro.mesAno).getMonth());
        }

    });
