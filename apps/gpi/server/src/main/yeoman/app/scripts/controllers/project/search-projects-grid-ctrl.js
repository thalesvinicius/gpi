'use strict';
angular.module('gpifrontApp')
    .controller('SearchProjectGridCtrl', function($scope, $filter, UserSession, TIPO_USUARIO, Restangular, configuration, GridButton, SITUACAO_PROJETO, ESTAGIO_PROJETO, growl, AuthService, AUTH_ROLES,
        $state, $http, $window, SITUACAO_EMPRESA, $rootScope, ngTableParams, $stateParams,ScreenService) {
        $scope.disableHeader = true;
        $scope.disableFooter = true;
        $scope.screenService = ScreenService;

        $scope.selectedItems = [];
        $scope.projetos = [];

        if($rootScope.previousState === 'base.project.projectTpl'){
            $scope.filtro = $rootScope.fromParams;
        }else{
            $scope.filtro = {};
        }

        $scope.isExternalUser = UserSession.getUser().tipo == TIPO_USUARIO.EXTERNO;

        $scope.rowStyle = function(row, childIndex) {
            var height = this.$parent.rowHeight;
            var top = 0;
            if (typeof childIndex === "undefined") {
                var totalTop = 0;
                if (row.isAggRow)
                    this.$parent.isAggregate = true;
                if (row.rowIndex > 0 || this.$index > 0) {
                    var topRow
                    if (this.$parent.isAggregate)
                        topRow = this.$parent.renderedRows[this.$index - 1];
                    else
                        topRow = this.$parent.renderedRows[row.rowIndex - 1];
                    var topExpandedHeight = 0;
                    if (this.$parent.nodeExpanded(topRow.entity.id))
                        topExpandedHeight = topRow.entity.versoes.length;
                    totalTop = (topRow.offsetTop + height) + (topExpandedHeight * height);
                }
                top = row.offsetTop = totalTop;
            } else {
                top = (height + (childIndex * height));
            }
            var ret = {
                "top": top + "px",
                "height": height + "px"
            };

            if (row.isAggRow) {
                ret.left = row.offsetLeft;
            }
            return ret;
        };

        $scope.rowTemplate = "<div ng-style=\"{ 'cursor': row.cursor }\" ng-repeat=\"col in renderedColumns\" ng-class=\"col.colIndex()\" class=\"ngCell {{col.cellClass}}\">\r" +
            "\n" +
            "\t<div class=\"ngVerticalBar\" ng-style=\"{height: rowHeight}\" ng-class=\"{ ngVerticalBarVisible: !$last }\">&nbsp;</div>\r" +
            "\n" +
            "\t<div ng-cell></div>\r" +
            "\n" +
            "</div>" +
            "<div ng-style=\"rowStyle(row, $index)\" ng-class=\"{selected: selectedChildrenItems[versao.id]}\" class=\"ngRow\" ng-repeat=\"versao in row.entity.versoes\" ng-show=\"nodeExpanded(row.entity.id)\">\r" +
            "<div ng-style=\"{ 'cursor': row.cursor }\" ng-repeat=\"col in renderedColumns\" ng-class=\"col.colIndex()\" class=\"ngCell {{col.cellClass}}\">\r" +
            "\n" +
            "\t<div class=\"ngVerticalBar\" ng-style=\"{height: rowHeight}\" ng-class=\"{ ngVerticalBarVisible: !$last }\">&nbsp;</div>\r" +
            "\n" +
            "\t<div ng-show=\"col.field!=='empresaUnidadeId' && col.field!=='visualize'\" class=\"ngCellText\" ng-class=\"col.colIndex()\"><span ng-cell-text>{{versao[col.field]}}</span></div>\r" +
            "\t<div class=\"ngSelectionCell\" ng-show=\"col.field==='empresaUnidadeId'\" ng-class=\"col.colIndex()\"><input tabindex=\"-1\" class=\"ngSelectionCheckbox\" type=\"checkbox\" ng-model=\"selectedChildrenItems[versao.id]\" ng-change=\"selectChildrenItem(versao)\" /></div>\r" +
            "\t<div class=\"ngCellText text-center\" ng-show=\"col.field==='visualize'\" ng-class=\"col.colIndex()\"><a href=#/projeto/cadastro/visualizar?id={{versao.id}}><i class=\"fa fa-eye page-header-icon\"></i></a></div>\r" +
            "\n" +
            "</div>" +
            "</div>"
        $scope.gridGroups = ['empresaUnidadeGroup'];
        $scope.aggregateTemplate = "<div ng-click=\"row.toggleExpand()\" ng-style=\"rowStyle(row)\" class=\"ngAggregate\">\r" +
            "\n" +
            "<div ng-style=\"{ 'cursor': row.cursor }\" ng-repeat=\"col in renderedColumns\" ng-class=\"col.colIndex()\" class=\"ngCell {{col.cellClass}}\">\r" +
            "\n" +
            "\t<div class=\"ngVerticalBar\" ng-style=\"{height: rowHeight}\" ng-class=\"{ ngVerticalBarVisible: !$last }\">&nbsp;</div>\r" +
            "\n" +
            "\t<div><div class=\"ngCellText\" ng-class=\"col.colIndex()\">" +
            "<span ng-show=\"col.field!=='nomeProjeto'\" ng-cell-text ng-class=\"{'{{row.aggClass()}}': $index == row.depth}\">{{row.entity.children[0].entity.empresaUnidade[col.field]}}</span>" +
            "<span ng-show=\"col.field==='nomeProjeto'\" ng-cell-text ng-class=\"{'{{row.aggClass()}}': $index == row.depth}\">{{row.entity.children[0].entity.empresaUnidade.nome}}</span>" +
            "</div></div>\r" +
            "\n" +
            "</div>" +
            "\n" +
            "</div>\r" +
            "\n";

        //--------------------------------------------------------------------- Início configs grid resultados

        $scope.columnDefs = [{
            field: 'empresaUnidadeGroup',
            translationKey: '',
            cellTemplate: "<div class=\"ngCellText\" ng-show=\"!versao\" ng-class=\"col.colIndex()\" ng-click=\"selectNodeHead(row.entity.id); $event.stopPropagation();\"><span ng-class=\"headClass(row.entity, row.entity.id, row.entity.versoes)\"></span></div>",
            width: 20,
            enableCellEdit: false
        }, {
            field: 'versaoCompleta',
            enableCellEdit: false,
            width: 50,
            translationKey: 'common.id'
        }, {
            field: 'nomeProjeto',
            enableCellEdit: false,
            width: "**",
            translationKey: 'common.name'
        }, {
            field: 'endereco',
            enableCellEdit: false,
            width: "**",
            translationKey: 'common.address'
        }, {
            field: 'situacao',
            enableCellEdit: false,
            width: "*",
            translationKey: 'common.situation'
        }, {
            field: 'estagio',
            enableCellEdit: false,
            width: "*",
            translationKey: 'common.stage'
        }, {
            field: 'versao',
            enableCellEdit: false,
            width: 60,
            translationKey: 'common.version'
        }, {
            field: 'ultimaAtividade',
            enableCellEdit: false,
            width: "*",
            translationKey: 'common.last.activity'
        }, {
            field: 'visualize',
            translationKey: 'common.show',
            showScreenRedirect: true,
            showLink: '/projeto/cadastro/visualizar'
        }];
        $scope.sortInfo = {
            fields: ['id'],
            directions: ['asc']
        };

        function disableButtonExternalUser() {
            return $scope.isExternalUser;
        }

        $scope.buttonsConfig = {
            //button(show, translationKey, searchMode, disableFn, action, screenLink)
            newButton: new GridButton(false, 'common.new', true, disableButtonExternalUser, null, '/projeto/cadastro/incluir'),
            editButton: new GridButton(false, 'common.edit', true, null, null, '/projeto/cadastro/editar'),
            deleteButton: new GridButton(false, 'project.print.ice', false, disableButtonExternalUser, null, '')
        };

        /*
         *   Verifica se o usuário logado é responsável pelo projeto selecionado
         */
        var isResponsavelProjeto = function() {
            return $scope.selectedItems[0].usuarioInvestimento.id === UserSession.getUser().id;
        }

        /*
         *   Verifica se o usuário logado tem permissão de editar projeto
         */
        var usuarioPossuiPermissao = function() {
            return AuthService.isUserAuthorized([AUTH_ROLES.infraAnalist, AUTH_ROLES.portfolioAnalist, AUTH_ROLES.promotionAnalist, AUTH_ROLES.manager, AUTH_ROLES.externalResponsible]);
        }

        /*
         *   Verifica se o usuário logado é responsável pelo departamento
         */
        var isResponsavelDepartamento = function() {
            return $scope.selectedItems[0].usuarioInvestimento.departamento.id === UserSession.getUser().departamento.id;
        }

        /*
         *   Verifica se o usuário logado é gerente
         */
        var isGerente = function() {
            return AuthService.isUserAuthorized([AUTH_ROLES.manager]);
        }

        /*
         *   Verifica se o usuário logado é gerente
         */
        var podeEditarQualquerProjeto = function() {
            return AuthService.isUserAuthorized([AUTH_ROLES.infraAnalist, AUTH_ROLES.portfolioAnalist]);
        }

        $scope.disableEditFn = function() {
            return !($scope.selectedItems.length === 1 // Somente pode editar um projeto por vez
                && $scope.selectedItems[0].situacaoEmpresa === SITUACAO_EMPRESA["ativa"].id // Só pode editar um projeto em que situação da Empresa ou da Empresa vinculada à Unidade for "Ativo"
                && (($scope.isExternalUser /*&& $scope.selectedItems[0].estagio === ESTAGIO_PROJETO["INICIO_PROJETO"].descricao*/) // Usuario externo só pode editar projeto em estagio inicio de projeto
                    || (isGerente() && isResponsavelDepartamento()) // Caso o usuário logado for um gerente ele poderá editar um projeto se ele for o responsável pelo departamento
                    || (usuarioPossuiPermissao() && isResponsavelProjeto()) || podeEditarQualquerProjeto())); // Se o usuário for responsável pelo projeto ou possuir permissão para editar qualquer projeto
        }

        $scope.disableNewFn = function() {
            return !AuthService.isUserAuthorized([AUTH_ROLES.portfolioAnalist, AUTH_ROLES.promotionAnalist, AUTH_ROLES.manager]);
        }

        $scope.disableIceFn = function() {
            return $scope.selectedItems.length == 0;
        };

        $scope.prepareNewFn = function() {
            $state.go('base.project.projectTpl', {
                screen: 'incluir'
            });
        }

        $scope.prepareEditFn = function() {
            var redirectLocationStr = 'base.project.projectTpl';

            if($scope.isExternalUser && $scope.selectedItems[0].estagio !== ESTAGIO_PROJETO["INICIO_PROJETO"].descricao) {
                redirectLocationStr = 'base.project.completeReportAnnualMonitoring';
            }

            $state.go(redirectLocationStr, {
                screen: 'editar',
                id: $scope.selectedItems[0].id
            });
        }

        $scope.printICEfn = function() {
            if ($scope.selectedItems.length > 0) {
                var ids = [];

                for (var i = $scope.selectedItems.length - 1; i >= 0; i--) {
                    ids.push($scope.selectedItems[i].id);
                };

                $http({
                    method: 'GET',
                    url: configuration.localPath + 'api/projeto/report',
                    params: {
                        'id': ids
                    },
                    headers: {
                        Accept: 'application/pdf',
                        Authorization: 'Bearer ' + $scope.loggedUser.accessToken
                    },
                    responseType: 'arraybuffer'
                }).
                success(function(response) {
                    $scope.screenService.browserAwareSave(response,"ice.pdf","application/pdf");
                }).
                error(function(data, status, headers, config) {
                    growl.error("Não foi possível realizar a impressão do ICE indicado. Tente novamente e se o erro persistir favor contatar o administrador do sistema.")
                });

            } else {
                growl.error('Selecione um projeto para efetuar o download.');
            }
        };

        var initialize = function() {
            if($rootScope.previousState != 'base.project.projectTpl'){
                $scope.incluirUsuarioResponsavel = true;
            }
            $scope.firstSearch();
            $scope.loggedUser = UserSession.getUser();
        };

        var getFilters = function(pageSize, currentPage, countQuery) {
            var result = {};
            if ($scope.isExternalUser) {
                if (UserSession.getUser().unidadeId > 0) {
                    result.unidadeId = UserSession.getUser().unidadeId;
                } else {
                    result.empresaId = UserSession.getUser().empresaId;
                }
            } else if ($stateParams.nomeEmpresa != null || $stateParams.nomeProjeto != null ||
                        $stateParams.cnpj != null || $stateParams.estagio != null ||
                        $stateParams.cadeiaProdutiva != null || $stateParams.situacaoProjeto != null
                        || $scope.filtro.departamento != null || $scope.filtro.usuarioResponsavel != null) {
                $scope.filtro.nomeEmpresa = (!($stateParams.nomeEmpresa === $scope.filtro.nomeEmpresa) && ($scope.filtro.nomeEmpresa === undefined || $scope.filtro.nomeEmpresa === null)) ? $stateParams.nomeEmpresa : $scope.filtro.nomeEmpresa;
                $scope.filtro.nomeProjeto = (!($stateParams.nomeProjeto === $scope.filtro.nomeProjeto) && ($scope.filtro.nomeProjeto === undefined || $scope.filtro.nomeProjeto === null)) ? $stateParams.nomeProjeto : $scope.filtro.nomeProjeto;
                $scope.filtro.cnpj = (!($stateParams.cnpj === $scope.filtro.cnpj) && ($scope.filtro.cnpj === undefined || $scope.filtro.cnpj === null)) ? $stateParams.cnpj : $scope.filtro.cnpj;
                $scope.filtro.estagio = (!($stateParams.estagio === $scope.filtro.estagio) && ($scope.filtro.estagio === undefined || $scope.filtro.estagio === null)) ? $stateParams.estagio : $scope.filtro.estagio;
                $scope.filtro.cadeiaProdutiva = (!($stateParams.cadeiaProdutiva === $scope.filtro.cadeiaProdutiva) && ($scope.filtro.cadeiaProdutiva === undefined || $scope.filtro.cadeiaProdutiva === null)) ? $stateParams.cadeiaProdutiva : $scope.filtro.cadeiaProdutiva;
                $scope.filtro.situacaoProjeto = (!($stateParams.situacaoProjeto === $scope.filtro.situacaoProjeto) && ($scope.filtro.situacaoProjeto === undefined || $scope.filtro.situacaoProjeto === null)) ? $stateParams.situacaoProjeto : $scope.filtro.situacaoProjeto;
                // $scope.filtro.departamento = $stateParams.departamento;
                // $scope.filtro.usuarioResponsavel = $stateParams.usuarioResponsavel;

                result.nomeProjeto = $scope.filtro.nomeProjeto;
                result.nomeEmpresa = $scope.filtro.nomeEmpresa;
                result.cnpj = $scope.filtro.cnpj;
                result.estagio = $scope.filtro.estagio;
                result.cadeiaProdutiva = $scope.filtro.cadeiaProdutiva;
                result.situacaoProjeto = $scope.filtro.situacaoProjeto;
                result.departamento = $scope.filtro.departamento;
                result.usuarioResponsavel = $scope.filtro.usuarioResponsavel;
            } else {
                result.nomeProjeto = $scope.filtro.nomeProjeto;
                result.nomeEmpresa = $scope.filtro.nomeEmpresa;
                result.cnpj = $scope.filtro.cnpj;
                result.estagio = $scope.filtro.estagio;
                result.cadeiaProdutiva = $scope.filtro.cadeiaProdutiva;
                result.situacaoProjeto = $scope.filtro.situacaoProjeto;
                result.departamento = $scope.filtro.departamento;
                result.usuarioResponsavel = $scope.filtro.usuarioResponsavel;
                result.situacaoEmpresa = $scope.filtro.situacaoEmpresa;
                result.empresaId = $scope.filtro.empresaId;
                result.unidadeId = $scope.filtro.unidadeId;

                if ($scope.incluirUsuarioResponsavel) {
                    result.usuarioResponsavel = [UserSession.getUser().id];
                }

                if (!result.situacaoEmpresa) {
                    result.situacaoEmpresa = SITUACAO_EMPRESA.ativa.id;
                }
            }

            result.countResults = countQuery ? countQuery : null;
            result.pageSize = pageSize;
            result.page = currentPage;

            return result;
        }

        var getCounterFilters = function() {
            return getFilters(null, null, true);
        }

        $scope.firstSearch = function() {
            Restangular.all('projeto').customGET('', getCounterFilters()).then(function(result) {
                $scope.gridScope.options.maxResults = parseInt(result);
                if ($scope.gridScope.options.maxResults === 0) {
                    //retirado conforme bug 59080 a pedido do Carlos Soares
                    //growl.error($filter('translate')('message.error.MSG004_NENHUM_REGISTRO_ENCONTRADO'));
                }else{
                    getPagedProject(0, 1, $scope.gridScope.pagingOptions.pageSize);
                }
            });
        }

        var getPagedProject = function(oldPage, newPage, pageSize) {
            var filters = getFilters(pageSize, newPage);
            $scope.projetos = [];

            Restangular.all('projeto').getList(filters).then(function(result) {
                if (result && result.length > 0) {
                    $scope.projetos = convertToDTO(result);
                } else {
                    growl.error($filter('translate')('message.error.MSG004_NENHUM_REGISTRO_ENCONTRADO'));
                }
            });
        }

        $scope.gridScope = {
            pageOnChange: getPagedProject,
            options: {
                isServerSide: true
            },
            filtros: $scope.filtro
        };

        var searchWithFilters = function(oldPage, newPage, pageSize) {
            getPagedProject(oldPage, newPage, pageSize);
        }

        $scope.search = function(filters) {
            $scope.incluirUsuarioResponsavel = false;
            getPagedProject(0, 1, $scope.gridScope.pagingOptions.pageSize);

            $state.go('base.projectSearch', {
                nomeEmpresa: $scope.filtro.nomeEmpresa,
                nomeProjeto: $scope.filtro.nomeProjeto,
                cnpj: $scope.filtro.cnpj,
                estagio: $scope.filtro.estagio,
                cadeiaProdutiva: $scope.filtro.cadeiaProdutiva,
                situacaoProjeto: $scope.filtro.situacaoProjeto
                // departamento: $scope.filtro.departamento,
                // usuarioResponsavel: $scope.filtro.usuarioResponsavel
            });
        };

        $scope.resetFilters = function() {
            $scope.filtro = {};
            $stateParams.nomeEmpresa = null;
            $stateParams.nomeProjeto = null;
            $stateParams.cnpj = null;
            $stateParams.estagio = null;
            $stateParams.cadeiaProdutiva = null;
            $stateParams.situacaoProjeto = null;
            $stateParams.departamento = null;
            $stateParams.usuarioResponsavel = null;
        };

        var parse = function(versoes) {
            if (!versoes || angular.isUndefined(versoes)) {
                return [];
            }

            $.each(versoes, function(index, versao) {
                if (versao.estagio) {
                    versao.estagio = ESTAGIO_PROJETO[versao.estagio].descricao;
                }
                if (versao.situacao) {
                    versao.situacao = versao.situacao ? SITUACAO_PROJETO[versao.situacao].descricao : null;
                }
                if (versao.nomeProjeto) {
                    versao.nomeProjeto = "\xa0\xa0" + versao.nomeProjeto;
                }

                versao.ultimaAtividade = (versao.ultimaAtividade != null) ? versao.ultimaAtividade.atividadeLocal.descricao : ' - ';

            });
            return versoes;
        };

        var convertToDTO = function(projeto) {
            $scope.projetoDTO = [];

            $.each(projeto, function(index, projetoEntity) {
                var dto = {
                    id: projetoEntity.id,
                    versaoCompleta: projetoEntity.versaoCompleta,
                    situacao: projetoEntity.situacao ? SITUACAO_PROJETO[projetoEntity.situacao].descricao : null,
                    estagio: ESTAGIO_PROJETO[projetoEntity.estagio].descricao,
                    versao: projetoEntity.versao,
                    nomeProjeto: projetoEntity.nomeProjeto,
                    empresaUnidadeGroup: projetoEntity.idEmpresa + (projetoEntity.idUnidade != null ? '.' + projetoEntity.idUnidade : ''),
                    ultimaAtividade: (projetoEntity.ultimaAtividade != null) ? projetoEntity.ultimaAtividade.atividadeLocal.descricao : ' - ',
                    empresaUnidadeId: (projetoEntity.idUnidade) ? projetoEntity.idUnidade : projetoEntity.idEmpresa,
                    empresaUnidade: {
                        nome: (projetoEntity.idUnidade) ? projetoEntity.nomeUnidade : projetoEntity.nomeEmpresa,
                        endereco: (projetoEntity.idUnidade) ? projetoEntity.enderecoUnidade.enderecoFormatado : projetoEntity.enderecoEmpresa.enderecoFormatado
                    },
                    versoes: parse(projetoEntity.versoes),
                    situacaoEmpresa: projetoEntity.situacaoEmpresa,
                    usuarioInvestimento: projetoEntity.usuarioInvestimento
                }
                $scope.projetoDTO.push(dto);
            });

            return $scope.projetoDTO;
        };

        $scope.loadAnalists = function() {
            $scope.analistas = []
            var usuarios = $scope.$parent.usuarios;

            for (var i = 0; i < usuarios.length; i++) {
                if ($scope.filtro && $scope.filtro.departamento.indexOf(usuarios[i].departamento.id) !== -1) {
                    $scope.analistas.push(usuarios[i]);
                }
            }
        }

        initialize();
    });
