'use strict';

angular.module('gpifrontApp')
    .controller('ContactCtrl', function($scope, Restangular, $filter, $rootScope, GridButton) {
        $scope.contacts = $scope.$parent.empresa.contatosPessoa;
        $scope.selectedItems = [];

        $scope.columnDefs = [{
            field: 'nome',
            enableCellEdit: true,
            width: "**",
            translationKey: 'common.name',
            editableCellTemplate: '<input type="text" placeholder="{{\'common.name\' | translate}}" ng-class="\'colt\' + col.index"  ng-input="COL_FIELD" ng-model="COL_FIELD" maxlength="100"/>'

        }, {
            field: 'cargo',
            enableCellEdit: true,
            width: "**",
            translationKey: 'common.job.title',
            editableCellTemplate: '<input type="text" placeholder="{{\'common.job.title\' | translate}}" ng-class="\'colt\' + col.index"  ng-input="COL_FIELD" ng-model="COL_FIELD" maxlength="100"/>'
        }, {
            field: 'telefone',
            enableCellEdit: true,
            width: "*",
            translationKey: 'common.phone1',
            editableCellTemplate: '<input type="phone" placeholder="{{\'common.phone1\' | translate}}" ng-class="\'colt\' + col.index" maxlength="18" ng-input="COL_FIELD" ng-model="COL_FIELD"/>'
        }, {
            field: 'telefoneAdicional',
            enableCellEdit: true,
            width: "*",
            translationKey: 'common.phone2',
            editableCellTemplate: '<input type="phone" placeholder="{{\'common.phone2\' | translate}}" ng-class="\'colt\' + col.index" maxlength="18" ng-input="COL_FIELD" ng-model="COL_FIELD"/>'

        }, {
            field: 'email',
            enableCellEdit: true,
            width: "**",
            translationKey: 'common.email',
            editableCellTemplate: '<input type="email" placeholder="example@email.com" ng-class="\'colt\' + col.index" maxlength="50" ng-input="COL_FIELD" ng-model="COL_FIELD" />'
        }];

        // function newFn() {
        //     $scope.$parent.empresa.contatosPessoa.unshift({"empresa_id": $scope.$parent.empresa.id});

        //     return false;
        // }

        $scope.buttonsConfig = {
            //function GridButton(show, translationKey, searchMode, disable, action, screenLink)
            newButton: new GridButton(true, 'contact.new', false, null, null, ''),
            deleteButton: new GridButton(true, 'common.delete', false, null, deleteFn, '')
        }

        $scope.sortInfo = {
            fields: ['nome'],
            directions: ['asc']
        };

        $scope.$parent.$watch('empresa', function(newVal, oldVal) {
            if (newVal.contatosPessoa !== oldVal.contatosPessoa) {
                $scope.contacts = newVal.contatosPessoa;
            }
        }, true);

        function deleteFn() {
            console.log("do delete()");
        }
        
    });
