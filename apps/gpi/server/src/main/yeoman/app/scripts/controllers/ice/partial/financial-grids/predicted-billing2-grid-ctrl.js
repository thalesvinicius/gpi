'use strict'

angular
	.module('gpifrontApp')
	.controller('PredictedBilling2GridCtrl', function($scope, Restangular, GridButton, growl, $filter){

        $scope.faturamentoPrevisto2 = $scope.$parent.faturamentoPrevisto2;            
        $scope.faturamentoPrevisto2SelectedItems = $scope.$parent.faturamentoPrevisto2SelectedItems;

		$scope.faturamentoPrevisto2ColumnDefs = [{
            field: "ano",
            enableCellEdit: true,
            width: "**",            
            translationKey: "common.achievement.year",
            editableCellTemplate: '<input type="numeric" ui-mask="9999" maxlength="4" ng-class="\'colt\' + col.index"  ng-input="COL_FIELD" ng-model="COL_FIELD"/>'                        
        }, {
            field: "valor",
            enableCellEdit: true,
            cellFilter: "currency:''",
            editableCellTemplate: '<input type="text" format="currency" maxlength="21" ng-class="\'colt\' + col.index"  ng-input="COL_FIELD" ng-model="COL_FIELD"/>',                                                            
            width: "**",            
            translationKey: "financial.revenue"         
        }];

        $scope.faturamentoPrevisto2ButtonsConfig = {            
            newButton: new GridButton(true, 'common.new', false, null, null, ''),
            deleteButton: new GridButton(true, 'common.delete', false, null, deleteFn, '')
        }


        //define its sort-info attribute if you desire a default-sort
        $scope.faturamentoPrevisto2SortInfo = {
            fields: ['ano'],
            directions: ['asc']
        };

        function deleteFn() {
            growl.info($filter('translate')('message.error.MSG38_SECAO_ATUALIZADA', {
                value: "Faturamento Previsto"
            }));                        
        }              		

        $scope.$parent.$watch('faturamentoPrevisto2', function(newVal, oldVal){
         	if ($scope.$parent.faturamentoPrevisto2 != null) {
                setTimeout(function() {
         		     $scope.faturamentoPrevisto2 = newVal;	
                }, true);
         	}                
        }, true);

	});


