'use strict';

angular.module('gpifrontApp')
    .controller('MenuCtrl', function($scope, $translate, AuthService, $rootScope, UserSession, Restangular, AUTH_ROLES) {

        $scope.disableNewFnProject = function() {
            return AuthService.isUserAuthorized([AUTH_ROLES.infraAnalist, AUTH_ROLES.informationAnalist,AUTH_ROLES.director,AUTH_ROLES.generalSecretary]);
        }

        $scope.disableNewFnCompany = function() {
            return AuthService.isUserAuthorized([AUTH_ROLES.infraAnalist, AUTH_ROLES.informationAnalist, AUTH_ROLES.director]);
        }

        $scope.disableNewFnUser = function() {
            return !AuthService.isUserAuthorized(AUTH_ROLES.itAdmin);
        }

        $scope.disableReportAnnualMonitoringFnUser = function() {
            return !AuthService.isUserAuthorized(AUTH_ROLES.infraAnalist);
        }

        $scope.disableFnActivityTemplate = function() {
            return !AuthService.isUserAuthorized([AUTH_ROLES.portfolioAnalist]);
        }

        $scope.disableFnReportProject = function() {
            return AuthService.isUserAuthorized([AUTH_ROLES.infraAnalist, AUTH_ROLES.generalSecretary]);
        }

        $scope.disableFnReportPositiveSchedule = function() {
            return AuthService.isUserAuthorized([AUTH_ROLES.infraAnalist, AUTH_ROLES.generalSecretary]);
        }

        $scope.disableExternalUser = function() {
            return AuthService.isUserAuthorized([AUTH_ROLES.externalResponsible, AUTH_ROLES.externalMeling, AUTH_ROLES.externalOther]);
        }

        $scope.disableFnNewFormalizationInstrument = function() {
            return AuthService.isUserAuthorized([AUTH_ROLES.portfolioAnalist, AUTH_ROLES.promotionAnalist, AUTH_ROLES.manager]);
        }

        $scope.showReportEnvironment = function() {
            return AuthService.isUserAuthorized([AUTH_ROLES.infraAnalist, AUTH_ROLES.portfolioAnalist, AUTH_ROLES.promotionAnalist, AUTH_ROLES.director, AUTH_ROLES.manager]);
        }

        $scope.showReportTrackingFormalizationInstrument = function() {
            return AuthService.isUserAuthorized([AUTH_ROLES.portfolioAnalist, AUTH_ROLES.promotionAnalist, AUTH_ROLES.director, AUTH_ROLES.manager, AUTH_ROLES.generalSecretary]);
        }
    });