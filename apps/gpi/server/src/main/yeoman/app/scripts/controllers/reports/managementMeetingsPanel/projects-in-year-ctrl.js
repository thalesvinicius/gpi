'use strict'

angular.module('gpifrontApp')
    .controller('ProjectsInYearCtrl', function($scope, ScreenService, Restangular, $rootScope, $filter) {
        $scope.screenService = ScreenService;
        $scope.filtro = {termo: "protocolos"};
        $scope.dados = [];

    	// Define os filtros que serão utilizados na busca.
    	$scope.filterOptions = {
    		estagio: false,
    		anual: false,
    		regiaoPlanejamento: false,
    		gerencia: true,
    		cadeiaProdutiva: true,
    		prospeccao: false,
            intervaloDatas: false,
            mesAno: true,
            tipoInstrumentoFormalizacao: true                        
    	};

		var margin = {top: 40, right: 10, bottom: 10, left: 10},
			width = 960 - margin.left - margin.right,
			height = 500 - margin.top - margin.bottom;

		function normalizeTreeMapData(dados) {
			if(dados && dados.length > 0) {
				var normalizedData = {"name": "cadeiasProdutivas"};
				normalizedData.children = [];

				angular.forEach(dados, function(filho){
					var pai = {};
					pai.name = filho.name;
					pai.children = [filho];
					normalizedData.children.push(pai);
				});
				return normalizedData;				
			}
			return {}
		}

		function initTreeMapData(result) {
			var treeMapNormalizedData = normalizeTreeMapData(result);	
			constructTreeMap(treeMapNormalizedData);
		}

		function constructTreeMap(dados) {
			var color = d3.scale.category20c();

			var treemap = d3.layout.treemap()
				.size([width, height])
				.sticky(true)
				.sort(function(a, b) {
  					return a.value < b.value ? -1 : a.value > b.value ? 1 : 0;
				})
				.value(function(d) { return d.size; });

			var div = d3.select(".div-treemap").text("")
				.style("position", "relative")
				.style("width", (width + margin.left + margin.right) + "px")
				.style("height", (height + margin.top + margin.bottom) + "px")
				.style("left", margin.left + "px")
				.style("top", margin.top + "px");		

			var mousemove = function(d) {				
			  var xPosition = d3.event.pageX + 5 >  $(window).width() - 200 ? d3.event.pageX - 100 : d3.event.pageX + 5;
			  var yPosition = d3.event.pageY + 5;

			  if (!document.getElementById("tooltip-treemap")) {
				  var heading = document.createElement("p");
	  			  heading.setAttribute("id", "heading")
	  			  heading.setAttribute("style", "font-weight: bold;");

				  var body = document.createElement("p");
	  			  body.setAttribute("id", "body")

				  var tooltip = document.createElement("div");
				  tooltip.appendChild(heading);
				  tooltip.appendChild(body);

				  document.body.appendChild(tooltip);
				  tooltip.setAttribute("id", "tooltip-treemap");  			  			  	
			  }

			  d3.select("#tooltip-treemap")
			    .style("left", xPosition + "px")
			    .style("top", yPosition + "px");
			  d3.select("#tooltip-treemap #heading")
			    .text(d["name"]);

			 var infoTooltip = $filter("currency") (d["size"] * 1000);
			 if ($scope.filtro && $scope.filtro.termo) {
			 	 if ($scope.filtro.termo === 'empregosDiretos') {
			 	 	infoTooltip = d["size"] + " emprego(s) direto(s) ";
			 	 } else if ($scope.filtro.termo === 'protocolos') {
			 	 	infoTooltip = d["size"] + " Instrumento(s)";
			 	 }
			 }
			d3.select("#tooltip-treemap #body")
			    .text(infoTooltip);
			  d3.select("#tooltip-treemap").classed("hidden", false);
			};

			var mouseout = function() {
			  d3.select("#tooltip-treemap").classed("hidden", true);
			};

			var node = div.datum(dados).selectAll(".node")
				.data(treemap.nodes)
				.enter().append("div")
				.attr("class", "node")
				.call(position)
				.style("background", function(d) { return d.children ? color(d.name) : null; })
				.text(function(d) { return d.children ? null : d.name; })
				.on("mousemove", mousemove)
      			.on("mouseout", mouseout);		
		}

		function position() {
			this.style("left", function(d) { return d.x + "px"; })
			.style("top", function(d) { return d.y + "px"; })
			.style("width", function(d) { return Math.max(0, d.dx - 1) + "px"; })
			.style("height", function(d) { return Math.max(0, d.dy - 1) + "px"; });
		}	

		$("input").click(function() {
			setTimeout(function() {
				$scope.filtro.mesAno = new Date($rootScope.mesAno).getTime();
				$scope.filtro.diretoria = $rootScope.diretoria;
				$scope.filtro.gerencias = $rootScope.gerencias;
				$scope.filtro.cadeiasProdutivas = $rootScope.cadeiasProdutivas;
				$scope.filtro.tiposInstrumentoFormalizacao = $rootScope.tiposInstrumentoFormalizacao;				
				Restangular.all("report/painelreuniao/projetosnoano").getList($scope.filtro).then(function(result){
					var treeMapNormalizedData = normalizeTreeMapData(result);	
					constructTreeMap(treeMapNormalizedData);
				});				
			})
		});

		$scope.$watch('dados', function(newValue, oldValue){
			if (oldValue != newValue) {
		        $scope.filtro = {termo: "protocolos"};
				var treeMapNormalizedData = normalizeTreeMapData($scope.dados);	
				constructTreeMap(treeMapNormalizedData);									
			}
		})		

	});    	
