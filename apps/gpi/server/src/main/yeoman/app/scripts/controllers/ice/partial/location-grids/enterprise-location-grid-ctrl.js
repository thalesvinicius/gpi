'use strict'

angular
	.module('gpifrontApp')
	.controller('EnterpriseLocationGridCtrl', function($scope, GridButton, growl, $filter){

        $scope.localizacoesEmpreendimento = $scope.$parent.localizacoesEmpreendimento;    
        $scope.localizacoesEmpreendimentoSelectedItems = [];           

        //Define its column-defs attribute
        $scope.localizacoesEmpreendimentoColumnDefs = [{
            field: 'municipio.nome',
            enableCellEdit: false,
            width: "**",
            translationKey: 'location.city',
            editableCellTemplate: '<input type="numeric" maxlength="10" ng-class="\'colt\' + col.index"  ng-input="COL_FIELD" ng-model="COL_FIELD" maxlength="100"/>'            
        }, {
        	field: 'microRegiao.nome',
            enableCellEdit: false,
            width: "**",
            translationKey: 'location.microregion1'
        }, {
            field: 'microrregiao2',
            enableCellEdit: false,
            width: "**",
            translationKey: 'location.microregion2',
			cellTemplate: ''            
        }, {
        	field: 'microrregiao3',
            enableCellEdit: false,
            width: "**",
            translationKey: 'location.microregion3',
			cellTemplate: ''            
        }, {
        	field: 'regiaoPlanejamento.nome',
            enableCellEdit: false,
            width: "**",
            translationKey: 'location.planing.region'
        }];

        //define its buttons-config attribute on a search screen
        $scope.localizacoesEmpreendimentoButtonsConfig = {            
            newButton: new GridButton(false, 'common.new', false, null, null, ''),
            deleteButton: new GridButton(true, 'common.delete', false, null, deleteFn, '')
        }

        //define its sort-info attribute if you desire a default-sort
        $scope.localizacoesEmpreendimentoSortInfo = {
            fields: ['municipio'],
            directions: ['asc']
        };

        $scope.$parent.$watch('localizacoesEmpreendimento', function(newVal, oldVal){
         	if ($scope.$parent.localizacoesEmpreendimento != null) {
             		$scope.localizacoesEmpreendimento = newVal;	
         	}                
        }, true);

        function deleteFn() {
            growl.info($filter('translate')('message.error.MSG38_SECAO_ATUALIZADA', {value: "Localização"}));                        
        }                

	})
