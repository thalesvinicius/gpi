'use strict';

angular
    .module('gpifrontApp', [
        'ngCookies',
        'ngResource',
        'ngSanitize',
        'ui.router',
        'ui.utils',
        'restangular',
        'ngGrid',
        'pascalprecht.translate',
        'angular-growl',
        'ui.bootstrap',
        'flow',
        'services.config',
        'dialogs.main',
        'angular-loading-bar',
        'ngTable',
        'ui.utils.masks',
        'ui.grid',
        'ui.grid.pagination',
        'ui.grid.autoResize',
        'ui.grid.exporter',
        'ui.grid.selection',
        'ui.grid.resizeColumns',
        'ui.grid.moveColumns',
        'ngCsv'
    ])
    .config(function($stateProvider, $urlRouterProvider, RestangularProvider, $translateProvider, configuration, AUTH_ROLES, cfpLoadingBarProvider, flowFactoryProvider) {
        $urlRouterProvider.otherwise("/");
        $translateProvider.useStaticFilesLoader({
            prefix: configuration.rootDir + 'languages/',
            suffix: '.json'
        });
        $translateProvider.preferredLanguage('pt');
        $stateProvider
            .state('base', {
                abstract: true,
                views: {
                    "": {
                        templateUrl: "templates/base.html"
                    },
                    "topBar@base": {
                        templateUrl: "templates/topBar.html",
                        controller: 'BaseCtrl'
                    },
                    "sideBar@base": {
                        templateUrl: "templates/sideBar.html",
                        controller: 'MenuCtrl'
                    }
                },
                data: {
                    authenticated: true,
                    displayName: 'Home',
                    proxy: 'base.home'
                }
            })
            .state('login', {
                url: "/login?environment?message",
                templateUrl: "templates/login.html",
                controller: 'LoginCtrl'
            })
            .state('password', {
                url: "/password?type?token&email",
                templateUrl: "views/user/password.html",
                controller: 'PasswordCtrl'
            })
            .state('base.registerUser', {
                url: "/registerUser?email",
                templateUrl: "views/user/registerUser.html",
                controller: 'RegisterUserCtrl'
            })
            .state('base.searchCompany', {
                url: "/empresa/consultar",
                templateUrl: "views/company/search-company.html",
                controller: 'SearchCompanyCtrl',
                data: {
                    displayName: 'Consultar Empresa'
                }
            })
            .state('base.company', {
                url: "/empresa",
                abstract: true,
                templateUrl: "views/company/company.html",
                controller: "CompanyCtrl",

            })
            .state("base.company.companyTpl", {
                url: "/cadastro/{screen}?id",
                templateUrl: "views/company/partial/persist-company-tpl.html",
                controller: 'PersistCompanyCtrl',
                data: {
                    authorizedRolesByType: {
                        VISUALIZAR: [
                            AUTH_ROLES.infraAnalist,
                            AUTH_ROLES.informationAnalist,
                            AUTH_ROLES.portfolioAnalist,
                            AUTH_ROLES.promotionAnalist,
                            AUTH_ROLES.manager,
                            AUTH_ROLES.generalSecretary,
                            AUTH_ROLES.director,
                            AUTH_ROLES.externalResponsible,
                            AUTH_ROLES.externalMeling,
                            AUTH_ROLES.externalOther

                        ],
                        EDITAR: [
                            AUTH_ROLES.portfolioAnalist,
                            AUTH_ROLES.manager,
                            AUTH_ROLES.generalSecretary,
                            AUTH_ROLES.promotionAnalist,
                            AUTH_ROLES.externalResponsible,
                            AUTH_ROLES.externalMeling,
                            AUTH_ROLES.externalOther
                        ],
                        INCLUIR: [
                            AUTH_ROLES.portfolioAnalist,
                            AUTH_ROLES.manager,
                            AUTH_ROLES.generalSecretary,
                            AUTH_ROLES.promotionAnalist
                        ]
                    },
                    displayName: 'Cadastro Empresa'
                }
            })
            .state("base.company.contactInformationTpl", {
                url: "/contato/{screen}?id",
                templateUrl: "views/company/partial/contact-information-tpl.html",
                controller: 'CompanyContactInformationCtrl',
                data: {
                    displayName: 'Cadastro Informações Adicionais'
                }
            })
            .state('base.company.registerExternalContact', {
                url: "/registerExternalContact/{screen}?id",
                templateUrl: "views/company/external/contact.html",
                controller: 'RegisterExternalContactCtrl'
            })
            .state("base.company.unitTpl", {
                url: "/unidade/{screen}?id",
                templateUrl: "views/company/partial/unit-tpl.html",
                controller: 'UnitCtrl',
                data: {
                    displayName: 'Cadastro Unidades'
                }
            })
            .state('base.projectSearch', {
                url: "/projeto/consultar?nomeEmpresa&nomeProjeto&cnpj&estagio&cadeiaProdutiva&situacaoProjeto&departamento&usuarioResponsavel",
                templateUrl: "views/project/search.html",
                controller: 'ProjectSearchCtrl',
                data: {
                    displayName: 'Consultar Projeto'
                }
            })
            .state('base.project', {
                url: "/projeto",
                abstract: true,
                templateUrl: "views/project/project.html",
                controller: "ProjectCtrl",
                data: {
                    displayName: 'Projeto',
                    proxy: 'base.project.projectTpl',
                    authorizedRoles: [
                        AUTH_ROLES.infraAnalist,
                        AUTH_ROLES.informationAnalist,
                        AUTH_ROLES.portfolioAnalist,
                        AUTH_ROLES.promotionAnalist,
                        AUTH_ROLES.director,
                        AUTH_ROLES.manager,
                        AUTH_ROLES.generalSecretary,
                        AUTH_ROLES.externalResponsible
                    ]
                }
            })
            .state("base.project.projectTpl", {
                url: "/cadastro/{screen}?id&nomeEmpresa&nomeProjeto&cnpj&estagio&cadeiaProdutiva&situacaoProjeto&departamento&usuarioResponsavel",
                templateUrl: "views/project/partial/persist-project-tpl.html",
                controller: 'PersistProjectCtrl'
            })
            .state("base.project.monitor", {
                url: "/acompanhar/{screen}?id",
                templateUrl: "views/project/partial/monitor-project-tpl.html",
                controller: "MonitorProjectCtrl",
                data: {
                    displayName: 'Acompanhamento do Projeto'
                },
                authorizedRolesByType: {
                    VISUALIZAR: [
                        AUTH_ROLES.infraAnalist,
                        AUTH_ROLES.informationAnalist,
                        AUTH_ROLES.portfolioAnalist,
                        AUTH_ROLES.promotionAnalist,
                        AUTH_ROLES.director,
                        AUTH_ROLES.manager,
                        AUTH_ROLES.generalSecretary,
                        AUTH_ROLES.externalResponsible
                    ],
                    EDITAR: [
                        AUTH_ROLES.portfolioAnalist,
                        AUTH_ROLES.manager,
                        AUTH_ROLES.promotionAnalist,
                        AUTH_ROLES.externalResponsible
                    ],
                    INCLUIR: [
                        AUTH_ROLES.portfolioAnalist,
                        AUTH_ROLES.manager,
                        AUTH_ROLES.promotionAnalist
                    ]
                }
            })
            .state('base.project.completeReportAnnualMonitoring', {
                url: "/relatorio/acompanhamentoAnual/{screen}?id",
                templateUrl: "views/project/partial/completeReportAnnualMonitoring.html",
                controller: 'CompleteReportAnnualMonitoringCtrl',
                data: {
                    displayName: 'Acompanhamento Anual do Projeto',
                    authorizedRoles: [
                        AUTH_ROLES.informationAnalist,
                        AUTH_ROLES.portfolioAnalist,
                        AUTH_ROLES.promotionAnalist,
                        AUTH_ROLES.director,
                        AUTH_ROLES.manager,
                        AUTH_ROLES.externalResponsible
                    ]
                }
            })
            .state("base.project.ice", {
                url: "/ice",
                abstract: true,
                templateUrl: "views/ice/ice.html",
                controller: "IceCtrl",
                data: {
                    displayName: 'ICE'
                }
            })
            /*Inicio Sub Abas ICE*/
            .state("base.project.ice.locationTpl", {
                url: "/localizacao/{screen}?id",
                templateUrl: "views/ice/partial/location-tpl.html",
                controller: "LocationCtrl",
                data: {
                    displayName: 'Localização'
                }
            })
            .state("base.project.ice.financialTpl", {
                url: "/financeiro/{screen}?id",
                templateUrl: "views/ice/partial/financial-tpl.html",
                controller: 'IceFinancialCtrl',
                data: {
                    displayName: 'Financeiro'
                }
            })
            .state("base.project.ice.jobsTpl", {
                url: "/emprego/{screen}?id",
                templateUrl: "views/ice/partial/jobs-tpl.html",
                controller: 'IceJobsCtrl',
                data: {
                    displayName: 'Empregos'
                }
            })
            .state("base.project.ice.scheduleTpl", {
                url: "/cronograma/{screen}?id",
                templateUrl: "views/ice/partial/schedule-tpl.html",
                controller: 'IceScheduleCtrl',
                data: {
                    displayName: 'Cronograma'
                }
            })
            .state("base.project.ice.infrastructureTpl", {
                url: "/infraestrutura/{screen}?id",
                templateUrl: "views/ice/partial/infrastructure-tpl.html",
                controller: "InfraestruturaCtrl",
                data: {
                    displayName: 'Infraestrutura'
                }
            })
            .state("base.project.ice.environmentTpl", {
                url: "/meio-ambiente/{screen}?id",
                templateUrl: "views/ice/partial/environment-tpl.html",
                controller: "IceEnvironmentCtrl",
                data: {
                    displayName: 'Meio Ambiente'
                }
            })
            .state("base.project.ice.insumosProdutosTpl", {
                url: "/insumos/{screen}?id",
                templateUrl: "views/ice/partial/inputs-and-products-tpl.html",
                controller: "InputsAndProductsCtrl",
                data: {
                    displayName: 'Insumos e produtos'
                }
            })
            .state("base.project.ice.claimTpl", {
                url: "/pleitos/{screen}?id",
                templateUrl: "views/ice/partial/claim-tpl.html",
                controller: 'IceClaimCtrl',
                data: {
                    displayName: 'Pleitos'
                }
            })
            /*Fim Sub Abas ICE*/

        /* Início Telas Usuário*/
        .state('base.userSearch', {
                url: "/usuario/consultar",
                templateUrl: "views/user/search.html",
                controller: 'UserSearchCtrl',
                data: {
                    displayName: 'Consultar Usuários',
                    authorizedRoles: [
                        AUTH_ROLES.itAdmin
                    ]
                }
            })
            .state('base.user', {
                url: "/usuario/{screen}?id",
                templateUrl: function($stateParams) {
                    var url = "views/user/user.html";
                    var stateParamScreenOk = typeof $stateParams.screen !== 'undefined' && $stateParams.screen;
                    var stateParamScreenIncluir = $stateParams.screen === "incluir";
                    var stateParamIdOk = typeof $stateParams.id !== undefined && $stateParams.id;

                    if (stateParamScreenIncluir) {
                        return url;
                    } else if (stateParamScreenOk && (!stateParamScreenIncluir && (stateParamIdOk))) {
                        return "views/user/user.html?screen=" + $stateParams.screen + "&id=" + $stateParams.id;
                    } else {
                        return "views/main.html";
                    }
                },
                controller: 'UserCtrl',
                data: {
                    displayName: 'Usuários',
                    authorizedRoles: [
                        AUTH_ROLES.itAdmin
                    ]
                }
            })
            /* Fim Telas Usuário*/

        /*Início Instrumento de formalização*/
        .state('base.formalizationInstrumentSearch', {
                url: "/instrumento-formalizacao/consultar",
                templateUrl: "views/formalizationInstrument/search.html",
                controller: 'FormalizationInstrumentSearchCtrl',
                data: {
                    displayName: 'Consultar Instrumento de Formalização',
                    authorizedRolesByType: {
                        VISUALIZAR: [
                            AUTH_ROLES.infraAnalist,
                            AUTH_ROLES.informationAnalist,
                            AUTH_ROLES.portfolioAnalist,
                            AUTH_ROLES.promotionAnalist,
                            AUTH_ROLES.director,
                            AUTH_ROLES.manager,
                            AUTH_ROLES.generalSecretary
                        ]
                    }
                }
            })
            .state('base.formalizationInstrument', {
                url: "/instrumento-formalizacao",
                abstract: true,
                templateUrl: "views/formalizationInstrument/formalization-instrument.html",
                controller: "FormalizationInstrumentCtrl",
                data: {
                    displayName: 'Instrumento de Formalização',
                    proxy: 'base.formalizationInstrument.formalizationInstrumentTpl',
                    authorizedRoles: [
                        AUTH_ROLES.infraAnalist,
                        AUTH_ROLES.informationAnalist,
                        AUTH_ROLES.portfolioAnalist,
                        AUTH_ROLES.promotionAnalist,
                        AUTH_ROLES.director,
                        AUTH_ROLES.manager,
                        AUTH_ROLES.generalSecretary
                    ]
                }
            })
            /*Início Abas Instrumento de formalização*/
            .state("base.formalizationInstrument.formalizationInstrumentTpl", {
                url: "/cadastro/{screen}?id",
                templateUrl: "views/formalizationInstrument/partial/persist-formalization-instrument-tpl.html",
                controller: 'PersistFormalizationInstrumentCtrl',
                data: {
                    displayName: 'Instrumento de Formalização',
                    authorizedRolesByType: {
                        VISUALIZAR: [
                            AUTH_ROLES.infraAnalist,
                            AUTH_ROLES.informationAnalist,
                            AUTH_ROLES.portfolioAnalist,
                            AUTH_ROLES.promotionAnalist,
                            AUTH_ROLES.director,
                            AUTH_ROLES.manager,
                            AUTH_ROLES.generalSecretary
                        ],
                        EDITAR: [
                            AUTH_ROLES.portfolioAnalist,
                            AUTH_ROLES.promotionAnalist,
                            AUTH_ROLES.manager
                        ],
                        INCLUIR: [
                            AUTH_ROLES.portfolioAnalist,
                            AUTH_ROLES.promotionAnalist,
                            AUTH_ROLES.manager
                        ]
                    }
                }

            })
            .state("base.formalizationInstrument.monitorTpl", {
                url: "/atividades-acompanhamento/{screen}?id",
                templateUrl: "views/formalizationInstrument/partial/monitor-form-inst-tpl.html",
                controller: 'MonitorFormInstCtrl',
                data: {
                    displayName: 'Atividades de Tramitação',
                    authorizedRolesByType: {
                        VISUALIZAR: [
                            AUTH_ROLES.infraAnalist,
                            AUTH_ROLES.informationAnalist,
                            AUTH_ROLES.portfolioAnalist,
                            AUTH_ROLES.promotionAnalist,
                            AUTH_ROLES.director,
                            AUTH_ROLES.manager,
                            AUTH_ROLES.generalSecretary

                        ],
                        EDITAR: [
                            AUTH_ROLES.portfolioAnalist,
                            AUTH_ROLES.manager,
                            AUTH_ROLES.promotionAnalist
                        ],
                        INCLUIR: [
                            AUTH_ROLES.portfolioAnalist,
                            AUTH_ROLES.manager,
                            AUTH_ROLES.promotionAnalist
                        ]
                    }
                }
            })
            .state("base.formalizationInstrument.publicationTpl", {
                url: "/publicacao/{screen}?id",
                templateUrl: "views/formalizationInstrument/partial/publication-tpl.html",
                controller: 'PublicationCtrl',
                data: {
                    displayName: 'Publicação',
                    authorizedRolesByType: {
                        VISUALIZAR: [
                            AUTH_ROLES.infraAnalist,
                            AUTH_ROLES.informationAnalist,
                            AUTH_ROLES.portfolioAnalist,
                            AUTH_ROLES.promotionAnalist,
                            AUTH_ROLES.director,
                            AUTH_ROLES.manager,
                            AUTH_ROLES.generalSecretary
                        ],
                        EDITAR: [
                            AUTH_ROLES.portfolioAnalist,
                            AUTH_ROLES.promotionAnalist,
                            AUTH_ROLES.manager
                        ],
                        INCLUIR: [
                            AUTH_ROLES.portfolioAnalist
                        ]
                    }
                }
            })
            .state("base.formalizationInstrument.satisfactionSurveyTpl", {
                url: "/pesquisa-satisfacao/{screen}?id",
                templateUrl: "views/formalizationInstrument/partial/satisfaction-survey-tpl.html",
                controller: 'SatisfactionSurveyCtrl',
                data: {
                    displayName: 'Pesquisa de Satisfação'
                }
            })
            /*Fim Instrumento de formalização*/

        /*Início dos relatórios gerenciais*/
        .state('base.reportCarteiraProject', {
                url: "/relatorio/projeto/carteira",
                templateUrl: "views/report/project/report-carteira-project.html",
                controller: 'ReportCarteiraProjectCtrl',
                data: {
                    displayName: 'Relatório Carteira de Projetos'
                }
            })
            .state('base.reportProjectStage', {
                url: "/relatorio/projeto/estagio",
                templateUrl: "views/report/project/tracking-stage.html",
                controller: 'ReportTrackingStageCtrl',
                data: {
                    displayName: 'Relatório de Acompanhamento de Datas dos Estágios'
                }
            })
            .state('base.reportTrackingFormalizationInstrument', {
                url: "/relatorio/instrumento-formalizacao/acompanhamento",
                templateUrl: "views/report/formalizationInstrument/tracking-formalization-instrument.html",
                controller: 'ReportTrackingFormalizationInstrumentCtrl',
                data: {
                    displayName: 'Relatório de Acompanhamento dos Instrumentos de Formalização',
                    authorizedRoles: [
                        AUTH_ROLES.portfolioAnalist,
                        AUTH_ROLES.promotionAnalist,
                        AUTH_ROLES.director,
                        AUTH_ROLES.manager,
                        AUTH_ROLES.generalSecretary
                    ]
                }
            })
            .state('base.reportPositiveSchedule', {
                url: "/relatorio/agenda-positiva/agendaPositiva",
                templateUrl: "views/report/positiveSchedule/positiveSchedule.html",
                controller: 'ReportPositiveScheduleCtrl',
                data: {
                    displayName: 'Relatório de Agenda Positiva',
                    authorizedRoles: [
                        AUTH_ROLES.informationAnalist,
                        AUTH_ROLES.portfolioAnalist,
                        AUTH_ROLES.promotionAnalist,
                        AUTH_ROLES.director,
                        AUTH_ROLES.manager
                    ]
                }
            })
            .state('base.reportAnnualMonitoring', {
                url: "/relatorio/consultaAcompanhamentoAnual",
                templateUrl: "views/annualMonitoring/reportAnnualMonitoring.html",
                controller: 'ReportAnnualMonitoringCtrl',
                data: {
                    displayName: 'Consultar Relatórios Anuais de Acompanhamento',
                    authorizedRoles: [
                        AUTH_ROLES.informationAnalist,
                        AUTH_ROLES.portfolioAnalist,
                        AUTH_ROLES.promotionAnalist,
                        AUTH_ROLES.director,
                        AUTH_ROLES.manager
                    ]
                }
            })
            .state('base.reportProjectEnvironment', {
                url: "/relatorio/projeto/ambiental",
                templateUrl: "views/report/project/tracking-environment.html",
                controller: 'ReportTrackingEnvironmentCtrl',
                data: {
                    displayName: 'Relatório de Acompanhamento Ambiental dos Projetos',
                    authorizedRoles: [
                        AUTH_ROLES.infraAnalist,
                        AUTH_ROLES.portfolioAnalist,
                        AUTH_ROLES.promotionAnalist,
                        AUTH_ROLES.director,
                        AUTH_ROLES.manager
                    ]
                }
            })
            .state('base.reportProjectTracking', {
                url: "/relatorio/projeto/acompanhamento",
                templateUrl: "views/report/project/tracking.html",
                controller: 'TrackingReportCtrl',
                data: {
                    displayName: 'Relatório de Acompanhamento',
                    authorizedRoles: [
                        AUTH_ROLES.portfolioAnalist,
                        AUTH_ROLES.promotionAnalist,
                        AUTH_ROLES.director,
                        AUTH_ROLES.manager,
                        AUTH_ROLES.informationAnalist
                    ]
                }
            })
            /*Fim dos relatórios gerenciais*/

        /*Início Fale Conosco*/
        .state('requestContact', {
                url: "/fale-conosco",
                templateUrl: "views/contact/request-contact-tpl.html",
                controller: 'RequestContactCtrl'
            })
            /*Fim Fale Conosco*/

        .state('base.activitySearch', {
                url: "/atividade/consultar",
                templateUrl: "views/activity/search-template.html",
                controller: 'ActivitySearchTemplateCtrl',
                authorizedRoles: [
                    AUTH_ROLES.portfolioAnalist
                ]
            })
            .state('base.activity', {
                url: "/atividade/{screen}?id",
                templateUrl: function($stateParams) {
                    var url = "views/activity/template.html";
                    var stateParamScreenOk = typeof $stateParams.screen !== 'undefined' && $stateParams.screen;
                    var stateParamScreenIncluir = $stateParams.screen === "incluir";
                    var stateParamIdOk = typeof $stateParams.id !== undefined && $stateParams.id;

                    if (stateParamScreenIncluir) {
                        return url;
                    } else if (stateParamScreenOk && (!stateParamScreenIncluir && (stateParamIdOk))) {
                        return "views/activity/template.html?screen=" + $stateParams.screen + "&id=" + $stateParams.id;
                    } else {
                        return "views/main.html";
                    }
                },
                controller: 'ActivityTemplateCtrl',
                data: {
                    displayName: "Template de Atividades",
                    authorizedRoles: [
                        AUTH_ROLES.portfolioAnalist
                    ]
                }
            })

        .state('base.satisfactionSurveySearch', {
                url: "/pesquisa-satisfacao/consultar",
                templateUrl: "views/satisfactionSurvey/search.html",
                controller: 'SatisfactionSurveySearchCtrl',
                authorizedRoles: []
            })
            .state('base.satisfactionSurvey', {
                url: "/pesquisa-satisfacao/{screen}?id",
                templateUrl: function($stateParams) {
                    var url = "views/formalizationInstrument/partial/satisfaction-survey-tpl.html";
                    var stateParamScreenOk = typeof $stateParams.screen !== 'undefined' && $stateParams.screen;
                    var stateParamScreenIncluir = $stateParams.screen === "incluir";
                    var stateParamIdOk = typeof $stateParams.id !== undefined && $stateParams.id;

                    if (stateParamScreenIncluir) {
                        return url;
                    } else if (stateParamScreenOk && (!stateParamScreenIncluir && (stateParamIdOk))) {
                        return "views/formalizationInstrument/partial/satisfaction-survey-tpl.html?screen=" + $stateParams.screen + "&id=" + $stateParams.id;
                    } else {
                        return "views/main.html";
                    }
                },
                controller: 'SatisfactionSurveyCtrl',
                data: {
                    displayName: "Pesquisa de Satisfação"
                }
            })

        .state("base.managementMeetingsPanel", {
            url: "/relatorio/painel-reuniao-gerencial",
            abstract: true,
            templateUrl: "views/report/managementMeetingsPanel/management-meetings-panel.html",
            controller: "ReportManagementMeetingsPanelCtrl",
            data: {
                displayName: 'Painel Reunião Gerencial',
                proxy: 'base.managementMeetingsPanel.investFormalizationIndex'
            }
        })

        /*Inicio Sub Abas Management Meetings Panel*/
        .state('base.managementMeetingsPanel.investFormalizationIndex', {
                url: "/indice-formalizacao-investimento",
                abstract: true,
                templateUrl: "views/report/managementMeetingsPanel/partial/invest-formalization-index.html",
                controller: "InvestFormalizationIndexCtrl",
                data: {
                    proxy: 'base.managementMeetingsPanel.investFormalizationIndex.protocolsInYear'
                }
            })
            /* Inicio Sub Abas investFormalizationIndex */
            .state('base.managementMeetingsPanel.investFormalizationIndex.protocolsInYear', {
                url: "/projetos-no-ano",
                templateUrl: "views/report/managementMeetingsPanel/partial/protocols-in-year.html",
                controller: "ProtocolsInYearCtrl",
                data: {
                    displayName: 'Relatório Protocolos no Ano'
                }
            })
            .state('base.managementMeetingsPanel.investFormalizationIndex.projectsInDF', {
                url: "/projetos-em-df",
                templateUrl: "views/report/managementMeetingsPanel/partial/projects-in-df.html",
                controller: "ProjectsInDFCtrl",
                data: {
                    displayName: 'Relatório Projetos em DF'
                }
            })
            .state('base.managementMeetingsPanel.investFormalizationIndex.projectsInYear', {
                url: "/projetos-no-ano",
                templateUrl: "views/report/managementMeetingsPanel/partial/projects-in-year.html",
                controller: "ProjectsInYearCtrl",
                data: {
                    displayName: 'Relatório Projetos no Ano'
                }
            })
            /* Fim Sub Abas investFormalizationIndex */

        .state('base.managementMeetingsPanel.notifications', {
                url: "/notificacoes",
                abstract: true,
                templateUrl: "views/report/managementMeetingsPanel/partial/notifications.html",
                controller: "NotificationsCtrl",
                data: {
                    proxy: 'base.managementMeetingsPanel.notifications.exceededExpectDate',
                }
            })
            /*Inicio Sub Abas Notifications*/
            .state('base.managementMeetingsPanel.notifications.exceededExpectDate', {
                url: "/data-assinatura-passada",
                templateUrl: "views/report/managementMeetingsPanel/partial/exceeded-expect-date.html",
                controller: "ExceededExpectDateCtrl",
                data: {
                    displayName: 'Relatório Documentos com Data Prevista de Assinatura Passada'
                }
            })
            .state('base.managementMeetingsPanel.notifications.intialContact', {
                url: "/contato-inicial",
                templateUrl: "views/report/managementMeetingsPanel/partial/intial-contact.html",
                controller: "IntialContactCtrl",
                data: {
                    displayName: 'Relatório Inicio de Projeto a mais de 120 dias'
                }
            })
            /*Fim Sub Abas Notifications*/

        .state('base.managementMeetingsPanel.satisfactionIndex', {
                url: "/indice-satisfacao",
                abstract: true,
                templateUrl: "views/report/managementMeetingsPanel/partial/satisfaction-index.html",
                controller: "SatisfactionIndexCtrl",
                data: {
                    proxy: 'base.managementMeetingsPanel.satisfactionIndex.satisfactionIndexChart',
                }
            })
            /* Inicio sub-abas satisfactionIndex */
            .state('base.managementMeetingsPanel.satisfactionIndex.satisfactionIndexChart', {
                url: "/pesquisas-recebidas",
                templateUrl: "views/report/managementMeetingsPanel/partial/satisfaction-index-chart.html",
                controller: "SatisfactionIndexChartCtrl",
                data: {
                    displayName: 'Relatório de Índice de Satisfação'
                }
            })
            .state('base.managementMeetingsPanel.satisfactionIndex.surveyReceived', {
                url: "/pesquisas-recebidas",
                templateUrl: "views/report/managementMeetingsPanel/partial/survey-received.html",
                controller: "SurveyReceivedCtrl",
                data: {
                    displayName: 'Relatório de Pesquisas Recebidas'
                }
            })
            /* Fim sub-abas satisfactionIndex */

        .state('base.managementMeetingsPanel.statusReport', {
                url: "/status-report",
                templateUrl: "views/report/managementMeetingsPanel/partial/status-report.html",
                controller: "StatusReportCtrl",
                data: {
                    displayName: 'Relatório Status Report'
                }
            })
            .state('base.managementMeetingsPanel.canceledProtocols', {
                url: "/protocolos-cancelados",
                templateUrl: "views/report/managementMeetingsPanel/partial/canceled-protocols.html",
                controller: "CanceledProtocolsCtrl",
                data: {
                    displayName: 'Relatório Protocolos Cancelados'
                }
            })
            .state('base.managementMeetingsPanel.canceledProjects', {
                url: "/projetos-cancelados",
                templateUrl: "views/report/managementMeetingsPanel/partial/canceled-projects.html",
                controller: "CanceledProjectsCtrl",
                data: {
                    displayName: 'Relatório Projetos Cancelados'
                }
            })
            .state('base.managementMeetingsPanel.positiveSchedule', {
                url: "/agenda-positiva",
                templateUrl: "views/report/managementMeetingsPanel/partial/positive-schedule.html",
                controller: "ManagementPositiveScheduleCtrl",
                data: {
                    displayName: 'Relatório Agenda Positiva'
                }
            })
            /*Fim Sub Abas Management Meetings Panel*/


        .state("base.weeklyMeetingsPanel", {
            url: "/relatorio/painel-reuniao-semanal",
            abstract: true,
            templateUrl: "views/report/weeklyMeetingsPanel/weekly-meetings-panel.html",
            controller: "ReportWeeklyMeetingsPanelCtrl",
            data: {
                proxy: 'base.weeklyMeetingsPanel.goalsDashboard',
                displayName: 'Painel Reunião Semanal'
            }
        })

        /*Inicio Sub Abas Weekly Meetings Panel*/
        .state('base.weeklyMeetingsPanel.goalsDashboard', {
                url: "/painel-metas",
                templateUrl: "views/report/weeklyMeetingsPanel/partial/goals-dashboard.html",
                controller: "GoalsDashboardCtrl",
                data: {
                    displayName: 'Relatório Painel de Metas'
                }
            })
            .state('base.weeklyMeetingsPanel.statusReport', {
                url: "/status-report",
                templateUrl: "views/report/weeklyMeetingsPanel/partial/status-report.html",
                controller: "WeeklyStatusReportCtrl",
                data: {
                    displayName: 'Relatório Status Report'
                }
            })
            .state('base.weeklyMeetingsPanel.positiveSchedule', {
                url: "/agenda-positiva",
                templateUrl: "views/report/weeklyMeetingsPanel/partial/positive-schedule.html",
                controller: "WeeklyPositiveScheduleCtrl",
                data: {
                    displayName: 'Relatório Agenda Positiva'
                }
            })
            /*Fim Sub Abas Weekly Meetings Panel*/

        .state('base.advancedSearch', {
            url: "/busca-avancada",
            templateUrl: "views/advancedSearch/advanced-search.html",
            controller: 'AdvancedSearchCtrl',
            data: {
                displayName: 'Busca Avançada'
            },
            authorizedRoles: [
                AUTH_ROLES.infraAnalist,
                AUTH_ROLES.informationAnalist,
                AUTH_ROLES.portfolioAnalist,
                AUTH_ROLES.promotionAnalist,
                AUTH_ROLES.director,
                AUTH_ROLES.manager,
                AUTH_ROLES.generalSecretary
            ]
        })

        .state('base.home', {
            url: "/",
            templateUrl: "views/main.html",
            controller: 'MainCtrl'
        });
        cfpLoadingBarProvider.latencyThreshold = 100;
        cfpLoadingBarProvider.includeSpinner = false;
        RestangularProvider.setBaseUrl(configuration.localPath + 'api/');
        flowFactoryProvider.defaults = {
            target: configuration.localPath + 'api/anexo',
            permanentErrors: [404, 500, 501],
            chunkSize: (50 * 1024 * 1024)
        };
    })
    .config(['growlProvider',
        function(growlProvider) {
            growlProvider.onlyUniqueMessages(true);
            growlProvider.globalTimeToLive(10000);
            // growlProvider.globalPosition("top-right");
        }
    ]);
