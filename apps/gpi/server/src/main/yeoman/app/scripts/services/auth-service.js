'use strict';

angular.module('gpifrontApp')
    .factory('AuthService', function($http, UserSession, AUTH_EVENTS, $rootScope, $state, configuration, TIPO_USUARIO) {
        var getUserRoles = function(accessToken, previousState) {
            $http({
                method: 'GET',
                url: configuration.localPath + 'api/user/info',
                headers: {
                    'Authorization': 'Bearer ' + accessToken
                }
            }).then(function(response) {
                if (response.status === 200) {
                    UserSession.createUser(response.data.userPrincipal);
                    if (UserSession.getUser().tipo == TIPO_USUARIO.EXTERNO) {
                        $state.go("base.projectSearch")
                    } else {
                        $state.go("base.home");
                    }
                }
            });
        };
        return {
            login: function(credentials, previousState, environment) {
                credentials.grant_type = 'password';
                credentials.client_id = '1';
                credentials.client_secret = '123456';
                credentials.environment = environment;
                var encondedData = $.param(credentials);
                return $http({
                        method: 'POST',
                        url: configuration.localPath + 'api/oauth/token',
                        data: encondedData,
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded'
                        }
                    })
                    .then(function(response) {
                        if (response.status === 200) {
                            UserSession.createToken(response.data.access_token, response.data.expires_in);
                            getUserRoles(response.data.access_token, previousState);
                        }
                    });
            },
            logout: function() {
                var isExternalUser = UserSession.getUser().tipo == TIPO_USUARIO.EXTERNO;
                UserSession.destroy();
                if (isExternalUser) {
                    $state.go('login', {
                        'environment': 'externo'
                    });
                } else {
                    $state.go('login');

                }
            },
            isAuthenticated: function() {
                return UserSession.getAccessToken();
            },
            isUserAuthorized: function(authorizedRoles) {
                var authorized = false;
                if (!authorizedRoles || angular.isUndefined(authorizedRoles)) {
                    return false;
                }
                if (!angular.isArray(authorizedRoles)) {
                    authorizedRoles = [authorizedRoles];
                }
                var userRoles = UserSession.getStringRoles();
                if (userRoles.length > 0 && this.isAuthenticated()) {
                    for (var i = 0; i < userRoles.length; i++) {
                        if (authorizedRoles.indexOf(userRoles[i]) !== -1) {
                            authorized = true;
                            break;
                        }
                    };
                }

                return authorized;
            },

            verifyUserIsAuthenticatedInsideController: function(authorizedFunction) {
                if (!authorizedFunction()) {
                    event.preventDefault();
                    alert('Usuário sem autorização para visualizar a tela');
                    $rootScope.$broadcast(AUTH_EVENTS.notAuthorized);
                    $state.go('base.home');
                }
            }
        };
    })
    .run(function($rootScope, AUTH_EVENTS, AuthService, $state, ScreenService) {
        $rootScope.$on('$stateChangeStart', function(event, next, toParams, fromState, fromParams) {
            if (next.data && next.data.authenticated) {
                var authorizedRoles = next.data.authorizedRoles;
                var authorizedRolesByType = next.data.authorizedRolesByType;
                var authenticated = AuthService.isAuthenticated();
                if ($rootScope.authError) {
                    event.preventDefault();
                } else if (!authenticated) {
                    event.preventDefault();
                    $rootScope.$broadcast(AUTH_EVENTS.notAuthenticated, next.name);
                } else if (authorizedRolesByType && !angular.isUndefined(next.data['authorizedRolesByType'])) {
                    var type = toParams.screen.toUpperCase();
                    if (!AuthService.isUserAuthorized(next.data['authorizedRolesByType'][type], next.data['authorizedFunction'])) {
                        event.preventDefault();
                        alert('Usuário sem autorização para visualizar a tela');
                        $rootScope.$broadcast(AUTH_EVENTS.notAuthorized);
                        $state.go('base.home');
                    }
                } else if (next.data['authorizedRoles'] && !angular.isUndefined(next.data['authorizedRoles']) && !AuthService.isUserAuthorized(authorizedRoles)) {
                    event.preventDefault();
                    alert('Usuário sem autorização para visualizar a tela');
                    $rootScope.$broadcast(AUTH_EVENTS.notAuthorized);
                    $state.go('base.home');
                }

            }
        });
        $rootScope.$on(AUTH_EVENTS.notAuthenticated, function(scope, nextScreen) {
            $rootScope.previousState = nextScreen;
            $rootScope.authError = AUTH_EVENTS.notAuthenticated;
            $state.go('login');
        });
        $rootScope.$on(AUTH_EVENTS.sessionTimeout, function(scope, currentScreen) {
            $rootScope.previousState = currentScreen;
            $rootScope.authError = AUTH_EVENTS.sessionTimeout;
            $state.go('login');
        });
    })
