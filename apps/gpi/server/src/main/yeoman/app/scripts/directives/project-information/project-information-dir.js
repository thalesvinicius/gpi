/*  PROJECT-INFORMATION

    @author: lucasgom

    Example of usage:
    
    ***********HTML**************
    <project-information nomeEmpresa="projeto.empresa.razaoSocial" 
    nomeProjeto="projeto.nome" versaoProjeto="projeto.versao" estagioAtualProjeto="projeto.estagioAtual" situacaoProjeto="projeto.situacao"></project-information>    

    **********Javascript*********
    You need to have the object referenced in htlm filled.
*/
angular.module('gpifrontApp')
    .directive('projectInformation', function($compile, $rootScope, configuration) {
        return {
            restrict: 'E',
            templateUrl: configuration.rootDir + 'scripts/directives/project-information/project-information.html',
            scope: {
                nomeEmpresa: "=nomeempresa",
                nomeProjeto: "=nomeprojeto",
                versaoProjeto: "=versaoprojeto",
                estagioAtualProjeto: "=estagioatualprojeto",
                situacaoProjeto: "=situacaoprojeto"
            },
            link: function(scope, iElement, iAttrs) {
                scope.$watch('nomeEmpresa', function(newVal, oldVal) {
                    if (scope.nomeEmpresa === undefined) {
                        scope.nomeEmpresa = "";
                    }
                    if (scope.nomeProjeto === undefined) {
                        scope.nomeProjeto = "";
                    }
                    if (scope.versaoProjeto === undefined) {
                        scope.versaoProjeto = "";
                    }
                    if (scope.estagioAtualProjeto === undefined) {
                        scope.estagioAtualProjeto = "";
                    }
                    if (scope.situacaoProjeto === undefined) {
                        scope.situacaoProjeto = "";
                    }
                }, true);

            }
        };
    });
