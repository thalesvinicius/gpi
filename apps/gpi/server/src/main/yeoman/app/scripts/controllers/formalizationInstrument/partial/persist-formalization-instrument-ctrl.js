'use strict';
angular.module('gpifrontApp')
    .controller('PersistFormalizationInstrumentCtrl', function($scope, Restangular, growl, configuration, $filter, dialogs, $state, $stateParams, UserSession, AUTH_ROLES, AuthService, ScreenService, GridButton, TIPO_INSTRUMENTO_FORMALIZACAO, SITUACAO_INSTRUMENTO_FORMALIZACAO, $http) {
        //--------------------------------------------------------------------- Início Configurações inicial da tela
        // Usado para validar o form
        $scope.formSubmitted = false;

        $scope.screenService = ScreenService;

        $scope.selectedItems = [];
        $scope.addedCompanies = [];
        $scope.projetos = [];
        $scope.formalizationTypes = TIPO_INSTRUMENTO_FORMALIZACAO.getAll();
        $scope.tipoInstrumentoConstant = TIPO_INSTRUMENTO_FORMALIZACAO;
        $scope.situacaoInstrumentoConstant = SITUACAO_INSTRUMENTO_FORMALIZACAO;

        $scope.sortInfo = {
            fields: ['nome'],
            directions: ['asc']
        }

        $scope.injectFuncion = ["emailValidation"];

        $scope.emailValidation = function(email) {
            var pattern = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            if (email && !email.match(pattern)) {
                growl.error("E-Mail Inválido");
            }
        }

        $scope.columnDefs = [{
            field: 'nome',
            enableCellEdit: true,
            width: "**",
            translationKey: 'common.name',
            editableCellTemplate: '<input type="text" placeholder="{{\'common.name\' | translate}}" ng-class="\'colt\' + col.index" ng-input="COL_FIELD" ng-model="COL_FIELD" maxlength="100">'

        }, {
            field: 'cargo',
            enableCellEdit: true,
            width: "**",
            translationKey: 'common.job.title',
            editableCellTemplate: '<input type="text" placeholder="{{\'common.job.title\' | translate}}" ng-class="\'colt\' + col.index"  ng-input="COL_FIELD" ng-model="COL_FIELD" maxlength="100" required>'
        }, {
            field: 'telefone1',
            enableCellEdit: true,
            width: "*",
            translationKey: 'common.phone1',
            editableCellTemplate: '<input type="phone" placeholder="{{\'common.phone1\' | translate}}" ng-class="\'colt\' + col.index" maxlength="18" ng-input="COL_FIELD" ng-model="COL_FIELD">'
        }, {
            field: 'telefone2',
            enableCellEdit: true,
            width: "*",
            translationKey: 'common.phone2',
            editableCellTemplate: '<input type="phone" placeholder="{{\'common.phone2\' | translate}}" ng-class="\'colt\' + col.index" maxlength="18" ng-input="COL_FIELD" ng-model="COL_FIELD"/>'

        }, {
            field: 'email',
            enableCellEdit: true,
            width: "**",
            translationKey: 'common.email',
            editableCellTemplate: '<input placeholder="example@email.com" ng-class="\'colt\' + col.index" maxlength="50"  ng-model="COL_FIELD" ng-blur="emailValidation(COL_FIELD)"/>'
        }];

        $scope.buttonsConfig = {
            newButton: new GridButton(true, 'formalizationInstrument.newSubscriber', false, null, null, ''),
            deleteButton: new GridButton(true, 'common.delete', false, null, deleteFn, '')
        }

        function deleteFn() {
            growl.info($filter('translate')('message.error.MSG38_SECAO_ATUALIZADA', {
                value: "Instrumento de Formalização"
            }));
        }

        // Chamado ao selecionar uma empresa ou unidade no autocomplete
        $scope.empresaSelected = function(selected) {
            if (selected != null) {
                $scope.empresaUnidadeSelecionadaPrev = selected;
            } else {
                $scope.empresaUnidadeSelecionadaPrev = null;
            }
        };

        // Atualiza a lista de empresas na medida que for digitado algo
        $scope.updateCompanies = function(typed) {
            if (typed) {
                var filtro = {
                    nomeEmpresa: typed
                }
                return Restangular.all('empresa/allEmpresaUnidadeDTOAtivas').getList(filtro);
            }
            return [];
        };

        var addProjectsToAvailable = function(newProjects) {
            _.each(newProjects, function(newProject) {
                var projectAdded = false;
                for (var i = 0; i < $scope.projetos.length; i++) {
                    if ($scope.projetos[i].id === newProject.id) {
                        projectAdded = true;
                        break;
                    }
                };
                if (!projectAdded) {
                    for (var i = 0; i < $scope.instrumentoFormalizacao.projetos.length; i++) {
                        if ($scope.instrumentoFormalizacao.projetos[i].id === newProject.id) {
                            projectAdded = true;
                            break;
                        }
                    };
                }
                if (!projectAdded) {
                    $scope.projetos.push(newProject);
                }
            });
        };

        var removeProjects = function(projectsToRemove) {
            var availableProjectsId = [];
            var selectedProjectsId = [];
            for (var a = 0; a < projectsToRemove.length; a++) {
                var removeProject = projectsToRemove[a];
                var canRemove = true;
                for (var b = 0; b < $scope.addedCompanies.length; b++) {
                    var company = $scope.addedCompanies[b];
                    for (var c = 0; c < company.projetos.length; c++) {
                        if (company.projetos[c].id === removeProject.id) {
                            canRemove = false;
                        }
                    };
                };
                if (canRemove) {
                    for (var b = 0; b < $scope.projetos.length; b++) {
                        if ($scope.projetos[b].id === removeProject.id) {
                            availableProjectsId.push(b);
                        }
                    };
                    for (var b = 0; b < $scope.instrumentoFormalizacao.projetos.length; b++) {
                        if ($scope.instrumentoFormalizacao.projetos[b].id === removeProject.id) {
                            selectedProjectsId.push(b);
                        }
                    };
                }
            };
            availableProjectsId = availableProjectsId.sort();
            selectedProjectsId = selectedProjectsId.sort();
            for (var i = availableProjectsId.length - 1; i >= 0; i--) {
                $scope.projetos.splice(availableProjectsId[i], 1);
            };
            for (var i = selectedProjectsId.length - 1; i >= 0; i--) {
                $scope.instrumentoFormalizacao.projetos.splice(selectedProjectsId[i], 1);
            };
        };

        // Adiciona a empresa selecionada
        $scope.addCompany = function(noDuplicateMessage) {
            //  TODO validar se a empresa foi inserida corretamente
            var added = false;
            var isUnit = ($scope.empresaUnidadeSelecionadaPrev.unidadeId && !angular.isUndefined($scope.empresaUnidadeSelecionadaPrev.unidadeId));
            _.each($scope.addedCompanies, function(company) {
                if (noDuplicateMessage != true && company.empresaId === $scope.empresaUnidadeSelecionadaPrev.empresaId && company.unidadeId === $scope.empresaUnidadeSelecionadaPrev.unidadeId) {
                    added = true;
                    growl.error($filter('translate')("message.error.MSG45_EMPRESA_SELECIONADA_DUPLICADA"));
                }
            });
            if (!added) {
                var addedCompanyIndex = $scope.addedCompanies.push($scope.empresaUnidadeSelecionadaPrev) - 1;

                if (isUnit) {
                    Restangular.all("instrumento/avaibleProjects/unidade").getList({
                        idUnidade: $scope.empresaUnidadeSelecionadaPrev.unidadeId
                    }).then(
                        function(projects) {
                            $scope.addedCompanies[addedCompanyIndex].projetos = projects;
                            addProjectsToAvailable(projects);
                        });
                } else {
                    Restangular.all("instrumento/avaibleProjects/empresa").getList({
                        idEmpresa: $scope.empresaUnidadeSelecionadaPrev.empresaId
                    }).then(
                        function(projects) {
                            $scope.addedCompanies[addedCompanyIndex].projetos = projects;
                            addProjectsToAvailable(projects);
                        });
                }
            }

            $scope.empresaUnidadeSelecionadaPrev = null;
            $scope.empresaUnidadeSelecionadaTxt = null;
        };

        $scope.removeCompany = function(companyUnit) {
            var index;
            for (var i = 0; i < $scope.addedCompanies.length; i++) {
                if ($scope.addedCompanies[i].empresaId == companyUnit.empresaId && $scope.addedCompanies[i].unidadeId == companyUnit.unidadeId) {
                    index = i;
                    break;
                }
            };
            var projects = $scope.addedCompanies[index].projetos;
            $scope.addedCompanies.splice(index, 1);
            removeProjects(projects);
        };

        function disableDatePicker() {
            return $scope.screenService.isShowScreen() || (!$scope.screenService.isNewScreen() && $scope.instrumentoFormalizacao.situacaoEnum != SITUACAO_INSTRUMENTO_FORMALIZACAO["EM_NEGOCIACAO"].id);
        }

        function initialize() {
            $scope.prospeccaoAtiva = "false";
            $scope.empresasUnidadesDTO = [];

            Restangular.all('domain/situacaoInstrumentoFormalizacao').getList().then(function(result) {
                $scope.situacaoInstrumentoFormalizacaoList = result;
            })

            getInstrumentoFormalizacao();

            // Configurações do datepicker
            $scope.configsDatepickerScheduledDate = {
                required: true,
                disabled: disableDatePicker()
            };

            $scope.disableJustificative = true;
        };

        $scope.showJustificative = function() {
            return $scope.instrumentoFormalizacao.situacaoEnum === SITUACAO_INSTRUMENTO_FORMALIZACAO["CANCELADO"].id || $scope.instrumentoFormalizacao.situacaoEnum === SITUACAO_INSTRUMENTO_FORMALIZACAO["EXTINTO"].id;
        }

        $scope.situationChanged = function() {
            if ($scope.instrumentoFormalizacao.situacaoEnum == $scope.instrumentoFormalizacao.situacaoAtual.situacao.id) {
                $scope.instrumentoFormalizacao.justificativa = $scope.instrumentoFormalizacao.situacaoAtual.justificativa;
                $scope.disableJustificative = true;
            } else {
                $scope.disableJustificative = false;
                $scope.instrumentoFormalizacao.justificativa = null;
            }

            $scope.configsDatepickerScheduledDate.disabled = disableDatePicker();
        }

        $scope.disabledProjetoEmpresa = function() {
            return $scope.instrumentoFormalizacao.situacaoAtual.situacao.id != SITUACAO_INSTRUMENTO_FORMALIZACAO.EM_NEGOCIACAO.id;
        }

        var lastProtocolVerified = null;
        var lastProcotolErrorMessage = null;
        $scope.checkProtocoloIntecoes = function() {

            if ($scope.instrumentoFormalizacao.protocolo !== '' && lastProtocolVerified !== $scope.instrumentoFormalizacao.protocolo) {
                Restangular.one("instrumento/originalProjetosAtuais").get({
                    'protocolo': $scope.instrumentoFormalizacao.protocolo,
                    'instrumentoId': $scope.instrumentoFormalizacao.id
                }).then(function(serverInstrument) {
                    if (serverInstrument) {
                        $scope.instrumentoFormalizacao.projetos = serverInstrument.projetosSelecionados;
                        $scope.addedCompanies = [];
                        if (!angular.isUndefined(serverInstrument.empresasSelecionadas) && serverInstrument.empresasSelecionadas[0]) {
                            for (var i = 0; i < serverInstrument.empresasSelecionadas.length; i++) {
                                $scope.empresaUnidadeSelecionadaPrev = serverInstrument.empresasSelecionadas[i];
                                $scope.addCompany(true);
                            };
                        }
                        lastProcotolErrorMessage = null;
                    }
                }, function(response) {
                    $scope.instrumentoFormalizacao.protocolo = '';
                    lastProcotolErrorMessage = response.data;
                    growl.error($filter('translate')(lastProcotolErrorMessage));
                });
                lastProtocolVerified = $scope.instrumentoFormalizacao.protocolo;

            } else if (lastProtocolVerified === $scope.instrumentoFormalizacao.protocolo && lastProcotolErrorMessage !== null) {
                $scope.instrumentoFormalizacao.protocolo = '';
            }

        }

        // Recupera o instrumento de formalizacao
        function getInstrumentoFormalizacao() {
            $scope.instrumentoFormalizacao = {
                projetos: [],
                signatarios: [],
                situacaoAtual: {
                    situacao: {}
                }
            };

            $scope.signatarios = $scope.instrumentoFormalizacao.signatarios;
            if (ScreenService.isNewScreen()) {
                getDadosIncluirInstrumento();
            } else {
                Restangular.one("instrumento", $scope.$parent.getId()).get().then(function(serverInstrument) {
                    // setTimeout(function() {
                        $scope.instrumentoFormalizacao = serverInstrument;
                        $scope.configsDatepickerScheduledDate.restangularUpdated = true;
                        $scope.instrumentoFormalizacao.projetos = serverInstrument.projetosSelecionados;
                        if (!angular.isUndefined(serverInstrument.empresasSelecionadas) && serverInstrument.empresasSelecionadas[0]) {
                            for (var i = 0; i < serverInstrument.empresasSelecionadas.length; i++) {
                                $scope.empresaUnidadeSelecionadaPrev = serverInstrument.empresasSelecionadas[i];
                                $scope.addCompany(true);
                            };
                        }
                        $scope.signatarios = $scope.instrumentoFormalizacao.signatarios;

                        $scope.configsDatepickerScheduledDate.disabled = disableDatePicker();

                    // }, true);
                });
            }
        };

        function getDadosIncluirInstrumento() {
            $scope.instrumentoFormalizacao.situacaoEnum = SITUACAO_INSTRUMENTO_FORMALIZACAO.EM_NEGOCIACAO.id;
            $scope.instrumentoFormalizacao.situacaoAtual = {
                situacao: {
                    id: SITUACAO_INSTRUMENTO_FORMALIZACAO.EM_NEGOCIACAO.id
                }
            }
            $scope.instrumentoFormalizacao.possuiContrapartidaFincGoverno = false;
        };

        /*
         *   Valida o campo passado
         */
        $scope.checkValidation = function(formField) {
            return formField.$invalid && (formField.$dirty || $scope.formSubmitted);
        }

        $scope.projectSelectValidation = function(formField) {
            return $scope.hasNotProjects() && ((formField !== undefined && formField.$dirty) || $scope.formSubmitted);
        }

        $scope.sendTechnicalNote = function() {
            $http({
                method: 'GET',
                url: configuration.localPath + 'api/instrumento/report',
                params: {
                    'id': $stateParams.id
                },
                headers: {
                    Accept: 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'
                },
                responseType: 'arraybuffer'
            }).
            success(function(response) {
                var a = document.createElement("a");
                document.body.appendChild(a);
                a.style = "display: none";
                var blob = new Blob([response], {
                    type: "octet/stream"
                });
                var url = window.URL.createObjectURL(blob);
                a.href = url;
                a.download = "NotaTecnica.docx";
                a.click();
                window.URL.revokeObjectURL(url);
            }).
            error(function(data, status, headers, config) {
                growl.error("Não foi possível realizar a impressão da Nota Técnica. Tente novamente e se o erro persistir favor contatar o administrador do sistema.")
            });
        }

        /*
         * Retorna true se o Instrumento de Formalização é do tipo Termo Aditivo.
         */
        $scope.isTipoTermoAditivo = function() {
            return $scope.instrumentoFormalizacao.tipo === $scope.tipoInstrumentoConstant['TERMO_ADITIVO'].id
        }

        /*
         *  Verifica se o select de tipo do Instrumento de Formalização deve estar habilitado.
         */
        $scope.disableEditInstrumentoFormalizacaoTipo = function(formFormalizationInstrument) {
            return $scope.isTipoTermoAditivo() && ScreenService.isEditScreen() && !formFormalizationInstrument.$dirty
        }

        /*
         *  Caso o Usuário Interno tenha acionado o comando Incluir Signatário e não ter
         *  informado o campo Nome e Telefone 1, o GPI deverá exibir a mensagem
         *   MSG73_DADOS_DO_CONTATO_NAO_INFORMADO
         */
        var validateSignatarios = function() {
            var isValid = true;
            if (!angular.isUndefined($scope.instrumentoFormalizacao.signatarios) && $scope.instrumentoFormalizacao.signatarios.length !== 0) {
                _.each($scope.instrumentoFormalizacao.signatarios, function(signatario) {
                    if (angular.isUndefined(signatario.nome) || !signatario.nome || angular.isUndefined(signatario.telefone1) || !signatario.telefone1) {
                        isValid = false;
                    }
                });
            }
            return isValid;
        }

        /*
         *   Replacer utilizado para remover a propriedade id do método JSON.stringify
         */
        var replacer = function(key, value) {
            if (key === "id") {
                return undefined;
            } else return value;
        }

        /*
         *   Caso o Usuário Interno informe um registro já existente, o GPI aciona a
         *   EX36_SIGNATÁRIO_DUPLICADO - MSG47_SIGNATÁRIO_DUPLICADO
         */
        var isSignatarioDuplicado = function() {
            if ($scope.instrumentoFormalizacao.signatarios.length > 1) {
                for (var i = 0; i < $scope.instrumentoFormalizacao.signatarios.length; i++) {
                    var signatarioIndex = JSON.stringify($scope.instrumentoFormalizacao.signatarios[i], replacer);
                    for (var j = i + 1; j < $scope.instrumentoFormalizacao.signatarios.length; j++) {
                        var signatarioCompare = JSON.stringify($scope.instrumentoFormalizacao.signatarios[j], replacer);
                        if (signatarioIndex === signatarioCompare) {
                            return true;
                        }
                    };
                };
            }
            return false;
        }

        // $scope.showJustificative = function() {
        //     if ($scope.instrumentoFormalizacao.situacaoAtual != undefined) {
        //         return $scope.instrumentoFormalizacao.situacaoAtual.situacao.id === SITUACAO_INSTRUMENTO_FORMALIZACAO["CANCELADO"].id || $scope.instrumentoFormalizacao.situacaoAtual.situacao.id === SITUACAO_INSTRUMENTO_FORMALIZACAO["EXTINTO"].id;
        //     }
        //     return false;
        // }

        $scope.hasNotProjects = function() {
            return $scope.instrumentoFormalizacao.projetos === undefined || $scope.instrumentoFormalizacao.projetos.length === 0;
        }

        /*
         *   Chamado pelo SALVAR nas telas de EDITAR e INCLUIR
         */
        $scope.submit = function(isValid) {
            if (!validateSignatarios()) {
                growl.error($filter('translate')("message.error.MSG73_DADOS_DO_CONTATO_NAO_INFORMADO"));
            } else if (isSignatarioDuplicado()) {
                growl.error($filter('translate')("message.error.MSG47_SIGNATÁRIO_DUPLICADO"));
            } else if ($scope.hasNotProjects()) {
                growl.error($filter('translate')("message.error.MSG001_INFORMAR_CAMPOS_OBRIGATORIOS", {
                    field: "Projetos"
                }));
            } else if (isValid && !angular.isUndefined($scope.instrumentoFormalizacao.projetos)) {

                var operation = Restangular.all('instrumento');
                operation.post($scope.instrumentoFormalizacao).then(function(id) {
                    //  Para atualizar as datas de todos os datepickers de acordo com as salvas no servidor
                    $scope.configsDatepickerScheduledDate.restangularUpdated = !$scope.configsDatepickerScheduledDate.restangularUpdated;
                    $scope.formFormalizationInstrument.$setPristine(true);
                    growl.success($filter('translate')("message.success.MSG006_REGISTRO_GRAVADO_SUCESSO"));

                    setTimeout(function() {
                        if (ScreenService.isNewScreen()) {
                            $state.go("base.formalizationInstrument.formalizationInstrumentTpl", {
                                "screen": "editar",
                                "id": id
                            });
                        } else {
                            $state.transitionTo($state.current, $stateParams, {
                                reload: true,
                                inherit: true,
                                notify: true
                            });
                        }
                    }, 2000);

                }, function() {
                    //  Para atualizar as datas de todos os datepickers de acordo com as salvas no servidor
                    $scope.configsDatepickerScheduledDate.restangularUpdated = !$scope.configsDatepickerScheduledDate.restangularUpdated;
                    growl.error("Ocorreu um erro ao tentar gravar o registro!");
                });


            } else {
                growl.error($filter('translate')("message.error.MSGXX_DADOS_INVALIDOS"));
                $scope.formSubmitted = true;
            }
        }

        initialize();
    });
