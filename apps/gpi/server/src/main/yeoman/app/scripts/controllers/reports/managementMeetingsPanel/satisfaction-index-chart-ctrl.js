'use strict'

angular.module('gpifrontApp')
    .controller('SatisfactionIndexChartCtrl', function($scope, ScreenService, Restangular) {
        $scope.screenService = ScreenService;
        $scope.dados = [];

        // Define os filtros que serão utilizados na busca.
        $scope.filterOptions = {
            estagio: false,
            anual: false,
            regiaoPlanejamento: false,
            gerencia: false,
            cadeiaProdutiva: false,
            prospeccao: false,
            intervaloDatas: false,
            mesAno: true,
            tipoInstrumentoFormalizacao: false
        };

        var initGraphics = function() {
            c3.generate($scope.chart1);
        }

        $scope.initData = function(result) {
            $scope.chart1 = {
                data: {
                    x:'x',
                    columns: result,
                    type: 'bar',
                    types: {
                        'Meta': 'line'
                    }
                },
                axis: {
                    x: {
                        type: 'category'
                    }
                }
            };
            result.push(['x', 'JAN', 'FEV', 'MAR', 'ABR', 'MAI', 'JUN', 'JUL', 'AGO', 'SET', 'OUT', 'NOV', 'DEZ']);
            initGraphics();
        }

    });
