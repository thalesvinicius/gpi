'use strict';

angular.module('gpifrontApp')
    .controller('InputsAndProductsMainCompetitorsGridCtrl', function($scope, Restangular, $filter, $rootScope, GridButton, growl) {

        $scope.concorrentes = $scope.$parent.insumoProduto.concorrentes;

        $scope.selectedItems = [];
       
        $scope.columnDefs = [{
            field: 'nome',
            enableCellEdit: true,
            width: "**",
            translationKey: 'inputsandproducts.competitors.minasgerais',
            editableCellTemplate: '<input type="text" placeholder="{{\'inputsandproducts.competitors.minasgerais\' | translate}}" ng-class="\'colt\' + col.index"  ng-input="COL_FIELD" ng-model="COL_FIELD" maxlength="100"/>'
        }, {
            field: 'descricaoProduto',
            enableCellEdit: true,
            width: "**",
            translationKey: 'common.products',
            editableCellTemplate: '<input type="text" placeholder="{{\'common.products\' | translate}}" ng-class="\'colt\' + col.index"  ng-input="COL_FIELD" ng-model="COL_FIELD" maxlength="100"/>'
        }];

        // function newFn() {
        //     $scope.$parent.empresa.contatosPessoa.unshift({"empresa_id": $scope.$parent.empresa.id});

        //     return false;
        // }

        $scope.buttonsConfig = {
            //function GridButton(show, translationKey, searchMode, disable, action, screenLink)
            newButton: new GridButton(true, 'common.new', false, null, null, ''),
            deleteButton: new GridButton(true, 'common.delete', false, null, deleteFn, '')
        }

        function deleteFn() {
            growl.info($filter('translate')('message.error.MSG38_SECAO_ATUALIZADA', {
                value: "Principais Concorrentes"
            }));                        
        }                   

        $scope.sortInfo = {
            fields: ['nome'],
            directions: ['asc']
        };

        $scope.$parent.$watch('insumoProduto', function(newVal, oldVal) {
            if (newVal.concorrentes !== oldVal.concorrentes) {
                $scope.concorrentes = newVal.concorrentes;
            }
        }, true);
    });
