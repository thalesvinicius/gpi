'use strict';

angular.module('gpifrontApp')
  .controller('MultiselectCtrl', function($scope) {

    // $scope.roles = [
    //     {roleId: 1, roleName: "Acre"},
    //     {roleId: 2, roleName: "Amazonas"},
    //     {roleId: 3, roleName: "Minas Gerais"},
    //     {roleId: 4, roleName: "Rio de Janeiro"},
    //     {roleId: 5, roleName: "São Paulo"}
    // ];
    
    // $scope.user = {
    //     userId: 1, 
    //     username: "JimBob",
    //     roles: [$scope.roles[0]]
    // };
})
.directive('multiSelect', function($q, configuration) {
  return {
    restrict: 'E',
    require: 'ngModel',
    templateUrl: configuration.rootDir + "scripts/directives/multiselect/multiselect-dir-tpl.html",
    scope: {
      selectedLabel: "@",
      availableLabel: "@",
      displayAttr: "@",
      available: "=",
      model: "=ngModel",
      handler: "&"
    },
    link: function(scope, elm, attrs) {
      scope.selected = {
        available: [],
        current: []
      };

      /* Handles cases where scope data hasn't been initialized yet */
      var dataLoading = function(scopeAttr) {
        var loading = $q.defer();
        if(scope[scopeAttr]) {
          loading.resolve(scope[scopeAttr]);
        } else {
          scope.$watch(scopeAttr, function(newValue, oldValue) {
            if(newValue !== undefined)
              loading.resolve(newValue);
          });  
        }
        return loading.promise;
      };

      /* Filters out items in original that are also in toFilter. Compares by reference. */
      var filterOut = function(original, toFilter) {
        var filtered = [];
        angular.forEach(original, function(entity) {
          var match = false;
          for(var i = 0; i < toFilter.length; i++) {
            if(toFilter[i][attrs.displayAttr] == entity[attrs.displayAttr]) {
              match = true;
              break;
            }
          }
          if(!match) {
            filtered.push(entity);
          }
        });
        return filtered;
      };

      scope.refreshAvailable = function() {
        scope.available = filterOut(scope.available, scope.model);
        scope.selected.available = [];
        scope.selected.current = [];
      }; 

      scope.add = function() {
        scope.model = scope.model ? scope.model.concat(scope.selected.available) : scope.selected.available;
        scope.refreshAvailable();
        scope.handler();
      };

      scope.remove = function() {
        scope.available = scope.available.concat(scope.selected.current);
        scope.model = filterOut(scope.model, scope.selected.current);
        scope.refreshAvailable();
        scope.handler();
      };

      $q.all([dataLoading("model"), dataLoading("available")]).then(function(results) {
        scope.refreshAvailable();
      });
      
    }
  };
})