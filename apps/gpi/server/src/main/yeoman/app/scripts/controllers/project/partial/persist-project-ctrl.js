'use strict';
angular.module('gpifrontApp')
    .controller('PersistProjectCtrl', function($log, $scope, Restangular, growl, configuration, $filter, dialogs, $state, $stateParams, UserSession, SITUACAO_PROJETO, ESTAGIO_PROJETO, AUTH_ROLES, AuthService, TIPO_USUARIO, ScreenService, SITUACAO_EMPRESA) {
        $scope.isExternalUser = UserSession.getUser().tipo == TIPO_USUARIO.EXTERNO;

        $scope.projeto = {
            anexos: [],
            empresaUnidadeDTO: {},
            empresa: {}
        };

        /*
         *   Verifica se o usuário logado é responsável pelo projeto selecionado
         */
        var isResponsavelProjeto = function() {
            return $scope.projeto.usuarioInvestimento.id === UserSession.getUser().id;
        }

        /*
         *   Verifica se o usuário logado tem permissão de editar projeto
         */
        var usuarioPossuiPermissao = function() {
            return AuthService.isUserAuthorized([AUTH_ROLES.infraAnalist, AUTH_ROLES.portfolioAnalist, AUTH_ROLES.promotionAnalist, AUTH_ROLES.manager, AUTH_ROLES.externalResponsible]);
        }

        /*
         *   Verifica se o usuário logado é responsável pelo departamento
         */
        var isResponsavelDepartamento = function() {
            return $scope.projeto.usuarioInvestimento.departamento.id === UserSession.getUser().departamento.id;
        }

        /*
         *   Verifica se o usuário logado é gerente
         */
        var isGerente = function() {
            return AuthService.isUserAuthorized([AUTH_ROLES.manager]);
        }

        var podeEditarQualquerProjeto = function() {
            return AuthService.isUserAuthorized([AUTH_ROLES.infraAnalist, AUTH_ROLES.portfolioAnalist]);
        }

        $scope.canEditProject = function() {
            var canEdit = true;
            if (ScreenService.isEditScreen()) {
                canEdit = ($scope.projeto.empresa.situacao === SITUACAO_EMPRESA["ativa"].id // Só pode editar um projeto em que situação da Empresa ou da Empresa vinculada à Unidade for "Ativo"
                    && (($scope.isExternalUser && $scope.projeto.estagioAtual === ESTAGIO_PROJETO["INICIO_PROJETO"].id) // Usuario externo só pode editar projeto em estagio inicio de projeto
                        || (isGerente() && isResponsavelDepartamento()) // Caso o usuário logado for um gerente ele poderá editar um projeto se ele for o responsável pelo departamento
                        || (usuarioPossuiPermissao() && isResponsavelProjeto()) || podeEditarQualquerProjeto())); // Se o usuário for responsável pelo projeto ou possuir permissão para editar qualquer projeto
            }
            return canEdit;
        }


        //--------------------------------------------------------------------- Início Configurações inicial da tela
        // Usado para validar o form
        $scope.formSubmitted = false;

        $scope.screenService = ScreenService;
        $scope.openCompanyOrUnit = "base.projectSearch";

        // Chamado ao selecionar uma empresa ou unidade no autocomplete
        $scope.empresaSelected = function(selected) {
            $scope.empresaUnidadeSelecionadaPrev = selected;
        };

        /*
         *   Redireciona para tela de empresa ou unidade dependendo da selecionada
         */
        $scope.openCompanyOrUnit = function() {
            var path = null;
            var id = null;

            //  se for empresa
            if ($scope.projeto.empresaUnidadeDTO.unidadeId) {
                path = "base.company.unitTpl";
                id = $scope.projeto.empresaUnidadeDTO.unidadeId;
            }
            //  Se for unidade
            else {
                path = "base.company.companyTpl";
                id = $scope.projeto.empresaUnidadeDTO.empresaId;
            }

            $state.go(path, {
                'screen': $scope.screenService.getScreenName(),
                'id': id
            });
        }

        var updateProjetoRecente = function() {
            Restangular.one("projeto").post("recentes", {
                "id": $stateParams.id
            }).then(function(serverProject) {
                console.log("Projeto " + $stateParams.id + " enviado projetos recentes.");
            });
        }

        // Atualiza a lista de empresas na medida que for digitado algo
        $scope.updateCompanies = function(typed) {
            if (typed) {
                var filtro = {
                    nomeEmpresa: typed
                }
                return Restangular.all('empresa/allEmpresaUnidadeDTOAtivas').getList(filtro);
            }
            return [];
        };

        // Adiciona a empresa selecionada
        $scope.addCompany = function() {
            $scope.projeto.empresaUnidadeDTO = $scope.empresaUnidadeSelecionadaPrev;

            $scope.empresaUnidadeSelecionadaPrev = null;
            $scope.empresaUnidadeSelecionadaTxt = null;
        };

        function initialize() {
            $scope.prospeccaoAtiva = "false";
            $scope.empresasUnidadesDTO = [];

            //  Cadeia Produtiva
            Restangular.all('domain/cadeiaProdutiva').getList().then(function(cadeiasProdutivas) {
                $scope.cadeiasProdutivas = cadeiasProdutivas;
            });

            Restangular.all('user').getList({
                ativo: true
            }).then(function(usuarios) {
                $scope.usuarios = usuarios;
            });

            // Restangular.all('empresa/allEmpresaUnidadeDTOAtivas').getList().then(function(result) {
            //     $scope.empresasUnidadesDTO = result;
            // });

            $scope.situacoes = SITUACAO_PROJETO.todas;

            // Configurações do datepicker
            $scope.configsDatepickerProject = {
                required: true,
                disabled: !$scope.screenService.isEditScreen() || !$scope.canEditProject()
            };

            getProjeto();
        };

        // Recupera o projeto
        function getProjeto() {
            if ($scope.screenService.isNewScreen()) {
                getDadosIncluirProjeto();
            } else {
                Restangular.one("projeto", $stateParams.id).get().then(function(serverProject) {
                    $scope.projeto = serverProject;

                    updateDatepicker();
                    $scope.projeto.estagio = ESTAGIO_PROJETO[$scope.projeto.estagioAtual].descricao;

                    $scope.prospeccaoAtiva = serverProject.prospeccaoAtiva === true ? "true" : "false";
                    // Seleciona a cadeiaProdutiva salva
                    $scope.projeto.cadeiaProdutiva = {
                        id: serverProject.cadeiaProdutiva.id
                    };

                    $scope.configsDatepickerProject.disabled &= !$scope.canEditProject()

                    updateProjetoRecente();
                });
            }
        };

        $scope.disableResponsavelInvestimento = function() {
            return !AuthService.isUserAuthorized(AUTH_ROLES.manager);
        };

        function getDadosIncluirProjeto() {
            $scope.projeto.prospeccaoAtiva = "false";
            $scope.projeto.cadeiaProdutiva = {};
            $scope.projeto.situacao = SITUACAO_PROJETO["ATIVO"].id;
            $scope.projeto.estagioAtual = ESTAGIO_PROJETO["INICIO_PROJETO"].id;
            $scope.projeto.estagio = ESTAGIO_PROJETO["INICIO_PROJETO"].descricao;

            //  Inicialmente preenchido com o nome do usuário que está incluindo um novo projeto.
            $scope.projeto.usuarioInvestimento = {
                id: UserSession.getUser().id,
                nome: UserSession.getUser().name,
                tipo: UserSession.getUser().tipo
            };

            // Data será a de hoje
            $scope.projeto.dataCadastro = new Date();
        };

        /*
         *   Valida o campo passado
         */
        $scope.checkValidation = function(formField) {
            return formField.$invalid && (formField.$dirty || $scope.formSubmitted);
        }

        //  Para atualizar as datas de todos os datepickers de acordo com as salvas no servidor
        var updateDatepicker = function() {
            $scope.configsDatepickerProject.restangularUpdated = !$scope.configsDatepickerProject.restangularUpdated;
        }

        $scope.showJustificative = function() {
            return $scope.projeto.situacao === SITUACAO_PROJETO["CANCELADO"].id || $scope.projeto.situacao === SITUACAO_PROJETO["SUSPENSO"].id;
        }

        $scope.goBack = function(isValid) {
            window.history.back();
        }

        $scope.disableJustificative = true;
        /*
         *   Chamado pelo SALVAR nas telas de EDITAR e INCLUIR
         */
        $scope.submit = function(isValid) {
            if (isValid) {
                if ($scope.projeto.dataCadastro > new Date().getTime()) {
                    growl.error("Somente é permitido informar data retroativa ou atual.");
                    return;
                }

                $scope.projeto.prospeccaoAtiva = $scope.prospeccaoAtiva === "true";
                if ($scope.screenService.isNewScreen()) {
                    var operation = Restangular.all('projeto');
                    operation.post($scope.projeto).then(function(id) {
                        updateDatepicker();
                        growl.success($filter('translate')("message.success.MSG006_REGISTRO_GRAVADO_SUCESSO"));
                        $scope.formProject.$setPristine();
                        $state.go("base.project.projectTpl", {
                            "screen": "editar",
                            "id": id
                        });
                    }, function() {
                        updateDatepicker();
                        growl.error("Ocorreu um erro ao tentar gravar o registro!");
                    });
                } else {
                    /* RNE080 - O GPI somente deverá permitir a edição de um Projeto para
                       Empresa e Unidade que estejam com situação "Ativa". */
                    if ($scope.projeto.empresa &&
                        $scope.projeto.empresa.situacao !== SITUACAO_EMPRESA['ativa'].id) {
                        growl.error($filter('translate')("Não pode editar projeto de empresa inativa"));
                        return;
                    }

                    $scope.projeto.put().then(function() {
                        updateDatepicker();
                        growl.success($filter('translate')("message.success.MSG006_REGISTRO_GRAVADO_SUCESSO"));
                        $scope.formProject.$setPristine();
                        setTimeout(function() {
                            $state.transitionTo($state.current, $stateParams, {
                                reload: true,
                                inherit: true,
                                notify: true
                            });
                        }, 2000);
                    }, function() {
                        updateDatepicker();
                        growl.error("Ocorreu um erro ao tentar gravar o registro!");
                    });
                }
            } else {
                growl.error($filter('translate')("message.error.MSGXX_DADOS_INVALIDOS"));
                $scope.formSubmitted = true;
            }
        }

        $scope.checkUserAuthorization();
        initialize();

        $scope.situationChanged = function() {
            if ($scope.projeto.situacao == $scope.projeto.situacaoAtual.situacao) {
                $scope.projeto.justificativa = $scope.projeto.situacaoAtual.justificativa;
                $scope.disableJustificative = true;
            } else {
                $scope.disableJustificative = false;
                $scope.projeto.justificativa = null;
            }
        }
    });
