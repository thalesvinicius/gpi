'use strict';
angular.module('gpifrontApp')
    .controller('ContactAttachedFilesGridCtrl', function($scope, GridButton, dialogs, $filter) {

        $scope.gridScope = {};

        $scope.sortInfo = {
            fields: ['fileName'],
            directions: ['asc']
        };

        $scope.buttonsConfig = {
            //function GridButton(show, translationKey, searchMode, disable, action, screenLink)
            newButton: new GridButton(true, 'common.attach.file', false, null, null, ''),
            deleteButton: new GridButton(true, 'common.delete', false, null, null, '')
        }


        //Grid de Arquivos
        $scope.anexos = [];
        $scope.attachedSelectedFiles = [];

        $scope.attachedFilesColumnDefs = [{
            field: "nome",
            translationKey: 'common.file.name',
            enableCellEdit: false
        }, {
            field: "dataAnexo",
            translationKey: 'common.date',
            enableCellEdit: false,
            cellFilter: 'dateTransformFilter'
        }];

        $scope.disableFn = function() {
            return $scope.attachedSelectedFiles.length < 1;
        }

        function translate(translationKey) {
            var text = "";
            if (typeof translationKey !== 'undefined' && translationKey) {
                text = $filter('translate')(translationKey);
            }
            return text;
        };

        $scope.preDeleteFile = function() {
            $scope.gridScope.preDelete();
            for (var i = 0; i < $scope.$flow.files.length; i++) {
                var file = $scope.$flow.files[i];
                file.cancel();
            };

        }

        $scope.$parent.$watch('contato.anexos', function(newVal, oldVal) {
            setTimeout(function() {
                $scope.$apply(function(internalData) {
                    if (internalData) {
                        $scope.anexos = internalData;
                    }
                }(newVal));
                // $scope.anexos = $scope.$parent.contato.anexos;
            }, true);
        }, true);
    });
