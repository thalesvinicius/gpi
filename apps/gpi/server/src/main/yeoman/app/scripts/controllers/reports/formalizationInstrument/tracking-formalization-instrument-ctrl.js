'use strict'
angular.module('gpifrontApp')
    .controller('ReportTrackingFormalizationInstrumentCtrl', function($scope, growl, ScreenService, Restangular, $filter, AuthService, AUTH_ROLES, UserSession, TIPO_INSTRUMENTO_FORMALIZACAO, $http, configuration) {

        $scope.configsDatepickers = {};

        $scope.disableHeader = true;

        /*  Configs iniciais dos filtros    */
        $scope.filtro = {
            gerencias: [],
            analistas: [],
            tipos: [],
            locais: []
        };

        $scope.selectedItems = [];
        $scope.sortInfo = {
            fields: ['dataInicioAtividade'],
            directions: ['desc']
        };

        // RNE163 se for gerente não exibe o campo Gerência e se for analista de portfolio
        //  não exibe os campos Gerência e Analista
        $scope.isGerente = function() {
            return AuthService.isUserAuthorized([AUTH_ROLES.manager]);
        }

        // RNE163 se for gerente não exibe o campo Gerência e se for analista de portfolio
        //  não exibe os campos Gerência e Analista
        $scope.isPromocao = function() {
            return AuthService.isUserAuthorized([AUTH_ROLES.promotionAnalist]);
        }

        /*
         *   Recupera todos os ids da lista passada
         */
         var getIdList = function(list) {
            var idList = [];
            if (list !== undefined) {
                if (_.isArray(list)) {
                    _.each(list, function(val, key) {
                        idList.push(val.id);
                    });
                } else {
                    idList.push(list.id);
                }
            }
            return idList;
        }

        $scope.disabledExport = function() {
            return $scope.data.length === 0;
        }

        /*
         *   Evento chamado ao selecionar gerência
         */
        $scope.changeManageMultiSelect = function() {
            //  Busca os usuários com base no departamento
            if ($scope.filtro.gerencias && $scope.filtro.gerencias.length > 0) {
                Restangular.all('user/findAnalistasByDepartamentos')
                    .getList({"departamentos": $scope.filtro.gerencias})
                        .then(function(analistas) {
                    $scope.analistas = analistas;
                    //  Por default todos os analistas vem selecionados
                    $scope.filtro.analistas = $scope.isPromocao() ? [UserSession.getUser().id] : [];
                });
            } else {
                $scope.analistas = [];
                $scope.filtro.analistas = [];
            }
        }

        /*
         *   Utilizado pelos usuarios GERENTE e PROMOCAO para recuperar as gerencias
         */
        var getDepartamentosUsuarioLogado = function() {
            Restangular.one('user/' + UserSession.getUser().id).get().then(function(user) {
                $scope.filtro.gerencias = getIdList(user.departamento);
                //  Aciona o evento onChange do select de departamentos
                //  para preencher os analistas e as cadeias produtivas
                $scope.changeManageMultiSelect();
            });
        }

        /*  Iniciando defaults do relatório */
        $scope.reset = function() {
            $scope.filtro.previsaoAssinaturaDe = new Date().getTime();
            $scope.filtro.previsaoAssinaturaAte = new Date().getTime();
            $scope.configsDatepickers.restangularUpdated = !$scope.configsDatepickers.restangularUpdated;

            /*  Inicializando as listas com seus respectivos registros selecionados. */
            $scope.tiposInstrumento = TIPO_INSTRUMENTO_FORMALIZACAO.getAll();
            // $scope.filtro.tipos = getIdList($scope.tiposInstrumento);

            Restangular.all('local').getList().then(function(result) {
                // $scope.filtro.locais = getIdList(result);
                $scope.locais = result;
            });

            // RNE163 se for gerente não exibe o campo Gerência e se for analista de portfolio
            //  não exibe os campos Gerência e Analista
            if ($scope.isGerente() || $scope.isPromocao()) {
                getDepartamentosUsuarioLogado();
            } else {
                Restangular.all('domain/departamento').getList().then(function(gerencias) {
                    $scope.gerencias = gerencias;
                    //  Por default todas as gerencias vem selecionadas
                    // $scope.filtro.gerencias = getIdList(gerencias);
                    $scope.changeManageMultiSelect();
                });
            }

            $scope.data = [];
        }

        var initialize = function() {
            $scope.reset();
        }

        initialize();

        var getPath = function(format) {
            return 'relatorio/instrumento/export/' + format;
        }

        var getHeader = function(format) {
            var header = '';
            if (format === 'pdf') {
                header = 'application/pdf';
            } else if (format === 'xls') {
                header = 'application/vnd.ms-excel';
            }
            return header;
        }

        var getFileName = function() {
            return 'RelatorioAcompanhamentoIF';
        }

        $scope.clean = function() {
            $scope.reset();
        }

        var fromDateToMilliseconds = function(date){
            if(date instanceof Date){
                return date.getTime();
            }
            return date;
        }

        $scope.export = function(format) {
            $scope.filtro.previsaoAssinaturaDe = fromDateToMilliseconds($scope.filtro.previsaoAssinaturaDe);
            $scope.filtro.previsaoAssinaturaAte = fromDateToMilliseconds($scope.filtro.previsaoAssinaturaAte);

            $http({
                method: 'GET',
                url: configuration.localPath + "api/" + getPath(format),
                params: {
                    "gerencias": $scope.filtro.gerencias,
                    "analistas": $scope.filtro.analistas,
                    "tipos": $scope.filtro.tipos,
                    'todosTipos' :getIdList($scope.tiposInstrumento),
                    "locais" : $scope.filtro.locais,
                    "previsaoAssinaturaDe": $scope.filtro.previsaoAssinaturaDe,
                    "previsaoAssinaturaAte": $scope.filtro.previsaoAssinaturaAte
                },
                headers: {
                    Accept: getHeader(format),
                    'Authorization': 'Bearer ' + UserSession.getAccessToken(),
                    'todasGerencias': getIdList($scope.gerencias),
                    'todosLocais': getIdList($scope.locais),
                    'todosAnalistas': getIdList($scope.analistas)
                },
                responseType: 'arraybuffer'
            }).
            success(function(response) {
                ScreenService.browserAwareSave(response,"RelatorioAcompanhamentoIF."+format);
            })
        }

        /*Início configs grid*/
        $scope.columnDefs = [{
            field: 'empresa',
            enableCellEdit: false,
            width: "150",
            translationKey: 'common.company'
        }, {
            field: 'titulo',
            enableCellEdit: false,
            width: "300",
            translationKey: 'formalizationInstrument.title'
        }, {
            field: 'previsaoAssinatura',
            enableCellEdit: false,
            width: "250",
            translationKey: 'reportTrackingFormalizationInstrument.signaturePrediction',
            cellFilter: 'dateTransformFilter:\'dd/MM/yyyy\''
        }, {
            field: 'tipo',
            enableCellEdit: false,
            width: "100",
            translationKey: 'common.type'
        }, {
            field: 'localAtividade',
            enableCellEdit: false,
            width: "100",
            translationKey: 'common.local'
        }, {
            field: 'atividade',
            enableCellEdit: false,
            width: "150",
            translationKey: 'common.activity'
        }, {
            field: 'dataInicioAtividade',
            enableCellEdit: false,
            width: "200",
            translationKey: 'reportTrackingFormalizationInstrument.activityStartDate',
            cellFilter: 'dateTransformFilter:\'dd/MM/yyyy\''
        }, {
            field: 'analista',
            enableCellEdit: false,
            width: "100",
            translationKey: 'common.analyst'
        }, {
            field: 'gerencia',
            enableCellEdit: false,
            width: "100",
            translationKey: 'common.manage'
        }];
        /*Fim configs grid*/

        $scope.search = function() {
            $scope.filtro.previsaoAssinaturaDe = fromDateToMilliseconds($scope.filtro.previsaoAssinaturaDe);
            $scope.filtro.previsaoAssinaturaAte = fromDateToMilliseconds($scope.filtro.previsaoAssinaturaAte);

            Restangular.all('relatorio/instrumento/generate').getList($scope.filtro).then(function(result) {
                $scope.data = result;
                if ($scope.data.length === 0) {
                    growl.error($filter('translate')("message.error.MSG004_NENHUM_REGISTRO_ENCONTRADO"));
                }
            }, function errorCallback(response) {
                if (response.status !== 404) {
                    growl.error("Error: The server returned status " + response.status);
                }
            });
        }
    });