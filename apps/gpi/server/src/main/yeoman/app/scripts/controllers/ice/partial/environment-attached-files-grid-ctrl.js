'use strict';
angular.module('gpifrontApp')
    .controller('EnvironmentAttachedFilesGridCtrl', function($scope, GridButton, dialogs, $filter, growl) {

        $scope.meioAmbiente = {anexos: []};
        $scope.gridScope = {};
        $scope.attachedSelectedFiles = [];

        $scope.sortInfo = {
            fields: ['fileName'],
            directions: ['asc']
        };

        $scope.attachedFilesColumnDefs = [{
            field: "nome",
            translationKey: 'common.file.name',
            enableCellEdit: false
        }, {
            field: "dataAnexo",
            translationKey: 'common.date',
            enableCellEdit: false,
            cellFilter: 'dateTransformFilter'
        }];

        $scope.buttonsConfig = {
            //function GridButton(show, translationKey, searchMode, disable, action, screenLink)
            newButton: new GridButton(true, 'common.attach.file', false, null, null, ''),
            deleteButton: new GridButton(true, 'common.delete', false, null, null, '')
        }

        $scope.$parent.$watch('meioAmbiente', function(newVal, oldVal) {
            if (newVal !== oldVal) {
                //Grid de Arquivos
                $scope.meioAmbiente = $scope.$parent.meioAmbiente;
                //  Adicionado para quando anexar um arquivo
                //  e tentar mudar de aba mostra a MSG33_DADOS_NAO_SALVOS
                if($scope.$parent.anexosOriginalLength !== newVal.anexos.length){
                    $scope.formEnvironment.$setDirty();
                }
            }
        }, true);

        $scope.disableFn = function () {
            return $scope.attachedSelectedFiles.length < 1;
        }

        function translate(translationKey) {
            var text = "";
            if (typeof translationKey !== 'undefined' && translationKey) {
                text = $filter('translate')(translationKey);
            }
            return text;
        };

        $scope.preDeleteFile = function() {
            var confirmDialog = dialogs.confirm(translate('DIALOGS_CONFIRMATION'), translate('message.confirm.MSG009_CONFIRMAR_EXCLUSAO'));
            confirmDialog.result.then(function(btn) {
                // Caso confirme executa a exclusão
                $scope.deleteFile();

                growl.info($filter('translate')('message.error.MSG38_SECAO_ATUALIZADA', {value: translate("common.attached.files")}));
            }, function(btn) {
                $scope.gridScope.checkSelectAllCheckBox(true, true);
                $scope.gridScope.checkSelectAllCheckBox(false, true);
                // Caso não confirme deseleciona os registros selecionados
                $scope.attachedSelectedFiles.splice(0, $scope.attachedSelectedFiles.length);
            });

        }

        $scope.deleteFile = function() {
            if ($scope.meioAmbiente.anexos.length > 0 && $scope.attachedSelectedFiles.length > 0) {
                $scope.attachedSelectedFiles.forEach(function(selected) {
                    var model = $scope.meioAmbiente.anexos;
                    var indexOnMainList = model.indexOf(selected);
                    // deleting each unit from main list
                    $scope.meioAmbiente.anexos.splice(indexOnMainList, 1);
                    for (var i = 0; i < $scope.$flow.files.length; i++) {
                        var file = $scope.$flow.files[i];
                        file.cancel();
                    };
                });
                // deleting all 'selectedItems' starting at index 0
                $scope.attachedSelectedFiles.splice(0, $scope.attachedSelectedFiles.length);
            }
        }
    });
