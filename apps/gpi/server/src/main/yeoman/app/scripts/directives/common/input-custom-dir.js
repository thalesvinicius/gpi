/*
    You can use this directive of the two different ways:
    
    Passing the 'custom' type + 'regex' attribute like this: 

    <input type="custom" regex="[0-9]+" maxlength="15" name="addressNumberInput" 
    placeholder="{{'common.number' | translate}}" class="form-control" 
    ng-model="empresa.endereco.numero">

    Or just passing a previously defined type inside the directive like this:

    <input type="numeric" maxlength="15" name="addressNumberInput" 
    placeholder="{{'common.number' | translate}}" class="form-control" 
    ng-model="empresa.endereco.numero">
*/

angular.module('gpifrontApp').directive('input', function() {
    return {
        restrict: 'E',
        require: '?ngModel',
        link: function(scope, element, attr, ngModel) {

            function setInputBehavior(regex) {
                element.unbind('input');
                element.bind('input', function() {
                    var inputValue = this.value;
                    if (inputValue === undefined) {
                        return '';
                    }
                    var transformedValue = !inputValue.match(regex) ? null : inputValue.match(regex).join("");
                    if (transformedValue != inputValue) {
                        ngModel.$setViewValue(transformedValue);
                        ngModel.$render();
                    }
                });
            }

            if (attr.type === 'numeric') {
                // Override the input event and add custom 'path' logic
                setInputBehavior(/[0-9]+/g);
            } else if (attr.type === 'phone') {
                setInputBehavior(/[0-9()-]+/g);
            } else if (attr.type === 'custom') {
                if (typeof attr.regex !== undefined) {
                    setInputBehavior(new RegExp(attr.regex, 'g'));
                }
            } else {
                return;
            }
        }
    };
});
