'use strict';

angular.module('gpifrontApp')
    .controller('IceEnvironmentCtrl', function($scope, UserSession, TIPO_USUARIO, $stateParams, Restangular, growl, CADEIA_PRODUTIVA, $state, ScreenService, AUTH_ROLES, AuthService, ESTAGIO_PROJETO, SITUACAO_PROJETO, TIPO_MEIO_AMBIENTE, CLASSE_EMPREENDIMENTO, $filter, CHANGE_MEIOAMBIENTE) {

        var baseProjetos = Restangular.all('projeto');

        $scope.screenService = ScreenService;
        $scope.classeEmpreendimento = CLASSE_EMPREENDIMENTO;
        $scope.isExternalUser = UserSession.getUser().tipo == TIPO_USUARIO.EXTERNO;
        $scope.selectedetapasMeioAmbiente = [];
        $scope.changes = [];
        $scope.change_meioAmbiente = CHANGE_MEIOAMBIENTE;

        $scope.buscarAlteracoesMeioAmbiente = function() {
            baseProjetos.one($stateParams.id).get().then(function(result) {
                setTimeout(function() {
                    $scope.projeto = result;
                    if (!$scope.externalUser() && $scope.projeto.situacaoAtual.situacao === SITUACAO_PROJETO.PENDENTE_ACEITE.id) {
                        baseProjetos.one($stateParams.id).getList("meioAmbiente/findChange").then(function(result) {
                            $scope.changes = (result[0] === undefined ? 0 : result[0]);
                        }, function() {
                            growl.error("Ocorreu um erro ao tentar buscar o registro!");
                        });
                    }
                }, true);
            });            
        }
        
        // Recupera o Meio ambiente do server
        $scope.getEnvironment = function() {
            Restangular.all('projeto').one($stateParams.id).one("meioAmbiente").getList().then(function(listMeioAmbiente) {
                $scope.meioAmbiente = listMeioAmbiente[0];



                if ($scope.meioAmbiente) {
                    var etapas = [];
                    _.forEach($scope.meioAmbiente.etapasMeioAmbiente, function(etapa) {
                        etapas.push(etapa.tipoMeioAmbiente);
                        // Adiciona os ids para realizar a persistencia
                        
                    });
                    $scope.meioAmbiente.selectedEtapasMeioAmbiente = etapas;
                    $scope.configsDatepickersEnviroment.restangularUpdated = !$scope.configsDatepickersEnviroment.restangularUpdated;
                } else {
                    // Radios inicialmente selecionados como "Sim"
                    $scope.meioAmbiente = {
                        anexos: [],
                        licenciamentoIniciado: true,
                        necessidadeEIARIMA: true,
                        realizadaVistoria: true,
                        pedidoInformacoesComplementar: true,
                        selectedEtapasMeioAmbiente: [],
                        usuarioResponsavel: {
                            nome: ''
                        },
                        dataUltimaAlteracao: ''
                    };
                }


                //  Variável utilizada para verificar se foi alterado os anexos
                $scope.anexosOriginalLength = $scope.meioAmbiente.anexos.length;
            });

            
        }

        $scope.buscarAlteracoesMeioAmbiente();

        //  Variável utilizada para verificar se foi alterado os anexos
        $scope.anexosOriginalLength = 0;

        //  Recupera o Meio Ambiente
        $scope.getEnvironment();

        $scope.externalUser = function() {
            return AuthService.isUserAuthorized([AUTH_ROLES.externalResponsible, AUTH_ROLES.externalMeling, AUTH_ROLES.externalOther]);
        }

        // Configurações iniciais do UC19 Manter Meio Ambiente
        var environmentConfigs = function() {
            $scope.configsDatepickersEnviroment = {
                disabled: $scope.screenService.isShowScreen()
            };

            $scope.meioAmbiente = {
                anexos: []
            };

            // Recupera as classes juntamente com os dias para realizar o calculo do prazo
            $scope.classesEmpreendimentos = $scope.classeEmpreendimento.getAll();
            $scope.etapasMeioAmbiente = TIPO_MEIO_AMBIENTE.getAll();

            // Inicialmente esconde os indicadores do prazo
            $scope.showScheduleThumbs = false;
        }

        environmentConfigs();

        // Watch para verificar se a dataEntregaEstudos está dentro ou fora do prazo
        // Foram feitos 2 watchs para alterar o prazo quando mudar a classe e a data
        $scope.$watch('meioAmbiente.dataEntregaEstudos', function(newVal, oldVal) {
            if (newVal !== oldVal && $scope.meioAmbiente.classeEmpreendimento) {
                $scope.calcSchedule();
            }
        }, true);

        $scope.onChangeClasseEmpreendimento = function() {
            if (!$scope.meioAmbiente.classeEmpreendimento) {
                $scope.showScheduleThumbs = false;
            } else {
                $scope.calcSchedule();
            }
        }

        $scope.calcSchedule = function() {
            $scope.showScheduleThumbs = true;
            Restangular.one("projeto", $stateParams.id).one("cronograma/getDataInicioOperacao").get().then(function(inicioOperacao) {
                if (inicioOperacao === undefined) {
                    // Se o Início de Operação não foi informado na aba Cronograma deverá
                    //constar como "no prazo"
                    $scope.isOnSchedule = true;
                } else {
                    inicioOperacao = inicioOperacao.replace(/\"/g, "");
                    var prazo = $scope.classeEmpreendimento[$scope.meioAmbiente.classeEmpreendimento].dias;
                    var dataInicioOperacao = new Date(inicioOperacao);
                    // Como o cronograma só salva mês e ano o sistema adotou como default
                    // o dia ser sempre o 1
                    dataInicioOperacao.setDate(1);
                    // RNE197 - Se o Usuário Interno selecionar "Não" em "Licenciamento já foi iniciado?"
                    // e informar o campo "Qual a classe do empreendimento?", o GPI deverá considerar a
                    // data do sistema para contar o prazo
                    var dataEntregaEstudos = new Date().getTime();
                    if ($scope.meioAmbiente.dataEntregaEstudos) {
                        dataEntregaEstudos = $scope.meioAmbiente.dataEntregaEstudos;
                    }
                    var dataPrazo = addDate(dataEntregaEstudos, prazo);

                    $scope.isOnSchedule = dataInicioOperacao > dataPrazo;

                    // Se estiver fora do prazo adiciona as informações no campo
                    var diff = diffDays(dataInicioOperacao, addDate(dataPrazo, 1));
                    diff = diff === 1 ? diff + " dia" : diff + " dias";
                    $scope.meioAmbiente.dataEntradaForaPrazo = diff;
                }
            });
        }

        /*
         *   Retorna a data adicionada dos dias
         */
        var addDate = function(date, days) {
            date = date instanceof Date ? date : new Date(date);
            return new Date(date.setDate(date.getDate() + days));
        }

        /*
         *   Retorna a diferença entre as datas
         */
        var diffDays = function(firstDate, secondDate) {
            var oneDay = 24 * 60 * 60 * 1000;
            return parseInt(Math.abs(Math.round((firstDate.getTime() - secondDate.getTime()) / (oneDay))));
        }

        $scope.saveEnvironment = function() {
            var tiposSelecionados = _.map($scope.selectedetapasMeioAmbiente,function(et){return et.id;});
            var tiposExistentes = _.map($scope.meioAmbiente.etapasMeioAmbiente,function(em){return em.tipoMeioAmbiente.id;});
            // Classe empreendimento obrigatório se informar data entrega estudos
            if ($scope.meioAmbiente.dataEntregaEstudos && !$scope.meioAmbiente.classeEmpreendimento) {
                growl.error($filter('translate')("message.error.MSG71_INFORMAR_CLASSE"));
            } else {
                // Remove o campo que exibe os dias fora do prazo
                delete $scope.meioAmbiente.dataEntradaForaPrazo;
                // var tiposNovos = _.map(_.difference(tiposSelecionados,tiposExistentes),
                //     function(tpe){return {tipoMeioAmbiente:TIPO_MEIO_AMBIENTE.getByID(tpe)};});
                // $scope.meioAmbiente.etapasMeioAmbiente.concat(tiposNovos);
                // Variável transiente utilizada para mandar email caso esteja fora do prazo
                $scope.meioAmbiente.dentroDoPrazo = $scope.isOnSchedule;
                $scope.meioAmbiente.selectedetapasMeioAmbiente = 

                Restangular.all('projeto').one($stateParams.id).post("meioAmbiente", $scope.meioAmbiente).then(function() {
                    growl.success("Registro gravado com sucesso!");
                    $scope.changeScreen();
                });
            }
        }

        $scope.changeScreen = function() {
            // Utilizado para permitir que o usuário mude de página
            // sem a mensagem de perca de dados
            $scope.formEnvironment.$setPristine();
            // Após salvar vai para a aba de insumos e produtos
            $state.go('base.project.ice.insumosProdutosTpl', {
                'screen': $scope.screenService.getScreenName(),
                'id': $stateParams.id
            });
        }

    }).constant('CHANGE_MEIOAMBIENTE', {
        REALIZADAVISTORIA: {
            posicao: 0,
            descricao: "Realizada Vistoria"
        },
        NECESSIDADEEIARIMA: {
            posicao: 1,
            descricao: "Necessidade EIARIMA"
        },
        DATAENTREGAINFORMACAO: {
            posicao: 2,
            descricao: "Data Entrega Informacao"
        },
        PEDIDOINFORMACOESCOMPLEMENTAR:{
            posicao: 3,
            descricao: "Pedido Informacoes Complementar"
        },
        OBSERVACAO: {
            posicao: 4,
            descricao: "Observacao"
        },
        LICENCIAMENTOINICIADO: {
            posicao: 5,
            descricao: "Licenciamento Iniciado"
        },
        ETAPASMEIOAMBIENTE: {
            posicao: 6,
            descricao: "Etapas Meio Ambiente"
        },
        ANEXOS: {
            posicao: 7,
            descricao: "Anexos"
        },
        CLASSEEMPREENDIMENTO: {
            posicao: 8,
            descricao: "Classe Empreendimento"
        },
        DATAPEDIDOINFORMACAO: {
            posicao: 9,
            descricao: "Data Pedido Informacao"
        },
        DATAREALIZADAVISTORIA: {
            posicao: 10,
            descricao: "Data Realizada Vistoria"
        },
        DATAENTREGAESTUDOS: {
            posicao: 11,
            descricao: "Data Entrega Estudos"
        }
    });