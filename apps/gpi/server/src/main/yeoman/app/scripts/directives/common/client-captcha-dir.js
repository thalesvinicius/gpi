angular.module('gpifrontApp')
    .directive('clientCaptcha', function($rootScope, configuration, $compile) {
        return {
            restrict: 'E',
            template: '<div class=\"realperson-challenge\">'
                + '<div class=\"realperson-text\" id=\"textContainer\"></div>'
                + '<div class=\"realperson-regen\"><button type="button" class="btn btn-xs panel" ng-click=\"generateNewText()\">{{options.regenerate}}</button></div>'
                + '</div>'
                + '<div class=\"form-group\" ng-class=\"{\'has-error\': checkValidation(loginForm.userChallengeText)}\">'
                + '<input ng-model=\"userText\" name=\"userChallengeText\" class=\"form-control input-lg\" placeholder=\"Insira os caracteres acima\" maxlength=\"{{options.length}}\" required>'
                + '<div ng-if=\"checkValidation(loginForm.userChallengeText)\" class=\"help-block\">{{\'common.field.required\' | translate}}</div>'
                + '</div>',
            scope: {
                challengeValid: "=",
                checkValidation: "=",
                loginForm: "=",
                clearFunctions: "="
            },

            link: function(scope, iElement, attrs) {
                var DOTS = [
                    ['   *   ', '  * *  ', '  * *  ', ' *   * ', ' ***** ', '*     *', '*     *'],
                    ['****** ', '*     *', '*     *', '****** ', '*     *', '*     *', '****** '],
                    [' ***** ', '*     *', '*      ', '*      ', '*      ', '*     *', ' ***** '],
                    ['****** ', '*     *', '*     *', '*     *', '*     *', '*     *', '****** '],
                    ['*******', '*      ', '*      ', '****   ', '*      ', '*      ', '*******'],
                    ['*******', '*      ', '*      ', '****   ', '*      ', '*      ', '*      '],
                    [' ***** ', '*     *', '*      ', '*      ', '*   ***', '*     *', ' ***** '],
                    ['*     *', '*     *', '*     *', '*******', '*     *', '*     *', '*     *'],
                    ['*******', '   *   ', '   *   ', '   *   ', '   *   ', '   *   ', '*******'],
                    ['      *', '      *', '      *', '      *', '      *', '*     *', ' ***** '],
                    ['*     *', '*   ** ', '* **   ', '**     ', '* **   ', '*   ** ', '*     *'],
                    ['*      ', '*      ', '*      ', '*      ', '*      ', '*      ', '*******'],
                    ['*     *', '**   **', '* * * *', '*  *  *', '*     *', '*     *', '*     *'],
                    ['*     *', '**    *', '* *   *', '*  *  *', '*   * *', '*    **', '*     *'],
                    [' ***** ', '*     *', '*     *', '*     *', '*     *', '*     *', ' ***** '],
                    ['****** ', '*     *', '*     *', '****** ', '*      ', '*      ', '*      '],
                    [' ***** ', '*     *', '*     *', '*     *', '*   * *', '*    * ', ' **** *'],
                    ['****** ', '*     *', '*     *', '****** ', '*   *  ', '*    * ', '*     *'],
                    [' ***** ', '*     *', '*      ', ' ***** ', '      *', '*     *', ' ***** '],
                    ['*******', '   *   ', '   *   ', '   *   ', '   *   ', '   *   ', '   *   '],
                    ['*     *', '*     *', '*     *', '*     *', '*     *', '*     *', ' ***** '],
                    ['*     *', '*     *', ' *   * ', ' *   * ', '  * *  ', '  * *  ', '   *   '],
                    ['*     *', '*     *', '*     *', '*  *  *', '* * * *', '**   **', '*     *'],
                    ['*     *', ' *   * ', '  * *  ', '   *   ', '  * *  ', ' *   * ', '*     *'],
                    ['*     *', ' *   * ', '  * *  ', '   *   ', '   *   ', '   *   ', '   *   '],
                    ['*******', '     * ', '    *  ', '   *   ', '  *    ', ' *     ', '*******'],
                    ['  ***  ', ' *   * ', '*   * *', '*  *  *', '* *   *', ' *   * ', '  ***  '],
                    ['   *   ', '  **   ', ' * *   ', '   *   ', '   *   ', '   *   ', '*******'],
                    [' ***** ', '*     *', '      *', '     * ', '   **  ', ' **    ', '*******'],
                    [' ***** ', '*     *', '      *', '    ** ', '      *', '*     *', ' ***** '],
                    ['    *  ', '   **  ', '  * *  ', ' *  *  ', '*******', '    *  ', '    *  '],
                    ['*******', '*      ', '****** ', '      *', '      *', '*     *', ' ***** '],
                    ['  **** ', ' *     ', '*      ', '****** ', '*     *', '*     *', ' ***** '],
                    ['*******', '     * ', '    *  ', '   *   ', '  *    ', ' *     ', '*      '],
                    [' ***** ', '*     *', '*     *', ' ***** ', '*     *', '*     *', ' ***** '],
                    [' ***** ', '*     *', '*     *', ' ******', '      *', '     * ', ' ****  ']
                ];

                scope.options = {
                    length: 6,
                    regenerate: 'Atualizar',
                    dot: '*',
                    dots: DOTS,
                    chars: 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
                }

                scope.clearFunctions.captchaScreen = function() {
                    scope.userText = '';
                    scope.generateNewText();
                }

                scope.generateNewText = function() {
                    if(scope.userText && scope.userText !== '')
                        scope.userText = '';
                    scope.text = '';
                    for (var i = 0; i < scope.options.length; i++) {
                        scope.text += scope.options.chars.charAt(Math.floor(Math.random() * scope.options.chars.length));
                    }

                    var challengeText = '';
                    for (var i = 0; i < scope.options.dots[0].length; i++) {
                        for (var j = 0; j < scope.text.length; j++) {
                            challengeText += scope.options.dots[scope.options.chars.indexOf(scope.text.charAt(j))][i].
                            replace(/ /g, '&nbsp;').replace(/\*/g, scope.options.dot) +
                                '&nbsp;&nbsp;';
                        }
                        challengeText += '<br>';
                    }
                    iElement.find('#textContainer').html($compile('<div>' + challengeText + '</div>')(scope));
                };
                scope.generateNewText();

                scope.$watch('userText', function() {
                    scope.challengeValid.isValid = (scope.userText === scope.text);
                })
            }
        }
    });
