'use strict'

angular
	.module('gpifrontApp')
	.controller('AttachedFilesAreaGridCtrl', function($scope, GridButton, dialogs, $filter, growl){

        $scope.gridScope = {};

        // initialize its data-model attribute
        $scope.anexosAreaAbrangencia = $scope.$parent.anexosAreaAbrangencia;

        // initialize its selected-items attribute
        $scope.anexosLocalizacaoSelectedItems = [];
        
        //Define its column-defs attribute
        $scope.anexosLocalizacaoColumnDefs = [{
            field: 'tipoLocalizacao',
            enableCellEdit: true,
            width: "**",
            translationKey: 'common.type',
            cellTemplate: '<select ng-model=\'row.entity.tipoLocalizacao\' ng-class="\'colt\' + col.index">' +
                        '<option value="1">Área de Abrangência</option>' +
                        '<option value="2">Localização da Planta Industrial</option>' +
                        '</select>'
        }, {
            field: 'nome',
            enableCellEdit: false,
            width: "**",
            translationKey: 'common.file.name'
        }, {
            field: 'dataAnexo',
            enableCellEdit: false,
            width: "**",
            translationKey: 'common.date',
            cellFilter: 'dateTransformFilter'
        }];

        //define its buttons-config attribute on a persistence screen
        $scope.anexosLocalizacaoButtonsConfig = {    
            newButton: new GridButton(true, 'common.attach.file', false, null, null, ''),
            deleteButton: new GridButton(true, 'common.delete', false, null, null, '')
        }

        //define its sort-info attribute if you desire a default-sort
        $scope.sortInfo = {
            fields: ['nome'],
            directions: ['asc']
        };

        $scope.tiposLocalizacao = [{
            id: 1, descricao: "Área de Abrangência"
        }, {
            id: 2, descricao: "Localização da Planta Industrial"
        }];

        function translate(translationKey) {
            var text = "";
            if (typeof translationKey !== 'undefined' && translationKey) {
                text = $filter('translate')(translationKey);
            }
            return text;
        };

       $scope.preDeleteFile = function() {
            var confirmDialog = dialogs.confirm(translate('DIALOGS_CONFIRMATION'), translate('message.confirm.MSG009_CONFIRMAR_EXCLUSAO'));
            confirmDialog.result.then(function(btn) {
                // Caso confirme executa a exclusão
                $scope.deleteFile();
            }, function(btn) {
                $scope.gridScope.checkSelectAllCheckBox(true, true);
                $scope.gridScope.checkSelectAllCheckBox(false, true);
                // Caso não confirme deseleciona os registros selecionados
                $scope.anexosLocalizacaoSelectedItems.splice(0, $scope.anexosLocalizacaoSelectedItems.length);
            });

        }

        $scope.deleteFile = function() {
            if ($scope.anexosAreaAbrangencia.length > 0 && $scope.anexosLocalizacaoSelectedItems.length > 0) {
                $scope.anexosLocalizacaoSelectedItems.forEach(function(selected) {
                    var model = $scope.anexosAreaAbrangencia;
                    var indexOnMainList = model.indexOf(selected);
                    // deleting each unit from main list
                    $scope.anexosAreaAbrangencia.splice(indexOnMainList, 1);
                    for (var i = 0; i < $scope.$flow.files.length; i++) {
                        var file = $scope.$flow.files[i];
                        file.cancel();
                    };
                });
                // deleting all 'selectedItems' starting at index 0
                $scope.anexosLocalizacaoSelectedItems.splice(0, $scope.anexosLocalizacaoSelectedItems.length);
                
                growl.info($filter('translate')('message.error.MSG38_SECAO_ATUALIZADA', {value: "Localização"}));                        
            }
        }
 
         $scope.disableFn = function () {
            return $scope.anexosLocalizacaoSelectedItems.length < 1;
        }              

	});