'use strict'

angular
	.module('gpifrontApp')
	.controller('PreviousBillingGridCtrl', function($scope, Restangular, GridButton, growl, $filter){

        $scope.faturamentoAnterior = $scope.$parent.faturamentoAnterior;            
        $scope.faturamentoAnteriorSelectedItems = $scope.$parent.faturamentoAnteriorSelectedItems;

		$scope.faturamentoAnteriorColumnDefs = [{
            field: "ano",
            enableCellEdit: true,
            width: "**",            
            translationKey: "common.achievement.year",
            editableCellTemplate: '<input type="numeric" ui-mask="9999" maxlength="4" ng-class="\'colt\' + col.index"  ng-input="COL_FIELD" ng-model="COL_FIELD"/>'                        
        }, {
            field: "valorGrupo",
            enableCellEdit: true,
            width: "**",          
            cellFilter: "currency:''",            
            editableCellTemplate: '<input type="text" format="currency" maxlength="21" ng-class="\'colt\' + col.index"  ng-input="COL_FIELD" ng-model="COL_FIELD"/>',                                    
            translationKey: "financial.group.billing"         
        }, {
            field: "valorMinasGerais",
            enableCellEdit: true,
            cellFilter: "currency:''",
            editableCellTemplate: '<input type="text" format="currency" maxlength="21" ng-class="\'colt\' + col.index"  ng-input="COL_FIELD" ng-model="COL_FIELD"/>',                                                
            width: "**",            
            translationKey: "financial.minas.billing"         
        }];

        $scope.faturamentoAnteriorButtonsConfig = {            
            newButton: new GridButton(true, 'common.new', false, null, null, ''),
            deleteButton: new GridButton(true, 'common.delete', false, null, deleteFn, '')
        }


        //define its sort-info attribute if you desire a default-sort
        $scope.faturamentoAnteriorSortInfo = {
            fields: ['ano'],
            directions: ['asc']
        };

        function deleteFn() {
            growl.info($filter('translate')('message.error.MSG38_SECAO_ATUALIZADA', {
                value: "Faturamento Anterior"
            }));                        
        }              		

        $scope.$parent.$watch('faturamentoAnterior', function(newVal, oldVal){
         	if ($scope.$parent.faturamentoAnterior != null) {
                setTimeout(function() {
         		    $scope.faturamentoAnterior = newVal;	
                }, true);
         	}                
        }, true);

	});


