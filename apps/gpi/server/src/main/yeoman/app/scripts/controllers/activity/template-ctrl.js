'use strict';

angular.module('gpifrontApp')
    .controller('ActivityTemplateCtrl', function($scope, ScreenService, Restangular, TIPO_TEMPLATE, STATUS_TEMPLATE, $stateParams, growl, $state, $filter) {

        var baseTemplates = Restangular.all('template');
        var formSubmitted = false;

        $scope.screenService = ScreenService;
    	$scope.tiposTemplate = TIPO_TEMPLATE;
    	$scope.statusTemplate = STATUS_TEMPLATE
		$scope.atividadesTemplate = [];

        $scope.type = {
            INCLUIR: {
                titlePath: 'activity.add',
                disabled: false,
                icon: "fa-plus"                
            },
            EDITAR: {
                titlePath: 'activity.edit',
                disabled: false,
                icon: "fa-edit"                
            },
            VISUALIZAR: {
                titlePath: 'activity.visualize',
                disabled: true,
                icon: "fa-eye"                
            }
        }
        $scope.screenType = $scope.type[$stateParams.screen.toUpperCase()];

        $scope.template = {};
        if ($scope.screenType != null) {
            $scope.disabled = $scope.screenType.disabled;
            if ($stateParams.screenType != $scope.type.INCLUIR && $stateParams.id) {
                Restangular.one('template', $stateParams.id).get().then(function(template) {
                    $scope.template = template;
                    $scope.atividadesTemplate = template.atividades;
                });
            } else if ($scope.screenType == $scope.type.INCLUIR) {
                $scope.template.status = "true";
            }
        }

        $scope.checkValidation = function(formField) {
            return formField.$invalid && (formField.$dirty || formSubmitted);
        }

        $scope.save = function(isFormValid) {
            var error = isFormValid ? false : true;

            if (error) {
                formSubmitted = true;
                growl.error($filter('translate')('message.error.MSGXX_DADOS_INVALIDOS'));
                return;
            }

            if ($scope.screenType != null && $scope.screenType == $scope.type.INCLUIR) {
                $scope.template.atividades = $scope.atividadesTemplate;  
            }            

            if ($scope.template.atividades.length > 0) {
                var erroAtividadeLocalRequired = false;

                for (var i = $scope.template.atividades.length - 1; i >= 0; i--) {
                    if ($scope.template.atividades[i].atividadeLocal == undefined) {
                        erroAtividadeLocalRequired = true;
                        break;
                    } else if ($scope.template.atividades[i].atividadeLocal.id == undefined) {
                        erroAtividadeLocalRequired = true;
                        break;
                    } else if ($scope.template.atividades[i].atividadeLocal.local == undefined) {
                        erroAtividadeLocalRequired = true;                        
                        break;
                    }                    
                };

                if (erroAtividadeLocalRequired) {
                    growl.error("Os campos local e atividade devem ser informados!");
                    return;                    
                }

            }

            baseTemplates.one("ativosByTipo").one($scope.template.tipo).getList().then(function(ativosByTipo){                

                if (!$scope.template.status || ativosByTipo.length == 0 || $scope.template.id == ativosByTipo[0].id) {

                    baseTemplates.post($scope.template).then(function() {
                        growl.success("Registro gravado com sucesso!");
                        $state.go("base.activitySearch");
                    },
                    function() {
                        growl.error("Ocorreu um erro ao tentar gravar o registro!");
                    });

                } else {
                    growl.error("Já existe um Template ativo para o Tipo de Template selecionado.");
                }


            })

        }
    	
    });