'use strict';

angular.module('gpifrontApp')
	.controller('IceCtrl', function($scope, ScreenService, Restangular, $stateParams, growl, $location, SITUACAO_PROJETO, ESTAGIO_PROJETO, AuthService, AUTH_ROLES, SITUACAO_EMPRESA, UserSession, CHANGE_INSUMOS) {

        $scope.screenService = ScreenService;

        var baseProjetos = Restangular.all('projeto');

        $scope.localizacao = [];
        $scope.changeLocalizacao = false;
        $scope.financeiro = [];
        $scope.changeFinanceiro = false;
        $scope.pleito = [];
        $scope.changePleito = false;
        $scope.emprego = [];
        $scope.changeEmprego = false;
        $scope.cronograma = [];
        $scope.changeCronograma = false;
        $scope.infraestrutura = [];
        $scope.changeInfraestrutura = false;
        $scope.meioAmbiente = [];
        $scope.changeMeioAmbiente = false;
        $scope.insumoProduto = [];
        $scope.changeInsumoProduto = false;

        $scope.type = {
            INCLUIR: {
                disabled: false
            },
            EDITAR: {
                disabled: false
            },
            VISUALIZAR: {
                disabled: true
            }
        };

        $scope.externalUser = function() {
            return AuthService.isUserAuthorized([AUTH_ROLES.externalResponsible, AUTH_ROLES.externalMeling, AUTH_ROLES.externalOther]);
        }

        $scope.projetoTO = {}
        var projetoId = $location.search() ? $location.search().id : undefined;

        if (projetoId != undefined) {
            Restangular.one('projeto', projetoId).one("projetoTO").get().then(function(result) {
                setTimeout(function(){
                    $scope.projetoTO = result;
                    $scope.situacaoProjeto = SITUACAO_PROJETO[$scope.projetoTO.situacao];
                    $scope.estagioProjeto = ESTAGIO_PROJETO[$scope.projetoTO.estagio];

                    if (!$scope.externalUser() && $scope.projetoTO.situacao === SITUACAO_PROJETO.PENDENTE_ACEITE.id) {
                        baseProjetos.one(projetoId).getList("localizacao/findChange").then(function(result) {
                            $scope.localizacao = (result[0] === undefined ? 0 : result[0]);
                            for (var i = 0; i < $scope.localizacao.length; i++) {
                                if ($scope.localizacao[i] === true) {
                                    $scope.changeLocalizacao = true;
                                }
                            };
                        }, function() {
                            growl.error("Ocorreu um erro ao tentar buscar as alterações de Localização!");
                        });

                        baseProjetos.one(projetoId).getList("financeiro/findChange").then(function(result) {
                            $scope.financeiro = (result[0] === undefined ? 0 : result[0]);
                            for (var i = 0; i < $scope.financeiro.length; i++) {
                                if ($scope.financeiro[i] === true || $scope.financeiro[i] === 1) {
                                    $scope.changeFinanceiro = true;
                                }
                            };                            
                        }, function() {
                            growl.error("Ocorreu um erro ao tentar buscar as alterações de Financeiro!");
                        });

                        Restangular.one("projeto", projetoId).getList("emprego/findChange").then(function(result) {
                            if (result[0] != undefined) {
                                for (var i = 0; i < result.length; i++) {
                                    $scope.emprego[result[i][0]] = (result[i][1] || result[i][2] || result[i][3] || result[i][4]);
                                    if ($scope.emprego[result[i][0]] === true) {
                                        $scope.changeEmprego = true;
                                    }
                                };                                
                            }
                        }, function() {
                            growl.error("Ocorreu um erro ao tentar buscar as alterações de Empregos!");
                        });

                        baseProjetos.one(projetoId).getList("cronograma/findChange").then(function(result) {
                            $scope.cronograma = (result[0] === undefined ? 0 : result[0]);
                            for (var i = 0; i < $scope.cronograma.length; i++) {
                                if ($scope.cronograma[i] === true) {
                                    $scope.changeCronograma = true;
                                }
                            };
                        }, function() {
                            growl.error("Ocorreu um erro ao tentar buscar as alterações de Cronograma!");
                        });     

                        baseProjetos.one(projetoId).getList("infraestrutura/findChange").then(function(result) {
                            $scope.infraestrutura = (result[0] === undefined ? 0 : result[0]);
                            for (var i = 0; i < $scope.infraestrutura.length; i++) {
                                if ($scope.infraestrutura[i] === true) {
                                    $scope.changeInfraestrutura = true;
                                }
                            };
                        }, function() {
                            growl.error("Ocorreu um erro ao tentar buscar as alterações de  Infraestrutura!");
                        });

                        baseProjetos.one(projetoId).getList("meioAmbiente/findChange").then(function(result) {
                            $scope.meioAmbiente = (result[0] === undefined ? 0 : result[0]);
                            for (var i = 0; i < $scope.meioAmbiente.length; i++) {
                                if ($scope.meioAmbiente[i] === true) {
                                    $scope.changeMeioAmbiente = true;
                                }
                            };
                        }, function() {
                            growl.error("Ocorreu um erro ao tentar buscar as alterações de Meio Ambiente!");
                        });

                        baseProjetos.one(projetoId).getList("insumoProduto/findChange/").then(function(result) {
                            $scope.insumoProduto = (result[0] === undefined ? 0 : result[0]);

                            for (var j = 0; j < result.length; j++) {

                                if(result[j][CHANGE_INSUMOS.PRODUTOSADQECOMPORMINAS.posicao] === true || result[j][CHANGE_INSUMOS.PRODUTOSADQECOMPORMINAS.posicao] === 1) {
                                    $scope.insumoProduto[CHANGE_INSUMOS.PRODUTOSADQECOMPORMINAS.posicao] = true;    
                                }                                                                
                                if(result[j][CHANGE_INSUMOS.PRODUTOSADQOUTROSESTADOSPARACOM.posicao] === true || result[j][CHANGE_INSUMOS.PRODUTOSADQOUTROSESTADOSPARACOM.posicao] === 1) {
                                    $scope.insumoProduto[CHANGE_INSUMOS.PRODUTOSADQOUTROSESTADOSPARACOM.posicao] = true;    
                                }


                                if(result[j][CHANGE_INSUMOS.PRODUTOSFABECOMPORMINAS.posicao] === true || result[j][CHANGE_INSUMOS.PRODUTOSFABECOMPORMINAS.posicao] === 1) {
                                    $scope.insumoProduto[CHANGE_INSUMOS.PRODUTOSFABECOMPORMINAS.posicao] = true;    
                                }
                                if(result[j][CHANGE_INSUMOS.PRODUTOSIMPPARACOM.posicao] === true || result[j][CHANGE_INSUMOS.PRODUTOSIMPPARACOM.posicao] === 1) {
                                    $scope.insumoProduto[CHANGE_INSUMOS.PRODUTOSIMPPARACOM.posicao] = true;    
                                }
                            }
                            for (var i = 0; i < $scope.insumoProduto.length; i++) {
                                if ($scope.insumoProduto[i] === true  || $scope.insumoProduto[i] === 1) {
                                    $scope.changeInsumoProduto = true;
                                }
                            };                            
                        }, function() {
                            growl.error("Ocorreu um erro ao tentar buscar o registro!");
                        });                        

                        baseProjetos.one(projetoId).getList("pleito/findChange").then(function(result) {
                            $scope.pleito = (result[0] === undefined ? 0 : result[0]);
                            for (var i = 0; i < $scope.pleito.length; i++) {
                                if ($scope.pleito[i] === true) {
                                    $scope.changePleito = true;
                                }
                            };
                        }, function() {
                            growl.error("Ocorreu um erro ao tentar buscar as alterações de Pleito!");
                        });
                    }
                })
            });
        }

        /*
         *   Verifica se o usuário logado tem permissão de editar projeto
         */
        var usuarioPossuiPermissao = function() {
            return AuthService.isUserAuthorized([AUTH_ROLES.infraAnalist, AUTH_ROLES.portfolioAnalist, AUTH_ROLES.promotionAnalist, AUTH_ROLES.manager, AUTH_ROLES.externalResponsible]);
        }

        /*
         *   Verifica se o usuário logado é responsável pelo projeto selecionado
         */
        var isResponsavelProjeto = function() {
            return $scope.projetoTO.responsavelInvestimento.id === UserSession.getUser().id;
        }

        /*
         *   Verifica se o usuário logado é o ínvestidor (usuario externo responsável da empresa)
         */
        $scope.isExternalUserResponsible = function() {
            return $scope.isExternalUser && $scope.projetoTO.usuarioExternoResponsavelId === UserSession.getUser().id;
        }

        /*
         *   Verifica se o usuário logado é gerente
         */
        var isGerente = function() {
            return AuthService.isUserAuthorized([AUTH_ROLES.manager]);
        }

        var podeEditarQualquerProjeto = function() {
            return AuthService.isUserAuthorized([AUTH_ROLES.infraAnalist, AUTH_ROLES.portfolioAnalist]);
        }

        /*
         *   Verifica se o usuário logado é responsável pelo departamento
         */
        var isResponsavelDepartamento = function() {
            return $scope.projetoTO.departamentoUsuarioInternoResponsavelId === UserSession.getUser().departamento.id;
        }

        $scope.canEditProject = function() {
            var canEdit = true;
            if (ScreenService.isEditScreen()) {
                canEdit = ($scope.projetoTO.situacaoEmpresa === SITUACAO_EMPRESA["ativa"].id // Só pode editar um projeto em que situação da Empresa ou da Empresa vinculada à Unidade for "Ativo"
                    && (($scope.isExternalUserResponsible() && $scope.projetoTO.estagioDescricao === ESTAGIO_PROJETO["INICIO_PROJETO"].descricao) // Usuario externo só pode editar projeto em estagio inicio de projeto
                        || (isGerente() && isResponsavelDepartamento()) // Caso o usuário logado for um gerente ele poderá editar um projeto se ele for o responsável pelo departamento
                        || (usuarioPossuiPermissao() && isResponsavelProjeto()) || podeEditarQualquerProjeto())); // Se o usuário for responsável pelo projeto ou possuir permissão para editar qualquer projeto
            }
            return canEdit;
        }

	});