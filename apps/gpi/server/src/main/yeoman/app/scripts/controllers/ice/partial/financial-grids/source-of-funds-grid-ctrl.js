'use strict'

angular
    .module('gpifrontApp')
    .controller('SourceOfFundsGridCtrl', function($scope, Restangular, GridButton, growl, $filter, BRASIL) {

        $scope.origemRecurso = $scope.$parent.origemRecurso;
        $scope.origemRecursoSelectedItems = $scope.$parent.origemRecursoSelectedItems;

        var obj = new Object();
        obj.ufs = [];
        obj.paises = [];

        var firstExecution = true;

        Restangular.all("domain/pais").getList().then(function(allCountries) {
            obj.paises = allCountries;
        })

        Restangular.all("domain/uf").getList().then(function(ufs) {
            obj.ufs = ufs;
        });

        obj.getBooleanValue = function(intVal) {
            if (typeof parseInt(intVal) === 'number') {
                return new Boolean(parseInt(intVal));
            } else {
                return null;
            }
        }

        obj.getIntValue = function(booleanVal) {
            return typeof booleanVal === 'boolean' ? (booleanVal ? 1 : 0) : null;
        }

        obj.firstExecution = function(entity) {
            entity.origemNacionalId = obj.getIntValue(entity.origemNacional);
            obj.loadData(entity);
        }

        obj.loadData = function(entity) {
            entity.obj = {
                paises: [],
                ufs: []
            };

            if (entity.origemNacionalId != null && typeof entity.origemNacionalId != 'undefined') {
                entity.origemNacional = obj.getBooleanValue(entity.origemNacionalId);

                for (var i = obj.paises.length - 1; i >= 0; i--) {
                    if ((entity.origemNacional.toString() === false.toString() && obj.paises[i].id.toString() !== BRASIL.id.toString()) || (entity.origemNacional.toString() === true.toString() && obj.paises[i].id.toString() === BRASIL.id.toString())) {
                        entity.obj.paises.push(obj.paises[i]);
                    }
                };

                if (eval(entity.origemNacional.toString())) {
                    entity.obj.ufs = obj.ufs;
                    entity.pais = {
                        id: parseInt(BRASIL.id),
                        nome: BRASIL.nome
                    };
                } else {
                    if (!firstExecution) {
                        entity.obj.ufs.splice(0, entity.obj.ufs.length);
                        entity.uf = null;
                        entity.pais = null;
                    }
                }
            }
        }

        obj.origens = [{
            id: 1,
            bolValue: true,
            nome: "Nacional"
        }, {
            id: 0,
            bolValue: false,
            nome: "Internacional"
        }];

        $scope.gridScope = {
            obj: obj,
            brasil: BRASIL
        };

        /*   $scope.columnDefs = [{
               field: 'localAtividade',
               enableCellEdit: false,
               width: "*",
               translationKey: 'common.local',
               cellTemplate: "<div ng-show=\"!isEditing(row.entity) || row.entity.obrigatorio\" class=\"ngCellText\" ng-class=\"col.colIndex()\"><span ng-cell-text title=\"{{row.entity.localAtividade.descricao}}\">{{row.entity.localAtividade.descricao}}</span></div>" +
                   '<select ng-show=\"isEditing(row.entity) && !row.entity.obrigatorio\" ng-change=\"onChangeLocal(row.entity.localAtividade, row.entity.atividadeLocal)\" ng-class="\'colt\' + col.index\" ng-input=\"COL_FIELD\" ng-model=\"row.entity.localAtividade\">' +
                   '<option title=\"{{local.descricao}}\" ng-value=\"local.id\" ng-repeat=\"local in locais\" ng-selected=\"row.entity.localAtividade.id === local.id\">{{local.descricao}}</option>' +
                   '</select>'

           }, {
               field: 'atividadeLocal',
               enableCellEdit: false,
               width: "***",
               translationKey: 'common.activity',
               cellTemplate: "<div ng-show=\"!isEditing(row.entity) || row.entity.obrigatorio\" class=\"ngCellText\" ng-class=\"col.colIndex()\"><span ng-cell-text title=\"{{row.entity.atividadeLocal.descricao}}\">{{row.entity.atividadeLocal.descricao}}</span></div>" +
                   '<select ng-show=\"isEditing(row.entity) && !row.entity.obrigatorio\" ng-class="\'colt\' + col.index\" ng-input=\"COL_FIELD\" ng-model=\"row.entity.atividadeLocal\">' +
                   '<option title=\"{{atividade.descricao}}\" ng-value=\"atividade.id\" ng-repeat=\"atividade in getAtividades(row.entity.localAtividade)\" ng-selected=\"row.entity.atividadeLocal.id === atividade.id\">{{atividade.descricao}}</option>' +
                   '</select>'
           }, {
               field: 'dataConclusao',
               enableCellEdit: false,
               width: '90',
               translationKey: 'common.done',
               cellTemplate: "<div ng-show=\"!isEditing(row.entity)\" class=\"ngCellText\" ng-class=\"col.colIndex()\"><span ng-cell-text>{{row.entity.dataConclusao | dateTransformFilter}}</span></div>" +
                   "<datepicker-custom ng-show=\"isEditing(row.entity) && (row.entity.ativo === true)\" id=\"datepicker\" date=\"COL_FIELD\" configs=\"configsDatepicker\"></datepicker-custom>"
           }];*/

        $scope.origemRecursoColumnDefs = [{
            field: "origemNacional",
            enableCellEdit: false,
            width: "**",
            translationKey: "common.origin",
            cellTemplate: '<select ng-change=\'obj.loadData(row.entity)\' ng-class="\'colt\' + col.index\" ng-input=\"COL_FIELD\" ng-model=\"row.entity.origemNacionalId\" ng-options="o.id as o.nome for o in obj.origens"/>'
        }, {
            field: "uf",
            enableCellEdit: false,
            width: "**",
            translationKey: "address.state",
            cellTemplate: '<select ng-show="row.entity.origemNacional" ng-class="\'colt\' + col.index" ng-input="COL_FIELD" ng-model="row.entity.uf.id" ng-options="uf.id as uf.nome for uf in row.entity.obj.ufs" />'

        }, {
            field: "pais",
            enableCellEdit: true,
            width: "**",
            translationKey: "address.country",
            cellTemplate: '<select ng-class="\'colt\' + col.index" ng-input="COL_FIELD" ng-model="row.entity.pais.id"  ng-options="p.id as p.nome for p in row.entity.obj.paises | orderBy:\'nome\'" />'

        }, {
            field: "percentualInvestimento",
            enableCellEdit: true,
            cellFilter: "currency:''",
            width: "**",
            editableCellTemplate: '<input type="text" format="number" maxlength="6" ng-class="\'colt\' + col.index"  ng-input="COL_FIELD" ng-model="COL_FIELD"/>',
            translationKey: "financial.perc.investiment"
        }, {
            field: "valorInvestimento",
            enableCellEdit: false,
            width: "**",
            cellFilter: "currency:''",
            translationKey: "financial.investment",
        }];

        $scope.origemRecursoButtonsConfig = {
            newButton: new GridButton(true, 'common.new', false, null, null, ''),
            deleteButton: new GridButton(true, 'common.delete', false, null, deleteFn, '')
        }

        //define its sort-info attribute if you desire a default-sort
        $scope.origemRecursoSortInfo = {
            fields: ['id'],
            directions: ['desc']
        };

        function newFn() {
            $scope.origemRecurso.unshift({
                origemNacional: true
            });
            return false;
        }

        function deleteFn() {
            growl.info($filter('translate')('message.error.MSG38_SECAO_ATUALIZADA', {
                value: "Origem dos Recursos"
            }));
        }

        $scope.$parent.$watch('origemRecurso', function(newVal, oldVal) {
            if ($scope.$parent.origemRecurso != null) {
                setTimeout(function() {
                    if (newVal != oldVal) {
                        $scope.origemRecurso = newVal;
                        if (firstExecution) {
                            for (var i = 0; i < $scope.origemRecurso.length; i++) {
                                obj.firstExecution($scope.origemRecurso[i]);
                            };

                            firstExecution = false;
                        }
                    }
                }, true);
            }
        }, true);

    });
