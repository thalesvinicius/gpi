'use strict';

angular.module('gpifrontApp')
    .controller('CompanyCtrl', function($scope, $state, ScreenService, growl, AuthService, AUTH_ROLES, UserSession, Restangular, TIPO_USUARIO) {
        $scope.isExternalUser = UserSession.getUser().tipo == TIPO_USUARIO.EXTERNO;

        $scope.screenType = {
            INCLUIR: {
                translationKey: "company.new",
                icon: "fa-plus"
            },
            EDITAR: {
                translationKey: "company.edit",
                icon: "fa-edit"
            },
            VISUALIZAR: {
                translationKey: "company.show",
                icon: "fa-eye"
            }
        };
        
        if(!$state.params.screen || $state.params.screen === "") {
            $state.params.screen = "incluir";
        }
        if($state.params.screen.toUpperCase() === "INCLUIR") {
            $state.params.id = null;
        }

        $scope.screenService = ScreenService;

        $scope.getId = function() {
            return $state.params.id;
        };

        $scope.disableOnShow = function($event) {
            $event.preventDefault();
            $event.stopPropagation();
            return false;
        }

        $scope.notAllowed = function () {
            growl.warning("Não é permitido acessar essa aba em modo de inclusão!");
        }

        $scope.checkUserAuthorization = function() {
            if ($scope.empresa && $scope.getId() && $scope.empresa.id === $scope.getId()) {
                AuthService.verifyUserIsAuthenticatedInsideController($scope.isUserAuthorized); 
            } else if (!$scope.screenService.isNewScreen() && $scope.getId() > 0) {
                Restangular.one("empresa", $scope.getId()).get().then(function(result) {
                    $scope.empresa = result;
                    AuthService.verifyUserIsAuthenticatedInsideController($scope.isUserAuthorized);                
                });
            }            
        }        

        $scope.isExternalUserResponsible = function() {
            return $scope.isExternalUser && $scope.empresa.responsavel !== null && $scope.empresa.responsavel.id === UserSession.getUser().id;
        }

        $scope.isUserAuthorized = function() {
            return $scope.isExternalUserResponsible() || !$scope.isExternalUser;
        }        

    });
