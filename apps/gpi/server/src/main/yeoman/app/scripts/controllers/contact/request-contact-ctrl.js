'use strict';
angular.module('gpifrontApp')
    .controller('RequestContactCtrl', function($translate, $scope, $filter, Restangular, growl, SITUACAO_EMPRESA, MINAS_GERAIS_ID, TIPO_USUARIO, BRASIL, $state, ValidationService, dialogs) {
        // Usado para validar o form
        $scope.formSubmitted = false;
        $scope.isInternational = false;
        // Usado para verificar se o cnpj está válido
        $scope.isCNPJValid = true;

        $scope.isOriginValid = true;

        // RNE 208
        $scope.enderecoObrigatorio = false;

        //  Configurações do datepicker
        $scope.configsDatepicker = {
            disabled: true
        };

        // Altera a classe do icone passado
        $scope.changeLanguage = function(language) {
            $translate.use(language);
        }

        var initialize = function() {
            $scope.origemEmpresa = {};

            $scope.INTERNACIONAL = {
                "id": 0,
                "nome": "INTERNACIONAL"
            };

            $scope.solicitarContato = {
                data: new Date(),
                endereco: {}
            };

            // Por default a empresa será mineira

            $scope.solicitarContato.empresaMineira = true;

            /*  Preenchendo as listas de dominio    */
            Restangular.all('domain/cadeiaProdutiva').getList().then(function(result) {
                $scope.cadeiasProdutivas = result;
            });

            Restangular.all('domain/pais/nome/asc').getList().then(function(result) {
                $scope.paises = result;
            });

            // Lista de UFs menos 'MINAS GERAIS' e adicionando o item 'INTERNACIONAL'
            Restangular.all('domain/uf').getList().then(function(result) {
                result.unshift($scope.INTERNACIONAL);
                for (var i = result.length; i--;) {
                    if (result[i].nome === "MINAS GERAIS") {
                        result.splice(i, 1);
                    }
                }
                $scope.origensEmpresa = result;
            });

            $scope.loadMunicipios(true);
        };

        $scope.loadMunicipios = function(isMineira) {
            var selectedEstado = null
            if (isMineira) {
                selectedEstado = MINAS_GERAIS_ID;
            } else {
                selectedEstado = $scope.origemEmpresa.id;
            }

            if (selectedEstado) {
                $scope.municipios = [];
                setTimeout(function() {
                    Restangular.all('municipio/' + selectedEstado).getList().then(function(result) {
                        $scope.municipios = result;
                    });
                }, true);
            }
        }

        var changeAddressInformation = function() {
            delete $scope.solicitarContato.endereco.logradouro;
            delete $scope.solicitarContato.endereco.numero;
            delete $scope.solicitarContato.endereco.complemento;
            delete $scope.solicitarContato.endereco.bairro;
            delete $scope.solicitarContato.endereco.cep;
            if ($scope.solicitarContato.endereco.municipio) {
                delete $scope.solicitarContato.endereco.municipio.id;
            }

            $scope.municipios = [];
        }

        /*
         *   Método change criado para limpar os campos ao trocar o select origemEmpresa
         */
        $scope.changeCompanyOrign = function() {
            $scope.isInternational = $scope.origemEmpresa.id === $scope.INTERNACIONAL.id;
            
            delete $scope.solicitarContato.endereco.cidade;
            delete $scope.solicitarContato.endereco.pais;
            changeAddressInformation();

            if ($scope.isNotInternacionalAndNotMineira()) {
                $scope.loadMunicipios(false);
            }

            $scope.isOriginValid = $scope.origemEmpresa.id != null;
        }

        $scope.changeEmpresaMineira = function() {
            if ($scope.solicitarContato.empresaMineira) {
                delete $scope.origemEmpresa.id;
            } else {
                changeAddressInformation();
            }
        }

        /*
         *   Método para verificar se a empresa é brasileira porém não é mineira.
         */
        $scope.isNotInternacionalAndNotMineira = function() {
            return $scope.origemEmpresa.id !== undefined && $scope.origemEmpresa.id !== $scope.INTERNACIONAL.id;
        }

        /*
         *   Valida o campo passado
         */
        $scope.checkValidation = function(formField) {
            return formField.$invalid && (formField.$dirty || $scope.formSubmitted);
        }

        /*
         *   Valida o campo passado
         */
        $scope.validationEmail = function(formField) {
            var isInvalid = $scope.checkValidation(formField);
            if (isInvalid) {
                $scope.messageConfirmEmail = $filter('translate')('message.error.MSG74_FORMATO_INVALIDO', {
                    "value": "\'" + $filter('translate')('common.confirmEmail') + "\'"
                });
            } else {
                if (formField.$dirty && $scope.solicitarContato.responsavel && $scope.solicitarContato.responsavel.confirmarEmail !== $scope.solicitarContato.responsavel.email) {
                    $scope.messageConfirmEmail = $filter('translate')('message.error.MSG84_EMAIL_NAO_CONFERE');
                    isInvalid = true;
                }
            }

            return isInvalid;
        }

        /*
         *   Chamado ao sair do campo CNPJ, para validar o numero digitado
         */
        $scope.blurCNPJ = function() {
            if ($scope.solicitarContato.cnpj) {
                $scope.isCNPJValid = ValidationService.validateCnpj($scope.solicitarContato.cnpj);
            } else {
                $scope.isCNPJValid = true;
            }
        }

        /*
         *   Chamado ao trocar a origem da empresa
         */
        $scope.changeOrigem = function() {
            $scope.isInternational = $scope.origemEmpresa.id === $scope.INTERNACIONAL.id;
            // Limpa os campos com exibição vinculada
            delete $scope.solicitarContato.endereco.cidade;
            delete $scope.solicitarContato.endereco.pais;

            // Deleta campos exibidos para empresas nacionais
            if ($scope.isInternational) {
                delete $scope.solicitarContato.endereco.cep;
                delete $scope.solicitarContato.endereco.municipio;
            }
        }

        /*
         *   Chamado ao preencher algum campo da sessão de endereço
         */
        $scope.changeEndereco = function() {
            $scope.enderecoObrigatorio = $scope.solicitarContato.endereco.logradouro || $scope.solicitarContato.endereco.numero || $scope.solicitarContato.endereco.bairro || $scope.solicitarContato.endereco.municipio;
        }

        /*
         *   Fecha a tela e volta para o portal do indi
         */
        $scope.cancel = function() {
            // Somente mostra a mensagem se o usuário tiver alterado algo no form
            if ($scope.formRequestContact !== undefined && $scope.formRequestContact.$dirty) {
                var cancelDialog = dialogs.confirm($filter('translate')('DIALOGS_CONFIRMATION'), $filter('translate')('message.confirm.MSG31_CANCELAR'));
                cancelDialog.result.then(function(btn) {
                    //  Altera o form para pristine para não exibir a MSG33_DADOS_NAO_SALVOS
                    $scope.formRequestContact.$setPristine();
                    window.location = 'http://www.indi.mg.gov.br';
                });
            } else {
                window.location = 'http://www.indi.mg.gov.br';
            }
        }

        /*
         *   Limpa os campos da tela
         */
        $scope.clean = function() {
            for (var key in $scope.solicitarContato) {
                if (key !== "data") {
                    delete $scope.solicitarContato[key];
                }
            }
            $scope.solicitarContato.empresaMineira = true;

            $scope.usuario = {};

            // Usado para não exibir a mensagem de campo obrigatório
            $scope.formRequestContact.$setPristine();
        }

        /*
         *   Tratamentos a serem feitos antes de salvar
         */
        $scope.preSave = function() {
            //definindo os campos empresa internacional e endereço
            if ($scope.origemEmpresa.id === $scope.INTERNACIONAL.id) {
                $scope.solicitarContato.empresaInternacional = true;
                $scope.solicitarContato.endereco.uf = null;
                $scope.solicitarContato.paisOrigem = {id:$scope.solicitarContato.endereco.pais.id};
            } else {
                $scope.solicitarContato.empresaInternacional = false;

                if (!angular.isUndefined($scope.origemEmpresa.id) && $scope.origemEmpresa.id !== null && $scope.origemEmpresa.id !== 0) {
                    $scope.solicitarContato.endereco.uf = {
                        id: $scope.origemEmpresa.id
                    };
                }

                // Caso seja empresa nacional adiciona o id
                $scope.solicitarContato.endereco.pais = {
                    id: BRASIL.id
                };
            }

            //  Se a empresa for mineira
            if ($scope.solicitarContato.empresaMineira === true) {
                $scope.solicitarContato.endereco.uf = {
                    id: MINAS_GERAIS_ID
                };
            } else if(!$scope.solicitarContato.empresaInternacional) {
                $scope.solicitarContato.estadoOrigem = {
                    id: $scope.solicitarContato.endereco.uf.id
                };
            }

            // Situação inicalmente como pendente de validação
            $scope.solicitarContato.situacao = SITUACAO_EMPRESA.pendenteValidacao.id;
        }

        /*
         *   Salva o Usuario Externo e a Empresa
         */
        $scope.save = function() {
            $scope.formSubmitted = true;

            if($scope.solicitarContato.empresaMineira){
                $scope.isOriginValid = true;
            }else{
                $scope.isOriginValid = $scope.origemEmpresa.id != null;
            }

            if ($scope.formRequestContact.$valid
                    && $scope.isCNPJValid
                    && $scope.solicitarContato.responsavel.confirmarEmail === $scope.solicitarContato.responsavel.email
                    && $scope.isOriginValid) {
                $scope.preSave();
                $scope.solicitarContato.responsavel.tipo = TIPO_USUARIO.EXTERNO;

                Restangular.all('empresa/savePrimeiroContato').post($scope.solicitarContato).then(function(id) {
                    // Utilizado para permitir que o usuário mude de página
                    // sem a mensagem de perca de dados
                    $scope.formRequestContact.$setPristine();

                    var notifyDialog = dialogs.notify($filter('translate')('DIALOGS_NOTIFICATION'), $filter('translate')('message.success.MSG81_SOLICITACAO_CONCLUIDA'));
                    notifyDialog.result.then(function(btn) {
                        window.location = 'http://www.indi.mg.gov.br';
                    });

                }, function(response) {
                    if (response.data.cause.errorMessage !== undefined) {
                        growl.error($filter('translate')('message.error.' + response.data.cause.errorMessage));
                    } else {
                        growl.error($filter('translate')("message.error.MSGXX_DADOS_INVALIDOS"));
                    }                    
                });
            } else {
                growl.error($filter('translate')("message.error.MSGXX_DADOS_INVALIDOS"));
            }
        }

        initialize();
    });
