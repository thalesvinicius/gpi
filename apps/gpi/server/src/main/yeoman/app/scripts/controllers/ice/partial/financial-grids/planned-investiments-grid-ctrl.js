'use strict'

angular
	.module('gpifrontApp')
	.controller('PlannedInvestmentsGridCtrl', function($scope, Restangular, GridButton, growl, $filter){

        $scope.investimentoPrevisto = $scope.$parent.investimentoPrevisto;            
        $scope.investimentoPrevistoSelectedItems = $scope.$parent.investimentoPrevistoSelectedItems;

		$scope.investimentoPrevistoColumnDefs = [{
            field: "ano",
            enableCellEdit: true,
            width: "**",            
            translationKey: "common.achievement.year",
            editableCellTemplate: '<input type="numeric" ui-mask="9999" maxlength="4" ng-class="\'colt\' + col.index"  ng-input="COL_FIELD" ng-model="COL_FIELD"/>'                        
        }, {
            field: "valorMaquinasEquip",
            enableCellEdit: true,
            cellFilter: "currency:''",
            editableCellTemplate: '<input type="text" format="currency" maxlength="21" ng-class="\'colt\' + col.index"  ng-input="COL_FIELD" ng-model="COL_FIELD"/>',                                                            
            width: "**",            
            translationKey: "financial.machinary.investiment"
        }, {
            field: "valorCapitalGiroEOutros",
            cellFilter: "currency:''",
            editableCellTemplate: '<input type="text" format="currency" maxlength="21" ng-class="\'colt\' + col.index"  ng-input="COL_FIELD" ng-model="COL_FIELD"/>',                                                            
            enableCellEdit: true,
            width: "**",            
            translationKey: "financial.other.investments"
        }, {
            field: "investimentoTotal",
            cellFilter: "currency:''",
            enableCellEdit: false,
            width: "**",            
            translationKey: "financial.total.investment"
        }];

        $scope.investimentoPrevistoButtonsConfig = {            
            newButton: new GridButton(true, 'common.new', false, null, null, ''),
            deleteButton: new GridButton(true, 'common.delete', false, null, deleteFn, '')
        }


        //define its sort-info attribute if you desire a default-sort
        $scope.investimentoPrevistoSortInfo = {
            fields: ['ano'],
            directions: ['asc']
        };

        function deleteFn() {
            growl.info($filter('translate')('message.error.MSG38_SECAO_ATUALIZADA', {
                value: "Investimento Previsto"
            }));                        
        }              		

        $scope.$parent.$watch('investimentoPrevisto', function(newVal, oldVal){
         	if ($scope.$parent.investimentoPrevisto != null) {
                setTimeout(function(){
                    $scope.investimentoPrevisto = newVal;   
                })         		
         	}                
        }, true);

	});


