'use strict'

angular
	.module('gpifrontApp')
	.controller('AttachedFilesSatisfactionSurveyGridCtrl', function($scope, GridButton, dialogs, $filter){

        $scope.gridScope = {};

        // initialize its data-model attribute
        $scope.anexosPesquisaSatisfacao = $scope.$parent.anexosPesquisaSatisfacao;

        // initialize its selected-items attribute
        $scope.anexosPesquisaSatisfacaoSelectedFiles = [];
        
        //Define its column-defs attribute
        $scope.anexosPesquisaSatisfacaoColumnDefs = [{        
            field: 'nome',
            enableCellEdit: false,
            width: "**",
            translationKey: 'common.file.name'
        }, {
            field: 'dataAnexo',
            enableCellEdit: false,
            width: "**",
            translationKey: 'common.date',
            cellFilter: 'dateTransformFilter'
        }];


        //define its sort-info attribute if you desire a default-sort
        $scope.sortInfo = {
            fields: ['nome'],
            directions: ['asc']
        };


        function translate(translationKey) {
            var text = "";
            if (typeof translationKey !== 'undefined' && translationKey) {
                text = $filter('translate')(translationKey);
            }
            return text;
        };

        $scope.preDeleteFile = function() {
            var confirmDialog = dialogs.confirm(translate('DIALOGS_CONFIRMATION'), translate('message.confirm.MSG009_CONFIRMAR_EXCLUSAO'));
            confirmDialog.result.then(function(btn) {
                // Caso confirme executa a exclusão
                $scope.deleteFile();
            }, function(btn) {
                $scope.gridScope.checkSelectAllCheckBox(true, true);
                $scope.gridScope.checkSelectAllCheckBox(false, true);
                // Caso não confirme deseleciona os registros selecionados
                $scope.anexosPesquisaSatisfacaoSelectedFiles.splice(0, $scope.anexosPesquisaSatisfacaoSelectedFiles.length);
            });

        }

        $scope.deleteFile = function() {
            if ($scope.$parent.anexosPesquisaSatisfacao.length > 0 && $scope.anexosPesquisaSatisfacaolength > 0) {
                $scope.anexosPesquisaSatisfacaoSelectedFiles.forEach(function(selected) {
                    var model = $scope.$parent.anexosPesquisaSatisfacao;
                    var indexOnMainList = model.indexOf(selected);
                    // deleting each unit from main list
                    $scope.$parent.anexosPesquisaSatisfacao.splice(indexOnMainList, 1);
                    for (var i = 0; i < $scope.$flow.files.length; i++) {
                        var file = $scope.$flow.files[i];
                        file.cancel();
                    };
                });
                // deleting all 'selectedItems' starting at index 0
                $scope.anexosPesquisaSatisfacaoSelectedFiles.splice(0, $scope.anexosPesquisaSatisfacaoSelectedFiles.length);
            }
        }  

        $scope.disableFn = function () {
            return $scope.anexosPesquisaSatisfacaoSelectedFiles.length < 1;
        }              


        $scope.$parent.$watch('pesquisa.anexos', function(newVal, oldVal) {
            setTimeout(function() {
                $scope.$apply(function(internalData) {
                    if (internalData) {
                        $scope.anexosPesquisaSatisfacao = internalData;
                    }
                }(newVal));
            }, true);
        }, true);


	});