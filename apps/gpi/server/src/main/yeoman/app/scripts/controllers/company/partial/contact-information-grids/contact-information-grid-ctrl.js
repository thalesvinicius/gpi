'use strict';

angular.module('gpifrontApp')
    .controller('ContactInformationGridCtrl', function($scope) {


//--------------------------------------------------------------------- Início Configs registeredContactsGrid

        // initialize its selected-items attribute
		$scope.contactGridFunctions = $scope.$parent.contactGridFunctions;
        $scope.informacoesContato = $scope.$parent.informacoesContato;

        $scope.sortInfo = {
            fields: ['assunto'],
            directions: ['asc']
        };

        $scope.registeredContactsColumnDefs = [{
            field: 'tipoContatoDescricao',
            enableCellEdit: false,
            width: "**",
            translationKey: 'common.contact.relationship.type'
        },{
            field: "assunto",
            translationKey: 'common.subject',
            enableCellEdit: false
        }, {
            field: "dataEfetivado",
            translationKey: 'common.date',
            enableCellEdit: false,
            cellFilter: 'date:\'dd/MM/yyyy\''
        }, {
            field: "id",
            translationKey: 'common.show',
			cellTemplate:  '<div class="text-center" ng-class="col.colIndex()">' +
			            		'<a ng-click="showContact(row.entity.id)"><i class="fa fa-eye page-header-icon"></i></a> </div>',
            enableCellEdit: false
        }];

        $scope.$parent.$watch('informacoesContato', function(newVal, oldVal) {
            if (newVal !== oldVal) {
                $scope.informacoesContato = newVal;
            }
        }, true);
});