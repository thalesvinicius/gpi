'use strict';

angular.module('gpifrontApp')
    .controller('IceScheduleCtrl', function($scope, Restangular, growl, $stateParams, $filter, ScreenService, CADEIA_PRODUTIVA, dialogs, $state, AUTH_ROLES, AuthService, SITUACAO_PROJETO, CHANGE_CRONOGRAMA) {
        var baseProjeto = Restangular.all("projeto");
        $scope.screenService = ScreenService;

        $scope.changes = [];
        $scope.change_cronograma = CHANGE_CRONOGRAMA;
        //--------------------------------------------------------------------- Início Configurações inicial da tela

        // Configurações do datepicker
        $scope.configsDatepickerSchedule = {
            viewMode : 'months',
            restangularUpdated: false
        };

        var initialize = function() {
            $scope.cadeiaProdutiva = CADEIA_PRODUTIVA;

            if (!ScreenService.isNewScreen()){
                // Recupera o projeto do servidor
                Restangular.one("projeto", $stateParams.id).get().then(function(serverProject) {
                    $scope.projeto = serverProject;
                    $scope.getCronograma();

                    if (!$scope.externalUser() && $scope.projeto.situacaoAtual.situacao === SITUACAO_PROJETO.PENDENTE_ACEITE.id) {
                        baseProjeto.one($stateParams.id).getList("cronograma/findChange").then(function(result) {
                            $scope.changes = (result[0] === undefined ? 0 : result[0]);
                        }, function() {
                            growl.error("Ocorreu um erro ao tentar buscar o registro!");
                        });
                    }
                });
            }

            $scope.configsDatepickerSchedule.disabled = $scope.screenService.isShowScreen() || $scope.isAnalistaInfra() || !$scope.canEditProject();
        };

        initialize();

        //--------------------------------------------------------------------- Fim Configurações inicial da tela

        //Verifica se o usuário logado é um "Usuário Externo"
        $scope.externalUser = function() {
            return AuthService.isUserAuthorized([AUTH_ROLES.externalResponsible, AUTH_ROLES.externalMeling, AUTH_ROLES.externalOther]);
        }

        // Recupera o cronograma
        $scope.getCronograma = function() {
            Restangular.one("projeto", $stateParams.id).one("cronograma").get().then(function(cronograma) {
                if (cronograma === undefined) {
                    $scope.cronograma = {projeto:{id: $stateParams.id},usuarioResponsavel: {
                        nome: ''
                    },
                    dataUltimaAlteracao: ''};
                } else {
                    $scope.cronograma = cronograma;
                }
                $scope.configsDatepickerSchedule.restangularUpdated = true;
            });
        };

        $scope.disableNewFn = function() {
            return AuthService.isUserAuthorized([AUTH_ROLES.infraAnalist,AUTH_ROLES.informationAnalist,AUTH_ROLES.director,AUTH_ROLES.generalSecretary]);
        }

        $scope.isSucroenergetico = function(){
            if($scope.projeto !== undefined){
                return $scope.projeto.cadeiaProdutiva.id === $scope.cadeiaProdutiva.SUCROENERGETICO.id;
            }
            return null;
        }

        $scope.save = function() {
            if($scope.isSucroenergetico()){
                delete $scope.cronograma.inicioProjeto;
                delete $scope.cronograma.inicioImplantacao;
                delete $scope.cronograma.terminoProjeto;
                delete $scope.cronograma.inicioOperacao;
            }else{
                delete $scope.cronograma.inicioProjetoExecutivo;
                delete $scope.cronograma.terminoProjetoExecutivo;
                delete $scope.cronograma.inicioImplantacaoAgricola;
                delete $scope.cronograma.inicioImplantacaoIndustrial;
                delete $scope.cronograma.terminoImplantacaoAgricola;
                delete $scope.cronograma.terminoImplantacaoIndustrial;
                delete $scope.cronograma.inicioContratacaoEquipamentos;
                delete $scope.cronograma.inicioOperacaoSucroenergetico;
            }

            Restangular.one("projeto", $stateParams.id).post("cronograma", $scope.cronograma).then(function() {
                growl.success($filter('translate')("message.success.MSG006_REGISTRO_GRAVADO_SUCESSO"));
                // Utilizado para permitir que o usuário mude de página
                // sem a mensagem de perca de dados
                $scope.formSchedule.$setPristine();
                //  Após salvar vai para a aba de Infraestrutura e Meio Ambiente
                $state.go('base.project.ice.infrastructureTpl', {
                    'screen': $scope.screenService.getScreenName(),
                    'id': $stateParams.id
                });
            }, function() {
                growl.error("Ocorreu um erro ao tentar gravar o registro!");
            });
        };
    }).constant('CHANGE_CRONOGRAMA', {
        INICIOCONTRATACAOEQUIPAMENTOS: {
            posicao: 0,
            descricao: "Início Contratacao de Equipamentos"
        },
        INICIOIMPLANTACAO: {
            posicao: 1,
            descricao: "Início Implantação"
        },
        INICIOIMPLANTACAOAGRICOLA: {
            posicao: 2,
            descricao: "Início Implantação Agricola"
        },
        INICIOIMPLANTACAOINDUSTRIAL: {
            posicao: 3,
            descricao: "Início Implantação Industrial"
        },
        INICIOOPERACAO: {
            posicao: 4,
            descricao: "Início Operação"
        },
        INICIOOPERACAOSUCROENERGETICO: {
            posicao: 5,
            descricao: "Inicio Operação Sucroenergetico"
        },
        INICIOPROJETO: {
            posicao: 6,
            descricao: "Início Projeto"
        },
        INICIOPROJETOEXECUTIVO: {
            posicao: 7,
            descricao: "Início Projeto Executivo"
        },
        INICIOTRATAMENTOTRIBUTARIOPREVISTO: {
            posicao: 8,
            descricao: "Início Tratamento Tributario Previsto"
        },
        INICIOTRATAMENTOTRIBUTARIOREALIZADO: {
            posicao: 9,
            descricao: "Início Tratamento Tributario Realizado"
        },
        TERMINOIMPLANTACAOAGRICOLA: {
            posicao: 10,
            descricao: "Término Implantacao Agricola"
        },
        TERMINOIMPLANTACAOINDUSTRIAL: {
            posicao: 11,
            descricao: "Término Implantacao Industrial"
        },
        TERMINOPROJETO: {
            posicao: 12,
            descricao: "Término Projeto"
        },
        TERMINOPROJETOEXECUTIVO: {
            posicao: 13,
            descricao: "Término Projeto Executivo"
        }
    });