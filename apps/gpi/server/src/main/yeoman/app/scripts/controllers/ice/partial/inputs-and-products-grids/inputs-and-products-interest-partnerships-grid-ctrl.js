'use strict';

angular.module('gpifrontApp')
    .controller('InputsAndProductsInterestPartnershipsGridCtrl', function($scope, Restangular, $filter, $rootScope, GridButton, growl) {
     
        $scope.parcerias = $scope.$parent.insumoProduto.parcerias;

        $scope.selectedItems = [];

        $scope.columnDefs = [{
            field: 'descricao',
            enableCellEdit: true,
            width: "**",
            translationKey: 'inputsandproducts.what',
            editableCellTemplate: '<input type="text" placeholder="{{\'inputsandproducts.what\' | translate}}" ng-class="\'colt\' + col.index"  ng-input="COL_FIELD" ng-model="COL_FIELD" maxlength="100"/>'

        }];

        $scope.buttonsConfig = {
            //function GridButton(show, translationKey, searchMode, disable, action, screenLink)
            newButton: new GridButton(true, 'contact.new', false, null, null, ''),
            deleteButton: new GridButton(true, 'common.delete', false, null, deleteFn, '')
        }

        function deleteFn() {
            growl.info($filter('translate')('message.error.MSG38_SECAO_ATUALIZADA', {
                value: "Interesse em Parcerias"
            }));                        
        }                   

        $scope.sortInfo = {
            fields: ['descricao'],
            directions: ['asc']
        };

        $scope.$parent.$watch('insumoProduto', function(newVal, oldVal) {
            if (newVal.parcerias !== oldVal.parcerias) {
                $scope.parcerias = newVal.parcerias;
            }
        }, true);
    });