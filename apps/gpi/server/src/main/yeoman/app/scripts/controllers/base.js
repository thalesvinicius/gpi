'use strict';

angular.module('gpifrontApp')
    .controller('BaseCtrl', function($scope, $translate, growl, $filter, AuthService, $rootScope, configuration, UserSession, Restangular) {

        $scope.langKey = "pt";
        $scope.loggedUser = UserSession.getUser();
        $scope.projetosRecentes = [];

        $scope.alertas = [];

        // Altera a classe do icone passado
        $scope.changeLanguage = function(language) {
            $translate.use(language);
            $scope.langKey = language;
        }

        $scope.logout = function() {
            AuthService.logout();
        }

        $scope.totalLoading = 0;

        $scope.getProjetoRecentes = function() {
            Restangular.all("projeto/recentes").getList().then(function(projetos) {
                $scope.projetosRecentes = projetos;
            });
        }

        $scope.getAlertas = function() {
            Restangular.all("alerta/"+$scope.loggedUser.id).getList().then(function(alertas) {
                $scope.alertas = alertas;
            });
        }

        $scope.getAlertas();

        $rootScope.$on('cfpLoadingBar:started', function() {
            $scope.showLoading = true;
            $scope.totalLoading++;
        });

        $rootScope.previousState;
        $rootScope.currentState;
        $rootScope.$on('$stateChangeSuccess', function(ev, to, toParams, from, fromParams) {
            $rootScope.previousState = from.name;
            $rootScope.currentState = to.name;
            $rootScope.fromParams = fromParams;

            if (!from.name === 'base.projectSearch') {
                fromParams = null;
            }
        });

        $rootScope.$on('cfpLoadingBar:completed', function() {
            $scope.totalLoading--;
            if ($scope.totalLoading <= 0) {
                setTimeout(function() {
                    $scope.showLoading = false;
                }, 500);
            }
        });

        if (AuthService.isAuthenticated() && $scope.loggedUser && $scope.loggedUser.accessToken) {
            Restangular.setDefaultHeaders({
                Authorization: "Bearer " + $scope.loggedUser.accessToken
            });
        }

        Restangular.setErrorInterceptor(function(response) {
            if (response.status == 401) {
                AuthService.logout();
                return false;
            } else if (response.data && response.data.consumerMessage) {
                growl.error($filter('translate')('message.error.' + response.data.consumerMessage));
                return false;
            }
            return true;
        });

        $scope.getProjetoRecentes();

    });
