'use strict'

angular.module('gpifrontApp')
    .controller('StatusReportCtrl', function($scope) {
        $scope.dados = [];
        // Define os filtros que serão utilizados na busca.
    	$scope.filterOptions = {
    		estagio: true,
    		anual: false,
    		regiaoPlanejamento: true,
    		gerencia: true,
    		cadeiaProdutiva: true,
    		prospeccao: true,
            intervaloDatas: false,
            mesAno: true
    	};           

        $scope.dataColumns = [{ 
            title: 'common.company', 
            field: 'empresa', 
            visible: true, 
            filter: { 'name': 'text' } 
        }, { 
            title: 'common.project', 
            field: 'projeto', 
            visible: true 
        }, { 
            title: 'address.city', 
            field: 'cidade', 
            visible: true 
        }, { 
            title: 'location.planing.region', 
            field: 'regiaoPlanejamento', 
            visible: true 
        }, { 
            title: 'reunionPanelReport.invest', 
            field: 'investimentoPrevisto', 
            visible: true,
            format: 'currency' 
        }, { 
            title: 'reunionPanelReport.initial.billing', 
            field: 'faturamentoInicial', 
            visible: true,
            format: 'currency'  
        }, { 
            title: 'reunionPanelReport.full.billing', 
            field: 'faturamentoPleno', 
            visible: true, 
            format: 'currency' 
        }, { 
            title: 'reunionPanelReport.direct.jobs', 
            field: 'empregosDiretos', 
            visible: true 
        }, { 
            title: 'reunionPanelReport.productive.chain', 
            field: 'cadeiaProdutiva', 
            visible: true 
        }, { 
            title: 'reunionPanelReport.current.stage', 
            field: 'estagioAtual', 
            visible: true 
        }, { 
            title: 'reunionPanelReport.stage.date', 
            field: 'dataEstagioAtual', 
            visible: true ,
            format: 'date'
        }, { 
            title: 'reunionPanelReport.analyst', 
            field: 'analista', 
            visible: true 
        }, { 
            title: 'reunionPanelReport.management', 
            field: 'gerencia', 
            visible: true 
        }];

	});    	
