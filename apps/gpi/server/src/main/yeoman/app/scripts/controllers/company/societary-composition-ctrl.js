'use strict';

angular.module('gpifrontApp')
    .controller('SocietaryCompositionCtrl', function($scope, Restangular, $filter, $rootScope, GridButton,growl) {

        $scope.societaryComposition = [];

        $scope.selectedItems = [];

        $scope.deletedItems = [];

        $scope.columnDefs = [{
            field: 'socio',
            enableCellEdit: true,
            width: "*",
            translationKey: 'common.partner',
            editableCellTemplate: '<input type="text" placeholder="{{\'common.partner\' | translate}}" ng-class="\'colt\' + col.index"  ng-input="COL_FIELD" ng-model="COL_FIELD" maxlength="50"/>'
        },{
            field: 'empresa_grupo',
            enableCellEdit: true,
            width: "*",
            translationKey: 'common.companyGroup',
            editableCellTemplate: '<input type="text" placeholder="{{\'common.companyGroup\' | translate}}" ng-class="\'colt\' + col.index"  ng-input="COL_FIELD" ng-model="COL_FIELD" maxlength="100"/>'
        },{
            field: 'participacao_societaria',
            enableCellEdit: true,
            cellFilter: "currency:''",
            width: "**",
            editableCellTemplate: '<input type="text" format="number" maxlength="6" placeholder="{{\'common.equityInterest\' | translate}}" ng-class="\'colt\' + col.index"  ng-input="COL_FIELD" ng-model="COL_FIELD"/>',
            translationKey: 'common.equityInterest'
        },{
            field: 'cpf_cnpj',
            enableCellEdit: true,
            width: "*",
            translationKey: 'common.cpf.cnpj',
            editableCellTemplate: '<input type="text" ui-mask="99999999999?999" ng-class="\'colt\' + col.index"  ng-input="COL_FIELD" ng-model="COL_FIELD" maxlength="14"/>'
        }];

        function deleteFn() {
            growl.info($filter('translate')('message.error.MSG38_SECAO_ATUALIZADA',{value:'&quot;Composição Societária&quot;'}));
        }

        $scope.buttonsConfig = {
            //function GridButton(show, translationKey, searchMode, disable, action, screenLink)
            newButton: new GridButton(true, 'company.new.societary.composition', false, null, null, ''),
            deleteButton: new GridButton(true, 'common.delete', false, null, deleteFn, '')

        }

        var changeColumnDisplayNames = function() {
            for (var i = 0; i < $scope.columnDefs.length; i++) {
                $scope.columnDefs[i].displayName = $scope.columnDefs[i].translationFn();
            };
        };


        $scope.$parent.$watch('empresa', function(newVal, oldVal) {
            setTimeout(function() {
                $scope.societaryComposition = newVal.composicaoSocietarias;
            })
        }, true);

        $scope.sortInfo = {
            fields: ['socio'],
            directions: ['asc']
        };

    });
