angular.module('gpifrontApp').directive('formCheckChanges', function(dialogs, $filter, $state) {
    return {
        link: function(scope, element, attr) {
            if (!scope.formsToCheck || angular.isUndefined(scope.formsToCheck)) {
                scope.formsToCheck = [];
            }
            scope.formsToCheck.push(attr.name);
            window.onbeforeunload = function() {
                for (var i = 0; i < scope.formsToCheck.length; i++) {
                    if (scope[scope.formsToCheck[i]].$dirty) {
                        return $filter('translate')('message.confirm.MSG33_DADOS_NAO_SALVOS');
                    }
                };
            };
            scope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {
                for (var i = 0; i < scope.formsToCheck.length; i++) {
                    if (scope[scope.formsToCheck[i]].$dirty) {
                        event.preventDefault();
                        var confirmDialog = dialogs.confirm($filter('translate')('DIALOGS_CONFIRMATION'), $filter('translate')('message.confirm.MSG33_DADOS_NAO_SALVOS'));
                        confirmDialog.result.then(function(btn) {
                            scope.formsToCheck = [];
                            $state.transitionTo(toState.name, toParams);
                        });
                        break;
                    }
                };
            });
        }
    };
});
