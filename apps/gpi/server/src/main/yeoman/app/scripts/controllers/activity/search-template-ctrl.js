'use strict';

angular.module('gpifrontApp')
    .controller('ActivitySearchTemplateCtrl', function($scope, TIPO_TEMPLATE, STATUS_TEMPLATE, Restangular, growl, $filter) {
    	$scope.atividadeTemplatesEncontrados = [];
    	$scope.tiposTemplate = TIPO_TEMPLATE;
    	$scope.statusTemplate = STATUS_TEMPLATE
		$scope.filtro = {};

		var baseTemplates = Restangular.all('template');

        function translate(translationKey) {
            var text = "";
            if (typeof translationKey !== 'undefined' && translationKey) {
                text = $filter('translate')(translationKey);
            }
            return text;
        };

		$scope.search = function() {
			baseTemplates.getList($scope.filtro).then(function(templates){
				$scope.atividadeTemplatesEncontrados = templates;				
                if (templates.length === 0) {
                    growl.error(translate('message.error.MSG004_NENHUM_REGISTRO_ENCONTRADO'));                    
                }
			})
		}

        $scope.reset = function() {
            $scope.filtro = {};            
        }
    	
    });
