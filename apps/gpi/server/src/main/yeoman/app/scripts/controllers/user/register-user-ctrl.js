'use strict';

angular.module('gpifrontApp')
    .controller('RegisterUserCtrl', function($scope, $rootScope, UserSession, $translate, AUTH_EVENTS, AuthService,
        $state, $stateParams, Restangular, $http, growl, configuration, $filter) {

        $scope.hasErrors = false;
        $scope.validationHelper = {};
        $scope.credentials = {
            username: '',
            password: '',
            confirmPassword: '',
            accessToken: ''
        };

        function init() {
            $scope.credentials.username = UserSession.getUser().email;
            $scope.credentials.accessToken = UserSession.getUser().accessToken;
        }

        function translate(translationKey) {
            var text = "";
            if (typeof translationKey !== 'undefined' && translationKey) {
                text = $filter('translate')(translationKey);
            }
            return text;
        };

        var validate = function() {
            $scope.hasErrors = false;
            if ($scope.hasErrors = $scope.hasErrors || !$scope.credentials.password) {
                $scope.validationHelper.password = $filter('translate')('common.field.required');
            }

            if ($scope.hasErrors = $scope.hasErrors || !$scope.credentials.confirmPassword) {
                $scope.validationHelper.confirmPassword = $filter('translate')('common.field.required');
            }

            if (!$scope.hasErrors && !isPasswordConfirmationValid()) {
                growl.error(translate('user.invalid.confirmPassword'));
                $scope.hasErrors = true;
            }

            if (!$scope.hasErrors && $scope.credentials.password && ($scope.credentials.password.length < 8 || $scope.credentials.password.length > 40)) {
                growl.error(translate('message.error.MSG87_SENHA_INVALIDA'));
                $scope.hasErrors = true;
            }

            return $scope.hasErrors;
        }

        var isPasswordConfirmationValid = function() {
            return $scope.credentials.password === $scope.credentials.confirmPassword;
        }

        $scope.cleanInputMessages = function() {
            $scope.hasErrors = false;
            $scope.validationHelper = {
                password: '',
                confirmPassword: ''
            };
        }

        $scope.salvar = function() {
            $scope.cleanInputMessages();
            if (!validate()) {
                var request = {
                    password: $scope.credentials.password,
                    email: $scope.credentials.username,
                    token: $scope.credentials.accessToken
                }

                submitResetPassword(request);
            }
        }

        var submitResetPassword = function(request) {            
            Restangular.all('user/alterarSenha').post(request).then(function() {
                growl.success("Senha alterada com sucesso.");
            }, function errorCallback(response) {
                if (response.status > 400) {
                    $scope.hasErrors = true;
                    $scope.errorMessage = $filter('translate')(response.data.consumerMessage);
                } else {
                    $scope.hasErrors = true;
                    growl.error(translate("Ocorreu o seguinte erro ao alterar a senha: " + response.message));
                }
            });
        }

        $scope.clearFunctions = {
            mainScreen: function() {
                $scope.credentials = {
                    username: '',
                    password: '',
                    confirmPassword: ''
                };
            },
            captchaScreen: function() {

            }
        };

        $scope.clearScreen = function() {
            $scope.clearFunctions.mainScreen();
            $scope.clearFunctions.captchaScreen();
        }

        init();

    });
