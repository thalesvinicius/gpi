'use strict';

angular.module('gpifrontApp')
    .controller('TemplateGridCtrl', function($scope, GridButton, TIPO_TEMPLATE, Restangular, growl, $filter) {

        var baseDomain = Restangular.all("domain");
        var baseAtividadeLocal = Restangular.all("atividadeLocal");

        $scope.localList = baseDomain.one("local").getList().$object;
        $scope.atividadeLocalList = baseDomain.one("atividadeLocal").getList().$object;

        var objAtividade = new Object();
        objAtividade.atividades = [];
        objAtividade.findAllAtividadeLocalByLocalId = function(localId){
            this.atividades = baseAtividadeLocal.one("findAllByLocalId", localId).getList().$object;
        }        

    	$scope.gridScope = {
    		tipo: TIPO_TEMPLATE,
            locais: $scope.localList,
            objAtividade: objAtividade,
            changeBloqueiaICE: function(row){                
                for (var i = $scope.atividadesTemplate.length - 1; i >= 0; i--) {
                    $scope.atividadesTemplate[i].bloqueiaICE = row.rowIndex === i && $scope.atividadesTemplate[i].bloqueiaICE === true;    
                };                    
            },
            changeCheckboxes: function(row, col) {        
                if (col.field === "ii" && $scope.atividadesTemplate[row.rowIndex].ii === true) {
                    $scope.atividadesTemplate[row.rowIndex].df = $scope.atividadesTemplate[row.rowIndex].oi = $scope.atividadesTemplate[row.rowIndex].cc = !$scope.atividadesTemplate[row.rowIndex].ii
                } else if (col.field === "df" && $scope.atividadesTemplate[row.rowIndex].df === true) {
                    $scope.atividadesTemplate[row.rowIndex].ii = $scope.atividadesTemplate[row.rowIndex].oi = $scope.atividadesTemplate[row.rowIndex].cc = !$scope.atividadesTemplate[row.rowIndex].df;
                } else if (col.field === "oi" && $scope.atividadesTemplate[row.rowIndex].oi === true) {
                    $scope.atividadesTemplate[row.rowIndex].ii = $scope.atividadesTemplate[row.rowIndex].df = $scope.atividadesTemplate[row.rowIndex].cc = !$scope.atividadesTemplate[row.rowIndex].oi;
                } else if (col.field === "cc" && $scope.atividadesTemplate[row.rowIndex].cc === true) {            
                    $scope.atividadesTemplate[row.rowIndex].ii = $scope.atividadesTemplate[row.rowIndex].oi = $scope.atividadesTemplate[row.rowIndex].df = !$scope.atividadesTemplate[row.rowIndex].cc;
                }
            }
    	}

   		// initialize its selected-items attribute
        $scope.atividadesTemplateSelectedItems = [];

        //Define its column-defs attribute
        $scope.atividadesTemplateColumnDefs = [{
            field: 'atividadeLocal.local',
            enableCellEdit: true,
            width: "*",
            translationKey: 'common.local',
            cellTemplate: "<div class=\"ngCellText\" ng-class=\"col.colIndex()\"><span ng-cell-text>{{COL_FIELD.descricao}}</span></div>",
            editableCellTemplate: '<select ng-change=\'objAtividade.findAllAtividadeLocalByLocalId(COL_FIELD.id)\' ng-class="\'colt\' + col.index" ng-input="COL_FIELD" ng-model="COL_FIELD" ng-options="local as local.descricao for local in locais | orderBy: \'descricao\'" />'
        }, {
            field: 'atividadeLocal',
            enableCellEdit: true,
            width: "**",
            translationKey: 'activity.activity',
            cellTemplate: "<div class=\"ngCellText\" ng-class=\"col.colIndex()\"><span ng-cell-text>{{COL_FIELD.descricao}}</span></div>",
            editableCellTemplate: '<select ng-class="\'colt\' + col.index" ng-input="COL_FIELD" ng-model="COL_FIELD" ng-options="atividade as atividade.descricao for atividade in objAtividade.atividades | orderBy: \'descricao\'" />'
        }, {
            field: "obrigatorio",
            enableCellEdit: false,
            width: "**",
            translationKey: 'common.required',
            cellTemplate: '<input type="checkbox" placeholder="{{\'common.name\' | translate}}" ng-class="\'colt\' + col.index"  ng-input="COL_FIELD" ng-model="COL_FIELD" maxlength="100"/>'
        }, {
            field: "bloqueiaICE",
            enableCellEdit: false,
            width: "**",
            translationKey: 'activity.block.ice',
            cellTemplate: '<input ng-change="changeBloqueiaICE(row)" type="checkbox" placeholder="{{\'common.name\' | translate}}" ng-class="\'colt\' + col.index"  ng-input="COL_FIELD" ng-model="COL_FIELD" maxlength="100"/>'
        }, {
            field: "df",
            enableCellEdit: false,
            width: "**",
            translationKey: "activity.df",
            cellTemplate: '<input ng-change="changeCheckboxes(row, col)" type="checkbox" placeholder="{{\'common.name\' | translate}}" ng-class="\'colt\' + col.index"  ng-input="COL_FIELD" ng-model="COL_FIELD" maxlength="100"/>'
        }, {
            field: "ii",
            enableCellEdit: false,
            width: "**",
            translationKey: "activity.ii",
            cellTemplate: '<input ng-change="changeCheckboxes(row, col)" name="ii" type="checkbox" placeholder="{{\'common.name\' | translate}}" ng-class="\'colt\' + col.index"  ng-input="COL_FIELD" ng-model="COL_FIELD" maxlength="100"/>'
        }, {
            field: "oi",
            enableCellEdit: false,
            width: "**",
            translationKey: "activity.oi",
            cellTemplate: '<input ng-change="changeCheckboxes(row, col)" name="oi" type="checkbox" placeholder="{{\'common.name\' | translate}}" ng-class="\'colt\' + col.index"  ng-input="COL_FIELD" ng-model="COL_FIELD" maxlength="100"/>'
        }, {
            field: "cc",
            enableCellEdit: false,
            width: "**",
            translationKey: "activity.cc",
            cellTemplate: '<input ng-change="changeCheckboxes(row, col)" name="cc" type="checkbox" placeholder="{{\'common.name\' | translate}}" ng-class="\'colt\' + col.index"  ng-input="COL_FIELD" ng-model="COL_FIELD" maxlength="100"/>'
        }];

        // $scope.atividadesTemplateButtonsConfig = {
        //     newButton: new GridButton(true, 'common.new', false, null, null, ''),
        //     editButton: new GridButton(false, 'common.edit', true, null, null, '/atividade/editar'),
        //     deleteButton: new GridButton(true, 'common.delete', false, null, deleteFn, '')
        // }

        function deleteFn() {
            growl.info($filter('translate')('message.error.MSG38_SECAO_ATUALIZADA', {value: "template de atividades"}));
        }

        // //define its sort-info attribute if you desire a default-sort
        // $scope.atividadesTemplateSortInfo = {
        //     fields: ['posicao'],
        //     directions: ['asc']
        // };

        $scope.addActivity = function() {
            var newOrder = $scope.$parent.atividadesTemplate.length + 1;
            $scope.$parent.atividadesTemplate.push({posicao: newOrder});
        }

    });
