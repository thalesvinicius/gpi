'use strict'
angular.module('gpifrontApp')
    .controller('ReportTrackingEnvironmentCtrl', function($scope, ScreenService , growl, Restangular, $filter, ESTAGIO_PROJETO, STATUS_PROJETO, AuthService, AUTH_ROLES, UserSession, STATUS_PROCESSO, TIPO_MEIO_AMBIENTE, SITUACAO_PROJETO, $http, configuration) {

        $scope.disableHeader = true;
        $scope.filtro = {};
        $scope.selectedItems = [];
        $scope.sortInfo = {
            fields: ['nomePrincipal'],
            directions: ['desc']
        };

        // RNE163 se for gerente não exibe o campo Gerência e se for analista de portfolio
        //  não exibe os campos Gerência e Analista
        $scope.isGerente = function() {
            return AuthService.isUserAuthorized([AUTH_ROLES.manager]);
        }

        // RNE163 se for gerente não exibe o campo Gerência e se for analista de portfolio
        //  não exibe os campos Gerência e Analista
        $scope.isPromocao = function() {
            return AuthService.isUserAuthorized([AUTH_ROLES.promotionAnalist]);
        }

        /*
         *   Recupera todos os ids da lista passada
         */
         var getIdList = function(list) {
            var idList = [];
            if (list !== undefined) {
                if (_.isArray(list)) {
                    _.each(list, function(val, key) {
                        idList.push(val.id);
                    });
                } else {
                    idList.push(list.id);
                }
            }
            return idList;
        }

        $scope.disabledExport = function() {
            return $scope.data.length === 0;
        }

        /*
         *   Evento chamado ao selecionar gerência
         */
        $scope.changeManageMultiSelect = function() {
            //  Busca os usuários com base no departamento
            if ($scope.filtro.gerencias && $scope.filtro.gerencias.length > 0) {
                Restangular.all('user/findAnalistasByDepartamentos')
                    .getList({"departamentos": $scope.filtro.gerencias})
                        .then(function(analistas) {
                    $scope.analistas = analistas;
                    //  Por default todos os analistas vem selecionados
                    $scope.filtro.analistas = $scope.isPromocao() ? [UserSession.getUser().id] : getIdList(analistas);
                });

                Restangular.all('cadeiaProdutiva/byDepartamentos').getList({
                    "departamentos": $scope.filtro.gerencias
                }).then(function(cadeiasProdutivas) {
                    $scope.cadeiasProdutivas = cadeiasProdutivas;
                });
            }
        }

        /*
         *   Utilizado pelos usuarios GERENTE e PROMOCAO para recuperar as gerencias
         */
        var getDepartamentosUsuarioLogado = function() {
            Restangular.one('user/' + UserSession.getUser().id).get().then(function(user) {
                $scope.filtro.gerencias = getIdList(user.departamento);
                //  Aciona o evento onChange do select de departamentos
                //  para preencher os analistas e as cadeias produtivas
                $scope.changeManageMultiSelect();
            });
        }

        /*  Iniciando defaults do relatório */
        $scope.reset = function() {
            var tipoMeioAmbiente = [];
            _.each($scope.etapa, function(val, key) {
                tipoMeioAmbiente.push(val.tipoMeioAmbiente);
            });

            // $scope.filtro.etapa = tipoMeioAmbiente;

            /*  Inicializando as listas com seus respectivos registros selecionados. */
            // $scope.filtro.estagios = getIdList($scope.estagios);
            // $scope.filtro.statusProcesso = getIdList($scope.statusProcesso);

            $scope.filtro.status = [STATUS_PROJETO.ATIVO.id];

            Restangular.all('domain/regiaoPlanejamento').getList().then(function(regioesPlanejamento) {
                $scope.regioesPlanejamento = regioesPlanejamento;
                //  Por default todas as regioesPlanejamento vem selecionadas
                // $scope.filtro.regioesPlanejamento = getIdList(regioesPlanejamento);
            });

            // RNE163 se for gerente não exibe o campo Gerência e se for analista de portfolio
            //  não exibe os campos Gerência e Analista
            if ($scope.isGerente() || $scope.isPromocao()) {
                getDepartamentosUsuarioLogado();
            } else {
                Restangular.all('domain/departamento').getList().then(function(departamentos) {
                    $scope.gerencias = departamentos;
                    //  Por default todas as gerencias vem selecionadas
                    //$scope.filtro.gerencias = getIdList(departamentos);
                    $scope.changeManageMultiSelect();
                });
            }

            $scope.data = [];
        }

        var initialize = function() {
            $scope.estagios = ESTAGIO_PROJETO.getAll();
            $scope.status = STATUS_PROJETO.getAll();
            $scope.statusProcesso = STATUS_PROCESSO.getAll();
            $scope.etapa = TIPO_MEIO_AMBIENTE.getAll();

            $scope.reset();
        }

        initialize();

        var getPath = function(format) {
            return 'relatorio/projeto/ambiental/acompanhamento/' + format;
        }

        var getHeader = function(format) {
            var header = '';
            if (format === 'pdf') {
                header = 'application/pdf';
            } else if (format === 'xlsx') {
                header = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
            }
            return header;
        }

        var getFileName = function() {
            return 'RelatorioAmbiental';
        }

        $scope.clean = function() {
            $scope.reset();
        }

        $scope.export = function(format) {
            $http({
                method: 'GET',
                url: configuration.localPath + "api/" + getPath(format),
                params: {
                    "status" : $scope.filtro.status,
                    "regioesPlanejamento" : $scope.filtro.regioesPlanejamento,
                    "gerencias" : $scope.filtro.gerencias,
                    "analistas" : $scope.filtro.analistas,
                    "cadeiasProdutivas" : $scope.filtro.cadeiasProdutivas,
                    "statusProcesso" : $scope.filtro.statusProcesso,
                    "etapa" :  $scope.filtro.etapa,
                    'todosEstagios': _.map($scope.estagios,function(estagio){return estagio.id}),
                    "estagios": $scope.filtro.estagios
                },
                headers: {
                    Accept: getHeader(format),
                    Authorization: 'Bearer ' + UserSession.getUser().accessToken,
                    'todasGerencias': getIdList($scope.gerencias),
                    'todasCadeiasProdutivas' :getIdList($scope.cadeiasProdutivas),
                    'todasRegioesPlanejamento': getIdList($scope.regioesPlanejamento),
                    'todosAnalistas': getIdList($scope.analistas)
                },
                responseType: 'arraybuffer'
            }).
            success(function(response) {
                ScreenService.browserAwareSave(response,"AcompanhamentoAmbiental."+format,
                    getHeader(format));
            })
        }

        /*Início configs grid*/
        $scope.columnDefs = [{
            field: 'nomePrincipal',
            enableCellEdit: false,
            width: "150",
            translationKey: 'common.company'
        }, {
            field: 'projeto',
            enableCellEdit: false,
            width: "100",
            translationKey: 'common.project'
        }, {
            field: 'cidade',
            enableCellEdit: false,
            width: "150",
            translationKey: 'address.city'
        }, {
            field: 'regiao',
            enableCellEdit: false,
            width: "200",
            translationKey: 'location.planing.region'
        }, {
            field: 'cadeiaProdutiva',
            enableCellEdit: false,
            width: "150",
            translationKey: 'common.productive.chain'
        }, {
            field: 'classeEmpreendimento',
            enableCellEdit: false,
            width: "200",
            translationKey: 'reportTrackingEnvironment.environmentClass'
        }, {
            field: 'etapas',
            enableCellEdit: false,
            width: "150",
            translationKey: 'reportTrackingEnvironment.step'
        }, {
            field: 'dataEntregaEstudos',
            enableCellEdit: false,
            width: "250",
            translationKey: 'environment.deliveryDateStudies',
            cellFilter: 'dateTransformFilter:\'dd/MM/yyyy\''
        }, {
            field: 'expectativaEmissaoLicenca',
            enableCellEdit: false,
            width: "300",
            translationKey: 'reportTrackingEnvironment.expectedLicenseEmission',
            cellFilter: 'dateTransformFilter:\'dd/MM/yyyy\''
        }, {
            field: 'expectativaOI',
            enableCellEdit: false,
            width: "150",
            translationKey: 'reportTrackingEnvironment.expectedOI',
            cellFilter: 'dateTransformFilter:\'dd/MM/yyyy\''
        }, {
            field: 'situacaoProcesso',
            enableCellEdit: false,
            width: "150",
            translationKey: 'reportTrackingEnvironment.processStatus'
        }, {
            field: 'diasForaDoPrazo',
            enableCellEdit: false,
            width: "150",
            translationKey: 'reportTrackingEnvironment.outOfTime'
        }, {
            field: 'situacaoAtual',
            enableCellEdit: false,
            width: "150",
            translationKey: 'common.currentStatus'
        }, {
            field: 'estagioAtual',
            enableCellEdit: false,
            width: "150",
            translationKey: 'common.currentStage'
        }, {
            field: 'analista',
            enableCellEdit: false,
            width: "100",
            translationKey: 'common.analyst'
        }, {
            field: 'departamento',
            enableCellEdit: false,
            width: "100",
            translationKey: 'common.manage'
        }];
        /*Fim configs grid*/

        $scope.search = function() {
            Restangular.all('relatorio/projeto/ambiental/findByFilters').getList($scope.filtro).then(function(result) {
                $scope.data = result;

                if ($scope.data.length === 0) {
                    growl.error($filter('translate')("message.error.MSG004_NENHUM_REGISTRO_ENCONTRADO"));
                }
            }, function errorCallback(response) {
                if (response.status !== 404) {
                    growl.error("Error: The server returned status " + response.status);
                }
            });
        }
    });