"use strict";

angular.module("gpifrontApp")
    .controller("PersistCompanyCtrl", function($scope, UserSession, ORIGEM_EMPRESA, TIPO_USUARIO, Restangular, $filter, $location, $state, $stateParams, configuration, growl, SITUACAO_EMPRESA, MINAS_GERAIS_ID, ValidationService, ScreenService, AuthService) {
        var baseEmpresa = Restangular.all('empresa');
        var formSubmitted = false;
        $scope.screenService = ScreenService;

        $scope.situacaoEmpresa = SITUACAO_EMPRESA;
        $scope.origemEmpresaEnum = ORIGEM_EMPRESA;

        $scope.isExternalUser = UserSession.getUser().tipo == TIPO_USUARIO.EXTERNO;

        $scope.checkValidation = function(formField) {
            if (!angular.isUndefined(formField)) {
                return formField.$invalid && (formField.$dirty || formSubmitted);
            }
        }

        $scope.BRASIL = {
            "id": 2,
            "nome": "Brasil"
        };

        function init() {            
            $scope.checkUserAuthorization();        

            $scope.empresa = {
                "cadeiaProdutiva": {},
                "endereco": {
                    "municipio": {},
                    "uf": {},
                    "pais": {}
                },
                "logo": {},

                "usuarioResponsavel": {},
                "responsavel": {},
                "origemEmpresa": $scope.origemEmpresaEnum.MINEIRA.id,
                "paisOrigem": $scope.BRASIL,
                "estadoOrigem": {id:MINAS_GERAIS_ID},
                "empresaNovaMG": false,
                "nomesEmpresa": [],
                "composicaoSocietarias": []
            };
            // if ($scope.screenService.isNewScreen()) {

            //     $scope.loadMunicipios(true);
            // }

            $scope.filledCnpj = function() {
                return typeof $scope.empresa.cnpj !== 'undefined' && $scope.empresa.cnpj !== null && $scope.empresa.cnpj.trim() !== '';
            }

            $scope.responsaveis = [];

            uploadInit();
            domainListsInit();

            if (!$scope.screenService.isNewScreen() && $scope.$parent.getId() > 0) {
                Restangular.one("empresa", $scope.$parent.getId()).get().then(function(result) {
                    $scope.empresa = result;
                    $scope.logoUrl = (result.logo) ? configuration.localPath + "api/anexo/logo.png?id=" + result.logo.path : null;
                    
                    // AuthService.verifyUserIsAuthenticatedInsideController($scope.isUserAuthorized);

                    Restangular.all('user/findByEmpresa/' + $scope.empresa.id).getList().then(function(result) {
                        $scope.responsaveis = result;
                    });
                    $scope.anotherNamesRadio = $scope.empresa.nomesEmpresa.length > 0;
                    // if ($scope.empresa.empresaInternacional === true) {
                    // $scope.origemEmpresa.id = $scope.INTERNACIONAL.id;
                    // } else {
                    // if (typeof $scope.empresa.endereco.uf !== 'undefined' && $scope.empresa.endereco.uf != null && typeof $scope.empresa.endereco.uf.id !== 'undefined' && $scope.empresa.endereco.uf.id > 0) {
                    //     $scope.origemEmpresa.id = $scope.empresa.endereco.uf.id

                    // if (typeof $scope.empresa.endereco !== 'undefined' && $scope.empresa.endereco.uf != null) {
                    //     $scope.loadMunicipios($scope.isMineira());
                    // }
                    // }
                    // }

                    $scope.changeOrigemEmpresa();

                    $scope.selectedSubclassesCnae = [];
                    for (var i = 0; i < $scope.empresa.subclassesCnae.length; i++) {
                        $scope.selectedSubclassesCnae.push($scope.empresa.subclassesCnae[i].cnaeId);
                    }

                    buildCnaeFromServer();
                });
            }


            //$scope.anotherNamesRadio = $scope.empresa.nomesEmpresa.length > 0;
            $scope.companyOriginSelect = "";


        };

        // $scope.loadMunicipios = function(isMineira) {
        //     var selectedEstado = null
        //     if (isMineira) {
        //         selectedEstado = MINAS_GERAIS_ID;
        //     } else if($scope.isOutrosEstados()){
        //         selectedEstado = $scope.origemEmpresa.id;
        //     }

        //     setTimeout(function() {
        //         $scope.municipiosEncontrados = [];

        //         Restangular.all('municipio/' + selectedEstado).getList().then(function(result) {
        //             $scope.municipiosEncontrados = result;
        //         });
        //     }, true);
        // }

        $scope.isOrigemBrasil = function() {
            return !_.isNull($scope.empresa.paisOrigem) && !_.isUndefined($scope.empresa.paisOrigem) && $scope.empresa.paisOrigem.id == $scope.BRASIL.id;
        };

        $scope.disableCnpj = function() {
            return $scope.empresa.id > 0 && $scope.filledCnpj();
        }

        function uploadInit() {
            $scope.flowInit = {
                singleFile: true,
                target: configuration.localPath + "api/anexo"
            };
        }

        function domainListsInit() {

            //TODO alterar para utilizar o serviço
            $scope.origensEmpresa = [];
            Restangular.all('domain/uf/nome/asc').getList().then(function(result) {
                // result.unshift($scope.INTERNACIONAL);
                // //Ao selecionar Empresa mineira? - Não, o campo Origem da Empresa não deve exibir o estado "MINAS GERAIS"
                // for (var i = result.length; i--;) {
                //     if (result[i].nome === "MINAS GERAIS") {
                //         result.splice(i, 1);
                //     }
                // }
                $scope.origensEmpresa = result;
            });

            $scope.paises = [];
            Restangular.all('domain/pais/nome/asc').getList().then(function(result) {
                $scope.paises = result;
            });

            $scope.naturezasJuridicas = [];
            Restangular.all('domain/naturezaJuridica').getList().then(function(result) {
                $scope.naturezasJuridicas = result;
            });

            //TODO chamar serviço que busca da tb CadeiaProdutiva
            $scope.cadeiasProdutivas = [];
            Restangular.all('domain/cadeiaProdutiva').getList().then(function(result) {
                $scope.cadeiasProdutivas = result;
            });
            cnaeInit();


        }

        function buildCnaeFromServer() {

            Restangular.all("cnae/cnaeWrapper").post(
                $scope.empresa
            ).then(function(cnaeDTO) {
                setTimeout(function() {
                    if (!$scope.$$phase) {
                        // returns a DTO of cnae list
                        var selectedDivisoes = [];
                        for (var i = 0; i < cnaeDTO.divisoesSelecionadas.length; i++) {
                            selectedDivisoes.push(cnaeDTO.divisoesSelecionadas[i].id);
                        };

                        $scope.selectedDivisoesCnae = selectedDivisoes;

                        $scope.gruposCnae = cnaeDTO.gruposCarregados;

                        var selectedGrupos = [];
                        for (var i = 0; i < cnaeDTO.gruposSelecionados.length; i++) {
                            selectedGrupos.push(cnaeDTO.gruposSelecionados[i].id);
                        };

                        $scope.selectedGruposCnae = selectedGrupos;

                        $scope.classesCnae = cnaeDTO.classesCarregadas;

                        var selectedClasses = [];
                        for (var i = 0; i < cnaeDTO.classesSelecionadas.length; i++) {
                            selectedClasses.push(cnaeDTO.classesSelecionadas[i].id);
                        };

                        $scope.selectedClassesCnae = selectedClasses;

                        $scope.subclassesCnae = cnaeDTO.subclassesCarregadas;

                        $scope.selectedDivisoesCnaeLoaded = true;
                        $scope.selectedGruposCnaeLoaded = true;
                        $scope.selectedClassesCnaeLoaded = true;
                    }
                }, true);
            })
        }

        function cnaeInit() {
            $scope.selectedDivisoesCnaeLoaded = false;
            $scope.selectedGruposCnaeLoaded = false;
            $scope.selectedClassesCnaeLoaded = false;

            $scope.divisoesCnae = []

            Restangular.all("cnae").getList().then(function(result) {
                $scope.divisoesCnae = result;
            });

            if ($scope.screenService.isNewScreen()) {

                $scope.selectedDivisoesCnae = [];

                $scope.gruposCnae = [];
                $scope.selectedGruposCnae = [];

                $scope.classesCnae = [];
                $scope.selectedClassesCnae = [];

                $scope.subclassesCnae = [];
                $scope.selectedSubclassesCnae = [];

                $scope.empresa.subclassesCnae = [];
            }
        }

        function adjustSelectedList(selectedList, loadedList, childSelectedList, childList) {
            var entitiesToRemoveFromSelectedList = [];
            for (var i = 0; i < selectedList.length; i++) {
                var containsSelected = false;
                for (var y = 0; y < loadedList.length; y++) {
                    if (selectedList[i] === loadedList[y].id) {
                        containsSelected = true;
                        break;
                    }
                };
                if (!containsSelected) {
                    entitiesToRemoveFromSelectedList.push(selectedList[i]);
                }
            };

            for (var i = 0; i < entitiesToRemoveFromSelectedList.length; i++) {
                selectedList.splice(selectedList.indexOf(entitiesToRemoveFromSelectedList[i]), 1);
            };

            if (selectedList.length <= 0) {
                childSelectedList.splice(0, childSelectedList.length - 1);
                childList.splice(0, childList.length - 1);;
            }

        }

        function buildDivisoesCnae() {
            if ($scope.selectedDivisoesCnae.length > 0) {
                Restangular.all('cnae/findByParentId').post($scope.selectedDivisoesCnae).then(function(result) {
                    $scope.gruposCnae = result;
                    adjustSelectedList($scope.selectedGruposCnae, $scope.gruposCnae, $scope.selectedClassesCnae, $scope.classesCnae);
                    buildGruposCnae();
                });
            } else {
                $scope.gruposCnae = [];
                //limpando os grupos selecionados
                $scope.selectedGruposCnae = [];

                //limpando as listas de classes e classes selecionadas
                $scope.classesCnae = [];
                $scope.selectedClassesCnae = [];

                //limpando as listas de subclasses e subclasses selecionadas
                $scope.subclassesCnae = [];
                $scope.selectedSubclassesCnae = [];
            }

        }

        function buildGruposCnae() {
            if ($scope.selectedGruposCnae.length > 0) {
                Restangular.all('cnae/findByParentId').post($scope.selectedGruposCnae).then(function(result) {
                    $scope.classesCnae = result;
                    adjustSelectedList($scope.selectedClassesCnae, $scope.classesCnae, $scope.selectedSubclassesCnae, $scope.subclassesCnae);
                    buildClassesCnae();
                });
            } else {
                $scope.classesCnae = [];
                // limpando a lista de classes selecionadas
                $scope.selectedClassesCnae = [];

                //limpando as listas de subclasses e subclasses selecionadas
                $scope.subclassesCnae = [];
                $scope.selectedSubclassesCnae = [];
            }
        }

        function buildClassesCnae() {
            if ($scope.selectedClassesCnae.length > 0) {
                Restangular.all('cnae/findByParentId').post($scope.selectedClassesCnae).then(function(result) {
                    $scope.subclassesCnae = result;
                    adjustSelectedList($scope.selectedSubclassesCnae, $scope.subclassesCnae, [], []);
                });
            } else {
                $scope.subclassesCnae = [];
                $scope.selectedSubclassesCnae = [];
            }
        }

        $scope.$watch("selectedDivisoesCnae", function(newVal, oldVal) {
            if ($scope.selectedDivisoesCnaeLoaded === true) {
                $scope.selectedDivisoesCnaeLoaded = false;
            } else if (newVal !== oldVal) {
                buildDivisoesCnae();
            }
        });

        $scope.$watch("selectedGruposCnae", function(newVal, oldVal) {
            if ($scope.selectedGruposCnaeLoaded === true) {
                $scope.selectedGruposCnaeLoaded = false;
            } else if (newVal !== oldVal) {
                buildGruposCnae();
            }
        });

        $scope.$watch("selectedClassesCnae", function(newVal, oldVal) {
            if ($scope.selectedClassesCnaeLoaded === true) {
                $scope.selectedClassesCnaeLoaded = false;
            } else if (newVal !== oldVal) {
                buildClassesCnae();
            }
        });

        init();


        $scope.hasLogo = function() {
            return !(typeof $scope.logoUrl === "undefined" || $scope.logoUrl === null);
        }

        var formatoLogoValido = function(format) {
            return angular.isUndefined(format) || format == null ? false : !!{
                png: 1,
                gif: 1,
                jpg: 1,
                jpeg: 1,
                bmp: 1,
                tif: 1
            }[format];
        }

        var tamanhoLogoValido = function(size) {
            return angular.isUndefined(size) || size == null ? false : size <= 5242880;
        }

        $scope.$on("flow::fileAdded", function(event, $flow, flowFile) {
            if (formatoLogoValido(flowFile.getExtension())) {
                if (tamanhoLogoValido(flowFile.size)) {
                    $scope.flow = $flow;
                    $scope.flowFile = flowFile;
                    $scope.$parent.companyForm.$setDirty();
                } else {
                    event.preventDefault();
                    if (!angular.isUndefined($scope.flow)) {
                        $scope.flow.files.pop();
                    }
                    growl.error(translate('message.error.MSG_TAMANHO_LOGO_EXCEDIDO'));
                }
            } else {
                event.preventDefault();
                if (!angular.isUndefined($scope.flow)) {
                    $scope.flow.files.pop();
                }
                growl.error(translate('message.error.MSG_FORMATO_LOGO_INVALIDO'));
            }
        });

        $scope.$on('flow::fileSuccess', function(event, $flow, flowFile, response) {
            var responseObj = JSON.parse(response);
            responseObj.nome = flowFile.name;
            $scope.$parent.empresa.logo = responseObj;

        });

        $scope.$on('flow::filesSubmitted', function(event, $flow, flowFile) {
            if (flowFile.length != 0) {
                $scope.flow.upload();
            }
        });

        $scope.cleanJustificative = function() {
            $scope.empresa.justificativaArquivamento = "";
        }

        $scope.checkIfHasNotNomesEmpresa = function() {
            if ($scope.empresa.nomesEmpresa !== undefined && $scope.empresa.nomesEmpresa.length > 0) {
                $scope.anotherNamesRadio = true;
                document.companyForm.anotherNamesRadio[0].checked = true
                growl.error('Favor excluir os nomes já cadastrados antes de marcar este campo!');
            }            
            return true;
        }

        var validate = function() {
            var invalidRazaoSocial = typeof $scope.empresa.razaoSocial === 'undefined' || !$scope.empresa.razaoSocial || $scope.empresa.razaoSocial.trim() === '';
            var invalidNomesEmpresa = typeof $scope.empresa.nomesEmpresa === 'undefined' || !$scope.empresa.nomesEmpresa || $scope.empresa.nomesEmpresa.length === 0;
            var haNomesRepetidos = false;
            if (!invalidNomesEmpresa) {
                invalidNomesEmpresa = _.reduce($scope.empresa.nomesEmpresa, function(acc, val) {
                    return (val === undefined || val.nome === undefined || val.nome.length === 0) || acc;
                }, false);
            }
            if (!invalidNomesEmpresa) {
                var conts = _.values(_.countBy($scope.empresa.nomesEmpresa, function(va) {
                    return va.nome;
                }));
                haNomesRepetidos = _.filter(conts, function(n) {
                    return n > 1;
                }).length > 0;
            }

            var valid = true;

            if (invalidRazaoSocial === true && invalidNomesEmpresa === true) {
                growl.error(translate('message.error.MSG28_RAZAO_SOCIAL_OU_OUTROS_NOMES_OBRIGATÓRIO'));
                valid = false;
            }

            if ($scope.filledCnpj() && !ValidationService.validateCnpj($scope.empresa.cnpj)) {
                growl.error(translate('message.error.MSG36_CNPJ_INVÁLIDO'));

                valid = false;
            }

            if ($scope.anotherNamesRadio == true && invalidNomesEmpresa) {
                growl.error($filter('translate')('message.error.MSG001_INFORMAR_CAMPOS_OBRIGATORIOS', {
                    field: '&quot;Outros Nomes&quot;'
                }));
                valid = false;
            }
            if (!invalidNomesEmpresa && haNomesRepetidos) {
                growl.error(translate('message.error.MSG21_NOME_DUPLICADA'));
                valid = false;
            }

            var totalComposicaoSocietarias = 0;
            for (var i = 0; i < $scope.empresa.composicaoSocietarias.length; i++) {
                totalComposicaoSocietarias += $scope.empresa.composicaoSocietarias[i].participacao_societaria;
            };

            if (totalComposicaoSocietarias > 100) {
                growl.error(translate('message.error.MSG_PERC_PARTICIPACAO_SOCIETARIA'));
                valid = false;
            }

            if ($scope.empresa.composicaoSocietarias !== undefined) {
                for (var i = $scope.empresa.composicaoSocietarias.length - 1; i >= 0; i--) {
                    // SM quando selecionado um NCM o campo quantidade é obrigatório
                    if (_.isUndefined($scope.empresa.composicaoSocietarias[i].socio) || _.isNull($scope.empresa.composicaoSocietarias[i].socio)
                        || _.isUndefined($scope.empresa.composicaoSocietarias[i].empresa_grupo) || _.isNull($scope.empresa.composicaoSocietarias[i].empresa_grupo)
                        || _.isUndefined($scope.empresa.composicaoSocietarias[i].participacao_societaria) || _.isNull($scope.empresa.composicaoSocietarias[i].participacao_societaria)
                        || _.isUndefined($scope.empresa.composicaoSocietarias[i].cpf_cnpj) || _.isNull($scope.empresa.composicaoSocietarias[i].cpf_cnpj)                        ) {
                        growl.error('message.error.MSG69_DADOS_NAO_INFORMADOS_SECAO');
                        valid = false;
                        break;
                    }
                }
            }

            return valid;
        }

        var preSave = function(isFormValid) {
            if (isFormValid) {
                if (validate() === true) {
                    //definindo os campos empresa internacional e endereço
                    // if (_.isUndefined($scope.origemEmpresa) || _.isUndefined($scope.origemEmpresa.id) || _.isNull($scope.origemEmpresa) || _.isNull($scope.origemEmpresa.id)) {
                    //     $scope.empresa.empresaInternacional = true;
                    //     $scope.empresa.endereco.uf = null;
                    // } else {
                    // $scope.empresa.empresaInternacional = false;

                    // if ($scope.isMineira()) {
                    //     $scope.empresa.endereco.pais = $scope.BRASIL;
                    //     $scope.empresa.endereco.uf = {
                    //         id: MINAS_GERAIS_ID
                    //     };
                    // } else if ($scope.origemEmpresa && typeof $scope.origemEmpresa.id !== 'undefined' && $scope.origemEmpresa.id !== null && $scope.origemEmpresa.id !== 0) {
                    //     $scope.empresa.endereco.uf = {
                    //         id: $scope.origemEmpresa.id
                    //     };
                    // } else if (typeof $scope.empresa.endereco !== 'undefined' &&
                    //     typeof $scope.empresa.endereco.municipio !== 'undefined' &&
                    //     $scope.empresa.endereco.municipio !== null &&
                    //     typeof $scope.empresa.endereco.municipio.id !== 'undefined' &&
                    //     typeof $scope.empresa.endereco.municipio.uf !== 'undefined') {
                    //     $scope.empresa.endereco.uf = $scope.empresa.endereco.municipio.uf;
                    // }

                    // }

                    if ($scope.screenService.isNewScreen()) {
                        $scope.empresa.situacao = SITUACAO_EMPRESA.ativa.id;
                    }

                    //preenchendo o objeto de divisoes do cnae
                    $scope.empresa.divisoesCnae = [];
                    if ($scope.selectedDivisoesCnae.length > 0) {
                        for (var i = 0; i < $scope.selectedDivisoesCnae.length; i++) {
                            $scope.empresa.divisoesCnae.push({
                                cnaeId: $scope.selectedDivisoesCnae[i],
                                empresaId: $scope.empresa.id,
                                nivel: 1
                            });
                        };
                    }

                    //preenchendo o objeto de grupos do cnae
                    $scope.empresa.gruposCnae = [];
                    if ($scope.selectedGruposCnae.length > 0) {
                        for (var i = 0; i < $scope.selectedGruposCnae.length; i++) {
                            $scope.empresa.gruposCnae.push({
                                cnaeId: $scope.selectedGruposCnae[i],
                                empresaId: $scope.empresa.id,
                                nivel: 2
                            });
                        };
                    }

                    //preenchendo o objeto de classes do cnae
                    $scope.empresa.classesCnae = [];
                    if ($scope.selectedClassesCnae.length > 0) {
                        for (var i = 0; i < $scope.selectedClassesCnae.length; i++) {
                            $scope.empresa.classesCnae.push({
                                cnaeId: $scope.selectedClassesCnae[i],
                                empresaId: $scope.empresa.id,
                                nivel: 3
                            });
                        };
                    }

                    //preenchendo o objeto de subclasses do cnae
                    $scope.empresa.subclassesCnae = [];
                    if ($scope.selectedSubclassesCnae.length > 0) {
                        for (var i = 0; i < $scope.selectedSubclassesCnae.length; i++) {
                            $scope.empresa.subclassesCnae.push({
                                cnaeId: $scope.selectedSubclassesCnae[i],
                                empresaId: $scope.empresa.id,
                                nivel: 4
                            });
                        };
                    }


                    return true;
                }
            } else {
                growl.error(translate("message.error.MSGXX_DADOS_INVALIDOS"));
                formSubmitted = true;
                return false;
            }
        }

        // var changeAddressInformation = function(ignoreOrigin) {
        //     delete $scope.empresa.endereco.logradouro;
        //     delete $scope.empresa.endereco.numero;
        //     delete $scope.empresa.endereco.complemento;
        //     delete $scope.empresa.endereco.bairro;
        //     delete $scope.empresa.endereco.cep;
        //     delete $scope.empresa.endereco.uf;
        //     if ($scope.empresa.endereco.municipio) {
        //         delete $scope.empresa.endereco.municipio.id;
        //     }
        //     delete $scope.municipiosEncontrados;

        //     if (!ignoreOrigin) {
        //         delete $scope.origemEmpresa.id;
        //     }
        // }

        /*
         *   Método para verificar se a empresa é brasileira porém não é mineira.
         */
        $scope.isOutrosEstados = function() {
            return $scope.empresa.origemEmpresa === $scope.origemEmpresaEnum.OUTRO_ESTADO.id;
        }


        /*
         *   Método para verificar se a empresa é mineira.
         */
        $scope.isMineira = function() {
            return $scope.empresa.origemEmpresa === $scope.origemEmpresaEnum.MINEIRA.id;
        }

        $scope.isInternacional = function() {
            return $scope.empresa.origemEmpresa === $scope.origemEmpresaEnum.INTERNACIONAL.id;
        }

        /*
         *   Método change criado para limpar os campos ao trocar o radio empresaMineira
         */
        $scope.changeOrigemEmpresa = function() {
            if ($scope.isOutrosEstados()){
                $scope.empresa.paisOrigem = $scope.BRASIL;
                if(_.isNull($scope.empresa.estadoOrigem) || ($scope.empresa.estadoOrigem && $scope.empresa.estadoOrigem.id === MINAS_GERAIS_ID)){
                    $scope.empresa.estadoOrigem = null;
                }
            }else if($scope.isInternacional() && (_.isNull($scope.empresa.paisOrigem) || ($scope.empresa.paisOrigem && $scope.empresa.paisOrigem.id === $scope.BRASIL.id))){
                $scope.empresa.paisOrigem = null;
            }else if ($scope.isMineira()) {
                $scope.empresa.paisOrigem = $scope.BRASIL;
                $scope.empresa.estadoOrigem = {id: MINAS_GERAIS_ID, nome: "MINAS GERAIS"};
            }
        }

        var validateResponsavel = function() {
            // se não foi selecionado nenhum responsavel pelo projeto
            if (!$scope.empresa.responsavel || !$scope.empresa.responsavel.id) {
                delete $scope.empresa.responsavel;
            }
        }

        var validateEmpresa = function(saveFunction) {
            if (!$scope.filledCnpj()) {
                validateResponsavel();
                saveFunction();
                return;
            }

            baseEmpresa.getList({
                "cnpj": $scope.empresa.cnpj
            }).then(function(result) {
                if ($scope.filledCnpj() && result.length > 0) {
                    growl.error(translate('message.error.MSG20_EMPRESA_DUPLICADA'));
                } else {
                    validateResponsavel();
                    saveFunction();
                }
            });
        }

        $scope.validateRegister = function() {
            if ($scope.empresa.situacao == SITUACAO_EMPRESA.pendenteValidacao.id) {
                Restangular.all("empresa/validarCadastro").post($scope.empresa.id).then(function(status) {
                    growl.success("Cadastro validado com sucesso!");
                    $state.go('base.company.companyTpl', {
                        'screen': 'visualizar',
                        'id': $stateParams.id
                    });
                }, function() {
                    growl.error("Ocorreu um erro ao tentar validar o cadastro!");
                });
            } else {
                growl.error("A empresa informada não encontra-se Pendente de Validação!");
            }
        }

        $scope.isEmpresaAtiva = function() {
            return $scope.empresa.situacao == SITUACAO_EMPRESA.ativa.id;
        }

        $scope.isCadastroArquivado = function() {
            return $scope.empresa.situacao === SITUACAO_EMPRESA.cadastroArquivado.id;
        }

        $scope.isExternalUserResponsible = function() {
            return $scope.isExternalUser && $scope.empresa.responsavel !== null && $scope.empresa.responsavel.id === UserSession.getUser().id;
        }

        $scope.isUserAuthorized = function() {
            return $scope.isExternalUserResponsible() || !$scope.isExternalUser;
        }

        $scope.archiveEmpresa = function(isFormValid) {
            if ($scope.isEmpresaAtiva()) {
                growl.error("Não é possivel arquivar empresa ativa!");
                return;
            }

            $scope.save(isFormValid, true);
        }

        function translate(translationKey) {
            var text = "";
            if (typeof translationKey !== 'undefined' && translationKey) {
                text = $filter('translate')(translationKey);
            }
            return text;
        };

        $scope.save = function(isFormValid, archive) {

            if (preSave(isFormValid)) {
                try {
                    if ($scope.screenService.isNewScreen()) {
                        validateEmpresa(function() {
                            if (archive) {
                                $scope.empresa.situacaoAnterior = $scope.empresa.situacao;
                                $scope.empresa.situacao = SITUACAO_EMPRESA.cadastroArquivado.id;
                            }
                            baseEmpresa.post($scope.empresa).then(function(id) {
                                growl.success(translate("message.success.MSG006_REGISTRO_GRAVADO_SUCESSO"));
                                $scope.companyForm.$setPristine();
                                $state.go('base.company.companyTpl', {
                                    'screen': 'editar',
                                    'id': id
                                });
                            }, function() {
                                $scope.empresa.situacao = $scope.empresa.situacaoAnterior;
                                growl.error("Ocorreu um erro ao tentar gravar o registro!");
                            });
                        });
                    } else {
                        baseEmpresa.getList({
                            "cnpj": $scope.empresa.cnpj,
                            "situacao": $scope.empresa.situacao,
                            "nomeEmpresa": $scope.empresa.nomePrincipal
                        }).then(function(result) {
                            if ($scope.filledCnpj() && result.length > 0 && _.find(result, function(empresa) {
                                    return empresa.id !== $scope.empresa.id && empresa.cnpj === $scope.empresa.cnpj
                                })) {
                                growl.error(translate('message.error.MSG20_EMPRESA_DUPLICADA'));
                            } else {
                                if (archive) {
                                    $scope.empresa.situacaoAnterior = $scope.empresa.situacao;
                                    $scope.empresa.situacao = SITUACAO_EMPRESA.cadastroArquivado.id;
                                }
                                $scope.empresa.put().then(function() {
                                    growl.success(translate("message.success.MSG006_REGISTRO_GRAVADO_SUCESSO"));
                                    $scope.companyForm.$setPristine();
                                    if ($scope.isCadastroArquivado()) {
                                        setTimeout(function() {
                                            $state.go('base.company.companyTpl', {
                                                'screen': 'visualizar',
                                                'id': $stateParams.id
                                            });
                                        }, 2000);
                                    } else {
                                        setTimeout(function() {
                                            $state.transitionTo($state.current, $stateParams, {
                                                reload: true,
                                                inherit: true,
                                                notify: true
                                            });
                                        }, 5000);
                                    }

                                }, function() {
                                    $scope.empresa.situacao = $scope.empresa.situacaoAnterior;
                                    growl.error("Ocorreu um erro ao tentar gravar o registro!");
                                });
                            }
                        });
                    }
                } catch (err) {
                    growl.error("Erro: " + err.message);
                }
            }
        }

    });
