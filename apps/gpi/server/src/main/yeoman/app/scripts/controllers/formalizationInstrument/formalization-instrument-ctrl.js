'use strict';

angular.module('gpifrontApp')
    .controller('FormalizationInstrumentCtrl', function($scope, $state, ScreenService, growl, Restangular, $stateParams, SITUACAO_INSTRUMENTO_FORMALIZACAO, AUTH_ROLES, AuthService) {
        $scope.screenType = {
            INCLUIR: {
                translationKey: "formalizationInstrument.new",
                icon: "fa-plus"
            },
            EDITAR: {
                translationKey: "formalizationInstrument.edit",
                icon: "fa-edit"
            },
            VISUALIZAR: {
                translationKey: "formalizationInstrument.show",
                icon: "fa-eye"
            }
        };

        if (!$state.params.screen || $state.params.screen === "") {
            $state.params.screen = "incluir";
        }
        if ($state.params.screen.toUpperCase() === "INCLUIR") {
            $state.params.id = null;
        }

        $scope.screenService = ScreenService;

        $scope.getId = function() {
            return $state.params.id;
        };

        $scope.disableOnShow = function($event) {
            $event.preventDefault();
            $event.stopPropagation();
            return false;
        }

        $scope.isPromocaoOuGerente = function () {
            return AuthService.isUserAuthorized(AUTH_ROLES.manager)
                || AuthService.isUserAuthorized(AUTH_ROLES.promotionAnalist);
        }

        $scope.isGerente = function () {
            return AuthService.isUserAuthorized(AUTH_ROLES.manager);
        }

        $scope.notAllowed = function() {
            if ($scope.screenService.isNewScreen()) {
                growl.warning("Não é permitido acessar essa aba em modo de inclusão!");
            }
        }
    });
