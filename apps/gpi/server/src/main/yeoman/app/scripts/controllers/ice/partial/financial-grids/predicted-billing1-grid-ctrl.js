'use strict'

angular
	.module('gpifrontApp')
	.controller('PredictedBilling1GridCtrl', function($scope, Restangular, GridButton, growl, $filter){

        $scope.faturamentoPrevisto1 = $scope.$parent.faturamentoPrevisto1;            
        $scope.faturamentoPrevisto1SelectedItems = $scope.$parent.faturamentoPrevisto1SelectedItems;

		$scope.faturamentoPrevisto1ColumnDefs = [{
            field: "ano",
            enableCellEdit: true,
            width: "**",            
            translationKey: "common.achievement.year",
            editableCellTemplate: '<input type="numeric" ui-mask="9999" maxlength="4" ng-class="\'colt\' + col.index"  ng-input="COL_FIELD" ng-model="COL_FIELD"/>'                        
        }, {
            field: "valorIndustrializados",
            enableCellEdit: true,
            cellFilter: "currency:''",
            editableCellTemplate: '<input type="text" format="currency" maxlength="21" ng-class="\'colt\' + col.index"  ng-input="COL_FIELD" ng-model="COL_FIELD"/>',                                                            
            width: "**",            
            translationKey: "financial.industrialized.goods"         
        }, {
            field: "valorAdquiridosComercializacao",
            enableCellEdit: true,
            cellFilter: "currency:''",
            editableCellTemplate: '<input type="text" format="currency" maxlength="21" ng-class="\'colt\' + col.index"  ng-input="COL_FIELD" ng-model="COL_FIELD"/>',                                                
            width: "**",            
            translationKey: "financial.bought.sales"         
        }];

        $scope.faturamentoPrevisto1ButtonsConfig = {            
            newButton: new GridButton(true, 'common.new', false, null, null, ''),
            deleteButton: new GridButton(true, 'common.delete', false, null, deleteFn, '')
        }


        //define its sort-info attribute if you desire a default-sort
        $scope.faturamentoPrevisto1SortInfo = {
            fields: ['ano'],
            directions: ['asc']
        };

        function deleteFn() {
            growl.info($filter('translate')('message.error.MSG38_SECAO_ATUALIZADA', {
                value: "Faturamento Previsto"
            }));                        
        }              		

        $scope.$parent.$watch('faturamentoPrevisto1', function(newVal, oldVal){
         	if ($scope.$parent.faturamentoPrevisto1 != null) {
                setTimeout(function(){
                    $scope.faturamentoPrevisto1 = newVal;   
                }, true);
         	}                
        }, true);

	});


