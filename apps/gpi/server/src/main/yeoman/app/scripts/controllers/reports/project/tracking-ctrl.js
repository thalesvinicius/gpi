'use strict'
angular.module('gpifrontApp')
    .controller('ReportTrackingCtrl', function($scope, growl, ESTAGIO_PROJETO, AuthService, AUTH_ROLES, Restangular, $translate, $http,
        $filter, configuration, $window, ngTableParams, $rootScope, UserSession) {

        $scope.loggedUser = UserSession.getUser();

        /*
         *   Recupera todos os ids da lista passada
         */
        var getIdList = function(list) {
            var idList = [];
            if (_.isArray(list)) {
                _.each(list, function(val, key) {
                    idList.push(val.id);
                });
            } else {
                idList.push(list.id);
            }
            return idList;
        }

        // RNE163 se for gerente não exibe o campo Gerência e se for analista de portfolio
        //  não exibe os campos Gerência e Analista
        $scope.isGerente = function() {
            return AuthService.isUserAuthorized([AUTH_ROLES.manager]);
        }

        var initialize = function() {
            $scope.tableParams = new ngTableParams({
                page: 1,
                // count: 0,
            }, {
                counts: []
            }, {
                total: 10000000
            });

            $scope.resetFilters();
        }

        $scope.resetFilters = function() {
            $scope.estagios = [ESTAGIO_PROJETO.FORMALIZADA, ESTAGIO_PROJETO.IMPLANTACAO_INICIADO, ESTAGIO_PROJETO.OPERACAO_INICIADA];
            $scope.filtro = {
                versao: true,
                notShowRealized: true
            };

            Restangular.all('domain/regiaoPlanejamento').getList().then(function(regioesPlanejamento) {
                $scope.regioesPlanejamento = regioesPlanejamento;
                //  Por default todas as regioesPlanejamento vem selecionadas
                $scope.filtro.regioesPlanejamento = getIdList(regioesPlanejamento);
            });

            // RNE163 se for gerente não exibe o campo Gerência e se for analista de portfolio
            //  não exibe os campos Gerência e Analista
            if($scope.isGerente() || $scope.isPromocao()){
                Restangular.one('user/' + $scope.loggedUser.id).get().then(function(user) {
                    $scope.filtro.departamentos = getIdList(user.departamento);
                    //  Aciona o evento onChange do select de departamentos
                    //  para preencher os analistas e as cadeias produtivas
                    $scope.changeManageMultiSelect();
                });
            } else {
                Restangular.all('domain/departamento').getList().then(function(departamentos) {
                    $scope.gerencias = departamentos;
                    //  Por default todas as gerencias vem selecionadas
                    $scope.filtro.departamentos = getIdList(departamentos);
                    $scope.changeManageMultiSelect();
                });
            }

            $scope.dataDF = [];
            $scope.dataII = [];
            $scope.dataOI = [];
        }

        function translate(translationKey) {
            var text = " ";
            if (typeof translationKey !== 'undefined' && translationKey) {
                text = $filter('translate')(translationKey);
            }
            return text;
        }

        //  Configurações do datepicker
        $scope.configsDatepicker = {
            disabled: false,
            viewMode : 'months',
            restangularUpdated: false
        }

        $scope.clean = function() {
            $scope.resetFilters();
        }

        $scope.export = function(type) {
            var mimetype = type == 'pdf' ? 'application/pdf' : 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
            $http({
                method: 'GET',
                url: configuration.localPath + 'api/relatorio/agendaPositiva/gerar/' + type,
                params: {
                    'departamentos': $scope.filtro.departamentos,
                    'regioesPlanejamento': $scope.filtro.regioesPlanejamento,
                    'dataIni': $scope.filtro.dataIni,
                    'dataFim': $scope.filtro.dataFim,
                    'cadeiasProdutivas': $scope.filtro.cadeiasProdutivas,
                    'estagios': $scope.filtro.estagios,
                    'usuariosResponsaveis': $scope.filtro.usuariosResponsaveis,
                    'versao': $scope.filtro.versao,
                    'diretoria': $scope.filtro.diretoria,
                    'todosEstagios': _.map($scope.estagios,function(estagio){return estagio.id})
                    
                    
                }, headers: {
                    Accept: mimetype,
                    Authorization: 'Bearer ' + $scope.loggedUser.accessToken,
                    'todasGerencias': getIdList($scope.gerencias),
                    'todasCadeiasProdutivas' :getIdList($scope.cadeiasProdutivas),
                    'todasRegioesPlanejamento': getIdList($scope.regioesPlanejamento),
                    'todosAnalistas': getIdList($scope.analistas)
                },
                responseType: 'arraybuffer',

            }).
            success(function(response) {
                var a = document.createElement("a");
                document.body.appendChild(a);
                a.style = "display: none";
                var blob = new Blob([response], {
                    type: "octet/stream"
                });
                var url = window.URL.createObjectURL(blob);
                a.href = url;
                a.download = "RelatorioAgendaPositiva" + "." + type;
                a.click();
                window.URL.revokeObjectURL(url);

                // var file = new Blob([response], {
                //     type: mimetype
                // });
                // var fileURL = URL.createObjectURL(file);
                // $window.location = fileURL;
            }).
            error(function(data, status, headers, config) {
                growl.error("Não foi possível realizar a impressão do Relatório de Acompanhamento. Tente novamente e se o erro persistir favor contatar o administrador do sistema.")
            });
        }

        $scope.disabledExport = function() {
            return ((!$scope.dataDF || $scope.dataDF.length <= 2) &&
                (!$scope.dataII || $scope.dataII.length <= 2) &&
                (!$scope.dataOI || $scope.dataOI.length <= 2));
        }

        $scope.search = function() {
            searchWithFilters($scope.filtro);
        };

        var searchWithFilters = function(filtro) {
            Restangular.all('relatorio/agendaPositiva/agendaPositivaTOByFilters').getList(filtro).then(function(result) {
                $scope.dataDF = result[0];
                $scope.dataII = result[1];
                $scope.dataOI = result[2];

                if((!$scope.dataDF || $scope.dataDF.length <= 2) &&
                (!$scope.dataII || $scope.dataII.length <= 2) &&
                (!$scope.dataOI || $scope.dataOI.length <= 2)){
                    growl.error(translate('message.error.MSG004_NENHUM_REGISTRO_ENCONTRADO'));
                }
            }, function errorCallback(response) {
                if (response.status !== 404) {
                    growl.error("Error: The server returned status " + response.status);
                }
            });
        }

        // RNE163 se for gerente não exibe o campo Gerência e se for analista de portfolio
        //  não exibe os campos Gerência e Analista
        $scope.isPromocao = function() {
            return AuthService.isUserAuthorized([AUTH_ROLES.promotionAnalist]);
        }

        /*
         *   Evento chamado ao selecionar gerência
         */
        $scope.changeManageMultiSelect = function() {
            //  Busca os usuários com base no departamento
            if ($scope.filtro.departamentos && $scope.filtro.departamentos.length > 0) {
                Restangular.all('user/byDepartamentos').getList({
                    "departamentos": $scope.filtro.departamentos
                }).then(function(analistas) {
                    $scope.analistas = analistas;
                    //  Por default todos os analistas vem selecionados
                    $scope.filtro.usuariosResponsavel = $scope.isPromocao() ? [UserSession.getUser().id] : getIdList(analistas);
                });

                Restangular.all('cadeiaProdutiva/byDepartamentos').getList({
                    "departamentos": $scope.filtro.departamentos
                }).then(function(cadeiasProdutivas) {
                    $scope.cadeiasProdutivas = cadeiasProdutivas;
                    //  Por default todas as cadeiasProdutivas vem selecionadas
                    $scope.filtro.cadeiasProdutivas = getIdList(cadeiasProdutivas);
                });
            }
        }

        initialize();
    });
