'use strict';

angular.module('gpifrontApp')
	.controller('ReportAnnualMonitoringGridCtrl', function($scope, GridButton, AuthService, AUTH_ROLES, UserSession){
        var loggedUser = UserSession.getUser();

        $scope.gridScope = {
            
        }

        $scope.relatorioAnualAcompanhamentoList = $scope.$parent.relatorioAnualAcompanhamentoList;
        $scope.relatorioAnualAcompanhamentoSelectedItems = $scope.$parent.relatorioAnualAcompanhamentoSelectedItems;

        $scope.relatorioAnualAcompanhamentoColumnDefs = [{
            field: 'nomeEmpresa',
            enableCellEdit: false,
            width: "300",
            translationKey: 'common.company'
        }, {
            field: 'nomeProjeto',
            enableCellEdit: false,
            width: "300",
            translationKey: 'common.project',
        }, {
            field: 'emailResponsavelAreaInvestidor',
            enableCellEdit: false,
            width: "250",
            translationKey: 'common.email.investor.area.responsible',
        }, {
            field: 'situacaoRelatorioEnum',
            enableCellEdit: false,
            width: "220",
            translationKey: 'reportAnnualMonitoring.reportSituation',
        }, {
            field: 'descricaoEstagioAtual',
            enableCellEdit: false,
            width: "150",
            translationKey: 'common.stage'
        }, {
            field: 'nomeUsuarioInterno',
            enableCellEdit: false,
            width: "300",
            translationKey: 'common.analyst'
        }, {
            field: 'dataValidacao',
            enableCellEdit: false,
            width: "120",
            translationKey: 'common.validationDate',
            cellFilter: 'dateTransformFilter'
        }, {
            field: 'dataUltimaAtualizacao',
            enableCellEdit: false,
            width: "170",
            translationKey: 'reportAnnualMonitoring.updated',
            cellFilter: 'dateTransformFilter'
        }, {
            field: 'periodoPrimeiraCampanha',
            enableCellEdit: false,
            width: "180",
            translationKey: 'reportAnnualMonitoring.firstCampaign'
        }, {
            field: 'periodoSegundaCampanha',
            enableCellEdit: false,
            width: "180",
            translationKey: 'reportAnnualMonitoring.secondCampaign'
        }, {
            field: 'enviarTerceiraCampanha',
            enableCellEdit: false,
            width: "200",
            translationKey: 'reportAnnualMonitoring.sendLetter'
        }, {
            field: 'show',
            translationKey: 'common.show',
            showScreenRedirect: true,
            width: "90",
            showLink: '/projeto/relatorio/acompanhamentoAnual/visualizar'
        }];

        function disableEditFn() {
            return (($scope.relatorioAnualAcompanhamentoSelectedItems.length !== 1) || 
                (AuthService.isUserAuthorized([AUTH_ROLES.portfolioAnalist, AUTH_ROLES.informationAnalist, AUTH_ROLES.director, AUTH_ROLES.generalSecretary]) 
                    && loggedUser.id !== $scope.relatorioAnualAcompanhamentoSelectedItems[0].usuario)
                && $scope.relatorioAnualAcompanhamentoSelectedItems[0].situacaoAtual  === 'NAO_SOLICITADO');
        }

        $scope.relatorioAnualAcompanhamentoButtonsConfig = {
            newButton: new GridButton(false, 'contact.new', true, null, null, ''),
            deleteButton: new GridButton(false, 'common.delete', false, null, null, ''),
            editButton: new GridButton(true, 'common.edit', true, disableEditFn, null, '/projeto/relatorio/acompanhamentoAnual/editar'),
        }

        $scope.$parent.$watch('relatorioAnualAcompanhamentoList', function(newVal, oldVal){
            if ($scope.$parent.relatorioAnualAcompanhamentoList != null) {
                setTimeout(function(){
                    $scope.relatorioAnualAcompanhamentoList = newVal;
                }, true);
            }
        }, true);

	});
