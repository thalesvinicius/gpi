'use strict';

angular.module('gpifrontApp')
    .controller('InputsAndProductsMainInputsGridCtrl', function($scope, Restangular, $filter, $rootScope, GridButton, INSUMO_PRODUTO_ORIGEM, growl) {

        $scope.insumos = $scope.$parent.insumoProduto.insumos;

        $scope.selectedItems = [];

        // $scope.gridScope = {
        //     ncmList: [],
        //     insumoProdutoOrigem: INSUMO_PRODUTO_ORIGEM,
        //     ncmProperties: {}
        // };

        // $scope.$parent.$watch('ncmList', function(newVal, oldVal) {
        //     if (newVal !== oldVal) {
        //         $scope.gridScope.ncmList = newVal;
        //         // setTimeout(function() {
        //             $scope.gridScope.insumoProdutoOrigem = INSUMO_PRODUTO_ORIGEM;
        //         // }, true);
        //     }
        // }, true);
        // // $scope.gridScope.ncmProperties = NCM_CONSTANT;
        // $scope.$parent.$watch('ncmProperties', function(newVal, oldVal) {
        //     if (newVal !== oldVal) {
        //         // $scope.gridScope.ncmProperties = newVal;
        //         // atribuindo aqui porque neste momento o gridScope já foi preenchido
        //     }
        // }, true);

        $scope.ncmCellTemplate = '<div class=\"ngCellText\" ng-class=\"col.colIndex()\"><span ng-cell-text>{{row.entity.ncm.id}}</span></div>';
        $scope.origemCellTemplate = '<div class=\"ngCellText\" ng-class=\"col.colIndex()\"><span ng-cell-text>{{insumoProdutoOrigem[row.entity.origem].descricao}}</span></div>';
        $scope.cellNCMSelectEditableTemplate = '<select placeholder="{{\'common.ncm\' | translate}}" ng-class="\'colt\' + col.index" ng-input="COL_FIELD" ng-model="row.entity.ncm" ng-options="ncm as ncm.id for ncm in ncmList" ng-change="row.entity.nome = row.entity.ncm.descricao"><option value="" translate>common.select</option></select>';
        $scope.cellOrigemSelectEditableTemplate = '<select placeholder="{{\'common.origin\' | translate}}" ng-class="\'colt\' + col.index" ng-input="COL_FIELD" ng-model="row.entity.origem" ng-options="origem.id as origem.descricao for origem in insumoProdutoOrigem.todas"><option value="" translate>common.select</option></select>';

        $scope.columnDefs = [{
            field: 'ncm.id',
            enableCellEdit: true,
            width: "**",
            translationKey: 'common.ncm',
            editableCellTemplate: $scope.cellNCMSelectEditableTemplate,
            cellTemplate: $scope.ncmCellTemplate
        }, {
            field: 'nome',
            enableCellEdit: true,
            width: "**",
            translationKey: 'common.inputs',
            editableCellTemplate: '<input type="text" placeholder="{{\'common.inputs\' | translate}}" ng-class="\'colt\' + col.index"  ng-input="COL_FIELD" ng-model="COL_FIELD" maxlength="200"/>'

        }, {
            field: 'origem',
            enableCellEdit: true,
            width: "*",
            translationKey: 'common.origin',
            editableCellTemplate: $scope.cellOrigemSelectEditableTemplate,
            cellTemplate: $scope.origemCellTemplate
            // cellFilter: "mapStatus:insumoProdutoOrigem"
        }, {
            field: 'quantidadeAno',
            enableCellEdit: true,
            width: "*",
            translationKey: 'inputsandproducts.quantity.per.year',
            editableCellTemplate: '<input type="numeric" placeholder="{{\'inputsandproducts.quantity.per.year\' | translate}}" ng-class="\'colt\' + col.index" maxlength="18" ng-input="COL_FIELD" ng-model="COL_FIELD"/>'

        }, {
            field: 'valorEstimadoAno',
            enableCellEdit: true,
            width: "**",
            translationKey: 'inputsandproducts.estimated.value.per.year',
            editableCellTemplate: '<input type="text" format="currency" placeholder="{{\'inputsandproducts.estimated.value.per.year\' | translate}}" ng-class="\'colt\' + col.index" maxlength="14" ng-input="COL_FIELD" ng-model="COL_FIELD" />',
            cellFilter: "currency:$"

        }, {
            field: 'unidadeMedida',
            enableCellEdit: true,
            width: "**",
            translationKey: 'inputsandproducts.measurement.unit',
            editableCellTemplate: '<input type="text" placeholder="{{\'inputsandproducts.measurement.unit\' | translate}}" ng-class="\'colt\' + col.index" maxlength="10" ng-input="COL_FIELD" ng-model="COL_FIELD" />'
        }, {
            field: 'empresaFornecedor',
            enableCellEdit: true,
            width: "**",
            translationKey: 'inputsandproducts.companies.suppliers',
            editableCellTemplate: '<input type="text" placeholder="{{\'inputsandproducts.companies.suppliers\' | translate}}" ng-class="\'colt\' + col.index" maxlength="70" ng-input="COL_FIELD" ng-model="COL_FIELD" />'
        }];


        $scope.buttonsConfig = {
            //function GridButton(show, translationKey, searchMode, disable, action, screenLink)
            newButton: new GridButton(true, 'common.new', false, null, null, ''),
            deleteButton: new GridButton(true, 'common.delete', false, null, deleteFn, '')
        }

        function deleteFn() {
            growl.info($filter('translate')('message.error.MSG38_SECAO_ATUALIZADA', {
                value: "Principais Insumos"
            }));
        }

        $scope.sortInfo = {
            fields: ['nome'],
            directions: ['asc']
        };

        $scope.$parent.$watch('insumoProduto', function(newVal, oldVal) {
            if (newVal.insumos !== oldVal.insumos) {
                $scope.insumos = newVal.insumos;
            }
        }, true);

    }).directive('ngBlur', function() {
        return function(scope, elem, attrs) {
            elem.bind('blur', function() {
                scope.$apply(attrs.ngBlur);
            });
        };
    })

// .filter('mapStatus', function($filter) {
//     return function(input, object) {
//         // if (!angular.isUndefined(object) && object) {
//         //     if (object instanceof Array) {
//         //         for (var i = 0; i < object.length; i++) {
//         //             if (object[i].id === input) {
//         //                 return object[i].descricao;
//         //             }
//         //         };
//         //     } else if(object[input]) {
//         //         return object[input].descricao;
//         //     }
//         //     return '';
//         // } else if(typeof input === 'boolean' || input === "true" || input === "false") {
//         //     return (input === "true" || input === true) ? $filter('translate')('common.yes') : $filter('translate')('common.no');
//         // } else {
//             return '';
//         // }
//     };
// });
