/*  NG-DATEPICKER-CUSTOM

    @author: marcoabc

    Example of usage:

    ***********HTML**************
    <ng-datepicker-custom date="projeto.dataRegistro"
    configs="configsDatepicker"></ng-datepicker-custom>

    **********Javascript*********
    // The configs array can contain the 'required' attribute wich will be add to your form $valid
    // viewMode if you want to choose days or month, restangularUpdated if the date must be
    //  updated from a Restangular call

    $scope.configsDatepicker = {
        required: true,
        viewMode: viewMode,
        restangularUpdated : true
    };

    $scope.projeto.dataRegistro = date;

*/

angular.module('gpifrontApp').directive('datepickerCustom', function($rootScope, configuration) {
    return {
        restrict: 'E',
        templateUrl: configuration.rootDir + 'scripts/directives/datepicker-custom/datepicker-custom.html',
        scope: {
            configs: "=",
            date: "=",
            formSubmitted: "=",
            onChangeMethod: "="
        },

        link: function(scope, iElement, attrs) {
            //Watch criado devido a alteracoes a partir do restangular
            scope.$watch('configs.restangularUpdated', function(newVal, oldVal) {
                if (scope.date) {
                    dateChanged();

                }
            }, true);

            // Atualiza a data
            scope.$watch('formattedDate', function(newVal, oldVal) {
                if (oldVal !== newVal) {
                    if (newVal) {
                        scope.date = unformatDate(scope.formattedDate);
                    }
                    setTimeout(function() {
                        if (!_.isUndefined(scope.onChangeMethod) && !_.isNull(scope.onChangeMethod)) {
                            scope.onChangeMethod();
                        }
                    }, true);
                }
            }, true);

            var dateChanged = function() {
                if (scope.date instanceof Array) { // formato [2014,1,30]
                    scope.date = new Date(scope.date);
                } else if (scope.date.toString().split('-').length === 3) { // formato 2014-1-30
                    var dateAux = scope.date.toString().split('-');
                    scope.date = new Date(dateAux[0], dateAux[1] - 1, dateAux[2]);
                } else { //  se for milliseconds ou Date
                    scope.date = new Date(scope.date);
                }

                scope.formattedDate = formatDate(scope.date);
            }

            var viewMode = scope.configs.viewMode;

            scope.placeholder = viewMode == 'months' ? 'mm/yyyy' : 'dd/mm/yyyy';

            var formatDate = function(date) {
                var day = date.getDate();
                var month = date.getMonth() + 1; //Months are zero based
                var year = date.getFullYear();

                if (viewMode == 'months') { // Formato mm/yyyy
                    return ('0' + month).slice(-2) + "/" + year;
                } else { // Formato dd/mm/yyyy
                    return ('0' + day).slice(-2) + "/" + ('0' + month).slice(-2) + "/" + year;
                }
            };

            var unformatDate = function(date) {
                date = date.split("/");

                //Months are zero based
                if (viewMode == 'months') { // Formato mm/yyyy
                    return new Date(parseInt(date[1]), parseInt(date[0] - 1)).getTime();
                } else { // Formato dd/mm/yyyy
                    return new Date(parseInt(date[2]), parseInt(date[1] - 1), parseInt(date[0])).getTime();
                }
            };

            scope.instanciateDatepicker = function() {
                // TODO ver porque não está puxando o idioma sozinho
                $.fn.datepicker.dates['pt-BR'] = {
                    days: ["Domingo", "Segunda", "Terça", "Quarta", "Quinta", "Sexta", "Sábado", "Domingo"],
                    daysShort: ["Dom", "Seg", "Ter", "Qua", "Qui", "Sex", "Sáb", "Dom"],
                    daysMin: ["Do", "Se", "Te", "Qu", "Qu", "Se", "Sa", "Do"],
                    months: ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"],
                    monthsShort: ["Jan", "Fev", "Mar", "Abr", "Mai", "Jun", "Jul", "Ago", "Set", "Out", "Nov", "Dez"],
                    today: "Hoje"
                };

                if (!scope.configs.disabled) {
                    iElement.find('#dateInput').datepicker({
                        // format: $filter('translate')(scope.configs.formatPath),
                        // language: $filter('translate')('common.datepicker.language'),
                        format: viewMode === 'months' ? 'mm/yyyy' : 'dd/mm/yyyy',
                        language: "pt-BR",
                        todayHighlight: true,
                        autoclose: true,
                        minViewMode: viewMode
                    });
                }
            };

            //  Limpa a data selecionada
            scope.clearDate = function() {
                if (!scope.configs.disabled) {
                    scope.date = null;
                    scope.formattedDate = null;
                }
            };

            // Mostra o Datepicker
            scope.showDatepicker = function() {
                if (!scope.configs.disabled) {
                    iElement.find('#dateInput').datepicker('show');
                }
            };

            // Verifica se o campo está válido
            scope.checkInvalidDate = function() {
                return scope.configs.required && iElement.find('input.ng-invalid').length > 0 && scope.formSubmitted;
            };

            scope.$watch('configs.disabled', function(newVal, oldVal) {
                if (newVal !== oldVal) {
                    iElement.find('#dateInput').datepicker({
                        // format: $filter('translate')(scope.configs.formatPath),
                        // language: $filter('translate')('common.datepicker.language'),
                        format: viewMode === 'months' ? 'mm/yyyy' : 'dd/mm/yyyy',
                        language: "pt-BR",
                        todayHighlight: true,
                        autoclose: true,
                        minViewMode: viewMode
                    });
                }
            });

            scope.instanciateDatepicker();

            // scope.removeDatepicker = function() {
            //     $('.datepickerClass').datepicker('remove');
            // }

            // $rootScope.$on('$translateChangeEnd', function() {
            //     scope.removeDatepicker();
            //     scope.instanciateDatepicker();
            // });

        }
    }
});
