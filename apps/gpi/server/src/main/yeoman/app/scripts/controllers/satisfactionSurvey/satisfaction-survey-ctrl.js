'use strict';

angular.module('gpifrontApp')
    .controller('SatisfactionSurveyCtrl', function($scope, ScreenService, TIPO_PERGUNTA, UserSession, Restangular, $stateParams, $state, growl, $filter) {
        $scope.anexosPesquisaSatisfacao = [];
        $scope.tipoPergunta = TIPO_PERGUNTA;

        var baseInstrumentoFormalizacao = Restangular.all("instrumento");

        $scope.pesquisa = {
            empresa :{}
        };
        $scope.projetos = [];
        $scope.instrumento = {};


        var serviceUrl = ""
        if ($state.current.name !== undefined && $state.current.name.indexOf("formalizationInstrument") !== -1) {
            serviceUrl = "byInstrumentoId"
        } else {
            serviceUrl = "byId"
        }

        baseInstrumentoFormalizacao.one("pesquisa").one(serviceUrl).one($stateParams.id).get().then(function(pesquisa) {
            $scope.pesquisa = pesquisa;            
            $scope.instrumento = $scope.pesquisa.instrumentoFormalizacao;
            $scope.pesquisa.empresa = _.where($scope.instrumento.empresasSelecionadas, {empresaId: UserSession.getUser().empresaId})[0];
            $scope.anexosPesquisaSatisfacao = $scope.pesquisa.anexos;
        });

        $scope.screenType = {
            EDITAR: {
                translationKey: "satisfactionSurvey.new",
                icon: "fa-edit"
            },
            VISUALIZAR: {
                translationKey: "satisfactionSurvey.show",
                icon: "fa-eye"
            }
        };

        $scope.screenService = ScreenService;

        $scope.cancelar = function(formUser) {
            if (UserSession.isExternalUser()) {
                $state.go('base.satisfactionSurveySearch');
            } else {
                $scope.screenService.cancelar('base.userSearch', formUser);
            }
        }

        $scope.send = function(isFormvalid) {
            if (!isFormvalid) {
                growl.error($filter('translate')("message.error.MSGXX_DADOS_INVALIDOS"));
                return false;
            }

            if ($scope.anexosPesquisaSatisfacao) {
                $scope.pesquisa.anexos = $scope.anexosPesquisaSatisfacao;
            }

            Restangular.all("instrumento/pesquisa").post($scope.pesquisa).then(function() {
                growl.success("Pesquisa enviada com sucesso!");

                $scope.formSatisfactionSurvey.$setPristine();
                // redirecionar
            }, function() {
                growl.error("Ocorreu um erro ao tentar enviar a pesquisa!");
                return false;
            });
        }

    })
    .directive('surveyPanelDir', function() {
        return {
            restrict: 'E',
            scope: {
                pesquisa: '=pesquisa',
                tipoPergunta: '=tipoPergunta'
            },
            template: "<div ng-repeat='p in pesquisa.pesquisaPerguntaPesquisaList'>" +
                "<div class='row'>" +
                "{{p.perguntaPesquisa.pergunta}}" +
                "</div>" +
                "<label class='radio' ng-repeat='resposta in tipoPergunta[p.perguntaPesquisa.tipo]'>" +
                "<input type='hidden' ng-model='p.pesquisa'>" +
                "<input type='radio' ng-value='resposta.id' ng-model='p.resposta' required>" +
                "<span class='lbl'>{{resposta.descricao}}</span>" +
                "</label>" +
                "<div class='form-group no-margin-hr'>" +
                "<label class='control-label'>Justifique</label>" +
                "<textarea class='form-control' ng-model='p.justificativa'></textarea>" +
                "</div>" +
                "</div>"
        };

    });
