'use strict';
angular.module('gpifrontApp')
    .controller('IceJobsCtrl', function($scope, Restangular, dialogs, $filter, $stateParams, UserSession, TIPO_EMPREGO, $state, growl,
        CADEIA_PRODUTIVA, ScreenService, SITUACAO_PROJETO, CHANGE_EMPREGO, AUTH_ROLES, AuthService) {
        //--------------------------------------------------- Início Configurações inicial da tela

        $scope.changes = [];
        $scope.change_emprego = CHANGE_EMPREGO;

        /*
        *   Carrega dados iniciais da tela
        */
        $scope.screenService = ScreenService;
        var initialize = function() {

            // Constante referente ao ENUM TipoEmprego
            $scope.tipoEmprego = TIPO_EMPREGO;

            $scope.cadeiaProdutiva = CADEIA_PRODUTIVA;

            // Recupera o projeto do servidor TODO RECUPERAR DO PAI
            Restangular.one("projeto", $stateParams.id).get().then(function(serverProject) {
                $scope.projeto = serverProject;
                $scope.getEmprego();

                if (!$scope.externalUser() && $scope.projeto.situacaoAtual.situacao === SITUACAO_PROJETO.PENDENTE_ACEITE.id) {
                    Restangular.one("projeto", $stateParams.id).getList("emprego/findChange").then(function(result) {
                        if (result[0] != undefined) {
                            for (var i = 0; i < result.length; i++) {
                                if ($scope.changes[result[i][0]] === undefined) {
                                    $scope.changes[result[i][0]] = (result[i][1] || result[i][2] || result[i][3] || result[i][4]);
                                }
                            };                            
                        }
                    }, function() {
                        growl.error("Ocorreu um erro ao tentar buscar o registro!");
                    });
                }                
            });
        };
        //------------------------------------------------------ Fim Configurações inicial da tela

        $scope.externalUser = function() {
            return AuthService.isUserAuthorized([AUTH_ROLES.externalResponsible, AUTH_ROLES.externalMeling, AUTH_ROLES.externalOther]);
        }

        /*
        *   Recupera o emprego de acordo com o id do projeto e preenche as tabelas
        */
        $scope.getEmprego = function(){
            $scope.idCadeiaProdutiva = $scope.projeto.cadeiaProdutiva.id;

            if ($scope.isNotSucroenergeticoEnergiasHidricasMineracaoFerroSiderurgia()) {
                getEmpregosByTipoEmprego($scope.tipoEmprego.TEMPORARIO.id);
                getEmpregosByTipoEmprego($scope.tipoEmprego.PERMANENTE.id);
            }
            if ($scope.isSucroenergetico()) {
                getEmpregosEmpregoAgricolaIndustria('TEMPORARIO');
                getEmpregosEmpregoAgricolaIndustria('PERMANENTE');
            }
            if ($scope.isEnergiaHidricaMineracaoFerroSiderurgia()) {
                getEmpregosByTipoEmprego($scope.tipoEmprego.TEMPORARIO_OBRA.id);
                getEmpregosByTipoEmprego($scope.tipoEmprego.PERMANENTE_OPERACAO.id);
            }
            $(window).trigger('resize');
        };

        $scope.isNotSucroenergeticoEnergiasHidricasMineracaoFerroSiderurgia = function(){
           return $scope.idCadeiaProdutiva !== $scope.cadeiaProdutiva.SUCROENERGETICO.id &&
                $scope.idCadeiaProdutiva !== $scope.cadeiaProdutiva.ENERGIAS_HIDRICAS.id &&
                    $scope.idCadeiaProdutiva !== $scope.cadeiaProdutiva.MINERACAO_FERRO.id &&
                        $scope.idCadeiaProdutiva !== $scope.cadeiaProdutiva.SIDERURGIA.id;
        }

        $scope.isSucroenergetico = function(){
           return $scope.idCadeiaProdutiva === $scope.cadeiaProdutiva.SUCROENERGETICO.id;
        }

        $scope.isEnergiaHidricaMineracaoFerroSiderurgia = function(){
           return $scope.idCadeiaProdutiva === $scope.cadeiaProdutiva.ENERGIAS_HIDRICAS.id ||
                $scope.idCadeiaProdutiva === $scope.cadeiaProdutiva.MINERACAO_FERRO.id ||
                    $scope.idCadeiaProdutiva === $scope.cadeiaProdutiva.SIDERURGIA.id;
        }

        /*
        *   Método utilizado para ajustar a sessão 'Responsável pelas Informações'
        */
        $scope.setUsuarioResponsavel = function(result){
            var usuario = undefined;

            if(angular.isUndefined(result) || result.length === 0){
                usuario = new Object();
                usuario.usuarioResponsavel = {nome : '--'};
            }else{
                usuario = $scope.projeto;
            }

            $scope.empregos = {
                usuarioResponsavel: {
                    nome: usuario.usuarioResponsavel.nome
                },
                dataUltimaAlteracao: usuario.dataUltimaAlteracao
            };
        }

        var getEmpregosEmpregoAgricolaIndustria = function(tipoEmprego){
            var path = tipoEmprego == "TEMPORARIO" ? 'allEmpregoAgricolaIndustriaDTOTemporario' : 'allEmpregoAgricolaIndustriaDTOPermanente';

            Restangular.all('projeto').one($stateParams.id).one('emprego').one(path).getList().then(function(result) {
                setTimeout(function() {
                    if(tipoEmprego === "TEMPORARIO"){
                        $scope.temporariosImplantacaoList = result;
                    }else{
                        $scope.permantentesMaturacaoList = result;
                    }

                    $scope.setUsuarioResponsavel(result);

                }, true);
            });
        };

        var getEmpregosByTipoEmprego = function(idTipoEmprego){
            Restangular.all('projeto').one($stateParams.id).one('emprego').one('allEmpregosByTipoEmprego', idTipoEmprego).getList().then(function(result) {
                setTimeout(function() {

                    if(idTipoEmprego === $scope.tipoEmprego.TEMPORARIO.id){
                        $scope.temporariosList = result;
                    }else if(idTipoEmprego === $scope.tipoEmprego.PERMANENTE.id){
                        $scope.permantentesList = result;
                    }else if(idTipoEmprego === $scope.tipoEmprego.TEMPORARIO_OBRA.id){
                        $scope.temporariosObraList = result;
                    }else if(idTipoEmprego === $scope.tipoEmprego.PERMANENTE_OPERACAO.id){
                        $scope.permantentesOperacaoList = result;
                    }

                    $scope.setUsuarioResponsavel(result);

                }, true);
            });
        };

        initialize();

        /*
        *   Valida os empregos de todas as listas
        */
        var isEmpregosValidos = function(){
            var isValid = true;
            if($scope.isSucroenergetico()){
                $.each($scope.todosEmpregosList, function(index, emprego){
                    if(emprego.diretoIndustria === "" || emprego.indiretoIndustria === "" || emprego.ano === ""
                        || emprego.diretoAgricola === "" || emprego.indiretoAgricola === ""){
                        isValid = false;
                    }
                });
            }else{
                $.each($scope.todosEmpregosList, function(index, emprego){
                    if(emprego.direto === "" || emprego.indireto === "" || emprego.ano === ""){
                        isValid = false;
                    }
                });
            }

            if(!isValid){
                growl.error($filter('translate')("message.error.MSGXX_DADOS_INVALIDOS"));
            }

            return isValid;
        };

        var convertFromEmpregosAgricolaIndustriaToEmpregos = function(tipoEmprego){
            var empregosAux = [];

            $.each($scope.permantentesMaturacaoList, function(index, e){
                empregosAux.push(convertEmpregoIndustria('PERMANENTE_MATURACAO_INDUSTRIAL', e));
                empregosAux.push(convertEmpregoAgricola('PERMANENTE_MATURACAO_AGRICOLA', e));
            });

            $.each($scope.temporariosImplantacaoList, function(index, e){
                empregosAux.push(convertEmpregoIndustria('TEMPORARIO_IMPLANTACAO_INDUSTRIAL', e));
                empregosAux.push(convertEmpregoAgricola('TEMPORARIO_IMPLANTACAO_AGRICOLA', e));
            });

            $scope.todosEmpregosList = empregosAux;
        };

        var convertEmpregoAgricola = function(tipoEmprego, emprego){
            return {id: emprego.empregoAgricolaId,
                direto: emprego.diretoAgricola,
                indireto: emprego.indiretoAgricola,
                tipoEmprego: tipoEmprego,
                ano: emprego.ano};
        };

        var convertEmpregoIndustria = function(tipoEmprego, emprego){
            return {id: emprego.empregoIndustriaId,
                direto: emprego.diretoIndustria,
                indireto: emprego.indiretoIndustria,
                tipoEmprego: tipoEmprego,
                ano: emprego.ano};
        };

        /*
        *   Ao salvar deve exibir a tela de cronograma
        */
        $scope.save = function() {
            //  hash que contém os ids e os dados das Grids
            $scope.hashGrids = [
            {id: '#temporaryJobsGeneratedGrid', dados: $scope.temporariosList},
            {id: '#permanentJobsGeneratedGrid', dados: $scope.permantentesList},
            {id: '#temporaryJobsDuringImplantationGrid', dados: $scope.temporariosImplantacaoList},
            {id: '#duringWorkTemporaryGrid', dados: $scope.temporariosObraList},
            {id: '#afterOperationStartPermanentGrid', dados: $scope.permantentesOperacaoList},
            {id: '#permanentJobsGeneratedAfterMaturationGrid', dados: $scope.permantentesMaturacaoList}];

            // Lista que será preenchida com todos models das grids exibidas na tela
            $scope.todosEmpregosList = [];

            $.each($scope.hashGrids, function(index, grid){
                //   Se a grid estiver visível
                if (grid.dados) {
                    $.each(grid.dados, function(index, dados){
                        $scope.todosEmpregosList.push(dados);
                    });
                }
            });

            if(isEmpregosValidos()){
                //  Se for sucroenergetico converte a lista para empregos
                if($scope.isSucroenergetico()){
                    convertFromEmpregosAgricolaIndustriaToEmpregos();
                }
                Restangular.all('projeto').one($stateParams.id).post("emprego", $scope.todosEmpregosList).then(function(empregoSalvo) {
                    growl.success($filter('translate')("message.success.MSG006_REGISTRO_GRAVADO_SUCESSO"));
                    // Utilizado para permitir que o usuário mude de página
                    // sem a mensagem de perca de dados
                    $scope.formJobs.$setPristine();
                    //  Após salvar vai para a aba de cronograma
                    $state.go('base.project.ice.scheduleTpl', {
                        'screen': $scope.screenService.getScreenName(),
                        'id': $scope.projeto.id
                    });
                }, function() {
                    growl.error("Ocorreu um erro ao tentar gravar o registro!");
                });
            }
        };

    }).directive('ngGridJobs', function(configuration, GridButton, Restangular, growl, $filter, dialogs, $window, AUTH_ROLES, AuthService) {
    return {
        restrict: 'E',
        templateUrl: configuration.rootDir + 'views/ice/partial/jobs-grid.html',
        scope: {
            listaEmpregos: "=",
            tipoEmprego: "="
        },
        link: function(scope, iElement, iAttrs) {

            scope.data = [];
            scope.selectedItems = [];
            scope.disableFooter = true;

            scope.columnDefs = [{
                field: 'ano',
                enableCellEdit: true,
                width: "*",
                translationKey: 'jobs.achievement.year',
                editableCellTemplate: '<input type="numeric" ng-class="\'colt\' + col.index"  ng-input="COL_FIELD" ng-model="COL_FIELD" maxlength="4" ui-mask="9999"/>'
            }, {
                field: 'direto',
                enableCellEdit: true,
                width: "*",
                translationKey: 'jobs.direct',
                editableCellTemplate: '<input type="numeric" placeholder="{{\'jobs.direct\' | translate}}" ng-class="\'colt\' + col.index"  ng-input="COL_FIELD" ng-model="COL_FIELD" maxlength="10"/>'
            }, {
                field: 'indireto',
                enableCellEdit: true,
                width: "*",
                translationKey: 'jobs.indirect',
                editableCellTemplate: '<input type="numeric" placeholder="{{\'jobs.indirect\' | translate}}" ng-class="\'colt\' + col.index"  ng-input="COL_FIELD" ng-model="COL_FIELD" maxlength="10"/>'
            }];

            /*
            *   Caso o emprego esteja persistido no banco, ele é deletado.
            *   Caso contrário ele é deletado da tabela
            */
            var deleteFn = function() {
                scope.deletedItems = [];
                for (var i = 0; i < scope.selectedItems.length; i++) {
                    //  se não tiver id é um registro novo
                    if (scope.selectedItems[i].id) {
                        scope.deletedItems.push(scope.selectedItems[i].id);
                    }
                };

                 // Se o usuário somente selecionou registros novos
                if (scope.deletedItems.length === 0 && scope.selectedItems.length !== 0) {
                    growl.success($filter('translate')('message.success.MSG010_EXCLUSÃO_REALIZADA'));
                }

                if (scope.deletedItems.length !== 0) {
                    Restangular.all('projeto').one(scope.$parent.projeto.id+"").post('emprego/removeEmprego', scope.deletedItems).then(function() {
                        scope.deletedItems = [];
                        growl.success($filter('translate')('message.success.MSG010_EXCLUSÃO_REALIZADA'));
                        return true;
                    }, function() {
                        growl.error('Ocorreu um erro ao tentar efetuar a exclusão.');
                        return false;
                    });
                }
            };

            var disableDeleteFn = function() {
                return AuthService.isUserAuthorized([AUTH_ROLES.infraAnalist, AUTH_ROLES.informationAnalist,
                    AUTH_ROLES.director, AUTH_ROLES.generalSecretary]);
            }

            var disableNewFn = function() {
                return AuthService.isUserAuthorized([AUTH_ROLES.infraAnalist, AUTH_ROLES.informationAnalist,
                    AUTH_ROLES.director, AUTH_ROLES.generalSecretary]);
            }

            scope.buttonsConfig = {
                //function GridButton(show, translationKey, searchMode, disable, action, screenLink)
                newButton: new GridButton(true, 'common.new', false, disableNewFn, null, ''),
                deleteButton: new GridButton(true, 'common.delete', false, disableDeleteFn, deleteFn, '')
            }

            scope.$watch('listaEmpregos', function(newVal, oldVal) {
                if (scope.listaEmpregos) {
                    setTimeout(function() {
                        scope.data = scope.listaEmpregos;
                        scope.totalIndireto = 0;
                        scope.totalDireto = 0;
                        $.each(scope.data, function(index, emprego) {
                            scope.totalIndireto += emprego.indireto ? parseInt(emprego.indireto) : 0;
                            scope.totalDireto += emprego.direto ? parseInt(emprego.direto) : 0;

                        // Se estiver incluindo nova linha adiciona o tipoEmprego
                        if(!emprego.tipoEmprego){
                            emprego.tipoEmprego = scope.tipoEmprego;
                        }
                    });
                    }, true);
                }
            }, true);
        }
    };
}).directive('ngGridJobsAgriculturalIndustry', function(configuration, GridButton, Restangular, growl, $filter, AUTH_ROLES, AuthService) {
    return {
        restrict: 'E',
        templateUrl: configuration.rootDir + 'views/ice/partial/jobs-grid-agricultural-industry.html',
        scope: {
            listaEmpregos: "=",
            tipoEmprego: "="
        },
        link: function(scope, iElement, iAttrs) {

            scope.data = [];
            scope.selectedItems = [];
            scope.disableFooter = true;

            scope.columnDefs = [{
                field: 'ano',
                enableCellEdit: true,
                width: "300",
                translationKey: 'jobs.achievement.year',
                editableCellTemplate: '<input type="numeric" ng-class="\'colt\' + col.index"  ng-input="COL_FIELD" ng-model="COL_FIELD" maxlength="4" ui-mask="9999"/>'
            }, {
                field: 'diretoIndustria',
                enableCellEdit: true,
                width: "*",
                translationKey: 'jobs.direct.industry',
                editableCellTemplate: '<input type="numeric" placeholder="{{\'jobs.direct.industry\' | translate}}" ng-class="\'colt\' + col.index"  ng-input="COL_FIELD" ng-model="COL_FIELD" maxlength="10"/>'
            }, {
                field: 'indiretoIndustria',
                enableCellEdit: true,
                width: "*",
                translationKey: 'jobs.indirect.industry',
                editableCellTemplate: '<input type="numeric" placeholder="{{\'jobs.indirect.industry\' | translate}}" ng-class="\'colt\' + col.index"  ng-input="COL_FIELD" ng-model="COL_FIELD" maxlength="10"/>'
            }, {
                field: 'diretoAgricola',
                enableCellEdit: true,
                width: "*",
                translationKey: 'jobs.direct.agricultural',
                editableCellTemplate: '<input type="numeric" placeholder="{{\'jobs.direct.agricultural\' | translate}}" ng-class="\'colt\' + col.index"  ng-input="COL_FIELD" ng-model="COL_FIELD" maxlength="10"/>'
            }, {
                field: 'indiretoAgricola',
                enableCellEdit: true,
                width: "*",
                translationKey: 'jobs.indirect.agricultural',
                editableCellTemplate: '<input type="numeric" placeholder="{{\'jobs.indirect.agricultural\' | translate}}" ng-class="\'colt\' + col.index"  ng-input="COL_FIELD" ng-model="COL_FIELD" maxlength="10"/>'
            }];

            /*
            *   Caso o emprego esteja persistido no banco, ele é deletado.
            *   Caso contrário ele é deletado da tabela
            */
            var deleteFn = function() {
                scope.deletedItems = [];
                for (var i = 0; i < scope.selectedItems.length; i++) {
                    //  se não tiver id é um registro novo
                    if (scope.selectedItems[i].empregoIndustriaId || scope.selectedItems[i].empregoAgricolaId) {
                        scope.deletedItems.push(scope.selectedItems[i].empregoIndustriaId);
                        scope.deletedItems.push(scope.selectedItems[i].empregoAgricolaId);
                    }
                };

                // Se o usuário somente selecionou registros novos
                if (scope.deletedItems.length === 0 && scope.selectedItems.length !== 0) {
                    growl.success($filter('translate')('message.success.MSG010_EXCLUSÃO_REALIZADA'));
                }

                if (scope.deletedItems.length !== 0) {
                    Restangular.all('projeto').one(scope.$parent.projeto.id + "").post('emprego/removeEmprego', scope.deletedItems).then(function() {
                        scope.deletedItems = [];
                        growl.success($filter('translate')('message.success.MSG010_EXCLUSÃO_REALIZADA'));
                        return true;
                    }, function() {
                        growl.error('Ocorreu um erro ao tentar efetuar a exclusão.');
                        return false;
                    });
                }
            };

            var disableDeleteFn = function() {
                return AuthService.isUserAuthorized([AUTH_ROLES.infraAnalist, AUTH_ROLES.informationAnalist,
                    AUTH_ROLES.director, AUTH_ROLES.generalSecretary]);
            }

            var disableNewFn = function() {
                return AuthService.isUserAuthorized([AUTH_ROLES.infraAnalist, AUTH_ROLES.informationAnalist,
                    AUTH_ROLES.director, AUTH_ROLES.generalSecretary]);
            }

            scope.buttonsConfig = {
                //function GridButton(show, translationKey, searchMode, disable, action, screenLink)
                newButton: new GridButton(true, 'common.new', false, disableNewFn, null, ''),
                deleteButton: new GridButton(true, 'common.delete', false, disableDeleteFn, deleteFn, '')
            }

            scope.$watch('listaEmpregos', function(newVal, oldVal) {
                if (scope.listaEmpregos) {
                    setTimeout(function() {
                        scope.data = scope.listaEmpregos;
                        scope.totalDiretoIndustria = 0
                        scope.totalIndiretoIndustria = 0;
                        scope.totalDiretoAgricola = 0;
                        scope.totalIndiretoAgricola = 0;
                        $.each(scope.data, function(index, emprego) {
                            scope.totalDiretoIndustria += emprego.diretoIndustria ? parseInt(emprego.diretoIndustria) : 0;
                            scope.totalIndiretoIndustria += emprego.indiretoIndustria ? parseInt(emprego.indiretoIndustria) : 0;
                            scope.totalDiretoAgricola += emprego.diretoAgricola ? parseInt(emprego.diretoAgricola) : 0;
                            scope.totalIndiretoAgricola += emprego.indiretoAgricola ? parseInt(emprego.indiretoAgricola) : 0;

                        // Se estiver incluindo nova linha adiciona o tipoEmprego
                        if(!emprego.tipoEmprego){
                            emprego.tipoEmprego = scope.tipoEmprego;
                        }
                    });
                    }, true);
                }
            }, true);
        }
    };
}).constant('CHANGE_EMPREGO', {
        TEMPORARIO: {
            posicao: 1,
            descricao: "Empregos temporários a serem gerados"
        },
        PERMANENTE: {
            posicao: 2,
            descricao: "Empregos permanentes a serem gerados"
        },
        TEMPORARIO_OBRA: {
            posicao: 3,
            descricao: "Temporário durante Obra"
        },
        PERMANENTE_OPERACAO: {
            posicao: 4,
            descricao: "Permanente após início Operação"
        },
        TEMPORARIO_IMPLANTACAO_AGRICOLA: {
            posicao: 5,
            descricao: "Temporário durante Implantação - Agrícola"
        },
        TEMPORARIO_IMPLANTACAO_INDUSTRIAL: {
            posicao: 6,
            descricao: "Temporário durante Implantação - Industrial"
        },
        PERMANENTE_MATURACAO_AGRICOLA: {
            posicao: 7,
            descricao: "Permanente após Maturação - Agrícola"
        },
        PERMANENTE_MATURACAO_INDUSTRIAL: {
            posicao: 8,
            descricao: "Permanente após Maturação - Industrial"
        }
    });