'use strict'
angular.module('gpifrontApp')
    .controller('reportProjectCtrl', function($scope, growl, Restangular, $translate, $filter) {

        $scope.title = "reportProjects.title";

        /*Início configs grid*/
        $scope.data = [];
        $scope.columnDefs = [{
            field: 'nomeEmpresa',
            width: "100",
            translationKey: 'common.company'
        }, {
            field: 'nomeProjeto',
            width: "200",
            translationKey: 'common.project'
        }, {
            field: 'cidade',
            width: "250",
            translationKey: 'address.city'
        }, {
            field: 'regiaoPlanejamento',
            width: "350",
            translationKey: 'location.planing.region'
        }, {
            field: 'valorInvestimento',
            width: "200",
            translationKey: 'reportProjects.investimento'
        }, {
            field: 'faturamentoInicial',
            width: "150",
            translationKey: 'reportProjects.faturamento.inicial'
        }, {
            field: 'faturamentoFinal',
            width: "150",
            translationKey: 'reportProjects.faturamento.final'
        }, {
            field: 'empregosDiretos',
            width: "250",
            translationKey: 'reportProjects.empregos.diretos'
        }, {
            field: 'empregosIndiretos',
            width: "250",
            translationKey: 'reportProjects.empregos.indiretos'
        }, {
            field: 'cadeiaProdutiva',
            width: "200",
            translationKey: 'common.productive.chain'
        }, {
            field: 'situacaoAtualDescricao',
            width: "100",
            translationKey: 'common.currentStatus'
        }, {
            field: 'estagioAtualDescricao',
            width: "200",
            translationKey: 'common.currentStage'
        }, {
            field: 'analista',
            width: "200",
            translationKey: 'common.analyst'
        }, {
            field: 'departamento',
            width: "200",
            translationKey: 'common.manage'
        }];
        /*Fim configs grid*/

        function translate(translationKey) {
            var text = "";
            if (typeof translationKey !== 'undefined' && translationKey) {
                text = $filter('translate')(translationKey);
            }
            return text;
        };

        $scope.search = function(filtros) {
            filtros.versao = true;

            Restangular.all('relatorio/projeto/getByFilters').getList(filtros).then(function(result) {
                $scope.data = result;

                if ($scope.data.length === 0) {
                    growl.error(translate('message.error.MSG004_NENHUM_REGISTRO_ENCONTRADO'));
                }
            }, function errorCallback(response) {
                if (response.status !== 404) {
                    growl.error("Error: The server returned status " + response.status);
                }
            });

        };

    });
