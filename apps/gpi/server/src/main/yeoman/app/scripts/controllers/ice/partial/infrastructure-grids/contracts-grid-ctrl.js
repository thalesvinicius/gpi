'use strict'

angular
    .module('gpifrontApp')
    .controller('ContractsGridCtrl', function($scope, GridButton, growl, $filter){

        $scope.energiaContratadas = $scope.$parent.energiaContratadas;              

        //Define its column-defs attribute
        $scope.energiaContratadasColumnDefs = [{
            field: 'contrato',
            enableCellEdit: true,
            width: "**",
            translationKey: 'common.contract',
            editableCellTemplate: '<input type="numeric" maxlength="10" ng-class="\'colt\' + col.index"  ng-input="COL_FIELD" ng-model="COL_FIELD" maxlength="100"/>'            
        }, {
            field: 'nome',
            enableCellEdit: true,
            width: "**",
            translationKey: 'common.name'
        }, {
            field: 'localizacao',
            enableCellEdit: true,
            width: "**",
            translationKey: 'common.location'
        }];

        //define its buttons-config attribute on a search screen
        $scope.energiaContratadasButtonsConfig = {            
            newButton: new GridButton(true, 'common.new', false, null, null, ''),
            deleteButton: new GridButton(true, 'common.delete', false, null, deleteFn, '')
        }

        //define its sort-info attribute if you desire a default-sort
        $scope.energiaContratadasSortInfo = {
            fields: ['id'],
            directions: ['asc']
        };

        $scope.$parent.$watch('energiaContratadas', function(newVal, oldVal){
            if ($scope.$parent.energiaContratadas != null) {
                setTimeout(function() {          
                    $scope.energiaContratadas = $scope.$parent.energiaContratadas;  
                }, true);
            }                
        }, true);

        function deleteFn() {
            growl.info($filter('translate')('message.error.MSG38_SECAO_ATUALIZADA', {value: "infraestrutura e Meio Ambiente"}));                        
        }                

    });