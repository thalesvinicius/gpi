'use strict';

angular.module('gpifrontApp')
    .controller('ExternalUserGridCtrl', function($scope, GridButton) {

    	//define its buttons-config attribute on a search screen
        $scope.buttonsConfig = {            
            newButton: new GridButton(false, 'common.new', false, null, null, ''),
            deleteButton: new GridButton(false, 'common.delete', false, null, null, '')
        }

        $scope.registeredExternalUserColumnDefs = [{
            field: 'nomeEmpresa',
            enableCellEdit: false,
            width: "**",
            translationKey: 'user.external.companyUnit'
        },{
            field: "nome",
            translationKey: 'common.name',
            enableCellEdit: false
        }, {
            field: "cargoExterno",
            translationKey: 'common.job.title',
            enableCellEdit: false
        }, {
            field: "email",
            translationKey: 'common.email',
            enableCellEdit: false
        }, {
            field: "status",
            enableCellEdit: false,
            width: "90px",
            translationKey: 'common.status',
            cellTemplate: '<div class="ngCellText" ng-switch on="row.entity.status"><span ng-class="\'colt\' + col.index" ng-switch-when="true" translate>common.active</span><span ng-class="\'colt\' + col.index" ng-switch-when="false" translate>common.inactive</span>'
        }, {
            field: "mailing",
            enableCellEdit: false,
            width: "90px",
            translationKey: 'user.external.mailing',
            cellTemplate: '<div class="ngCellText" ng-switch on="row.entity.mailing"><span ng-class="\'colt\' + col.index" ng-switch-when="true" translate>common.yes</span><span ng-class="\'colt\' + col.index" ng-switch-when="false" translate>common.no</span>'
        }, {
            field: "id",
            translationKey: 'common.show',
			cellTemplate:  '<div class="text-center" ng-class="col.colIndex()">' +
			            		'<a ng-click="showContactExternal(row.entity.id)"><i class="fa fa-eye page-header-icon"></i></a> </div>',
            enableCellEdit: false
        }];
});