'use strict'

angular
	.module('gpifrontApp')
	.controller('PlannedInvestments2GridCtrl', function($scope, Restangular, GridButton, growl, $filter){

        $scope.investimentoPrevisto2 = $scope.$parent.investimentoPrevisto2;            
        $scope.investimentoPrevisto2SelectedItems = $scope.$parent.investimentoPrevisto2SelectedItems;

		$scope.investimentoPrevisto2ColumnDefs = [{
            field: "ano",
            enableCellEdit: true,
            width: "**",
            translationKey: "common.achievement.year",
            editableCellTemplate: '<input type="numeric" ui-mask="9999" maxlength="4" ng-class="\'colt\' + col.index"  ng-input="COL_FIELD" ng-model="COL_FIELD"/>'                                    
        }, {
            field: "valorIndustrial",
            enableCellEdit: true,
            width: "**",
            cellFilter: "currency:''",
            editableCellTemplate: '<input type="text" format="currency" maxlength="21" ng-class="\'colt\' + col.index"  ng-input="COL_FIELD" ng-model="COL_FIELD"/>',                                                
            translationKey: "financial.industrial.investment"
        }, {
            field: "valorAgricola",
            enableCellEdit: true,
            width: "**",
            cellFilter: "currency:''",            
            editableCellTemplate: '<input type="text" format="currency" maxlength="21" ng-class="\'colt\' + col.index"  ng-input="COL_FIELD" ng-model="COL_FIELD"/>',                                                
            translationKey: "financial.agricultural.investiment"
        }, {
            field: "valorCapacitacao",
            enableCellEdit: true,
            width: "**",
            cellFilter: "currency:''",            
            editableCellTemplate: '<input type="text" format="currency" maxlength="21" ng-class="\'colt\' + col.index"  ng-input="COL_FIELD" ng-model="COL_FIELD"/>',                                                
            translationKey: "financial.traning.investiment"
        }, {
            field: "valorProprios",
            enableCellEdit: true,
            width: "**",
            cellFilter: "currency:''",
            editableCellTemplate: '<input type="text" format="currency" maxlength="21" ng-class="\'colt\' + col.index"  ng-input="COL_FIELD" ng-model="COL_FIELD"/>',                                                
            translationKey: "financial.own.investiment"
        }];

        $scope.investimentoPrevisto2ButtonsConfig = {            
            newButton: new GridButton(true, 'common.new', false, null, null, ''),
            deleteButton: new GridButton(true, 'common.delete', false, null, deleteFn, '')
        }

        //define its sort-info attribute if you desire a default-sort
        $scope.investimentoPrevisto2SortInfo = {
            fields: ['ano'],
            directions: ['asc']
        };

        function deleteFn() {
            growl.info($filter('translate')('message.error.MSG38_SECAO_ATUALIZADA', {
                value: "Investimento Previsto"
            }));                        
        }              
		
        $scope.$parent.$watch('investimentoPrevisto2', function(newVal, oldVal){
            if ($scope.$parent.investimentoPrevisto2 != null) {
                    $scope.investimentoPrevisto2 = newVal;   
            }                
        }, true);

	});


