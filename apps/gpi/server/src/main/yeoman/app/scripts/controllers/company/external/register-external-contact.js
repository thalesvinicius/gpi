'use strict';

angular.module('gpifrontApp')
    .controller('RegisterExternalContactCtrl', function($scope, Restangular, $stateParams, $filter, growl, UserSession, TIPO_USUARIO,
        $rootScope, GridButton, $translate, configuration, ScreenService, AUTH_ROLES, AuthService, dialogs, $state, $location, $anchorScroll) {
        $scope.externalUserSelectedItems = [];

        $scope.externalUserGridFunctions = ["showExternalUser"];

        $scope.informacoesUsuarioExterno = [];

        $scope.nomeEmpresa = "";

        $scope.gridScope = {
            showContactExternal: showContactExternal
        };

        // Atualiza a lista de empresas na medida que for digitado algo
        $scope.updateCompanies = function(typed) {
            //tratamentos referentes ao campo de empresa
            if (typed) {
                var filtro = {
                    nomeEmpresa: typed
                }
                return Restangular.all('empresa/' + $scope.getCompany() + '/empresaUnidadeDTOAtivas').getList(filtro);
            }
            return [];
        };

        // Chamado ao selecionar uma empresa ou unidade no autocomplete
        $scope.empresaSelected = function(selected) {
            $scope.empresaUnidadeSelecionadaPrev = selected;
        };

        // Adiciona a empresa selecionada
        $scope.addCompany = function() {
            if ($scope.empresaUnidadeSelecionadaPrev) {
                $scope.contactExternal.empresaId = $scope.empresaUnidadeSelecionadaPrev.empresaId;
                $scope.contactExternal.unidadeId = $scope.empresaUnidadeSelecionadaPrev.unidadeId;

                $scope.empresaUnidadeSelecionadaPrev = null;
                $scope.empresaUnidadeSelecionadaTxt = null;
            }
        };

        var isCreate = false;

        var getContactFromScopeById = function(contactId) {
            for (var i = $scope.informacoesUsuarioExterno.length - 1; i >= 0; i--) {
                if ($scope.informacoesUsuarioExterno[i].id == contactId) {
                    return $scope.informacoesUsuarioExterno[i];
                }
            };
            return null;
        };

        function showContactExternal(contactId) {
            $scope.disableContactExternalFormDiv = true;
            $scope.contactExternal = getContactFromScopeById(contactId);
            showContactExternalForm();
        };

        $scope.disableactiveInactiveFn = function() {
            return $scope.externalUserSelectedItems.length === 0;
        }

        $scope.getNomeEmpresa = function() {
            return $scope.nomeEmpresa;
        }

        $scope.init = function() {
            $scope.contactExternalInformationsVisible = true;
            hideContactForm();
            $scope.empresasUnidadesDTO = [];
            $scope.empresaUnidadeSelecionadaTxt = null;

            var empresaId = $scope.getCompany();
            Restangular.one('empresa/' + empresaId).get().then(function(empresa) {
                $scope.nomeEmpresa = empresa.nomePrincipal;
            });

            Restangular.all('userExternal/byEmpresa/' + empresaId).getList().then(function(usuariosExternos) {
                $scope.informacoesUsuarioExterno = usuariosExternos;
            });
        }

        $scope.disableEditFn = function() {
            return $scope.externalUserSelectedItems.length !== 1;
        }

        $scope.getCompany = function() {
            if (UserSession.getUser().tipo === 2) {
                if (UserSession.getUser().empresaId !== undefined) {
                    return UserSession.getUser().empresaId;
                }
            }

            return $stateParams.id;
        }

        /*
         * Prepara a tela para incluir contato.
         */
        $scope.prepareCreateContactExternal = function() {
            $scope.disableContactExternalFormDiv = false;
            clearUserExternalForm();
            $scope.contactExternal = {
                mailing: false
            }
            showContactExternalForm();
            isCreate = true;
        };

        $scope.prepareEditContactExternal = function() {
            $scope.disableContactExternalFormDiv = false;
            $scope.contactExternal = $scope.externalUserSelectedItems[0];
            $scope.contactExternal.confirmEmail = $scope.contactExternal.email;
            showContactExternalForm();
            isCreate = false;
        };

        $scope.isNewRegister = function() {
            return isCreate;
        }

        /*
         *   Valida o campo passado
         */
        $scope.checkValidation = function(formField) {
            return formField.$invalid && (formField.$dirty || $scope.formSubmitted);
        }

        /*
         * Salva os dados do contato
         */
        $scope.save = function(isFormValid) {
            if (isFormValid && $scope.checkEmail()) {
                $scope.addCompany();
                if (!$scope.contactExternal.empresaId) {
                    growl.error("Favor selecionar uma empresa/unidade válida.");
                } else {
                    $scope.contactExternal.tipo = TIPO_USUARIO.EXTERNO;
                    //$scope.contactExternal.empresaId = $scope.getCompany();

                    Restangular.all('user/crudExternalUser').post($scope.contactExternal).then(function(result) {
                        hideContactForm();
                        //      if ($scope.isNewRegister()) {
                        // Restangular.one('user/sendEmail/' + $scope.contactExternal.email).get().then(function() {});
                        //      }
                        growl.success("Registro gravado com sucesso!");
                        $scope.contactExternalForm.$setPristine();
                        setTimeout(function() {
                            $state.transitionTo($state.current, $stateParams, {
                                reload: true,
                                inherit: true,
                                notify: true
                            });
                        }, 5000);

                        //$state.go("base.company.registerExternalContact");
                    }, function(response) {
                        growl.error("Ocorreu um erro ao tentar gravar o registro!");
                    });
                }

            } else {
                growl.error($filter('translate')("message.error.MSGXX_DADOS_INVALIDOS"));
                $scope.formSubmitted = true;
            }
        }

        $scope.disableheader = true;

        /*
         * Prepara para ativar/desativar os contatos selecionados.
         */
        $scope.preActiveInactive = function() {
            var confirmDialog = dialogs.confirm($filter('translate')('DIALOGS_CONFIRMATION'), $filter('translate')('message.confirm.MSG011_CONFIRMAR_ATIVAR_INATIVAR'));
            confirmDialog.result.then(function(btn) {
                // Caso confirme executa a ativar/inativar
                $scope.activeInactiveFn();
            }, function(btn) {
                $scope.gridScope.checkSelectAllCheckBox(true, true);
                $scope.gridScope.checkSelectAllCheckBox(false, true);
                // Caso não confirme deseleciona os registros selecionados
                $scope.externalUserSelectedItems.splice(0, $scope.externalUserSelectedItems.length);
            });
        }

        /*
         * Ativa e desativa os contatos selecionados.
         */
        $scope.activeInactiveFn = function() {
            for (var i = $scope.externalUserSelectedItems.length - 1; i >= 0; i--) {
                $scope.externalUserSelectedItems[i].tipo = TIPO_USUARIO.EXTERNO;

                Restangular.all('user/activateInactivateExternalUser').post($scope.externalUserSelectedItems[i]).then(function(result) {
                    growl.success("Ativar/Inativar realizado com sucesso!");
                    setTimeout(function() {
                        $state.transitionTo($state.current, $stateParams, {
                            reload: true,
                            inherit: true,
                            notify: true
                        });
                    }, 1000);
                }, function(response) {
                    if (response.status > 400) {
                        $scope.hasErrors = true;
                        $scope.errorMessage = $filter('translate')(response.data.consumerMessage);
                    } else {
                        growl.error($filter('translate')("Houve um erro ao gravar o registro!"));
                    }
                });
            };
        }

        $scope.requestNewPasswordFn = function() {
            var selectedEmails = _.map($scope.externalUserSelectedItems, function(selected) {
                return {
                    emailAddress: selected.email
                }
            });
            console.log(selectedEmails);
            Restangular.all('user/solicitarResetSenhas').post(selectedEmails).then(function() {
                growl.success("E-mail(s) enviado(s) com sucesso.");
            });
        }

        $scope.disablerequestNewPasswordFn = function() {
            return $scope.externalUserSelectedItems.length < 1;
        }

        var clearUserExternalForm = function() {
            $scope.contactExternal = {};
            document.contactExternalForm.reset();
        };

        var showContactExternalForm = function() {
            showContactForm();
            $scope.showContactExternalForm = true;
            setTimeout(function() {
                if ($scope.disableContactFormDiv) {
                    $location.hash('companyInput');
                    $anchorScroll();
                } else {
                    angular.element("#companyInput").focus()
                }
            }, true);

        };

        $scope.checkEmail = function() {
            var retorno = ($scope.contactExternal.email === $scope.contactExternal.confirmEmail);
            if (!retorno) {
                growl.error($filter('translate')("message.error.MSG84_EMAIL_NAO_CONFERE"));
            }
            return retorno;
        }

        var hideContactForm = function() {
            angular.element("#contactExternalFormDiv").hide();
        };

        var showContactForm = function() {
            angular.element("#contactExternalFormDiv").show();
        };

        $scope.closeForm = function() {
            $scope.contactExternalForm = false;
            hideContactForm();
        }

        $scope.cancel = function() {
            if ($scope.contactExternalForm.$dirty) {
                var cancelDialog = dialogs.confirm($filter('translate')('DIALOGS_CONFIRMATION'), $filter('translate')('message.confirm.MSG31_CANCELAR'));
                cancelDialog.result.then(function(btn) {
                    //  Altera o form para pristine para não exibir a MSG33_DADOS_NAO_SALVOS
                    $scope.contactExternalForm.$setPristine();
                    $scope.closeForm();
                });
            } else {
                $scope.closeForm();
            }
        }

        $scope.checkUserAuthorization();        
        $scope.init();

    });
