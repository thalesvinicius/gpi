'use strict';

angular.module('gpifrontApp')
	.controller('SatisfactionSurveyGridCtrl', function($scope, TIPO_PERGUNTA, GridButton, TIPO_INSTRUMENTO_FORMALIZACAO){
        $scope.gridScope = {
            tipoInstrumentoFormalizacao: TIPO_INSTRUMENTO_FORMALIZACAO
        }

        $scope.pesquisaSatisfacaoList = $scope.$parent.pesquisaSatisfacaoList;

        $scope.pesquisaSatisfacaoSelectedItems = [];

        $scope.pesquisaSatisfacaoColumnDefs = [{
            field: 'instrumentoFormalizacao.protocolo',
            enableCellEdit: false,
            width: "**",
            translationKey: 'satisfactionSurvey.grid.protocol'
        }, {
            field: 'instrumentoFormalizacao.tipo',
            enableCellEdit: false,
            width: "**",
            cellTemplate: "<div class=\"ngCellText\" ng-class=\"col.colIndex()\"><span ng-cell-text>{{tipoInstrumentoFormalizacao[row.entity.instrumentoFormalizacao.tipo].descricao}}</span></div>",
            translationKey: 'satisfactionSurvey.grid.type',
        }, {
            field: 'instrumentoFormalizacao.titulo',
            enableCellEdit: false,
            width: "**",
            translationKey: 'satisfactionSurvey.grid.title',
        }, {
            field: 'instrumentoFormalizacao.situacaoAtual.situacao.descricao',
            enableCellEdit: false,
            width: "**",
            translationKey: 'satisfactionSurvey.grid.situation',
        }, {
            field: 'instrumentoFormalizacao.dataAssinatura',
            enableCellEdit: false,
            width: "**",
            translationKey: 'satisfactionSurvey.grid.signature.date',
            cellFilter: 'dateTransformFilter'
        }, {
            field: 'show',
            translationKey: 'common.show',
            showScreenRedirect: true,
            width: "**",
            showLink: '/pesquisa-satisfacao/visualizar'
        }];

        function disableEditFn() {
            return $scope.pesquisaSatisfacaoSelectedItems.length !== 1;
        }

        $scope.pesquisaSatisfacaoButtonsConfig = {
            newButton: new GridButton(false, 'contact.new', true, null, null, ''),
            deleteButton: new GridButton(false, 'common.delete', false, null, null, ''),
            editButton: new GridButton(true, 'common.edit', true, disableEditFn, null, '/pesquisa-satisfacao/editar')
        }

        $scope.$parent.$watch('pesquisaSatisfacaoList', function(newVal, oldVal){
            if ($scope.$parent.pesquisaSatisfacaoList != null) {
                setTimeout(function(){
                    $scope.pesquisaSatisfacaoList = newVal;
                }, true);
            }
        }, true);


	});
