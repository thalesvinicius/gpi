'use strict';
angular.module('gpifrontApp')
    .controller('PublicationAttachedFilesGridCtrl', function($scope, GridButton, dialogs, $filter) {

        $scope.publicacao = {anexos: []};
        $scope.gridScope = {};

        $scope.sortInfo = {
            fields: ['fileName'],
            directions: ['asc']
        };

        $scope.columnDefs = [{
            field: 'fileName',
            enableCellEdit: true,
            width: "*",
            translationKey: 'common.file.name'
        }, {
            field: 'date',
            enableCellEdit: true,
            width: "*",
            translationKey: 'common.date',
            cellFilter: 'dateTransformFilter'
        }, {
            field: 'visualize',
            enableCellEdit: true,
            width: "*",
            translationKey: 'common.show'
        }];

        $scope.buttonsConfig = {
            //function GridButton(show, translationKey, searchMode, disable, action, screenLink)
            newButton: new GridButton(true, 'common.attach.file', false, null, null, ''),
            deleteButton: new GridButton(true, 'common.delete', false, null, null, '')
        }

        $scope.$parent.$watch('publicacao', function(newVal, oldVal) {
            if (newVal !== oldVal) {
                //Grid de Arquivos
                $scope.publicacao = $scope.$parent.publicacao;      
            }
        }, true); 

        $scope.attachedSelectedFiles = [];

        $scope.attachedFilesColumnDefs = [{
            field: "nome",
            translationKey: 'common.file.name',
            enableCellEdit: false
        }, {
            field: "dataAnexo",
            translationKey: 'common.date',
            enableCellEdit: false,
            cellFilter: 'dateTransformFilter'
        }];

        $scope.disableFn = function () {
            return $scope.attachedSelectedFiles.length < 1;
        }

        function translate(translationKey) {
            var text = "";
            if (typeof translationKey !== 'undefined' && translationKey) {
                text = $filter('translate')(translationKey);
            }
            return text;
        };

        $scope.preDeleteFile = function() {
            var confirmDialog = dialogs.confirm(translate('DIALOGS_CONFIRMATION'), translate('message.confirm.MSG009_CONFIRMAR_EXCLUSAO'));
            confirmDialog.result.then(function(btn) {
                // Caso confirme executa a exclusão
                $scope.deleteFile();
            }, function(btn) {
                $scope.gridScope.checkSelectAllCheckBox(true, true);
                $scope.gridScope.checkSelectAllCheckBox(false, true);
                // Caso não confirme deseleciona os registros selecionados
                $scope.attachedSelectedFiles.splice(0, $scope.attachedSelectedFiles.length);
            });

        }

        $scope.deleteFile = function() {
            if ($scope.publicacao.anexos.length > 0 && $scope.attachedSelectedFiles.length > 0) {
                $scope.attachedSelectedFiles.forEach(function(selected) {
                    var model = $scope.publicacao.anexos;
                    var indexOnMainList = model.indexOf(selected);
                    // deleting each unit from main list
                    $scope.publicacao.anexos.splice(indexOnMainList, 1);
                    for (var i = 0; i < $scope.$flow.files.length; i++) {
                        var file = $scope.$flow.files[i];
                        file.cancel();
                    };
                });
                // deleting all 'selectedItems' starting at index 0
                $scope.attachedSelectedFiles.splice(0, $scope.attachedSelectedFiles.length);
            }
        }
    });
