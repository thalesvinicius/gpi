'use strict'

angular
    .module('gpifrontApp')
    .controller('InputsAndProductsCtrl', function($scope, $stateParams, $state, $filter, configuration, growl, CADEIA_PRODUTIVA, SITUACAO_PROJETO, ESTAGIO_PROJETO, ScreenService, Restangular, CHANGE_INSUMOS, AuthService, AUTH_ROLES) {

        var baseInsumoProduto = Restangular.all('projeto/' + $stateParams.id + "/insumoProduto");
        var baseProjetos = Restangular.all('projeto');
        $scope.screenService = ScreenService;
        $scope.cadeiaProdutiva = CADEIA_PRODUTIVA;
        $scope.changes = [];
        $scope.change_insumos = CHANGE_INSUMOS;        

        $scope.ncmList = [];
        $scope.ncmProperties = [];

        function init() {
            $scope.projetoTO = {
                cadeiaProdutiva: {
                    id: -1
                }
            }

            $scope.insumoProduto = {
                "insumos": [],
                "produtosAdqEComPorMinas": [],
                "produtosAdqOutrosEstadosParaCom": [],
                "produtosFabEComPorMinas": [],
                "produtosImpParaCom": [],
                "parcerias": [],
                "servicos": [],
                "producoes": [],
                "concorrentes": [],
                "aquisicaoOutroEstado": false,
                "interesseParceria": true,
                "importacaoProduto": true
            };

            Restangular.one('projeto', $stateParams.id).one("projetoTO").get().then(function(result) {
                $scope.projetoTO = result;

                // $scope.projetoTO.cadeiaProdutiva.id = $scope.cadeiaProdutiva.
                $scope.situacaoProjeto = SITUACAO_PROJETO[$scope.projetoTO.situacao];
                $scope.estagioProjeto = ESTAGIO_PROJETO[$scope.projetoTO.estagio];

                if (!$scope.externalUser() && $scope.projetoTO.situacao === SITUACAO_PROJETO.PENDENTE_ACEITE.id) {
                    baseProjetos.one($stateParams.id).getList("insumoProduto/findChange/").then(function(result) {
                        $scope.changes = (result[0] === undefined ? 0 : result[0]);
                        
                        for (var j = 0; j < result.length; j++) {
                            if(result[j][CHANGE_INSUMOS.PRODUTOSADQECOMPORMINAS.posicao] === true || result[j][CHANGE_INSUMOS.PRODUTOSADQECOMPORMINAS.posicao] === 1) {
                                $scope.changes[CHANGE_INSUMOS.PRODUTOSADQECOMPORMINAS.posicao] = true;    
                            }                                                                
                            if(result[j][CHANGE_INSUMOS.PRODUTOSADQOUTROSESTADOSPARACOM.posicao] === true || result[j][CHANGE_INSUMOS.PRODUTOSADQOUTROSESTADOSPARACOM.posicao] === 1) {
                                $scope.changes[CHANGE_INSUMOS.PRODUTOSADQOUTROSESTADOSPARACOM.posicao] = true;    
                            }
                            if(result[j][CHANGE_INSUMOS.PRODUTOSFABECOMPORMINAS.posicao] === true || result[j][CHANGE_INSUMOS.PRODUTOSFABECOMPORMINAS.posicao] === 1) {
                                $scope.changes[CHANGE_INSUMOS.PRODUTOSFABECOMPORMINAS.posicao] = true;    
                            }
                            if(result[j][CHANGE_INSUMOS.PRODUTOSIMPPARACOM.posicao] === true || result[j][CHANGE_INSUMOS.PRODUTOSIMPPARACOM.posicao] === 1) {
                                $scope.changes[CHANGE_INSUMOS.PRODUTOSIMPPARACOM.posicao] = true;    
                            }
                        }
                        
                    }, function() {
                        growl.error("Ocorreu um erro ao tentar buscar o registro!");
                    });
                }
            });


            // Inicialmente preenchido como 'sim'
            $scope.insumoProduto.aquisicaoOutroEstado = true;

            Restangular.one('projeto', $stateParams.id).one("insumoProduto").get().then(function(result) {
                if (!angular.isUndefined(result)) {
                    $scope.insumoProduto = result;
                }
                // if ($scope.excetoServicosEnergiasHidricasESucroEnergetico()) {
                //     $scope.insumoProduto.importacaoProduto = true;
                // }

                if (angular.isUndefined($scope.insumoProduto.importacaoProduto) || $scope.insumoProduto.importacaoProduto === null) {
                    $scope.insumoProduto.importacaoProduto = false;
                }
            });

            function verifySumPercent(values) {
                var sumValues = 0.00;
                _.each(values, function(value) {
                    sumValues += parseFloat(value ? value : 0.00);
                });

                if (sumValues != 100.00) {
                    return true;
                } else {
                    return false;
                }
            }

            $scope.showSumErrorOrigin = false;
            $scope.showSumErrorPercentAllChain = false;
            $scope.showSumErrorMainClients = false;
            $scope.showSumErrorMainMarkets = false;

            $scope.importacaoProdutoChanged = function() {
                $scope.insumoProduto.produtosImpParaCom = [];
                $scope.insumoProduto.percentualFabricaMinas = "";
                $scope.insumoProduto.percentualAdquiridoOutro = "";
                $scope.insumoProduto.percentualImportado = "";
            }


            $scope.verifySumErrorMainMarkets = function() {
                $scope.showSumErrorMainMarkets = verifySumPercent([$scope.insumoProduto.percentualMercadoMinas,
                    $scope.insumoProduto.percentualMercadoNacional,
                    $scope.insumoProduto.percentualMercadoExterior
                ]);
            }

            $scope.verifyOriginSumError = function() {
                $scope.showSumErrorOrigin = verifySumPercent([$scope.insumoProduto.percentualOrigemMinas,
                    $scope.insumoProduto.percentualOrigemOutros,
                    $scope.insumoProduto.percentualOrigemImportado
                ]);
            }

            $scope.verifySumErrorPercentAllChain = function() {
                $scope.showSumErrorPercentAllChain = verifySumPercent([$scope.insumoProduto.percentualFabricaMinas,
                    $scope.insumoProduto.percentualAdquiridoOutro,
                    $scope.insumoProduto.percentualImportado
                ]);
            }

            $scope.verifySumErrorMainClients = function() {
                $scope.showSumErrorMainClients = verifySumPercent([$scope.insumoProduto.percentualClienteIndustria,
                    $scope.insumoProduto.percentualClienteComercial,
                    $scope.insumoProduto.percentualClienteConsumidor
                ]);
            }

            $scope.excetoServicos = function() {
                return $scope.projetoTO.cadeiaProdutiva.id !== $scope.cadeiaProdutiva.SERVICOS.id;
            }

            $scope.excetoComercializacao = function() {
                return $scope.projetoTO.cadeiaProdutiva.id !== $scope.cadeiaProdutiva.COMERCIO_E_SERVICOS.id;
            }

            $scope.excetoSucroEnergetico = function() {
                return $scope.projetoTO.cadeiaProdutiva.id !== $scope.cadeiaProdutiva.SUCROENERGETICO.id;
            }

            $scope.excetoEnergiasHidricas = function() {
                return $scope.projetoTO.cadeiaProdutiva.id !== $scope.cadeiaProdutiva.ENERGIAS_HIDRICAS.id;
            }

            $scope.showMainInputsGrid = function() {
                return $scope.excetoServicos() && $scope.excetoComercializacao();
            }

            $scope.excetoServicosEComercializacao = function() {
                return $scope.excetoServicos() && $scope.excetoComercializacao();
            }

            $scope.showMadeInMinasProductsGrid = function() {
                return $scope.excetoServicos() && $scope.excetoComercializacao() && $scope.excetoSucroEnergetico();
            }

            $scope.excetoServicosEnergiasHidricasESucroEnergetico = function() {
                return $scope.excetoServicos() && $scope.excetoEnergiasHidricas() && $scope.excetoSucroEnergetico();
            }

            $scope.excetoServicosComercializacaoESucroEnergetico = function() {
                return $scope.excetoServicos() && $scope.excetoComercializacao() && $scope.excetoSucroEnergetico();
            }

            $scope.showAcquiredInOtherStatesProducts = function() {
                return $scope.excetoServicosEnergiasHidricasESucroEnergetico();
            }

            $scope.showAcquiredInOtherCountriesProducts = function() {
                return $scope.excetoServicosEnergiasHidricasESucroEnergetico();
            }
            $scope.isCadeiaProdutivaSucroEnergetico = function() {
                return !$scope.excetoSucroEnergetico();
            }

            $scope.isCadeiaProdutivaServicos = function() {
                return !$scope.excetoServicos();
            }

            $scope.isCadeiaProdutivaComercializacaoOuServicos = function() {
                return $scope.isCadeiaProdutivaServicos() || !$scope.excetoComercializacao();
            }
            //Done to resolve blank grid rendering problem.
            $(window).trigger('resize');
        }

        init();

        function translate(translationKey) {
            var text = "";
            if (typeof translationKey !== 'undefined' && translationKey) {
                text = $filter('translate')(translationKey);
            }
            return text;
        };

        $scope.externalUser = function() {
            return AuthService.isUserAuthorized([AUTH_ROLES.externalResponsible, AUTH_ROLES.externalMeling, AUTH_ROLES.externalOther]);
        }

        var isFieldValid = function(field){
            return !angular.isUndefined(field) && field !== '';
        }

        function preSave(isFormValid) {

            var productsArray = new Array();
            productsArray = productsArray.concat($scope.insumoProduto.produtosAdqEComPorMinas);
            productsArray = productsArray.concat($scope.insumoProduto.produtosImpParaCom);
            productsArray = productsArray.concat($scope.insumoProduto.produtosAdqOutrosEstadosParaCom);
            productsArray = productsArray.concat($scope.insumoProduto.produtosFabEComPorMinas);

            if (productsArray !== undefined) {
                for (var i = productsArray.length - 1; i >= 0; i--) {
                    // SM quando selecionado um NCM o campo quantidade é obrigatório
                    if (productsArray[i].ncm !== undefined &&
                        !isFieldValid(productsArray[i].quantidade1) ||
                        !isFieldValid(productsArray[i].quantidade2) ||
                        !isFieldValid(productsArray[i].quantidade3)) {
                        growl.error('Favor informar a quantidade');
                    }
                    if (productsArray[i].nome == undefined ||
                        productsArray[i].ncm == undefined ||
                        productsArray[i].sujeitoST == undefined ||
                        !isFieldValid(productsArray[i].quantidade1) ||
                        !isFieldValid(productsArray[i].quantidade2) ||
                        !isFieldValid(productsArray[i].quantidade3) ||
                        productsArray[i].unidadeMedida == undefined) {
                        return false
                    }
                }
            }

            if ($scope.insumoProduto.parcerias !== undefined) {
                for (var i = $scope.insumoProduto.parcerias.length - 1; i >= 0; i--) {
                    if ($scope.insumoProduto.parcerias[i].descricao == undefined) {
                        return false
                    }
                }
            }

            if ($scope.insumoProduto.concorrentes !== undefined) {
                for (var i = $scope.insumoProduto.concorrentes.length - 1; i >= 0; i--) {
                    if ($scope.insumoProduto.concorrentes[i].nome == undefined ||
                        $scope.insumoProduto.concorrentes[i].descricaoProduto == undefined) {
                        return false
                    }
                }
            }

            if ($scope.insumoProduto.insumos !== undefined) {
                for (var i = $scope.insumoProduto.insumos.length - 1; i >= 0; i--) {
                    // SM quando selecionado um NCM o campo quantidade é obrigatório
                    if ($scope.insumoProduto.insumos[i].ncm !== undefined &&
                        !isFieldValid($scope.insumoProduto.insumos[i].quantidadeAno)) {
                        growl.error('Favor informar a quantidade');
                    }
                    if ($scope.insumoProduto.insumos[i].nome == undefined ||
                        $scope.insumoProduto.insumos[i].ncm == undefined ||
                        $scope.insumoProduto.insumos[i].origem == undefined ||
                        !isFieldValid($scope.insumoProduto.insumos[i].quantidadeAno) ||
                        $scope.insumoProduto.insumos[i].valorEstimadoAno == undefined ||
                        $scope.insumoProduto.insumos[i].unidadeMedida == undefined ||
                        $scope.insumoProduto.insumos[i].empresaFornecedor == undefined) {
                        return false
                    }
                }
            }

            if ($scope.insumoProduto.producoes !== undefined) {
                for (var i = $scope.insumoProduto.producoes.length - 1; i >= 0; i--) {
                    if ($scope.insumoProduto.producoes[i].ano == undefined ||
                        $scope.insumoProduto.producoes[i].areaPlantada == undefined ||
                        $scope.insumoProduto.producoes[i].moagemCana == undefined ||
                        $scope.insumoProduto.producoes[i].producaoEtanol == undefined ||
                        $scope.insumoProduto.producoes[i].producaoAcucar == undefined ||
                        $scope.insumoProduto.producoes[i].geracaoEnergia == undefined ||
                        $scope.insumoProduto.producoes[i].outro == undefined) {
                        return false
                    }
                }
            }

            if ($scope.insumoProduto.servicos !== undefined) {
                for (var i = $scope.insumoProduto.servicos.length - 1; i >= 0; i--) {
                    if ($scope.insumoProduto.servicos[i].descricao == undefined) {
                        return false
                    }
                }
            }

            return true;
        }

        function hasErrorSumPercentFiels () {
            if ($scope.excetoServicosEnergiasHidricasESucroEnergetico() && $scope.showSumErrorPercentAllChain) {
                growl.error($filter('translate')("A soma percentual dos campos da seção Percentual dos Produtos não deve ser diferente de 100%"));
                return true;
            }
            if ($scope.showSumErrorMainClients) {
                growl.error($filter('translate')("A soma percentual dos campos da seção Principais Clientes não deve ser diferente de 100%"));
                return true;
            }
            if ($scope.showSumErrorMainMarkets) {
                growl.error($filter('translate')("A soma percentual dos campos da seção Principais Mercados não deve ser diferente de 100%"));
                return true;
            }
            if ($scope.showMainInputsGrid() && $scope.showSumErrorOrigin) {
                growl.error($filter('translate')("A soma percentual dos campos da seção Origem dos Insumos não deve ser diferente de 100%"));
                return true;
            }

            return false;
        }

        $scope.save = function(isFormValid) {
            if (hasErrorSumPercentFiels())
                return;

            if (preSave(isFormValid)) {
                try {
                    if (angular.isUndefined($scope.insumoProduto.id) || $scope.insumoProduto.id == null || $scope.insumoProduto.id <= 0) {
                        baseInsumoProduto.post($scope.insumoProduto).then(function() {
                            growl.success(translate("message.success.MSG006_REGISTRO_GRAVADO_SUCESSO"));
                            $scope.formInputsAndProducts.$setPristine();
                            $state.go('base.project.ice.claimTpl', {
                                'screen': 'editar',
                                'id': $stateParams.id
                            });
                        }, function() {
                            growl.error("Ocorreu um erro ao tentar gravar o registro!");
                        });
                    } else {
                        $scope.insumoProduto.put().then(function() {
                            growl.success(translate("message.success.MSG006_REGISTRO_GRAVADO_SUCESSO"));
                            $scope.formInputsAndProducts.$setPristine();
                            $state.go('base.project.ice.claimTpl', {
                                'screen': 'editar',
                                'id': $stateParams.id
                            });
                        }, function() {
                            growl.error("Ocorreu um erro ao tentar gravar o registro!");
                        });
                    }
                } catch (err) {
                    growl.error("Erro: " + err.message);
                }
            } else {
                growl.error($filter('translate')("message.error.MSG69_DADOS_NAO_INFORMADOS_SECAO"));
            }
        }
    }).constant('CHANGE_INSUMOS', {
        PARCERIAS: {
            posicao: 0,
            descricao: "Parcerias"
        },
        PERCENTUALADQUIRIDOOUTRO: {
            posicao: 1,
            descricao: "Percentual Adquirido Outro"
        },
        PERCENTUALCLIENTECOMERCIAL: {
            posicao: 2,
            descricao: "Percentual Cliente Comercial"
        },
        PERCENTUALCLIENTECONSUMIDOR:{
            posicao: 3,
            descricao: "Percentual Cliente Consumidor"
        },
        PERCENTUALCLIENTEINDUSTRIA: {
            posicao: 4,
            descricao: "Percentual Cliente Industria"
        },
        PERCENTUALFABRICAMINAS: {
            posicao: 5,
            descricao: "Percentual Fabrica Minas"
        },
        PERCENTUALICMS: {
            posicao: 6,
            descricao: "Percentual ICMS"
        },
        PERCENTUALIMPORTADO: {
            posicao: 7,
            descricao: "Percentual Importado"
        },
        INSUMOS: {
            posicao: 8,
            descricao: "Insumos"
        },
        INTERESSEPARCERIA: {
            posicao: 9,
            descricao: "Interesse Parceria"
        },
        IMPORTACAOPRODUTO: {
            posicao: 10,
            descricao: "Importacao Produto"
        },
        CONTRIBUINTESUBSTITUTO: {
            posicao: 11,
            descricao: "Contribuinte Substituto"
        },
        CONCORRENTES: {
            posicao: 12,
            descricao: "Concorrentes"
        },
        AQUISICAOOUTROESTADO: {
            posicao: 13,
            descricao: "Aquisicao Outro Estado"
        },
        SERVICOS: {
            posicao: 14,
            descricao: "Servicos"
        },
        PRODUCOES: {
            posicao: 15,
            descricao: "Producoes"
        },
        PRODUTOS: {
            posicao: 16,
            descricao: "Produtos"
        },
        PRODUTOSADQECOMPORMINAS: {
            posicao: 17,
            descricao: "Produtos Adq E Com Por Minas"
        },
        PRODUTOSADQOUTROSESTADOSPARACOM: {
            posicao: 18,
            descricao: "Produtos Adq Outros Estados Para Com"
        },
        PRODUTOSFABECOMPORMINAS: {
            posicao: 19,
            descricao: "Produtos Fab E Com Por Minas"
        },
        PRODUTOSIMPPARACOM: {
            posicao: 20,
            descricao: "Produtos Imp Para Com"
        },
        PERCENTUALMERCADOEXTERIOR: {
            posicao: 21,
            descricao: "Percentual Mercado Exterior"
        },
        PERCENTUALMERCADOMINAS: {
            posicao: 22,
            descricao: "Percentual Mercado Minas"
        },
        PERCENTUALMERCADONACIONAL: {
            posicao: 23,
            descricao: "Percentual Mercado Nacional"
        },
        PERCENTUALORIGEMIMPORTADO: {
            posicao: 24,
            descricao: "Percentual Origem Importado"
        },
        PERCENTUALORIGEMMINAS: {
            posicao: 25,
            descricao: "Percentual Origem Minas"
        },
        PERCENTUALORIGEMOUTROS: {
            posicao: 26,
            descricao: "Percentual Origem Outros"
        },
        PERCENTUALTRIBUTOEFETIVO: {
            posicao: 27,
            descricao: "Percentual Tributo Efetivo"
        },
        PERCENTUALVALORAGREGADO: {
            posicao: 28,
            descricao: "Percentual Valor Agregado"
        }
    });