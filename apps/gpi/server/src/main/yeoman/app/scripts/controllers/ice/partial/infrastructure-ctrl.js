'use strict';

angular.module('gpifrontApp')
    .controller('InfraestruturaCtrl', function($scope, $stateParams, Restangular, growl, CADEIA_PRODUTIVA, $state, ScreenService, AUTH_ROLES, AuthService, ESTAGIO_PROJETO, SITUACAO_PROJETO, TIPO_MEIO_AMBIENTE, $filter, CHANGE_INFRAESTRUTURA) {

        $scope.screenService = ScreenService;

        var baseProjetos = Restangular.all('projeto');

        $scope.energiaContratadas = [];
        $scope.energiaContratadasSelectedItems = [];
        $scope.infraestrutura = {};
        $scope.changes = [];
        $scope.change_infraestrutura = CHANGE_INFRAESTRUTURA;

        baseProjetos.one($stateParams.id).get().then(function(result) {
            setTimeout(function() {
                $scope.projeto = result;
                $scope.situacaoProjeto = SITUACAO_PROJETO[$scope.projeto.situacao];
                $scope.estagioProjeto = ESTAGIO_PROJETO[$scope.projeto.estagioAtual];
                $scope.cadeiaProdutiva = CADEIA_PRODUTIVA;
                if ($stateParams.screen != null) {
                    if ($stateParams.screen != $scope.type.INCLUIR && $stateParams.id) {
                        baseProjetos.one($stateParams.id).one("infraestrutura").getList().then(function(infraestruturas) {
                            if (infraestruturas === undefined || infraestruturas.length == 0){
                                $scope.infraestrutura = {
                                    possuiTerreno :true,
                                    areaAlagada :true,
                                    contatoCemig :true,
                                    energiaJaContratada : true,
                                    usuarioResponsavel: {
                                        nome: ''
                                    },
                                    dataUltimaAlteracao: ''};
                            } else {
                                $scope.infraestrutura = infraestruturas[0];
                                if (infraestruturas[0].energiaContratadas && infraestruturas[0].energiaContratadas.length > 0) {
                                    $scope.energiaContratadas = infraestruturas[0].energiaContratadas;
                                }
                            }

                            if (!$scope.externalUser() && $scope.projeto.situacaoAtual.situacao === SITUACAO_PROJETO.PENDENTE_ACEITE.id) {
                                baseProjetos.one($stateParams.id).getList("infraestrutura/findChange").then(function(result) {
                                    $scope.changes = (result[0] === undefined ? 0 : result[0]);
                                }, function() {
                                    growl.error("Ocorreu um erro ao tentar buscar o registro!");
                                });
                            }                            
                        });
                    }
                }
            }, true);
        });

        $scope.disableNewFn = function() {
            return AuthService.isUserAuthorized([AUTH_ROLES.informationAnalist, AUTH_ROLES.director, AUTH_ROLES.generalSecretary]);
        }

        $scope.externalUser = function() {
            return AuthService.isUserAuthorized([AUTH_ROLES.externalResponsible, AUTH_ROLES.externalMeling, AUTH_ROLES.externalOther]);
        }

        function validateEnergiaContratadas() {
            if ($scope.energiaContratadas !== undefined) {
                $scope.infraestrutura.energiaContratadas = $scope.energiaContratadas;
                for (var i = $scope.infraestrutura.energiaContratadas.length - 1; i >= 0; i--) {
                    if ($scope.infraestrutura.energiaContratadas[i].contrato == undefined ||
                        $scope.infraestrutura.energiaContratadas[i].nome == undefined ||
                        $scope.infraestrutura.energiaContratadas[i].localizacao == undefined) {
                        return false;
                    }
                };
            }
            return true;
        }

        $scope.saveInfra = function() {
            if ($scope.projeto) {
                if (!validateEnergiaContratadas()) {
                    growl.error($filter('translate')("message.error.MSG69_DADOS_NAO_INFORMADOS_SECAO"));
                    return;
                }

                baseProjetos.one($stateParams.id).post("infraestrutura", $scope.infraestrutura).then(function() {
                    growl.success("Registro gravado com sucesso!");
                    $scope.changeScreen();
                }, function() {
                    growl.error("Ocorreu um erro ao tentar gravar o registro!");
                });
            } else {
                growl.error("Modo de tela invalido!");
            }
        }

        $scope.changeScreen = function() {
            // Utilizado para permitir que o usuário mude de página
            // sem a mensagem de perca de dados
            $scope.formInfrastructure.$setPristine();
            // Após salvar vai para a aba de Meio Ambiente
            $state.go('base.project.ice.environmentTpl', {
                'screen': $scope.screenService.getScreenName(),
                'id': $stateParams.id
            });
        }

    }).constant('CHANGE_INFRAESTRUTURA', {
        POSSUITERRENO: {
            posicao: 0,
            descricao: "Possui Terreno"
        },
        POTENCIAENERGETICAESTIMADA: {
            posicao: 1,
            descricao: "Potencia Energetica Estimada"
        },
        AREAALAGADA: {
            posicao: 2,
            descricao: "Area Alagada"
        },
        AREACONSTRUIDA:{
            posicao: 3,
            descricao: "Area Construida"
        },
        AREANECESSARIA: {
            posicao: 4,
            descricao: "Area Necessaria"
        },
        CONSUMOAGUAESTIMADO: {
            posicao: 5,
            descricao: "Consumo Agua Estimado"
        },
        CONSUMOCOMBUSTIVELESTIMADO: {
            posicao: 6,
            descricao: "Consumo Combustivel Estimado"
        },
        CONSUMOGASESTIMADO: {
            posicao: 7,
            descricao: "Consumo Gas Estimado"
        },
        CONTATOCEMIG: {
            posicao: 8,
            descricao: "Contato Cemig"
        },
        ENERGIACONTRATADAS: {
            posicao: 9,
            descricao: "Energia Contratadas"
        },
        ENERGIAJACONTRATADA: {
            posicao: 10,
            descricao: "Energia Ja Contratada"
        },
        DEMANDAENERGIAESTIMADA: {
            posicao: 11,
            descricao: "Demanda Energia Estimada"
        },
        GERACAOENERGIAPROPRIA: {
            posicao: 12,
            descricao: "Geracao Energia Propria"
        },
        GERACAOENERGIAVENDA: {
            posicao: 13,
            descricao: "Geracao Energia Venda"
        },
        OFERTAENERGIAVENDAESTIMADA: {
            posicao: 14,
            descricao: "Oferta Energia Venda Estimada"
        }
    });