'use strict';

angular.module('gpifrontApp')
    .controller('CompleteReportAnnualMonitoringCtrl', function($scope, growl, ScreenService, $location, Restangular, SITUACAO_PROJETO, ESTAGIO_PROJETO, 
    	TIPO_USUARIO, UserSession, $filter, $state, SITUACAO_RELATORIO, $http, configuration, DESCRICAO_FASE) {

	   	var projetoId = $location.search() ? $location.search().id : undefined;

        $scope.screenService = ScreenService;

        $scope.isExternalUser = UserSession.getUser().tipo == TIPO_USUARIO.EXTERNO;

        $scope.gridType = 'ACOMPANHAMENTO'
        
        $scope.relatorioAcompanhamento = {
        	investimentos:[], faturamentos:[], calendarios:[], empregos:[], projeto: {},relatorioAcompanhamento: {}
        }
 
		$scope.relatorioAcompanhamentoList = []

        $scope.relatorio = {
        	investimentos: [], faturamentos: [], calendarios: [], empregos: [], relatorioAcompanhamento: { anexos: [] }
       	}       	

       	$scope.files = [];

    	$scope.gridScope = {
    		disableObservation: function() {
    			$("#observations-modal textarea").prop("disabled", true);
    			$('#save-button').css('display', 'none');
    			$('#close-button').prop('disabled', false);
    		}    		
    	}    	

        $scope.selectedItems = []    	
 
        $scope.columnDefs = [{
            field: 'ano',
            enableCellEdit: false,
            width: "10%",
            translationKey: 'common.year'
        }, {
            field: 'nomeResponsavelRelatorio',
            enableCellEdit: false,
            width: "30%",
            translationKey: 'common.name'
        }, {
            field: 'emailResponsavelRelatorio', 
            enableCellEdit: false,
            width: "30%",
            translationKey: 'common.email'
        },{
            field: 'telefoneResponsavelRelatorio',
            enableCellEdit: false,
            width: "15%",
            translationKey: 'common.phone'
        },{
            field: "observacao",
            translationKey: 'common.observation',
            cellTemplate: '<div class="text-center" ng-class="col.colIndex()">' +
                '<a ng-click="showActivityObservation(row.entity); disableObservation();" data-toggle="modal" data-target="#observations-modal"><i class="fa fa-comments-o page-header-icon"></i></a></fieldset></div>',
            width: 80,
            enableCellEdit: false
        }];      


       	function init() {
            $scope.hasReport = false;
	        if (projetoId != undefined) {            
	            Restangular.one('projeto', projetoId).one("projetoTO").get().then(function(result) {

	            	try {
		                $scope.projetoTO = result;
		                $scope.situacaoProjeto = SITUACAO_PROJETO[$scope.projetoTO.situacao];
		                $scope.estagioProjeto = ESTAGIO_PROJETO[$scope.projetoTO.estagio];

						Restangular.all("projeto").get("acompanhamentoAnual", {
                            familiaId: $scope.projetoTO.versaoPai, 
                            projetoId: $scope.projetoTO.id
                        }).then(function(result){
							$scope.relatorio = result;
                            $scope.hasReport = true;
                            
							if ($scope.relatorio.relatorioAcompanhamento 
									&& $scope.relatorio.relatorioAcompanhamento.anexos 
									&& $scope.relatorio.relatorioAcompanhamento.anexos.length > 0) {
								$scope.files = $scope.relatorio.relatorioAcompanhamento.anexos;	
							}					

                            var params = { familiaId: $scope.projetoTO.versaoPai }                       

							if ($scope.relatorio.relatorioAcompanhamento) {
								params.ano = $scope.relatorio.relatorioAcompanhamento.ano
							}
			                
							Restangular.all("projeto/acompanhamentoAnual/anosanteriores").getList(params).then(function(result){
								$scope.relatorioAcompanhamentoList = result;
							})

						}, function(response) {
		                    growl.error(response.data.message);
		                });				

	            	} catch (err) {
	            		growl.error(err);
	            	}
	            });
	        } else {
                growl.error("É necessário informar um ID de projeto na URL!");
            }	        
       	}

       	$scope.hasNotRelatorioAcompanhamento = function() {
       		return !$scope.relatorio.relatorioAcompanhamento;
       	}

        $scope.isProjetoAtivoAndEstagioInIIOIDF = function() {
        	return $scope.projetoTO 
        		&& SITUACAO_PROJETO[$scope.projetoTO.situacao] === SITUACAO_PROJETO['ATIVO']
        		&& (ESTAGIO_PROJETO[$scope.projetoTO.estagio] === ESTAGIO_PROJETO['FORMALIZADA']
        			|| ESTAGIO_PROJETO[$scope.projetoTO.estagio] === ESTAGIO_PROJETO['IMPLANTACAO_INICIADO']
        			|| ESTAGIO_PROJETO[$scope.projetoTO.estagio] === ESTAGIO_PROJETO['OPERACAO_INICIADA']);  
        }

        $scope.isResponsavelPeloInvestimento = function() {
        	return $scope.projetoTO 
        		&& $scope.projetoTO.responsavelInvestimento 
        		&& $scope.projetoTO.responsavelInvestimento.id === UserSession.getUser().id;
        }

        $scope.isUsuarioExternoResponsavel = function() {
        	return $scope.relatorio 
                && $scope.relatorio.usuarioExternoResponsavelId === UserSession.getUser().id;
        }

        $scope.isPendenteValidacao = function() {
        	return $scope.relatorio 
        		&& $scope.relatorio.relatorioAcompanhamento 
        		&& $scope.relatorio.relatorioAcompanhamento.situacaoAtual == SITUACAO_RELATORIO["PENDENTE_VALIDACAO"].id;
        }

        $scope.isValidado = function() {
        	return $scope.relatorio 
        		&& $scope.relatorio.relatorioAcompanhamento 
        		&& $scope.relatorio.relatorioAcompanhamento.situacaoAtual == SITUACAO_RELATORIO["VALIDADO"].id;
        }

		$scope.isAnoCampanha = function(ano) {
			if ($scope.relatorio && $scope.relatorio.relatorioAcompanhamento) {
				return $scope.relatorio.relatorioAcompanhamento.ano == ano
			}
        	return false;
    	}

    	$scope.isTratamentoTributario = function(faseId) {
        	return faseId == DESCRICAO_FASE.INICIO_TRATAMENTO_TRIBUTARIO.id;
        }

    	$scope.startField = function(field, list) {
    		try {
    			var anoCampanha = $scope.relatorio.relatorioAcompanhamento.ano;

	    		if (!list || list.length == 0) {	    			
	    			return (field == 'ano') ? anoCampanha : 0;
	    		}

	    		if (field == 'ano') {
	        		var maiorAno = 0;
	        		for (var i = 0; i < list.length; i++) { 
	        			if (maiorAno < list[i].ano) {
							maiorAno = list[i].ano;
	        			}
	        		}
	        		return (maiorAno >= anoCampanha) ? anoCampanha : maiorAno + 1;        			
	    		}

    		} catch(err) {
    			return (field == 'ano') ? anoCampanha : 0;
    		}

    		return 0;
    	}

		$scope.isGreaterOrEqualsThanAnoCampanha = function(ano) {
			if ($scope.relatorio && $scope.relatorio.relatorioAcompanhamento) {
				return ano >= $scope.relatorio.relatorioAcompanhamento.ano
			}
        	return false;
    	}

		$scope.isGreaterThanAnoCampanha = function(ano) {			
			if ($scope.relatorio && $scope.relatorio.relatorioAcompanhamento) {
				return ano > $scope.relatorio.relatorioAcompanhamento.ano
			}
        	return false;
    	}        

    	$scope.canEdit = function() {    		
    		return ($scope.isUsuarioExternoResponsavel() || $scope.isResponsavelPeloInvestimento()) 
    			&& !$scope.hasNotRelatorioAcompanhamento()
    			&& !$scope.isValidado() 
    			&& $scope.isProjetoAtivoAndEstagioInIIOIDF()
                && !ScreenService.isShowScreen();
    	}

        $scope.preValidate = function() {
        	/* RNE245 - Somente o responsável pelo investimento, gerente do analista responsável e investidor terão acesso 
        	a edição antes ou após a Validação. Após a Validação, os campos preenchidos pelo usuário externo (investidor) 
        	não poderão mais serem editados. */

			/* Data de Início da Utilização do Tratamento Tributário - Editável somente os anos solicitados durante as chamadas/campanhas, 
			enquanto ainda não forem validados pelo analista de promoção ou gerente */

			for (var i = 0; i < $scope.relatorio.investimentos.length; i++) {

				var investimento = $scope.relatorio.investimentos[i];

	        	/* EX56_ANO_NAO_PERMITIDO - Caso exista algum campo ANO superior ao ano referente a campanha de preenchimento do 
	        	relatório ou ao inserir anos manualmente o usuário tenha saltado a sequência de anos. */
				// if($scope.isGreaterThanAnoCampanha(investimento.ano) && !investimento.id) {
	   //              growl.error($filter('translate')('message.error.MSG95_ANO_SUPERIOR_CAMPANHA', {
	   //                    	ano: investimento.ano
	   //                	}));
				// 	return;						
				// }

				for (var j = 0; j < $scope.relatorio.investimentos.length; j++) {

					/* EX55_ANO_DUPLICADO - Caso exista algum campo ANO informado repetidamente. */
					if ($scope.relatorio.investimentos[j].ano == investimento.ano && j != i) {						
		                growl.error($filter('translate')('message.error.MSG94_ANO_DUPLICADO', {
  	                      	ano: investimento.ano, 
  	                      	secao: "Investimentos"
  	                  	}));
						return;
					} 
				}
			}				

			for (var i = 0; i < $scope.relatorio.faturamentos.length; i++) {

				var faturamento = $scope.relatorio.faturamentos[i];

	        	/* EX56_ANO_NAO_PERMITIDO - Caso exista algum campo ANO superior ao ano referente a campanha de preenchimento do 
	        	relatório ou ao inserir anos manualmente o usuário tenha saltado a sequência de anos. */
				// if($scope.isGreaterThanAnoCampanha(faturamento.ano)  && !faturamento.id) {
	   //              growl.error($filter('translate')('message.error.MSG95_ANO_SUPERIOR_CAMPANHA', {
	   //                    	ano: faturamento.ano
	   //                	}));
				// 	return;						
				// }

				for (var j = 0; j < $scope.relatorio.faturamentos.length; j++) {

					/* EX55_ANO_DUPLICADO - Caso exista algum campo ANO informado repetidamente. */
					if ($scope.relatorio.faturamentos[j].ano == faturamento.ano  && j != i) {
		                growl.error($filter('translate')('message.error.MSG94_ANO_DUPLICADO', {
		                    ano: faturamento.ano, 
		                    secao: "Faturamento"
  	                  	}));
						return;
					}
				}
			}

			for (var i = 0; i < $scope.relatorio.empregos.length; i++) {

				var emprego = $scope.relatorio.empregos[i];

	        	/* EX56_ANO_NAO_PERMITIDO - Caso exista algum campo ANO superior ao ano referente a campanha de preenchimento do 
	        	relatório ou ao inserir anos manualmente o usuário tenha saltado a sequência de anos. */
				// if($scope.isGreaterThanAnoCampanha(emprego.ano) && !emprego.id) {
	   //              growl.error($filter('translate')('message.error.MSG95_ANO_SUPERIOR_CAMPANHA', {
	   //                    	ano: emprego.ano
	   //                	}));
				// 	return;						
				// }

				for (var j = 0; j < $scope.relatorio.empregos.length; j++) {

					/* EX55_ANO_DUPLICADO - Caso exista algum campo ANO informado repetidamente. */
					if ($scope.relatorio.empregos[j].ano == emprego.ano  && j != i) {
		                growl.error($filter('translate')('message.error.MSG94_ANO_DUPLICADO', {
  	                      ano: emprego.ano, secao: "Empregos"
  	                  	}));
						return;
					}
				}
			}

			return true;

        }

        $scope.validate = function() {

        	if (!$scope.preValidate()) {
        		return;
        	}

        	/* O GPI valida os dados de todas as campanhas de preenchimento anteriores para este projeto caso não tenham aceite,
        	armazena as informações, exibe a mensagem MSG006_REGISTRO_GRAVADO_SUCESSO e permanece na aba “Relatório Anual de Acompanhamento”. */
        	
        	try {
				Restangular.all("projeto/acompanhamentoAnual").get("validar", {relatorioId: $scope.relatorio.relatorioAcompanhamento.id}).then(function(result){
					$scope.relatorio.relatorioAcompanhamento = result;
					growl.info("Relatorio validado com sucesso!");
					return;
				}, function() {
	                growl.error("Ocorreu um erro ao tentar validar o relatório!");
	                return;
	            });				
        	} catch(err) {
                growl.error(err.message);
                return;
        	}        	

        }        

        $scope.print = function() {
            var formData = $scope.relatorio;
            formData.projeto = $scope.projetoTO;
           
            if(formData.relatorioAcompanhamento === null){
                formData.relatorioAcompanhamento = $scope.relatorioAcompanhamento;
            }  
            $http({method:"POST", data: formData,
                responseType: "arraybuffer",
                url: configuration.localPath + "api/projeto/acompanhamentoAnual/" + "print/pdf",
                headers: {Accept: "application/pdf"} }).success(function(response) {
                    ScreenService.browserAwareSave( response, "acompanhamento.pdf","application/pdf" );
                });
        }

		/* RNE240 - Todas as informações devem ser recuperadas e persistidas na versão corrente do projeto, exceto as informações 
		   de compromisso previstas, que devem ser recuperadas da última versão congelada, são dados de cronograma, investimento, 
		   faturamento e empregos. Lembrando que estas informações deverão ser versionadas junto com o projeto, conforme RNE89. */


		/* RNE243 - Sempre que o usuário responsável pelo investimento logar no sistema, deverá ser verificado se existem 
		   "Relatórios de Acompanhamento" preenchidos pelo investidor que ainda não foram validados, e assim emitir um 
		   alerta para cada relatório, alerta: Relatório de Acompanhamento do Projeto [Nome do Projeto] está disponível 
		   para validação. com um link que deverá redirecionar o usuário diretamente para este relatório. */       


		/* RNE245 - Somente o responsável pelo investimento, gerente do analista responsável e investidor terão acesso 
		   a edição antes ou após a Validação. Após a Validação, os campos preenchidos pelo usuário externo (investidor) 
		   não poderão mais serem editados. */

        $scope.save = function() {

        	if (!$scope.preValidate()) {
        		return;
        	}

        	$scope.relatorio.relatorioAcompanhamento.anexos = $scope.files;

        	Restangular.all("projeto/acompanhamentoAnual").post($scope.relatorio).then(function() {
                    growl.success("Registro gravado com sucesso!");
                    $scope.formAnnualMonitorProject.$setPristine();
                    // $state.go("base.project.projectTpl", {"id": projetoId, "screen": $state.params.screen});
                },
                function(err) {
                    growl.error(err.data.message);
                });
        	
        } 

        $scope.checkUserAuthorization();
		init();

	})
	.controller('SchedulerGridCtrl', function($scope, DESCRICAO_FASE, GridButton, growl, ScreenService){

		$scope.schedulerList = $scope.$parent.relatorio.calendarios;
        $scope.schedulerSelectedItems = []

        var canEdit = $scope.$parent.canEdit;

        $scope.configsDatepicker = {
            required: true,
            disabled: ScreenService.isShowScreen()
        };

        $scope.gridScope = {
        	descFase: DESCRICAO_FASE,
        	isTratamentoTributario: $scope.$parent.isTratamentoTributario,
        	configsDatepicker: $scope.configsDatepicker,
        	canEdit: canEdit
        }        

        $scope.schedulerButtonsConfig = {            
            newButton: new GridButton(false, 'common.new', false, null, null, ''),
            deleteButton: new GridButton(false, 'common.delete', false, null, null, '')
        }

        $scope.sortInfo = { fields: ['ano'], directions: ['desc'] }

		$scope.schedulerColumnDefs = [{
			field: 'descricaoFaseEnum',		
            enableCellEdit: false,
            width: "*",
            cellTemplate: '<div>{{descFase[COL_FIELD].descricao}}</div>',
            translationKey: 'common.phase'			
		}, {
			field: 'dataPrevisto',
            enableCellEdit: false,
            width: "*",
            cellFilter: 'dateTransformFilter',   
            // editableCellTemplate: '<div ng-edit-cell-if="isTratamentoTributario(row.entity.descricaoFaseEnum)"><input type="text" ui-mask="99/99/9999" maxlength="10" ng-class="\'colt\' + col.index"  ng-input="COL_FIELD" ng-model="COL_FIELD"/></div>',                                                         
            translationKey: 'common.predicted',			
            cellTemplate: "<div ng-show=\"!isTratamentoTributario(row.entity.descricaoFaseEnum) || !canEdit()\" class=\"ngCellText\" ng-class=\"col.colIndex()\"><span ng-cell-text>{{row.entity.dataPrevisto | dateTransformFilter}}</span></div>" +
                "<span ng-if=\"isTratamentoTributario(row.entity.descricaoFaseEnum) && canEdit()\"><datepicker-custom id=\"datepicker\" date=\"COL_FIELD\" configs=\"configsDatepicker\"></datepicker-custom></span>"

		}, {
			field: 'dataRealizado',
            enableCellEdit: false,
            width: "*",
            cellFilter: 'dateTransformFilter',   
            // editableCellTemplate: '<div ng-edit-cell-if="isTratamentoTributario(row.entity.descricaoFaseEnum)"><input type="text" ui-mask="99/99/9999"  maxlength="10" ng-class="\'colt\' + col.index"  ng-input="COL_FIELD" ng-model="COL_FIELD"/></div>',                                                                                             
            translationKey: 'common.realized',
            cellTemplate: "<div ng-show=\"!isTratamentoTributario(row.entity.descricaoFaseEnum) || !canEdit()\" class=\"ngCellText\" ng-class=\"col.colIndex()\"><span ng-cell-text>{{row.entity.dataRealizado | dateTransformFilter}}</span></div>" +
                "<span ng-if=\"isTratamentoTributario(row.entity.descricaoFaseEnum) && canEdit()\"><datepicker-custom id=\"datepicker\" date=\"COL_FIELD\" configs=\"configsDatepicker\"></datepicker-custom></span>"

		}];

        $scope.$parent.$watch('relatorio', function(newVal, oldVal){
            if ($scope.$parent.relatorio && $scope.$parent.relatorio.calendarios) {
                setTimeout(function(){
                    $scope.schedulerList = newVal.calendarios;  
                }, true);                
            }                                
        }, true);		

	})
	.controller('InvestmentsGridCtrl', function($scope, GridButton) {

		$scope.investmentsList = $scope.$parent.relatorio.investimentos;
        $scope.investmentsSelectedItems = []

        var isAnoCampanha = $scope.$parent.isAnoCampanha
        var isGreaterOrEqualsThanAnoCampanha = $scope.$parent.isGreaterOrEqualsThanAnoCampanha
		var startField = $scope.$parent.startField

        $scope.gridScope = {
        	isAnoCampanha: isAnoCampanha,
        }

        $scope.investmentsButtonsConfig = {            
            newButton: new GridButton(true, 'common.new', false, disabledNewFn, newFn, ''),
            deleteButton: new GridButton(true, 'common.delete', false, disableDeleteFn, null, '')
        }

        $scope.sortInfo = { fields: ['ano'], directions: ['desc'] }

        $scope.disableFooter = true;

		$scope.investmentsColumnDefs = [{
			field: 'ano',		
            enableCellEdit: false,
            width: "*",
            editableCellTemplate: '<input type="numeric" ui-mask="9999" maxlength="4" ng-class="\'colt\' + col.index"  ng-input="COL_FIELD" ng-model="COL_FIELD"/>',                                    
            translationKey: 'common.investments'			
		}, {
			field: 'valorPrevisto',
            enableCellEdit: false,
            width: "*",
            cellFilter: "currency:''",
            editableCellTemplate: '<input type="text" format="currency" maxlength="21" ng-class="\'colt\' + col.index"  ng-input="COL_FIELD" ng-model="COL_FIELD"/>',                                                                        
            translationKey: 'common.predicted'			
		}, {
			field: 'valorRealizado',
            enableCellEdit: true,
            width: "*",
            cellFilter: "currency:''",
            // editableCellTemplate: '<input type="text" format="currency" maxlength="21" ng-class="\'colt\' + col.index"  ng-input="COL_FIELD" ng-model="COL_FIELD"/>',
            editableCellTemplate: '<input ng-disabled="!isAnoCampanha(row.entity.ano)" type="text" format="currency" maxlength="21" ng-class="\'colt\' + col.index"  ng-input="COL_FIELD" ng-model="COL_FIELD"/>',                                                                                                
            translationKey: 'common.realized'			
		}, {
			field: 'valorPrevisto',		
            enableCellEdit: false,
            width: "*",            
            cellTemplate: '<div>{{row.entity.valorRealizado > 0 ? row.entity.valorRealizado / row.entity.valorPrevisto * 100 : 0 | currency:""}}</div>',
            translationKey: 'common.percent'
		}];

		function newFn() {
			$scope.investmentsList.unshift({
				ano: startField('ano', $scope.investmentsList), 
				valorPrevisto: startField('valorPrevisto', $scope.investmentsList),
				novo: true
			});
			return false;
		}

        function disableDeleteFn() {
        	var ret = false;
        	for (var i = 0; i < $scope.investmentsSelectedItems.length; i++) {        		
        		var investimentoSelecionado = $scope.investmentsSelectedItems[i];
        		if (!investimentoSelecionado.novo) {
        			ret = true;
        		}
        	}
        	return ret;
        }

		function disabledNewFn() {
			var maiorAno = 0;
        	for (var i = 0; i < $scope.investmentsList.length; i++) {        		
        		var investimento = $scope.investmentsList[i];
        		maiorAno = (investimento.ano > maiorAno) ? investimento.ano : maiorAno;        		
        	}
        	return isGreaterOrEqualsThanAnoCampanha(maiorAno);
		}

        $scope.$parent.$watch('relatorio', function(newVal, oldVal){
            if ($scope.$parent.relatorio && $scope.$parent.relatorio.investimentos) {
                setTimeout(function(){
                    $scope.investmentsList = newVal.investimentos;                    

                    $scope.totalInvestimentoPrevisto = 0;                    
                    $.each($scope.investmentsList, function() {
                        $scope.totalInvestimentoPrevisto += this.valorPrevisto;
                    })              

                    $scope.totalInvestimentoRealizado = 0;                    
                    $.each($scope.investmentsList, function() {
                        $scope.totalInvestimentoRealizado += this.valorRealizado;
                    })                    

                }, true);                
            }                                
        }, true);		

	})
	.controller('BillingsGridCtrl', function($scope, GridButton){

		$scope.billingsList = $scope.$parent.relatorio.faturamentos
        $scope.billingsSelectedItems = []

        var isAnoCampanha = $scope.$parent.isAnoCampanha
        var isGreaterOrEqualsThanAnoCampanha = $scope.$parent.isGreaterOrEqualsThanAnoCampanha                
		var startField = $scope.$parent.startField

        $scope.gridScope = {
        	isAnoCampanha: isAnoCampanha
        }

        $scope.billingsButtonsConfig = { 
            newButton: new GridButton(true, 'common.new', false, disabledNewFn, newFn, ''),
            deleteButton: new GridButton(true, 'common.delete', false, disableDeleteFn, null, '')
        }

        $scope.sortInfo = { fields: ['ano'], directions: ['desc'] }

        $scope.disableFooter = true;

		$scope.billingsColumnDefs = [{
			field: 'ano',		
            enableCellEdit: false,
            editableCellTemplate: '<input type="numeric" ui-mask="9999" maxlength="4" ng-class="\'colt\' + col.index"  ng-input="COL_FIELD" ng-model="COL_FIELD"/>',                        
            width: "*",
            translationKey: 'common.billing'			
		}, {
			field: 'valorPrevisto',
            enableCellEdit: false,
            width: "*",
            cellFilter: "currency:''",
            editableCellTemplate: '<input type="text" format="currency" maxlength="21" ng-class="\'colt\' + col.index"  ng-input="COL_FIELD" ng-model="COL_FIELD"/>',                                                                                    
            translationKey: 'common.predicted'			
		}, {
			field: 'valorRealizado',
            enableCellEdit: true,
            width: "*",
            cellFilter: "currency:''",
            // editableCellTemplate: '<input type="text" format="currency" maxlength="21" ng-class="\'colt\' + col.index"  ng-input="COL_FIELD" ng-model="COL_FIELD"/>',                                                                                                
            editableCellTemplate: '<input ng-disabled="!isAnoCampanha(row.entity.ano)" type="text" format="currency" maxlength="21" ng-class="\'colt\' + col.index"  ng-input="COL_FIELD" ng-model="COL_FIELD"/>',                                                                                                            
            translationKey: 'common.realized'			
		}, {
			field: 'valorPrevisto',		
            enableCellEdit: false,
            width: "*",            
            cellTemplate: '<div>{{row.entity.valorRealizado > 0 ? row.entity.valorRealizado / row.entity.valorPrevisto * 100 : 0 | currency:""}}</div>',
            translationKey: 'common.percent'
		}];

		function newFn() {
			$scope.billingsList.unshift({
				ano: startField('ano', $scope.billingsList), 				
				valorPrevisto: startField('valorPrevisto', $scope.billingsList),
				novo: true
			});
			return false;
		}

        function disableDeleteFn() {
        	var ret = false;        	
        	for (var i = 0; i < $scope.billingsSelectedItems.length; i++) {        		
        		var faturamentoSelecionado = $scope.billingsSelectedItems[i];
        		if (!faturamentoSelecionado.novo) {
        			ret = true;
        		}
        	}
            return ret;
        }

		function disabledNewFn() {
			var maiorAno = 0;
        	for (var i = 0; i < $scope.billingsList.length; i++) {        		
        		var faturamento = $scope.billingsList[i];
        		maiorAno = (faturamento.ano > maiorAno) ? faturamento.ano : maiorAno;        		
        	}
        	return isGreaterOrEqualsThanAnoCampanha(maiorAno);
		}

        $scope.$parent.$watch('relatorio', function(newVal, oldVal){
            if ($scope.$parent.relatorio && $scope.$parent.relatorio.faturamentos) {
                setTimeout(function(){
                    $scope.billingsList = newVal.faturamentos;  

                    $scope.totalFaturamentoPrevisto = 0;                    
                    $.each($scope.billingsList, function() {
                        $scope.totalFaturamentoPrevisto += this.valorPrevisto;
                    })              

                    $scope.totalFaturamentoRealizado = 0;                    
                    $.each($scope.billingsList, function() {
                        $scope.totalFaturamentoRealizado += this.valorRealizado;
                    })                    

                }, true);                
            }                                
        }, true);		


	})			
	.controller('JobsGridCtrl', function($scope, GridButton){

		$scope.jobsList = []
        $scope.jobsSelectedItems = []

        var isAnoCampanha = $scope.$parent.isAnoCampanha
        var isGreaterOrEqualsThanAnoCampanha = $scope.$parent.isGreaterOrEqualsThanAnoCampanha                        
		var startField = $scope.$parent.startField

        $scope.gridScope = {
        	isAnoCampanha: isAnoCampanha
        }        

        $scope.jobsButtonsConfig = {       
            newButton: new GridButton(true, 'common.new', false, disabledNewFn, newFn, ''),
            deleteButton: new GridButton(true, 'common.delete', false, disableDeleteFn, null, '')
        }

        $scope.sortInfo = { fields: ['ano'], directions: ['desc'] }

        $scope.disableFooter = true;

		$scope.jobsColumnDefs = [{
			field: 'ano',		
            enableCellEdit: false,
            editableCellTemplate: '<input type="numeric" ui-mask="9999" maxlength="4" ng-class="\'colt\' + col.index"  ng-input="COL_FIELD" ng-model="COL_FIELD"/>',                                   
            width: "*",
            translationKey: 'common.direct.jobs.created'			
		}, {
			field: 'numeroPrevisto',
            enableCellEdit: false,
            width: "*",
            translationKey: 'common.predicted'			
		}, {
			field: 'numeroRealizado',
            enableCellEdit: true,            
            width: "*",
            // editableCellTemplate: '<input type="numeric" ng-class="\'colt\' + col.index"  ng-input="COL_FIELD" ng-model="COL_FIELD" maxlength="4" />',
            editableCellTemplate: '<input ng-disabled="!isAnoCampanha(row.entity.ano)" type="numeric" ng-class="\'colt\' + col.index"  ng-input="COL_FIELD" ng-model="COL_FIELD" maxlength="4"/>',
            translationKey: 'common.realized'			
		}, {
			field: 'numeroPrevisto',		
            enableCellEdit: false,
            width: "*",            
            cellTemplate: '<div>{{row.entity.numeroRealizado > 0 ? row.entity.numeroRealizado / row.entity.numeroPrevisto * 100 : 0 | currency:""}}</div>',
            translationKey: 'common.percent'
		}];

		function newFn() {
			$scope.jobsList.unshift({
				ano: startField('ano', $scope.jobsList), 
				numeroPrevisto: startField('numeroPrevisto', $scope.jobsList),
				novo: true
			});
			return false;
		}

        function disableDeleteFn() {
        	var ret = false;        	
        	for (var i = 0; i < $scope.jobsSelectedItems.length; i++) {        		
        		var empregoSelecionado = $scope.jobsSelectedItems[i];
        		if (!empregoSelecionado.novo) {
        			ret = true;
        		}
        	}
            return ret;
        }

		function disabledNewFn() {
			var maiorAno = 0;
        	for (var i = 0; i < $scope.jobsList.length; i++) {        		
        		var emprego = $scope.jobsList[i];
        		maiorAno = (emprego.ano > maiorAno) ? emprego.ano : maiorAno;        		
        	}
        	return isGreaterOrEqualsThanAnoCampanha(maiorAno);
		}

        $scope.$parent.$watch('relatorio', function(newVal, oldVal){
            if ($scope.$parent.relatorio && $scope.$parent.relatorio.empregos) {
                setTimeout(function(){
                    $scope.jobsList = newVal.empregos;  

                    $scope.totalEmpregoPrevisto = 0;                    
                    $.each($scope.jobsList, function() {
                        $scope.totalEmpregoPrevisto += this.numeroPrevisto;
                    })              

                    $scope.totalEmpregoRealizado = 0;                    
                    $.each($scope.jobsList, function() {
                        $scope.totalEmpregoRealizado += this.numeroRealizado;
                    })                    

                }, true);                
            }                                
        }, true);				

	})
	.controller('FilesGridCtrl', function($scope, GridButton, growl, dialogs, $filter){

		$scope.files = $scope.$parent.files;
        $scope.filesSelectedFiles = []

		$scope.filesColumnDefs = [{        
            field: 'nome',
            enableCellEdit: false,
            width: "**",
            translationKey: 'common.file.name'
        }, {
            field: 'dataAnexo',
            enableCellEdit: false,
            width: "**",
            translationKey: 'common.date',
            cellFilter: 'dateTransformFilter'
        }];

        function translate(translationKey) {
            var text = "";
            if (typeof translationKey !== 'undefined' && translationKey) {
                text = $filter('translate')(translationKey);
            }
            return text;
        };

        $scope.preDeleteFile = function() {
            var confirmDialog = dialogs.confirm(translate('DIALOGS_CONFIRMATION'), translate('message.confirm.MSG009_CONFIRMAR_EXCLUSAO'));
            confirmDialog.result.then(function(btn) {
                // Caso confirme executa a exclusão
                $scope.deleteFile();
            }, function(btn) {
                $scope.gridScope.checkSelectAllCheckBox(true, true);
                $scope.gridScope.checkSelectAllCheckBox(false, true);
                // Caso não confirme deseleciona os registros selecionados
                $scope.filesSelectedFiles.splice(0, $scope.filesSelectedFiles.length);
            });

        }

        $scope.deleteFile = function() {
            if ($scope.files.length > 0) {
                $scope.filesSelectedFiles.forEach(function(selected) {
                    var model = $scope.$parent.files;
                    var indexOnMainList = model.indexOf(selected);
                    // deleting each unit from main list
                    $scope.$parent.files.splice(indexOnMainList, 1);
                    for (var i = 0; i < $scope.$flow.files.length; i++) {
                        var file = $scope.$flow.files[i];
                        file.cancel();
                    };
                });
                // deleting all 'selectedItems' starting at index 0
                $scope.filesSelectedFiles.splice(0, $scope.filesSelectedFiles.length);
                growl.info($filter('translate')('message.error.MSG38_SECAO_ATUALIZADA', {
                     value: "Arquivos Anexos"
                 }));
            }
        }  

        $scope.disableFn = function () {
            return $scope.filesSelectedFiles.length < 1;
        }     

        $scope.$parent.$watch('files', function(newVal, oldVal) {
            if ($scope.$parent.files != null) {
	            setTimeout(function() {
                    $scope.files = newVal;
	            }, true);
        	}
        }, true);


	})
