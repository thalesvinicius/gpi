'use strict'
angular.module('gpifrontApp')
    .controller('ReportTrackingStageCtrl', function($scope, growl, Restangular, $filter, ESTAGIO_PROJETO, STATUS_PROJETO, AuthService, AUTH_ROLES, UserSession, $http, configuration, ScreenService) {

        $scope.disableHeader = true;
        $scope.filtro = {
            estagios: [],
            cadeiasProdutivas: [],
            regioesPlanejamento: [],
            departamentos: [],
            usuariosResponsavel: []
        };
        $scope.selectedItems = [];
        $scope.sortInfo = {
            fields: ['nomeEmpresa'],
            directions: ['asc']
        };
        $scope.gridScope = {
            options : {
             enableCellSelection : false,  
             selectWithCheckboxOnly: false,
             showSelectionCheckbox: false
            }
        };

        // RNE163 se for gerente não exibe o campo Gerência e se for analista de portfolio
        //  não exibe os campos Gerência e Analista
        $scope.isGerente = function() {
            return AuthService.isUserAuthorized([AUTH_ROLES.manager]);
        }

        // RNE163 se for gerente não exibe o campo Gerência e se for analista de portfolio
        //  não exibe os campos Gerência e Analista
        $scope.isPromocao = function() {
            return AuthService.isUserAuthorized([AUTH_ROLES.promotionAnalist]);
        }

        /*
         *   Recupera todos os ids da lista passada
         */
        var getIdList = function(list) {
            var idList = [];
            if (list !== undefined) {
                if (_.isArray(list)) {
                    _.each(list, function(val, key) {
                        idList.push(val.id);
                    });
                } else {
                    idList.push(list.id);
                }
            }
            return idList;
        }

        $scope.disabledExport = function() {
            return $scope.data.length === 0;
        }

        /*
         *   Evento chamado ao selecionar gerência
         */
        $scope.changeManageMultiSelect = function() {
            //  Busca os usuários com base no departamento
            if ($scope.filtro.departamentos && $scope.filtro.departamentos.length > 0) {
                Restangular.all('user/byDepartamentos').getList({
                    "departamentos": $scope.filtro.departamentos
                }).then(function(analistas) {
                    $scope.analistas = analistas;
                    //  Por default todos os analistas vem selecionados
                    $scope.filtro.usuariosResponsavel = $scope.isPromocao() ? [UserSession.getUser().id] : [];


                });

                Restangular.all('cadeiaProdutiva/byDepartamentos').getList({
                    "departamentos": $scope.filtro.departamentos
                }).then(function(cadeiasProdutivas) {
                    $scope.cadeiasProdutivas = cadeiasProdutivas;
                    //  Por default todas as cadeiasProdutivas vem selecionadas
                    // $scope.filtro.cadeiasProdutivas = getIdList(cadeiasProdutivas);
                });
            } else {
                $scope.analistas = [];
                $scope.cadeiasProdutivas = [];
                $scope.filtro.usuariosResponsavel = [];
                $scope.filtro.cadeiasProdutivas = [];
            }
        }

        /*
         *   Utilizado pelos usuarios GERENTE e PROMOCAO para recuperar as gerencias
         */
        var getDepartamentosUsuarioLogado = function() {
            Restangular.one('user/' + UserSession.getUser().id).get().then(function(user) {
                $scope.filtro.departamentos = getIdList(user.departamento);
                //  Aciona o evento onChange do select de departamentos
                //  para preencher os analistas e as cadeias produtivas
                $scope.changeManageMultiSelect();
            });
        }

        /*  Iniciando defaults do relatório */
        $scope.reset = function() {
            $scope.filtro.versao = true; // por default o radio 'Corrente' vem selecionado

            /*  Inicializando as listas com seus respectivos registros selecionados. */
            // $scope.filtro.estagios = getIdList($scope.estagios);
            $scope.filtro.status = [STATUS_PROJETO.ATIVO.id];

            Restangular.all('domain/regiaoPlanejamento').getList().then(function(regioesPlanejamento) {
                $scope.regioesPlanejamento = regioesPlanejamento;
                //  Por default todas as regioesPlanejamento vem selecionadas
                // $scope.filtro.regioesPlanejamento = getIdList(regioesPlanejamento);
            });

            // RNE163 se for gerente não exibe o campo Gerência e se for analista de portfolio
            //  não exibe os campos Gerência e Analista
            if ($scope.isGerente() || $scope.isPromocao()) {
                getDepartamentosUsuarioLogado();
            } else {
                Restangular.all('domain/departamento').getList().then(function(departamentos) {
                    $scope.gerencias = departamentos;
                    //  Por default todas as gerencias vem selecionadas
                    // $scope.filtro.departamentos = getIdList(departamentos);
                    $scope.changeManageMultiSelect();
                });
            }

            $scope.data = [];
        }

        var initialize = function() {
            $scope.estagios = ESTAGIO_PROJETO.getAll();
            $scope.status = STATUS_PROJETO.getAll();

            Restangular.all('domain/regiaoPlanejamento').getList().then(function(regioesPlanejamento) {
                $scope.regioesPlanejamento = regioesPlanejamento;
                //  Por default todas as regioesPlanejamento vem selecionadas
                // $scope.filtro.regioesPlanejamento = getIdList(regioesPlanejamento);
            });

            $scope.reset();

            setTimeout(function() {
                $scope.search();
            }, true);
        }

        initialize();

        //  Recupera o caminho baseado no relatório
        var getPath = function(format) {
            return 'relatorio/projeto/estagio/' + format;
        }

        var getHeader = function(format) {
            var header = '';
            if (format === 'pdf') {
                header = 'application/pdf';
            } else if (format === 'xlsx') {
                header = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            }
            return header;
        }

        var getFileName = function() {
            return 'RelatorioEstagio';
        }

        $scope.clean = function() {
            $scope.reset();
        }

        $scope.export = function(format) {
            $http({
                method: 'GET',
                url: configuration.localPath + "api/" + getPath(format),
                params: {
                    "estagios": $scope.filtro.estagios,
                    "cadeiasProdutivas": $scope.filtro.cadeiasProdutivas,
                    "status": $scope.filtro.status,
                    "departamentos": $scope.filtro.departamentos,
                    "regioesPlanejamento": $scope.filtro.regioesPlanejamento,
                    "usuariosResponsavel": $scope.filtro.usuariosResponsavel,
                    "todosEstagios": _.map($scope.estagios,function(estagio){return estagio.id}),
                    "versao": $scope.filtro.versao
                },
                headers: {
                    Accept: getHeader(format),
                    'todasGerencias': getIdList($scope.gerencias),
                    'todasCadeiasProdutivas' :getIdList($scope.cadeiasProdutivas),
                    'todasRegioesPlanejamento': getIdList($scope.regioesPlanejamento),
                    'todosAnalistas': getIdList($scope.analistas),
                    'Authorization': 'Bearer ' + UserSession.getAccessToken() 
                },
                responseType: 'arraybuffer'
            }).
            success(function(response) {
                ScreenService.browserAwareSave(response,"AcompanhamentoEstagio."+format,
                    getHeader(format));
            })
        }

        /*Início configs grid*/
        $scope.columnDefs = [{
            field: 'nomeEmpresa',
            enableCellEdit: false,
            width: "150",
            translationKey: 'common.company'
        }, {
            field: 'nomeProjeto',
            enableCellEdit: false,
            width: "100",
            translationKey: 'common.project'
        }, {
            field: 'cidade',
            enableCellEdit: false,
            width: "150",
            translationKey: 'address.city'
        }, {
            field: 'regiaoPlanejamento',
            enableCellEdit: false,
            width: "200",
            translationKey: 'location.planing.region'
        }, {
            field: 'cadeiaProdutiva',
            enableCellEdit: false,
            width: "150",
            translationKey: 'common.productive.chain'
        }, {
            field: 'situacaoAtualDescricao',
            enableCellEdit: false,
            width: "100",
            translationKey: 'common.currentStatus'
        }, {
            field: 'estagioAtualDescricao',
            enableCellEdit: false,
            width: "150",
            translationKey: 'common.currentStage'
        }, {
            field: 'analista',
            enableCellEdit: false,
            width: "100",
            translationKey: 'common.analyst'
        }, {
            field: 'departamento',
            enableCellEdit: false,
            width: "100",
            translationKey: 'common.manage'
        }, {
            field: 'dataPrevistaDF',
            enableCellEdit: false,
            width: "150",
            translationKey: 'reportTrackingStage.predictedDate.df',
            cellFilter: 'dateTransformFilter:\'MMM/yyyy\''
        }, {
            field: 'dataPrevistaII',
            enableCellEdit: false,
            width: "150",
            translationKey: 'reportTrackingStage.predictedDate.ii',
            cellFilter: 'dateTransformFilter:\'MMM/yyyy\''
        }, {
            field: 'dataPrevistaOI',
            enableCellEdit: false,
            width: "150",
            translationKey: 'reportTrackingStage.predictedDate.oi',
            cellFilter: 'dateTransformFilter:\'MMM/yyyy\''
        }, {
            field: 'dataRealIP',
            enableCellEdit: false,
            width: "150",
            translationKey: 'reportTrackingStage.realDate.ip',
            cellFilter: 'dateTransformFilter:\'MMM/yyyy\''
        }, {
            field: 'dataRealPP',
            enableCellEdit: false,
            width: "150",
            translationKey: 'reportTrackingStage.realDate.pp',
            cellFilter: 'dateTransformFilter:\'MMM/yyyy\''
        }, {
            field: 'dataRealDF',
            enableCellEdit: false,
            width: "150",
            translationKey: 'reportTrackingStage.realDate.df',
            cellFilter: 'dateTransformFilter:\'MMM/yyyy\''
        }, {
            field: 'dataRealII',
            enableCellEdit: false,
            width: "150",
            translationKey: 'reportTrackingStage.realDate.ii',
            cellFilter: 'dateTransformFilter:\'MMM/yyyy\''
        }, {
            field: 'dataRealOI',
            enableCellEdit: false,
            width: "150",
            translationKey: 'reportTrackingStage.realDate.oi',
            cellFilter: 'dateTransformFilter:\'MMM/yyyy\''
        }, {
            field: 'dataRealCC',
            enableCellEdit: false,
            width: "150",
            translationKey: 'reportTrackingStage.realDate.cc',
            cellFilter: 'dateTransformFilter:\'MMM/yyyy\''
        }];
        /*Fim configs grid*/

        $scope.search = function() {
            Restangular.all('relatorio/projeto/getByFilters').getList($scope.filtro).then(function(result) {
                $scope.data = result;

                if ($scope.data.length === 0) {
                    growl.error($filter('translate')("message.error.MSG004_NENHUM_REGISTRO_ENCONTRADO"));
                }
            }, function errorCallback(response) {
                if (response.status !== 404) {
                    growl.error("Error: The server returned status " + response.status);
                }
            });
        }
    });
