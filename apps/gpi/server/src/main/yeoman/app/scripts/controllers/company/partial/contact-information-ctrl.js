'use strict';
angular.module('gpifrontApp')
    .controller('CompanyContactInformationCtrl', function($scope, UserSession, TIPO_USUARIO, Restangular, $translate, $filter, growl, TIPO_CONTATO, $rootScope,
        $stateParams, GridButton, configuration, ScreenService, AUTH_ROLES, AuthService, dialogs, $state, $location, $anchorScroll) {

        $scope.screenService = ScreenService;

        // Usado para validar o form
        $scope.formSubmitted = false;
        $scope.isExternalUser = UserSession.getUser().tipo == TIPO_USUARIO.EXTERNO;

        $scope.initialize = function() {
            //  Esconde/Exibe a seção de informações de contato
            $scope.contactInformationsVisible = true;
            $scope.contactsSelectedItems = [];

            $scope.TIPO_CONTATOS = TIPO_CONTATO.todos;

            $scope.contactGridFunctions = ["showContact"];

            $scope.informacoesContato = [];

            $scope.showContactForm = false;

            Restangular.one("empresa", $stateParams.id).one("contato").getList().then(function(informacoesContato) {
                $scope.informacoesContato = informacoesContato;
            });

            // Configurações do datepicker
            $scope.configs = {
                required: true
            };

            $scope.buttonsConfig = {
                //function GridButton(show, translationKey, searchMode, disable, action, screenLink)
                newButton: new GridButton(false, 'common.new', false, null, null, ''),
                deleteButton: new GridButton(false, 'common.delete', false, null, null, '')
            };
        };
        //--------------------------------------------------------------------- Fim screenType

        /*
         *   Valida o campo passado
         */
        $scope.checkValidation = function(formField) {
            return formField.$invalid && (formField.$dirty || $scope.formSubmitted);
        }

        $scope.save = function(isFormValid) {
            if (isFormValid) {
                if ($scope.contato.dataEfetivado > new Date().getTime()) {
                    growl.error($filter('translate')("message.error.MSG39_DATA_INVALIDA"));
                } else {
                    Restangular.one("empresa", $stateParams.id).post("contato", $scope.contato).then(function() {
                        // Utilizado para permitir que o usuário mude de página
                        // sem a mensagem de perca de dados
                        $scope.formContactInformation.$setPristine();
                        //  Para atualizar as datas de todos os datepickers de acordo com as salvas no servidor
                        growl.success("Registro gravado com sucesso!");
                        setTimeout(function() {
                            $state.transitionTo($state.current, $stateParams, {
                                reload: true,
                                inherit: true,
                                notify: true
                            });
                        }, 5000);
                    }, function() {
                        growl.error("Ocorreu um erro ao tentar gravar o registro!");
                    });
                }
            } else {
                growl.error($filter('translate')("message.error.MSGXX_DADOS_INVALIDOS"));
                $scope.formSubmitted = true;
            }
        }

        var getContactFromScopeById = function(contactId) {
            for (var i = $scope.informacoesContato.length - 1; i >= 0; i--) {
                if ($scope.informacoesContato[i].id == contactId) {
                    return $scope.informacoesContato[i];
                }
            };
            return null;
        };

        $scope.showContact = function(contactId) {
            $scope.disableContactFormDiv = true;
            changeDatepickerVisualization();
            $scope.contato = getContactFromScopeById(contactId);
            updateDatepicker();
            showContactForm();
        };

        var clearContactForm = function() {
            $scope.contato = {
                anexos: []
            };
            document.formContactInformation.reset();
        };

        $scope.prepareCreateContact = function() {
            $scope.disableContactFormDiv = false;
            changeDatepickerVisualization();
            clearContactForm();
            showContactForm();
        };

        var getSelectedContacts = function() {
            return $scope.contactsSelectedItems;
        };

        $scope.disableNewFn = function() {
            return AuthService.isUserAuthorized([AUTH_ROLES.infraAnalist, AUTH_ROLES.informationAnalist, AUTH_ROLES.director]);
        }

        $scope.disableEditFn = function() {
            return $scope.contactsSelectedItems.length !== 1 ||
                AuthService.isUserAuthorized([AUTH_ROLES.infraAnalist, AUTH_ROLES.informationAnalist, AUTH_ROLES.director]);
        }

        $scope.disableDeleteFn = function() {
            return $scope.contactsSelectedItems.length == 0 ||
                AuthService.isUserAuthorized([AUTH_ROLES.infraAnalist, AUTH_ROLES.informationAnalist, AUTH_ROLES.director]);
        }

        $scope.prepareEditContact = function() {
            $scope.disableContactFormDiv = false;
            changeDatepickerVisualization();
            var selectedContact = getSelectedContacts()[0].id;
            $scope.contato = getContactFromScopeById(selectedContact);
            showContactForm();
            updateDatepicker();
        };

        //  Desabilita o datepicker caso seja tela de visualizar
        var changeDatepickerVisualization = function(){
            $scope.configs.disabled = $scope.disableContactFormDiv;
        }

        //  Atualiza o datepicker
        var updateDatepicker = function(){
            $scope.configs.restangularUpdated = !$scope.configs.restangularUpdated;
        }

        $scope.deleteContact = function() {
            var deleteDialog = dialogs.confirm($filter('translate')('DIALOGS_CONFIRMATION'), $filter('translate')('message.confirm.MSG009_CONFIRMAR_EXCLUSAO'));
            deleteDialog.result.then(function(btn) {
                $scope.deletedItems = [];
                for (var i = 0; i < $scope.contactsSelectedItems.length; i++) {
                    $scope.deletedItems.push($scope.contactsSelectedItems[i].id);
                };

                Restangular.all('empresa/' + $stateParams.id + '/contato/removeContato').post(
                    $scope.deletedItems
                ).then(function(result) {
                    growl.success("Registro excluído com sucesso!");

                    for (var a = $scope.informacoesContato.length - 1; a >= 0; a--) {
                        for (var i = $scope.deletedItems.length - 1; i >= 0; i--) {
                            if ($scope.informacoesContato[a].id == $scope.deletedItems[i]) {
                                $scope.informacoesContato.splice(a, 1);
                                break;
                            }
                        };
                    }

                    $scope.deletedItems = [];
                }, function() {
                    growl.error("Ocorreu um erro ao tentar gravar o registro!");
                    return false;
                });

                return true;
            }, function(){
                return false;
            });
        }

        var hideContactForm = function() {
            angular.element("#contactFormDiv").hide();
        };

        var showContactForm = function() {
            $scope.showContactForm = true;
            setTimeout(function() {
                if($scope.disableContactFormDiv){
                    $location.hash('contactTypeSelect');
                    $anchorScroll();
                }else{
                    angular.element("#contactTypeSelect").focus()
                }
            }, true);

        };

        $scope.closeForm = function(){
            $scope.showContactForm = false;
            hideContactForm();
            clearContactForm();
        }

        $scope.cancel = function() {
            if($scope.formContactInformation.$dirty){
                var cancelDialog = dialogs.confirm($filter('translate')('DIALOGS_CONFIRMATION'), $filter('translate')('message.confirm.MSG31_CANCELAR'));
                cancelDialog.result.then(function(btn) {
                    Restangular.one("empresa", $stateParams.id).one("contato").getList().then(function(informacoesContato) {
                        $scope.informacoesContato = informacoesContato;
                    });
                    $scope.formContactInformation.$setPristine();
                    $scope.closeForm();
                });
            }else{
                $scope.closeForm();
            }
        }

        $scope.checkUserAuthorization();        
        $scope.initialize();
        
    });
