'use strict';

angular.module('gpifrontApp')
    .controller('InputsAndProductsServicesToBeProvidedGridCtrl', function($scope, Restangular, $filter, $rootScope, GridButton, growl) {
     
        $scope.servicos = $scope.$parent.insumoProduto.servicos;

        $scope.selectedItems = [];

        $scope.columnDefs = [{
            field: 'descricao',
            enableCellEdit: true,
            width: "**",
            translationKey: 'common.services',
            editableCellTemplate: '<input type="text" placeholder="{{\'common.services\' | translate}}" ng-class="\'colt\' + col.index"  ng-input="COL_FIELD" ng-model="COL_FIELD" maxlength="100"/>'

        }];

        $scope.buttonsConfig = {
            //function GridButton(show, translationKey, searchMode, disable, action, screenLink)
            newButton: new GridButton(true, 'common.new', false, null, null, ''),
            deleteButton: new GridButton(true, 'common.delete', false, null, deleteFn, '')
        }

        function deleteFn() {
            growl.info($filter('translate')('message.error.MSG38_SECAO_ATUALIZADA', {
                value: "Produtos e Serviços a serem Fornecidos"
            }));                        
        }                   

        $scope.sortInfo = {
            fields: ['descricao'],
            directions: ['asc']
        };

         $scope.$parent.$watch('insumoProduto', function(newVal, oldVal) {
            if (newVal.servicos !== oldVal.servicos) {
                $scope.servicos = newVal.servicos;
            }
        }, true);

    });