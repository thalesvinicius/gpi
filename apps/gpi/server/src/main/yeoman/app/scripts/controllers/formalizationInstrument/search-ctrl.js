'use strict';

angular.module('gpifrontApp')
    .controller('FormalizationInstrumentSearchCtrl', function($scope, Restangular, $filter, GridButton, AuthService, AUTH_ROLES, SITUACAO_INSTRUMENTO_FORMALIZACAO, TIPO_INSTRUMENTO_FORMALIZACAO,growl,$rootScope) {
        $scope.selectedItems = [];

        $scope.instrumentos = [];

        $scope.formalizationTypes = TIPO_INSTRUMENTO_FORMALIZACAO.getAll();
        $scope.situations = SITUACAO_INSTRUMENTO_FORMALIZACAO.getAll();

        $scope.clean = function() {
            $scope.filters = {
                situacao: null,
                tipoInstrumento: null,
                empresa: null,
                protocolo: null,
                titulo: null,
                nomeProjeto: null
            }

            $scope.instrumentos = [];
        }

        function translate(translationKey) {
            var text = "";
            if (typeof translationKey !== 'undefined' && translationKey) {
                text = $filter('translate')(translationKey);
            }
            return text;
        };

        $scope.clean();
        $scope.gridScope = {};


        $scope.columnDefs = [{
            field: 'instrumentoFormalizacaoId',
            translationKey: '',
            enableCellEdit: false,
            cellTemplate: "<div class=\"ngCellText\" ng-show=\"!vinculado\" ng-class=\"col.colIndex()\" ng-click=\"selectNodeHead(row.entity.id); $event.stopPropagation();\"><span ng-class=\"headClass(row.entity, row.entity.id, row.entity.vinculados)\"></span></div>",
            width: 25
        },{
            field: 'id',
            enableCellEdit: false,
            translationKey: 'ID',
            width: 35
        }, {
            field: 'protocolo',
            enableCellEdit: false,
            width: "**",
            translationKey: 'formalizationInstrument.protocolCode',
            cellFilter: "filterProtocolo:'protocolo'"
        }, {
            field: 'tipoInstrFormalizacao',
            enableCellEdit: false,
            width: "**",
            cellTemplate: "<div ng-click=\"getIntrumetType()\" class=\"ngCellText\" ng-class=\"col.colIndex()\"><span ng-cell-text>{{row.getProperty(col.field)}}</span></div>",
            translationKey: 'formalizationInstrument.type'
        }, {
            field: 'situacao',
            enableCellEdit: false,
            width: "**",
            translationKey: 'common.situation'
        }, {
            field: 'dataAssinatura',
            enableCellEdit: false,
            width: "**",
            translationKey: 'common.signDate',
            cellFilter: 'dateTransformFilter'
        }, {
            field: 'localDocumento',
            enableCellEdit: false,
            width: "**",
            translationKey: 'formalizationInstrument.documentLocal'
        }, {
            field: 'versao',
            enableCellEdit: false,
            width: 60,
            translationKey: 'common.version'
        }, {
            field: 'joinedEmpresas',
            enableCellEdit: false,
            width: "**",
            cellTemplate: "<div class=\"ngCellText\" ng-class=\"col.colIndex()\"><span ng-cell-text>{{row.getProperty(col.field)}}</span></div>",
            translationKey: 'formalizationInstrument.companyUnit'
        }, {
            field: 'show',
            translationKey: 'common.show',
            showScreenRedirect: true,
            showLink: '/instrumento-formalizacao/cadastro/visualizar'
        }];

        $scope.rowStyle = function(row, childIndex) {
            var height = this.$parent.rowHeight;
            var top = 0;
            if (typeof childIndex === "undefined") {
                var totalTop = 0;
                if (row.isAggRow)
                    this.$parent.isAggregate = true;
                if (row.rowIndex > 0 || this.$index > 0) {
                    var topRow
                    if (this.$parent.isAggregate)
                        topRow = this.$parent.renderedRows[this.$index - 1];
                    else
                        topRow = this.$parent.renderedRows[row.rowIndex - 1];
                    var topExpandedHeight = 0;
                    if (this.$parent.nodeExpanded(topRow.entity.id))
                        topExpandedHeight = topRow.entity.vinculados.length;
                    totalTop = (topRow.offsetTop + height) + (topExpandedHeight * height);
                }
                top = row.offsetTop = totalTop;
            } else {
                top = (height + (childIndex * height));
            }
            var ret = {
                "top": top + "px",
                "height": height + "px"
            };

            if (row.isAggRow) {
                ret.left = row.offsetLeft;
            }
            return ret;
        };

        $scope.rowTemplate =
            "<div ng-style=\"{ 'cursor': row.cursor }\" ng-repeat=\"col in renderedColumns\" ng-class=\"col.colIndex()\" class=\"ngCell {{col.cellClass}}\">\r" +
            "\n" +
            "\t<div class=\"ngVerticalBar\" ng-style=\"{height: rowHeight}\" ng-class=\"{ ngVerticalBarVisible: !$last }\">&nbsp;</div>\r" +
            "\n" +
            "\t<div ng-cell></div>\r" +
            "\n" +
            "</div>" +
            "<div ng-style=\"rowStyle(row, $index, rowHeight, renderedRows)\" ng-class=\"{selected: selectedChildrenItems[vinculado.id]}\" class=\"ngRow\" ng-repeat=\"vinculado in row.entity.vinculados\" ng-show=\"nodeExpanded(row.entity.id)\">\r" +
            "<div ng-style=\"{ 'cursor': row.cursor }\" ng-repeat=\"col in renderedColumns\" ng-class=\"col.colIndex()\" class=\"ngCell {{col.cellClass}}\">\r" +
            "\n" +
            "\t<div class=\"ngVerticalBar\" ng-style=\"{height: rowHeight}\" ng-class=\"{ ngVerticalBarVisible: !$last }\">&nbsp;</div>\r" +
            "\n" +
            "\t<div class=\"ngSelectionCell\" ng-show=\"col.field==='instrumentoFormalizacaoId'\" ng-class=\"col.colIndex()\"><input tabindex=\"-1\" class=\"ngSelectionCheckbox\" type=\"checkbox\" ng-model=\"selectedChildrenItems[vinculado.id]\" ng-change=\"selectChildrenItem(vinculado)\" /></div>\r" +
            "\t<div class=\"ngCellText\" ng-if=\"col.field!=='show'\" ng-class=\"col.colIndex()\"><span ng-if=\"col.field == \'protocolo\'\" ng-cell-text>{{vinculado[col.field] | filterProtocolo:col.field}}</span> <span ng-if=\"col.field == \'dataAssinatura\'\" ng-cell-text>{{vinculado[col.field] | dateTransformFilter}}</span> <span ng-if=\"col.field != \'protocolo\' && col.field != \'dataAssinatura\'\" ng-cell-text>{{vinculado[col.field]}}</span></div>\r" +
            "\t<div class=\"ngCellText text-center\" ng-show=\"col.field==='show'\" ng-class=\"col.colIndex()\"><a href=#/instrumento-formalizacao/cadastro/visualizar?id={{vinculado.id}}><i class=\"fa fa-eye page-header-icon\"></i></a></div>\r" +
            "\n" +
            "</div>" +
            "</div>";

        $scope.buttonsConfig = {
            newButton: new GridButton(true, 'common.new', true, disableNewFn, null, '/instrumento-formalizacao/cadastro/incluir'),
            editButton: new GridButton(true, 'common.edit', true, disableEditFn, null, '/instrumento-formalizacao/cadastro/editar'),
            deleteButton: new GridButton(false, 'common.delete', false, null, null, '')
        };

        function disableEditFn() {
            return !AuthService.isUserAuthorized([AUTH_ROLES.promotionAnalist, AUTH_ROLES.manager, AUTH_ROLES.portfolioAnalist, AUTH_ROLES.infraAnalist, AUTH_ROLES.itAdmin]);
        }

        function disableNewFn() {
            return !AuthService.isUserAuthorized([AUTH_ROLES.promotionAnalist, AUTH_ROLES.manager, AUTH_ROLES.portfolioAnalist, AUTH_ROLES.infraAnalist, AUTH_ROLES.itAdmin]);
        }

        $scope.search = function() {
            $scope.instrumentos = [];
            $rootScope.filtersIF = $scope.filters;

            if(validateSearch()){
                Restangular.all('instrumento').getList($scope.filters).then(function(instrumentos) {
                    $scope.instrumentos = instrumentos;
                    if ($scope.instrumentos.length === 0) {
                        growl.error(translate('message.error.MSG004_NENHUM_REGISTRO_ENCONTRADO'));
                    }
                });
            }
        };

        if ($rootScope.filtersIF !== undefined){
            $scope.filters = $rootScope.filtersIF
            setTimeout(function(){
                $scope.search();    
            }, true)                
        }

        function validateSearch(){

            if ($scope.filters.situacao === null &&
               ($scope.filters.tipoInstrumento === null || $scope.filters.tipoInstrumento.trim() === '') &&
               ($scope.filters.protocolo === null || $scope.filters.protocolo.trim() === '') &&
               ($scope.filters.titulo === null || $scope.filters.titulo.trim() === '') &&
               ($scope.filters.empresa === null || $scope.filters.empresa.trim() === '') &&
               ($scope.filters.nomeProjeto === null || $scope.filters.nomeProjeto.trim() === '')) {

                growl.error(translate('message.error.MSG29_OBRIGATORIO_INFORMAR_PELO_MENOS_UM_CAMPO_NA_PESQUISA'));
                return false;
            } else {
                return true;
            }
        }


    }).filter('filterProtocolo', function() {
        return function(protocolo, field) {
            if (protocolo && field === 'protocolo') {
                return protocolo.toString().slice(0, 3).concat("/").concat(protocolo.slice(3));
            }
        }
    });
