'use strict'

angular
    .module('gpifrontApp')
    .controller('UnitContactGridCtrl', function($scope, GridButton, growl, $filter) {

        $scope.contatosPessoa = [];

        $scope.contatoUnidadeColumnDefs = [{
            field: 'nome',
            enableCellEdit: true,
            width: "**",
            translationKey: 'common.name',
            editableCellTemplate: '<input type="text" placeholder="{{\'common.name\' | translate}}" ng-class="\'colt\' + col.index"  ng-input="COL_FIELD" ng-model="COL_FIELD" maxlength="100"/>'

        }, {
            field: 'cargo',
            enableCellEdit: true,
            width: "*",
            translationKey: 'common.job.title',
            editableCellTemplate: '<input type="text" placeholder="{{\'common.job.title\' | translate}}" ng-class="\'colt\' + col.index"  ng-input="COL_FIELD" ng-model="COL_FIELD" maxlength="100"/>'
        }, {
            field: 'telefone',
            enableCellEdit: true,
            width: "*",
            translationKey: 'common.phone1',
            editableCellTemplate: '<input type="phone" placeholder="{{\'common.phone1\' | translate}}" ng-class="\'colt\' + col.index" maxlength="18" ng-input="COL_FIELD" ng-model="COL_FIELD"/>'
        }, {
            field: 'telefoneAdicional',
            enableCellEdit: true,
            width: "*",
            translationKey: 'common.phone2',
            editableCellTemplate: '<input type="phone" placeholder="{{\'common.phone2\' | translate}}" ng-class="\'colt\' + col.index" maxlength="18" ng-input="COL_FIELD" ng-model="COL_FIELD"/>'

        }, {
            field: 'email',
            enableCellEdit: true,
            width: "**",
            translationKey: 'common.email',
            editableCellTemplate: '<input type="email" placeholder="example@email.com" ng-class="\'colt\' + col.index" maxlength="50" ng-input="COL_FIELD" ng-model="COL_FIELD" />'
        }];

        //define its buttons-config attribute on a search screen
        $scope.contatoUnidadeButtonsConfig = {
            newButton: new GridButton(true, 'common.new', false, null, null, ''),
            deleteButton: new GridButton(true, 'common.delete', false, null, deleteFn, '')
        }

        //define its sort-info attribute if you desire a default-sort
        $scope.contatoUnidadeSortInfo = {
            fields: ['id'],
            directions: ['asc']
        };

        $scope.$parent.$watch('unidade.contatosPessoa', function(newVal, oldVal) {
            setTimeout(function() {
                if(newVal !== $scope.contatosPessoa){
                    $scope.contatosPessoa = newVal;
                }
            }, true);
        }, true);

        function deleteFn() {
            growl.info($filter('translate')('message.error.MSG38_SECAO_ATUALIZADA', {
                value: "Unidades"
            }));
        }

    });
