'use strict'

angular.module('gpifrontApp')
    .controller('GoalsDashboardCtrl', function($scope, ScreenService, ESTAGIO_PROJETO, Restangular, $rootScope, $filter) {
        $scope.screenService = ScreenService;
        $scope.dados = [];

        $scope.filterOptions = {
            estagioNotMultiple: true,
            anual: false,
            regiaoPlanejamento: false,
            gerencia: true,
            cadeiaProdutiva: false,
            prospeccao: true,
            intervaloDatas: true,
            mesAno: false,
            tipoInstrumentoFormalizacao: false
        };

        var getXAxis = function() {
            return ['x', 'JAN', 'FEV', 'MAR', 'ABR', 'MAI', 'JUN', 'JUL', 'AGO', 'SET', 'OUT', 'NOV', 'DEZ'];
        }

        var createCumulLine = function(c3Data, columnReference, excludedColumns, limit) {
            var cumulativeLine = ['Acumulado']
            var columns = [];
            if (excludedColumns) {
                columns = c3Data.data.columns.filter(function(d) {
                    return excludedColumns.lastIndexOf(d[0]) === -1;
                });
            } else {
                columns = c3Data.data.columns;
            }

            if (columnReference === 'all') {
                for (var i = 1; i < limit; i++) {
                    var newValue = getGroupDataSum(i, columns);
                    if (i === 1) {
                        cumulativeLine.push(newValue);
                    } else {
                        cumulativeLine.push(cumulativeLine[i - 1] + newValue);
                    }
                }
            } else {
                angular.forEach(columns, function(column) {
                    if (column[0] && column[0] === columnReference) {
                        for (var i = 1; i < limit; i++) {
                            if (i == 1) {
                                cumulativeLine.push(column[i]);
                            } else {
                                //sum previous data with the new data
                                cumulativeLine.push(column[i - 1] + column[i]);
                            }
                        }
                    }
                });
            }
            c3Data.data.columns.push(cumulativeLine);
            angular.extend(c3Data.data.types, {
                Acumulado: 'line'
            });
        }

        var getGroupDataSum = function(i, dataArray, excludedColumns) {
            var sum = 0;

            angular.forEach(dataArray, function(arrayValues) {
                sum += arrayValues[i];
            });

            return sum;
        }

        var initGraphics = function(graphic, name, dataFim) {
            createCumulLine(graphic, 'all', ['Meta' + name, 'x'], dataFim + 2);
            c3.generate(graphic);
        }

        $scope.initData = function(result) {
            $scope.panelScope.estagios = [
                ESTAGIO_PROJETO.PROJETO_PROMISSOR,
                ESTAGIO_PROJETO.FORMALIZADA,
                ESTAGIO_PROJETO.IMPLANTACAO_INICIADO,
                ESTAGIO_PROJETO.OPERACAO_INICIADA
            ];

            var graficosMap = [];
            angular.forEach(result, function(it) {
                graficosMap[it[0]] = it;
            });

            var dataFim = new Date($scope.panelScope.filtro.dataFim).getMonth();

            $scope.chartProjetos = {
                bindto: "#chartProjetos",
                data: {
                    x: 'x',
                    columns: [],
                    type: 'bar',
                    types: {
                        'MetaProjetos': 'line'
                    }
                },
                zoom: {
                    enabled: true
                },
                axis: {
                    x: {
                        show: true,
                        type: 'category'
                    },
                    y: {
                        tick: {
                            format:function (d) { return $filter('number')(d, 0); }
                        }
                    }
                }
            };
            if (graficosMap['MetaProjetos']) {
                $scope.chartProjetos.data.columns.push(graficosMap['MetaProjetos']);    
            }
            if (graficosMap['Projetos']) {
                $scope.chartProjetos.data.columns.push(graficosMap['Projetos']);                
            }            
            $scope.chartProjetos.data.columns.push(getXAxis());            
            initGraphics($scope.chartProjetos, 'Projetos', dataFim);


            $scope.chartInvestimento = {
                bindto: "#chartInvestimento",
                data: {
                    x: 'x',
                    colors: {
                        Investimento: 'green'
                    },
                    columns: [],
                    type: 'bar',
                    types: {
                        'MetaInvestimento': 'line'
                    }
                },
                zoom: {
                    enabled: true
                },                
                axis: {
                    x: {
                        type: 'category'
                    },
                    y: {
                        tick: {
                            format:function (d) { return $filter('number')(d, 0); }
                        }
                    }
                }
            };
            if (graficosMap['MetaInvestimento']) {
                $scope.chartInvestimento.data.columns.push(graficosMap['MetaInvestimento']);    
            }
            if (graficosMap['Investimento']) {
                $scope.chartInvestimento.data.columns.push(graficosMap['Investimento']);                
            }            
            $scope.chartInvestimento.data.columns.push(getXAxis());            
            initGraphics($scope.chartInvestimento, 'Investimento', dataFim);            

            $scope.chartEmpregos = {
                bindto: "#chartEmpregos",
                data: {
                    x: 'x',
                    colors: {
                        Empregos: 'red'
                    },
                    columns: [],
                    type: 'bar',
                    types: {
                        'MetaEmpregos': 'line'
                    }
                },
                zoom: {
                    enabled: true
                },                
                axis: {
                    x: {
                        type: 'category'
                    },
                    y: {
                        tick: {
                            format:function (d) { return $filter('number')(d, 0); }
                        }
                    }
                }
            };
            if (graficosMap['MetaEmpregos']) {
                $scope.chartEmpregos.data.columns.push(graficosMap['MetaEmpregos']);    
            }
            if (graficosMap['Empregos']) {
                $scope.chartEmpregos.data.columns.push(graficosMap['Empregos']);                
            }            
            $scope.chartEmpregos.data.columns.push(getXAxis());            
            initGraphics($scope.chartEmpregos, 'Empregos', dataFim);


            $scope.chartFaturamento = {
                bindto: "#chartFaturamento",
                data: {
                    x: 'x',
                    colors: {
                        Faturamento: 'purple'
                    },
                    columns: [],
                    type: 'bar',
                    types: {
                        'MetaFaturamento': 'line'
                    }
                },
                zoom: {
                    enabled: true
                },                
                axis: {
                    x: {
                        type: 'category'
                    },
                    y: {
                        tick: {
                            format:function (d) { return $filter('number')(d, 0); }
                        }
                    }
                }
            };            
            if (graficosMap['MetaFaturamento']) {
                $scope.chartFaturamento.data.columns.push(graficosMap['MetaFaturamento']);    
            }
            if (graficosMap['Faturamento']) {
                $scope.chartFaturamento.data.columns.push(graficosMap['Faturamento']);                
            }            
            $scope.chartFaturamento.data.columns.push(getXAxis());            
            initGraphics($scope.chartFaturamento, 'Faturamento', dataFim);

        }

    });
