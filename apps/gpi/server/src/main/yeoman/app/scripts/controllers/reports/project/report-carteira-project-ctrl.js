'use strict'
angular.module('gpifrontApp')
    .controller('ReportCarteiraProjectCtrl', function($scope, growl, Restangular, $filter, ESTAGIO_PROJETO, STATUS_PROJETO, AuthService, AUTH_ROLES, UserSession, ScreenService ,$http, configuration) {

        $scope.disableHeader = true;
        $scope.filtro = {
            estagios: [],
            cadeiasProdutivas: [],
            regioesPlanejamento: [],
            departamentos: [],
            usuariosResponsavel: []
        };
        $scope.selectedItems = [];
        $scope.sortInfo = {
            fields: ['empresa'],
            directions: ['asc']
        };
        $scope.gridScope = {
            options : {
             enableCellSelection : false,  
             selectWithCheckboxOnly: false,
             showSelectionCheckbox: false
            }
        };


        // RNE163 se for gerente não exibe o campo Gerência e se for analista de portfolio
        //  não exibe os campos Gerência e Analista
        $scope.isGerente = function() {
            return AuthService.isUserAuthorized([AUTH_ROLES.manager]);
        }

        // RNE163 se for gerente não exibe o campo Gerência e se for analista de portfolio
        //  não exibe os campos Gerência e Analista
        $scope.isPromocao = function() {
            return AuthService.isUserAuthorized([AUTH_ROLES.promotionAnalist]);
        }

        /*
         *   Recupera todos os ids da lista passada
         */
        var getIdList = function(list) {
            var idList = [];
            if (list !== undefined) {
                if (_.isArray(list)) {
                    _.each(list, function(val, key) {
                        idList.push(val.id);
                    });
                } else {
                    idList.push(list.id);
                }
            }
            return idList;
        }

        $scope.disabledExport = function() {
            return $scope.data.length === 0;
        }

        /*
         *   Evento chamado ao selecionar gerência
         */
        $scope.changeManageMultiSelect = function() {
            //  Busca os usuários com base no departamento
            if ($scope.filtro.departamentos && $scope.filtro.departamentos.length > 0) {
                Restangular.all('user/byDepartamentos').getList({
                    "departamentos": $scope.filtro.departamentos
                }).then(function(analistas) {
                    $scope.analistas = analistas;
                    //  Por default todos os analistas vem selecionados
                    $scope.filtro.usuariosResponsavel = $scope.isPromocao() ? [UserSession.getUser().id] : [];


                });

                Restangular.all('cadeiaProdutiva/byDepartamentos').getList({
                    "departamentos": $scope.filtro.departamentos
                }).then(function(cadeiasProdutivas) {
                    $scope.cadeiasProdutivas = cadeiasProdutivas;
                    //  Por default todas as cadeiasProdutivas vem selecionadas
                    // $scope.filtro.cadeiasProdutivas = getIdList(cadeiasProdutivas);
                });
            } else {
                $scope.analistas = [];
                $scope.cadeiasProdutivas = [];
                $scope.filtro.usuariosResponsavel = [];
                $scope.filtro.cadeiasProdutivas = [];
            }
        }

        /*
         *   Utilizado pelos usuarios GERENTE e PROMOCAO para recuperar as gerencias
         */
        var getDepartamentosUsuarioLogado = function() {
            Restangular.one('user/' + UserSession.getUser().id).get().then(function(user) {
                $scope.filtro.departamentos = getIdList(user.departamento);
                //  Aciona o evento onChange do select de departamentos
                //  para preencher os analistas e as cadeias produtivas
                $scope.changeManageMultiSelect();
            });
        }

        /*  Iniciando defaults do relatório */
        $scope.reset = function() {
            $scope.data = [];

            /*  Inicializando as listas com seus respectivos registros selecionados. */
            // $scope.filtro.estagios = getIdList($scope.estagios);
            $scope.filtro.status = [STATUS_PROJETO.ATIVO.id];

            Restangular.all('domain/regiaoPlanejamento').getList().then(function(regioesPlanejamento) {
                $scope.regioesPlanejamento = regioesPlanejamento;
                //  Por default todas as regioesPlanejamento vem selecionadas
                // $scope.filtro.regioesPlanejamento = getIdList(regioesPlanejamento);
            });

            // RNE163 se for gerente não exibe o campo Gerência e se for analista de portfolio
            //  não exibe os campos Gerência e Analista
            if ($scope.isGerente() || $scope.isPromocao()) {
                getDepartamentosUsuarioLogado();
            } else {
                Restangular.all('domain/departamento').getList().then(function(departamentos) {
                    $scope.gerencias = departamentos;
                    //  Por default todas as gerencias vem selecionadas
                    // $scope.filtro.departamentos = getIdList(departamentos);
                    $scope.changeManageMultiSelect();

                    // Ao carregar a tela realiza a consulta inicial
                    setTimeout(function() {
                        if ($scope.data.length === 0) {
                            $scope.search();
                        }
                    }, true);
                });
            }
        }

        var initialize = function() {
            $scope.estagios = ESTAGIO_PROJETO.getAll();
            $scope.status = STATUS_PROJETO.getAll();

            Restangular.all('domain/regiaoPlanejamento').getList().then(function(regioesPlanejamento) {
                $scope.regioesPlanejamento = regioesPlanejamento;
                //  Por default todas as regioesPlanejamento vem selecionadas
                // $scope.filtro.regioesPlanejamento = getIdList(regioesPlanejamento);
            });

            $scope.reset();
        }

        initialize();

        //  Recupera o caminho baseado no relatório
        var getPath = function(format) {
            return 'relatorio/projeto/carteira/' + format;
        }

        var getHeader = function(format) {
            var header = '';
            if (format === 'pdf') {
                header = 'application/pdf';
            } else if (format === 'xlsx') {
                header = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
            }
            return header;
        }

        var getFileName = function() {
            return 'RelatorioCarteira';
        }

        $scope.clean = function() {
            $scope.reset();
        }

        $scope.export = function(format) {
            var mimetype = getHeader(format);

            $http({
                method: 'GET',
                url: configuration.localPath + "api/" + getPath(format),
                params: {
                    "estagios": $scope.filtro.estagios,
                    "cadeiasProdutivas": $scope.filtro.cadeiasProdutivas,
                    "status": $scope.filtro.status,
                    "departamentos": $scope.filtro.departamentos,
                    "regioesPlanejamento": $scope.filtro.regioesPlanejamento,
                    "usuariosResponsavel": $scope.filtro.usuariosResponsavel,
                    "todosEstagios": _.map($scope.estagios, function(estagio) {
                        return estagio.id
                    })
                },
                headers: {
                    Accept: mimetype,
                    'todasGerencias': getIdList($scope.gerencias),
                    'todasCadeiasProdutivas': getIdList($scope.cadeiasProdutivas),
                    'todasRegioesPlanejamento': getIdList($scope.regioesPlanejamento),
                    'todosAnalistas': getIdList($scope.analistas),
                    'Authorization': 'Bearer ' + UserSession.getAccessToken()
                },
                responseType: 'arraybuffer'
            }).
            success(function(response) {
                ScreenService.browserAwareSave(response,"carteiraProjetos." + format ,mimetype);
            })
        }

        $scope.empregosTemplate = '<div class=\"ngCellText\" ng-class=\"col.colIndex()\"><input type="numeric" ng-class="\'colt\' + col.index" ng-input="COL_FIELD" ng-model="COL_FIELD"/></div>';

        /*Início configs grid*/
        $scope.columnDefs = [{
            field: 'empresa',
            width: "200",
            translationKey: 'common.company',
            enableCellEdit: false
        }, {
            field: 'projeto',
            width: "200",
            translationKey: 'common.project',
            enableCellEdit: false
        }, {
            field: 'cidade',
            width: "200",
            translationKey: 'address.city',
            enableCellEdit: false
        }, {
            field: 'regiaoPlanejamento',
            width: "180",
            translationKey: 'location.planing.region',
            enableCellEdit: false
        }, {
            field: 'investimentoPrevisto',
            width: "150",
            translationKey: 'reportProjects.investimento.investimento',
            cellFilter: "currency:''",
            enableCellEdit: false
        }, {
            field: 'faturamentoInicial',
            width: "150",
            translationKey: 'reportProjects.faturamento.inicial',
            cellFilter: "currency:''",
            enableCellEdit: false
        }, {
            field: 'faturamentoFinal',
            width: "150",
            translationKey: 'reportProjects.faturamento.final',
            cellFilter: "currency:''",
            enableCellEdit: false
        }, {
            field: 'empregosDiretos',
            width: "150",
            translationKey: 'reportProjects.empregos.diretos',
            enableCellEdit: false,
            cellFilter: 'number:0'
        }, {
            field: 'empregosIndiretos',
            width: "150",
            translationKey: 'reportProjects.empregos.indiretos',
            enableCellEdit: false,
            cellFilter: 'number:0'
        }, {
            field: 'cadeiaProdutiva',
            width: "200",
            translationKey: 'common.productive.chain',
            enableCellEdit: false
        }, {
            field: 'situacaoProjeto',
            width: "100",
            translationKey: 'common.currentStatus',
            enableCellEdit: false
        }, {
            field: 'estagioAtual',
            width: "200",
            translationKey: 'common.currentStage',
            enableCellEdit: false
        }, {
            field: 'analista',
            width: "100",
            translationKey: 'common.analyst',
            enableCellEdit: false
        }, {
            field: 'gerencia',
            width: "100",
            translationKey: 'common.manage',
            enableCellEdit: false
        }];
        /*Fim configs grid*/

        function translate(translationKey) {
            var text = "";
            if (typeof translationKey !== 'undefined' && translationKey) {
                text = $filter('translate')(translationKey);
            }
            return text;
        };

        $scope.search = function() {
            Restangular.all('relatorio/projeto/getCarteira').getList($scope.filtro).then(function(result) {
                $scope.data = result;

                if ($scope.data.length === 0) {
                    growl.error(translate('message.error.MSG004_NENHUM_REGISTRO_ENCONTRADO'));
                }
            }, function errorCallback(response) {
                if (response.status !== 404) {
                    growl.error("Error: The server returned status " + response.status);
                }
            });
        };
    });
