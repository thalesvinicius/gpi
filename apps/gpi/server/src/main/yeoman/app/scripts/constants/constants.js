'use strict';

angular.module('gpifrontApp')
    .constant('AUTH_EVENTS', {
        loginSuccess: 'auth-login-success',
        loginFailed: 'auth-login-failed',
        logoutSuccess: 'auth-logout-success',
        sessionTimeout: 'auth-session-timeout',
        notAuthenticated: 'auth-not-authenticated',
        notAuthorized: 'auth-not-authorized'
    })
    .constant('AUTH_ROLES', {
        itAdmin: 'Administrador de TI',
        infraAnalist: 'Analista de Infraestrutura e Meio Ambiente',
        informationAnalist: 'Analista de Informação',
        portfolioAnalist: 'Analista de Portfólio',
        promotionAnalist: 'Analista de Promoção',
        director: 'Diretor',
        manager: 'Gerente',
        generalSecretary: 'Secretaria Geral',
        externalResponsible: 'Responsável pelo acesso na Área do Investidor',
        externalOther: 'Outros'
    })
    .constant('MINAS_GERAIS_ID', 35)
    .constant('BRASIL', {
        id: 2,
        nome: "BRASIL"
    })
    .constant('ESTAGIO_PROJETO', {
        INICIO_PROJETO: {
            id: "INICIO_PROJETO",
            idValue: 3,
            "descricao": "Início de Projeto"
        },
        PROJETO_PROMISSOR: {
            id: "PROJETO_PROMISSOR",
            idValue: 5,
            "descricao": "Projeto Promissor"
        },
        FORMALIZADA: {
            id: "FORMALIZADA",
            idValue: 1,
            "descricao": "Decisão Formalizada"
        },
        IMPLANTACAO_INICIADO: {
            id: "IMPLANTACAO_INICIADO",
            idValue: 2,
            "descricao": "Implantação Iniciada"
        },
        OPERACAO_INICIADA: {
            id: "OPERACAO_INICIADA",
            idValue: 4,
            "descricao": "Operação Iniciada"
        },
        COMPROMISSO_CUMPRIDO: {
            id: "COMPROMISSO_CUMPRIDO",
            idValue: 6,
            "descricao": "Compromisso Cumprido"
        },
        getAll: function() {
            var allProperties = [];
            _.each(this, function(val, key) {
                if (!_.isFunction(val)) {
                    allProperties.push(val);
                }
            });
            return allProperties;
        }
    })
.constant('ESTAGIO_PROJETO_PREVISTO', {
        FORMALIZADA: {
            id: "FORMALIZADA",
            idValue: 1,
            "descricao": "Decisão Formalizada"
        },
        IMPLANTACAO_INICIADO: {
            id: "IMPLANTACAO_INICIADO",
            idValue: 2,
            "descricao": "Implantação Iniciada"
        },
        OPERACAO_INICIADA: {
            id: "OPERACAO_INICIADA",
            idValue: 4,
            "descricao": "Operação Iniciada"
        },
        getAll: function() {
            var allProperties = [];
            _.each(this, function(val, key) {
                if (!_.isFunction(val)) {
                    allProperties.push(val);
                }
            });
            return allProperties;
        }
    })
    .constant('TIPO_CONTATO', {
        EMAIL: {
            id: "EMAIL",
            descricao: "Troca de E-mail"
        },
        REUNIAO: {
            id: "REUNIAO",
            descricao: "Reunião"
        },
        TELEFONE: {
            id: "TELEFONE",
            descricao: "Telefonema"
        },
        OUTROS: {
            id: "OUTROS",
            descricao: "Outros"
        },
        todos: [{
            id: "EMAIL",
            descricao: "Troca de E-mail"
        }, {
            id: "REUNIAO",
            descricao: "Reunião"
        }, {
            id: "TELEFONE",
            descricao: "Telefonema"
        }, {
            id: "OUTROS",
            descricao: "Outros"
        }],
        getAll: function() {
            var allProperties = [];
            _.each(this, function(val, key) {
                if (!_.isFunction(val)) {
                    allProperties.push(val);
                }
            });
            return allProperties;
        }
    })
    .constant('SITUACAO_EMPRESA', {
        ativa: {
            id: "ATIVA",
            idValue: 1,
            descricao: "Ativa"
        },
        cadastroArquivado: {
            id: "CADASTRO_ARQUIVADO",
            idValue: 2,
            descricao: "Cadastro Arquivado"
        },
        pendenteValidacao: {
            id: "PEDENTE_VALIDACAO",
            idValue: 3,
            descricao: "Pendente de Validação"
        },
        pendenteAprovacao: {
            id: "PEDENTE_APROVACAO",
            idValue: 4,
            descricao: "Pendente de Aprovação"
        },
        todas: [{
            id: "ATIVA",
            idValue: 1,
            descricao: "Ativa"
        }, {
            id: "CADASTRO_ARQUIVADO",
            idValue: 2,
            descricao: "Cadastro Arquivado"
        }, {
            id: "PEDENTE_VALIDACAO",
            idValue: 3,
            descricao: "Pendente de Validação"
        }, {
            id: "PEDENTE_APROVACAO",
            idValue: 4,
            descricao: "Pendente de Aprovação"
        }]
    })
    .constant('SITUACAO_PROJETO', {
        ATIVO: {
            id: "ATIVO",
            idValue: 1,
            descricao: "Ativo"
        },
        CANCELADO: {
            id: "CANCELADO",
            idValue: 2,
            descricao: "Cancelado"
        },
        PENDENTE_ACEITE: {
            id: "PENDENTE_ACEITE",
            idValue: 3,
            descricao: "Pendente de Aceite"
        },
        PEDENTE_VALIDACAO: {
            id: "PEDENTE_VALIDACAO",
            idValue: 4,
            descricao: "Pendente de Validação"
        },
        SUSPENSO: {
            id: "SUSPENSO",
            idValue: 5,
            descricao: "Suspenso"
        },
        todas: [{
            id: "ATIVO",
            idValue: 1,
            descricao: "Ativo"
        }, {
            id: "CANCELADO",
            idValue: 2,
            descricao: "Cancelado"
        }, {
            id: "PENDENTE_ACEITE",
            idValue: 3,
            descricao: "Pendente de Aceite"
        }, {
            id: "PEDENTE_VALIDACAO",
            idValue: 4,
            descricao: "Pendente de Validação"
        }, {
            id: "SUSPENSO",
            idValue: 5,
            descricao: "Suspenso"
        }],
        getAll: function() {
            var allProperties = [];
            _.each(this, function(val, key) {
                if (!_.isFunction(val)) {
                    allProperties.push(val);
                }
            });
            return allProperties;
        }
    })
    .constant("CADEIA_PRODUTIVA", {
        ENERGIAS_HIDRICAS: {
            id: 23,
            descricao: "Energias Hídricas"
        },
        OUTROS: {
            id: 36,
            descricao: "Outros"
        },
        SERVICOS: {
            id: 38,
            descricao: "Serviços"
        },
        SUCROENERGETICO: {
            id: 42,
            descricao: "Sucroenergético"
        },
        MINERACAO_FERRO: {
            id: 34,
            descricao: "Mineração Ferro"
        },
        SIDERURGIA: {
            id: 39,
            descricao: "Siderurgia"
        },
        COMERCIO_E_SERVICOS: {
            id: 12,
            descricao: "Comércio e Serviços"
        },
        ELETRICO_ELETROELETRONICOS: {
            id: 17,
            descricao: "Elétrico e Eletroeletrônicos"
        }
    })
    .constant("SITUACAO_INSTRUMENTO_FORMALIZACAO", {
        ASSINADO: {
            id: 1,
            descricao: "Assinado"
        },
        CANCELADO: {
            id: 2,
            descricao: "Cancelado"
        },
        EM_NEGOCIACAO: {
            id: 3,
            descricao: "Em Negociação"
        },
        EXTINTO: {
            id: 4,
            descricao: "Extinto"
        },
        getAll: function() {
            var allProperties = [];
            _.each(this, function(val, key) {
                if (!_.isFunction(val)) {
                    allProperties.push(val);
                }
            });
            return allProperties;
        }
    })
    .constant("TIPO_INSTRUMENTO_FORMALIZACAO", {
        APROV_FINANCIAMENTO: {
            id: 'APROV_FINANCIAMENTO',
            idValue: 1,
            descricao: "Aprovação Financiamento BDMG"
        },
        AQUIS_TERRENO: {
            id: 'AQUIS_TERRENO',
            idValue: 2,
            descricao: "Aquisição de Terreno"
        },
        AUTORIZACAO_DIRETORIA: {
            id: 'AUTORIZACAO_DIRETORIA',
            idValue: 3,
            descricao: "Autorização Diretoria"
        },
        PROTOCOLO_INTENCAO: {
            id: 'PROTOCOLO_INTENCAO',
            idValue: 4,
            descricao: "Protocolo de Intenções"
        },
        TERMO_ADITIVO: {
            id: 'TERMO_ADITIVO',
            idValue: 5,
            descricao: "Termo Aditivo"
        },
        getAll: function() {
            var allProperties = [];
            _.each(this, function(val, key) {
                if (!_.isFunction(val)) {
                    allProperties.push(val);
                }
            });
            return allProperties;
        }
    })
    .constant('TIPO_EMPREGO', {
        TEMPORARIO: {
            id: 1,
            chave: "TEMPORARIO",
            descricao: "Temporário"
        },
        PERMANENTE: {
            id: 2,
            chave: "PERMANENTE",
            descricao: "Permanente"
        },
        TEMPORARIO_OBRA: {
            id: 3,
            chave: "TEMPORARIO_OBRA",
            descricao: "Temporário durante Obra"
        },
        PERMANENTE_OPERACAO: {
            id: 4,
            chave: "PERMANENTE_OPERACAO",
            descricao: "Permanente após início Operação"
        },
        TEMPORARIO_IMPLANTACAO_AGRICOLA: {
            id: 5,
            chave: "TEMPORARIO_IMPLANTACAO_AGRICOLA",
            descricao: "Temporário durante Implantação - Agrícola"
        },
        PERMANENTE_MATURACAO_AGRICOLA: {
            id: 7,
            chave: "PERMANENTE_MATURACAO_AGRICOLA",
            descricao: "Permanente após Maturação - Agrícola"
        },
        TEMPORARIO_IMPLANTACAO_INDUSTRIAL: {
            id: 6,
            chave: "TEMPORARIO_IMPLANTACAO_INDUSTRIAL",
            descricao: "Temporário durante Implantação - Industrial"
        },
        PERMANENTE_MATURACAO_INDUSTRIAL: {
            id: 8,
            chave: "PERMANENTE_MATURACAO_INDUSTRIAL",
            descricao: "Permanente após Maturação - Industrial"
        },
        getAll: function() {
            var allProperties = [];
            _.each(this, function(val, key) {
                if (!_.isFunction(val)) {
                    allProperties.push(val);
                }
            });
            return allProperties;
        }
    })
    .constant('TIPO_INVESTIMENTO', {
        INVESTIMENTO_MAQ_EQUIP_TERRENO_OBRAS: {
            id: "INVESTIMENTO_MAQ_EQUIP_TERRENO_OBRAS",
            descricao: "Investimento em máquinas, equipamentos, terrenos e obras"
        },
        INVESTIMENTO_CAPITAL_GIRO_E_OUTROS: {
            id: "INVESTIMENTO_CAPITAL_GIRO_E_OUTROS",
            descricao: "Capital de Giro e Outros investimentos (em R$)"
        },
        INVESTIMENTO_INDUSTRIAL: {
            id: "INVESTIMENTO_INDUSTRIAL",
            descricao: "Investimento Industrial"
        },
        INVESTIMENTO_AGRICOLA: {
            id: "INVESTIMENTO_AGRICOLA",
            descricao: "Investimento Agrícola"
        },
        INVESTIMENTO_CAPACITACAO_PROFISSIONAL: {
            id: "INVESTIMENTO_CAPACITACAO_PROFISSIONAL",
            descricao: "Investimento em capacitação profissional dos empregados"
        },
        INVESTIMENTO_PROPRIOS: {
            id: "INVESTIMENTO_PROPRIOS",
            descricao: "Investimentos Próprios"
        },
        INVESTIMENTO_TERRENO: {
            id: "INVESTIMENTO_TERRENO",
            descricao: "Terreno"
        },
        INVESTIMENTO_ESTUDO_PROJETO_DESP_PRE_OPERACIONAIS: {
            id: "INVESTIMENTO_ESTUDO_PROJETO_DESP_PRE_OPERACIONAIS",
            descricao: "Estudos/Projetos e Despesas Pré-Operacionais"
        },
        INVESTIMENTO_OBRAS_CIVIS_INSTALACOES: {
            id: "INVESTIMENTO_OBRAS_CIVIS_INSTALACOES",
            descricao: "Obras Civis/Instalações"
        },
        INVESTIMENTO_MAQ_EQUIP_NACIONAIS: {
            id: "INVESTIMENTO_MAQ_EQUIP_NACIONAIS",
            descricao: "Máquinas/Equip. Nacionais"
        },
        INVESTIMENTO_MAQ_EQUIP_IMPORTADOS: {
            id: "INVESTIMENTO_MAQ_EQUIP_IMPORTADOS",
            descricao: "Máquinas/Equip. Importados"
        },
        INVESTIMENTO_MAQ_EQUIP_USADOS: {
            id: "INVESTIMENTO_MAQ_EQUIP_USADOS",
            descricao: "Máquinas/Equip. Usados"
        },
        INVESTIMENTO_CAPITAL_GIRO: {
            id: "INVESTIMENTO_CAPITAL_GIRO",
            descricao: "Capital de Giro"
        },
        OUTROS_INVESTIMENTOS: {
            id: "OUTROS_INVESTIMENTOS",
            descricao: "Outros investimentos"
        },
        RECURSOS_PROPRIOS: {
            id: "RECURSOS_PROPRIOS",
            descricao: "Recursos próprios"
        },
        BDMG: {
            id: "BDMG",
            descricao: "BDMG"
        },
        OUTRAS_FONTES: {
            id: "OUTRAS_FONTES",
            descricao: "Outras Fontes"
        }
    })
    .constant('INSUMO_PRODUTO_ORIGEM', {
        IMPORTADOS: {
            id: "IMPORTADOS",
            descricao: "Importados"
        },
        MINAS_GERAIS: {
            id: "MINAS_GERAIS",
            descricao: "Minas Gerais"
        },
        OUTROS_ESTADOS: {
            id: "OUTROS_ESTADOS",
            descricao: "Outros Estados"
        },
        todas: [{
            id: "IMPORTADOS",
            descricao: "Importados"
        }, {
            id: "MINAS_GERAIS",
            descricao: "Minas Gerais"
        }, {
            id: "OUTROS_ESTADOS",
            descricao: "Outros Estados"
        }]
    }).constant('TIPO_FATURAMENTO', {
        FATURAMENTO_DO_GRUPO: {
            id: "FATURAMENTO_DO_GRUPO",
            descricao: "Faturamento do Grupo"
        },
        FATURAMENTO_REALIZADO_EM_MG: {
            id: "FATURAMENTO_REALIZADO_EM_MG",
            descricao: "Faturamento Realizado em Minas Gerais"
        },
        FATURAMENTO_PREVISTO: {
            id: "FATURAMENTO_PREVISTO",
            descricao: "Faturamento"
        },
        FATURAMENTO_PRODUTOS_INDUSTRIALIZADOS: {
            id: "FATURAMENTO_PRODUTOS_INDUSTRIALIZADOS",
            descricao: "Produtos Industrializados"
        },
        FATURAMENTO_PRODUTOS_ADQ_PARA_COMERCIALIZACAO: {
            id: "FATURAMENTO_PRODUTOS_ADQ_PARA_COMERCIALIZACAO",
            descricao: "Produtos Adquiridos Somente para Comercialização"
        }
    })
    .constant("TIPO_MEIO_AMBIENTE", {
        LP: {
            descricao: "LP - Licença Prévia",
            tipoMeioAmbiente: "LP",
            id: 1
        },
        LI: {
            descricao: "LI - Licença de Instalação",
            tipoMeioAmbiente: "LI",
            id: 2
        },
        LO: {
            descricao: "LO - Licença de Operação",
            tipoMeioAmbiente: "LO",
            id: 3
        },
        AAF: {
            descricao: "AAF - Autorização Ambiental de Funcionamento",
            tipoMeioAmbiente: "AAF",
            id: 4
        },
        OUTROS: {
            descricao: "Outros",
            tipoMeioAmbiente: "OUTROS",
            id: 5
        },
        getAll: function() {
            var allProperties = [];
            _.each(this, function(val, key) {
                if (!_.isFunction(val)) {
                    allProperties.push(val);
                }
            });
            return allProperties;
        }
    })
    .constant("CLASSE_EMPREENDIMENTO", {
        1: {
            id: 1,
            dias: 30
        },
        2: {
            id: 2,
            dias: 30
        },
        3: {
            id: 3,
            dias: 360
        },
        4: {
            id: 4,
            dias: 360
        },
        5: {
            id: 5,
            dias: 540
        },
        6: {
            id: 6,
            dias: 540
        },
        getAll: function() {
            var allProperties = [];
            _.each(this, function(val, key) {
                if (!_.isFunction(val)) {
                    allProperties.push(val);
                }
            });
            return allProperties;
        }
    })
    // Incluído para UC28
    .constant('STATUS_PROJETO', {
        ATIVO: {
            id: "ATIVO",
            descricao: "Ativo"
        },
        SUSPENSO: {
            id: "SUSPENSO",
            descricao: "Suspenso"
        },
        CANCELADO: {
            id: "CANCELADO",
            descricao: "Cancelado"
        },
        // CONCLUIDO: {
        //     id: "CONCLUIDO",
        //     descricao: "Concluído"
        // },
        getAll: function() {
            var allProperties = [];
            _.each(this, function(val, key) {
                if (!_.isFunction(val)) {
                    allProperties.push(val);
                }
            });
            return allProperties;
        }
    })
    .constant('TIPO_TEMPLATE', {
        PROJETO: {
            id: "PROJETO",
            descricao: "Projeto"
        },
        PROTOCOLO_INTENCOES: {
            id: "PROTOCOLO_INTENCOES",
            descricao: "Protocolo de Intenções"
        },
        TERMO_ADITIVO: {
            id: "TERMO_ADITIVO",
            descricao: "Termo Aditivo"
        }
    })
    .constant('STATUS_TEMPLATE', {
        false: "Inativo",
        true: "Ativo"
    }) //Below they are with numbers on the beginning because of the order who has to be respected
    .constant('TIPO_PERGUNTA', {
        SIM_OU_NAO: {
            _1SIM: {
                id: 1,
                descricao: 'SIM'
            },
            _2NAO: {
                id: 2,
                descricao: 'NÂO'
            }
        },
        AVALIATIVA: {
            _3EXCELENTE: {
                id: 3,
                descricao: 'EXCELENTE'
            },
            _4BOA: {
                id: 4,
                descricao: 'BOA'
            },
            _5REGULAR: {
                id: 5,
                descricao: 'REGULAR'
            },
            _6RUIM: {
                id: 6,
                descricao: 'RUIM'
            }
        }
    })
    .constant('TIPO_USUARIO', {
        INTERNO: 1,
        EXTERNO: 2
    })
    .constant('STATUS_PROCESSO', {
        DENTRO_PRAZO: {
            id: 1,
            descricao: "Dentro do Prazo"
        },
        FORA_PRAZO: {
            id: 0,
            descricao: "Fora do Prazo"
        },
        getAll: function() {
            var allProperties = [];
            _.each(this, function(val, key) {
                if (!_.isFunction(val)) {
                    allProperties.push(val);
                }
            });
            return allProperties;
        }
    })
    .constant('SITUACAO_RELATORIO_ACOMPANHAMENTO', {
        NAO_SOLICITADO: {
            id: "NAO_SOLICITADO",
            "descricao": "Não Solicitado pelo INDI"
        },
        NAO_RESPONDIDO: {
            id: "NAO_RESPONDIDO",
            "descricao": "Não Respondido pela Empresa"
        },
        PENDENTE_VALIDACAO: {
            id: "PENDENTE_VALIDACAO",
            "descricao": "Pendente de Validação"
        },
        VALIDADO: {
            id: "VALIDADO",
            "descricao": "Validado pelo Analista"
        },
        getAll: function() {
            var allProperties = [];
            _.each(this, function(val, key) {
                if (!_.isFunction(val)) {
                    allProperties.push(val);
                }
            });
            return allProperties;
        }
    })

.constant('ORIGEM_EMPRESA', {
        MINEIRA: {
            id: "MINEIRA",
            idValue: 1,
            "descricao": "Mineira"
        },
        OUTRO_ESTADO: {
            id: "OUTRO_ESTADO",
            idValue: 2,
            "descricao": "Outros Estados"
        },
        INTERNACIONAL: {
            id: "INTERNACIONAL",
            idValue: 3,
            "descricao": "Internacional"
        },
        getAll: function() {
            var allProperties = [];
            _.each(this, function(val, key) {
                if (!_.isFunction(val)) {
                    allProperties.push(val);
                }
            });
            return allProperties;
        }
    })
    .constant('DESCRICAO_FASE', {
        DECISAO_FORMALIZADA: {
            id: "DECISAO_FORMALIZADA",
            "descricao": "Decisão Formalizada"
        },
        INICIO_IMPLANTACAO: {
            id: "INICIO_IMPLANTACAO",
            "descricao": "Inicio de implantação do projeto"
        },
        INICIO_OPERACAO: {
            id: "INICIO_OPERACAO",
            "descricao": "Início de operação"
        },
        INICIO_TRATAMENTO_TRIBUTARIO: {
            id: "INICIO_TRATAMENTO_TRIBUTARIO",
            "descricao": "Inicio da utilização de tratamento tributário"
        }
    })
    .constant('SITUACAO_RELATORIO', {
        NAO_SOLICITADO: {
            id: "NAO_SOLICITADO",
            descricao: "Não Solicitado pelo INDI"
        },
        NAO_RESPONDIDO: {
            id: "NAO_RESPONDIDO",
            descricao: "Não Respondido pela Empresa"
        },
        PENDENTE_VALIDACAO: {
            id: "PENDENTE_VALIDACAO",
            descricao: "Pendente de Validação"
        },
        VALIDADO: {
            id: "VALIDADO",
            descricao: "Validado pelo Analista"
        },
        TODOS: {
            id: "TODOS",
            descricao: "Todos"
        }
    })
