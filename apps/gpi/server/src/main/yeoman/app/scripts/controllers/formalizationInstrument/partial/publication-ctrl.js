'use strict';
angular.module('gpifrontApp')
    .controller('PublicationCtrl', function($scope, growl, dialogs, $state, $filter, $stateParams, ScreenService, TIPO_INSTRUMENTO_FORMALIZACAO, Restangular, SITUACAO_INSTRUMENTO_FORMALIZACAO, TIPO_USUARIO) {

        // Usado para validar o form
        $scope.formSubmitted = false;
        $scope.screenService = ScreenService;
        $scope.showProtocolCode = true;

        var initialize = function() {

            // Configurações do datepicker
            $scope.configsDatepickerPublication = {
                required: true,
                disabled: $scope.screenService.isShowScreen() || $scope.isPromocaoOuGerente()
            };

            Restangular.one("instrumento").one($stateParams.id).one("publicacao").get().then(function(publicacao) {
                $scope.publicacao = publicacao;
                //  Atualiza o datepicker
                $scope.configsDatepickerPublication.restangularUpdated = !$scope.configsDatepickerPublication.restangularUpdated;

                // RNE101
                if(!$scope.isProtocolCodeVisible()){
                   $scope.showProtocolCode = false;
                }
                if($scope.isProtocolCodeDisabled()){
                    $scope.disableProtocolCode = true;
                }
            });
        };

        //Método criado para validar se o usuário abriu a aba publicação via URL
        var validAccessPublication = function() {
            Restangular.one("instrumento/recoverSituacaoById", $scope.getId()).get().then(function(situacao) {
                var situacaoIF = SITUACAO_INSTRUMENTO_FORMALIZACAO[situacao.replace('"',"").replace('"',"")].id;

                if (situacaoIF !== SITUACAO_INSTRUMENTO_FORMALIZACAO['ASSINADO'].id &&
                        situacaoIF !== SITUACAO_INSTRUMENTO_FORMALIZACAO['CANCELADO'].id) {
                    growl.error("Somente é permitido acessar essa aba se a situação do Instrumento de Formalização for \'Assinado\' e \'Cancelado\'");
                    $state.go('base.formalizationInstrument.formalizationInstrumentTpl', {
                        'screen': $scope.screenService.getScreenName(),
                        'id': $scope.getId()
                    });
                }
            });
        }

        // Valida e carrega as configurações iniciais da tela
        validAccessPublication();
        initialize();

        $scope.isProtocolCodeVisible = function(){
            return $scope.publicacao.tipo === TIPO_INSTRUMENTO_FORMALIZACAO['TERMO_ADITIVO'].id ||
                    $scope.publicacao.tipo === TIPO_INSTRUMENTO_FORMALIZACAO['PROTOCOLO_INTENCAO'].id;
        }

        $scope.isProtocolCodeDisabled = function(){
            return $scope.publicacao.tipo === TIPO_INSTRUMENTO_FORMALIZACAO['TERMO_ADITIVO'].id;
        }

        /*
         *   Valida o campo passado
         */
        $scope.checkValidation = function(formField) {
            return formField.$invalid && (formField.$dirty || $scope.formSubmitted);
        }

        $scope.save = function(isValid) {
            if (isValid) {
                if(!$scope.publicacao.anexos || $scope.publicacao.anexos.length < 1){
                    growl.error("Obrigatório anexar pelo menos um documento na aba \"Publicação\". ");
                }else{
                    if(!$scope.publicacao.usuarioResponsavel.tipo) {
                        $scope.publicacao.usuarioResponsavel.tipo = TIPO_USUARIO.INTERNO;
                    }
                    Restangular.all('instrumento').one($stateParams.id).post("publicacao", $scope.publicacao).then(function(empregoSalvo) {
                        growl.success($filter('translate')("message.success.MSG006_REGISTRO_GRAVADO_SUCESSO"));
                        // Utilizado para permitir que o usuário mude de página
                        // sem a mensagem de perca de dados
                        $scope.formPublication.$setPristine();
                        setTimeout(function() {
                            $state.transitionTo($state.current, $stateParams, {
                                reload: true,
                                inherit: true,
                                notify: true
                            });
                        }, 2000);
                    }, function(response) {
                        growl.error($filter('translate')(response.data));
                    });
                }
            } else {
                growl.error($filter('translate')("message.error.MSGXX_DADOS_INVALIDOS"));
                $scope.formSubmitted = true;
            }
        }
    });