'use strict'

angular
	.module('gpifrontApp')
	.controller('AttachedFilesFinancialGridCtrl', function($scope, GridButton, dialogs, $filter){

        $scope.gridScope = {};

        // initialize its data-model attribute
        $scope.anexosFinanceiro = $scope.$parent.anexosFinanceiro;

        // initialize its selected-items attribute
        $scope.anexosFinanceiroSelectedFiles = [];
        
        //Define its column-defs attribute
        $scope.anexosFinanceiroColumnDefs = [{        
            field: 'nome',
            enableCellEdit: false,
            width: "**",
            translationKey: 'common.file.name'
        }, {
            field: 'dataAnexo',
            enableCellEdit: false,
            width: "**",
            translationKey: 'common.date',
            cellFilter: 'dateTransformFilter'
        }];


        //define its sort-info attribute if you desire a default-sort
        $scope.sortInfo = {
            fields: ['nome'],
            directions: ['asc']
        };


        function translate(translationKey) {
            var text = "";
            if (typeof translationKey !== 'undefined' && translationKey) {
                text = $filter('translate')(translationKey);
            }
            return text;
        };

        $scope.preDeleteFile = function() {
            var confirmDialog = dialogs.confirm(translate('DIALOGS_CONFIRMATION'), translate('message.confirm.MSG009_CONFIRMAR_EXCLUSAO'));
            confirmDialog.result.then(function(btn) {
                // Caso confirme executa a exclusão
                $scope.deleteFile();
            }, function(btn) {
                $scope.gridScope.checkSelectAllCheckBox(true, true);
                $scope.gridScope.checkSelectAllCheckBox(false, true);
                // Caso não confirme deseleciona os registros selecionados
                $scope.anexosFinanceiroSelectedFiles.splice(0, $scope.anexosFinanceiroSelectedFiles.length);
            });

        }

        $scope.deleteFile = function() {
            if ($scope.$parent.anexosFinanceiro.length > 0 && $scope.anexosFinanceiro.length > 0) {
                $scope.anexosFinanceiroSelectedFiles.forEach(function(selected) {
                    var model = $scope.$parent.anexosFinanceiro;
                    var indexOnMainList = model.indexOf(selected);
                    // deleting each unit from main list
                    $scope.$parent.anexosFinanceiro.splice(indexOnMainList, 1);
                    for (var i = 0; i < $scope.$flow.files.length; i++) {
                        var file = $scope.$flow.files[i];
                        file.cancel();
                    };
                });
                // deleting all 'selectedItems' starting at index 0
                $scope.anexosFinanceiroSelectedFiles.splice(0, $scope.anexosFinanceiroSelectedFiles.length);
                growl.info($filter('translate')('message.error.MSG38_SECAO_ATUALIZADA', {
                     value: "Anexos Financeiros"
                 }));
            }
        }  

        $scope.disableFn = function () {
            return $scope.anexosFinanceiroSelectedFiles.length < 1;
        }              

        $scope.$parent.$watch('financeiro.anexos', function(newVal, oldVal) {
            setTimeout(function() {
                $scope.$apply(function(internalData) {
                    if (internalData) {
                        $scope.anexosFinanceiro = internalData;
                    }
                }(newVal));
                // $scope.anexos = $scope.$parent.contato.anexos;
            }, true);
        }, true);


	});