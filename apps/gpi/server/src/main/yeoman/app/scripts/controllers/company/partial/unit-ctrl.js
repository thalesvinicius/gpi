'use strict';

angular.module('gpifrontApp')
    .controller('UnitCtrl', function($scope, UserSession, TIPO_USUARIO, Restangular, $stateParams, growl, dialogs, $filter, $state, ScreenService, AUTH_ROLES, AuthService, $location, $anchorScroll) {

        var baseEmpresas = Restangular.all('empresa');
        var formSubmitted = false;

        $scope.checkValidation = function(formField) {
            return formField.$invalid && (formField.$dirty || formSubmitted);
        }

        $scope.isExternalUser = UserSession.getUser().tipo == TIPO_USUARIO.EXTERNO;

        $scope.screenService = ScreenService;

        $scope.gridScope = {preDelete: function(){return false}};

        $scope.unidade = {
        };
        $scope.unidades = [];

        // initialize its selected-items attribute
        $scope.unidadesSelectedItems = [];
        $scope.contatoUnidadeSelectedItems = [];
        $scope.unitGridFunctions = ["showUnit"];

        $scope.showUnitForm = false;

        var clearUnitForm = function() {
            angular.element("#unitForm").trigger("reset");
        };

        var getSelectedUnits = function() {
            return $scope.unidadesSelectedItems;
        };

        var showUnitForm = function() {
            $scope.showUnitForm = true;
            setTimeout(function(){
                if($scope.disableUnitFormDiv){
                    $location.hash('unit-name-input');
                    $anchorScroll();
                }else{
                    angular.element("#unit-name-input").focus()
                }
            },true);
        };

        var hideUnitForm = function() {
            $scope.showUnitForm = false;
        };

        var getUnitFromScopeById = function(unitId) {
            for (var i = $scope.unidades.length - 1; i >= 0; i--) {
                if ($scope.unidades[i].id == unitId) {
                    return $scope.unidades[i];
                }
            }
            return null;
        };

        $scope.cancel = function() {
            if($scope.unitForm.$dirty){
                var cancelDialog = dialogs.confirm($filter('translate')('DIALOGS_CONFIRMATION'), $filter('translate')('message.confirm.MSG31_CANCELAR'));
                cancelDialog.result.then(function(btn) {
                    //  Altera o form para pristine para não exibir a MSG33_DADOS_NAO_SALVOS
                    $scope.unitForm.$setPristine();
                    $scope.setUnidades();
                    $scope.returnBack();
                });
            }else{
                $scope.returnBack();
            }
        }

        $scope.setUnidades = function() {
            $scope.unidades = baseEmpresas.one($stateParams.id).one("unidade").getList().$object;
        }

        $scope.returnBack = function() {
            $scope.unidade = {
            };
            hideUnitForm();
            clearUnitForm();
        }

        $scope.showUnit = function(unitId) {
            $scope.disableUnitFormDiv = true;
            $scope.unidade = getUnitFromScopeById(unitId);
            showUnitForm();
        }

        $scope.disableButtons = function() {
            return $scope.unidadesSelectedItems.length !== 1;
        }

        $scope.prepareCreateUnit = function() {
            $scope.disableUnitFormDiv = false;
            $scope.unidade = {
            };
            clearUnitForm();
            showUnitForm();
        };

        $scope.disableNewFn = function() {
            return AuthService.isUserAuthorized([AUTH_ROLES.infraAnalist, AUTH_ROLES.informationAnalist, AUTH_ROLES.director]);
        }

        $scope.disableEditFn = function() {
            return $scope.unidadesSelectedItems.length !== 1 || AuthService.isUserAuthorized([AUTH_ROLES.infraAnalist, AUTH_ROLES.informationAnalist, AUTH_ROLES.director]);
        }

        $scope.disableDeleteFn = function() {
            return $scope.unidadesSelectedItems.length == 0 || AuthService.isUserAuthorized([AUTH_ROLES.infraAnalist, AUTH_ROLES.informationAnalist, AUTH_ROLES.director]);
        }

        $scope.prepareEditUnit = function() {
            $scope.disableUnitFormDiv = false;
            var selectedUnit = getSelectedUnits()[0].id;
            $scope.unidade = getUnitFromScopeById(selectedUnit);
            showUnitForm();
        };

        $scope.preDelete = function () {
            var unidadesId = [];

            for (var i = 0; i < $scope.unidadesSelectedItems.length; i++) {
                unidadesId.push($scope.unidadesSelectedItems[i].id);
            };

            Restangular.all('projeto/unidade').getList({"id" : unidadesId}).then(function(result) {
                var projetosTO = result;

                if (projetosTO.length > 0) {
                    var unidadesWithProjetosId = [];
                    for (var i = 0; i < projetosTO.length; i++) {
                        unidadesWithProjetosId.push(projetosTO[i].idEmpresa);
                    };
                    growl.error($filter('translate')('message.error.MSG35_EXCLUSAO_UNIDADE'));
                    return false;
                } else {
                    $scope.deleteUnit();
                    $scope.gridScope.deleteFn(true);
                }
            });

            return false;

        }

        $scope.deleteUnit = function() {

            var idsToDelete = [];
            if ($scope.unidades.length > 0 && $scope.unidadesSelectedItems.length > 0) {
                $scope.unidadesSelectedItems.forEach(function(selected) {
                    idsToDelete.push(selected.id);
                });
            }

            if (idsToDelete.length > 0) {
                baseEmpresas.one($stateParams.id).one("unidade").post("removeUnidade", idsToDelete).then(function() {
                    growl.success("Exclusão efetuada com sucesso.")
                    hideUnitForm();
                }, function() {
                    growl.error('Ocorreu um erro ao tentar efetuar a exclusão.')
                    return false;
                })
            }
            $scope.returnBack();

            return true;
        };

        $scope.save = function(isFormValid) {
            if (!isFormValid) {
                formSubmitted = true
                growl.error($filter('translate')("message.error.MSGXX_DADOS_INVALIDOS"));
                return false;
            }

            $scope.unidade.empresa = new Object();
            $scope.unidade.empresa.id = $stateParams.id;

            //  Verifica se já existe uma unidade com este nome salva na empresa
            baseEmpresas.one($stateParams.id).one("unidade/getUnidadeComNome").get({"nomeUnidade": $scope.unidade.nome}).then(function(idUnidade) {
                if(angular.isUndefined(idUnidade) || (!angular.isUndefined($scope.unidade.id) && idUnidade === $scope.unidade.id.toString())){
                    baseEmpresas.one($stateParams.id).post("unidade", $scope.unidade).then(function() {
                        growl.success("Registro gravado com sucesso!");
                        // Utilizado para permitir que o usuário mude de página
                        // sem a mensagem de perca de dados
                        $scope.unitForm.$setPristine();
                        setTimeout(function() {
                            $state.transitionTo($state.current, $stateParams, {
                                reload: true,
                                inherit: true,
                                notify: true
                            });
                        }, 5000);
                    }, function() {
                        growl.error("Ocorreu um erro ao tentar gravar o registro!");
                        return false;
                    });

                }else{
                    growl.error($filter('translate')("message.error.MSG25_UNIDADE_DUPLICADA"));
                    return false;
                }
            });

            return true;
        }

        $scope.checkUserAuthorization();        
        $scope.setUnidades();

    });
