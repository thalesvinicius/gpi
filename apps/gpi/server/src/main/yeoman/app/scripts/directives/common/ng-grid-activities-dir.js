'use strict';

angular
    .module('gpifrontApp')
    .directive('ngGridActivities', function($rootScope, $filter, configuration, growl, ScreenService, dialogs, Restangular, $state, $stateParams) {
        return {
            restrict: 'E',
            scope: {
                model: '=model',
                columnDefs: '=columnDefs',
                selectedItems: '=selectedItems',
                deleteServicePath: '=deleteServicePath',
                saveServicePath: '=saveServicePath',
                gridType: '=gridType',
                small: '=small'
            },
            templateUrl: configuration.rootDir + 'views/common/ng-grid-activities-dir-tpl.html',
            compile: function(cElem, cAttrs) {
                return {
                    pre: function(scope, iElement, iAttrs) {

                        scope.hasNew = false;

                        if (iAttrs.gridScope) {
                            for (var property in scope.$parent[iAttrs.gridScope]) {
                                scope[property] = scope.$parent[iAttrs.gridScope][property];
                            }
                            scope.$parent[iAttrs.gridScope] = scope;
                        }

                        if (_.isUndefined(scope.disableActions)) {
                            scope.disableActions = function() {
                                return ScreenService.isShowScreen();
                            }
                        }

                        scope.fileAdded = function(event, $flow, flowFile, activity) {
                            if (!scope.validateFileExtensions(flowFile.name)) {
                                growl.error(translate("message.error.MSG61_EVIDENCIA_INVALIDA"));
                                event.preventDefault();
                                return false;
                            } else if (!scope.validateFileSize(flowFile.size)) {
                                growl.error(translate("message.error.MSG61_EVIDENCIA_INVALIDA"));
                                event.preventDefault();
                                return false;
                            }

                            activity.internalFlowFile = flowFile;
                            setTimeout(function() {
                                if (!flowFile.isUploading() && !flowFile.isComplete()) {
                                    flowFile.resume();
                                }
                            }, true);
                        };
                        scope.fileSuccess = function(flowFile, activity, response) {
                            var responseObj = JSON.parse(response);
                            activity.internalFlowFile = null;
                            activity.anexo = responseObj;
                            scope.preSaveActivity(activity);
                        };
                        scope.internalFlowFiles = {};
                        scope.gridScope = {};
                        scope.injectionFunctions = 'downloadLink';

                        /**
                         * Valida Tamanho Máximo
                         */
                        scope.validateFileSize = function(fileSize) {
                                if (fileSize > 52428800) {
                                    return false;
                                } else {
                                    return true;
                                }
                            }
                            /**
                             * Valida Formatos permitidos
                             */
                        scope.validateFileExtensions = function(fileName) {
                            var fileExtensioRegExt = /(?:\.([^.]+))?$/;
                            var ext = fileExtensioRegExt.exec(fileName)[1];

                            var acceptedExtensions = ['doc', 'docx', 'pdf', 'xls', 'xlsx', 'txt', 'jpg', 'gif', 'png', 'bmp', 'html', 'swf', 'xml', 'htmlx', 'tif', 'odt', 'pptx', 'ppt', 'xlf', 'msg', 'eml', 'dbx', 'zip', 'rar', 'gtm', 'kml', 'kmz'];

                            if (ext && _.contains(acceptedExtensions, ext)) {
                                return true;
                            } else {
                                return false;
                            }
                        };

                        scope.gridOptions = {};
                        scope.gridOptions.selectedItems = scope.selectedItems;
                        scope.gridOptions.columnDefs = scope.columnDefs;
                        scope.gridOptions.data = 'model';
                        scope.gridOptions.sortInfo = {
                            fields: ['posicao'],
                            directions: ['asc']
                        };

                        scope.gridOptions.rowTemplate = "<div ng-class=\"{yellowRow: row.entity.ativo}\"><div ng-style=\"{ 'cursor': row.cursor }\" ng-repeat=\"col in renderedColumns\" ng-class=\"col.colIndex()\" class=\"ngCell {{col.cellClass}}\">\r" +
                            "\n" +
                            "\t<div class=\"ngVerticalBar\" ng-style=\"{height: rowHeight}\" ng-class=\"{ ngVerticalBarVisible: !$last }\">&nbsp;</div>\r" +
                            "\n" +
                            "\t<div ng-cell></div>\r" +
                            "\n" +
                            "</div></div>";

                        scope.gridOptions.enableRowSelection = false;
                        scope.gridOptions.enableSorting = false;
                        scope.gridOptions.enableColumnResize = true;

                        scope.configsDatepicker = {
                            viewMode: 'days',
                            restangularUpdated: false
                        };

                        scope.columnDefs.unshift({
                            field: "xpto",
                            translationKey: '',
                            width: 25,
                            cellTemplate: "<div class=\"ngCellText\" ng-if=\"!row.entity.concluido && !row.entity.ativo\" ng-class=\"col.colIndex()\">" +
                                "<div ng-if=\"!hasNew && !disableActions()\" ng-click=\"activityUp(row.entity.posicao);\" class=\"gridChevrons\"><span class=\"fa fa-chevron-up gridChevrons\"></span></div>" +
                                "<div ng-if=\"!hasNew && !disableActions()\" ng-click=\"activityDown(row.entity.posicao);\"><span class=\"fa fa-chevron-down gridChevrons\"></span></div></div>",
                            enableCellEdit: false
                        });

                        function showObservationField() {
                            scope.columnDefs.push({
                                field: "obs",
                                translationKey: 'common.observation',
                                cellTemplate: '<div ng-if=\"!hasNew && !disableActions()\" class="text-center" ng-class="col.colIndex()">' +
                                    '<a ng-click="showActivityObservation(row.entity)" data-toggle="modal" data-target="#observations-modal"><i class="fa fa-comments-o page-header-icon"></i></a></div>',
                                width: 80,
                                enableCellEdit: false
                            });
                        }

                        scope.enablePathField = function (activity) {
                            var isIIorOIorCC = activity.ii || activity.oi || activity.cc;
                            var dontNeedAttachmentToSave = (isGridProjeto() && !isIIorOIorCC);
                            var needAttachmentToSave = isGridProjeto() && isIIorOIorCC;
                            return (((!isGridProjeto() || dontNeedAttachmentToSave) && activity.concluido && !scope.hasNew) || (activity.ativo && needAttachmentToSave)) && !scope.disableActions();
                        }

                        function showPathField() {
                            scope.columnDefs.push({
                                field: "path",
                                translationKey: 'common.evidence',
                                cellTemplate: '<div class="text-center" ng-class="col.colIndex()" flow-init="{singleFile:true, testChunks:false}" flow-file-added="fileAdded($event, $flow, $file, row.entity)" flow-file-success="fileSuccess($file, row.entity, $message)">' +
                                    '<i flow-btn ng-if="enablePathField(row.entity)" class="fa fa-paperclip page-header-icon"></i>' +
                                    '<a target="_self" href="{{downloadLink(row.entity.anexo.path, row.entity.anexo.nome)}}" ng-if="!row.entity.internalFlowFile && row.entity.anexo"><i class="fa fa-eye page-header-icon"></i></a>' +
                                    '<div ng-if="row.entity.internalFlowFile" class="progress progress-striped" ng-class="{active: row.entity.internalFlowFile.isUploading()}">' +
                                    '<div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" ng-style="{width: (row.entity.internalFlowFile.progress() * 100) + \'%\'}" style="width: 100%;">' +
                                    '<span class="sr-only ng-binding">1% Complete</span>' +
                                    '</div>' +
                                    '</div>' +
                                    '</div>',
                                width: 80,
                                enableCellEdit: false
                            });
                        }

                        function showDeleteField() {
                            scope.columnDefs.push({
                                field: "delete",
                                translationKey: 'common.delete',
                                cellTemplate: (!isGridTemplate() ?
                                    '<div class="text-center" ng-if="!row.entity.dataConclusao && !row.entity.ativo && !row.entity.obrigatorio && (!row.entity.id || !hasNew) && !disableActions()" ng-class="col.colIndex()" ng-click="preRemoveActivity(row.entity)"><i class="fa fa-trash-o page-header-icon"></i></div>' :
                                    '<div class="text-center" ng-class="col.colIndex()" ng-click="preRemoveActivity(row.entity)"><i class="fa fa-trash-o page-header-icon"></i></div>'),

                                width: 65,
                                enableCellEdit: false
                            });
                        }

                        function showEditField() {
                            scope.columnDefs.push({
                                field: "edit",
                                translationKey: 'common.edit',
                                cellTemplate: '<div ng-if="!isEditing(row.entity) && !row.entity.concluido && (!row.entity.id || !hasNew) && !disableActions()" class="text-center" ng-class="col.colIndex()" ng-click="editActivity(row.entity)"><i class="fa fa-edit page-header-icon"></i></div>' +
                                    '<div ng-if="isEditing(row.entity) && (!row.entity.id || !hasNew)" class="text-center" ng-class="col.colIndex()" ng-click="preSaveActivity(row.entity)"><i class="fa fa-check page-header-icon"></i></div>',
                                width: 65,
                                enableCellEdit: false
                            })
                        }

                        function addColumnsOnEnd(columns) {
                            for (var i = 0; i < columns.length; i++) {
                                scope.columnDefs.push(columns[i]);
                            };
                        }

                        function showAllFields() {
                            showObservationField();
                            showPathField();
                            showDeleteField();
                            showEditField();
                        }

                        function isGridMonitoring() {
                            return scope.gridType && scope.gridType === 'ACOMPANHAMENTO';
                        }

                        function isGridTemplate() {
                            return scope.gridType && scope.gridType === 'TEMPLATE';
                        }

                        function isGridProjeto() {
                            return scope.gridType && scope.gridType === 'PROJETO';
                        }

                        function isGridInstrument() {
                            return scope.gridType && scope.gridType === 'INSTRUMENTO';
                        }


                        if (eval(iAttrs.hasAll)) {
                            showAllFields();
                        }

                        if (eval(iAttrs.hasShow) && !eval(iAttrs.hasAll)) {
                            showObservationField();
                        }

                        if (eval(iAttrs.hasPath) && !eval(iAttrs.hasAll)) {
                            showPathField();
                        }

                        if (eval(iAttrs.hasDelete) && !eval(iAttrs.hasAll)) {
                            showDeleteField();
                        }

                        if (eval(iAttrs.hasEdit) && !eval(iAttrs.hasAll)) {
                            showEditField();
                        }

                        if (iAttrs.columnDefsOnEnd) {
                            addColumnsOnEnd(scope.$parent[iAttrs.columnDefsOnEnd]);
                        }

                        scope.activityUp = function(activityOrder) {
                            for (var index = 0; index < scope.model.length; index++) {
                                var activity = scope.model[index];
                                if (activity.posicao === activityOrder && activity.id) {
                                    var parentActivity = scope.model[index - 1];
                                    if (parentActivity && parentActivity.id && !angular.isUndefined(parentActivity) && !parentActivity.concluido && !parentActivity.ativo) {
                                        scope.changeItemsPosition(index, index - 1);
                                        break;
                                    }
                                }
                            }
                        };
                        scope.activityDown = function(activityOrder) {
                            for (var index = 0; index < scope.model.length; index++) {
                                var activity = scope.model[index];
                                if (activity.posicao === activityOrder && activity.id) {
                                    var bottomActivity = scope.model[index + 1];
                                    if (bottomActivity && bottomActivity.id && !angular.isUndefined(bottomActivity) && !bottomActivity.concluido && !bottomActivity.ativo) {
                                        scope.changeItemsPosition(index + 1, index);
                                        break;
                                    }
                                }
                            }
                        };

                        scope.getCurrentLocation = function() {
                            var currentLocationFound = false;
                            scope.currentLocation = {};
                            for (var i = 0; i < scope.model.length; i++) {
                                if ((angular.isUndefined(scope.model[i].dataConclusao) || !scope.model[i].dataConclusao)) {
                                    scope.model[i].concluido = false;
                                    if (!currentLocationFound && !scope.model[i].paralela) {
                                        scope.model[i].ativo = true;

                                        scope.currentLocation = scope.model[i];

                                        currentLocationFound = true;
                                    } else {
                                        scope.model[i].ativo = false;
                                    }
                                } else {
                                    scope.model[i].ativo = false;
                                    scope.model[i].concluido = true;
                                }
                            };
                        }

                        scope.getCurrentLocationPosition = function() {
                            for (var i = 0; i < scope.model.length; i++) {
                                if (scope.model[i].ativo === true) {
                                    return i;
                                }
                            };
                            // returning the last position of array
                            return scope.model.length - 1;
                        }

                        scope.changeItemsPosition = function(topItemIndex, bottomItemIndex) {
                            var posicaoTemp = scope.model[topItemIndex].posicao;
                            scope.model[topItemIndex].posicao = scope.model[bottomItemIndex].posicao;
                            scope.model[bottomItemIndex].posicao = posicaoTemp;

                            var bottomItem = scope.model.splice(bottomItemIndex, 1);
                            scope.model[bottomItemIndex] = scope.model[topItemIndex - 1];
                            setTimeout(function() {
                                scope.model.splice(topItemIndex, 0, bottomItem[0]);
                                if (scope.saveServicePath) {
                                    scope.saveActivity(scope.model[topItemIndex], true, true);
                                    scope.saveActivity(scope.model[bottomItemIndex], false, true);
                                }
                                scope.$apply();
                            }, true);
                        };

                        scope.showActivityObservation = function(activity) {
                            scope.observationActivity = activity;
                            scope.currentObservation = activity.observacao;
                        };

                        scope.saveObservation = function(activity) {
                            scope.observationActivity.observacao = scope.currentObservation;
                            scope.preSaveActivity(scope.observationActivity);
                            // scope.cleanObservation();
                        };

                        scope.cleanObservation = function() {
                            scope.observationActivity = null;
                            scope.currentObservation = '';
                        };

                        scope.isEditing = function(activity) {
                            if (activity) {
                                if (activity.concluido === true) {
                                    return false;
                                }
                            }

                            if (!activity)
                                return (scope.editingActivity && !angular.isUndefined(scope.editingActivity));
                            return (scope.editingActivity && !angular.isUndefined(scope.editingActivity) && scope.editingActivity.posicao === activity.posicao);
                        };

                        scope.addActivity = function(paralela) {
                            var currentLocationPosition = scope.getCurrentLocationPosition();
                            var newOrder = currentLocationPosition + 2;
                            if (scope.model.length > 0 && angular.isUndefined(scope.model[scope.model.length - 1].id)) {
                                return;
                            }

                            var dataInicio = null;

                            for (var i = scope.model.length - 1; i >= 0; i--) {
                                if (scope.model[i].paralela === true) {
                                    continue;
                                }

                                if (!angular.isUndefined(scope.model[i].dataConclusao) && scope.model[i].dataConclusao != null) {
                                    dataInicio = scope.model[i].dataConclusao;
                                    break;
                                } else if (!angular.isUndefined(scope.model[i].dataInicio) && scope.model[i].dataInicio != null) {
                                    break;
                                }
                            };


                            if (dataInicio && !paralela) {
                                scope.model.splice(currentLocationPosition + 1, 0, {
                                    posicao: newOrder,
                                    concluido: false,
                                    ativo: false,
                                    paralela: paralela,
                                    dataInicio: dataInicio
                                });
                                // scope.model.push({
                                //     posicao: newOrder,
                                //     concluido: false,
                                //     ativo: false,
                                //     paralela: paralela,
                                //     dataInicio: dataInicio
                                // });

                            } else {
                                scope.model.splice(currentLocationPosition + 1, 0, {
                                    posicao: newOrder,
                                    concluido: false,
                                    ativo: false,
                                    paralela: paralela
                                });

                                // scope.model.push({
                                //     posicao: newOrder,
                                //     concluido: false,
                                //     ativo: false,
                                //     paralela: paralela
                                // });
                            }

                            if (dataInicio) {
                                dataInicio.delete;
                            }
                            scope.hasNew = true;

                            scope.getCurrentLocation();
                        }

                        scope.editActivity = function(activity) {
                            if (scope.isEditing()) {
                                angular.copy(scope.tempEditingActivity, scope.editingActivity);
                            }
                            scope.editingActivity = activity;
                            scope.tempEditingActivity = angular.copy(activity);
                        };

                        scope.preSaveActivity = function(activity) {
                            convertAtividadeAndLocal(activity);

                            if (angular.isUndefined(activity.localAtividade) || !activity.localAtividade) {
                                growl.error("Campo \"Local\" é obrigatório!<br>Campo \"Atividade\" é obrigatório!");
                            } else if (angular.isUndefined(activity.atividadeLocal) || !activity.atividadeLocal) {
                                growl.error("Campo \"Atividade\" é obrigatório!");
                            } else if (activity.paralela && activity.dataConclusao && (angular.isUndefined(activity.dataInicio) || !activity.dataInicio)) {
                                growl.error("Campo \"Início\" é obrigatório!");
                            } else if (isGridProjeto() && activity.ativo && (angular.isUndefined(activity.dataConclusao) || !activity.dataConclusao)) {
                                growl.error("Campo \"Data de Realização\" é obrigatório!");
                            } else if (activity.dataConclusao && !activity.concluido) {
                                var confirmDialog = dialogs.confirm(translate('DIALOGS_CONFIRMATION'), translate('message.confirm.MSG59_CONFIRMA_CONCLUSAO_ATIVIDADE'));
                                confirmDialog.result.then(function(btn) {
                                    if (isGridProjeto()) {
                                        Restangular.one("projeto/" + $stateParams.id + "/localizacao").get().then(function(localizacao) {
                                            if (activity.ii || activity.oi || activity.cc) {
                                                if (activity.ii) {
                                                    if (angular.isUndefined(localizacao[0]) || !localizacao[0] || angular.isUndefined(localizacao[0].municipio) || !localizacao[0].municipio) {
                                                        growl.error(translate("message.error.MSG62_MUNICIPIO_NAO_PREENCHIDO"));
                                                        return;
                                                    }
                                                }
                                                if (angular.isUndefined(activity.anexo) || !activity.anexo) {
                                                    growl.error("Campo \"Evidência\" é obrigatório!");
                                                    return;
                                                }
                                            }
                                            // Caso confirme executa a persistencia
                                            scope.saveActivity(activity, false, false, true, true);
                                            // fillDataInicioNextActivity(activity);
                                        });
                                    } else {
                                        // Caso confirme executa a persistencia
                                        scope.saveActivity(activity, false, false, true, true);
                                        // fillDataInicioNextActivity(activity);
                                    }
                                }, function(btn) {

                                });

                            } else {
                                scope.saveActivity(activity);
                            }
                        }

                        function convertAtividadeAndLocal(activity) {
                            if (typeof activity.localAtividade == "string") {
                                activity.localAtividade = {
                                    id: parseInt(activity.localAtividade)
                                };
                                if (activity.atividadeLocal) {
                                    activity.atividadeLocal = {
                                        id: parseInt(activity.atividadeLocal),
                                        local: activity.localAtividade
                                    }
                                }
                            } else if (typeof activity.atividadeLocal == "string") {
                                activity.atividadeLocal = {
                                    id: parseInt(activity.atividadeLocal),
                                    local: activity.localAtividade
                                }
                            }
                        }

                        function fillDataInicioNextActivity(activity) {
                            var i = activity.posicao;
                            /* Iterating on the main list looking for the first
                                     standard activity to set its dataInicio property */
                            while (scope.model.length >= i) {
                                var nextActivity = scope.model[i];
                                if (nextActivity && !nextActivity.paralela) {
                                    nextActivity.dataInicio = activity.dataConclusao;
                                    //calling save recursively passing the nextActivity as parameter and saveNext == false to stop recursivity
                                    scope.saveActivity(nextActivity, true, false, false, false);
                                    break;
                                }
                                i++;
                            }

                        }

                        scope.saveActivity = function(activity, noMessage, noReload, conclusion, saveNext) {
                            if (conclusion) {
                                activity.concluido = true;
                            }

                            Restangular.all(scope.saveServicePath).post(activity).then(function(projectCloned) {
                                scope.hasNew = false;
                                scope.editingActivity = null;
                                scope.getCurrentLocation();
                                if (!noMessage) {
                                    if (conclusion === true) {
                                        growl.success(translate('message.success.MSG60_ATIVIDADE_CONCLUIDA'));
                                    } else {
                                        growl.success(translate('message.success.MSG006_REGISTRO_GRAVADO_SUCESSO'));
                                    }
                                }

                                if (projectCloned === true || projectCloned === "true") {
                                    growl.info(translate('message.info.MSG67_VERSAO_PROJETO'));
                                }

                                if (activity.posicao < scope.model.length) {
                                    var nextActivity = scope.model[activity.posicao];
                                    if (activity.posicao == nextActivity.posicao) {
                                        scope.model[activity.posicao].posicao = activity.posicao + 1;
                                        convertAtividadeAndLocal(nextActivity);
                                        scope.saveActivity(nextActivity, false, false, true);
                                    }
                                }

                                if (saveNext === true) {
                                    fillDataInicioNextActivity(activity);
                                }

                                scope.$parent.getForm().$setPristine();

                                if (!noReload) {
                                    setTimeout(function() {
                                        $state.transitionTo($state.current, $stateParams, {
                                            reload: true,
                                            inherit: true,
                                            notify: true
                                        });
                                    }, 1000);
                                }
                            }, function() {
                                activity.concluido = false;
                                growl.error("Ocorreu um erro ao tentar gravar o registro!");
                            });

                        };

                        scope.preRemoveActivity = function(activity) {
                            var confirmDialog = dialogs.confirm(translate('DIALOGS_CONFIRMATION'), translate('message.confirm.MSG009_CONFIRMAR_EXCLUSAO'));
                            confirmDialog.result.then(function(btn) {
                                // Caso confirme executa a exclusão
                                scope.removeActivity(activity);
                            }, function(btn) {

                            });

                        }

                        scope.removeActivity = function(activity) {
                            convertAtividadeAndLocal(activity);

                            var removeInternal = function(activity) {
                                for (var i = 0; i < scope.model.length; i++) {
                                    if (scope.model[i].posicao === activity.posicao) {
                                        scope.model.splice(i, 1);
                                        //reseting activities position to save correctly after remove one
                                        while (i < scope.model.length) {
                                            scope.model[i].posicao = scope.model[i].posicao - 1;
                                            scope.saveActivity(scope.model[i], true);
                                            i++
                                        };
                                        break;
                                    }
                                };
                                scope.getCurrentLocation();
                                if (isGridTemplate()) {
                                    growl.info(translate('message.error.MSG38_SECAO_ATUALIZADA'));
                                } else {
                                    growl.success(translate('message.success.MSG010_EXCLUSÃO_REALIZADA'));
                                }
                            }

                            if (activity.id && scope.deleteServicePath) {
                                Restangular.all(scope.deleteServicePath).post(
                                    activity
                                ).then(function(result) {
                                    removeInternal(activity);
                                }, function() {
                                    growl.error("Ocorreu um erro ao tentar gravar o registro!");
                                });
                            } else {
                                removeInternal(activity);
                            }

                        }

                        function translate(translationKey) {
                            return $filter('translate')(translationKey);
                        }

                        var translateColumnsDisplayName = function() {
                            for (var i = 0; i < scope.columnDefs.length; i++) {
                                if (typeof scope.columnDefs[i].translationKey !== 'undefined' && scope.columnDefs[i].translationKey != null) {
                                    scope.columnDefs[i].displayName = translate(scope.columnDefs[i].translationKey);
                                }
                            };
                        }();

                        $rootScope.$on('$translateChangeEnd', function() {
                            translateColumnsDisplayName();
                        });

                        scope.downloadLink = function(path, fileName) {
                            return configuration.localPath + 'api/anexo/' + encodeURIComponent(fileName) + '?id=' + path;
                        };

                        scope.$watch('model.length', function() {
                            if (!scope.isEditing()) {
                                scope.getCurrentLocation();
                            }
                        }, true);
                    }
                }
            }
        }
    });
