'use strict'
angular.module('gpifrontApp')
    .controller('WeeklyPositiveScheduleCtrl', function($scope, ESTAGIO_PROJETO) {        
        $scope.dados = [];

    	// Define os filtros que serão utilizados na busca.
    	$scope.filterOptions = {
    		estagio: true,
    		anual: false,
    		regiaoPlanejamento: true,
    		gerencia: true,
    		cadeiaProdutiva: true,
    		prospeccao: false,
            intervaloDatas: true,
            mesAno: false,
            tipoInstrumentoFormalizacao: false                        
    	};

        $scope.$watch('dados', function(newValue, oldValue) {
            if ($scope.panelScope == undefined) {
                $scope.panelScope = {}
            }

            $scope.panelScope.estagios = [
                ESTAGIO_PROJETO.IMPLANTACAO_INICIADO,
                ESTAGIO_PROJETO.FORMALIZADA,
                ESTAGIO_PROJETO.OPERACAO_INICIADA
            ];
            
            if (oldValue !== newValue) {
                $scope.dataDF = $scope.dados[0];
                $scope.dataII = $scope.dados[1];
                $scope.dataOI = $scope.dados[2];                
            }
        })

        $scope.getRowColorByData = function(row) {
            var dataFim = new Date($scope.panelScope.filtro.dataFim);

            //considera-se apenas mês e ano do filtro. A data fim é considerada como o 1o dia do mês.
            dataFim.setDate(1);
            dataFim.setHours(0);
            dataFim.setMinutes(0);
            dataFim.setSeconds(0);
            dataFim.setMilliseconds(0);

            if (row.dataPrevisao > 0 && row.dataPrevisao < dataFim && !(row.dataReal > 0)) {
                return '#e66454';  // vermelho
            } else if (row.dataReal > 0) {
                return '#5ebd5e';  // verde
            }
            return '';
        }
        
    });
