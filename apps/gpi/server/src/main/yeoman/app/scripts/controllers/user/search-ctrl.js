'use strict'

angular
	.module('gpifrontApp')
	.controller('UserSearchCtrl', function($scope, Restangular, $stateParams, growl, $filter){

    	var baseDomain = "domain/";
		var baseUsuarios = Restangular.all('user');

		$scope.cargos = [];
		$scope.grupoUsuarios = [];
		$scope.departamentos = [];
		$scope.filtro = {ativo: true};

		Restangular.all(baseDomain + "departamento").getList().then(function(departamentos){
            $scope.departamentos = departamentos;
		});

		Restangular.all(baseDomain + "grupoUsuario").getList().then(function(gruposUsuarios){
            $scope.gruposUsuarios = gruposUsuarios;
		});

        Restangular.all(baseDomain + "cargo").getList().then(function(cargos){
            $scope.cargos = cargos;
        });		

        function translate(translationKey) {
            var text = "";
            if (typeof translationKey !== 'undefined' && translationKey) {
                text = $filter('translate')(translationKey);
            }
            return text;
        };

        $scope.usuariosEncontrados = [];
		$scope.search = function() {
			if ($scope.filtro) {
				if ($scope.filtro.matricula !== undefined && $scope.filtro.matricula.length === 0) {
					$scope.filtro.matricula = undefined;	
				}				
			}

			baseUsuarios.getList($scope.filtro).then(function(usuarios){
				$scope.usuariosEncontrados = usuarios;

                if (usuarios.length === 0) {
                    growl.error(translate('message.error.MSG004_NENHUM_REGISTRO_ENCONTRADO'));
                }				
			})
		}

        $scope.reset = function() {
            $scope.filtro = {ativo: true};
            $scope.search();
        }

		// Ao entrar na tela de consulta, o GPI deve recuperar os registros paginados em 10 (RNE002)
		$scope.search();

	});


