'use strict'

angular
    .module('gpifrontApp')
    .controller('PasswordCtrl', function($scope, Restangular, growl, $stateParams, $translate, $rootScope, $state, $filter, ScreenService) {

        $scope.hasErrors = false;
        $scope.validationHelper = {};
        $scope.credentials = {
            password: '',
            confirmPassword: ''
        };

        /**
         * Submit resetPassword request - Used to change the password that the user loses
         */
        var submitResetPassword = function(request) {
            Restangular.all('oauth/reset/password').post(request).then(function(result) {
                $state.go('login',{environment:'externo',message:'Nova senha cadastrada com sucesso.'});
            }, function errorCallback(response) {
                if (response.status > 400) {
                    $scope.hasErrors = true;
                    $scope.errorMessage = $filter('translate')(response.data.consumerMessage);
                } else {
                    $scope.hasErrors = true;
                    $scope.errorMessage = "Token expirado ou senha já cadastrada para este usuário!";
                }
            });
        }

        /**
         * Submit NewPasswordRequest - Used for verify new users e-mail, create the first password
         * and activate the user
         */
        var submitNewPasswordRequest = function(request) {
            Restangular.all('user/ativar').post(request).then(function(result) {
                $state.go('login',{environment:'externo',message:'Nova senha cadastrada com sucesso.'});
            }, function errorCallback(response) {
                if (response.status > 400) {
                    $scope.hasErrors = true;
                    $scope.errorMessage = $filter('translate')(response.data.consumerMessage);
                } else {
                    $scope.hasErrors = true;
                    $scope.errorMessage = "Token expirado ou senha já cadastrada para este usuário!";
                }
            });
        }

        /**
         * Verify mandatory parameters
         */
        var initialize = function() {
            if (!$stateParams.token || !$stateParams.type || !$stateParams.email) {
                $state.go('login',{environment:'externo'});
            }
        }

        /**
         * Indicates a valid the request - Checks confirmation of typed password and obligatory of parameters
         */
        var validate = function() {
            $scope.hasErrors = false;
            if ($scope.hasErrors = $scope.hasErrors || !$scope.credentials.password) {
                $scope.validationHelper.password = $filter('translate')('common.field.required');
            }

            if ($scope.hasErrors = $scope.hasErrors || !$scope.credentials.confirmPassword) {
                $scope.validationHelper.confirmPassword = $filter('translate')('common.field.required');
            }

            if (!$scope.hasErrors && !isPasswordConfirmationValid()) {
                $scope.validationHelper.confirmPassword = $filter('translate')('user.invalid.confirmPassword');
                $scope.hasErrors = true;
            }

            if (!$scope.hasErrors && $scope.credentials.password && ($scope.credentials.password.length < 8 || $scope.credentials.password.length > 40)) {
                $scope.validationHelper.password = $filter('translate')('message.error.MSG87_SENHA_INVALIDA');
                $scope.hasErrors = true;
            }

            return $scope.hasErrors;
        }

        var isPasswordConfirmationValid = function() {
            return $scope.credentials.password === $scope.credentials.confirmPassword;
        }

        /**
         * Submit the ChangePasswordRequest
         */
        $scope.submit = function(form) {
            $scope.cleanInputMessages();
            if (!validate()) {
                var request = {
                    password: $scope.credentials.password,
                    email: $stateParams.email,
                    token: $stateParams.token
                }

                if ($stateParams.type === 'new') {
                    submitNewPasswordRequest(request);
                } else {
                    submitResetPassword(request);
                }
            }
        }

        /**
         * Cleans message errors
         */
        $scope.cleanInputMessages = function() {
            $scope.hasErrors = false;
            $scope.validationHelper = {
                password: '',
                confirmPassword: ''
            };
        }

        initialize();

    });
