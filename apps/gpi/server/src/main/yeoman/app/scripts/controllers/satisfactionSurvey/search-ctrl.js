'use strict';

angular.module('gpifrontApp')
	.controller('SatisfactionSurveySearchCtrl', function($scope, ScreenService, TIPO_PERGUNTA, Restangular, $stateParams, $state, growl, $filter){
		$scope.pesquisaSatisfacaoList = [];

        var baseInstrumentoFormalizacao = Restangular.all("instrumento/pesquisa/empresausuario");

        baseInstrumentoFormalizacao.getList().then(function(pesquisas){
            if(pesquisas.length > 0) {
                $scope.pesquisaSatisfacaoList = pesquisas;
            } else {
            	growl.error($filter('translate')('message.error.MSG90_NENHUM_REGISTRO_ENCONTRADO'));
            }
        });

	});
