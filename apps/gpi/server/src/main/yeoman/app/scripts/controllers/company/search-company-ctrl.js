'use strict';

angular.module('gpifrontApp')
    .controller('SearchCompanyCtrl', function($scope, UserSession, TIPO_USUARIO, Restangular, $filter, $rootScope, GridButton, growl, SITUACAO_EMPRESA, MINAS_GERAIS_ID, AuthService, AUTH_ROLES) {

        $scope.companies = [];
        $scope.selectedItems = [];

        $scope.SITUACOES_EMPRESA = SITUACAO_EMPRESA.todas;

        $scope.cadeiasProdutivas = [];

        $scope.isExternalUser = UserSession.getUser().tipo == TIPO_USUARIO.EXTERNO;

        
        var initialize = function() {
            Restangular.all('domain/cadeiaProdutiva').getList().then(function(result) {
                $scope.cadeiasProdutivas = result;
            });

            if ($scope.isExternalUser) {
                Restangular.one('empresa/' + UserSession.getUser().empresaId).get().then(function(result) {
                    $scope.companies = [result];
                    if ($scope.companies.length === 0) {
                        growl.error(translate('message.error.MSG004_NENHUM_REGISTRO_ENCONTRADO'));
                    }
                });
            }

            if ($rootScope.filtersCompany !== undefined){
                $scope.filters = $rootScope.filtersCompany
                setTimeout(function(){
                    $scope.search();    
                }, true)                
            } else {
                $scope.filters = {
                    situacao: {
                        id: null
                    },
                    cadeiaProdutiva: {
                        id: null
                    },
                    empresa: null,
                    cnpj: null,
                    municipioOuCidade: null,
                    pais: null,
                    estado: null
                }
            }            
        }

        $scope.clean = function() {
            $scope.filters = {
                situacao: {
                    id: null
                },
                cadeiaProdutiva: {
                    id: null
                },
                empresa: null,
                cnpj: null,
                municipioOuCidade: null,
                pais: null,
                estado: null
            }

            $scope.companies = [];
        }

        function translate(translationKey) {
            var text = "";
            if (typeof translationKey !== 'undefined' && translationKey) {
                text = $filter('translate')(translationKey);
            }
            return text;
        };

        function validations() {
            if (($scope.filters.empresa === null || $scope.filters.empresa.trim() === '') && ($scope.filters.cadeiaProdutiva.id === null || $scope.filters.cadeiaProdutiva.id === '') && ($scope.filters.cnpj === null || $scope.filters.cnpj.trim() === '') && ($scope.filters.situacao.id === null || $scope.filters.situacao.id.trim() === '') && ($scope.filters.municipioOuCidade === null || $scope.filters.municipioOuCidade.trim() === '') && ($scope.filters.estado === null || $scope.filters.estado.trim() === '') && ($scope.filters.pais === null || $scope.filters.pais.trim() === '')) {
                growl.error(translate('message.error.MSG29_OBRIGATORIO_INFORMAR_PELO_MENOS_UM_CAMPO_NA_PESQUISA'));
                return false;
            } else {
                return true;
            }
        }
        var getFilters = function(pageSize, currentPage, countQuery) {
            return {
                "nomeEmpresa": $scope.filters.empresa,
                "cadeiaProdutiva": $scope.filters.cadeiaProdutiva.id,
                "situacaoEmpresa": $scope.filters.situacao.id,
                "cnpj": $scope.filters.cnpj,
                "municipioOuCidade": $scope.filters.municipioOuCidade,
                "estado": $scope.filters.estado,
                "pais": $scope.filters.pais,
                "pageSize": pageSize ? pageSize : null,
                "page": currentPage ? currentPage : null,
                "countResults": countQuery ? countQuery : null
            };
        }

        var getCounterFilters = function() {
            return getFilters(null, null, true);
        }

        $scope.setMaxPages = function() {
            Restangular.all('empresa').customGET('', getCounterFilters()).then(function(result) {
                $scope.gridScope.options.maxResults = parseInt(result);
                if ($scope.gridScope.options.maxResults === 0) {
                    growl.error(translate('message.error.MSG004_NENHUM_REGISTRO_ENCONTRADO'));
                }
            });
        }

        var getPagedCompany = function(oldPage, newPage, pageSize) {
            Restangular.all('empresa').getList(getFilters(pageSize, newPage)).then(function(result) {
                $scope.companies = _.map(result, function(r) {
                    var aux = r;
                    aux.situacao = _.find($scope.SITUACOES_EMPRESA,
                        function(sit) {
                            return sit.id == r.situacao;
                        }).descricao;
                    return aux;
                });
            });
        }


        $scope.gridScope = {
            pageOnChange: getPagedCompany,
            options: {
                isServerSide: true
            }
        };

        $scope.search = function() {
            $rootScope.filtersCompany = $scope.filters;
            if (validations()) {
                $scope.setMaxPages();
                getPagedCompany(0, 1, $scope.gridScope.pagingOptions.pageSize);
            }
        }


        $scope.rowStyle = function(row, childIndex) {
            var height = this.$parent.rowHeight;
            var top = 0;
            if (typeof childIndex === "undefined") {
                if (row.rowIndex > 0) {
                    var topRow = this.$parent.renderedRows[row.rowIndex - 1];
                    var topExpandedHeight = 0;
                    if (this.$parent.nodeExpanded(topRow.entity.id))
                        topExpandedHeight = topRow.entity.unidades.length;
                    top = row.offsetTop = ((topRow.offsetTop + height) + (topExpandedHeight * height));
                }
            } else {
                top = (height + (childIndex * height));
            }
            var ret = {
                "top": top + "px",
                "height": height + "px"
            };

            if (row.isAggRow) {
                ret.left = row.offsetLeft;
            }
            return ret;
        };

        $scope.columnDefs = [{
            field: '',
            translationKey: '',
            enableCellEdit: false,
            cellTemplate: "<div class=\"ngCellText\" ng-show=\"!unit\" ng-class=\"col.colIndex()\" ng-click=\"selectNodeHead(row.entity.id); $event.stopPropagation();\"><span ng-class=\"headClass(row.entity, row.entity.id, row.entity.unidades)\"></span></div>",
            width: 25
        }, {
            field: 'id',
            internalField: 'id',
            enableCellEdit: false,
            width: 50,
            translationKey: 'common.id'
        }, {
            field: 'nomePrincipal',
            internalField: 'nomePrincipal',
            enableCellEdit: false,
            width: "**",
            translationKey: 'company.companyUnit'
        }, {
            field: 'cnpjFormatado',
            enableCellEdit: false,
            width: 150,
            translationKey: 'common.cnpj'
        }, {
            field: 'endereco.enderecoFormatado',
            internalField: 'endereco.enderecoFormatado',
            enableCellEdit: false,
            width: "***",
            translationKey: 'common.address'
        }, {
            field: 'situacao',
            enableCellEdit: false,
            translationKey: 'common.situation'
        }, {
            field: 'show',
            translationKey: 'common.show',
            showScreenRedirect: true,
            showLink: '/empresa/cadastro/visualizar'
        }];

        // ng-class=\"{brownRow: row.entity.situacao == " + SITUACAO_EMPRESA.cadastroArquivado.id + ", yellowRow: row.entity.status == " + SITUACAO_EMPRESA.pendenteValidacao.id + "}\"
        $scope.CADASTRO_ARQUIVADO = SITUACAO_EMPRESA.cadastroArquivado.descricao;
        $scope.PEDENTE_VALIDACAO = SITUACAO_EMPRESA.pendenteValidacao.descricao;

        $scope.rowTemplate =
            "<div style=\"height: 100%\" ng-class=\"{brownRow: row.entity.situacao === '" + $scope.CADASTRO_ARQUIVADO + "', yellowRow: row.entity.situacao === '" + $scope.PEDENTE_VALIDACAO + "'}\">" +
            "<div ng-style=\"{ 'cursor': row.cursor }\" ng-repeat=\"col in renderedColumns\" ng-class=\"col.colIndex()\" class=\"ngCell {{col.cellClass}}\">\r" +
            "\n" +
            "\t<div class=\"ngVerticalBar\" ng-style=\"{height: rowHeight}\" ng-class=\"{ ngVerticalBarVisible: !$last }\">&nbsp;</div>\r" +
            "\n" +
            "\t<div ng-cell></div>\r" +
            "\n" +
            "</div>" +
            "<div ng-style=\"rowStyle(row, $index, rowHeight, renderedRows)\" class=\"ngRow\" ng-repeat=\"unit in row.entity.unidades\" ng-if=\"nodeExpanded(row.entity.id)\">\r" +
            "<div ng-style=\"{ 'cursor': row.cursor }\" ng-repeat=\"col in renderedColumns\" ng-class=\"col.colIndex()\" class=\"ngCell {{col.cellClass}}\">\r" +
            "\n" +
            "\t<div class=\"ngVerticalBar\" ng-style=\"{height: rowHeight}\" ng-class=\"{ ngVerticalBarVisible: !$last }\">&nbsp;</div>\r" +
            "\n" +
            "\t<div class=\"ngCellText\" ng-class=\"col.colIndex()\"><span ng-cell-text>{{getChildProperty(unit, columnDefs[col.index - 1].internalField)}}</span></div>\r" +
            "\n" +
            "</div>" +
            "</div>" +
            "</div>"

        function isEditable() {
            return $scope.selectedItems[0] && $scope.selectedItems[0].situacao == SITUACAO_EMPRESA.ativa.id;
        }

        function disableEditFn() {
            return !AuthService.isUserAuthorized([AUTH_ROLES.portfolioAnalist, AUTH_ROLES.promotionAnalist, AUTH_ROLES.manager, AUTH_ROLES.generalSecretary, AUTH_ROLES.externalResponsible, AUTH_ROLES.externalOther]);
        }

        function disableNewFn() {
            return !AuthService.isUserAuthorized([AUTH_ROLES.portfolioAnalist, AUTH_ROLES.promotionAnalist, AUTH_ROLES.manager, AUTH_ROLES.generalSecretary]);
        }

        function disableDeleteFn() {
            return !AuthService.isUserAuthorized(AUTH_ROLES.portfolioAnalist);
        }

        function preDelete() {
            var empresasId = [];

            for (var i = 0; i < $scope.selectedItems.length; i++) {
                empresasId.push($scope.selectedItems[i].id);
            };

            Restangular.all('projeto/empresa').getList({
                "id": empresasId
            }).then(function(result) {
                var projetosTO = result;

                if (projetosTO.length > 0) {
                    var empresasWithProjetosId = [];
                    for (var i = 0; i < projetosTO.length; i++) {
                        empresasWithProjetosId.push(projetosTO[i].idEmpresa);
                    };
                    growl.error(translate('message.error.MSG23_EXCLUSAO_EMPRESA'));
                    return false;
                } else {
                    deleteFn();
                    $scope.gridScope.deleteFn(true);
                }
            });

            return false;

        }

        function deleteFn() {
            $scope.deletedItems = [];
            for (var i = 0; i < $scope.selectedItems.length; i++) {
                $scope.deletedItems.push($scope.selectedItems[i].id);
            };

            Restangular.all('empresa/removeEmpresa').post(
                $scope.deletedItems
            ).then(function(result) {
                $scope.deletedItems = [];
                growl.success(translate('message.success.MSG010_EXCLUSÃO_REALIZADA'))
            }, function() {
                growl.error("Ocorreu um erro ao tentar gravar o registro!");
            });
        }

        $scope.buttonsConfig = {
            newButton: new GridButton(!$scope.isExternalUser, 'common.new', true, disableNewFn, null, '/empresa/cadastro/incluir'),
            editButton: new GridButton(true, 'common.edit', true, disableEditFn, null, '/empresa/cadastro/editar'),
            deleteButton: new GridButton(!$scope.isExternalUser, 'common.delete', false, disableDeleteFn, preDelete, '')
        }

        $scope.sortInfo = {
            fields: ['nomePrincipal'],
            directions: ['asc']
        };

        initialize();
        
    });
