'use strict'

angular.module('gpifrontApp')
    .controller('IntialContactCtrl', function($scope) {

        // Define os filtros que serão utilizados na busca.
        $scope.filterOptions = {
            estagio: false,
            anual: false,
            regiaoPlanejamento: false,
            gerencia: false,
            cadeiaProdutiva: false,
            prospeccao: false,
            intervaloDatas: false,
            mesAno: true
        };

        /*Início configs grid*/
        $scope.dataColumns = [{
            field: 'empresa',
            visible: true,
            title: 'common.company'
        }, {
            field: 'projeto',
            visible: true,
            title: 'common.project'
        }, {
            field: 'cidade',
            visible: true,
            title: 'address.city',
        }, {
            field: 'regiaoPlanejamento',
            visible: true,
            title: 'location.planing.region'
        }, {
            field: 'investimentoPrevisto',
            visible: true,
            title: 'reunionPanelReport.invest',
            format: 'currency'
        }, {
            field: 'faturamentoInicial',
            visible: true,
            title: 'reunionPanelReport.initial.billing',
            format: 'currency'
        }, {
            field: 'faturamentoPleno',
            visible: true,
            title: 'reunionPanelReport.full.billing',
            format: 'currency'
        }, {
            field: 'empregosDiretos',
            visible: true,
            title: 'reunionPanelReport.direct.jobs'
        }, {
            field: 'dataEstagioAtual',
            visible: true,
            title: 'reunionPanelReport.initial.contact.date',
            format: 'date'
        }, {
            field: 'analista',
            visible: true,
            title: 'reunionPanelReport.analyst'
        }, {
            field: 'gerencia',
            visible: true,
            title: 'reunionPanelReport.management'
        }];
        /*Fim configs grid*/

	});

