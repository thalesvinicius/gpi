'use strict';
angular.module('gpifrontApp')
    .controller('ProjectCtrl', function($scope, UserSession, TIPO_USUARIO, ScreenService, $state, AUTH_ROLES, AuthService, Restangular) {
        $scope.screenService = ScreenService;
        $scope.isExternalUser = UserSession.getUser().tipo == TIPO_USUARIO.EXTERNO;
        $scope.screenType = {
            INCLUIR: {
                titleTranslationKey: 'project.add',
                icon: "fa-plus",
                disabled: false
            },
            EDITAR: {
                titleTranslationKey: 'project.edit',
                icon: "fa-edit",
                disabled: false
            },
            VISUALIZAR: {
                titleTranslationKey: 'project.visualize',
                icon: "fa-eye",
                disabled: true
            }
        };

        $scope.getId = function() {
            return $state.params.id;
        };

        /*
        *   Inserido para SM onde o analista de infraestrutura e meio ambiente
        *   somente tem acesso de edição às abas 'infraestrutura' e 'meio ambiente', nas outras ele somente
        *   poderá visualizar
        */
        $scope.isAnalistaInfra = function(){
           return AuthService.isUserAuthorized(AUTH_ROLES.infraAnalist);
        }

        /*
         *   Verifica se o usuário logado é gerente
         */
        var isGerente = function() {
            return AuthService.isUserAuthorized([AUTH_ROLES.manager]);
        }

        $scope.canEditProject = function() {
            var canEdit = true;
            if (ScreenService.isEditScreen()) {
                canEdit = ($scope.projetoTO.situacaoEmpresa === SITUACAO_EMPRESA["ativa"].id // Só pode editar um projeto em que situação da Empresa ou da Empresa vinculada à Unidade for "Ativo"
                    && (($scope.isExternalUser && $scope.projetoTO.estagioDescricao === ESTAGIO_PROJETO["INICIO_PROJETO"].descricao) // Usuario externo só pode editar projeto em estagio inicio de projeto
                        || (isGerente() && isResponsavelDepartamento()) // Caso o usuário logado for um gerente ele poderá editar um projeto se ele for o responsável pelo departamento
                        || (usuarioPossuiPermissao() && isResponsavelProjeto()) || podeEditarQualquerProjeto())); // Se o usuário for responsável pelo projeto ou possuir permissão para editar qualquer projeto
            }
            return canEdit;
        }

        /*
         *   Verifica se o usuário logado é responsável pelo departamento
         */
        var isResponsavelDepartamento = function() {
            return $scope.projetoTO.departamentoUsuarioInternoResponsavelId === UserSession.getUser().departamento.id;
        }

        $scope.checkUserAuthorization = function() {
            $scope.projetoTO = {}
            var projetoId = $scope.getId();

            if (projetoId != undefined && $scope.projeto && $scope.projeto.id === projetoId) {
                AuthService.verifyUserIsAuthenticatedInsideController($scope.isUserAuthorized); 
            } else if (!$scope.screenService.isNewScreen() && projetoId !== undefined) {
                Restangular.one('projeto', projetoId).one("projetoTO").get().then(function(result) {
                    $scope.projetoTO = result;
                    AuthService.verifyUserIsAuthenticatedInsideController($scope.isUserAuthorized); 
                });                
            }
        } 

        $scope.isExternalUserResponsible = function() {
            return $scope.isExternalUser && $scope.projetoTO.usuarioExternoResponsavelId === UserSession.getUser().id;
        }

        $scope.isUserAuthorized = function() {
            return $scope.isExternalUserResponsible() || !$scope.isExternalUser;
        }        

    });