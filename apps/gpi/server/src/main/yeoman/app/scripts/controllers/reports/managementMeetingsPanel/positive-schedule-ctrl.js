'use strict'
angular.module('gpifrontApp')
    .controller('ManagementPositiveScheduleCtrl', function($scope, ESTAGIO_PROJETO) {        
        $scope.dados = [];

    	// Define os filtros que serão utilizados na busca.
    	$scope.filterOptions = {
    		estagio: true,
    		anual: false,
    		regiaoPlanejamento: true,
    		gerencia: true,
    		cadeiaProdutiva: true,
    		prospeccao: false,
            intervaloDatas: false,
            mesAno: true,
            tipoInstrumentoFormalizacao: false                        
    	};

        $scope.$watch('dados', function(newValue, oldValue) {
            if ($scope.panelScope == undefined) {
                $scope.panelScope = {}
            }

            $scope.panelScope.estagios = [
                ESTAGIO_PROJETO.IMPLANTACAO_INICIADO,
                ESTAGIO_PROJETO.FORMALIZADA,
                ESTAGIO_PROJETO.OPERACAO_INICIADA
            ];
            
            if (oldValue !== newValue) {
                $scope.dataDF = $scope.dados[0];
                $scope.dataII = $scope.dados[1];
                $scope.dataOI = $scope.dados[2];                
            }
        })

        $scope.getRowColorByData = function(row) {
            var inicioDoMesReferencia = new Date($scope.panelScope.filtro.mesAno);
            inicioDoMesReferencia.setDate(1);
            inicioDoMesReferencia.setHours(0);
            inicioDoMesReferencia.setMinutes(0);
            inicioDoMesReferencia.setSeconds(0);
            inicioDoMesReferencia.setMilliseconds(0);

            if (row.dataPrevisao > 0 && row.dataPrevisao < inicioDoMesReferencia && !(row.dataReal > 0)) {
                return '#e66454';  // vermelho
            } else if (row.dataReal > 0) {
                return '#5ebd5e';  // verde
            }
            return '';
        }
        
    });
