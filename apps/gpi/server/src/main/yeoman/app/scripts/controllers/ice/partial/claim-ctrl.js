'use strict';
angular.module('gpifrontApp')
    .controller('IceClaimCtrl', function($scope, TIPO_USUARIO, $stateParams, Restangular, growl, $state, $filter, ScreenService, dialogs, SITUACAO_PROJETO, ESTAGIO_PROJETO,
        $http, $window, configuration,$rootScope, ngTableParams, UserSession, AuthService, CHANGE_PLEITO, AUTH_ROLES) {
        //--------------------------------------------------------------------- Início Configurações inicial da tela

        var baseProjetos = Restangular.all('projeto');

        $scope.screenService = ScreenService;

        $scope.projetoTO = {};

        $scope.isExternalUser = UserSession.getUser().tipo == TIPO_USUARIO.EXTERNO;

        $scope.changes = [];

        $scope.change_pleito = CHANGE_PLEITO;

        $scope.externalUser = function() {
            return AuthService.isUserAuthorized([AUTH_ROLES.externalResponsible, AUTH_ROLES.externalMeling, AUTH_ROLES.externalOther]);
        }

        $scope.initialize = function() {

            Restangular.one('projeto', $stateParams.id).one("projetoTO").get().then(function(result) {
                $scope.projetoTO = result;
                $scope.isPendenteValidacao = SITUACAO_PROJETO["PEDENTE_VALIDACAO"].id === $scope.projetoTO.situacao;
                $scope.isAceite = SITUACAO_PROJETO["PENDENTE_ACEITE"].id === $scope.projetoTO.situacao;

                if (!$scope.externalUser() && $scope.projetoTO.situacao === SITUACAO_PROJETO.PENDENTE_ACEITE.id) {
                    baseProjetos.one($stateParams.id).getList("pleito/findChange").then(function(result) {
                        $scope.changes = (result[0] === undefined ? 0 : result[0]);
                    }, function() {
                        growl.error("Ocorreu um erro ao tentar buscar o registro!");
                    });
                }                
            });

            if ($stateParams.screen != null) {
                if ($stateParams.screen != $scope.type.INCLUIR && $stateParams.id) {
                    baseProjetos.one($stateParams.id).one("pleito").getList().then(function(pleitos) {
                        if (pleitos === undefined || pleitos.length == 0) {
                            $scope.pleito = {
                                usuarioResponsavel: {
                                    nome: ''
                                },
                                dataUltimaAlteracao: ''
                            };
                        } else {
                            $scope.pleito = pleitos[0];
                        }
                    });
                }
            }

            $scope.loggedUser = UserSession.getUser();

        };
        //--------------------------------------------------------------------- Fim Configurações inicial da tela


        $scope.save = function() {
            baseProjetos.one($stateParams.id).post("pleito", $scope.pleito).then(function() {
                successAction();
            }, function() {
                growl.error("Ocorreu um erro ao tentar gravar o registro!");
            });
        }

        $scope.printICEfn = function() {

            var ids = [$stateParams.id];

            $http({
                method: 'GET',
                url: configuration.localPath + 'api/projeto/report',
                params: {
                    'id': ids
                },
                headers: {
                    Accept: 'application/pdf',
                    Authorization: 'Bearer ' + $scope.loggedUser.accessToken
                },
                responseType: 'arraybuffer'
            }).
            success(function(response) {
                ScreenService.browserAwareSave(response,"ICE.pdf","application/pdf");
            }).
            error(function(data, status, headers, config) {
                growl.error("Não foi possível realizar a impressão do ICE indicado. Tente novamente e se o erro persistir favor contatar o administrador do sistema.")
            });
        };

        function translate(translationKey) {
            var text = "";
            if (typeof translationKey !== 'undefined' && translationKey) {
                text = $filter('translate')(translationKey);
            }
            return text;
        };

        $scope.acceptICE = function() {
            Restangular.all("projeto/" + $stateParams.id + "/pleito/accept").post().then(function() {
                successAction();
            })
        }

        $scope.disabledICEStageIP = function() {
            if (ESTAGIO_PROJETO["INICIO_PROJETO"].id === $scope.projetoTO.estagio) {
                return false;
            } else {
                return true;
            }
        }

        $scope.validateICE = function() {
            //TODO validation
            Restangular.one("projeto", $stateParams.id).post('pleito/validate').then(function(serverProject) {
                if (serverProject && serverProject.length > 0) {
                    growl.error(serverProject.join('<br/>*'), {
                        ttl: -1
                    });
                } else {
                    if (ESTAGIO_PROJETO["INICIO_PROJETO"].id === $scope.projetoTO.estagio) {
                        growl.success(translate("message.info.MSG63_MUDANCA_ESTAGIO").replace("<estagio>", ESTAGIO_PROJETO["PROJETO_PROMISSOR"].id));
                    }
                    successAction();
                }
            });
        }

        var successAction = function() {
            // Utilizado para permitir que o usuário mude de página
            // sem a mensagem de perca de dados
            growl.success(translate("message.success.MSG006_REGISTRO_GRAVADO_SUCESSO"));
            $scope.formClaim.$setPristine();
            setTimeout(function() {
                $state.transitionTo($state.current, $stateParams, {
                    reload: true,
                    inherit: true,
                    notify: true
                });
            }, 2000);
        }

        var changeProjectSituation = function(situacaoId) {
            Restangular.all("projeto/" + $stateParams.id + "/updateSituacao").post({
                situacao: situacaoId
            }).then(function() {
                successAction();
            })
        }
    }).constant('CHANGE_PLEITO', {
        PLEITOS: {
            posicao: 0,
            descricao: "Pleitos"
        },
        OBSERVACOES: {
            posicao: 1,
            descricao: "Observacoes"
        }
    });
