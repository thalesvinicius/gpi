'use strict'

angular
    .module('gpifrontApp')
        .controller('LocationCtrl', function($scope, Restangular, AuthService, AUTH_ROLES, CHANGE_LOCALIZACAO, $stateParams, growl, ScreenService, $state, $window, $filter, CADEIA_PRODUTIVA, dialogs, SITUACAO_PROJETO) {
        $scope.screenService = ScreenService;

        var baseProjetos = Restangular.all('projeto');

        $scope.selectedItem = {};
        $scope.projeto = {cadeiaProdutiva: -1};
        $scope.localizacao = {};
        $scope.autoCompleteList = [];
        $scope.anexosAreaAbrangencia = [];
        $scope.anexosAreaLocalAbrangencia = [];
        $scope.localizacoesEmpreendimento = [];
        $scope.disableFooter = true;
        $scope.containsAreaAbrangencia = false;
        $scope.containsLOcalizacaoIndustrial = false;
        $scope.changes = [];
        $scope.change_localizacao = CHANGE_LOCALIZACAO;

        var putLocalizacoesEmpreendimento = function() {
            if (!$scope.localizacao.aDefinir) {
                if ($scope.localizacoesEmpreendimento.length > 0) {
                    angular.forEach($scope.localizacoesEmpreendimento, function(locEmp) {
                        if (locEmp.municipio != null) {
                            $scope.localizacao.municipio = locEmp.municipio;
                        } else if (locEmp.microRegiao != null) {
                            $scope.localizacao.microRegiao = locEmp.microRegiao;
                        } else if (locEmp.regiaoPlanejamento != null) {
                            $scope.localizacao.regiaoPlanejamento = locEmp.regiaoPlanejamento;
                        }
                    });
                } else {
                    $scope.localizacao.municipio = null;
                    $scope.localizacao.microRegiao = null;
                    $scope.localizacao.regiaoPlanejamento = null;
                }
            }
        }

        var setAnexosAbrangencia = function(anexosList, isAnexoAreaLocal) {

            angular.forEach(anexosList, function(anexo) {

                var anexoLocalizacao = new Object();                
                anexoLocalizacao.anexoAreaLocal = isAnexoAreaLocal;
                anexoLocalizacao.id = anexo.idPai;
                anexoLocalizacao.anexo = { id: anexo.id, nome: anexo.nome, path: anexo.path, extencao: anexo.extencao, dataAnexo: anexo.dataAnexo }
                anexoLocalizacao.tipoLocalizacao = anexo.tipoLocalizacao ;

                if (anexoLocalizacao.tipoLocalizacao == 1) {
                    $scope.containsAreaAbrangencia = true;
                };

                if (anexoLocalizacao.tipoLocalizacao == 2) {
                    $scope.containsLOcalizacaoIndustrial = true;
                };

                if (!isAnexoAreaLocal) {
                    anexoLocalizacao.tipoLocalizacao = anexo.tipoLocalizacao;
                }

                $scope.localizacao.anexosLocalizacao.push(anexoLocalizacao);

            });
        }

        $scope.styleBackGroundRed = function() {
            return false;
        }

        $scope.initialize = function() {

            $scope.cadeiaProdutiva = CADEIA_PRODUTIVA;
            $scope.localizacao.anexosLocalizacao = [];

            if ($stateParams.screen != null || $stateParams.screen != $scope.type.INCLUIR && $stateParams.id) {
                baseProjetos.one($stateParams.id).get().then(function(result) {
                    setTimeout(function() {
                        $scope.projeto = result;
                        
                        if (!$scope.externalUser() && $scope.projeto.situacaoAtual.situacao === SITUACAO_PROJETO.PENDENTE_ACEITE.id) {
                            baseProjetos.one($stateParams.id).getList("localizacao/findChange").then(function(result) {
                                $scope.changes = (result[0] === undefined ? 0 : result[0]);
                            }, function() {
                                growl.error("Ocorreu um erro ao tentar buscar o registro!");
                            });
                        }

                        baseProjetos.one($stateParams.id).one("localizacao").getList().then(function(localizacoes){
                            if (localizacoes[0] != undefined) {
                                $scope.localizacao = localizacoes[0];
                                $scope.localizacoesEmpreendimento = [localizacoes[0]];
                                if (localizacoes[0].anexosLocalizacao) {
                                    angular.forEach(localizacoes[0].anexosLocalizacao, function (anexoLocalizacao) {
                                        anexoLocalizacao.anexo.idPai = anexoLocalizacao.id;
                                        if (anexoLocalizacao.anexoAreaLocal == false) {
                                            anexoLocalizacao.anexo.tipoLocalizacao = anexoLocalizacao.tipoLocalizacao;
                                            $scope.anexosAreaAbrangencia.push(anexoLocalizacao.anexo);
                                        } else {
                                            $scope.anexosAreaLocalAbrangencia.push(anexoLocalizacao.anexo);
                                        }
                                    });
                                }
                            }
                        });
                    }, true);
                });
            }
        };

        $scope.externalUser = function() {
            return AuthService.isUserAuthorized([AUTH_ROLES.externalResponsible, AUTH_ROLES.externalMeling, AUTH_ROLES.externalOther]);
        }

        var cacheBusca = [];

        $scope.updateLocationRegions = function(typed) {

            var inCache = $.map(cacheBusca, function(value, key) {
                if (key.indexOf(typed) !== -1) {
                    return value;
                }
            })

            if (inCache.length == 0) {                                
                cacheBusca[typed] = baseProjetos.one($stateParams.id).one("localizacao").one("findLocalizacoes", typed).getList();
            } 

            return cacheBusca[typed];
        }

        $scope.includeEnterpriseLocation = function() {
            $scope.localizacoesEmpreendimento = [$scope.$item];
        }

        $scope.locationSelected = function($item, $model, $label) {
            $scope.$item = $item;
            $scope.$model = $model;
            $scope.$label = $label;
        }

        $scope.save = function(formLocation) {
            if (!$scope.localizacao) {
                growl.error("Ocorreu um erro ao tentar gravar o registro!");
            }

            $scope.localizacao.anexosLocalizacao = [];

            if ($stateParams.screen != $scope.type.INCLUIR && $scope.projeto) {

                putLocalizacoesEmpreendimento();

                if($scope.localizacao.aDefinir == null) {
                    $scope.localizacao.aDefinir = false;
                }

                setAnexosAbrangencia($scope.anexosAreaAbrangencia, false);
                setAnexosAbrangencia($scope.anexosAreaLocalAbrangencia, true);

                // if ($scope.isCadeiaProdutivaSucroEnergetico() &&
                //     (!$scope.containsLOcalizacaoIndustrial || !$scope.containsAreaAbrangencia)) {
                //     growl.error("Obrigatório inserir Àrea de abrangência do empreendimento " +
                //         "(Decreto 45.041/2009) e Localização da Planta Industrial.");
                //     return;
                // };

                baseProjetos.one($stateParams.id).post("localizacao", $scope.localizacao).then(function() {
                    growl.success("Registro gravado com sucesso!");
                    formLocation.$setPristine();
                    $state.go("base.project.ice.financialTpl", {
                            screen: $scope.screenService.getScreenName(),
                            id:  $stateParams.id
                        });
                }, function() {
                    growl.error("Ocorreu um erro ao tentar gravar o registro!");
                });

            } else {
                growl.error("Modo de tela invalido!");
            }

        }

        $scope.isCadeiaProdutivaEnergiasHidricas = function() {
            return $scope.projeto.cadeiaProdutiva.id === $scope.cadeiaProdutiva['ENERGIAS_HIDRICAS'].id;
        }

        $scope.isCadeiaProdutivaSucroEnergetico = function() {
            return $scope.projeto.cadeiaProdutiva.id === $scope.cadeiaProdutiva['SUCROENERGETICO'].id;
        }

        $scope.initialize();
    }).constant('CHANGE_LOCALIZACAO', {
        LATITUDE: {
            posicao: 0,
            descricao: "Latitude"
        },
        LONGITUDE: {
            posicao: 1,
            descricao: "Longitude"
        },
        ANEXOS: {
            posicao: 2,
            descricao: "Anexos"
        },
        ADEFINIR:{
            posicao: 3,
            descricao: "A Definir"
        },
        LOCALIZACAOSECUNDARIA: {
            posicao: 4,
            descricao: "Localização Secundária"
        },
        MICROREGIAO: {
            posicao: 5,
            descricao: "Micro Região"
        }
    });