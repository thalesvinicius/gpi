'use strict';
angular.module('gpifrontApp')
    .controller('MonitorFormInstCtrl', function($scope, growl, dialogs, $state, $filter, $stateParams, ScreenService, TIPO_INSTRUMENTO_FORMALIZACAO, Restangular, SITUACAO_INSTRUMENTO_FORMALIZACAO) {

        // Usado para validar o form
        $scope.formSubmitted = false;
        $scope.screenService = ScreenService;
        /*
         *   Valida o campo passado
         */
        $scope.checkValidation = function(formField) {
            return formField.$invalid && (formField.$dirty || $scope.formSubmitted);
        }

        $scope.columnDefs = [{
            field: 'localAtividade',
            enableCellEdit: false,
            width: "*",
            translationKey: 'common.local',
            cellTemplate: "<div ng-show=\"!isEditing(row.entity) || row.entity.obrigatorio\" class=\"ngCellText\" ng-class=\"col.colIndex()\"><span ng-cell-text title=\"{{row.entity.localAtividade.descricao}}\">{{row.entity.localAtividade.descricao}}</span></div>" +
                '<select ng-show=\"isEditing(row.entity) && !row.entity.obrigatorio\" ng-change=\"onChangeLocal(row.entity.localAtividade, row.entity.atividadeLocal)\" ng-class="\'colt\' + col.index\" ng-input=\"COL_FIELD\" ng-model=\"row.entity.localAtividade\">' +
                '<option title=\"{{local.descricao}}\" ng-value=\"local.id\" ng-repeat=\"local in locais\" ng-selected=\"row.entity.localAtividade.id === local.id\">{{local.descricao}}</option>' +
                '</select>'

        }, {
            field: 'atividadeLocal',
            enableCellEdit: false,
            width: "***",
            translationKey: 'common.activity',
            cellTemplate: "<div ng-show=\"!isEditing(row.entity) || row.entity.obrigatorio\" class=\"ngCellText\" ng-class=\"col.colIndex()\"><span ng-cell-text title=\"{{row.entity.atividadeLocal.descricao}}\">{{row.entity.atividadeLocal.descricao}}</span></div>" +
                '<select ng-show=\"isEditing(row.entity) && !row.entity.obrigatorio\" ng-class="\'colt\' + col.index\" ng-input=\"COL_FIELD\" ng-model=\"row.entity.atividadeLocal\">' +
                '<option title=\"{{atividade.descricao}}\" ng-value=\"atividade.id\" ng-repeat=\"atividade in getAtividades(row.entity.localAtividade)\" ng-selected=\"row.entity.atividadeLocal.id === atividade.id\">{{atividade.descricao}}</option>' +
                '</select>'
        }, {
            field: 'dataInicio',
            enableCellEdit: false,
            width: '*',
            translationKey: 'common.start',
            cellTemplate: "<div ng-show=\"!isEditing(row.entity) || (isEditing(row.entity) && (!row.entity.paralela && !row.entity.ativo)) \" class=\"ngCellText\" ng-class=\"col.colIndex()\"><span ng-cell-text>{{row.entity.dataInicio | dateTransformFilter}}</span></div>" +
                "<datepicker-custom ng-show=\"isEditing(row.entity) && (row.entity.paralela || row.entity.ativo)\" id=\"datepicker\" date=\"COL_FIELD\" configs=\"configsDatepicker\"></datepicker-custom>"
        }, {
            field: 'dataConclusao',
            enableCellEdit: false,
            width: '*',
            translationKey: 'common.end',
            cellTemplate: "<div ng-show=\"!isEditing(row.entity)\" class=\"ngCellText\" ng-class=\"col.colIndex()\"><span ng-cell-text>{{row.entity.dataConclusao | dateTransformFilter}}</span></div>" +
                "<datepicker-custom ng-show=\"isEditing(row.entity) && row.entity.dataInicio > 0\" id=\"datepicker\" date=\"COL_FIELD\" configs=\"configsDatepicker\"></datepicker-custom>"
        }];

        $scope.columnDefsOnEnd = [{
            field: 'paralela',
            enableCellEdit: false,
            width: '*',
            translationKey: 'common.paralel',
            cellTemplate: "<div class=\"ngCellText\" ng-class=\"col.colIndex()\"><span ng-cell-text>{{COL_FIELD === true ? \"Paralela\" : \"\"}}</span></div>"
        }];

        $scope.selectedItems = [];

        function translate(translationKey) {
            var text = "";
            if (typeof translationKey !== 'undefined' && translationKey) {
                text = $filter('translate')(translationKey);
            }
            return text;
        };

        var initialize = function() {

            // Configurações do datepicker
            $scope.configsDatepickerPublication = {
                required: true,
                disabled: $scope.screenService.isShowScreen()
            };

            Restangular.one("instrumento/findTOById/" + $stateParams.id).get().then(function(instrumento) {
                $scope.instrumento = instrumento;
                $scope.disableGrid = function() {
                    return ScreenService.isShowScreen() || ($scope.instrumento.situacao !== SITUACAO_INSTRUMENTO_FORMALIZACAO.ASSINADO.descricao && $scope.instrumento.situacao !== SITUACAO_INSTRUMENTO_FORMALIZACAO.EM_NEGOCIACAO.descricao);
                }
                $scope.gridScope.disableActions = $scope.disableGrid;

            });
            $scope.atividades = [];

            Restangular.all("instrumento/" + $stateParams.id + "/atividadeInstrumento").getList().then(function(atividades) {
                if (!atividades || _.isEmpty(atividades)) {
                    var confirmCopyTemplate = dialogs.confirm($filter('translate')('DIALOGS_CONFIRMATION'),
                        "As atividades serão carregadas de acordo com o template ativo para Instrumento de Formalização se houverem. Deseja continuar?");
                    confirmCopyTemplate.result.then(function(btn) {
                        Restangular.all("instrumento/" + $stateParams.id + "/atividadeInstrumento/copyFromTemplate").post().then(function(result) {
                            $scope.atividades = result;
                        });
                    }, function(btn) {
                        $state.go("base.formalizationInstrument.formalizationInstrumentTpl", $stateParams);
                    });
                } else {
                    $scope.atividades = atividades;
                }

            });

            $scope.locais = Restangular.all("local").getList().$object;
            $scope.atividadesLocais = [];

            $scope.getAtividades = function(local) {
                if (local && local.atividades) {
                    return local.atividades;
                }
                return $scope.atividadesLocais;
            }

            $scope.onChangeLocal = function(local, atividadeLocal) {

                for (var i = 0; i < $scope.locais.length; i++) {
                    if ($scope.locais[i].id === parseInt(local)) {
                        local = $scope.locais[i];
                        $scope.atividadesLocais = local.atividades;
                        break;
                    }
                };
            }

            $scope.gridScope = {
                locais: $scope.locais,
                atividadesLocais: $scope.atividadesLocais,
                onChangeLocal: $scope.onChangeLocal,
                getAtividades: $scope.getAtividades,
                currentLocation: {}
            }

            $scope.deleteServicePath = "instrumento/" + $stateParams.id + "/atividadeInstrumento/removeOne";
            $scope.saveServicePath = "instrumento/" + $stateParams.id + "/atividadeInstrumento";

            $scope.getForm = function() {
                return $scope.formMonitor;
            }
        };

        // Valida e carrega as configurações iniciais da tela
        initialize();
    });
