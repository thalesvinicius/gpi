'use strict'

angular
	.module('gpifrontApp')
	.controller('UnitsGridCtrl', function($scope, GridButton, $stateParams) {

		$scope.unitGridFunctions = $scope.$parent.unitGridFunctions;

        //Define its column-defs attribute
        $scope.unidadesColumnDefs = [{
            field: 'nome',
            enableCellEdit: false,
            width: "**",
            translationKey: 'unit.name',
        }, {
        	field: 'endereco.enderecoFormatado',
            enableCellEdit: false,
            width: "**",
            translationKey: 'common.address'
        }, {
            field: 'id',
            translationKey: 'common.show',
            enableCellEdit: false,
            cellTemplate:  '<div class="text-center" ng-class="col.colIndex()">' +
            		'<a ng-click="showUnit(row.entity.id)"><i class="fa fa-eye page-header-icon"></i></a> </div>'
        }];

        //define its buttons-config attribute on a search screen
        $scope.unidadesButtonsConfig = {            
            newButton: new GridButton(false, 'common.new', false, null, null, ''),
            deleteButton: new GridButton(false, 'common.delete', false, null, $scope.$parent.preDelete, '')
        }

        //define its sort-info attribute if you desire a default-sort
        $scope.unidadesSortInfo = {
            fields: ['id'],
            directions: ['asc']
        };

        // function deleteFn () {                        

        //     if ($scope.unidadesSelectedItems[0].ativo == true) {
        //         growl.error("Não é permitido excluir registros com o Status igual a 'Ativo'.");                
        //     } 
        //     else {
        //         var ask = window.confirm("Deseja excluir este(s) registro(s)?")                

        //         var ids = [];
        //         angular.forEach($scope.unidadesSelectedItems, function(usuario){
        //             ids.push(usuario.id)
        //         })
                
        //         if (ask && ids.length > 0) {
        //             Restangular.all('empresa').one($stateParams.id).post(ids).then(function(){                
        //                 growl.success('Exclusão efetuada com sucesso.')
        //             }, function(){
        //                 growl.error('Ocorreu um erro ao tentar efetuar a exclusão.')
        //             })               
        //             return true;
        //         } 
        //     }

        //     return false;
        // }

	});