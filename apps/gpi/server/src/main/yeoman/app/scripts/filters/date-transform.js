angular.module('gpifrontApp')
    .filter('dateTransformFilter', function($filter) {
        return function(input, format) {


            if (input) {
                if (typeof input == "string") {
                    var array = input.split("-");
                    var convertedDate = new Date(parseInt(array[0]), parseInt(array[1])-1, parseInt(array[2]) );
                    return $filter('date')(convertedDate, format ? format : 'dd/MM/yyyy');
                } else if (_.isArray(input)) {
                    var convertedDate = new Date(parseInt(input[0]), parseInt(input[1])-1, parseInt(input[2]) );
                    return $filter('date')(convertedDate, format ? format : 'dd/MM/yyyy');
                } else {
                    var convertedDate = new Date(input);
                    return $filter('date')(convertedDate, format ? format : 'dd/MM/yyyy');
                }
            }

        };
    });
