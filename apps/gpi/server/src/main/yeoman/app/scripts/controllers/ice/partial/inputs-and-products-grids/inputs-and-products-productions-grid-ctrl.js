'use strict';

angular.module('gpifrontApp')
    .controller('InputsAndProductsProductionsGridCtrl', function($scope, Restangular, $filter, $rootScope, GridButton, growl) {
     
        $scope.producoes = $scope.$parent.insumoProduto.producoes;

        $scope.selectedItems = [];

        $scope.columnDefs = [{
            field: 'ano',
            enableCellEdit: true,
            width: "**",
            translationKey: 'common.year',
            editableCellTemplate: '<input type="numeric" placeholder="{{\'AAAA\' | translate}}" ng-class="\'colt\' + col.index"  ng-input="COL_FIELD" ng-model="COL_FIELD" maxlength="4"/>'
        }, {
            field: 'areaPlantada',
            enableCellEdit: true,
            width: "**",
            translationKey: 'inputsandproducts.planted.area',
            editableCellTemplate: '<input type="numeric" placeholder="{{\'inputsandproducts.planted.area\' | translate}}" ng-class="\'colt\' + col.index" maxlength="18" ng-input="COL_FIELD" ng-model="COL_FIELD"/>'
        }, {
            field: 'moagemCana',
            enableCellEdit: true,
            width: "*",
            translationKey: 'inputsandproducts.cane.crushing',
            editableCellTemplate: '<input type="numeric" placeholder="{{\'inputsandproducts.cane.crushing\' | translate}}" ng-class="\'colt\' + col.index" maxlength="18" ng-input="COL_FIELD" ng-model="COL_FIELD"/>'
        }, {
            field: 'producaoEtanol',
            enableCellEdit: true,
            width: "*",
            translationKey: 'inputsandproducts.ethanol.production',
            editableCellTemplate: '<input type="numeric" placeholder="{{\'inputsandproducts.ethanol.production\' | translate}}" ng-class="\'colt\' + col.index" maxlength="18" ng-input="COL_FIELD" ng-model="COL_FIELD"/>'

        }, {
            field: 'producaoAcucar',
            enableCellEdit: true,
            width: "*",
            translationKey: 'inputsandproducts.sugar.production',
            editableCellTemplate: '<input type="numeric" placeholder="{{\'inputsandproducts.sugar.production\' | translate}}" ng-class="\'colt\' + col.index" maxlength="18" ng-input="COL_FIELD" ng-model="COL_FIELD"/>'

        }, {
            field: 'geracaoEnergia',
            enableCellEdit: true,
            width: "*",
            translationKey: 'inputsandproducts.power.cogeneration',
            editableCellTemplate: '<input type="numeric" placeholder="{{\'inputsandproducts.power.cogeneration\' | translate}}" ng-class="\'colt\' + col.index" maxlength="18" ng-input="COL_FIELD" ng-model="COL_FIELD"/>'

        }, {
            field: 'outro',
            enableCellEdit: true,
            width: "**",
            translationKey: 'inputsandproducts.other.products',
            editableCellTemplate: '<input type="numeric" placeholder="{{\'inputsandproducts.other.products\' | translate}}" ng-class="\'colt\' + col.index" maxlength="50" ng-input="COL_FIELD" ng-model="COL_FIELD" />'
        }];


        $scope.buttonsConfig = {
            //function GridButton(show, translationKey, searchMode, disable, action, screenLink)
            newButton: new GridButton(true, 'common.new', false, null, null, ''),
            deleteButton: new GridButton(true, 'common.delete', false, null, deleteFn, '')
        }

        function deleteFn() {
            growl.info($filter('translate')('message.error.MSG38_SECAO_ATUALIZADA', {
                value: "Produção"
            }));                        
        }                   

        $scope.sortInfo = {
            fields: ['nome'],
            directions: ['asc']
        };

        $scope.$parent.$watch('insumoProduto', function(newVal, oldVal) {
            if (newVal.producoes !== oldVal.producoes) {
                $scope.producoes = newVal.producoes;
            }
        }, true);
    });