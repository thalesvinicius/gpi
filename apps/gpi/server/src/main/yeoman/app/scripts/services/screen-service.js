'use strict';

angular.module('gpifrontApp')
    .factory('ScreenService', function($state, growl, dialogs, $filter) {
        return {
            isStateActive: function(stateName) {
                return $state.current.name === stateName;
            },
            isParentStateActive: function(stateName) {
                var valid = false;
                if (stateName) {
                    var parentStates = stateName.split('.');
                    var currentParentStates = $state.current.name.split('.');
                    for (var i = 0; i < parentStates.length; i++) {
                        if (parentStates[i] === currentParentStates[i]) {
                            valid = true;
                        } else {
                            valid = false;
                            break;
                        }
                    };
                }
                return valid;
            },
            isNewScreen: function() {
                return ($state.params.screen && $state.params.screen.toUpperCase() === 'INCLUIR');
            },
            isEditScreen: function() {
                return ($state.params.screen && $state.params.screen.toUpperCase() === 'EDITAR');
            },
            isShowScreen: function() {
                return ($state.params.screen && $state.params.screen.toUpperCase() === 'VISUALIZAR');
            },
            getScreenName: function() {
                return $state.params.screen;
            },
            getScreen: function(screenType) {
                return screenType[$state.params.screen.toUpperCase()];
            },
            notAllowed: function() {
                growl.warning("Não é permitido acessar essa aba em modo de inclusão!");
            },
            notAllowedGeneric: function() {
                growl.warning("Não é permitido acessar essa aba do modo solicitado!");  
            },
            cancelar: function(stateToGo, form, params) {
                // Somente mostra a mensagem se o usuário tiver alterado algo no form
                if(form !== undefined && form.$dirty){
                    var cancelDialog = dialogs.confirm($filter('translate')('DIALOGS_CONFIRMATION'), $filter('translate')('message.confirm.MSG31_CANCELAR'));
                    cancelDialog.result.then(function(btn) {
                        if (stateToGo === undefined && form === undefined && params === undefined) {
                            window.history.back();
                        } else {
                            //  Altera o form para pristine para não exibir a MSG33_DADOS_NAO_SALVOS
                            form.$setPristine();
                            $state.go(stateToGo, params);                            
                        }
                    });                    
                }else{
                        if (stateToGo === undefined && form === undefined && params === undefined) {
                            window.history.back();
                        } else {
                            $state.go(stateToGo, params);
                        }
                }
            },

            blobSave: function(file,blob){

                var url = URL.createObjectURL(blob);                
                var a = document.createElement("a");
                
                var is_chrome = navigator.userAgent.toLowerCase().indexOf('chrome') > -1;
                var is_mozilla = navigator.userAgent.toLowerCase().indexOf('mozilla') > -1;
                
                
                    a.download = file;
                    a.href = url;
                
                
                if(is_chrome){
                    window.self.document.body.appendChild(a);
                    a.click();                    
                }else if(is_mozilla)  {
                     var newWindow = window.open("","","",false);
                     newWindow.document.body.appendChild(a);
                     a.click();

                } else {
                    window.open(url,"","",false)
                }

            }  ,  
            browserAwareSave: function(data,file,mimetype){
                var blob = new Blob([data], {
                            type: mimetype  });
                if (navigator.saveBlob || window.saveAs) {                        
                        if (!file) file = "Download.bin";
                        if (window.saveAs) {
                            window.saveAs(blob, file);
                        } else {
                            navigator.saveBlob(blob, file);
                        }
                    
                } else if(navigator.msSaveBlob){
                    navigator.msSaveBlob(blob, file)
                } else{
                    this.blobSave(file,blob);
                }
            }//Fim Função

            

        };
    });
