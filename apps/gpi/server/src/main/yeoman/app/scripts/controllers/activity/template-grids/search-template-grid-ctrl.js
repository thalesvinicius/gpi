'use strict';

angular.module('gpifrontApp')
    .controller('SearchTemplateGridCtrl', function($scope, GridButton, TIPO_TEMPLATE, growl, Restangular) {
    	
    	$scope.gridScope = {
    		tipo: TIPO_TEMPLATE
    	}

   		// initialize its selected-items attribute
        $scope.atividadeTemplatesEncontradosSelectedItems = [];
        
        //Define its column-defs attribute
        $scope.atividadeTemplatesEncontradosColumnDefs = [{            
            field: 'nome',
            enableCellEdit: false,
            width: "*",
            translationKey: 'common.name'
        }, {
            field: 'tipo',
            enableCellEdit: false,
            width: "**",
            translationKey: 'common.type',
            cellTemplate: '<div class="ngCellText"><span ng-class="\'colt\' + col.index"> {{tipo[row.entity.tipo].descricao}} </span>'                        
        }, {
            field: "status",
            enableCellEdit: false,
            width: "**",
            translationKey: 'common.status',
            cellTemplate: '<div class="ngCellText" ng-switch on="row.entity.status"><span ng-class="\'colt\' + col.index" ng-switch-when="true" translate>common.active</span><span ng-class="\'colt\' + col.index" ng-switch-when="false" translate>common.inactive</span>'            
        }, {
            field: 'show',
            translationKey: 'common.show',
            showScreenRedirect: true,
            width: "**",
            showLink: '/atividade/visualizar'
        }];

        $scope.atividadeTemplatesEncontradosButtonsConfig = {
            
            newButton: new GridButton(true, 'common.new', true, null, null, '/atividade/incluir'),
            editButton: new GridButton(true, 'common.edit', true, disableFn, null, '/atividade/editar'),
            deleteButton: new GridButton(true, 'common.delete', false, null, deleteFn, '')
        }

        //define its sort-info attribute if you desire a default-sort
        $scope.atividadeTemplatesEncontradosSortInfo = {
            fields: ['id'],
            directions: ['asc']
        };

        function disableFn() {
            return $scope.atividadeTemplatesEncontradosSelectedItems.length !== 1
        }

        function deleteFn () {

            var idsToDelete = [];
            if ($scope.atividadeTemplatesEncontrados.length > 0 && $scope.atividadeTemplatesEncontradosSelectedItems.length > 0) {
                $scope.atividadeTemplatesEncontradosSelectedItems.forEach(function(selected) {
                    idsToDelete.push(selected.id);
                });
            }

            if (idsToDelete.length > 0) {

                Restangular.all('template/removeTemplate').post(idsToDelete).then(function() {
                    growl.success("Exclusão efetuada com sucesso.")
                
                }, function() {
                    growl.error('Ocorreu um erro ao tentar efetuar a exclusão.')
                    return false;
                })
            }

            return true;
        };        


    });
