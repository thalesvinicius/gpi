angular.module('gpifrontApp')
    .directive('reunionPanelReport', function($compile, $rootScope, configuration, growl, ESTAGIO_PROJETO, AuthService, AUTH_ROLES, Restangular, $translate, $http, $filter, $window, ngTableParams, GridButton, TIPO_INSTRUMENTO_FORMALIZACAO) {

        return {
            restrict: 'E',
            templateUrl: configuration.rootDir + 'scripts/directives/reunion-panel-reports/reunion-panel-reports.html',
            scope: {
                serviceUrl: "=serviceUrl",
                filterOptions: "=filterOptions",
                columnDefs: "=columnDefs",
                hideTotals: "=hideTotals",
                columnsName: "=columnsName",
                data: "=ngModel",
                callbackPromise: "="
            },
            link: function(scope, iElement, iAttrs) {
                // scope.data = [];
                scope.displaySearchFilters = false;
                scope.$parent.panelScope = scope;

                var daysInMonth = function(month, year) {
                    var m = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
                    if (month != 2) return m[month - 1];
                    if (year % 4 != 0) return m[1];
                    if (year % 100 == 0 && year % 400 != 0) return m[1];
                    return m[1] + 1;
                }

                var initialize = function() {
                    scope.canEditSearchPanel = false;
                    scope.disableFooter = true;
                    scope.selectedItems = [];
                    scope.filtro = {
                        regioesPlanejamento: [],
                        departamentos: []
                    };

                    if (scope.filterOptions !== undefined && scope.filterOptions.estagioNotMultiple) {
                        scope.filtro.estagio = ESTAGIO_PROJETO.FORMALIZADA.id;
                    }

                    if (scope.filterOptions !== undefined && scope.filterOptions.tipoInstrumentoFormalizacao) {
                        scope.filtro.tiposInstrumentoFormalizacao = [TIPO_INSTRUMENTO_FORMALIZACAO.PROTOCOLO_INTENCAO.id];
                    }

                    if ($rootScope.mesAno === undefined && scope.filterOptions.mesAno) {
                        $rootScope.mesAno = new Date().getTime();
                    }

                    if (($rootScope.dataInicio === undefined || $rootScope.dataFim === undefined) && scope.filterOptions.intervaloDatas) {
                        var aux = new Date();
                        aux.setDate(1);
                        $rootScope.dataInicio = aux.getTime();
                        $rootScope.dataFim = new Date().getTime();
                    }

                    if (scope.filterOptions !== undefined && scope.filterOptions.mesAno) {
                        if ($rootScope.dataInicio !== undefined) {
                            scope.filtro.mesAno = $rootScope.dataInicio;
                        } else {
                            scope.filtro.mesAno = $rootScope.mesAno;
                        }
                    } else if (scope.filterOptions !== undefined && scope.filterOptions.intervaloDatas) {
                        if (scope.serviceUrl === 'report/painelreuniao/projetosemdf' && $rootScope.mesAno !== undefined) {
                            scope.filtro.dataInicio = $rootScope.mesAno;
                            scope.filtro.dataFim = scope.filtro.dataInicio;
                            if (scope.filtro.dataFim && new Date(scope.filtro.dataFim).getMonth() !== new Date().getMonth()) {
                                var ultimoDiaMes = new Date(scope.filtro.dataInicio);
                                ultimoDiaMes.setDate(daysInMonth(ultimoDiaMes.getMonth() + 1, ultimoDiaMes.getYear()));
                                scope.filtro.dataFim = ultimoDiaMes;
                            } else {
                                scope.filtro.dataFim = new Date(scope.filtro.dataFim);
                                scope.filtro.dataFim.setDate(new Date().getDate());
                            }
                        } else {
                            scope.filtro.dataInicio = $rootScope.dataInicio;
                            scope.filtro.dataFim = $rootScope.dataFim;
                        }
                    }

                    if (scope.filtro.diretoria === undefined) {
                        scope.filtro.diretoria = $rootScope.diretoria;
                    }

                    scope.filtro.anual = false;

                    scope.searchFromPanel();
                    scope.resetFilters();
                }

                //define its buttons-config attribute on a persistence screen
                scope.buttonsConfig = {
                    newButton: new GridButton(false, 'contact.new', false, null, null, ''),
                    deleteButton: new GridButton(false, 'common.delete', false, null, null, '')
                }

                scope.gridScope = {
                    options: {
                        showSelectionCheckbox: false,
                        enableCellSelection: false,
                        enableCellEdit: false,
                        enableRowSelection: false
                    }
                }

                function translate(translationKey) {
                    var text = "";
                    if (typeof translationKey !== 'undefined' && translationKey) {
                        text = $filter('translate')(translationKey);
                    }
                    return text;
                }

                //  Configurações do datepicker
                scope.configMonthYearDatePicker = {
                    disabled: false,
                    viewMode: 'months',
                    restangularUpdated: false
                }
                scope.configsIntervalDatepicker = {
                    disabled: false,
                    viewMode: 'days',
                    restangularUpdated: false
                }


                scope.resetFilters = function() {
                    scope.estagios = [
                        ESTAGIO_PROJETO.INICIO_PROJETO,
                        ESTAGIO_PROJETO.PROJETO_PROMISSOR,
                        ESTAGIO_PROJETO.FORMALIZADA,
                        ESTAGIO_PROJETO.IMPLANTACAO_INICIADO,
                        ESTAGIO_PROJETO.OPERACAO_INICIADA,
                        ESTAGIO_PROJETO.COMPROMISSO_CUMPRIDO
                    ];

                    scope.filtro.estagios = [ESTAGIO_PROJETO.FORMALIZADA.id];
                    scope.changeStageSelect();

                    Restangular.all('domain/regiaoPlanejamento').getList().then(function(regioesPlanejamento) {
                        scope.regioesPlanejamento = regioesPlanejamento;
                        //  Por default todas as regioesPlanejamento vem selecionadas
                        // scope.filtro.regioesPlanejamento = getIdList(regioesPlanejamento);
                    });

                    Restangular.all('domain/departamento').getList().then(function(departamentos) {
                        scope.gerencias = departamentos;
                        //  Por default todas as gerencias vem selecionadas
                        // scope.filtro.departamentos = getIdList(departamentos);
                    });

                    Restangular.all('departamento/diretorias').getList().then(function(diretorias) {
                        scope.diretorias = diretorias;
                    })

                    scope.tiposInstrumentoFormalizacao = TIPO_INSTRUMENTO_FORMALIZACAO.getAll();

                    scope.data = [];
                }

                scope.clean = function() {
                    scope.resetFilters();
                }

                scope.enableEditSearchPanel = function() {
                    scope.configMonthYearDatePicker.disabled = false;
                    scope.canEditSearchPanel = true;
                    scope.displaySearchFilters = false;
                }

                var normalizeDatas = function(panelFilters) {
                    if (scope.filterOptions.mesAno) {
                        if (scope.filtro.mesAno && scope.filtro.mesAno !== 0) {
                            scope.filtro.mesAno = new Date(scope.filtro.mesAno).getTime();
                            panelFilters.mesAno = scope.filtro.mesAno;
                        } else {
                            throw "O Período de Análise deve ser informado!";
                        }
                    }

                    if (scope.filterOptions.intervaloDatas) {
                        if (scope.filtro.dataInicio && scope.filtro.dataInicio !== 0) {
                            var aux = new Date(scope.filtro.dataInicio);
                            scope.filtro.dataInicio = aux.getTime();
                            panelFilters.dataInicio = scope.filtro.dataInicio;
                        } else {
                            throw "A data de início deve ser informada!";
                        }

                        if (scope.filtro.dataFim && scope.filtro.dataFim !== 0) {
                            scope.filtro.dataFim = new Date(scope.filtro.dataFim).getTime();
                            panelFilters.dataFim = scope.filtro.dataFim;
                        } else {
                            throw "A data de fim deve ser informada!";
                        }

                        if (scope.filtro.dataInicio !== undefined && scope.filtro.dataFim !== undefined && scope.filtro.dataInicio > scope.filtro.dataFim) {
                            throw "O Período de Análise informado é inválido!";
                        }
                    }

                }

                var setNormalizedDiretoria = function(panelFilters) {
                    if (scope.filtro.diretoria !== undefined && scope.filtro.diretoria !== "") {
                        panelFilters.diretoria = scope.filtro.diretoria;
                    }
                }

                scope.searchFromPanel = function() {
                    var panelFilters = {}

                    /* Limpa as variaveis de escopo global, dos filtros de busca avançada, que são utilizadas apenas
                       pelo serviço "report/painelreuniao/projetosnoano". Essas variáveis foram a forma encontrada para
                       a diretiva passar parâmetros para o controller pai. */
                    if (scope.serviceUrl !== 'report/painelreuniao/projetosnoano') {
                        $rootScope.gerencias = [];
                        $rootScope.cadeiasProdutivas = [];
                        $rootScope.tiposInstrumentoFormalizacao = [];
                    }

                    try {
                        if (scope.filterOptions != undefined) {
                            if (scope.filterOptions.tipoInstrumentoFormalizacao) {
                                panelFilters.tiposInstrumentoFormalizacao = scope.filtro.tiposInstrumentoFormalizacao
                            }
                            if (scope.filterOptions.estagioNotMultiple) {
                                panelFilters.estagio = scope.filtro.estagio;
                            }
                        }

                        setNormalizedDiretoria(panelFilters);
                        normalizeDatas(panelFilters);
                        searchWithFilters(panelFilters);

                    } catch (err) {
                        growl.error(err);
                    }
                }

                scope.search = function() {
                    scope.filtro.diretoria = undefined;
                    try {
                        normalizeDatas(scope.filtro);
                        searchWithFilters(scope.filtro);
                    } catch (err) {
                        growl.error(err);
                    }
                };

                var searchWithFilters = function(filtro) {
                    scope.numeroProjetos = 0;
                    scope.totalInvestimentoPrevisto = 0;
                    scope.totalFaturamentoInicial = 0;
                    scope.totalFaturamentoPleno = 0;
                    scope.totalEmpregosDiretos = 0;

                    if (filtro && filtro.dataInicio) {
                        filtro.dataIni = filtro.dataInicio;
                    }
                    if (!scope.serviceUrl) {
                        growl.error("Error: service URL must be entered!");
                        return;
                    }

                    Restangular.all(scope.serviceUrl).getList(filtro).then(function(result) { 
                        scope.data = result;

                        if (scope.columnsName && scope.data.length > 0) {
                            scope.tableParams = new ngTableParams({
                                page: 1, // show first page
                                count: scope.data.length
                            }, {
                                total: scope.data.length, // length of data
                                getData: function($defer, params) {                                                                
                                    var orderedData = params.sorting() ?
                                        $filter('orderBy')(scope.data, params.orderBy()) :
                                        scope.data;

                                    $defer.resolve(orderedData);
                                }
                            });
                        }
                        if (result.length === 0) {
                            growl.error(translate('message.error.MSG004_NENHUM_REGISTRO_ENCONTRADO'));
                        } 
                        if (scope.callbackPromise) {
                            scope.callbackPromise(result);
                        }

                        angular.forEach(result, function(it) {
                            scope.totalInvestimentoPrevisto += it.investimentoPrevisto;
                            scope.totalFaturamentoInicial += it.faturamentoInicial;
                            scope.totalFaturamentoPleno += it.faturamentoPleno;
                            scope.totalEmpregosDiretos += it.empregosDiretos;
                            scope.numeroProjetos++
                        })

                    }, function errorCallback(response) {
                        if (response.status !== 404) {
                            growl.error("Error: The server returned status " + response.status);
                        }
                    });
                }

                /*
                 *   Recupera todos os ids da lista passada
                 */
                var getIdList = function(list) {
                    var idList = [];
                    if (_.isArray(list)) {
                        _.each(list, function(val, key) {
                            idList.push(val.id);
                        });
                    } else {
                        idList.push(list.id);
                    }
                    return idList;
                }

                // RNE163 se for gerente não exibe o campo Gerência e se for analista de portfolio
                //  não exibe os campos Gerência e Analista
                scope.isGerente = function() {
                    return AuthService.isUserAuthorized([AUTH_ROLES.manager]);
                }

                // RNE163 se for gerente não exibe o campo Gerência e se for analista de portfolio
                //  não exibe os campos Gerência e Analista
                scope.isPromocao = function() {
                    return AuthService.isUserAuthorized([AUTH_ROLES.promotionAnalist]);
                }


                scope.changeStageSelect = function() {
                    scope.estagioSelecionado = ESTAGIO_PROJETO[scope.filtro.estagio];
                }

                /*
                 *   Evento chamado ao selecionar gerência
                 */
                scope.changeManageMultiSelect = function() {
                    //  Busca os cadeiras produtivas com base no departamento
                    if (scope.filtro.departamentos && scope.filtro.departamentos.length > 0) {

                        Restangular.all('cadeiaProdutiva/byDepartamentos').getList({
                            "departamentos": scope.filtro.departamentos
                        }).then(function(cadeiasProdutivas) {
                            scope.cadeiasProdutivas = cadeiasProdutivas;
                            //  Por default todas as cadeiasProdutivas vem selecionadas
                            // scope.filtro.cadeiasProdutivas = getIdList(cadeiasProdutivas);
                        });
                    } else {
                        scope.cadeiasProdutivas = [];
                        scope.filtro.cadeiasProdutivas = [];
                    }
                }

                scope.changeDiretoriaMultiselect = function() {
                    if (scope.filtro.diretoria && scope.filtro.diretoria !== "") {
                        Restangular.all('departamento/departamentoSuperiorOf').getList({
                            'departamentoSuperiorId': scope.filtro.diretoria
                        }).then(function(departamentos) {
                            scope.gerencias = departamentos;
                            //  Por default todas as gerencias vem selecionadas
                            // scope.filtro.departamentos = getIdList(departamentos);
                        });
                    } else {
                        Restangular.all('domain/departamento').getList().then(function(departamentos) {
                            scope.gerencias = departamentos;
                            //  Por default todas as gerencias vem selecionadas
                            // scope.filtro.departamentos = getIdList(departamentos);
                        });
                    }
                }

                scope.toggleDisplaySearchFilters = function() {
                    if (scope.displaySearchFilters) {
                        scope.displaySearchFilters = false;
                    } else {
                        scope.displaySearchFilters = true;
                        scope.canEditSearchPanel = false;
                    }

                    return scope.displaySearchFilters;
                }

                scope.hasBodyPanelFilters = function() {
                    return scope.filterOptions !== undefined && (scope.filterOptions.estagio || scope.filterOptions.regiaoPlanejamento || scope.filterOptions.gerencia || scope.filterOptions.cadeiaProdutiva || scope.filterOptions.prospeccao);
                }

                scope.$watch('filtro.mesAno', function() {
                    $rootScope.mesAno = scope.filtro.mesAno;
                });
                scope.$watch('filtro.dataInicio', function() {
                    $rootScope.dataInicio = scope.filtro.dataInicio;
                });
                scope.$watch('filtro.dataFim', function() {
                    $rootScope.dataFim = scope.filtro.dataFim;
                });
                scope.$watch('filtro.diretoria', function() {
                    $rootScope.diretoria = scope.filtro.diretoria;
                });
                scope.$watch('filtro.gerencias', function() {
                    $rootScope.gerencias = scope.filtro.gerencias;
                });
                scope.$watch('filtro.cadeiasProdutivas', function() {
                    $rootScope.cadeiasProdutivas = scope.filtro.cadeiasProdutivas;
                });
                scope.$watch('filtro.tiposInstrumentoFormalizacao', function() {
                    $rootScope.tiposInstrumentoFormalizacao = scope.filtro.tiposInstrumentoFormalizacao;
                });

                initialize();
            }
        };

    });
