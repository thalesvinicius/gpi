#!/bin/bash
#Require 
sudo -i
#Installing NVM
curl https://raw.githubusercontent.com/creationix/nvm/v0.10.0/install.sh | sh
source ~/.bash_profile
#Install NPM
nvm install 0.10.28
nvm use 0.10.28
nvm alias default 0.10
#Install yeoman / bower / grunt
npm install -g yo
mkdir -p /opt/.cache
cd /opt/.cache
wget http://download.oracle.com/otn-pub/java/jdk/8u5-b13/jdk-8u5-linux-x64.tar.gz
wget http://ftp.unicamp.br/pub/apache/maven/maven-3/3.2.2/binaries/apache-maven-3.2.2-bin.tar.gz
wget http://download.netbeans.org/netbeans/8.0/final/bundles/netbeans-8.0-javaee-linux.sh
mv *.* /opt/

cd /opt/
tar -xvzf jdk-8u5-linux-x64.tar.gz
rm jdk-8u5-linux-x64.tar.gz
tar -xvzf apache-maven-3.2.2-bin.tar.gz
rm apache-maven-3.2.2-bin.tar.gz
sudo ln -s /opt/apache-maven-3.2.2/bin/mvn /usr/bin/mvn

chmod +x /opt/netbeans-8.0-javaee-linux.sh
#Install Netbeans
sudo sh /opt/netbeans-8.0-javaee-linux.sh
#Importar configurações do nebeans