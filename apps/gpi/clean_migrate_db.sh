mvn -f server/pom.xml flyway:clean flyway:migrate
H2_LOCATION=~/.m2/repository/com/h2database/h2/1.3.176/h2-1.3.176.jar
POPULATE_SCRIPT=./server/src/main/resources/db/migration/Add_Info_BD.sql
java -cp $H2_LOCATION org.h2.tools.RunScript -url "jdbc:h2:~/db;MODE=MSSQLServer;AUTO_SERVER=TRUE;" -script $POPULATE_SCRIPT -user "sa" -password "" 
